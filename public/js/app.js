const REQUEST_TYPE_POST = 'post';
const REQUEST_TYPE_GET  = 'get';
const REQUEST_TYPE_PUT  = 'put';
const REQUEST_TYPE_DELETE  = 'delete';
const DATA_TYPE_JSON = 'json';
const BORDER_COLOR = 'border-color';
const COLOR = 'color';
const RED_COLOR = 'red';
const LIGHT_GREY_COLOR = 'lightgrey';
const SUCCESS_MODAL_DURATION = 5000;
const MONTHLY = 'monthly';
const ALL = 'all';
const DELETE = 'DELETE';
const REMOVE = 'REMOVE';
const YES = 'yes';
const NO = 'no';
const SUCCESS = 'success';
const FAILED = 'failed';

$(function(){

	var blockScreen = false;
	$.fn.setError = (element, message) => {
		var parent = element.parents('.form-errors:eq(0)');
		if(parent.hasClass('has-error')){
			return null;
		}
		parent.addClass('has-error')
		.append(`<span class="help-block">${message}</span>`)
	};

	$.fn.unsetError = (element) => {
		var parent = element.parents('.form-errors:eq(0)');
		parent.removeClass('has-error');	
		parent.find('.help-block').remove();
	};

	$.fn.displayErrors = (errors) => {
	    var html=`<ul style="list-style: none;"`;
	    $.each(errors, function(i, v) {
	        html += `<li>${v}</li>`;                
	    });
	    return html+'</ul>';
	};

	$.fn.ajaxCall = (type, url, data, dataType) => {
		$('#errorsDiv').html('');
		blockScreen = true;
		return new Promise( (resolve, reject) => {
			var ajaxRequest = {
				type: type,
				url: url,
				data: data,
		        dataType: dataType,
		        success: function (data) {
		        	blockScreen = false;
		            resolve(data);
		        },
		        error: function (data) {
                    var response = JSON.parse(data.responseText);
                    var errors = [];
                    if (typeof(response.errors) === 'string') {
                        errors.push(response.errors);
                    } else {
                        $.each(response.errors, function(i, v) {
                            $.each(v, function(x, e) {
                                errors.push(e);
                            });
                        });
                    }
                    $('#errorModalErrors').html($.fn.displayErrors(errors));
                    $('#errorModal').modal('show');
		        	//reject(data);
		        }
			};
			if (type !== REQUEST_TYPE_GET) {
		        var headers = {
		        	headers: {
		        		'X-CSRF-TOKEN': 
		        			$('meta[name="csrf-token"]').attr('content')
		        	},
		        	contentType: 'application/json; charset=utf-8'
		        };
		        ajaxRequest = Object.assign(headers, ajaxRequest);
			}

			$.ajax(ajaxRequest);
		});
	};

	$.fn.ajaxBlockUI = (msg='') => {
		$.blockUI({message: 
			`<h2>${msg} <i class="fa fa-spinner fa-pulse fa-fw"></i></h2>`, 
			baseZ: 1005 
		});
	};

	$.fn.validateRequest = (form) => {
		$(form).find('.required').css(BORDER_COLOR, LIGHT_GREY_COLOR);
		var response = true;
		$.each($(form).find('.required'), (i, v) => {
			if(!$(v).val().trim().length) {
				$(v).focus();
				$(v).css(BORDER_COLOR, RED_COLOR);
				response = false;
				return response;
			}
		});
		
		return response;
	};

	$.fn.yajraDataTable = (table, url, columns, initComplete = null) => {
		$(table).DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: url,
	        columns: columns,
	        "initComplete": initComplete

	    });
	};

	if($('#serverErrors').length){
        $('#errorModal').modal('show');
    }

    if($('#successStatus').length){
        $('#successModal').modal('show');
        setTimeout(function() {
          $('#successModal').modal('hide');
        }, SUCCESS_MODAL_DURATION);
    }

    $.fn.checkBrowser = () => {
	    var ua = window.navigator.userAgent;
	    var msie = ua.indexOf("MSIE ");
	    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  
	    // If Internet Explorer, return version number
	    {
	        $('html').addClass("ie");
	    }
	    var c = navigator.userAgent.search("Chrome");
	    var d = navigator.userAgent.search("Edge");
	    var f = navigator.userAgent.search("Firefox");
	    if (c > -1) {
	        $('html').addClass("chrome");
	    }
	    else if (f > -1) {
	        $('html').addClass("firefox");
	    }
	    if (d > -1) {
	        $('html').removeClass("firefox");
	        $('html').removeClass("chrome");
	        $('html').addClass("edge");
	    }
	}

	$.fn.checkBrowser();

    $(document).ajaxStart(() => {
    	if(blockScreen){
    		$.blockUI();
    	}
    }).ajaxStop($.unblockUI);

    if ($('.js__profile-toggle').length) {
	    $('.js__profile-toggle').on('click', () => {
	        $('.js__profile').toggleClass('active');
	        $('.js__profile-toggle i').toggleClass('hidden');
	    });
	}


	$.fn.showOverlay = (dataOverlay) => {
		var $overlay = $('.overlay[data-overlay=' + dataOverlay + ']');
        $overlay.addClass('active');
        setTimeout(function () {
            $overlay.addClass('animate');
        }, 100);
	}

    $(document).on('click','.js__overlay-show', function(e) {
        e.preventDefault();
        $.fn.showOverlay($(this).data('overlay'));
    });

    $(document).on('click','.js__overlay-close', function(e) {
        var dataOverlay = $(this).data('overlay');
        var $overlay = $('.overlay[data-overlay=' + dataOverlay + ']');
        e.preventDefault();
        $overlay.removeClass('active');
        setTimeout(function () {
            $overlay.removeClass('animate');
        }, 350);
        // Wanneer er een video speelt, video pauzeren
        if ($overlay.hasClass('js__overlay-video')) {
            var $video = $overlay.find('iframe');
            var thisFrame = $video.get(0);
            $(thisFrame).attr('src', $(thisFrame).attr('src'));
        }
    });

    if ($('.select2').length) {
    	$('.select2').select2();
    }

    $(document).on('keydown', 'form', function(e){
        if (e.keyCode === 13) {
            if ($(this).find('.submitBtn').length === 1) {
                $(this).find('.submitBtn').trigger('click');
            }
            return false;
        }
    });

    $(document).on('submit', 'form', function(e){
        $(this).find('.submitBtn').prop('disabled', true);
    });

    $(document).on('change','.templateStatus',function () {
        if($('.templateStatus:checked').val() === "Yes") {
            $("#learningGoalList").removeClass('hidden');
        } else {
            $("#learningGoalList").addClass('hidden');
        }
    });

    // preview learning goal
    $("#view_learning").click(function (){

        if($.trim($("input[name='video1']").val())=='' || $.trim($("input[name='video1']").val())==undefined){
            $('#video1-box .video').hide();
            $('#video1-box .invalid-video').show();
        } else {
            $('#video1-box .video').show();
            $('#video1-box .invalid-video').hide();
            $('#lg_video1').attr('src', $.trim($("input[name='video1']").val()));
        }

        if($.trim($("input[name='video2']").val())=='' || $.trim($("input[name='video2']").val())==undefined){
            $('#video2-box .video').hide();
            $('#video2-box .invalid-video').show();
        } else {
            $('#video2-box .video').show();
            $('#video2-box .invalid-video').hide();
            $('#lg_video2').attr('src', $.trim($("input[name='video2']").val()));
        }
        
        $("#learningGoalId").html($.trim($("input[name='learningGoalId']").val()));
        $("#lg_name").html($.trim($("input[name='name']").val()));
        $("#lg_theory").html($.trim(CKEDITOR.instances.theory.getData()));
        $("#lg_example").html($.trim(CKEDITOR.instances.example.getData()));
        $("#lg_videotext1").html($.trim($("input[name='video_text1']").val()));
        $("#lg_videotext2").html($.trim($("input[name='video_text2']").val()));
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        MathJax.Hub.Queue(function(){
            $.fn.replacePercentage('lg_theory');
            $.fn.replacePercentage('lg_example');
        });
        $(document).find('table').addClass('table');
    });


    // toggle vie more/less
	$('.toggle-content').css('max-height', 110);
    $('.js__toggle-content').click(function (e) {
        e.preventDefault();
            if ($(this).hasClass('up')) { // all content visible
                $(this).siblings('.toggle-content').css('max-height', '110px');
                $(this).removeClass('up');
                $(this).html($(this).attr('attr-more'));
            }
            else { //content hidden
                $(this).siblings('.toggle-content').css('max-height', '100%');
                $(this).addClass('up');
                $(this).html($(this).attr('attr-less'));
            }
    });

    $.fn.replacePercentage = (id) => {
    	var val = $("#"+id).html();
	    val = val.replace(/\\%/g, '%');
	    $("#"+id).html();
	    $("#"+id).html(val);
    };

    $.fn.loadValidity = (typeSlct) => {
        var validityInpt = typeSlct.closest('form').find('.validityInpt');
        if(typeSlct.val() === MONTHLY){
            validityInpt.val('').parent().css('display', 'none');
        } else {
            validityInpt.val('365').parent().css('display', 'block');
        }
    };

    $('#codeInptBtn').on('click', function(){
        var code = $('#codeInpt').val();
        window.location = `${$(this).attr('data-route')}/${code}`;
    });

    $('.js__dropdown').click(function (e) {
        e.stopPropagation();
        var $this = $(this);
        var $item = $this.find('li');

        if ($this.hasClass('active')) {
            $this.removeClass('active');
        }
        else {
            $this.addClass('active');
            $item.each(function () {
                $(this).addClass('show');
            });
            $this.find('li').click(function (e) {
                e.preventDefault();
                $item.each(function () {
                    $(this).removeClass('show');
                    $(this).removeClass('active');
                })
                $(this).addClass('active');
            });
        }
    });

    $(document).mouseup(function (e) {
        var container = $('.js__dropdown');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.removeClass('active');
            container.find('li').removeClass('show');
            $('.js__dropdown li').unbind('click');
        }
    });
    
    $(document).find('.math-tex').css('font-size', '90%');

    $.fn.sendStudentFeedback = data => {
        var route = $('#feedbackRoute').val();
        if(data.message.length && route.length) {
            $.fn.ajaxCall(REQUEST_TYPE_POST, route, JSON.stringify(data), 
                DATA_TYPE_JSON).then((successResponse) => {
                    $('#successModal').find('#successModalStatus').html(successResponse.message);
                    $('#successModal').modal('show');
                }, (errorResponse) => {
            });
        }
    };

    $('#generalFeedbackSend').on('click', () => {
        var data = {};
        data.message = $('#generalFeedbackText').val();
        if ($('#userLicenseId').length) {
            data.license_id = $('#userLicenseId').val();
        }
        if ($('#userSubjectId').length) {
            data.subject_id = $('#userSubjectId').val();
        }
        if ($('#userLearningGoalId').length) {
            data.learning_goal_id = $('#userLearningGoalId').val();
        }
        if ($(document).find('#learningGoalQuestionDiv').length) {
            var learningGoalQuestionDiv = $(document).find('#learningGoalQuestionDiv');
            data.question_answer_id = learningGoalQuestionDiv.attr('data-questionId');
        }
        $.fn.sendStudentFeedback(data);
        $('#generalFeedbackText').val('');
    });

    $(document).find('table').addClass('table');

    $.fn.subjectLearningGoal = (route, data, val, field) => {
        $.fn.ajaxCall(REQUEST_TYPE_POST, route, JSON.stringify(data), DATA_TYPE_JSON)
        .then((successResponse) => {
            $.each(successResponse, function (key, value) {
               field.closest("tr").find(".subjectLearningGoal").append(
                "<option value=" + value.id + ">" + value.name + "</option>");
           });
           if(val != "") {
               field.closest("tr").find(".subjectLearningGoal").val(val);
           }
        });
    }

    $('#studentDashboardBtn').on('click', function(){
        var thisBtn = $(this);
        var data = {
            "license": thisBtn.attr('data-licenseId'),
        }
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            thisBtn.attr('data-route'), JSON.stringify(data), DATA_TYPE_JSON)
        .then((successResponse) => {
            if (successResponse.response) {
                window.location =  successResponse.route;
            } else {
                $('#errorModal').find('#errorModalErrors').html(successResponse.message);
                $('#errorModal').modal('show'); 
            }
        });
    });

    //Progress bar animate js
    $('.js__progress-bar').each(function () {
        $(this)
            .data("origWidth", $(this).width())
            .width(0)
            .animate({
                width: $(this).data('origWidth')
            }, 1200);
    });

    $('.js__chart-bar > span').each(function () {
        $(this)
            .data("origHeight", $(this).height())
            .height(0)
            .animate({
                height: $(this).data('origHeight')
            }, 1200);
    });
});
