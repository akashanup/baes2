$(function(){
	$('#addPracticeTestQuestionBtn').on('click', function () {
       $('#practiceTestDetailTable').append($('#hidden_row').clone());
       $('#practiceTestDetailTable tr:last').removeAttr('id');
       $('#practiceTestDetailTable tr:last').removeClass('hidden');
       $('#practiceTestDetailTable tr:last td:first').html(
        parseInt($('#practiceTestDetailTable tr:last').prev().find('td:first').html()) + 1);
    });
    $(document).on('click', '.deletePracticeTestQuestionBtn', function(){
    	$(this).closest("tr").remove();
        var table_length  = $('#practiceTestDetailTable').find('tbody').children().length;
        var delete_serial = parseInt($(this).closest('tr').find('td:nth-child(1)').html())+1;
        for(var index = delete_serial;index <= table_length; index++) {
            $('#practiceTestDetailTable tr:nth-child('+ index +') td:nth-child(1)').html(index-1);
        }
    });

    $(document).on('change', '.practiceTestDetailSubject', function (event,val) {
        var field= $(this);
        field.closest('tr').find("select[name='goal_selected']").empty();
        if(field.val() != '') {
           	var data = {
                'subject_id' : field.val(),
            };
            $.fn.subjectLearningGoal($('#subjectLearningGoal').val(), data, val, field);
        }
    });

    $('#savePracticeTestBtn').on('click', function(){
    	var template = {};
        var practice_test_name = $.trim($("#testTemplateName").val());
        if(practice_test_name) {
            $("#practiceTestDetailTable").find("tbody").find('tr').each(function (i, el) {
                if(i != 0 && !(isNaN(parseInt($(this).find(".practiceTestDetailSubject").val())))
                    && !(isNaN(parseInt($(this).find(".subjectLearningGoal").val())))){
                	template[i] = {};
                    template[i]['subject_id'] = $(this).find(".practiceTestDetailSubject").val();
                    template[i]['goal_id']    = $(this).find(".subjectLearningGoal").val();
                    template[i]['level']      = $(this).find(".complexityLevel").val();
                    var checkbox = $(this).find("input[name='random_selected']");
                    if(checkbox.prop("checked")) {
                        template[i]['random'] = YES;
                    } else {
                        template[i]['random'] = NO;
                    }
                }
            });
            if(Object.keys(template).length>0) {
                var data = {
                    "template": JSON.stringify(template),
                    "name": practice_test_name,
                };
                $.fn.ajaxCall(REQUEST_TYPE_POST, $(this).attr('data-route'), JSON.stringify(data), DATA_TYPE_JSON)
		        .then((successResponse) => {
		            if(successResponse.response){
		            	window.location = successResponse.route;
		            } else{
		                $('#errorModalErrors').html(successResponse.message);
		                $('#errorModal').modal('show');
		            }
		        });
            } 
        }
    });
});