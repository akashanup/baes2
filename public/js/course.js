$(function(){
    const INSTITUTION = 'institution';
    const SUBJECT = 'subject';
    var deleteStatus = false;

    if($('#course-table').length) {
        $.fn.yajraDataTable(
            '#course-table', 
            $('#coursesRoute').val(), 
            [
                {data: 'id', name: 'courses.id'},
                {data: 'name', name: 'courses.name'},
                {data: 'institution', name: 'institution', searchable: false},
                {data: 'subject', name: 'count', searchable: false},
                {data: 'action', orderable: false, searchable: false},
            ]
        );
    }
    $('#addSubject').on('click', () => {
        var tableBody = $('#subjectTable').find('tbody');
        tableBody.append($('#subjectHiddenTable').find('tbody').html());
        $.fn.checkSubjectChangeForPutRequest();
    });

    $(document).on('change', '.subjectId', function(){
        $(this).closest('tr').find('.learningGoalsCount').val(
            $(this).find(':selected').attr('data-learning-goal-count')
        );
        $('#subjectTable').find('thead').find('tr').css(COLOR, 'black');
    });

    $(document).on('click', '.deleteSubject', function(){
        $(this).closest('.subject').remove();
        $.fn.checkSubjectChangeForPutRequest();
    });

    $.fn.checkSubjectChangeForPutRequest = () => {
        if ($('#saveCourseBtn').attr('data-request-type') == REQUEST_TYPE_PUT) {
            $('#saveCourseBtn').attr('data-subjectsChecked', 0);
        }
    }

    $('#institutions').on('change', () => {
        if ($('#saveCourseBtn').attr('data-request-type') == REQUEST_TYPE_PUT) {
            $('#saveCourseBtn').attr('data-institutionsChecked', 0);
        }
    });

    $('#saveCourseBtn').on('click', function(){
        var form = $(this).closest('form');
        if($.fn.validateRequest(form)) {
            var institutions = form.find('#institutions');
            if(institutions.val().length){
                var subjects = [];
                var subjectCount = 0;
                var newSubjectIds = [];
                $.each($('#subjectTable').find('tbody').find('tr'), (key, value) => {
                    var subjectId = null;
                    if ($(value).find('select.subjectId').length) {
                        subjectId = $(value).find('select.subjectId').val();
                    } else if ($(value).find('input.subjectId').length) {
                        subjectId = $(value).find('input.subjectId').attr('data-id');
                    }
                    if (subjectId) {
                        newSubjectIds.push(subjectId);
                        subjectCount++;
                        subjects[key] = {
                            "subjectId": subjectId,
                            "order": $(value).find('.subjectOrder').val()
                        }
                    }
                });
                if (subjects.length) {
                    var data = {
                        "name": form.find('#name').val(),
                        "institutionIds": form.find('#institutions').val(),
                        "subjects": subjects
                    }

                    var requestType = $(this).attr('data-request-type');
                    if (requestType === REQUEST_TYPE_PUT) {
                        var existingInstitutions = JSON.parse($('#existingInstitutions').val());
                        var existingSubjects = JSON.parse($('#existingSubjects').val());
                        if ($(this).attr('data-institutionsChecked') == 0) {
                            $.fn.isDeleted(existingInstitutions, data.institutionIds, INSTITUTION);
                            if (deleteStatus === true) {
                                deleteStatus = false;
                                $('#deleteInstitutionModal').modal('show');
                            } else {
                                $('#saveCourseBtn').attr('data-institutionsChecked', 1);
                            }
                        }
                        if ($(this).attr('data-institutionsChecked') == 1 && 
                            $(this).attr('data-subjectsChecked') == 0){
                            $.fn.isDeleted(existingSubjects, newSubjectIds, SUBJECT);
                            if (deleteStatus === true) {
                                deleteStatus = false;
                                $('#deleteSubjectModal').modal('show');
                            } else {
                                $('#saveCourseBtn').attr('data-subjectsChecked', 1)
                            }
                        }
                    }
                    
                    if ($(this).attr('data-institutionsChecked') == 1 
                        && $(this).attr('data-subjectsChecked') == 1) {
                        // Check for duplicate subjects
                        if (Array.from(new Set(newSubjectIds)).length === subjectCount) {
                            $.fn.ajaxCall(requestType, form.attr('action'), 
                                JSON.stringify(data), DATA_TYPE_JSON
                                ).then((successResponse) => {
                                    window.location = successResponse.route;
                                }, (errorResponse) => {
                            });
                        } else {
                           $('#errorModal').find('#errorModalErrors')
                           .html($('#duplicatedSubjectWarning').val());
                           $('#errorModal').modal('show'); 
                        }
                    }
                } else {
                    $('#subjectTable').find('thead').find('tr').css(COLOR, RED_COLOR);
                }
            } else {
                institutions.focus();
            }
        }
    });

    $.fn.isDeleted = (existingData, newData, data) => {
        $.each(existingData, (i, v) => {
            if (data === SUBJECT) {
                v = v.id;
            }
            if($.inArray(v.toString(), newData) == -1){
                deleteStatus = true;
                return false;
            }
        });
    };

    if($('#addSubject').length) {
        $('#addSubject').click();
    }

    $(document).on('click', '#confirmDeleteInstitutionBtn', () => {
        if($(document).find('#confirmDeleteInstitutionInpt').val() === REMOVE) {
            $('#deleteInstitutionModal').modal('hide');
            $('#saveCourseBtn').attr('data-institutionsChecked', 1).click();
        }
    });

    $(document).on('click', '#confirmDeleteSubjectBtn', () => {
        if($(document).find('#confirmDeleteSubjectInpt').val() === REMOVE) {
            $('#deleteSubjectModal').modal('hide');
            $('#saveCourseBtn').attr('data-subjectsChecked', 1).click();
        }
    });

    $(document).on('click', '.deleteCourse', function(){
        $('#deleteCourseModal').find('#confirmDeleteCourseBtn').attr('data-route', $(this).attr('data-route'));
        $('#deleteCourseModal').modal('show');
    });

    $(document).on('click', '#confirmDeleteCourseBtn', function(){
        if($(document).find('#confirmDeleteCourseInpt').val() === DELETE) {
            $('#deleteCourseModal').modal('hide');
            // Send ajax to delete
            $.fn.ajaxCall(REQUEST_TYPE_DELETE, $(this).attr('data-route'), {}, null
                ).then((successResponse) => {
                    window.location = successResponse.route;
                }, (errorResponse) => {
            });
        } else {
            $(document).find('#confirmDeleteCourseInpt').css(BORDER_COLOR, RED_COLOR);
        }
    });

    $(document).on('click', '.copyCourse', function(){
        $.fn.ajaxCall(REQUEST_TYPE_POST, $(this).attr('data-route'), {}, DATA_TYPE_JSON
            ).then((successResponse) => {
                window.location = successResponse.route;
            }, (errorResponse) => {
        });
    });

    $("#deleteCourseModal").on('hidden.bs.modal', function () {
        $(this).find('#confirmDeleteCourseInpt').val('');
    });
});