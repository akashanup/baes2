$(function(){
    var deleteStatus = false;

    if($('#curriculum-table').length) {
        $.fn.yajraDataTable('#curriculum-table', 
            $('#curriculumsRoute').val(), [
                {data: 'id', name: 'subjects.id'},
	            {data: 'name', name: 'subjects.name'},
	            {data: 'count', name: 'count', searchable: false},
	            {data: 'action', orderable: false, searchable: false},
            ]
        );
    }

    $(document).on('click', '.deleteCurriculum', function(){
        $('#deleteCurriculumModal').find('#confirmDeleteCurriculumBtn')
        .attr('data-route', $(this).attr('data-route'));
        $('#deleteCurriculumModal').modal('show');
    });

    $(document).on('click', '#confirmDeleteCurriculumBtn', function(){
        if($(document).find('#confirmDeleteCurriculumInpt').val() === DELETE) {
            $('#deleteCurriculumModal').modal('hide');
            // Send ajax to delete
            $.fn.ajaxCall(REQUEST_TYPE_DELETE, $(this).attr('data-route'), {}, null
                ).then((successResponse) => {
                    window.location = successResponse.route;
                }, (errorResponse) => {
            });
        }
    });

    $('#saveCurriculumBtn').on('click', function(){
        var form = $(this).closest('form');
        var selectedLearningGoalIds = [];
        // var selectedLearningGoalList = $(form).find('#selectedLearningGoals');
        // $.each(selectedLearningGoalList.find('li').not('.disabled'), (i, v) => {
        //     selectedLearningGoalIds.push($(v).attr('data-id'));
        // });

        $('input[name="learningGoalId"]:checked').each(function() {
            
            if(!isNaN(this.value)){
                selectedLearningGoalIds.push(this.value);
            }
        });

        if($.fn.validateRequest(form)) {
            var data = {
                "name": $(form).find('#name').val(),
                "learningGoalIds": selectedLearningGoalIds,
            };
            
            var requestType = $(this).attr('data-request-type');
            if (requestType === REQUEST_TYPE_PUT) {
                var existingLearningGoalIds = JSON.parse($('#existingLearningGoalIds').val());
                if ($(this).attr('data-learningGoalChecked') == 0) {
                    $.fn.isDeleted(existingLearningGoalIds, data.learningGoalIds);
                    if (deleteStatus === true) {
                        deleteStatus = false;
                        $('#removeLearningGoalModal').modal('show');
                    } else {
                        $('#saveCurriculumBtn').attr('data-learningGoalChecked', 1);
                    }
                }
            }

            if ($(this).attr('data-learningGoalChecked') == 1) {
                $.fn.ajaxCall(requestType, form.attr('action'), JSON.stringify(data), DATA_TYPE_JSON)
                .then((successResponse) => {
                   window.location = successResponse.route;
                }, (errorResponse) => {
                });
            }
        }
    });

    $(document).on('click', '#confirmRemoveLearningGoalBtn', () => {
        if($(document).find('#confirmRemoveLearningGoalInpt').val() === REMOVE) {
            $('#removeLearningGoalModal').modal('hide');
            $('#saveCurriculumBtn').attr('data-learningGoalChecked', 1).click();
        }
    });

    $.fn.isDeleted = (existingData, newData) => {
        $.each(existingData, (i, v) => {
            if($.inArray(v.toString(), newData) == -1){
                deleteStatus = true;
                return false;
            }
        });
    };

    if ($('.list-group-sortable-connected').length) {
        $('.list-group-sortable-connected').sortable({
            placeholderClass: 'list-group-item',
            connectWith: '.connected',
            items: ':not(.disabled)'
        });  
    }
});