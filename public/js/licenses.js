$(function(){
    var licenseRoute = `${$('#homeRoute').val()}/licenses`;

    $.fn.initLicenseDatatable = (course, institution) => {
        $('#license-table').DataTable().clear().destroy();
        var route = `${licenseRoute}/courses/${course}/institutions/${institution}`; 
        history.pushState(null, null, route);
        $.fn.yajraDataTable('#license-table', 
            `${route}/indexData`, [
                {data: 'course', name: 'courses.name', className: 'rowCourse'},
                {data: 'institution', name: 'institutions.name', className: 'rowInstitution'},
                {data: 'code', name: 'licenses.code', className: 'rowCode'},
                {data: 'price', name: 'licenses.price', className: 'rowPrice'},
                {data: 'status', name: 'licenses.status', className: 'rowStatus'},
                {data: 'usersCount', name: 'usersCount', searchable: false},
                {data: 'subjectsCount', name: 'subjectsCount', searchable: false},
                {data: 'validity', name: 'licenses.validity', className: 'rowValidity'},
                {data: 'end_date', name: 'licenses.end_date', className: 'rowEndDate'},
                {data: 'role', name: 'licenses.role', className: 'rowRole'},
                {data: 'type', name: 'licenses.type', className: 'rowType'},
                {data: 'action', orderable: false, searchable: false},
            ], () => {
                $('#license-table_filter').before($('#filterLblDiv').html());
                $('#courseDropDown').val(course);
                $('#institutionDropDown').val(institution);
            }
        );
    };

    if($('#license-table').length) {
        $.fn.initLicenseDatatable($('#courseSlug').val(), $('#institutionSlug').val());
    }

    $(document).on('change', '#courseDropDown', function(){
        $.fn.initLicenseDatatable($(this).val(), $('#institutionDropDown').val());
    });

    $(document).on('change', '#institutionDropDown', function(){
        $.fn.initLicenseDatatable($('#courseDropDown').val(), $(this).val());
    });

    $('#createLicenseModalBtn').on('click', () => {
        $.fn.loadInstitutions();
        $.fn.loadValidity($('#createLicenseModal').find('.typeSlct'));
        $('#createLicenseModal').modal('show');       
    });

    $(document).on('change', '#courseSlct', () => {
        $.fn.loadInstitutions();
    });

    $(document).on('change', '.typeSlct', function(){
        $.fn.loadValidity($(this));
    });

    $.fn.loadInstitutions = () => {
        var course = $(document).find('#courseSlct').find(':selected');
        if (course.length) {
            $.fn.ajaxCall(REQUEST_TYPE_GET, 
                course.attr('data-institutionsRoute'), {}, null)
            .then((successResponse) => {
            $('#institutionSlct').html(successResponse);
               $('#createLicenseModal').modal('show');
            });
        }
    };

    $(document).on('click', '.editLicense', function(){
        var editModal = $('#editLicenseModal');
        var license = $(this).closest('tr');
        var type = license.find('.rowType').find('span').attr('data-value');
        var courseName = editModal.find('.courseName');
        var institutionName = editModal.find('.institutionName');
        courseName.val(license.find('.rowCourse').find('a').html());
        courseName.attr('data-courseId', $(this).attr('data-courseId'));
        institutionName.val(license.find('.rowInstitution').html());
        institutionName.attr('data-institutionId', $(this).attr('data-institutionId'));
        editModal.find('.code').val(license.find('.rowCode').html());
        editModal.find('.role').val(license.find('.rowRole').html());
        editModal.find('.price').val(license.find('.rowPrice').html());
        editModal.find('.status').val(license.find('.rowStatus').find('span').attr('data-value'));
        editModal.find('.typeSlct').val(type);
        if (type == MONTHLY) {
            editModal.find('.validityInpt').val('').parent().css('display', 'none');
        } else {
            editModal.find('.validityInpt').val(license.find('.rowValidity').html())
            .parent().css('display', 'block');
        }
        editModal.find('.endDate').val(license.find('.rowEndDate').find('span').html());
        $('#updateLicenseBtn').attr('data-id', $(this).attr('data-id'));
        $('#editLicenseModal').modal('show');
    });

    $(document).on('click', '#updateLicenseBtn', function(){
        var form = $(this).closest('form');
        var data = {
            "code": form.find('.code').val(),
            "course_id": form.find('.courseName').attr('data-courseId'),
            "institution_id": form.find('.institutionName').attr('data-institutionId'),
            "validity": form.find('.validityInpt').val(),
            "price": form.find('.price').val(),
            "end_date": form.find('.endDate').val(),
            "type": form.find('.typeSlct').val(),
            "status": form.find('.status').val(),
            "role": form.find('.role').val(),
        };
        if($.fn.validateRequest(form)) {
            $.fn.ajaxCall(REQUEST_TYPE_PUT, 
                `${licenseRoute}/${$(this).attr('data-id')}`, 
                JSON.stringify(data), DATA_TYPE_JSON)
            .then((successResponse) => {
                $.fn.initLicenseDatatable($('#courseDropDown').val(), 
                    $('#institutionDropDown').val());
                $('#editLicenseModal').modal('hide');
                if (successResponse.response) {
                    $('#successModal').find('#successModalStatus').html(successResponse.message);
                    $('#successModal').modal('show');
                } else {
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
            });
        }
    });

    $(document).on('click', '#storeLicenseBtn', function(){
        var form = $(this).closest('form');
        var data = {
            "code": form.find('.code').val(),
            "course_id": form.find('.courseName').val(),
            "institution_id": form.find('.institutionName').val(),
            "validity": form.find('.validityInpt').val(),
            "price": form.find('.price').val(),
            "end_date": form.find('.endDate').val(),
            "type": form.find('.typeSlct').val(),
            "status": form.find('.status').val(),
            "role": form.find('.role').val(),
        };
        if($.fn.validateRequest(form)) {
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                form.attr('action'), 
                JSON.stringify(data), DATA_TYPE_JSON)
            .then((successResponse) => {
                $.fn.initLicenseDatatable(ALL, ALL);
                $('#createLicenseModal').modal('hide');
                if (successResponse.response) {
                    $('#successModal').find('#successModalStatus').html(successResponse.message);
                    $('#successModal').modal('show');
                } else {
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
            });
        }
    });

    $(document).on('click', '.deleteLicense', function(){
        $('#deleteLicenseModalBtn').attr('data-route', $(this).attr('data-route'));
        $('#deleteLicenseModal').modal('show');
    });

    $(document).on('click', '#deleteLicenseModalBtn', function(){
        if ($('#deleteLicenseModalInpt').val() == DELETE) {
            $.fn.ajaxCall(REQUEST_TYPE_DELETE, 
                $(this).attr('data-route'), {}, DATA_TYPE_JSON)
            .then((successResponse) => {
                $.fn.initLicenseDatatable($(document).find('#courseDropDown').val(), 
                    $(document).find('#institutionDropDown').val());
                $('#deleteLicenseModal').modal('hide');
                $('#successModal').find('#successModalStatus').html(successResponse.message);
                $('#successModal').modal('show');
            });
        } else {
            $(document).find('#deleteLicenseModalInpt').css(BORDER_COLOR, RED_COLOR);
        }
    });

    $("#createLicenseModal").on('hidden.bs.modal', function () {
        var form = $(this).find('form');
        form.find('.code').val('');
        form.find('.courseName').prop('selectedIndex', 0);
        form.find('.validityInpt').val('');
        form.find('.price').val('0');
        form.find('.endDate').val('');
        form.find('.typeSlct').prop('selectedIndex', 0);
        form.find('.status').prop('selectedIndex', 0);
        form.find('.role').prop('selectedIndex', 0);
    });

    $("#editLicenseModal").on('hidden.bs.modal', function () {
        var form = $(this).find('form');
        form.find('.code').val('');
        form.find('.courseName').val('');
        form.find('.institutionName').val('');
        form.find('.validityInpt').val('');
        form.find('.price').val('0');
        form.find('.endDate').val('');
        form.find('.typeSlct').prop('selectedIndex', 0);
        form.find('.status').prop('selectedIndex', 0);
        form.find('.role').prop('selectedIndex', 0);
        $('#updateLicenseBtn').attr('data-id', '');
    });

    $("#deleteLicenseModal").on('hidden.bs.modal', function () {
        $(this).find('#deleteLicenseModalInpt').val('');
    });
});