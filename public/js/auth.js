$(function(){
    $('#phone').mask('+99 9 99 99 99 99');
    $('#profile_picture').css( 'cursor', 'pointer' );
    $('#profile_picture').on('click', function(){
    	$('#file_upload').trigger('click');
    });
    $('#file_upload').on('change', function(){
    	$('#edit_profile_form').submit();
    });
});