$(function(){
    var studentFeedbacksRoute = `${$('#homeRoute').val()}/student-feedbacks`;
    $.fn.initFeedbackDatatable = (course, institution) => {
        $('#feedback-table').DataTable().clear().destroy();
        var route = `${studentFeedbacksRoute}/institutions/${institution}/courses/${course}`; 
        history.pushState(null, null, route);
        $('#feedback-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: `${route}/indexData`,
            columns: [
                {data: 'questionAnswerId', name: 'question_answers.id'},
                {data: 'learningGoalName', name: 'learning_goals.name'},
                {data: 'level', name: 'levels', searchable: false},
                {data: 'created_at', name: 'student_feedbacks.created_at'},
                {data: 'message', name: 'student_feedbacks.message'},
                {data: 'users', name: 'users', searchable: false},
                {data: 'institutionName', name: 'institutions.name'},
                {data: 'courseName', name: 'courses.name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "initComplete": () => {
                $('#user-table_filter').before($('#filterLblDiv').html());
                $('#courseDropDown').val(course);
                $('#institutionDropDown').val(institution);
            },
            pageLength: 100
        });
    };

    if($('#feedback-table').length) {
        $.fn.initFeedbackDatatable($('#courseSlug').val(), $('#institutionSlug').val());
    }

    $(document).on('change', '#courseDropDown', function(){
        $.fn.initFeedbackDatatable($(this).val(), $('#institutionDropDown').val());
    });

    $(document).on('change', '#institutionDropDown', function(){
        $.fn.initFeedbackDatatable($('#courseDropDown').val(), $(this).val());
    });

    $(document).on('click', '.questionAnswerBtn', function(){
        var questionAnswerId = $(this).attr('data-id');
        $.fn.ajaxCall(REQUEST_TYPE_GET, $(this).attr('data-route'), {}, null)
        .then((successResponse) => {
            $('#feedbackQuestionModal').find('.modal-body').html(successResponse);
            $('#feedbackQuestionModal').modal('show');
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            $(document).find('table').addClass('table');

        }, (errorResponse) => {
        });
    });

    $("#feedbackQuestionModal").on('hidden.bs.modal', function () {
        $(this).find('.modal-body').html('');
    });

    $(document).on('click', '.feedbackNote', function(){
        var modal = $('#feedbackNoteModal');
        modal.find('textarea')
        .val($(this).attr('data-note'));
        modal.find('#feedbackSaveNoteBtn').attr('data-route', 
            $(this).attr('data-route'));
        modal.modal('show');
    });

    $("#feedbackNoteModal").on('hidden.bs.modal', function () {
        $(this).find('textarea').val('');
        $(this).find('#feedbackSaveNoteBtn').attr('data-route', '');
    });

    $('#feedbackSaveNoteBtn').on('click', function(){
        var feedbackModal = $('#feedbackNoteModal');
        var data = {
            'note': feedbackModal.find('textarea').val()
        };
        $.fn.ajaxCall(REQUEST_TYPE_PUT, $(this).attr('data-route'), 
            JSON.stringify(data),  DATA_TYPE_JSON).then((successResponse) => {
                feedbackModal.modal('hide');
                $.fn.initFeedbackDatatable($('#courseSlug').val(), 
                    $('#institutionSlug').val());
                $('#successModal').find('#successModalStatus').html(
                    successResponse.message);
                $('#successModal').modal('show');
            }, (errorResponse) => {
        });
    });

    $(document).on('click', '.feedbackStatus', function(){
        var modal = $('#feedbackStatusModal');
        modal.find('#feedbackStatusRoute').val(
            $(this).attr('data-route'));
        var emailType = $(this).attr('data-type');
        if(emailType.length){
            modal.find(`.feedbackMailBtn[data-type='${emailType}']`)
            .addClass('greenBtn');
        }
        modal.modal('show');
    });

    $("#feedbackStatusModal").on('hidden.bs.modal', function () {
        $(this).find('#feedbackStatusRoute').val('');
        $(this).find('.feedbackMailBtn').removeClass('greenBtn');
    });

    $(document).on('click', '.feedbackMailBtn', function () {
        var modal = $("#feedbackStatusModal");
        var data = {
            "response_email_type": $(this).attr('data-type')
        };
        $.fn.ajaxCall(REQUEST_TYPE_PUT, 
            modal.find('#feedbackStatusRoute').val(), 
            JSON.stringify(data),  DATA_TYPE_JSON).then((successResponse) => {
                modal.modal('hide');
                $.fn.initFeedbackDatatable($('#courseSlug').val(), 
                    $('#institutionSlug').val());
                $('#successModal').find('#successModalStatus').html(
                    successResponse.message);
                $('#successModal').modal('show');
            }, (errorResponse) => {
        });
    });
});