$(document).ready(function(){
    $('#add-class').on('click', function(){
        var form = $(this).closest('form');
        var year = '';
        var makeRequest = true;
    	var className = $('#clasName').val();
        if($.fn.validateRequest(form)){
        	console.log(true);
        	$('#yearList').css('border-color', 'lightgrey');
        	$('#customYear').css('border-color', 'lightgrey');
        	year = $('#yearList option:selected').val().trim();
        	if(year === 'others'){
        		year = $('#customYear').val().trim();
        		if(year === ''){
        			$('#customYear').css('border-color', 'red');
        			makeRequest = false;
        		} else{
        			$('#customYear').css('border-color', 'lightgrey');
        			makeRequest = true;
        		}
        	} else if(year === ''){
        		$('#yearList').css('border-color', 'red');
    			makeRequest = false;
        	} else{
        		$('#yearList').css('border-color', 'lightgrey');
        		makeRequest = true;
        	} 
        } else{
        	makeRequest = false;
        }
        if(makeRequest === true){
        	// insert class data
        	var data = {
        		'className' : className,
        		'classYear' : year
        	};
        	$.fn.insertClassData($('#insertClassData').val(), data);
        }
    });

     $.fn.insertClassData = (route, data) => {
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
            if (successResponse.response) {
                // $('#successModal').find('#successModalStatus').html(successResponse.message);
                // $('#successModal').modal('show');
                window.location.reload(true);
            } else {
                $('#errorModalErrors').html(successResponse.message);
                $('#errorModal').modal('show');
            }
            $('.overlay__close').click();
        });
    }

    // Edit student selection
    $('#editStudents').on('click', function(){
        var studentIds = [];
        $('.selectStudent:checked').each(function() {
            if(!isNaN(this.value)){
                studentIds.push(this.value);
            }
        });
        if(studentIds.length > 0){
            $("#edit-year").val("");
            $("#edit-class").val("");
            $("#edit-subclass").val("");
            $("#edit-status").val("");

            $("#edit-student-button").data("key",studentIds);
            if(studentIds.length > 1){
                $("#edit-name-div").addClass('hidden');
                $("#edit-name").val("");
            } else if(studentIds.length === 1){
                $("#edit-name-div").removeClass('hidden');
                $("#edit-name").val($.trim($(".selectStudent:checked")
                    .closest("tr").find(".studentName").text()));
                var selectedYear = $.trim($(".selectStudent:checked")
                    .closest("tr").find(".studentYear").text());
                if(selectedYear){   
                    $("#edit-class").empty();
                    $(document).find('#edit-year').val(parseInt(selectedYear));
                        $.fn.getGradeNames($('#gradesByYearRoute').val()+'/'+
                        parseInt(selectedYear));
                }


            }
            $("input:checkbox[value=edit-name]").prop('checked',true);
            $("input:checkbox[value=edit-subclass]").prop('checked',true);
            $("input:checkbox[value=edit-status]").prop('checked',true);
            $("input:checkbox[value=edit-year]").prop('checked',true);
            var $overlay = $('.overlay[data-overlay = "bewerk_leerling"]');
            $overlay.addClass('active');
            setTimeout(function () {
                $overlay.addClass('animate');
            }, 100);
        }
    });

    // Displaying grade on selection of year
    $(document).on('change','#edit-year',function () {
        $("#edit-class").find("option").remove();
        $("input:checkbox[value=edit-year]").prop('checked',false);
        var year = $('#edit-year option:selected').val();
        if(!isNaN(year)){
            //Make request
            $.fn.getGradeNames($('#gradesByYearRoute').val()+'/'+year);
        }
    });

    $.fn.getGradeNames = (route, data) => {
            $.fn.ajaxCall(REQUEST_TYPE_GET, 
                route, {}, null)
            .then((successResponse) => {
                if(successResponse.response === true){
                    $.each(successResponse.grades, function(item, value){
                        $('<option>').val(item).text(value).appendTo('#edit-class');
                    })
                }
            });
        }

    // Unchecking checkboxes on change for edit overlay
    $(document).on('input',"#edit-name",function () {
        $("input:checkbox[value=edit-name]").prop('checked',false);
    });

    $(document).on('change',"#edit-status",function () {
        $("input:checkbox[value=edit-status]").prop('checked',false);
    });

     // Update Records
        $(document).on('click','#edit-student-button',function () {
            var update_fields = [];
            var update_values = [];
            var updateData = [];
            var studentData = [];
            if($("input:checkbox[value=edit-year]"). prop("checked") == true && 
                $("input:checkbox[value=edit-name]"). prop("checked") == true && 
                $("input:checkbox[value=edit-status]"). prop("checked") == true ){
                console.log("do not update");
            } else{
                 $("input[id='update-check']:not(:checked)").each(function(){
                   update_fields.push($(this).val());
                   update_values.push($.trim($("#"+$(this).val()).val()));
                });
                 update_values.push($.trim($("#edit-class").val()));
               
                if(update_fields.length > 0){
                    console.log("update data");
                    var data = {
                        fields:update_fields,
                        values:update_values,
                        keys  :$("#edit-student-button").data("key")
                    }
                     $.fn.updateStudentData($('#updateStudentsRoute').val(), data);


                } else{
                    console.log("show error");
                }
            }
        });
        $.fn.updateStudentData = (route, data) => {
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                route, JSON.stringify(data), DATA_TYPE_JSON
            ).then((successResponse) => {
                if (successResponse.response) {
                    window.location.reload(true);
                } else {
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
                $('.overlay__close').click();
            });
        }

    // Delete button click model and value set
        $(document).on('click','#deleteClass',function(){
            var year  = $.trim($(this).closest('tr').find('.classYear').text());
            var grade = $.trim($(this).closest('tr').find('.classGrade').attr('slug'));
            var row   = $(this).closest('tr');
            $('#delete_confirm').data("year_value",year);
            $('#delete_confirm').data("grade_value",grade);
            $('#delete_confirm').data("row", row);
            $('#delete_model').modal('show');
        });

    // Request for deleting the class
        $(document).on('click','#delete_confirm',function(){
            var year  = $('#delete_confirm').data('year_value');
            var grade = $('#delete_confirm').data('grade_value');
             var row = $('#delete_confirm').data('row');
            $('#delete_model').modal('hide');
            if(year!="" && grade != "") {
                var data = {
                        classYear:year,
                        className:grade
                    };
                $.fn.deleteClass($('#deleteClassRoute').val(), data, row);
            }
        });

        $.fn.deleteClass = (route, data , row) => {
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                route, JSON.stringify(data), DATA_TYPE_JSON
            ).then((successResponse) => {
                if (successResponse.response) {
                    $('#successModal').find('#successModalStatus').html(successResponse.message);
                    $('#successModal').modal('show');
                    $.fn.classListDatatable($('#classListRoute').val());
                } else {
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
                $('.overlay__close').click();
            });
        }

        // Teacher's overview button
        $('#teachersOverviewButton').on('click', function(){
            var schoolGrade = $('#gradeDropdown').find('li.active').
            find('button').attr('data-slug');
            
            var userSubject = $('#subjectDropdown').find('li.active').
            find('button').attr('data-slug');
            
            var teachersViewDropdown = $('#teachersViewDropdown')
            .find('li.active').find('button').attr('data-slug');

            var teachersViewSwitch = $('#teachersViewSwitch')
            .find('li.active').find('button').attr('data-slug');
            
            var route = '';
            var defaultRoute =  $('#defaultRoute').val();   
            if(teachersViewDropdown === 'scoreView'){
                route = defaultRoute+ '/score-overview/grade/' + schoolGrade 
                + '/subjects/' + userSubject;
            } else if(teachersViewDropdown === 'activityView'){
                route = defaultRoute+ '/activity-overview/grade/' + schoolGrade 
                + '/subjects/' + userSubject;
            } else if(teachersViewDropdown === 'attemptsView'){
                route = defaultRoute+ '/attempts-overview/grade/' + schoolGrade 
                + '/subjects/' + userSubject;
            }
            window.location = route + '/' + teachersViewSwitch;
        });

        $.fn.studentsListDatatable = (route) => {
        $('#students-details-table').DataTable().clear().destroy();
        $.fn.yajraDataTable('#students-details-table', route, [
            {data: 'name', name: 'users.first_name'},
            {data: 'year', name: 'grades.year', orderable: false, searchable: false},
            {data: 'class', name: 'grades.name', orderable: false, searchable: false},
            // {data: 'baes_score', name: 'grades.year', orderable: false, searchable: false},
            {data: 'status', name: 'user_licenses.blocked_by_teacher', orderable: false, searchable: false},
            {data: 'selection', name: 'selection', orderable: false, searchable: false},
        ], function(settings, json){
        });
    }
    $.fn.studentsListDatatable($('#getstudentsListRoute').val());

    // Student list filter
        $('#studentListFilter').on('click', function(){
            var schoolGrade = $('#studentYearFilter').find('li.active').
            find('button').val();
            var schoolYear = $('#studentClassFilter').find('li.active').
            find('button').val();
            var courseId = $('#courseId').val();
            var redirectRoute = $('#defaultRoute').val()
            +'/students-list/'+courseId+'/'+schoolGrade+'/'+schoolYear;
            $.fn.studentsListDatatable(redirectRoute);
        });

        $('#switchStudentEnvironment').on('click', function(){
            $('#studentDashboardBtn').click();
        });

        // Editting the class
        $(document).on('click','#editClass',function () {
             var year  = $.trim($(this).closest('tr').find('.classYear').text());
            var grade = $.trim($(this).closest('tr').find('.classGrade').text());
            $('#editClassButton').data("year_value",year);
            $('#editClassButton').data("grade_value",grade);
            $('#editClassButton').attr("class-id", $(this).attr('data-id'));
            $('#editClassYear').val(year);
            $('#editClassName').val(grade);
        });

        $('#editClassButton').on('click', function(){
            var form = $(this).closest('form');
            var year = $('#editClassYear option:selected').val();
            var className = $('#editClassName').val();
            if($.fn.validateRequest(form)){
                var data = {
                    classYear : year,
                    className: className,
                    id : $('#editClassButton').attr("class-id")
                };
                $.fn.updateClass($('#updateClassRoute').val(), data);
            }

        });

        $.fn.updateClass = (route, data) => {
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                route, JSON.stringify(data), DATA_TYPE_JSON
            ).then((successResponse) => {
                if (successResponse.response) {
                   window.location.reload(true);
                } else {
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
                $('.overlay__close').click();
            });
        }

        $.fn.classListDatatable = (route) => {
            $('#class-details-table').DataTable().clear().destroy();
            $.fn.yajraDataTable('#class-details-table', route, [
                {data: 'year', name: 'grades.year'},
                {data: 'name', name: 'grades.name'},
                {data: 'students', name: 'grades.name', orderable: false, searchable: false},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
            ]);
        }
        $.fn.classListDatatable($('#classListRoute').val());

        // Get class details by Year
        $('#getYearClasses').click( function() {
            var route = $('#classListRoute').val();
             var year = $('#classYearDropdown').find('li.active').
             find('button').val();
             if(year !== ALL){
                route = route + '/' + year;
             }   
             $.fn.classListDatatable(route);

        });

        // Update PracticeTestFolder 

        $(document).on('click', '.edit-action i', function(){
            var folderName = $(this).parent('span').parent().
            siblings('a').children().children('.folder-name').text();

            var folderDescription = $(this).parent('span').parent().
            siblings('a').children().children('.folder-description').text();

            var updateRoute = $(this).parent('span').attr('update-route');

            $('#editFolderModal #editFolderName').val(folderName);
            $('#editFolderModal #editFolderDescription').val(folderDescription);
            $('#updateFolderButton').attr('update-route', updateRoute);
            $('#editFolderModal').modal('show');
        });

        $(document).on('click', '#updateFolderButton', function(){
            var form = $(this).closest('form');
            if($.fn.validateRequest(form)){
                var data = {
                    folder_name : $('#editFolderName').val(),
                    folder_description : $('#editFolderDescription').val(),
                };
                $.fn.updateFolderDetails(
                    $('#updateFolderButton').attr('update-route'), data);
            }

        });

        $.fn.updateFolderDetails = (route, data) => {
            $('#editFolderModal').modal('hide');
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                route, JSON.stringify(data), DATA_TYPE_JSON
            ).then((successResponse) => {
                if (successResponse === SUCCESS) {
                   window.location.reload(true);
                } 
            });
        }

        //Delete PracticeTestFolder

        $(document).on('click', '.delete-action i', function(){
            
            var deleteRoute = $(this).parent('span').attr('delete-route');

            $('#deleteFolder #confirmFolderDelete').attr('delete-route', deleteRoute);
            $('#deleteFolder').modal('show');
        });

        $(document).on('click', '#confirmFolderDelete', function(){
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                 $('#confirmFolderDelete').attr('delete-route'), DATA_TYPE_JSON
            ).then((successResponse) => {   
                if (successResponse.status === SUCCESS) {
                   window.location.reload(true);
                } else{
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
            });

        });
});
