$(function(){
var unique = NO;
	$.fn.curriculumAjaxRequest = (route, slug) => {
        
		$.fn.ajaxCall(REQUEST_TYPE_GET, 
		    $('#curriculumData').val(), {}, null
		).then((successResponse) => {
            
		    $('#lg_management_table_filter').before(successResponse);
		    $('#curriculumDropDown option[data-route="'+route+'"]').attr("selected","selected");
		    var slug = $("#curriculumDropDown option:selected").val();
		    if(slug){
		        history.pushState(null, null, $('#learningCurriculumRoute').val()+'/'+slug);
		    } else {
		        history.pushState(null, null, $('#CurrentRoute').val());
		    }
		}, (errorResponse) => {
		});
	}

	$.fn.learningGoalDatatable = (route, slug) => {
		$('#lg_management_table').DataTable().clear().destroy();
		$.fn.yajraDataTable('#lg_management_table', route, [
            {data: 'id', name: 'learning_goals.id'},
            {data: 'name', name: 'learning_goals.name'},
            {data: 'theory', name: 'learning_goals.theory', orderable: false, searchable: false},
            {data: 'mcq', name: 'learning_goals.mcq', orderable: false, searchable: false},
            {data: 'direct', name: 'direct', orderable: false, searchable: false}
        ], function(settings, json){
            $.fn.curriculumAjaxRequest(route, slug);
        });
    }

    $.fn.templateDatatable = (route, slug) => {
        $.fn.yajraDataTable('#template-table',route, [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'learning_goals.name' },
            { data: 'template', name: 'question_templates.template' },
            { data: 'complexity', name: 'question_templates.complexity' },
            { data: 'order', name: 'question_templates.order' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        function (settings, json){
            $.fn.templateAjaxRequest(route, slug);
        })
    }

    $.fn.allQuestionsDatatable = (route) => {
           $('#question-table').DataTable().clear().destroy();
        $.fn.yajraDataTable('#question-table',route, [
            {data: 'id', name: 'question_answers.id', orderable: false,},
            {data: 'name', name: 'learning_goals.name'},
            {data: 'complexity', name: 'question_templates.complexity' , searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        function (settings, json){
            
            //$.fn.templateAjaxRequest(route, slug);
        })
    }

    // Ajax request for learning goals data table
    $.fn.learningGoalDatatable($('#learningGoalRoute').val(), null);
    $(document).on('change', '#curriculumDropDown', function(){
        var slug = $("#curriculumDropDown option:selected").val();
        var route = $("#curriculumDropDown option:selected").attr('data-route');
        $.fn.learningGoalDatatable(route, slug);
    }); 

    $(document).on('change', '#templateCurriculumDropDown', function(){
        var slug = $("#templateCurriculumDropDown option:selected").val();
        if(slug !== '0'){
            var route = $('#templateTableRoute').val()+'/'+slug;
        }else{
            var route = $('#templateTableRoute').val();
        }
        $('#template-table').DataTable().clear().destroy();
        $.fn.templateDatatable(route, slug);
    });

    $(document).on('change', '#templateLearningDropDown', function(){
        var route = $("#templateLearningDropDown option:selected").val();
        if(route === '0'){
            route = $('#templateTableRoute').val();
        }
        $('#template-table').DataTable().clear().destroy();
        $.fn.templateDatatable(route, null);
    });

    $.fn.templateAjaxRequest = (route, slug) => {
        $.fn.ajaxCall(REQUEST_TYPE_GET, 
            $('#TemplateCurriculumData').val(), {}, null
        ).then((successResponse) => {
            $('#template-table_filter').before(successResponse);
            
            if(slug){
                $('#templateCurriculumDropDown option[value="'+slug+'"]').attr("selected","selected");
            }
            if(route){
                $('#templateLearningDropDown option[value="'+route+'"]').attr("selected","selected");
            }
        }, (errorResponse) => {
            // handle error if error in AJAX request
        });
    }

 	// Ajax request for Template data table
	$.fn.templateDatatable(route=$('#templateTableRoute').val(),  null);

    // upload variable set
    $(document).on('click', '.uploadVariableSet', function(){
        var fileUpload = $('#fileUpload');
        fileUpload.parent('form').attr('action', $(this).attr('data-route'));
        fileUpload.click();
    });

    // upload variable set
    $(document).on('click', '#uploadTheoryPdf', function(){
        var fileUpload = $('#uploadTheoryPdfForm');
        fileUpload.parent('form').attr('action', $(this).attr('data-route'));
        fileUpload.click();
    });

    $('#fileUpload').on('change', function () {
        $(this).parent('form').submit();
    });
    $('#uploadTheoryPdfForm').on('change', function () {
        $(this).parent('form').submit();
    });

    // Preview MCQ template in modal
    $("#view_mcq").click(function(){
        $.blockUI({ message: '<h3>{{__("languages.loading")}} <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>',baseZ: 1005 });
        $("#mcqquestion").html($.trim(CKEDITOR.instances.question.getData()));
        $("div #option1").html($.trim(CKEDITOR.instances.option1.getData()))
        $("div #option2").html($.trim(CKEDITOR.instances.option2.getData()))
        $("div #option3").html($.trim(CKEDITOR.instances.option3.getData()))
        $("div #option4").html($.trim(CKEDITOR.instances.option4.getData()))
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        MathJax.Hub.Queue(function(){
            replacePercentage('question');
            replacePercentage('feedback2');
            replacePercentage('feedback3');
            replacePercentage('feedback4');
        });
        $.unblockUI();
    });

    // Preview Direct template in modal
    $("#view_direct").click(function(){
        var data = {
                id       : $("input[name='questionTemplateId']").val(),
                question : $.trim(CKEDITOR.instances.old_question.getData()),
                feedback2: $.trim(CKEDITOR.instances.old_feedback2.getData()),
                feedback3: $.trim(CKEDITOR.instances.old_feedback3.getData()),
                feedback4: $.trim(CKEDITOR.instances.old_feedback4.getData()),
            };

        $.fn.getQuestionTemplateRequest($('#previewDirectTemplate').val(), data);
    });

    function replacePercentage(id)
    {
        var val = $("#"+id).html();
        val = val.replace(/\\%/g, '%');
        $("#"+id).html();
        $("#"+id).html(val);
    }

    $.fn.getQuestionTemplateRequest = (route, data) => {
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
            $.blockUI({ message: '<h3>{{__("languages.loading")}} <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>',baseZ: 1005 });
            $("#question").html(successResponse.question);
            $("#feedback2").html(successResponse.feedback2);
            $("#feedback3").html(successResponse.feedback3);
            $("#feedback4").html(successResponse.feedback4);
            $('.directTemplatePreview table').addClass('table');
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            MathJax.Hub.Queue(function(){
                replacePercentage('question');
                replacePercentage('feedback2');
                replacePercentage('feedback3');
                replacePercentage('feedback4');
            });
            $.unblockUI();
            if(successResponse === SUCCESS){
                 window.location.reload(true);
            } else{
                // $("#some_error").removeClass("hidden");
                return "Error in Data";
            }
        });
    } 


    $.fn.allQuestionsDatatable(route=$('#questionsRoute').val()+'/'+unique);

    //Save practice test template
    $.fn.savePracticeTestTemplate = (route, data) => {
        
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
            if(successResponse.status === SUCCESS){
                 window.location.href = successResponse.route;
            } else{
                $("#some_error").removeClass("hidden");
                return "Error in Data";
            }
        });
    }
    
    $.fn.practiceTestTemplateDataTable = (route) => {
        
        $.fn.yajraDataTable('#practice-test-tempate-table',route, [
            {data: 'name', name: 'practice_test_templates.name'}
        ],
        function (settings, json){
            // $.fn.templateAjaxRequest(route, slug);
        })
    }

    $.fn.practiceTestTemplateDataTable($('#listpracticeTestRoute').val());

    $.fn.activePracticeTestDatatable = (route) => {
        $.fn.yajraDataTable('#active-practice-test-table',route, [
            {data: 'name', name: 'practice_test_details.name'}
        ],
        function (settings, json){
            
            // $.fn.templateAjaxRequest(route, slug);
        })
    }

    $.fn.activePracticeTestDatatable($('#listActiveTestRoute').val());


    // delete practice making key block
    // Deleting a row on click
        $(document).on('click','#deletePracticeBlock',function () {
            $(this).closest("tr").remove();
            var table_length  = $("table > tbody").children().length;
            var delete_serial = parseInt($(this).closest("tr").find('td:nth-child(1)').html())+1;
            for(var index = delete_serial;index <= table_length; index++) {
                $('table tr:nth-child('+ index +') td:nth-child(1)').html(index-1);
            }
        });

        // Add practice making key block
         // Adding new row to table
        $("#addPracticeBlock").click(function () {
           $("#practice-test-table.table").append($("#hidden_row").clone());
           $("#practice-test-table.table tr:last").removeAttr("id");
           $("#practice-test-table.table tr:last").removeClass("hidden");
           var th = $("#practice-test-table.table tr:last");
           var num = parseInt($(th).prev().find('td:first').html());
           $("#practice-test-table.table tr:last td:first").html(num+1);
        });

         $(document).on('change',".test-template-subject" ,function (event,val) {
            var field= $(this);
            $(this).closest("tr").find("select[name='goal_selected']").empty();
            if(field.val() != "") {
               //ajax
               var data = {
                    'subject_id' : $(this).val(),
                };
               console.log("ajax req");
               $.fn.subjectLearningGoal($('#subjectLearningGoal').val(), data, val, field);
            }
        });     

        // Saving practice test template
        $(document).on('click','#insert_practice_template',function () {
            
            var template = {};
            var practice_test_name = $.trim($("#testTemplateName").val());
            var folderId = $('#testTemplateFolder option:selected').val();
            if(practice_test_name != "" && !isNaN(folderId)) {
                $("#practice-test-table.table > tbody").find('tr').each(function (i, el) {
                    
                    if(i != 0 && !(isNaN(parseInt($(this).find(".test-template-subject").val())))
                        && !(isNaN(parseInt($(this).find(".subjectLearningGoal").val())))){
                        
                        template[i] = {};
                        template[i]['subject_id'] = $(this).find(".test-template-subject").val();
                        template[i]['goal_id']    = $(this).find(".subjectLearningGoal").val();
                        template[i]['level']      = $(this).find(".complexityLevel").val();
                        var checkbox = $(this).find("input[name='random_selected']");
                        if(checkbox.prop("checked")) {
                            template[i]['random'] = YES;
                        } else {
                            template[i]['random'] = NO;
                        }
                    }
                });
                if(Object.keys(template).length>0) {
                    console.log(template);
                    var templateId = $("#testTemplateId").val();
                    if(!isNaN(templateId)){
                        var update = YES;
                    } else{
                        var update = NO;
                        templateId = '';
                    }
                    // Ajax request
                    var data = {
                        'template' : template,
                        'test_name' : practice_test_name,
                        'number_of_template'  : Object.keys(template).length,
                        'update' : update,
                        'folderId' : folderId,
                        'templateId' : templateId 

                    };
                    $.fn.savePracticeTestTemplate($('#saveTestTemplate').val(), data);
                } 
            } 
        });

        // Verify date and time to activate practicet test.
        $.fn.verifyDateTime = function(start_dateTime, end_dateTime){
            
            start_dateTime  = start_dateTime.split(' ');
            end_dateTime    = end_dateTime.split(' ');

            var start_date  = new Date(start_dateTime[0].substring(0,4), start_dateTime[0].substring(5,7) -1, start_dateTime[0].substring(8), start_dateTime[1].substring(0,2), start_dateTime[1].substring(3), 0, 0);
            var end_date   = new Date(end_dateTime[0].substring(0,4), end_dateTime[0].substring(5,7) -1, end_dateTime[0].substring(8), end_dateTime[1].substring(0,2), end_dateTime[1].substring(3), 0, 0);
            if(end_date > new Date() && end_date > start_date) {
                return true;
            } else {
                return false;
            }
        }

        $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        $(document).on('click','#current_date',function(){
            $("#start_date").val($.fn.getFormattedCurrentTime());
        });
        $.fn.getFormattedCurrentTime = function(endDate=null){
            
            var date = new Date();
            var nearest_minutes_to_five = (date.getMinutes() % 5) > 0 ? date.getMinutes()- 
            (date.getMinutes() % 5) : date.getMinutes();
            return  (endDate?(date.getFullYear() + 10):date.getFullYear())+'-'+
                    (date.getMonth() < 9 ? "0" + (date.getMonth()+1) : (date.getMonth()+1))+
                    '-'+(date.getDate()  < 10 ? "0" + date.getDate()  : date.getDate())+
                    " "+(date.getHours() < 10 ?'0'+date.getHours() : date.getHours())+
                    ":"+(nearest_minutes_to_five < 10 ? "0" + nearest_minutes_to_five : 
                        nearest_minutes_to_five);
        };

        $(document).on('click', '#createFolderButton', function(){
            $('#createFolderModal').modal('show');
        });

        $.fn.calculateGMT = function(){
            var current_date    = new Date().toString();
            var start           = current_date.indexOf("GMT");
            var end             = start +8;
            var gmTime          = current_date.substring(start,end);
            var split_timezone  = gmTime.split("+");
            var flag            = 0;
            if(split_timezone[0].indexOf("-") != -1) {
                split_timezone  = gmTime.split("-");
                flag            = 1;
            }
            var time_array              = split_timezone[1].split("");
            var coverted_hours_to_min   = (parseInt(time_array[0])*10 + parseInt(time_array[1]))*60;
            var gmt                     = (parseInt(time_array[2])*10 + parseInt(time_array[3]))+coverted_hours_to_min;
            if(flag) {
                gmt = (-gmt);
            }
            return gmt;   
        };

        $(document).on('click','.activateTest',function () {
            
           $("#save_test").attr('template-id', $(this).data('id'));
            $("#start_date").val("");
            $("#end_date").val("");
        });

        // Create Test
        $(document).on('click',"#save_test",function (event) {
            // event.preventDefault();
            var verifiedDateTime = $.fn.verifyDateTime(
                    $("#start_date").val(), $("#end_date").val());
            if(verifiedDateTime){         
            var grade = $("#grade option:selected").val();
            var start =  $("#start_date").val();
            var end = $("#end_date").val();
            var time = $("#time").val();
            var feedback = $("#feedback_type option:selected").val();
            var testTemplateId =  $("#save_test").attr('template-id');
            var availability = $("#availability option:selected").val();
            var gmt = $.fn.calculateGMT();
            var licenseId = $('#licenseId').val();
            var folderId = $('#folderId').val();
            if(grade && start && end && time && 
                feedback && testTemplateId && availability && gmt && licenseId && folderId){
                // make ajax request
            var data =  {
                        grade: grade,
                        start: start,
                        end: end,
                        time:time,
                        feedback:feedback,
                        testTemplateId:testTemplateId,
                        availability:availability,
                        gmt:gmt,
                        folderId:folderId,
                        licenseId:licenseId


                    };
                $.fn.saveTestData($('#saveTestDetailsRoute').val(), data);
            } else{
                console.log("Insufficient Data");
            }
        } 
        });

    //Save Test details and question
    $.fn.saveTestData = (route, data) => {
        
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
            if(successResponse === SUCCESS){
                 window.location.reload(true);
            } else{
                $('#errorModalErrors').html(successResponse.message);
                $('#errorModal').modal('show');
            }
        });
    }

    // Ending test
    $(document).on('click','.test_finished',function(event){
        
        event.preventDefault();
        var data = {'id' : $(this).data('id') };
        $.fn.updateTestStatus($('#changeTestStatusRoute').val(), data);
    });

    $.fn.updateTestStatus = (route, data) => {
        
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
            
            if(successResponse === SUCCESS){
                 window.location.reload(true);
            } else{
                // $("#some_error").removeClass("hidden");
                return "Error in Data";
            }
        });
    }

    $(document).on('click','.deleteTestTemplate',function(event){
        
        event.preventDefault();
        $('[name="testTemplateId"]').val($(this).data('id'));
        $('#deletePracticeTestTemplate').modal('show');
    });

    $(document).on('click','#confirmTemplateDelete',function(event){
        
        event.preventDefault();
        if(!isNaN($('[name="testTemplateId"]').val())){
            console.log("submit form");
            $('#deleteTemplateForm').submit();
        } else{
            console.log("Errro");
        }
    });

    $(document).on('click', '.copyTestTemplate', function(){
        
        $.fn.ajaxCall(REQUEST_TYPE_POST, $(this).attr('data-route'), {}, DATA_TYPE_JSON
            ).then((successResponse) => {
                 if(successResponse === SUCCESS){
                    window.location.reload(true);
                } else{
                    $("#some_error").removeClass("hidden");
                    return "Error in Data";
                }
            });
    });

    $.fn.finishedPracticeTest = (route) => {
        $('#finishedTestTable').DataTable().clear().destroy();
        $.fn.yajraDataTable('#finishedTestTable', route, [
            {data: 'name', name: 'practice_test_details.name'},
            {data: 'end_date', name: 'practice_test_details.end_date'},
            {data: 'year', name: 'grades.year', searchable: false},
            {data: 'grade', name: 'grades.name'},
            {data: 'student_attempted', name: 'practice_test_details.student_attempted', orderable: false, searchable: false},
            {data: 'avg_score', name: 'practice_test_details.avg_score', orderable: false, searchable: false}
        ], function(settings, json){
            // $.fn.curriculumAjaxRequest(route, slug);
        });
    }
    $.fn.finishedPracticeTest($('#finishedTestTableRoute').val());
    
    $(document).on('click','#apply-button',function(){
        
        var year   = $('#edit-filter-year li.active button').val();
        var grade  = $('#edit-filter-class li.active button').val();
        if(year === '0'){
            year = 0;
        } 
        if(grade === '0'){
            grade = 0;
        } 
        year = '/'+year + '/';
        grade = grade + '/';
        var route =$('#finishedTestTableRoute').val()+year+grade;
        $.fn.finishedPracticeTest(route);
    });

    $(document).on('click','[name="testTemplateCheck"]',function(){
        if(!isNaN($('[name="testTemplateCheck"]:checked').val())){
            $('#changeFolderSection').show();
        } else{
            $('#changeFolderSection').hide();
        }
    });

    $(document).on('click','#changeFolderButton',function(){
        
        var templateIdArray = [];
        var folderId = $('#testTemplateFolder option:selected').val();
        $('input[name="testTemplateCheck"]:checked').each(function() {
            
            if(!isNaN(this.value)){
                templateIdArray.push(this.value);
            }
        });
        console.log(templateIdArray);
        if(templateIdArray.length && !isNaN(folderId)){
            console.log("ajax request");
            var data = {
                'templateIdArray' : templateIdArray,
                'folderId' : folderId
            };
            $.fn.updateTestTemplateFolder($('#updateTestTemplateRoute').val(), data);

        }
    });

    $.fn.updateTestTemplateFolder = (route, data) => {
        
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
               
            if(successResponse === SUCCESS){
                 window.location.reload(true);
            } else{
                // $("#some_error").removeClass("hidden");
                return "Error in Data";
            }
        });
    } 

    $(document).on('change',"#questionLearningGoal" ,function (event) {
        var learningGoalId = $('#questionLearningGoal option:selected').val();
        if(!isNaN(learningGoalId)){
            var route = $('#questionsRoute').val() + '/' + unique;
            if(learningGoalId !== '0'){
                var route = route + '/'+learningGoalId;
            }
            $.fn.allQuestionsDatatable(route);
        }        
    });

    $('.form.createQuestionTemplateForm :input').on('change invalid', function() {
        var textfield = $(this).get(0);
        // 'setCustomValidity not only sets the message, but also marks
        // the field as invalid. In order to see whether the field really is
        // invalid, we have to remove the message first
        var requiredField = $('#requiredFieldValidation').val();
        textfield.setCustomValidity('');
        if (!textfield.validity.valid) {
          textfield.setCustomValidity(requiredField);
        }
    });

    // View question answer
    $(document).on('click','.viewQuestionBtn',function(){
        var questionAnswerId = $(this).attr('data-id');
        if(!isNaN(questionAnswerId)){
            var data = {
                id : questionAnswerId
            };
            console.log("Ajax request"); 
        }
        $.fn.getQuestionAnswer($('#viewQuestionRoute').val(), data);
    });
    
    $.fn.customUnblockUi = function (data) {
        $.unblockUI();
        $.blockUI({ message: data.error, timeout: 2000,baseZ: 1005 });
    };

    $.fn.getQuestionAnswer = (route, data) => {
        
        $.blockUI({ message: '<h3><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>'
            ,baseZ: 1005 });
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((data) => {
            $(document).find('.directQuestionDiv').html('');
            if (data.question) {
                $(document).find('#questionDiv').html(data.question);
                if(data.options) {
                    var adder = 97;
                    var html  = '';
                    $.each(JSON.parse(data.options), function(key, value){
                        html += '<i class="icon-' + 
                        String.fromCharCode(adder++) +
                         ' form__icon"></i>' +
                            value   +'<br>';
                    });
                    html += '<br><div>'+
                                '<h4>'+$("#correctAnswer").val()+':</h4>'+
                                data.answer+
                            '</div>';
                    $(document).find('#optionDiv').html(html);
                } else {
                    $(document).find('#optionDiv').html(data.hints.join(''));
                }
                $.unblockUI();
            } else {
                $.fn.customUnblockUi(data);
            }
            $('.directQuestionDiv table').addClass('table');
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            MathJax.Hub.Queue(
                function () {
                    $.fn.replacePercentageFn();
                }
            );
        });
    } 

    $(document).on('click','.deleteLearningGoal',function(){
        var route = $(this).attr('data-route');
        if(route){
            $('#deleteLearningGoalForm').attr('action', route);
        }
    });

    $('#uniqueQuestions').on('click', function(){
            unique = $(this).attr('data-val');
            $(this).css('display', 'none');
            $('#allQuestions').css('display', 'block');
            $('#questionLearningGoal').change();
    });

    $('#allQuestions').on('click', function(){
        unique = $(this).attr('data-val');
        $(this).css('display', 'none');
        $('#uniqueQuestions').css('display', 'block');
        $('#questionLearningGoal').change();
    });
    
     $(document).on('click','#class_overview_btn',function(){
        var dataRoute = $(this).siblings().children('#gradeDropdown')
        .children('li.active').children('button').attr('data-route');
        window.location = dataRoute;        
    });    

     $('.showOnHover').hover(function(e){
        
        e.preventDefault();

        var target = $(this).data('targetid');

        $('#'+target).show();
        $('#'+target).text($(this).data('title'));

        $(this).mouseout(function(){
            $('#'+target).hide();
        });

    });

     $('#folderGradeBtn').on('click', function() {
            var schoolGrade = $('#folderDropdownGrade').find('ul > li.active').find('button').attr('data-slug');
            window.location = $('#folderTestRoute').val()+'/'+schoolGrade;
        });

    $('#uploadFileBtn').on('click', function(){
        $(this).css('display', 'none');
        $('#uploadFileForm').css('display', 'block');
    });

    $("#variableSetTable td:not(:last-child) > a").each(function(){
        var value = $.trim($(this).text());
        value = value.replace(/([\\\%])+/g,"%");
        $(this).text(value);
    });
    $("#variableSetForm input").each(function(){
        var value = $(this).val();
        value = value.replace(/([\\\%])+/g,"%");
        $(this).val(value);
    });

    // Update Variable Set 
    $("#updateVariableSet").on('click', function(event){
        var data = {};
        event.preventDefault();
        var errorResponse = ValidateForm();
        if(errorResponse){
         return false;
        }
        $("#variableSetForm input").each(function(){
            var name = $(this).attr('name');
            var value = $(this).val();
           data[name] = value;
        });
            $.fn.updateTemplateVariableData($('#variableSetForm').attr('action'), data);
    });
    function ValidateForm(){
        var displayError = false;
        $('.error_div').text('');
        $("#variableSetForm input").each(function(){
            if($(this).attr('required')){
                if(!$(this).val()){
                    displayError  = true;
                    $(this).siblings('.error_div').text("Please insert value");
                }
            }
        });
        return displayError;
    }

    $.fn.updateTemplateVariableData = (route, data) => {
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            route, JSON.stringify(data), DATA_TYPE_JSON
        ).then((successResponse) => {
            if(successResponse.status === SUCCESS){
                 window.location = successResponse.route;
            } else{
                return "Error in Data";
            }
        });
    }

    // Year dropdown select
    $(document).on('change','#yearList',function () {
            $('#customYear').val('');
            if($.trim($('#yearList option:selected').val()) == 'others') {
                $('#customYearField').removeClass('hidden');
            } else {
                $('#customYearField').addClass('hidden');
                $('#customYear').val($.trim($('#yearList option:selected').val()));
            }
        });
// yearList
});


