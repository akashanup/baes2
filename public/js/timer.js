$(function(){
	var practiceTestSlug = $('#practiceTestSlug').val();
	var minutesTxt = $('#minutesTxt').val();
	var timeLeft = $('#timeLeft').val();
	// Setting initial time limit
   	if(docCookies.hasItem(`time_left${practiceTestSlug}`)) {
        localStorage.setItem(`deadline_time${practiceTestSlug}`, docCookies.getItem(`time_left${practiceTestSlug}`));
   	}
    docCookies.removeItem(`time_left${practiceTestSlug}`)

    // Update the count down every 1 second
    var x = setInterval(function() {
        // Find the distance between now an the count down date
        var distance = localStorage.getItem(`deadline_time${practiceTestSlug}`)-1;
        localStorage.setItem(`deadline_time${practiceTestSlug}`, distance);
        // Time calculations for minutes and seconds
        var minutes = Math.floor((distance % (60 * 60)) / 60);
        var seconds = Math.floor(distance % 60);
        minutes     = minutes < 10 ? "0"+ minutes : minutes;
        seconds     = seconds < 10 ? "0"+ seconds : seconds;
        // Display the result in the element with id="timer"
        $("#timer").html(minutes + ":" + seconds + " " + minutesTxt);
        // If the count down is finished, trigger submitBtn
        if (distance <= 0) {
            localStorage.removeItem(`deadline_time${practiceTestSlug}`);
            $.fn.submitPracticeTest($('#confirmSubmitPracticeTestBtn').attr('data-route'));
            $("#timer").html("00:00 "+ minutesTxt);
        }
    }, 1000);
});