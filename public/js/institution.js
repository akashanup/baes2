$(function(){
    var institutionRoute = `${$('#homeRoute').val()}/institutions`;
    var licenseRoute = `${$('#homeRoute').val()}/licenses`;

    $.fn.initInstitutionDatatable = (course, institution) => {
        $('#institution-table').DataTable().clear().destroy();
        var route = `${institutionRoute}/${institution}/courses/${course}`; 
        history.pushState(null, null, route);
        $.fn.yajraDataTable('#institution-table', 
            `${route}/indexData`, [
                {data: 'institution', name: 'institutions.name', className: 'rowInstitution'},
                {data: 'image', name: 'institutions.image', orderable: false, searchable: false},
                {data: 'course', name: 'courses.name', className: 'rowCourse'},
                {data: 'users', name: 'users', searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ], () => {
                $('#institution-table_filter').before($('#filterLblDiv').html());
                $('#courseDropDown').val(course);
                $('#institutionDropDown').val(institution);
            }
        ); 
    };

    if($('#institution-table').length) {
        $.fn.initInstitutionDatatable($('#courseSlug').val(), $('#institutionSlug').val());
    }

    $(document).on('change', '#courseDropDown', function(){
        $.fn.initInstitutionDatatable($(this).val(), $('#institutionDropDown').val());
    });

    $(document).on('change', '#institutionDropDown', function(){
        $.fn.initInstitutionDatatable($('#courseDropDown').val(), $(this).val());
    });

    $(document).on('click', '.editInstitution', function(){
        var modal = $('#editInstitutionModal');
        var institution = $(this).closest('tr');
        modal.find('input[name="name"]').val($.trim(institution.find('.rowInstitution').find('a').html()));
        modal.find('select').val($(this).attr('data-type'));
        modal.find('form').attr('action', $(this).attr('data-updateRoute'));
        modal.modal('show');
    });

    $("#editInstitutionModal").on('hidden.bs.modal', function () {
        var modal = $(this);
        modal.find('input[name="name"]').val('');
        modal.find('select').prop('selectedIndex', 0);;
        modal.find('input[name="image"]').val('');
        modal.find('form').attr('action', '');
    });

    $(document).on('click', '.deleteInstitution', function(){
        $('#deleteInstitutionModalBtn').attr('data-route', $(this).attr('data-deleteRoute'));
        $('#deleteInstitutionModal').modal('show');
    });

    $(document).on('click', '#deleteInstitutionModalBtn', function(){
        if ($('#deleteInstitutionModalInpt').val() == DELETE) {
            $.fn.ajaxCall(REQUEST_TYPE_DELETE, 
                $(this).attr('data-route'), {}, DATA_TYPE_JSON)
            .then((successResponse) => {
                $.fn.initInstitutionDatatable($(document).find('#courseDropDown').val(), 
                    ALL);
                $('#deleteInstitutionModal').modal('hide');
                if (successResponse.response) {
                    $('#successModal').find('#successModalStatus').html(successResponse.message);
                    $('#successModal').modal('show');
                } else {
                    $('#errorModalErrors').html(successResponse.message);
                    $('#errorModal').modal('show');
                }
            });
        } else {
            $(document).find('#deleteInstitutionModalInpt').css(BORDER_COLOR, RED_COLOR);
        }
    });



    $("#deleteInstitutionModal").on('hidden.bs.modal', function () {
        $(this).find('#deleteInstitutionModalInpt').val('');
    });

    $(document).on('click', '.createLicense', function(){
        var institution = $(this).closest('tr');
        var modal = $('#createLicenseModal');
        modal.find('#institution').val($.trim(institution.find('.rowInstitution').find('a').html()));
        modal.find('#course').val(institution.find('.rowCourse').html());
        $.fn.loadValidity(modal.find('.typeSlct'));
        $('#saveLicenseBtn').attr('data-courseId', $(this).attr('data-courseId'));
        $('#saveLicenseBtn').attr('data-institutionId', $(this).attr('data-institutionId'));
        $('#saveLicenseBtn').attr('data-courseSlug', $(this).attr('data-courseSlug'));
        $('#saveLicenseBtn').attr('data-institutionSlug', $(this).attr('data-institutionSlug'));
        modal.modal('show');
    });

    $(document).on('change', '.typeSlct', function(){
        $.fn.loadValidity($(this));
    });

    $("#createLicenseModal").on('hidden.bs.modal', function () {
        var form = $(this).find('form');
        form.find('#code').val('');
        form.find('#course').val('');
        form.find('#institution').val('');
        form.find('.validityInpt').val('');
        form.find('#price').val('0');
        form.find('#endDate').val('');
        form.find('.typeSlct').prop('selectedIndex', 0);
        form.find('#status').prop('selectedIndex', 0);
        form.find('#role').prop('selectedIndex', 0);
        $('#saveLicenseBtn').attr('data-courseId', '');
        $('#saveLicenseBtn').attr('data-institutionId', '');
        $('#saveLicenseBtn').attr('data-courseSlug', '');
        $('#saveLicenseBtn').attr('data-institutionSlug', '');
    });

    $(document).on('click', '#saveLicenseBtn', function(){
        var form = $(this).closest('form');
        if($.fn.validateRequest(form)) {
            var data = {
                "code": form.find('#code').val(),
                "course_id": $(this).attr('data-courseId'),
                "institution_id": $(this).attr('data-institutionId'),
                "validity": form.find('#validity').val(),
                "price": form.find('#price').val(),
                "end_date": form.find('#endDate').val(),
                "type": form.find('.typeSlct').val(),
                "status": form.find('#status').val(),
                "role": form.find('#role').val(),
            };
            var courseSlug = $(this).attr('data-courseSlug');
            var institutionSlug = $(this).attr('data-institutionSlug');
            $.fn.ajaxCall(REQUEST_TYPE_POST, 
                form.attr('action'), JSON.stringify(data), DATA_TYPE_JSON)
            .then((successResponse) => {
                $('#createLicenseModal').modal('hide');
                $('#successModal').find('#successModalStatus').html(successResponse.message);
                $('#successModal').modal('show');
                window.location =  `${licenseRoute}/courses/${courseSlug}/institutions/${institutionSlug}`;
            });
        }
    });

    $(document).on('click', '.teacherLoginBtn', function(){
        var thisBtn = $(this);
        var data = {
            "institutionId": thisBtn.attr('data-institutionId'),
            "courseId": thisBtn.attr('data-courseId'),
        }
        $.fn.ajaxCall(REQUEST_TYPE_POST, 
            thisBtn.attr('data-route'), JSON.stringify(data), DATA_TYPE_JSON)
        .then((successResponse) => {
            if (successResponse.response) {
                window.location =  successResponse.route;
            } else {
                $('#errorModal').find('#errorModalErrors').html(successResponse.message);
                $('#errorModal').modal('show'); 
            }
        });
    });
});