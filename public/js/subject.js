$(function(){
    const CREATE = 'create';

    var deleteStatus = false;

    if($('#subject-table').length) {
        $.fn.yajraDataTable('#subject-table', 
            $('#subjectsRoute').val(), [
                {data: 'id', name: 'subjects.id'},
                {data: 'name', name: 'subjects.name'},
                {data: 'curriculum', name: 'curriculums.name'},
                {data: 'action', orderable: false, searchable: false},
            ]
        );
    }

    $.fn.populateLearningGoals = () => {
        $.fn.ajaxCall(REQUEST_TYPE_GET, 
            $('#curriculumId').find(':selected').attr('data-route'), {}, null
            ).then((successResponse) => {
                $('#learningGoals').html(successResponse);
                $.fn.initJquerySortable();
            }, (errorResponse) => {
        });
    };

    $('#curriculumId').on('change', function(){
        $.fn.populateLearningGoals();
    });

    // Check whether the subject is edited or created.
    if($('#curriculumId').length) {
        if($('#curriculumId').attr('data-type') === CREATE) {
            $('#curriculumId').change();
        } else {
            $('#curriculumId').attr('data-type', CREATE);
        }
    }
    
    $('#saveSubjectBtn').on('click', function(){
        var form = $(this).closest('form');
        var selectedLearningGoalIds = [];
        var selectedLearningGoalList = $(form).find('#selectedLearningGoals');
        $.each(selectedLearningGoalList.find('li').not('.disabled'), (i, v) => {
            selectedLearningGoalIds.push($(v).attr('data-id'));
        });
        if($.fn.validateRequest(form)) {
            if (selectedLearningGoalIds.length) {
                var data = {
                    "name": $(form).find('#name').val(),
                    "curriculumId": $(form).find('#curriculumId').find(':selected').val(),
                    "learningGoalIds": selectedLearningGoalIds,
                };
                var requestType = $(this).attr('data-request-type');
                if (requestType === REQUEST_TYPE_PUT) {
                    var existingLearningGoalIds = JSON.parse($('#existingLearningGoalIds').val());
                    if ($(this).attr('data-learningGoalChecked') == 0) {
                        $.fn.isDeleted(existingLearningGoalIds, data.learningGoalIds);
                        if (deleteStatus === true) {
                            deleteStatus = false;
                            $('#removeLearningGoalModal').modal('show');
                        } else {
                            $('#saveSubjectBtn').attr('data-learningGoalChecked', 1);
                        }
                    }
                }
                if ($(this).attr('data-learningGoalChecked') == 1) {
                    $.fn.ajaxCall(requestType, form.attr('action'), JSON.stringify(data), DATA_TYPE_JSON)
                    .then((successResponse) => {
                       window.location = successResponse.route;
                    }, (errorResponse) => {
                    });
                }
            } else {
                selectedLearningGoalList.find('.disabled').css('color', RED_COLOR).focus();
            }
        }
    });

    $(document).on('click', '#confirmRemoveLearningGoalBtn', () => {
        if($(document).find('#confirmRemoveLearningGoalInpt').val() === REMOVE) {
            $('#removeLearningGoalModal').modal('hide');
            $('#saveSubjectBtn').attr('data-learningGoalChecked', 1).click();
        }
    });

    $.fn.isDeleted = (existingData, newData) => {
        $.each(existingData, (i, v) => {
            if($.inArray(v.toString(), newData) == -1){
                deleteStatus = true;
                return false;
            }
        });
    };

    $.fn.initJquerySortable = () => {
        $('.list-group-sortable-connected').sortable({
            placeholderClass: 'list-group-item',
            connectWith: '.connected',
            items: ':not(.disabled)'
        });
    };

    if ($('.list-group-sortable-connected').length) {
        $.fn.initJquerySortable();        
    }

    $(document).on('click', '.deleteSubjects', function(){
        $('#deleteSubjectModal').find('#confirmDeleteSubjectBtn').attr('data-route', $(this).attr('data-route'));
        $('#deleteSubjectModal').modal('show');
    });

    $(document).on('click', '#confirmDeleteSubjectBtn', function(){
        if($(document).find('#confirmDeleteSubjectInpt').val() === DELETE) {
            $('#deleteSubjectModal').modal('hide');
            // Send ajax to delete
            $.fn.ajaxCall(REQUEST_TYPE_DELETE, $(this).attr('data-route'), {}, null
                ).then((successResponse) => {
                    window.location = successResponse.route;
                }, (errorResponse) => {
            });
        } else {
            $(document).find('#confirmDeleteSubjectInpt').css(BORDER_COLOR, RED_COLOR);
        }
    });

    $("#deleteSubjectModal").on('hidden.bs.modal', function () {
        $(this).find('#confirmDeleteSubjectInpt').val('');
    });
});