$(function(){
    $.fn.yajraDataTable(
    	'#roles-table', 
    	$('#rolesRoute').val(), 
    	[
            {data: 'id', name: 'roles.id'},
            {data: 'name', name: 'roles.name'},
            {data: 'action', orderable: false, searchable: false},
        ]
    );	
});