$(function(){
    $.fn.yajraDataTable(
    	'#permissions-table', 
    	$('#permissionsRoute').val(), 
    	[
            {data: 'id', name: 'permissions.id'},
            {data: 'name', name: 'permissions.name'},
            {data: 'action', orderable: false, searchable: false},
        ]
    );	
});