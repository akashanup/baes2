$(function(){
    $.fn.yajraDataTable(
    	'#users-table', 
    	$('#usersRoute').val(), 
    	[
            {data: 'id', name: 'users.id'},
            {data: 'name', name: 'users.first_name'},
            {data: 'email', name: 'users.email'},
            {data: 'role', orderable: false, searchable: false},
            {data: 'action', orderable: false, searchable: false},
        ]
    );	
});