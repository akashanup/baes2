$(function(){
    $(document).find('textarea').each(function() {
        CKEDITOR.config.mathJaxLib = mathjax;
        CKEDITOR.config.extraPlugins = 'uploadimage';
        CKEDITOR.config.imageUploadUrl = $('#awsImageUpload').val();
        CKEDITOR.replace($(this).get(0), {
            extraPlugins: 'mathjax',
            toolbar: 'Basic',
            filebrowserUploadUrl : $('#awsImageUpload').val()
        });
    })
});