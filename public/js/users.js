$(function(){
    const STATUS = 'status';
    const PASSWORD = 'password';
    const LICENSES = 'licenses';
    const ACTIVITY = 'activity';
    const DELETE_USER = 'delete';
    var userRoute = `${$('#homeRoute').val()}/users`;

    $.fn.userDatatable = (course, institution, license) => {
        $('#user-table').DataTable().clear().destroy();
        var route = `${userRoute}/institutions/${institution}/courses/${course}/licenses/${license}`; 
        history.pushState(null, null, route);
        $('#user-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: `${route}/indexData`,
            columns: [
                {data: 'id', name: 'users.id', className: 'rowId'},
                {data: 'name', name: 'users.first_name', className: 'rowName'},
                {data: 'email', name: 'users.email', className: 'rowEmail'},
                {data: 'status', name: 'users.status', className: 'rowStatus'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "initComplete": () => {
                $('#user-table_filter').before($('#filterLblDiv').html());
                $('#courseDropDown').val(course);
                $('#institutionDropDown').val(institution);
            },
            pageLength: 100
        });
    };

    $.fn.licenseData = (route) => {
        return new Promise((resolve, reject) => {
            var html = null;
            $.fn.ajaxCall(REQUEST_TYPE_GET, route, {}, DATA_TYPE_JSON)
            .then((successResponse) => {
                resolve(successResponse.licenses);
            });
        });
        
    };

    if($('#user-table').length) {
        $.fn.userDatatable($('#courseSlug').val(), $('#institutionSlug').val(), $('#licenseId').val());
    }

    $(document).on('change', '#courseDropDown', function(){
        $.fn.userDatatable($(this).val(), $('#institutionDropDown').val(), ALL);
    });

    $(document).on('change', '#institutionDropDown', function(){
        $.fn.userDatatable($('#courseDropDown').val(), $(this).val(), ALL);
    });

    $(document).on('click', '.userActions', function(){
        var thisAction = $(this);
        if ($.inArray(thisAction.attr('data-function'), [STATUS, DELETE_USER, PASSWORD]) != -1) {
            $.fn.ajaxCall(thisAction.attr('data-request'), thisAction.attr('data-route'), 
                {}, DATA_TYPE_JSON)
            .then((successResponse) => {
                $.fn.userDatatable($('#courseDropDown').val(), $('#institutionDropDown').val(), $('#licenseId').val());
                $('#successModal').find('#successModalStatus').html(successResponse.message);
                $('#successModal').modal('show');
            });
        } else if (thisAction.attr('data-function') == LICENSES) {
            $.fn.licenseData(thisAction.attr('data-route')).then((html) => {
                $('#userLicenseModal').find('#userLicensesDiv').html(html);
                $('#userLicenseModal').modal('show');
            });
        }
    });

    $("#userLicenseModal").on('hidden.bs.modal', function () {
        $(this).find('#userLicensesDiv').html('');
    });

    $(document).on('click', '.activityBtn', function(){
        var activity = $(this).closest('tbody').find(`#activity${$(this).attr('data-id')}`);
        if(activity.attr('data-status') === 'hidden'){
            activity.attr('data-status', 'shown');
            activity.removeClass('hidden');
        } else {
            activity.attr('data-status', 'hidden');
            activity.addClass('hidden');
        }
    });

    $(document).on('click', '.changeUserLicenseBtn', function(){
        $.fn.ajaxCall(REQUEST_TYPE_PUT, $(this).attr('data-route'), 
            {}, DATA_TYPE_JSON)
        .then((successResponse) => {
            $("#userLicenseModal").modal('hide');
            $('#successModal').find('#successModalStatus').html(successResponse.message);
            $('#successModal').modal('show');
        });
    });
});