$(function(){
    const license = $('#license').val();
    const subjectId =  $('#subjectId').val();
    const learningGoalId =  $('#learningGoalId').val();
    const MCQ = 'MCQ';
    const DIRECT = 'DIRECT';

	$('.beginLearningGoalBtn').on('click', function(){
		var data = {
			"license" : $(this).attr('data-licenseID'),
			"subjectId": $(this).attr('data-subjectId'),
			"learningGoalId": $(this).attr('data-learningGoalId'),
			"userId": $(this).attr('data-userId'),
			"status": $(this).attr('data-updateStatus'),
		};
		$.fn.ajaxCall(REQUEST_TYPE_POST, $(this).attr('data-updateroute'), JSON.stringify(data), DATA_TYPE_JSON
            ).then((successResponse) => {
                window.location = $(this).attr('data-redirectroute');
            }, (errorResponse) => {
        });
	});

	$(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');

        $('.js__examples-overflow').css('max-height', 110);
        $('.js__toggle-examples').removeClass('up');
        $('.js__toggle-examples').html('Lees meer');

        $('.js__intro-overflow').css('max-height', 110);
        $('.js__toggle-intro').removeClass('up');
        $('.js__toggle-intro').html('Lees meer');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    var introHeight = $('.js__intro-overflow').outerHeight();
    $('.js__intro-overflow').css('max-height', 110);
    $('.js__toggle-intro').click(function (e) {
        e.preventDefault();
        setTimeout(function () {
            if ($('.js__toggle-intro').hasClass('up')) {
                $('.js__intro-overflow').css('max-height', 110);
                $('.js__toggle-intro').removeClass('up');
                $('.js__toggle-intro').html('Lees meer');
            }
            else {
                $('.js__intro-overflow').css('max-height', introHeight + 50);
                $('.js__toggle-intro').addClass('up');
                $('.js__toggle-intro').html('Lees minder');
            }
        }, 100);
    });

    var examplesHeight = $('.js__examples-overflow').outerHeight();
    $('.js__examples-overflow').css('max-height', 110);
    $('.js__toggle-examples').click(function (e) {
        e.preventDefault();
        setTimeout(function () {
            if ($('.js__toggle-examples').hasClass('up')) {
                $('.js__examples-overflow').css('max-height', 110);
                $('.js__toggle-examples').removeClass('up');
                $('.js__toggle-examples').html('Lees meer');

            }
            else {
                $('.js__examples-overflow').css('max-height', examplesHeight + 30);
                $('.js__toggle-examples').addClass('up');
                $('.js__toggle-examples').html('Lees minder');

            }
        }, 100);
    });

    $.fn.addClassTable = () => {
        $.each($(document).find('table'), function(i, v){
            if(!$(v).hasClass('table')){
                $(v).addClass('table');
            }
        });
    };

    $.fn.checkAnswer = (data, introductionQuestion=null) => {

        $.fn.ajaxCall(REQUEST_TYPE_POST, $('#checkQuestionAnswerRoute').val(), 
            JSON.stringify(data), null)
        .then((successResponse) => {
            var attempt = successResponse.attempt;
            var questionDiv = $('#questionDiv');
            var introductionQuestion = questionDiv.attr('data-introductionQuestion');
            var $feedbackBlock = $('.js__block-feedback');
            if ($('#mortarboardDiv').length && $('#eligibility').val().length 
                && !successResponse.eligibility) {
                $('#mortarboardDiv').addClass('hidden');
            }
            if (attempt <= 4) {
                questionDiv.attr('data-attempt', (parseInt(attempt)+1));
                $feedbackBlock.removeClass('hidden');
                if (successResponse.correctAnswer) {
                    // Correct answer
                    $(`.score__item:eq(${(attempt - 1)})`).addClass('score__item--correct');
                    $('#answerBtn').addClass('hidden');
                    $('#retryBtn').addClass('hidden');
                    if (introductionQuestion && attempt  <= 2) {
                        $(document).find('.mcqAnswers').prop('disabled', true);
                        $('#learningGoalTestBtn').removeClass('hidden');
                    } else {
                        if($(document).find('.mcqAnswers:checked').length) {
                            $(document).find('.mcqAnswers').prop('disabled', true);
                        } else if ($(document).find('#inptAnswer').length 
                            && $(document).find('#inptAnswer').val()) {
                            $(document).find('#inptAnswer').prop('disabled', true);
                        }
                        $('#nextQuestionBtn').removeClass('hidden');
                    }
                    if(successResponse.hint) {
                        $feedbackBlock.find('[data-message=correct]')
                        .find('.hint').html(successResponse.hint);
                    }
                    $feedbackBlock.find('[data-message=wrong]').hide();
                    $feedbackBlock.find('[data-message=correct]').show();
                } else {
                    // Wrong answer
                    if (attempt == 1) {
                        $('#answerBtn').addClass('hidden');
                        $('#retryBtn').removeClass('hidden');
                    }
                    $(`.score__item:eq(${(attempt - 1)})`).addClass('score__item--wrong');
                    if (introductionQuestion && attempt  <= 2) {
                        var mcqAnswers = $(document).find('.mcqAnswers');
                        mcqAnswers.prop('checked', false);
                        if (attempt == 2) {
                            mcqAnswers.prop('disabled', true);
                            $('#retryBtn').addClass('hidden');
                            $('#nextQuestionBtn').removeClass('hidden');
                        }
                    } else {
                        if($(document).find('.mcqAnswers:checked').length) {
                            var mcqAnswers = $(document).find('.mcqAnswers');
                            mcqAnswers.prop('checked', false);
                            if (attempt == 4) {
                                mcqAnswers.prop('disabled', true);
                                $('#retryBtn').addClass('hidden');
                                $('#nextQuestionBtn').removeClass('hidden');
                            }
                        } else if ($(document).find('#inptAnswer').length 
                            && $(document).find('#inptAnswer').val()) {
                            var inptAnswer = $(document).find('#inptAnswer');
                            inptAnswer.val('');
                            if (attempt == 4) {
                                inptAnswer.prop('disabled', true);
                                $('#retryBtn').addClass('hidden');
                                $('#nextQuestionBtn').removeClass('hidden');
                            }
                        }
                    }
                    if(successResponse.hint) {
                        $feedbackBlock.find('[data-message=wrong]').find('#wrongAnswer').css('display', 'none');
                        $feedbackBlock.find('[data-message=wrong]').find('.hint').html(successResponse.hint);
                    } else {
                        $feedbackBlock.find('[data-message=wrong]').find('#wrongAnswer').css('display', 'block');
                    }
                    $feedbackBlock.find('[data-message=wrong]').show();
                }
            }
            if ((successResponse.eligibility || successResponse.improve) 
                && (successResponse.correctAnswer)) {
                if (successResponse.finishedLearningGoalModalData) {
                    $('#learningGoalFinishedModalData').html(successResponse.finishedLearningGoalModalData);
                    $(document).find('#learningGoalFinishedModal').modal('show');
                } else if($(document).find('#learningGoalFinishedModal').length) {
                    $(document).find('#learningGoalFinishedModal').modal('show');
                }
            }
            if (successResponse.learningGoalTestScoreDiv) {
                $('#learningGoalTestScoreDiv').html(successResponse.learningGoalTestScoreDiv);
            }
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            if ($feedbackBlock.find('[data-message=wrong]').find('.hint').html().length) {
                MathJax.Hub.Queue(function(){
                    $feedbackBlock.find('[data-message=wrong]').find('.hint').html(
                        $feedbackBlock.find('[data-message=wrong]').find('.hint').html().replace(/\\%/g, '%'));
                });    
            }
            if ($feedbackBlock.find('[data-message=correct]').find('.hint').html().length) {
                MathJax.Hub.Queue(function(){
                    $feedbackBlock.find('[data-message=correct]').find('.hint').html(
                        $feedbackBlock.find('[data-message=correct]').find('.hint').html().replace(/\\%/g, '%'));
                });    
            }
        });
    };

    $(document).on('click', '#nextQuestionBtn', () => {
        var learningGoalQuestionDiv = $('#learningGoalQuestionDiv');
        var learningGoalTestId = learningGoalQuestionDiv.attr('data-learningGoalTestId');
        var previousTestQuestionId = learningGoalQuestionDiv.attr('data-questionId');
        if(!isNaN(parseInt(previousTestQuestionId)) && isNaN(parseInt(learningGoalTestId))){
            $.fn.getLearningGoalTestQuestion(previousTestQuestionId);
        } else {
            $.fn.getLearningGoalTestQuestion();
        }
        $('#nextQuestionBtn').addClass('hidden');
        $('#answerBtn').removeClass('hidden');
        $('.hint').html('');
        var $feedbackBlock = $('.js__block-feedback');
        $feedbackBlock.find('[data-message=wrong]').hide();
        $feedbackBlock.find('[data-message=correct]').hide();
        $feedbackBlock.addClass('hidden');
        $('.score__item').removeClass('score__item--wrong');
        $('.score__item').removeClass('score__item--correct');        
    });

    $('.answerBtn').on('click', function(e){
        var input  = '';
        var answer = '';
        var data   = '';
        var questionDiv = $('#questionDiv');
        var attempt = questionDiv.attr('data-attempt');
        var learningGoalQuestionDiv = $('#learningGoalQuestionDiv');
        var questionId = learningGoalQuestionDiv.attr('data-questionId');
        var learningGoalTestId = learningGoalQuestionDiv.attr('data-learningGoalTestId');

        if($(document).find('.mcqAnswers:checked').length) {
            input = $('.mcqAnswers:checked');
            data = {
                "questionId": questionId,
                "learningGoalTestId": learningGoalTestId,
                "answer": input.val(),
                "license": license,
                "subjectId": subjectId,
                "learningGoalId": learningGoalId,
                "type": MCQ,
                "attempt": attempt,
                "subjectSlug": $('#subjectSlug').val()
            }
            $.fn.checkAnswer(data, questionDiv.attr('data-introductionQuestion'));
        } else if( $(document).find('#inptAnswer').length 
            && $(document).find('#inptAnswer').val() ) {
            input = $(document).find('#inptAnswer');
            $.fn.modifyAnswerField($(input));
            var answer = $.fn.formatAnswer(input.val());
            data = {
                "questionId": questionId,
                "learningGoalTestId": learningGoalTestId,
                "answer": answer,
                "license": license,
                "subjectId": subjectId,
                "learningGoalId": learningGoalId,
                "type": DIRECT,
                "attempt": attempt,
                "subjectSlug": $('#subjectSlug').val()
            }
            if (answer.length) {
                $.fn.checkAnswer(data, questionDiv.attr('data-introductionQuestion'));
            }
        }
        
    });

    $.fn.getLearningGoalTestQuestion = (previousTestQuestionId = null) => {
        var route = $('#learningGoalTestQuestionRoute').val();
        if (previousTestQuestionId) {
            route += `/${previousTestQuestionId}`;
        }
        $.fn.ajaxCall(REQUEST_TYPE_GET, 
            route, {}, null)
        .then((successResponse) => {
            $('#questionDiv').attr('data-attempt', 1);
            $('#questionDiv').html(successResponse);
            if ($('#questionLevelText').length && $('#questionLevel').val().length) {
                $('.baesLevel').html($('#questionLevel').val());
            }
            if ($('#mortarboardDiv').length && $('#eligibility').val().length) {
                $('#mortarboardDiv').removeClass('hidden');
            } else {
                $('#mortarboardDiv').addClass('hidden');
            }

            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            $(document).find('table').addClass('table');
            if ($(document).find('.form__radio-label_new').length) {
                $.each($(document).find('.form__radio-label_new'), (i, v) => {
                    if($(v).find('.math-tex').length == 0){
                        $(v).css('display', 'block');
                    }
                })

            }
        });
    };

    if ($('#learningGoalTestQuestionRoute').length) {        
        $.fn.getLearningGoalTestQuestion();
    }

    $(document).on('hidden.bs.modal', "#learningGoalFinishedModal", function () {
        location.reload();
    });

    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);

    $.fn.savePracticeTestAnswer = (data, route) => {
        $.fn.ajaxCall(REQUEST_TYPE_PUT, route, 
            JSON.stringify(data), DATA_TYPE_JSON)
        .then((successResponse) => {
            window.location = successResponse.route;
        });
    };

    $('#practiceTestAnswerBtn').on('click', function() {
        var questionId = $(this).attr('data-questionId');
        var route = $(this).attr('data-savePracticeAnswerRoute');
        if($(document).find('.mcqAnswers:checked').length) {
            var data = {
                "question_answer_id": questionId,
                "answer": $('.mcqAnswers:checked').val(),
                "type": MCQ,
            }
            $.fn.savePracticeTestAnswer(data, route);
        } else if( $(document).find('#inptAnswer').length 
            && $(document).find('#inptAnswer').val() ) {
            var input = $(document).find('#inptAnswer');
            $.fn.modifyAnswerField($(input));
            var answer = $.fn.formatAnswer(input.val());
            var data = {
                "question_answer_id": questionId,
                "answer": answer,
                "type": DIRECT,
            }
            if (answer.length) {
                $.fn.savePracticeTestAnswer(data, route);
            }
        }
    });

    $.fn.submitPracticeTest = (route) => {
        $.fn.ajaxCall(REQUEST_TYPE_PUT, route, 
            {}, DATA_TYPE_JSON)
        .then((successResponse) => {
            window.location = successResponse.route;
        });
    };

    $('#submitPracticeTestBtn').on('click', function(){
        if ($('.questionLink[data-status="undone"]').length) {
            $('#unansweredQuestionModal').modal('show');
        } else {
            $.fn.submitPracticeTest($(this).attr('data-route'));
        }
    });

    $('#confirmSubmitPracticeTestBtn').on('click', function(){
        $('#unansweredQuestionModal').modal('hide');
        $.fn.submitPracticeTest($(this).attr('data-route'));
    });

    $('.overlayBtn').on('click', () => {
        $(document).find('.form--answers').remove();
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        $(document).find('table').addClass('table');
    });

    $('.updateStudentPracticeTestStatusBtn').on('click', function(){
        var redirectRoute = $(this).attr('data-redirectRoute');
        $.fn.ajaxCall(REQUEST_TYPE_PUT, $(this).attr('data-updateRoute'), 
            {}, DATA_TYPE_JSON)
        .then((successResponse) => {
            window.location = redirectRoute;
        });
    });

    $(document).on('click', '.youtubeVideoOverlayDiv', () => {
        $(document).find('#youtubeVideoOverlayCloseDiv').trigger('click');
    });
});