$(document).ready(function () {

    var controls    = [8, 16, 17, 35, 36, 37, 38, 39, 40];
    var validChars  = ['0','1','2','3','4','5','6','7','8','9', '-', '%', ',', '.'];

    $(document).on('keypress paste', '.numeric', function (e) {
        var keyCode     = e.keyCode ? e.keyCode : e.which;
        
        if(controls.indexOf(keyCode) == -1) {
           if(validChars.indexOf(String.fromCharCode(keyCode)) == -1) {
                e.preventDefault();
            }
        }

        if(String.fromCharCode(keyCode) == ',' && $(this).val().indexOf(',') != -1){
            e.preventDefault();
        }

        if(String.fromCharCode(keyCode) == '%' && $(this).val().indexOf('%') != -1){
            e.preventDefault();
        }

        if(String.fromCharCode(keyCode) == '-' && $(this).val().indexOf('-') != -1){
            e.preventDefault();
        }

        if(String.fromCharCode(keyCode) == '.' && ($(this).val().indexOf(',') != -1 
            || $(this).val().indexOf('.') != -1) ){
            e.preventDefault();
        }
        $(this).val($(this).val().replace(".",","));
    });

    $.fn.modifyAnswerField = function(thisInpt) {
        var val             = thisInpt.val();
        if(val.length) {
            val                 = val.replace('.', ',');
            if(val.indexOf('-') != -1){
                val = '-' + val.replace(/-/g, '');
            }

            if(val.indexOf('%') != -1){
                val = val.replace(/%/g, '') + '%';
            }

            if(val.indexOf(',') != -1){
                var num = val.substring(0, val.lastIndexOf(',')+1).replace(/,/g,'');
                var decimal = val.substring(val.lastIndexOf(',')+1).replace(/%/g,'');
                var percentage = '';
                if(decimal.length == 0){
                    decimal = ',00';
                }
                else if(!$.isNumeric(decimal)) {
                    if(!$.isNumeric(decimal.substring(0,1))) {
                        decimal = ',00';
                    }
                    else {
                        decimal = ',' + decimal.substring(0,1) + '0';
                    }
                }
                else {
                    decimal = ',' + decimal;
                }

                
                if(val.indexOf('%') != -1) {
                    percentage = '%';
                }

                val = num + decimal + percentage;
            }
            else {
                var num = val.substring(0);
                var decimal = ',00';
                if(num.length == 0 || !/\d/.test(num)){
                    var decimal = '0,00';   
                }
                var percentage = '';

                if(val.indexOf('%') != -1) {
                    num = num.replace(/%/g, '');
                    percentage = '%';
                }

                val = num + decimal + percentage;

            }

            val = val.replace(/\./g,'');
            var number =  val.substring(val.indexOf('-') != -1 ? 1 : 0 , val.indexOf(','));
            
            var inptLength = number.length;

            if(inptLength > 3) {
                var pivot = inptLength - 3 + (val.indexOf('-') != -1 ? 1 : 0);
                for( var i = 0; i< inptLength-3; i=i+3) {
                    val = val.substring(0, pivot-i) + '.' + val.substring(pivot-i);
                }
            };

            thisInpt.val(val);
        }
    }

    $(document).on('focusout paste', '.numeric', function (e) {
        $.fn.modifyAnswerField($(this));
    });



    $.fn.formatAnswer = function(answer) {
        answer = answer.replace(/\./g, '');
        answer = answer.replace(',', '.');
        return answer;
    };

});