<?php

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [
        'localeSessionRedirect', 'localizationRedirect', 'localeViewPath',
    ],
], function () {
    Route::get('/', function () {
        return redirect()->route('login');
    })->name('home');

    Auth::routes();

    Route::post('/payments/paymentWebHook', 'PaymentController@paymentWebHook')
        ->name('payments.paymentWebHook');

    Route::group(['middleware' => ['verifyUserToken']], function () {
        Route::get('/users/activation/{token}',
            'Auth\UserActivationController@activate')->name('users.activate');
        Route::post('/users/save-password/{token}',
            'Auth\UserActivationController@savePassword')->name('users.savePassword');
    });

    Route::group(['middleware' => ['auth', 'activeUser', 'permission']], function () {
        Route::get('/permissions', 'Acl\PermissionController@index')
            ->name('permissions.index');
        Route::get('/permissions/indexData',
            'Acl\PermissionController@indexData')->name('permissions.indexData');
        Route::get('/permissions/create', 'Acl\PermissionController@create')
            ->name('permissions.create');
        Route::post('/permissions', 'Acl\PermissionController@store')
            ->name('permissions.store');
        Route::get('/permissions/{permission}/edit',
            'Acl\PermissionController@edit')->name('permissions.edit');
        Route::put('/permissions/{permission}',
            'Acl\PermissionController@update')->name('permissions.update');
        Route::delete('/permissions/{permission}',
            'Acl\PermissionController@destroy')->name('permissions.destroy');

        Route::get('/roles', 'Acl\RoleController@index')
            ->name('roles.index');
        Route::get('/roles/indexData', 'Acl\RoleController@indexData')
            ->name('roles.indexData');
        Route::get('/roles/create', 'Acl\RoleController@create')
            ->name('roles.create');
        Route::post('/roles', 'Acl\RoleController@store')
            ->name('roles.store');
        Route::get('/roles/{role}/edit', 'Acl\RoleController@edit')
            ->name('roles.edit');
        Route::put('/roles/{role}', 'Acl\RoleController@update')
            ->name('roles.update');
        Route::delete('/roles/{role}', 'Acl\RoleController@destroy')
            ->name('roles.destroy');

        Route::get('/roles/{role}/permissions',
            'Acl\RolePermissionController@index')
            ->name('roles.permissions.index');
        Route::put('/roles/{role}/permissions',
            'Acl\RolePermissionController@update')
            ->name('roles.permissions.update');

        Route::get('/acl/users', 'Acl\UserController@index')
            ->name('acl.users.index');
        Route::get('/acl/users/indexData', 'Acl\UserController@indexData')
            ->name('acl.users.indexData');
        Route::get('/acl/users/create', 'Acl\UserController@create')
            ->name('acl.users.create');
        Route::post('/acl/users', 'Acl\UserController@store')
            ->name('acl.users.store');
        Route::get('/acl/users/{user}/edit', 'Acl\UserController@edit')
            ->name('acl.users.edit');
        Route::put('/acl/users/{user}', 'Acl\UserController@update')
            ->name('acl.users.update');
        Route::delete('/acl/users/{user}', 'Acl\UserController@destroy')
            ->name('acl.users.destroy');

        Route::get('/admins/dashboard', 'AdminController@dashboard')
            ->name('admins.dashboard');
        Route::post('/admins/uploadImage', 'AdminController@uploadImage')
            ->name('admins.uploadImage');
        Route::get('/sub-admins/dashboard', 'AdminController@subAdminDashboard')
            ->name('sub-admins.dashboard');
        Route::post('/admins/teacher-login', 'AdminController@teacherLogin')
            ->name('admins.teacherLogin');
        Route::get('/admins/export-table', 'AdminController@exportTable')
            ->name('admins.exportTable');
        Route::get('/admins/export-data', 'AdminController@exportData')
            ->name('admins.exportData');

        Route::get('/institutions/{institution}/courses/{course}',
            'InstitutionController@index')->name('institutions.index');
        Route::get('/institutions/{institution}/courses/{course}/indexData',
            'InstitutionController@indexData')->name('institutions.indexData');
        Route::get('/institutions/create', 'InstitutionController@create')
            ->name('institutions.create');
        Route::post('/institutions', 'InstitutionController@store')
            ->name('institutions.store');
        Route::put('/institutions/{slug}', 'InstitutionController@update')
            ->name('institutions.update');
        Route::delete('/institutions/{slug}', 'InstitutionController@destroy')
            ->name('institutions.destroy');

        Route::get('/courses/', 'CourseController@index')
            ->name('courses.index');
        Route::get('/courses/indexData', 'CourseController@indexData')
            ->name('courses.indexData');
        Route::get('/courses/create', 'CourseController@create')
            ->name('courses.create');
        Route::post('/courses', 'CourseController@store')
            ->name('courses.store');
        Route::get('/courses/{slug}/edit', 'CourseController@edit')
            ->name('courses.edit');
        Route::put('/courses/{slug}', 'CourseController@update')
            ->name('courses.update');
        Route::delete('/courses/{slug}', 'CourseController@destroy')
            ->name('courses.destroy');
        Route::post('/courses/{slug}/copy', 'CourseController@copy')
            ->name('courses.copy');
        Route::get('/courses/{slug}/institutions',
            'CourseController@institutions')->name('courses.institutions');

        Route::get('/licenses/courses/{course}/institutions/{institution}',
            'LicenseController@index')->name('licenses.index');
        Route::get('/licenses/courses/{course}/institutions/{institution}/indexData',
            'LicenseController@indexData')->name('licenses.indexData');
        Route::post('/licenses', 'LicenseController@store')
            ->name('licenses.store');
        Route::get('/licenses/{license}/{code?}', 'LicenseController@show')
            ->name('licenses.show');
        Route::put('/licenses/{license}', 'LicenseController@update')
            ->name('licenses.update');
        Route::delete('/licenses/{license}', 'LicenseController@destroy')
            ->name('licenses.destroy');

        Route::get('/curriculums', 'CurriculumController@index')
            ->name('curriculums.index');
        Route::get('/curriculums/create', 'CurriculumController@create')
            ->name('curriculums.create');
        Route::post('/curriculums', 'CurriculumController@store')
            ->name('curriculums.store');
        Route::get('/curriculums/{curriculum}/edit',
            'CurriculumController@edit')->name('curriculums.edit');
        Route::get('/curriculums/indexData', 'CurriculumController@indexData')
            ->name('curriculums.indexData');
        Route::delete('/curriculums/{curriculum}',
            'CurriculumController@destroy')->name('curriculums.destroy');
        Route::put('/curriculums/{curriculum}', 'CurriculumController@update')
            ->name('curriculums.update');
        Route::get('/curriculums/learning-goals/{curriculum}',
            'CurriculumController@learningGoals')
            ->name('curriculums.learning-goals');

        Route::get('/learning-goals/create', 'LearningGoalController@create')
            ->name('learning-goals.create');
        Route::post('/learning-goals', 'LearningGoalController@store')
            ->name('learning-goals.store');

        Route::get('/learning-goals', 'LearningGoalController@index')
            ->name('learning-goals.index');
        Route::get('/learning-goals/curriculum/{curriculum?}',
            'LearningGoalController@index')
            ->name('learning-goals.curriculum.index');
        Route::get('/learning-goals/indexData/{curriculum?}',
            'LearningGoalController@indexData')
            ->name('learning-goals.indexData');
        Route::get('/learning-goals/curriculumData/{requestType}/{type?}',
            'LearningGoalController@curriculumData')
            ->name('learning-goals.curriculumData');
        Route::get('/learning-goals/{learningGoal}/edit',
            'LearningGoalController@edit')->name('learning-goals.edit');
        Route::put('/learning-goals/{learningGoal}/update',
            'LearningGoalController@update')->name('learning-goals.update');
        Route::delete('/learning-goals/{learningGoal}',
            'LearningGoalController@destroy')->name('learning-goals.destroy');

        Route::get('/subjects/', 'SubjectController@index')
            ->name('subjects.index');
        Route::get('/subjects/indexData', 'SubjectController@indexData')
            ->name('subjects.indexData');
        Route::get('/subjects/create', 'SubjectController@create')
            ->name('subjects.create');
        Route::post('/subjects', 'SubjectController@store')
            ->name('subjects.store');
        Route::get('/subjects/{slug}/edit', 'SubjectController@edit')
            ->name('subjects.edit');
        Route::put('/subjects/{slug}', 'SubjectController@update')
            ->name('subjects.update');
        Route::delete('/subjects/{slug}', 'SubjectController@destroy')
            ->name('subjects.destroy');

        Route::get('/question-template/upload/{learningGoal}/{type}',
            'QuestionTemplateController@uploadTemplate')
            ->name('template.upload');
        Route::post('/question-template/uploadContent',
            'QuestionTemplateController@uploadData')
            ->name('template.uploadContent');

        Route::get('/question-template/template-list/{learningGoal}/{type}/{curriculum?}',
            'QuestionTemplateController@getTemplateList')
            ->name('template.getTemplateList');
        Route::get('/template/variable-set/{id}/edit',
            'QuestionTemplateController@editVariableSet')
            ->name('template.editVariableSet');
        Route::get('/question-template/pdf-download/{learningGoal}/{type}', 'QuestionTemplateController@downloadLearningGoalQuestion')
            ->name('question-template.downloadPdf');
        Route::get('/question-template/variable-set/{type}/curriculum/{curriculumSlug?}',
            'QuestionTemplateController@listVariableSet')
            ->name('template.curriculumTemplate');
        Route::get('/question-template/edit/{learningGoal}/{type}',
            'QuestionTemplateController@edit')
            ->name('template.edit');
        Route::get('/learning-goal/{slug}/question-template/{type}/{templateId}/edit',
            'QuestionTemplateController@editQuestionTemplate')
            ->name('template.editQuestionTemplate');
        Route::get('/question-template/mcq-template-list/{learningGoal}',
            'QuestionTemplateController@mcqTemplateList')
            ->name('template.mcqTemplateList');
        Route::get('/question-template/learning-goal/{learningGoal}/{type}/download',
            'QuestionTemplateController@downloadQuestionTemplate')
            ->name('template.downloadQuestionTemplate');
        Route::get('/variable-set/{learningGoal}/download',
            'QuestionTemplateController@downloadVariableSet')
            ->name('template.downloadVariableSet');
        Route::get('learning-goal/{learningGoal}/template/variable-set/{templateId}/edit',
            'VariableSetController@listVariableSet')
            ->name('variable-set.showVariableSet');
        Route::post('/variable-set/{learningGoal}/upload',
            'VariableSetController@uploadVariableSet')
            ->name('variable-set.upload');

        Route::post('learning-goal/{learningGoalId}/question-template/{type}/{id}/update',
            'QuestionTemplateController@updateQuestionTemplate')
            ->name('question-template.update');

        Route::post('/payments/makePayment/',
            'PaymentController@makePayment')
        ->name('payments.makePayment');
        Route::get('/payments/success/{id}', 'PaymentController@index')
            ->name('payments.index');
        Route::get('/payments/createSubscription',
            'PaymentController@createSubscription')
            ->name('payments.createSubscription');
        Route::get('/payments/paymentHistory', 'PaymentController@paymentHistory')
            ->name('payments.paymentHistory');

        Route::get('/users/licenses', 'UserController@licenses')
            ->name('users.licenses');
        Route::get('/users/available-licenses/{code?}',
            'UserController@availableLicenses')
            ->name('users.availableLicenses');
        Route::get('users/institutions/{institution}/courses/{course}/licenses/{licenseId}',
            'UserController@index')
        ->name('users.index');
        Route::get('/users/institutions/{institution}/courses/{course}/licenses/{licenseId}/indexData',
            'UserController@indexData')
        ->name('users.indexData');
        Route::put('/users/{user}', 'UserController@changeStatus')
            ->name('users.changeStatus');
        Route::delete('/users/{user}', 'UserController@destroy')
            ->name('users.destroy');
        Route::get('/users/{user}/reset-password', 'UserController@resetPassword')
            ->name('users.resetPassword');
        Route::get('/users/{user}/licenses', 'UserController@showAllLicenses')
            ->name('users.show.licenses');
        Route::put('/users/{user}/licenses/{license}', 'UserController@changeLicense')
            ->name('users.changeLicense');

        Route::group(['middleware' => ['authCurrentUser']], function () {
            Route::get('/users/{userSlug}/edit', 'UserController@edit')
                ->name('users.edit');
            Route::put('/users/{userSlug}/update', 'UserController@update')
                ->name('users.update');
        });

        Route::post('/users/subject/learning-goals',
            'PracticeTestTemplateController@subjectlearningGoal')
            ->name('teachers.subjectlearningGoal');

        Route::post('/users/save/test-template-details/test-template',
            'PracticeTestController@saveTestDetails')
            ->name('teachers.saveTestDetails');

        Route::post('/practice-test-template/update/folder',
            'PracticeTestTemplateController@updateTestTemplateFolder')
            ->name('teachers.updateTestTemplateFolder');

        Route::get('/questions/data/{unique?}/{id?}',
            'TeacherController@questionsData')
            ->name('teachers.questionsData');

        Route::post('/practice-test-template/delete',
            'PracticeTestTemplateController@deleteTestTemplate')
            ->name('teachers.deleteTestTemplate');
        Route::post('/practice-test-template/change-status',
            'PracticeTestController@changeTestStatus')
            ->name('teachers.changeStatus');
        Route::post('/practice-test-template/{practiceTestTemplate}/copy',
            'PracticeTestTemplateController@copyTemplate')
            ->name('teachers.copyTestTemplate');

        Route::post('questions/learning-goals/view',
            'TeacherController@viewQuestion')
            ->name('teachers.viewQuestion');

        Route::get('/licenses/{license}/courses/{slug}/score-overview/grade/{grade}/subjects/{subject}/{viewType}/{sortOrder?}',
            'GradeController@getGradeOverview')
            ->name('grades.overview');
        Route::get('/licenses/{license}/courses/{slug}/grade/{grade}/subjects/{subject}/student-overview/{studentId}',
            'GradeController@getGradeStudent')
            ->name('grades.students');

        Route::get('/licenses/{license}/courses/{slug}/students-management/{year}/{class}',
            'ClassController@studentManagement')
            ->name('teachers.student-management');

        Route::get('/student-feedbacks/institutions/{institution}/courses/{course}',
            'StudentFeedbackController@index')
            ->name('studentFeedbacks.index');
        Route::get('/student-feedbacks/institutions/{institution}/courses/{course}/indexData',
            'StudentFeedbackController@indexData')
            ->name('studentFeedbacks.indexData');
        Route::post('student-feedbacks',
            'StudentFeedbackController@store')
            ->name('studentFeedbacks.store');
        Route::put('student-feedbacks/{studentFeedback}/store-note',
            'StudentFeedbackController@storeNote')
            ->name('studentFeedbacks.storeNote');
        Route::put('student-feedbacks/{studentFeedback}/update',
            'StudentFeedbackController@updateStatus')
            ->name('studentFeedbacks.updateStatus');

        Route::get('/question-answers/{questionAnswer}/show',
            'QuestionAnswerController@show')
            ->name('questionAnswers.show');

        Route::get('/learning-goal/question-template/{type}/create',
            'QuestionTemplateController@createTemplate')
            ->name('admins.create-template');
        Route::post('/learning-goal/question-template/{type}/create',
            'QuestionTemplateController@saveTemplate')
            ->name('admins.save-template');

        Route::get('/question-template/variable-set/selected/{id}/edit',
            'VariableSetController@editSelectedVariableSet')
            ->name('admins.editSelectedVariableSet');

        Route::post('/question-template/variable-set/{id}/update',
            'VariableSetController@updateVariableSet')
            ->name('admins.updateVariableSet');

        Route::post('/question-template/view/direct/',
            'TemplateViewController@previewDirectTemplate')
            ->name('admins.previewDirectTemplate');
        Route::post('/question-template/direct/variable-set/store',
            'VariableSetController@storeDirectVariableSet')
            ->name('template.direct-store');

        Route::get('/question-template/variable-set/selected/{id}/download',
            'VariableSetController@downloadQuestionVariableSet')
            ->name('admins.downloadQuestionVariableSet');

        Route::post('/question-template/variable-set/selected/{id}/upload',
            'VariableSetController@uploadQuestionVariableSet')
            ->name('admins.uploadQuestionVariableSet');

        Route::get('/learning-goal/{learninGoal}/theory/preview',
            'LearningGoalController@theoryPreview')
            ->name('learning-goals.theoryPreview');
        Route::post('/learning-goal/{learninGoal}/theory-pdf/upload',
            'LearningGoalController@uploadTheoryPdf')
            ->name('learning-goals.uploadTheoryPdf');

        Route::get('/payments/licenses/{license}/cancel',
            'PaymentController@cancelUserLicense')
            ->name('license.cancelUserLicense');
        Route::group(['middleware' => ['authLicense']], function () {
            Route::get('/users/licenses/{license}/show',
                'UserController@showLicense')->name('users.licenses.show');
            Route::get('/licenses/{license}/courses/{courseSlug}/subjects',
                'UserController@courses')->name('users.courses');

            Route::get('/licenses/{license}/courses/{slug}/practice-test-management',
                'PracticeTestController@managePracticeTest')
                ->name('teachers.manageTest');
            Route::get('/licenses/{license}/courses/{slug}/practice-test-finished',
                'PracticeTestController@practiceTestFinished')
                ->name('teachers.finishedPracticeTest');
            Route::get('/licenses/{license}/courses/{course}/list-finished-test-table/{year?}/{grade?}/',
                'PracticeTestController@listFinishedTestTable')
                ->name('teachers.finishedPracticeTestTable');
            Route::get('/licenses/{license}/courses/{slug}/folder/{folder}/practice-test-template',
                'PracticeTestTemplateController@testTemplate')
                ->name('teachers.manage-practice-test-template');
            Route::get('/licenses/{license}/courses/{slug}/folder/{folder}/practice-test-template/{id}/edit',
                'PracticeTestTemplateController@editTestTemplate')
                ->name('teachers.editPracticeTestTemplate');
            Route::post('licenses/{license}/courses/{slug}/save/practice-test-template',
                'PracticeTestTemplateController@saveTestTemplate')
                ->name('teachers.savepracticeTestTemplate');
            Route::get('/users/list/licenses/{license}/courses/{courseSlug}/folders/{folder}/practice-test-template',
                'PracticeTestController@listPracticeTestTemplate')
                ->name('teachers.listPracticeTestTemplate');
            Route::get('/users/list/licenses/{license}/active-practice-test/folders/{folder}/{grade?}',
                'PracticeTestController@listActivePracticeTest')
                ->name('teachers.listActivePracticeTest');
            Route::get('/licenses/{license}/courses/{courseSlug}/questions/learning-goals/',
                'TeacherController@learningGoalQuestions')
                ->name('teachers.learningGoalQuestions');
            Route::post('/licenses/{license}/courses/{slug}/template-questions/create/folder',
                'PracticeTestController@createFolder')
                ->name('teachers.createFolder');
            Route::get('/licenses/{license}/courses/{courseSlug}/folder/{folder}/practice-tests/{grade?}',
                'PracticeTestController@folderPracticeTests')
                ->name('users.folder-practice-test');
            Route::post('/teachers/student-dashboard',
                'TeacherController@studentLogin')
                ->name('teachers.studentLogin');
            Route::get('/licenses/{license}/courses/{slug}/class-management',
                'ClassController@classManagement')
                ->name('teachers.classManagement');
            Route::post('/licenses/{license}/courses/{slug}/create-class/{id}',
                'ClassController@saveClass')
                ->name('teachers.saveClass');
            Route::post('/licenses/{license}/courses/{slug}/create-sub-class/{id}',
                'ClassController@saveSubClass')
                ->name('teachers.saveSubClass');
            Route::get('/licenses/{license}/courses/{slug}/grades/year/{year?}',
                'ClassController@gradesByYear')
                ->name('teachers.gradesByYear');
            Route::post('/licenses/{license}/courses/{slug}/update-students',
                'ClassController@updateStudents')
                ->name('teachers.updateStudents');
            Route::get('/licenses/{license}/courses/{slug}/activity-overview/grade/{grade}/subjects/{subject}/{viewType}/{sortOrder?}',
            'GradeController@getActivityOverview')
            ->name('grades.activityOverview');
            Route::post('/licenses/{license}/courses/{slug}/delete-class',
                'ClassController@deleteClass')
                ->name('teachers.deleteClass');
            Route::get('/licenses/{license}/courses/{slug}')
                ->name('teachers.defaultRoute');
            Route::get('/licenses/{license}/courses/{slug}/students-list/{courseId}/{year}/{class}',
                'ClassController@studentsList')
                ->name('teachers.studentsList');
            Route::get('/licenses/{license}/courses/{slug}/learning-goal/{id}/subject/{subjectId}/grade/{grade}/student/{studentId}/export-question-pdf',
                'LearningGoalController@exportQuestionPdf')
                ->name('teachers.exportQuestionPdf');
            Route::post('/licenses/{license}/courses/{slug}/updateClass',
                'ClassController@updateClass')
                ->name('teachers.updateClass');
            Route::get('/licenses/{license}/courses/{slug}/class-list/{year?}',
            'ClassController@classList')
            ->name('teachers.classList');
            Route::get('/licenses/{license}/courses/{slug}/attempts-overview/grade/{grade}/subjects/{subject}/{viewType}/{sortOrder?}',
            'GradeController@getAttemptsOverview')
            ->name('grades.attemptsOverview');
            Route::post('/licenses/{license}/courses/{slug}/practice-test-management/update/{id}/folder',
                'PracticeTestController@updateFolder')
                ->name('teachers.updateFolder');
            Route::post('/licenses/{license}/courses/{slug}/practice-test-management/delete/{id}/folder',
                'PracticeTestController@deleteFolder')
                ->name('teachers.deleteFolder');

            Route::group(['middleware' => ['studentLicense']], function () {
                Route::get('/students/licenses/{license}/courses/{courseSlug}/practice-tests/create',
                    'StudentPracticeTestController@create')->name('students.practiceTests.create');
                Route::post('/students/licenses/{license}/courses/{courseSlug}/practice-tests/store',
                    'StudentPracticeTestController@store')->name('students.practiceTests.store');
                Route::get('/students/licenses/{license}/subjects/{subjectSlug}/practice-tests',
                    'StudentPracticeTestController@index')->name('students.practiceTests.index');

                Route::group(['middleware' => ['authStudentPracticeTest']], function () {
                    Route::group(['middleware' => ['activeStudentPracticeTest']], function () {
                        Route::get('/students/licenses/{license}/practice-tests/{practiceTestSlug}/questions/{questionAnswerId?}',
                            'StudentPracticeTestController@test')->name('students.practiceTests.test');
                        Route::put('/students/licenses/{license}/practice-tests/{practiceTestSlug}/update-status',
                            'StudentPracticeTestController@updateStatus')
                            ->name('students.practiceTests.updateStatus');
                        Route::put('/students/licenses/{license}/practice-tests/{practiceTestSlug}/store-answer',
                            'StudentPracticeTestController@storeAnswer')
                            ->name('students.practiceTests.storeAnswer');
                        Route::put('/students/licenses/{license}/practice-tests/{practiceTestSlug}/submit',
                            'StudentPracticeTestController@submit')
                            ->name('students.practiceTests.submit');
                    });
                    Route::get('/students/licenses/{license}/practice-tests/{practiceTestSlug}/result',
                        'StudentPracticeTestController@result')
                        ->name('students.practiceTests.result');
                });

                Route::group(['middleware' => ['authStudentTask']], function () {
                    Route::post('/students/learninGoals/start',
                        'StudentController@startStudentLearningGoal')
                        ->name('students.learning-goals.start');
                    Route::post('/students/question-answers/check',
                        'StudentController@checkQuestionAnswer')
                        ->name('students.checkQuestionAnswer');
                    Route::get('/students/licenses/{license}/subjects/{subjectSlug}/lgs/{learningGoalSlug}/testQuestion/{questionAnswerId?}',
                        'StudentController@learningGoalTestQuestion')
                        ->name('students.learning-goals.learningGoalTestQuestion');

                    Route::group(['middleware' => ['studentProgress']], function () {
                        Route::get('/students/licenses/{license}/subjects/{subjectSlug}/learning-goals',
                            'StudentController@subjectLearningGoals')
                            ->name('students.subjects.learning-goals');
                        Route::get('/students/licenses/{license}/subjects/{subjectSlug}/learning-goals/{learningGoalSlug}',
                            'StudentController@showLearningGoal')
                            ->name('students.learning-goals.show');
                        Route::get('/students/licenses/{license}/subjects/{subjectSlug}/lgs/{learningGoalSlug}/introduction',
                            'StudentController@learningGoalTest')
                            ->name('students.learning-goals.introductionTest');
                        Route::get('/students/licenses/{license}/subjects/{subjectSlug}/lgs/{learningGoalSlug}/tests',
                            'StudentController@learningGoalTest')
                            ->name('students.learning-goals.test');

                        Route::get('/students/licenses/{license}/subjects/{subjectSlug}/learning-goals/{learningGoalSlug}/export-theory',
                            'LearningGoalController@exportTheory')
                            ->name('students.exportTheory');
                    });
                });
            });

            Route::group(['middleware' => ['teacherLicense']], function () {
                Route::get('/teachers/licenses/{license}/practice-tests/{practiceTestSlug}/overview',
                    'TeacherController@practiceTestOverview')->name('teachers.practiceTests.overview');
                Route::post('/teachers/licenses/{license}/practice-tests/{practiceTestSlug}/feedbackAvailable',
                    'TeacherController@practiceTestFeedbackAvailable')->name('teachers.practiceTests.feedbackAvailable');
            });
        });
    });
});
