<?php

namespace App\Listeners;

use App\Events\DeactivateUserMollieSubsctriptionEvent;
use Log;

class DeactivateUserMollieSubsctriptionEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  UpdateStudentLearningGoalEvent  $event
     * @return void
     */
    public function handle(DeactivateUserMollieSubsctriptionEvent $event)
    {
        Log::info($event->mollieSubscription->mollie_subscription_id .
            " and " . $event->mollieSubscription->mollie_customer_unique_id);

        $customer = $event->mollie->customers->get(
            $event->mollieSubscription->mollie_customer_unique_id);

        $subscription = $customer->getSubscription(
            $event->mollieSubscription->mollie_subscription_id);

        if ($subscription->status === config('constants.ACTIVE')) {
            $canceledSusbscription = $customer->cancelSubscription(
                $event->mollieSubscription->mollie_subscription_id);
            Log::info("Canceled status: " .
                $canceledSusbscription->status);
            $event->mollieSubscription->updateSubscription(
                ['status' => config('constants.INACTIVE')]);
        }
    }
}
