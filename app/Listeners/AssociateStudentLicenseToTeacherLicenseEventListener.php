<?php

namespace App\Listeners;

use App\Events\AssociateStudentLicenseToTeacherLicenseEvent;
use App\Repositories\UserRepository;

class AssociateStudentLicenseToTeacherLicenseEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssociateStudentLicenseToTeacherLicenseEvent  $event
     * @return void
     */
    public function handle(AssociateStudentLicenseToTeacherLicenseEvent $event)
    {
        UserRepository::studentLogin($event->user, $event->license);
    }
}
