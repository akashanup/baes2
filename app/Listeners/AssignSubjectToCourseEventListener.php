<?php

namespace App\Listeners;

use App\Events\AssignSubjectToCourseEvent;
use App\Models\Course;
use App\Models\License;
use Carbon\Carbon;
use DB;

class AssignSubjectToCourseEventListener
{
    /**
     * Handle the event.
     *
     * @param  AssignSubjectToCourseEvent  $event
     * @return void
     */
    public function handle(AssignSubjectToCourseEvent $event)
    {
        switch ($event->type) {
            case config('constants.CREATE_REQUEST'):
                $subjectIds = [];
                foreach ($event->subjects as $subject) {
                    $subjectIds[$subject['subjectId']] = ['order' => $subject['order']];
                }
                $event->course->subjects()->sync($subjectIds);
                break;
            case config('constants.UPDATE_REQUEST'):
                $currectSubjects = $event->course->subjects;
                $subjectIds = [];
                foreach ($event->subjects as $subject) {
                    $subjectIds[$subject['subjectId']] = ['order' => $subject['order']];
                }
                $event->course->subjects()->sync($subjectIds);
                //Modify student learning goals with the updated course
                $course = Course::select('id')->find($event->course->id);
                $modifiedSubjects = $course->subjects;
                $removedSubjects = $currectSubjects->diff($modifiedSubjects);
                $addedSubjects = $modifiedSubjects->diff($currectSubjects);
                // Find all the student licenses for tha course
                $studentLicenses = License::where('course_id', $course->id)
                    ->where('role', config('constants.STUDENT'))->get();
                // Remove data from student learning goals for removed subjects for the course
                DB::Table('student_learning_goals')->whereIn('license_id',
                    $studentLicenses->pluck('id')->toArray())->whereIn('subject_id',
                    $removedSubjects->pluck('id')->toArray())->delete();
                // Add data in student learning goals for added subjects for the course
                $addSubjectLearningGoals = [];
                $key = 0;
                foreach ($studentLicenses as $license) {
                    foreach ($license->users as $user) {
                        foreach ($addedSubjects as $subject) {
                            foreach ($subject->learningGoals as $learningGoal) {
                                $addSubjectLearningGoals[$key++] = [
                                    'user_id' => $user->id,
                                    'license_id' => $license->id,
                                    'subject_id' => $subject->id,
                                    'learning_goal_id' => $learningGoal->id,
                                    'order' => $learningGoal->pivot->order,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now()];
                            }
                        }
                    }
                }
                DB::Table('student_learning_goals')->insert($addSubjectLearningGoals);
                break;
            default:
                # code...
                break;
        }
    }
}
