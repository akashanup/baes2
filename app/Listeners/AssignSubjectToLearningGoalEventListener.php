<?php

namespace App\Listeners;

use App\Events\AssignSubjectToLearningGoalEvent;
use App\Models\Subject;
use DB;

class AssignSubjectToLearningGoalEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignSubjectToLearningGoalEvent  $event
     * @return void
     */
    public function handle(AssignSubjectToLearningGoalEvent $event)
    {
        $curriculumIds = [];
        foreach ($event->curriculumIds as $key => $curriculumId) {
            $curriculumIds[$curriculumId] = ['order' => ($key + 1)];
        }
        DB::table('subject_learning_goals')->join('subjects',
            'subject_learning_goals.subject_id', 'subjects.id')
            ->where('subjects.type', Subject::CURRICULUM)
            ->where('subject_learning_goals.learning_goal_id', $event->learningGoal->id)
            ->delete();
        $event->learningGoal->subjects()->where(
            'type', Subject::CURRICULUM)->attach($curriculumIds);
    }
}
