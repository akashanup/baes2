<?php

namespace App\Listeners;

use App\Events\AssignLearningGoalToSubjectEvent;

class AssignLearningGoalToSubjectEventListener
{
    /**
     * Handle the event.
     *
     * @param  AssignLearningGoalToSubjectEvent  $event
     * @return void
     */
    public function handle(AssignLearningGoalToSubjectEvent $event)
    {
        $learningGoalIds = [];
        foreach ($event->learningGoalIds as $key => $learningGoalId) {
            $learningGoalIds[$learningGoalId] = ['order' => ($key + 1)];
        }
        $event->subject->learningGoals()->sync($learningGoalIds);
    }
}
