<?php

namespace App\Listeners;

use App\Events\AssignPracticeTestToStudentsEvent;
use App\Repositories\UserRepository;

class AssignPracticeTestToStudentsEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignPracticeTestToStudentsEvent  $event
     * @return void
     */
    public function handle(AssignPracticeTestToStudentsEvent $event)
    {
        $practiceTestDetail = $event->practiceTestDetail;
        $license = $event->license;
        $students = UserRepository::institutionCourseGradeStudents(
            $license->institution_id, $license->course_id, $event->gradeId)
            ->get();
        foreach ($students as $student) {
            $student->addPracticeTestStatuses($practiceTestDetail);
            $student->addPracticeTestQuestions($practiceTestDetail);
        }
    }
}
