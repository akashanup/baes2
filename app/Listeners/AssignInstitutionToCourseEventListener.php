<?php

namespace App\Listeners;

use App\Events\AssignInstitutionToCourseEvent;
use App\Models\Course;
use App\Models\License;

class AssignInstitutionToCourseEventListener
{
    /**
     * Handle the event.
     *
     * @param  AssignInstitutionToCourseEvent  $event
     * @return void
     */
    public function handle(AssignInstitutionToCourseEvent $event)
    {
        $event->course->institutions()->sync($event->institutionIds);
        // Remove licenses for the deleted institutions for this course
        $course = Course::select('id')->find($event->course->id);
        License::where('course_id', $course->id)
            ->whereNotIn('institution_id', $course->institutions()
                    ->pluck('id')->toArray())->delete();
    }
}
