<?php

namespace App\Listeners;

use App\Events\AssignQuestionAnswerToLearningGoalTestEvent;
use App\Models\LearningGoalTest;

class AssignQuestionAnswerToLearningGoalTestEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignQuestionAnswerToLearningGoalTestEvent  $event
     * @return void
     */
    public function handle(AssignQuestionAnswerToLearningGoalTestEvent $event)
    {
        $learningGoalTest = LearningGoalTest::findOrFail(
            $event->learningGoalTestId)->questionAnswers()->attach(
            $event->questionAnswerId);
    }
}
