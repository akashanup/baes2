<?php

namespace App\Listeners;

use App\Events\AddActivityEvent;
use App\Repositories\UserRepository;

class AddActivityEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddActivityEvent  $event
     * @return void
     */
    public function handle(AddActivityEvent $event)
    {
        UserRepository::addStudentActivity($event->user, $event->licenseId,
            $event->subjectId, $event->learningGoalId);
    }
}
