<?php

namespace App\Listeners;

use App\Events\CreateLearningGoalTestEvent;
use App\Models\LearningGoalTest;

class CreateLearningGoalTestEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateLearningGoalTestEvent  $event
     * @return void
     */
    public function handle(CreateLearningGoalTestEvent $event)
    {
        $learningGoalTest = LearningGoalTest::create();
        // Assign the created test to the student learning goal.
        $learningGoalTest->assignStudentLearningGoal(
            $event->studentLearningGoal);
    }
}
