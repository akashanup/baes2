<?php

namespace App\Listeners;

use App\Events\UpdateStudentLearningGoalEvent;

class UpdateStudentLearningGoalEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateStudentLearningGoalEvent  $event
     * @return void
     */
    public function handle(UpdateStudentLearningGoalEvent $event)
    {
        $event->subject->updateStudentLearningGoals();
    }
}
