<?php

namespace App\Listeners;

use App\Events\AssignLearningGoalToUserEvent;
use App\Repositories\UserRepository;

class AssignLearningGoalToUserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AssignLearningGoalToUserEvent $event)
    {
        UserRepository::attachLearningGoalToStudent($event->user, $event->license);
    }
}
