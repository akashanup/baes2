<?php

namespace App\Listeners;

use App\Events\AssignPermissionToSuperAdminEvent;
use App\Models\Role;

class AssignPermissionToSuperAdminEventListener
{
    /**
     * Handle the event.
     *
     * @param  AssignPermissionToSuperAdminEvent  $event
     * @return void
     */
    public function handle(AssignPermissionToSuperAdminEvent $event)
    {
        $superAdminRole = Role::where('name', config('constants.SUPER_ADMIN'))->first();
        $superAdminRole->permissions()->attach($event->permission->id);
    }
}
