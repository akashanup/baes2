<?php

namespace App\Listeners;

use App\Events\CreateLicenseDefaultFolderEvent;
use App\Models\PracticeTestFolder;
use Log;

class CreateLicenseDefaultFolderEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateLicenseDefaultFolderEvent  $event
     * @return void
     */
    public function handle(CreateLicenseDefaultFolderEvent $event)
    {
        $data = [
            'name' => config('constants.FOLDER_NAME'),
            'description' => config('constants.DESCRIPTION_NAME'),
            'license_id' => $event->licenseId,
            'user_id' => $event->userId,
        ];
        Log::info("In event listener");
        PracticeTestFolder::saveFolder($data);
    }
}
