<?php

namespace App\Listeners;

use App\Events\AssignActivePracticeTestsToUserEvent;
use App\Models\PracticeTestDetail;

class AssignActivePracticeTestsToUserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignActivePracticeTestsToUserEvent  $event
     * @return void
     */
    public function handle(AssignActivePracticeTestsToUserEvent $event)
    {
        $activePracticeTests = PracticeTestDetail::activePracticeTestsForLicense(
            $event->license, $event->gradeId);
        $user = $event->user;
        foreach ($activePracticeTests as $practiceTest) {
            $user->addPracticeTestStatuses($practiceTest);
            $user->addPracticeTestQuestions($practiceTest);
        }
    }
}
