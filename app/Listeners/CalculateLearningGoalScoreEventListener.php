<?php

namespace App\Listeners;

use App\Events\CalculateLearningGoalScoreEvent;
use App\Models\LearningGoalTest;
use App\Repositories\UserRepository;

class CalculateLearningGoalScoreEventListener
{
    const MINIMUM_BAES_SCORE = 0.0;
    const MAXIMUM_BAES_SCORE = 10.0;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateLearningGoalScoreEvent  $event
     * @return void
     */
    public function handle(CalculateLearningGoalScoreEvent $event)
    {
        $studentLearningGoal = $event->studentLearningGoal;
        $studentLearningGoalTestLastTwoQuestions = LearningGoalTest::findOrFail(
            $studentLearningGoal->learning_goal_test_id)->questionAnswers()
            ->whereNotNull('learning_goal_test_question_answers.answer')
            ->orderBy('learning_goal_test_question_answers.id', 'desc')
            ->limit(2)->select('learning_goal_test_question_answers.points')
            ->get();
        $current = $last = null;
        if (sizeof($studentLearningGoalTestLastTwoQuestions) == 2) {
            $current = $studentLearningGoalTestLastTwoQuestions->first();
            $last = $studentLearningGoalTestLastTwoQuestions->last();
        } else {
            $current = $studentLearningGoalTestLastTwoQuestions->first();
        }
        if ($current) {
            $currentPoint = $current->points;
            $lastPoint = $last ? $last->points : 0;

            $currentScore = $studentLearningGoal->current_score;
            $change = $studentLearningGoal->change;
            if ($currentPoint != $lastPoint) {
                $delta = ($currentPoint - $currentScore);
                $change = round(($delta / config('constants.BAES_FACTOR')), 1);
            }
            $currentScore = round(($currentScore + $change), 1);

            // Baes score doesn't exceeds 10.0
            $currentScore = $currentScore > static::MAXIMUM_BAES_SCORE
            ? static::MAXIMUM_BAES_SCORE : $currentScore;
            // Baes score doesn't go beneath 0.0
            $currentScore = $currentScore < static::MINIMUM_BAES_SCORE
            ? static::MINIMUM_BAES_SCORE : $currentScore;
            if ((($change > 0 && $currentScore <= $currentPoint)
                || ($change < 0 && $currentScore >= $currentPoint))
                && ($current->pivot->score_calculated === config('constants.NO'))
            ) {
                $current->pivot->score_calculated = config('constants.YES');
                $current->pivot->save();
                UserRepository::updateStudentLearningGoalCurrentScore(
                    auth()->user()->id, $studentLearningGoal->license_id,
                    $studentLearningGoal->subject_id,
                    $studentLearningGoal->learning_goal_id, $currentScore, $change);
            }
        }
    }
}
