<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\ExportDataNotification;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Lang;
use Maatwebsite\Excel\Facades\Excel;

class ExportTableJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var integer
     */
    public $tries = 1;

    /**
     * Array
     *
     * @var array
     */
    protected $tables;

    /**
     * The id of an institution.
     *
     * @var integer
     */
    public $institutionId;

    /**
     * User model
     *
     * @var App\Models\User
     */
    protected $user;

    const FILE_FORMAT = 'xlsx';
    const MAXIMUM_ROWS = 1000;
    const EXPORT_LOCATION = 'exports/';
    const STUDENT_ACTIVITY = 'student_activities';
    const LEARNING_GOAL_TEST_QUESTION_ANSWERS = 'learning_goal_test_question_answers';
    const STUDENT_PERFORMANCE = 'student_performance';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $tables, $institutionId = null)
    {
        $this->user = $user;
        $this->tables = $tables;
        $this->institutionId = $institutionId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileName = str_slug(Lang::get('admins.export_data_1') . '_' . Carbon::now(), '-');
        $tables = $this->tables;
        $institutionId = $this->institutionId;
        Excel::create($fileName, function ($excel) use ($tables, $institutionId) {
            foreach ($tables as $key => $exportTable) {
                //Sheetname can't have more than 31 chars
                $sheetName = $exportTable;
                if ($exportTable === static::LEARNING_GOAL_TEST_QUESTION_ANSWERS) {
                    $sheetName = static::STUDENT_PERFORMANCE;
                }
                $excel->sheet($sheetName,
                    function ($sheet) use ($exportTable, $institutionId) {
                        $table = DB::Table($exportTable);
                        if ($institutionId) {
                            $table = static::specialData($institutionId, $exportTable);
                        }
                        $rows = $table->limit(static::MAXIMUM_ROWS)->get();
                        if ($rows->count()) {
                            //Header row
                            $sheet->rows([array_keys(json_decode(json_encode($rows->first()), true))]);
                            $rowsCount = 0;
                            while ($rows->count()) {
                                $rowsCount = $rowsCount + $rows->count();
                                $sheet->rows(json_decode(json_encode($rows->toArray()), true));
                                $rows = $table->offset($rowsCount)->limit(static::MAXIMUM_ROWS)->get();
                            }
                        } else {
                            $sheet->rows([Lang::get('admins.empty_table')], true);
                        }
                    }
                );
            }
        })->store(static::FILE_FORMAT);

        //Move file to AWS S3
        $completeFileName = $fileName . '.' . static::FILE_FORMAT;
        $filePath = storage_path('exports') . '/' . $completeFileName;
        $completeFilePath = static::EXPORT_LOCATION . $completeFileName;
        Storage::put($completeFilePath, file_get_contents($filePath),
            config('constants.S3_IMAGE_VISIBLE'));
        // Delete the file
        unlink($filePath);
        // Send email to download the exported tables
        $this->user->notify(new ExportDataNotification(Storage::url($completeFilePath),
            $this->user->name(), $this->tables));
    }

    protected static function specialData($institutionId, $exportTable)
    {
        $table = null;
        switch ($exportTable) {
            case static::LEARNING_GOAL_TEST_QUESTION_ANSWERS:
                $table = DB::Table($exportTable)->join('student_learning_goals',
                    'learning_goal_test_question_answers.learning_goal_test_id',
                    'student_learning_goals.learning_goal_test_id')->join('question_answers',
                    'learning_goal_test_question_answers.question_answer_id',
                    'question_answers.id')->join('question_templates',
                    'question_answers.question_template_id', 'question_templates.id')
                    ->join('user_licenses', function ($join) {
                        $join->on('student_learning_goals.user_id', 'user_licenses.user_id')
                            ->where('user_licenses.is_associated', config('constants.NO'));
                    })
                    ->join('licenses', 'user_licenses.license_id', 'licenses.id')
                    ->join('users', 'student_learning_goals.user_id', 'users.id')
                    ->where('licenses.institution_id', $institutionId)
                    ->where('licenses.role', config('constants.STUDENT'))
                    ->orderBy('users.id')
                    ->select('users.id as UserId', 'users.first_name as FirstName',
                        'users.last_name as LastName', 'users.email as Email',
                        'student_learning_goals.learning_goal_id as LearningGoalId',
                        'question_templates.id as QuestionTemplatesId',
                        'question_answers.id as QuestionId',
                        'question_templates.complexity as QuestionDifficulty',
                        'question_answers.question as Question',
                        'question_answers.answer as CorrectAnswer',
                        DB::raw("learning_goal_test_question_answers.answer->>'$.\"1\"' as Answer1"),
                        DB::raw("learning_goal_test_question_answers.answer->>'$.\"2\"' as Answer2"),
                        DB::raw("learning_goal_test_question_answers.answer->>'$.\"3\"' as Answer3"),
                        DB::raw("learning_goal_test_question_answers.answer->>'$.\"4\"' as Answer4")
                    );
                break;
            case static::STUDENT_ACTIVITY:
                $table = DB::Table($exportTable)->join('user_licenses', function ($join) {
                    $join->on('student_activities.user_id', 'user_licenses.user_id')
                        ->where('user_licenses.is_associated', config('constants.NO'));
                })->join('licenses', 'user_licenses.license_id', 'licenses.id')
                    ->join('users', 'student_activities.user_id', 'users.id')
                    ->where('licenses.institution_id', $institutionId)
                    ->where('licenses.role', config('constants.STUDENT'))
                    ->select('users.id as UserId', 'users.first_name as FirstName',
                        'users.last_name as LastName', 'users.email as Email',
                        DB::raw('sum(student_activities.points) as Activity'),
                        'student_activities.date as Date')->groupBy('student_activities.date')
                    ->groupBy('users.id')->orderBy('users.id')->orderBy('student_activities.date');
                break;
            default:
                # code...
                break;
        }

        return $table;
    }
}
