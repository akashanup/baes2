<?php

namespace App\Jobs;

use App\Notifications\PdfDownloadMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use PDF;

class ExportTheoryPdf implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $learningGoal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($learningGoal)
    {
        $this->learningGoal = $learningGoal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("in handle");
        $pdf = PDF::loadView(
            'learning-goals.export-theory',
            [
                'learningGoal' => $this->learningGoal,
            ]
        )
            ->setOptions(['debugCss' => false]);
        $time = time();
        Log::info($time);
        $fileName = $this->learningGoal->slug . '-' . $time . '.pdf';
        Log::info($fileName);
        $output = $pdf->download($fileName);
        Storage::put('images/' . $fileName, $output->__toString(), config('constants.S3_IMAGE_VISIBLE'));
        $fileLink = Storage::url('images/' . $fileName);
        $this->user->notify(new PdfDownloadMail($fileLink));
    }
}
