<?php

namespace App\Jobs;

use App\Notifications\PdfDownloadMail;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Log;
use PDF;

class GenerateLearningGoalQuestion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var integer
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * Array of learning Goal's Questions.
     *
     * @return void
     */

    public $templateData;

    /**
     * Learning Goal Name.
     *
     * @return void
     */

    public $learningGoalName;

    /**
     * Question Type
     *
     * @return void
     */

    public $type;

    /**
     * Array of user Details
     *
     * @return void
     */

    public $user;

    /**
     * Initialized values in constructor.
     * @param array $templateData, string $learningGoalName
     * @param string $type, array $user
     * @return void
     */
    public function __construct($templateData, $learningGoalName, $type, $user)
    {
        $this->templateData = $templateData;
        $this->learningGoalName = $learningGoalName;
        $this->type = $type;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->type === config('constants.MCQ_TYPE')) {
            $pdf = PDF::loadView(
                'templates.mcq-question-pdf',
                [
                    'mcqQuestionTemplates' => $this->templateData,
                    'learningGoalName' => $this->learningGoalName,
                ]
            );
        } elseif ($this->type === config('constants.DIRECT_TYPE')) {
            try {
                $pdf = PDF::loadView(
                    'templates.direct-question-pdf',
                    [
                        'questionTemplateData' => $this->templateData,
                        'learningGoalName' => $this->learningGoalName,
                    ]
                );
            } catch (Exception $e) {
                Log:info($e);
            }
        }

        $pdfName = $this->type . '-' . $this->learningGoalName . time() . '.pdf';
        $output = $pdf->download($pdfName);
        Storage::put('images/' . $pdfName, $output->__toString(), config('constants.S3_IMAGE_VISIBLE'));
        $fileLink = Storage::url('images/' . $pdfName);
        $this->user->notify(new PdfDownloadMail($fileLink));
    }
}
