<?php

namespace App\Jobs;

use App\Jobs\GenerateQuestions;
use App\Mail\VariableSetImportException;
use App\Models\LearningGoal;
use App\Models\QuestionAnswer;
use App\Models\TemplateVariable;
use App\Models\User;
use App\Notifications\VariableSetNotification;
use App\Services\TemplateUploadService;
use App\Services\TemplateUtilityService;
use DB;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Lang;
use Log;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

class ImportVariableSet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var integer
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * App\Model LearningGoal
     *
     * @var App\Model
     */
    protected $learningGoal;

    /**
     * File path
     *
     * @var string
     */
    protected $filePath;

    /**
     * File extension
     *
     * @var string
     */
    protected $fileExtension;

    /**
     * User Model
     *
     * @var App\Model
     */
    protected $user;

    /**
     * Error
     *
     * @var Strig
     */
    protected $errorMessage = null;

    /**
     * Exception
     *
     * @var Strig
     */

    /**
     * Exception
     *
     * @var Strig
     */
    protected $exceptionMessage = null;

    const YES = 'yes';
    const NO = 'no';
    const ODS = 'ods';
    const SUCCESS = 'success';
    const FAILED = 'failed';
    const INACTIVE = 'inactive';

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($learningGoal, $filePath, $fileExtension, $user_id)
    {
        $this->learningGoal = LearningGoal::findBySlug($learningGoal)->first();

        $this->filePath = $filePath;
        $this->fileExtension = $fileExtension;
        $this->user = User::find($user_id);
        $this->errorMessage = Lang::get('admins.failed_variable_set_upload');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = null;
        DB::beginTransaction();
        try {
            if (Storage::exists($this->filePath)) {
                Storage::disk('exports')->put($this->filePath, Storage::get($this->filePath));
                $file = storage_path('exports') . '/' . $this->filePath;

                // Reading Excel file
                $variable_sets = Excel::load($file)->toArray();
                // check the file extension and parse it accordingly
                if ($this->fileExtension == static::ODS) {
                    $variable_sets = $variable_sets[0];
                }
                // Total Variables/rows present
                $variable_count = count($variable_sets);
                // Checking if question variable colummn name are as per documentation
                $header_question_variable = [
                    'qv_id',
                    'qt_id',
                    'qv_group',
                    'qv_variable',
                    'qv_value',
                    'value_type',
                    'rounding_value',
                ];

                if (empty(array_diff(array_keys($variable_sets[0]), $header_question_variable))) {
                    // Sort variable set based on qt_id and then qv_group
                    $arr = [];
                    $qt_ids = array_unique(array_column($variable_sets, 'qt_id', 'qt_id'));
                    sort($qt_ids);
                    foreach ($qt_ids as $key => $qt_id) {
                        $intermediateArray = array_filter(
                            $variable_sets,
                            function ($i) use ($qt_id) {
                                return $i['qt_id'] == $qt_id;
                            }
                        );
                        usort(
                            $intermediateArray,
                            function ($a, $b) {
                                return $a['qv_group'] - $b['qv_group'];
                            }
                        );
                        $arr = array_merge($arr, $intermediateArray);
                    }
                    $variable_sets = $arr;

                    // Counting total number of group present
                    $total_group = 0;
                    $qv_groups = [];
                    $qv_variables = [];
                    $qv_variable = null;
                    foreach ($variable_sets as $key => $variable_set) {
                        $thisArray = [
                            $variable_set['qt_id'],
                            $variable_set['qv_group'],
                        ];
                        if (!in_array($thisArray, $qv_groups)) {
                            $qv_variables[$total_group - 1] = $qv_variable;
                            $qv_variable = [];
                            $qv_groups[] = $thisArray;
                            $total_group++;
                        }
                        $qv_variable[] = $variable_set['qv_variable'];
                    }
                    $qv_variables[$total_group - 1] = $qv_variable;
                    $questionTemplateIds = $this->learningGoal->questionTemplates()
                        ->where('status', config('constants.ACTIVE'))
                        ->where('type', QuestionAnswer::INPUT_TYPE)
                        ->pluck('id')->toArray();

                    $templateVariables = TemplateVariable::where('status_edit', static::NO)
                        ->whereIn('question_template_id', $questionTemplateIds)
                        ->orderBy('question_template_id')->get();

                    $questionAnswers = QuestionAnswer::where('status', config('constants.ACTIVE'))
                        ->whereIn('question_template_id', $questionTemplateIds)
                        ->update(['status' => static::INACTIVE]);
                    if ($templateVariables->count() == $total_group) {
                        $current_line = 0;
                        foreach ($templateVariables as $key => $templateVariable) {
                            $updateVariableSet = TemplateVariable::where('id', $templateVariable->id)
                                ->update(['status_edit' => config('constants.YES')]);
                            $values = json_decode($templateVariable->values, true);
                            $next_group_line = $current_line + sizeof($values);
                            $variable_array = [];
                            Log::info("Debugg");
                            Log::info($qv_variables[$key]);
                            $array_diff = array_diff_key($values, array_flip($qv_variables[$key]));
                            if ($templateVariable->question_template_id == $variable_sets[$current_line]['qt_id']
                                && !sizeof($array_diff)
                            ) {
                                $this->errorMessage = false;
                                while ($current_line < $next_group_line
                                    && $variable_sets[$current_line]['qt_id'] == $templateVariable->question_template_id
                                ) {
                                    // Adding value type symbol to values
                                    if ($variable_sets[$current_line]['value_type'] == TemplateUploadService::TYPE_CURRENCY) {
                                        // Adding Euro sign before the value
                                        $value = TemplateUtilityService::returnSymbol() .
                                        number_format($variable_sets[$current_line]['qv_value'],
                                            $variable_sets[$current_line]['rounding_value'], ',', '.');
                                    } elseif ($variable_sets[$current_line]['value_type'] == TemplateUploadService::TYPE_PERCENTAGE) {
                                        $value = number_format(($variable_sets[$current_line]['qv_value'] * 100), $variable_sets[$current_line]['rounding_value'], ',', '.') . '\\%';
                                    } elseif ($variable_sets[$current_line]['value_type'] == TemplateUploadService::TYPE_NUMBER) {
                                        if ($variable_sets[$current_line]['qv_variable'] == TemplateUploadService::ANSWER) {
                                            $value = round($variable_sets[$current_line]['qv_value'], $variable_sets[$current_line]['rounding_value']);
                                        } else {
                                            $value = number_format($variable_sets[$current_line]['qv_value'],
                                                $variable_sets[$current_line]['rounding_value'], ',', '.');
                                        }
                                    } else {
                                        $value = $variable_sets[$current_line]['qv_value'];
                                    }

                                    // Storing answer in variables
                                    if ($variable_sets[$current_line]['qv_variable'] == TemplateUploadService::ANSWER) {
                                        if ($variable_sets[$current_line]['value_type'] == TemplateUploadService::TYPE_CURRENCY) {
                                            $correct_answer = round($variable_sets[$current_line]['qv_value'],
                                                $variable_sets[$current_line]['rounding_value']);
                                        } elseif ($variable_sets[$current_line]['value_type'] == TemplateUploadService::TYPE_PERCENTAGE) {
                                            $correct_answer = number_format(($variable_sets[$current_line]['qv_value'] * 100),
                                                $variable_sets[$current_line]['rounding_value'], '.', ',') . '%';
                                        } else {
                                            $correct_answer = $value;
                                        }
                                    }

                                    // Storing variable into array in key value form : key is variable name in sheet and value is value given in sheet to that variable
                                    $variable_array[trim($variable_sets[$current_line]['qv_variable'])] = $value;
                                    $current_line++;
                                } // end while

                                // Inserting All variable,values and answer into db
                                $insert_into_qvDB = new TemplateVariable();
                                $insert_into_qvDB->values = json_encode($variable_array);
                                $insert_into_qvDB->correct_answer = $correct_answer;
                                $insert_into_qvDB->question_template_id = $templateVariable->question_template_id;
                                $insert_into_qvDB->save();
                            } else {
                                if (sizeof($array_diff)) {
                                    // Error message - For the template id #### there are variables mismatch which are as follows.
                                    $this->errorMessage = '[' . implode(',', array_flip($array_diff)) . ']' . Lang::get('admins.variable_mismatch') . $templateVariable->question_template_id;
                                } else {
                                    // Error message - The order of variables is not correct.
                                    $this->errorMessage = Lang::get('admins.variable_sequence_mismatch') . $templateVariable->question_template_id;
                                }
                                break;
                            } // end if
                        } // end foreach
                        if (!$this->errorMessage) {
                            DB::commit();
                            app(Dispatcher::class)->dispatch(new GenerateQuestions());
                            $this->user->notify(new VariableSetNotification($this->learningGoal->name,
                                $this->user->first_name,
                                Lang::get('admins.variables_uploaded_successfully'),
                                static::SUCCESS, $file)
                            );
                        }
                    } else {
                        $this->errorMessage = Lang::get('admins.variable_count_mismatch');
                    }
                } else {
                    $this->errorMessage = Lang::get('admins.invalid_columns');
                }
            } else {
                $this->errorMessage = Lang::get('admins.file_not_found');
            }
        } catch (Exception $e) {
            $this->exceptionMessage = $e;
        } finally {
            if (($this->errorMessage) || ($this->exceptionMessage)) {
                DB::rollback();
                if ($this->errorMessage) {
                    $this->user->notify(new VariableSetNotification(
                        $this->learningGoal->name, $this->user->first_name, $this->errorMessage, static::FAILED, $file)
                    );
                }
                if ($this->exceptionMessage) {
                    $developers = json_decode(config('app.developers'), true);
                    Mail::to(current($developers))->cc($developers)
                        ->send(new VariableSetImportException($this->learningGoal->name, $this->exceptionMessage, $file)
                        );
                }
            }
            Storage::delete($this->filePath);
            Storage::disk('exports')->delete($this->filePath);
        } //end try
    }
}
