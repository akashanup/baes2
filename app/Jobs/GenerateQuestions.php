<?php

namespace App\Jobs;

use App\Models\Hint;
use App\Models\QuestionAnswer;
use App\Models\QuestionTemplate;
use App\Models\TemplateVariable;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class GenerateQuestions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUOTE = '<p>&quot;</p>';

    /**
     * Execute the job for Generating questions.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("Job started");
        DB::beginTransaction();
        $templateVariables = TemplateVariable::where('status', TemplateVariable::PENDING_STATUS)->get();
        foreach ($templateVariables as $key => $templateVariable) {
            $questionTemplate = $templateVariable->questionTemplate;
            $questionType = $questionTemplate->type;
            $questionAnswer = [];
            switch ($questionType) {
                case QuestionAnswer::INPUT_TYPE:
                    $question = QuestionAnswer::generateQuestion($templateVariable);
                    $questionFeedbacks = $questionTemplate->questionFeedbacks()->get();
                    // hints
                    $hint = Hint::generateHint($questionFeedbacks, $templateVariable);
                    break;
                case QuestionAnswer::MCQ_TYPE:
                    $question = $templateVariable->questionTemplate->template;
                    $questionAnswer['option'] = $templateVariable->values;
                    // hints
                    $hint = null;
                    break;
            }
            // Removing the extra spaces at the start and the end in question text.
            $question = str_replace(static::QUOTE, '', $question);
            $question = trim($question);
            $questionAnswer['question'] = $question;
            $questionAnswer['answer'] = $templateVariable->correct_answer;
            $questionAnswer['status'] = config('constants.ACTIVE');
            $questionAnswer['template_variable_id'] = $templateVariable->id;
            $questionAnswer['upper_bound'] = $templateVariable->upper_bound;
            $questionAnswer['lower_bound'] = $templateVariable->lower_bound;

            $questionAnswer = $questionTemplate->addQuestionAnswer($questionAnswer);
            // Removing the extra spaces at the start and the end in hints.
            $hint = str_replace(static::QUOTE, '', $hint);
            $hint = trim($hint);
            $hint = $questionAnswer->addHint($hint);
            $templateVariable->update(['status' => TemplateVariable::FINISHED_STATUS]);
        }
        DB::commit();
        Log::info("Job finished");
    }
}
