<?php

namespace App\Jobs;

use App\Models\QuestionAnswer;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class ChangeQuestionAnswerStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var integer
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    public $questionData;

    public $templateData;

    public $inActiveTemplateQuestion;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($questionData, $templateData = null, $inActiveTemplateQuestion = null)
    {
        $this->questionData = $questionData;
        $this->templateData = $templateData;
        $this->inActiveTemplateQuestion = $inActiveTemplateQuestion;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();
        if (count($this->inActiveTemplateQuestion)) {
            //Inactive specific QT and QV questions
            Log::info("in active temp");
            foreach ($this->inActiveTemplateQuestion as $inactiveValue) {
                QuestionAnswer::updateVariableQuestions($inactiveValue['questionTemplate'], '');
            }
        } elseif (count($this->templateData)) {
            Log::info("In templateData");
            foreach ($this->templateData as $templateData) {
                QuestionAnswer::updateVariableQuestions(
                    $templateData['questionTemplate'], $templateVariable = null);
            }
        }
        DB::commit();
    }
}
