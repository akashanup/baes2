<?php

namespace App\Models;

use App\Models\Institution;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'year', 'parent_id',
        'institution_id'];

    /**
     * Get the institution for the grade.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function institution()
    {
        return $this->belongsTo(Institution::class)
            ->select('institutions.id', 'institutions.name');
    }

    /**
     * Get all the grades.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopegetGrades($query)
    {
        return $query->addSelect(['grades.id', 'grades.name',
            'grades.slug', 'grades.year']);
    }

    /**
     * Get all the grades by institution ID.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getInstitutionGrades($institutionId)
    {
        return static::getGrades()
            ->where('grades.institution_id', $institutionId)
            ->get();
    }

    /**
     * Scope to get Grade by slug
     *
     * @param App\Model\Grade $query
     * @param string $slug
     * @return App\Models\Grade
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('grades.slug', $slug);
    }
    /**
     * Create grade for Institution
     *
     * @param App\Model\Institution $institution
     * @param array $data
     * @return App\Models\Grade
     */
    public static function createGrade($institution, $data)
    {
        return $institution->grades()->create($data);
    }

    /**
     * Get all the grades by institution and year.
     * @param integer institutionId
     * @param integer $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getGradesByYear($institutionId, $year)
    {
        return static::getGrades()
            ->where('grades.institution_id', $institutionId)
            ->where('grades.year', $year)
            ->get();
    }

    /**
     * Get all the grades by institution and year.
     * @param integer institutionId
     * @param integer $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function deleteGrade($grade, $year)
    {
        return static::where('grades.slug', $grade)
            ->where('grades.year', $year)
            ->first()
            ->delete();
    }

    /**
     * Get grade by slug and year
     * @param string $slug
     * @param integer $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getGradesBySlugYear($slug, $year)
    {
        return static::getGrades()
            ->where('grades.slug', $slug)
            ->where('grades.year', $year)
            ->first();
    }

    /**
     * Update grade by id and institution id
     * @param integer $institutionId
     * @param integer $gradeId
     * @param string $name
     * @param integer $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function updateClassDetails(
        $institutionId,
        $gradeId,
        $name,
        $year
    ) {
        return static::where('grades.institution_id', $institutionId)
            ->where('grades.id', $gradeId)
            ->update(['name' => $name, 'year' => $year]);
    }

    /**
     * get grade by license institution
     * @param App\Models\License $license
     * @param integer $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getGradesByLicenseInstitution(
        $license,
        $year
    ) {
        $grades = $license->institution->grades();
        if ($year) {
            $grades = $grades->where('grades.year', $year);
        }
        return $grades;
    }
}
