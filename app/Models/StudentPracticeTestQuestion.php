<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentPracticeTestQuestion extends Model
{
    protected $fillable = ['user_id', 'practice_test_detail_id',
        'question_answer_id', 'answer', 'is_correct'];
}
