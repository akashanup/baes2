<?php

namespace App\Models;

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'detail'];

    /**
     * Get the roles that belong to the permission.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function roles()
    {
        return $this->belongsToMany(
            Role::class, 'role_permissions', 'permission_id', 'role_id'
        )->select('roles.id')->withTimestamps();
    }

    /**
     * Save permission.
     *
     * @param array $data Permission data
     * @return App\Models\Permission
     */
    public static function savePermission($data)
    {
        return static::create($data);
    }

    /**
     * Update permission.
     *
     * @param array $data Permission data
     * @return void
     */
    public function updatePermission($data)
    {
        $this->update($data);
    }

    /**
     * Query to only include id and name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetAll($query)
    {
        return $query->select(['id', 'name']);
    }
}
