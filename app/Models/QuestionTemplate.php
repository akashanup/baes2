<?php

namespace App\Models;

use App\Models\QuestionAnswer;
use Illuminate\Database\Eloquent\Model;

class QuestionTemplate extends Model
{
    protected $table = 'question_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['template', 'language', 'complexity', 'type',
        'learningGoal_id', 'status', 'order'];

    /**
     * Get the template_variables for the question_template.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function templateVariables()
    {
        return $this->hasMany(TemplateVariable::class, 'question_template_id');
    }

    /**
     * Get the question_feedbacks for the question_template.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function questionFeedbacks()
    {
        return $this->hasMany(QuestionFeedback::class, 'question_template_id');
    }

    /**
     * Get the question/answer for the question_template.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class);
    }

    /**
     * Add question/answer for the template_variable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function addQuestionAnswer($questionAnswer)
    {
        return $this->questionAnswers()->create($questionAnswer);
    }

    /**
     * Get the learning goal that owns the question_template.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function learningGoal()
    {
        return $this->belongsTo(LearningGoal::class, 'learningGoal_id');
    }

    /**
     * Scope to get all question template
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function scopegetAll($query)
    {
        return $query->addSelect(['question_templates.id', 'question_templates.template', 'question_templates.language', 'question_templates.complexity', 'question_templates.type', 'question_templates.learningGoal_id', 'question_templates.status', 'question_templates.order']);
    }

    /**
     * Scope to get template question
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $learning_goal_id
     * @param string $status
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function scopegetTemplateQuestion(
        $query,
        $learningGoalId,
        $status,
        $type = null
    ) {
        $query = $query->where('question_templates.learningGoal_id',
            $learningGoalId)
            ->where('question_templates.status', $status);
        if ($type) {
            $query = $query->where('question_templates.type', $type);
        }
        return $query;
    }

    /**
     * Get the template questions by learning goal
     * @param app\Model\QuestionTemplate $slug, $type
     * @return app\Model\QuestionTemplate
     */
    public static function getLearningTemplateQuestion($slug, $type)
    {
        return QuestionTemplate::getAll()
            ->addSelect('learning_goals.name')
            ->join('learning_goals', 'learning_goals.id', 'question_templates.learningGoal_id')
            ->where('question_templates.type', $type)
            ->where('question_templates.status', config('constants.ACTIVE'))
            ->where('learning_goals.slug', $slug)
            ->orderBy('question_templates.order');
    }

    /**
     * Get the template questions by learning goal, question, complexity, type
     * @param $learning_goal_id, $questions, $mcq_complexity, $type
     * @return app\Model\QuestionTemplate
     */
    public static function checkTemplateQuestion($learning_goal_id, $questions, $mcq_complexity, $type)
    {
        return QuestionTemplate::where('template', $questions)
            ->where('learningGoal_id', $learning_goal_id)
            ->where('complexity', $mcq_complexity)
            ->where('type', $type)
            ->get();
    }

    /**
     * Create question Template
     * @param array $questionTemplateData
     * @return app\Model\QuestionTemplate
     */
    public static function insertQuestionTemplate($questionTemplateData)
    {
        \Log::info($questionTemplateData);
        return static::create($questionTemplateData);
    }

    /**
     * Update question template status
     * @param string $question_type, array $data
     * @return app\Model\QuestionTemplate
     */
    public static function updatetemplateStatus($question_type, $data)
    {
        return static::where(
            'question_templates.learningGoal_id', $data['goal_id'])
            ->where('question_templates.type', $question_type)
            ->update(['question_templates.status' => config('constants.INACTIVE')]);
    }

    /**
     * Get current question template by
     * @param string $status, $question, $mcqComplexity
     * @param integer $learningGoalId
     * @return app\Model\QuestionTemplate
     */
    public static function getCurrentTemplateQuestion(
        $learningGoalId,
        $status,
        $question,
        $mcqComplexity
    ) {
        return static::getTemplateQuestion($learningGoalId, $status)
            ->getAll()
            ->where('question_templates.template', $question)
            ->where('question_templates.complexity', $mcqComplexity);
    }

    /**
     * update QuestionTemplate data
     * @param array $data
     * @return app\Model\QuestionTemplate
     */
    public function updateQuestionTemplate($data)
    {
        return $this->update($data);
    }
}
