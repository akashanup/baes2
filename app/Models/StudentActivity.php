<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class StudentActivity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'license_id', 'subject_id',
        'learning_goal_id', 'points', 'date'];
    /**
     * Get the user that has the activity.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function user()
    {
        return $this->belongsTo(User::class)->select('users.id');
    }
}
