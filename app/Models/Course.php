<?php

namespace App\Models;

use App\Models\Institution;
use App\Models\License;
use App\Models\Subject;
use DB;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    /**
     * Get the licenses for the course.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function licenses()
    {
        return $this->hasMany(License::class)->select('licenses.id',
            'licenses.code', 'licenses.status', 'licenses.price',
            'licenses.validity');
    }

    /**
     * Get the subjects that belong to the course.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'course_subjects',
            'course_id', 'subject_id')->select('subjects.id', 'subjects.name',
            'subjects.slug')->withPivot('order')->withTimestamps();
    }

    /**
     * Get the institutions that belong to the course.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function institutions()
    {
        return $this->belongsToMany(Institution::class, 'institution_courses',
            'course_id', 'institution_id')->select('institutions.id',
            'institutions.name', 'institutions.slug')->withTimestamps();
    }

    /**
     * Query to only find by slug.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Save course.
     *
     * @param string $name Course name
     * @return App\Models\Course
     */
    public static function saveCourse($name)
    {
        return static::create(['name' => $name]);
    }

    /**
     * Update course.
     *
     * @param string $name Course name
     * @return void
     */
    public function updateCourse($name)
    {
        $this->update(['name' => $name]);
    }

    /**
     * List of all courses for datatable
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCoursesForDatatable()
    {
        return static::leftJoin('course_subjects', 'course_subjects.course_id',
            'courses.id')->leftJoin('institution_courses',
            'institution_courses.course_id', 'courses.id')->leftJoin(
            'institutions', 'institution_courses.institution_id',
            'institutions.id')->select('courses.id', 'courses.name',
            'courses.slug',
            DB::raw("COUNT(DISTINCT course_subjects.subject_id) as subject"),
            DB::raw("GROUP_CONCAT(DISTINCT institutions.name SEPARATOR ' | ')
              as institution"))->groupBy('courses.id');
    }

    /**
     * Get subjects list with learning goal count
     *
     * @param string $slug Course slug
     * @return array
     */
    public static function getSubjectsWithLearningGoalCount($slug)
    {
        return Subject::getAll(config('constants.SUBJECT'))
            ->getSubjectWithLearningGoalCount()->join('course_subjects',
            'course_subjects.subject_id', 'subjects.id')->join('courses',
            'course_subjects.course_id', 'courses.id')->addSelect(
            'course_subjects.order')->where('courses.slug', $slug)
            ->groupBy('courses.id');
    }

    /**
     * Scope to get course data
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetAll($query)
    {
        return $query->addSelect(['courses.id', 'courses.name',
            'courses.slug']);
    }
}
