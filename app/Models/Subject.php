<?php

namespace App\Models;

use App\Models\Course;
use App\Models\LearningGoal;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name', 'slug', 'type', 'curriculum_id'];

    const SUBJECT = 'subject';
    const CURRICULUM = 'curriculum';

    /**
     * Many to many relationship between learning_goals and subjects.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function learningGoals()
    {
        return $this->belongsToMany(LearningGoal::class,
            'subject_learning_goals', 'subject_id', 'learning_goal_id')
            ->select('learning_goals.id', 'learning_goals.name')
            ->withPivot('order')->withTimestamps();
    }

    /**
     * Get the courses that belong to the subject.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_subjects',
            'course_id', 'subject_id')->select('courses.id', 'courses.name',
            'courses.slug')->withPivot('order')->withTimestamps();
    }

    /**
     * Store all learning goals in relation table
     *
     * @return App\Model\Subject
     */

    public function saveLearningGoals($learning_goal_id)
    {
        $this->learningGoals()->attach($learning_goal_id);
    }

    /**
     * Scope to get curriculuum's list by -  $id/$slug/$date
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetAll($query, $type = null)
    {
        $query->addSelect(['subjects.id', 'subjects.name',
            'subjects.slug']);
        if ($type) {
            $query->where('subjects.type', $type);
        }
        return $query;
    }

    /**
     * Scope to get subject by slug
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('subjects.slug', $slug);
    }

    /**
     * List of all curriculums with count in relation table
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCurriculumsForDatatable()
    {
        return static::getAll(config('constants.CURRICULUM'))
            ->getSubjectWithLearningGoalCount();
    }

    /**
     * Get subject with learning goal count
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopegetSubjectWithLearningGoalCount($query)
    {
        return $query->addSelect(
            DB::raw("count(subject_learning_goals.subject_id) as count")
        )->leftJoin('subject_learning_goals',
            'subject_learning_goals.subject_id', 'subjects.id'
        )->groupBy('subjects.id');
    }

    /**
     * Save subject.
     *
     * @param array $data Subject data
     * @return App\Models\Subject
     */
    public static function saveSubject($data, $type)
    {
        return static::create(['name' => $data['name'],
            'curriculum_id' => array_key_exists('curriculumId', $data) ?
            $data['curriculumId'] : null, 'type' => $type]);
    }

    /**
     * Update subject.
     *
     * @param array $data Subject data
     * @return void
     */
    public function updateSubject($data, $type)
    {
        $this->update(['name' => $data['name'],
            'curriculum_id' => array_key_exists('curriculumId', $data) ?
            $data['curriculumId'] : null, 'type' => $type]);
    }

    /**
     * Scope to get curriculum of a subject
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $slug Subject $slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetCurriculumOfSubject($query, $slug = null)
    {
        $query = $query->join('subjects as curriculums', 'curriculums.id',
            'subjects.curriculum_id')->addSelect(
            'curriculums.name as curriculum');
        if ($slug) {
            $query = $query->where('subjects.slug', $slug);
        }
        return $query;
    }

    /**
     * List of all subjects for datatable
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getSubjectsForDatatable()
    {
        return static::getAll(config('constants.SUBJECT'))->leftJoin(
            'subjects as curriculums', 'curriculums.id',
            'subjects.curriculum_id')->addSelect(
            'curriculums.name as curriculum');
    }

    /**
     * Update student learning goals
     *
     * @return void
     */
    public function updateStudentLearningGoals()
    {
        if ($this->type == static::SUBJECT) {
            $subjectLearningGoals = $this->learningGoals;
            $studentsLicenses = DB::Table('student_learning_goals')
                ->where('subject_id', $this->id)->select(
                DB::raw("DISTINCT license_id, user_id"))->get()->toArray();

            foreach ($studentsLicenses as $studentsLicense) {
                $newStudentLearningGoals = [];
                $key = 0;
                $studentLearningGoals = UserRepository::studentLearningGoalIds(
                    $studentsLicense->license_id, $studentsLicense->user_id, $this->id);
                $newLearningGoals = $subjectLearningGoals->diff($studentLearningGoals);
                $removedLearningGoals = $studentLearningGoals->diff($subjectLearningGoals);
                // Remove student learning goals
                if ($removedLearningGoals->count()) {
                    $deleteStudentLearningGoals = $removeStudentLearningGoals = DB::Table('student_learning_goals')
                        ->where('license_id', $studentsLicense->license_id)
                        ->where('user_id', $studentsLicense->user_id)
                        ->where('subject_id', $this->id)
                        ->whereIn('learning_goal_id', $removedLearningGoals->pluck(
                            'id')->toArray());
                    $removeStudentLearningGoals = $removeStudentLearningGoals
                        ->pluck('learning_goal_test_id');
                    // Remove LearningGoalTests
                    LearningGoalTest::whereIn('id', $removeStudentLearningGoals)->delete();
                    // Remove student learning goals
                    $deleteStudentLearningGoals->delete();
                }
                if ($newLearningGoals->count()) {
                    foreach ($newLearningGoals as $newLearningGoal) {
                        $newStudentLearningGoals[$key++] = ['user_id' => $studentsLicense->user_id,
                            'license_id' => $studentsLicense->license_id,
                            'subject_id' => $this->id, 'learning_goal_id' => $newLearningGoal->id,
                            'order' => $newLearningGoal->pivot->order, 'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()];
                    }
                }
                if (sizeof($newStudentLearningGoals)) {
                    DB::Table('student_learning_goals')->insert($newStudentLearningGoals);
                }
                // Update student learning goals order
                $existingLearningGoals = UserRepository::studentLearningGoalIds(
                    $studentsLicense->license_id, $studentsLicense->user_id, $this->id);
                foreach ($existingLearningGoals as $existingLearningGoal) {
                    DB::Table('student_learning_goals')
                        ->where('license_id', $studentsLicense->license_id)
                        ->where('user_id', $studentsLicense->user_id)
                        ->where('subject_id', $this->id)
                        ->where('learning_goal_id', $existingLearningGoal->id)
                        ->update(['order' =>
                            $this->learningGoals()->find($existingLearningGoal->id)
                                ->pivot->order]);
                }
            }
        }
    }

        /**
     * get all learning goals by subject
     * @param slug $slug
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public static function subjectLearningGoals($slug)
    {
        return static::addSelect('learning_goals.id', 'learning_goals.name',
        'learning_goals.slug', 'subject_learning_goals.order')
        ->join('subject_learning_goals',
        'subject_learning_goals.subject_id', 'subjects.id')
        ->join('learning_goals', 'learning_goals.id', 'subject_learning_goals.learning_goal_id')
        ->where('subjects.slug', $slug)
        ->orderBy('subject_learning_goals.order')
        ->get();
    }
}
