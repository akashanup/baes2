<?php

namespace App\Models;

use App\Models\QuestionAnswer;
use App\Services\GenerateTextService;
use Illuminate\Database\Eloquent\Model;

class Hint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['hint', 'question_answer_id'];

    /**
     * Get the questionAnswer that owns the hint.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function questionAnswer()
    {
        return $this->belongsTo(QuestionAnswer::class);
    }

    /**
     * Generate hint text for hint
     */
    public static function generateHint($hintTemplates, $templateVariable)
    {
        $hint = [];
        foreach ($hintTemplates as $key => $hintTemplate) {
            $hint[$hintTemplate->step] = GenerateTextService::text(
                $hintTemplate->template, $templateVariable->values);
        }
        return json_encode($hint);
    }
}
