<?php

namespace App\Models;

use App\Models\License;
use App\Models\QuestionTemplate;
use App\Models\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Lang;

class PracticeTestDetail extends Model
{
    protected $fillable = ['grade_id', 'start_date', 'end_date',
        'time', 'feedback_type', 'status', 'avg_score', 'student_attempted',
        'name', 'slug', 'template', 'user_id', 'course_id', 'institution_id',
        'folder_id', 'gmt', 'availability', 'created_by'];

    const STUDENT_PRACTICE_TEST_TIME = 60;
    /**
     * Save practice test Details.
     *
     * @param string $data PracticeTestDetail data
     * @return App\Models\PracticeTestDetail
     */
    public static function saveTestDetails($data)
    {
        return static::create($data);
    }

    /**
     * Save practice test Details.
     *
     * @param string $data PracticeTestDetail data
     * @return void
     */
    public static function saveStudentTest($data, $gradeId, $userId, $courseId, $institutionId)
    {
        $additionalData = ['grade_id' => $gradeId, 'start_date' => Carbon::now(),
            'slug' => static::createSlug($data['name']), 'user_id' => $userId,
            'course_id' => $courseId, 'institution_id' => $institutionId,
            'created_by' => config('constants.STUDENT'), 'time' => static::STUDENT_PRACTICE_TEST_TIME];
        return static::create(array_merge($additionalData, $data));
    }

    /**
     * Create slug.
     *
     * @param string $name name
     * @return string
     */
    public static function createSlug($name)
    {
        $slug = str_slug($name, '-');
        $rows = static::where('slug', 'like', $slug . '%')->get();
        if ($rows->count()) {
            $row = $rows->last();
            $slug .= '-' . $row->id;
        }
        return $slug;
    }

    /**
     * The question_answers that belong to the practice test template.
     */
    public function questionAnswers()
    {
        return $this->belongsToMany(QuestionAnswer::class,
            'practice_test_questions', 'practice_test_detail_id',
            'question_answer_id')->withPivot('id', 'random_key')
            ->withTimestamps();
    }

    public function addQuestionAnswers()
    {
        $templates = json_decode($this->template, true);
        $response = [];
        $response['response'] = false;
        $learningGoals = [];
        $complexities = [];
        $questionTemplates = [];
        foreach ($templates as $key => $template) {
            $questionTemplate = QuestionTemplate::where('learningGoal_id',
                $template['goal_id'])->where('complexity', $template['level'])
                ->where('status', config('constants.ACTIVE'));
            // If learning goal and complexity are same then select different question templates.
            if (($key - 1)
                && (in_array($template['goal_id'], $learningGoals)
                    && in_array($template['level'], $complexities)
                    && (
                        !sizeof(
                            array_diff(
                                array_keys($learningGoals, $template['goal_id']),
                                array_keys($complexities, $template['level'])
                            )
                        )
                    )
                )
            ) {
                // Keep track of previous learning goals and complexities.
                $arrayKeys = array_keys($learningGoals, $template['goal_id']);
                $accessedQuestionTemplates = array_intersect_key(
                    $questionTemplates, array_flip($arrayKeys));
                $questionTemplate = $questionTemplate->whereNotIn(
                    'id', $accessedQuestionTemplates);
            }
            $questionTemplate = $questionTemplate->inRandomOrder()->first();
            if (!$questionTemplate) {
                $questionTemplate = QuestionTemplate::where('learningGoal_id',
                    $template['goal_id'])
                    ->where('complexity', $template['level'])
                    ->where('status', config('constants.ACTIVE'))
                    ->inRandomOrder()->first();
            }
            if ($questionTemplate) {
                // Save the learning goal, complexity and the question template as a set to avoid repeatitions.
                $learningGoals[$key] = $template['goal_id'];
                $complexities[$key] = $template['level'];
                $questionTemplates[$key] = $questionTemplate->id;
                $questions = $questionTemplate->questionAnswers()
                    ->where('status', config('constants.ACTIVE'))
                    ->inRandomOrder()->get();
                $attachQuestionAnswerResponse = $this->attachQuestionAnswer(
                    $questions, $template, $key);
                if ($attachQuestionAnswerResponse) {
                    $response['response'] = false;
                    $response['response'] = array_merge($response,
                        $attachQuestionAnswerResponse);
                    break;
                }
            } else {
                $subject = Subject::findOrFail($template['subject_id']);
                $learningGoal = LearningGoal::findOrFail($template['goal_id']);
                $response['message'] = Lang::get('users.no_question_found')
                . ' ' . $subject->name . '->' . $learningGoal->name . '->' .
                config('constants.COMPLEXITY')[$template['level']];
                $response['response'] = false;
                break;
            }
            $response['response'] = true;
        }
        return $response;
    }

    /**
     * Attach question to practice test
     *
     * @param array questions
     * @param array template
     * @param integer key
     * @return array
     */
    public function attachQuestionAnswer($questions, $template, $key)
    {
        $response = null;
        if (count($questions)) {
            if ($template['random'] == config('constants.YES')) {
                foreach ($questions as $question) {
                    $this->questionAnswers()->attach($question->id,
                        ['random_key' => $key]);
                }
            } else {
                $question = $questions->first();
                $this->questionAnswers()->attach($question->id,
                    ['random_key' => $key]);
            }
        } else {
            $learningGoal = LearningGoal::findOrFail($template['goal_id']);
            $response['message'] = Lang::get('users.no_question_found')
            . ' ' . $learningGoal->name . '->' .
            config('constants.COMPLEXITY')[$template['level']];
        }
        return $response;
    }

    /**
     * Get all active practice tests
     * by courseId and InstitutionId
     * @param integer courseId
     * @param integer institutionId
     * @param string $status
     * @return array
     */
    public static function practiceTestsByTeacher(
        $courseId,
        $institutionId,
        $status = null
    ) {
        $status = $status ? $status : config('constants.STARTED');
        return static::where('practice_test_details.course_id', $courseId)
            ->where('practice_test_details.institution_id', $institutionId)
            ->where('practice_test_details.status', $status)
            ->where('practice_test_details.created_by', config('constants.TEACHER'));
    }

    /**
     * Get all active practice tests
     * by courseId and InstitutionId
     * @param integer courseId
     * @param integer institutionId
     * @param string $status
     * @return array
     */
    public static function listPracticeTests(
        $courseId,
        $institutionId,
        $status,
        $folderId = null,
        $gradeId = null,
        $year = null
    ) {

        $query = static::practiceTestsByTeacher($courseId, $institutionId,
            $status)->addSelect('practice_test_details.id',
            'practice_test_details.end_date', 'practice_test_details.avg_score',
            'practice_test_details.student_attempted', 'grades.name as grade',
            'grades.year', 'practice_test_details.name',
            'practice_test_details.slug')
            ->join('grades', 'grades.id', 'practice_test_details.grade_id');
        if ($folderId) {
            $query = $query->where('practice_test_details.folder_id',
                $folderId);
        }
        if ($gradeId) {
            $query = $query->where('grades.id', $gradeId);
        }
        if ($year) {
            $query = $query->where('grades.year', $year);
        }
        return $query;
    }

    /**
     * Update Test practice status
     *
     * @return void
     */
    public function finishPracticeTest()
    {
        $this->update(['status' => config('constants.FINISHED')]);
        static::finishPracticeTestDetail($this);
    } //end finishPracticeTest()

    /**
     * Get the students associated with the practice_test_details.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'student_practice_test_statuses',
            'practice_test_detail_id', 'user_id')->select('users.id')
            ->withPivot('status', 'score')->withTimestamps();
    }

    /**
     * Get the students' questions associated with the practice_test_details.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function usersQuestions()
    {
        return $this->belongsToMany(User::class, 'student_practice_test_questions',
            'practice_test_detail_id', 'user_id')->select('users.id')
            ->withPivot('question_answer_id', 'answer')->withTimestamps();
    }

    /**
     * Get the subject_ids associated with the practice_test_detail.
     *
     * @return array
     */
    public function subjects()
    {
        $subjects = static::select(DB::raw(
            'JSON_EXTRACT(`template`,"$**.subject_id") as subjects'))
            ->where('id', $this->id)->first();
        $subjects = $subjects ? array_unique(json_decode(
            $subjects->subjects, true)) : [];
        return $subjects;
    }

    /**
     * Active practice tests of a license.
     *
     * @param App\Models\License $license License model
     * @param integer $gradeId Grade id
     * @return collection
     */
    public static function activePracticeTestsForLicense(License $license, $gradeId)
    {
        return static::practiceTestsByTeacher($license->course_id,
            $license->institution_id, config('constants.STARTED'))->where(
            'practice_test_details.grade_id', $gradeId)->where(
            'practice_test_details.availability', config('constants.ALL_USER'))
            ->get();
    }

    /**
     * Query to only find by slug.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $slug User slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Finish expired practice test Details.
     *
     * @return void
     */
    public static function finishExpiredPracticeTestsDetails()
    {
        $expirsedPracticeTests = static::where('status', config('constants.STARTED'))
            ->where('created_by', config('constants.TEACHER'))->get()
            ->filter(function ($activePracticeTest) {
                return Carbon::parse($activePracticeTest->end_date)->lte(
                    Carbon::now('GMT')->addMinutes($activePracticeTest->gmt));
            });
        foreach ($expirsedPracticeTests as $practiceTestDetail) {
            static::finishPracticeTestDetail($practiceTestDetail);
        }
    }

    /**
     * Finish practice test Details.
     *
     * @return void
     */
    public static function finishPracticeTestDetail($practiceTestDetail)
    {
        // Finish corresponding student practice tests.
        foreach ($practiceTestDetail->users()->where('student_practice_test_statuses.status',
            '!=', config('constants.FINISHED'))->get() as $user) {
            UserRepository::finishStudentPracticeTest($user, $practiceTestDetail->id);
        }
        $practiceTestDetail->update(['status' => config('constants.FINISHED'), 'avg_score' =>
            $practiceTestDetail->users()->avg('student_practice_test_statuses.score')]);
    }

    /**
     * Update practice test score
     *
     * @param integer $practiceTestDetailId PracticeTestDetail id
     * @return void
     */
    public static function updatePracticeTestDetailScore($practiceTestDetailId)
    {
        $practiceTestDetail = PracticeTestDetail::findOrFail($practiceTestDetailId);
        $average_score = UserRepository::studentsOfPracticeTest($practiceTestDetail)
            ->get()->avg('score');
        $practiceTestDetail->update(['avg_score' => $average_score]);
    }
}
