<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = ['mollie_subscription_id',
        'mollie_customer_unique_id', 'mandata_id',
        'user_id', 'status', 'susbcription_period',
        'subscription_start_date', 'subscription_payment_date',
        'status'];

    /**
     * Query to find available subscriptions.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetSubscriptions($query)
    {
        return $query->addSelect('subscriptions.mollie_subscription_id',
            'subscriptions.mollie_customer_unique_id',
            'subscriptions.mandata_id', 'subscriptions.user_id',
            'subscriptions.susbcription_period', 'subscriptions.status');
    }

    /**
     * Query to get subscriptions by mollie_subscription_id and
     *    mollie_customer_unique_id.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $mollie_subscription_id mollie_subscription_id
     * @param string $mollie_customer_unique_id mollie_customer_unique_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCurrentSubscription($subscriptionId, $customerId)
    {
        $query = static::getSubscriptions()
            ->where('subscriptions.mollie_customer_unique_id', $customerId);
        if ($subscriptionId) {
            $query = $query->where('subscriptions.mollie_subscription_id', $subscriptionId);
        }
        return $query->first();
    }

    /**
     * Save Subscription.
     *
     * @param string $data Subscription data
     * @return App\Models\Subscription
     */
    public static function saveSubscription($data)
    {
        return static::create($data);
    }

    /**
     * Update Susbscription.
     *
     * @param string $data Susbscription data
     */
    public function updateSubscription($data)
    {
        static::update($data);
    }

    /**
     * Get subscription details by mollie subscription Id
     *
     * @param string  $mollieSubscriptionId
     * @return App\Models\Subscription
     */
    public static function getSubscriptionByMollieId($mollieSubscriptionId)
    {
        return static::where('subscriptions.id', $mollieSubscriptionId)
            ->first();
    }
}
