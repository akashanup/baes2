<?php

namespace App\Models;

use App\Models\Course;
use App\Models\Institution;
use App\Models\User;
use App\Notifications\FeedbackResponseNotification;
use App\Services\EmailConfiguration;
use Illuminate\Database\Eloquent\Model;

class StudentFeedback extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'question_answer_id', 'license_id',
        'message', 'note', 'status', 'response_email_type'];

    /**
     * Get the user that has given the feedback.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * List of all feedbacks for datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getStudentFeedbacksForDatatable(
        $institution,
        $course
    ) {
        $query = static::join('users', 'student_feedbacks.user_id', 'users.id')
            ->leftJoin('licenses', 'student_feedbacks.license_id', 'licenses.id')
            ->leftJoin('institutions', 'licenses.institution_id', 'institutions.id')
            ->leftJoin('courses', 'licenses.course_id', 'courses.id')
            ->leftJoin('question_answers', 'student_feedbacks.question_answer_id',
                'question_answers.id')
            ->leftJoin('question_templates', 'question_answers.question_template_id',
                'question_templates.id')
            ->leftJoin('learning_goals', 'question_templates.learningGoal_id',
                'learning_goals.id')
            ->select('institutions.name as institutionName',
                'courses.name as courseName',
                'learning_goals.name as learningGoalName',
                'question_templates.complexity', 'student_feedbacks.created_at',
                'student_feedbacks.message', 'users.first_name', 'users.last_name',
                'student_feedbacks.id', 'student_feedbacks.status',
                'student_feedbacks.note', 'student_feedbacks.response_email_type',
                'users.email', 'question_answers.id as questionAnswerId')
            ->orderBy('student_feedbacks.id', 'DESC');

        if (Course::findBySlug($course)->count()) {
            $query->where('courses.slug', $course);
        }
        if (Institution::findBySlug($institution)->count()) {
            $query->where('institutions.slug', $institution);
        }
        return $query;
    }

    /**
     * Update the note for the student feedback
     *
     * @param array $data StudentFeedback data
     * @return void
     */
    public function updateNote($data)
    {
        $this->update($data);
    }

    /**
     * Update the note for the student feedback
     *
     * @param array $data StudentFeedback data
     * @return void
     */
    public function updateStatus($data)
    {
        $status = $this->status === config('constants.PROCESSED')
        ? config('constants.UNPROCESSED') : config('constants.PROCESSED');
        $this->update(['status' => $status,
            'response_email_type' => $data['response_email_type']]);
    }

    /**
     * Send student feedback response
     *
     * @return void
     */
    public function sendFeedbackResponse()
    {
        $emailType = $this->response_email_type;
        if (in_array($emailType, [
            config('constants.PROCESSED'),
            config('constants.ROUNDING'),
            config('constants.INCORRECT'),
            config('constants.CUSTOM')])) {
            // Change config to send feedback mail from different server.
            EmailConfiguration::change();
            $user = $this->user;
            $user->notify(new FeedbackResponseNotification(
                $user->name(), $this));
        }
    }
}
