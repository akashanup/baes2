<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class PracticeTestFolder extends Model
{
    protected $fillable = ['name', 'slug', 'description',
        'license_id', 'user_id', 'institution_id', 'course_id'];

    /**
     * Save Practice test folder.
     *
     * @param string $data PracticeTestFolder data
     * @return App\Models\Course
     */

    public static function saveFolder($data)
    {
        return static::create($data);
    }

    /**
     * Scope to get folder by slug
     *
     * @param App\Model\PracticeTestFolder $query
     * @param string $slug
     * @return App\Model\PracticeTestFolder
     */
    public static function scopefindBySlug($query, $slug)
    {
        return $query->where('practice_test_folders.slug', $slug);
    }

    /**
     * update Practice test folder.
     *
     * @param string $folderName
     * @param string $folderDescription
     * @param string $folderId
     * @return App\Models\Course
     */

    public static function updateFolder(
        $folderName,
        $folderDescription,
        $folderId
    ) {
        return static::where('practice_test_folders.id', $folderId)
            ->update(['name' => $folderName, 'description' => $folderDescription]);
    }

    /**
     * PracticeTestTemplate relation
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function practiceTestTemplates()
    {
        return $this->hasMany(PracticeTestTemplate::class)
            ->select('practice_test_templates.id');
    }

    /**
     * Get PracticeTestFolder details
     *
     * @param intiger $institutionId
     * @param intiger $courseId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getInstitutionCourseFolders(
        $institutionId,
        $courseId
    ) {
        return PracticeTestFolder::
            select('practice_test_folders.id',
            'practice_test_folders.name', 'practice_test_folders.description',
            'practice_test_folders.slug', DB::raw('COUNT(practice_test_templates.id) as count'))
            ->leftJoin('practice_test_templates', 'practice_test_templates.folder_id', 'practice_test_folders.id')
            ->where('practice_test_folders.institution_id', $institutionId)
            ->where('practice_test_folders.course_id', $courseId)
            ->groupBy('practice_test_folders.id')
            ->get();
    }
}
