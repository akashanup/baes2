<?php

namespace App\Models;

use App\Models\Hint;
use App\Models\LearningGoalTest;
use App\Models\QuestionTemplate;
use App\Services\AnswerFormatService;
use App\Services\GenerateTextService;
use DB;
use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'option', 'answer', 'status',
        'question_template_id', 'template_variable_id',
        'upper_bound', 'lower_bound'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['answer', 'upper_bound', 'lower_bound'];

    const INPUT_TYPE = 'direct';
    const MCQ_TYPE = 'mcq';

    /**
     * Generate question text for question_answer
     *
     * @param App\Models\TemplateVariable $templateVariable
     * @return string
     */
    public static function generateQuestion(TemplateVariable $templateVariable)
    {
        $questionTemplate = $templateVariable->questionTemplate;
        $question = GenerateTextService::text($questionTemplate->template,
            $templateVariable->values);
        return $question;
    }

    /**
     * Get the question_template that owns the question_answer.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function questionTemplate()
    {
        return $this->belongsTo(QuestionTemplate::class);
    }

    /**
     * Get the hint the question_answer.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function hint()
    {
        return $this->hasOne(Hint::class);
    }

    /**
     * Add hint for the question.
     *
     * @param text $hint
     * @return App\Models\Hint
     */
    public function addHint($hint)
    {
        return $this->hint()->create(['hint' => $hint]);
    }

    /**
     * Get the test that owns the question_answer.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function learningGoalTests()
    {
        return $this->belongsToMany(LearningGoalTest::class,
            'learning_goal_test_question_answers', 'question_answer_id',
            'learning_goal_test_id')->withPivot('id', 'answer', 'points',
            'score_calculated')->withTimestamps();
    }

    public function practiceTestDetails()
    {
        return $this->belongsToMany(PracticeTestDetail::class, 'practice_test_questions',
            'question_answer_id', 'practice_test_detail_id')
            ->withPivot('id', 'random_key')->withTimestamps();
    }

    /**
     * Update Question answer status by learning goal and type
     *
     * @param array $questionData
     * @return void
     */
    public static function updateLearningGoalQuestions($questionData)
    {
        $questionAnswerIds = static::addSelect('question_answers.id')
            ->where('question_templates.learningGoal_id',
                $questionData['goal_id'])->where('question_templates.status',
            config('constants.INACTIVE'))->where('question_templates.type',
            $questionData['type'])->join('question_templates',
            'question_templates.id', 'question_answers.question_template_id')
            ->join('template_variables', 'template_variables.id',
                'question_answers.template_variable_id');

        static::whereIN('id', $questionAnswerIds->pluck('id'))
            ->update(['question_answers.status' => config('constants.INACTIVE')]
            );
    }

    /**
     * Update Question answer status by question template ID and template variable ID
     *
     * @param array $questionTemplate, $templateVariable
     * @return App\Model\QuestionAnswer
     */
    public static function updateVariableQuestions(
        $questionTemplate,
        $templateVariable = null
    ) {
        $query = static::where('question_template_id', $questionTemplate);
        if ($templateVariable) {
            $query = $query->where('template_variable_id', $templateVariable);
        }
        return $query->update(['status' => config('constants.INACTIVE')]);
    }

    /**
     * get all learning goal question by ID
     * @param array $learninGoalId
     * @return App\Model\QuestionAnswer
     */
    public static function learningGoalQuestions($unique, $learninGoalId)
    {
        if ($unique === config('constants.NO')) {
            $query = static::addSelect('question_answers.id',
                'question_answers.question', 'question_answers.option',
                'question_answers.answer', 'question_answers.status',
                'learning_goals.name', 'question_templates.complexity');
        } elseif ($unique === config('constants.YES')) {
            $query = static::addSelect(DB::raw('min(question_answers.id) as id'),
                'learning_goals.name', 'question_templates.complexity',
                'question_templates.type')
                ->groupBy('question_answers.question_template_id');
        }

        return $query = $query->join('question_templates',
            'question_answers.question_template_id',
            'question_templates.id')
            ->join('learning_goals', 'learning_goals.id',
                'question_templates.learningGoal_id')
            ->where('question_templates.status', config('constants.ACTIVE'))
            ->where('question_answers.status', config('constants.ACTIVE'))
            ->whereIn('learning_goals.id', $learninGoalId);
    }

    /**
     * Verify answer of a question
     *
     * @param string $answer
     * @return boolean
     */
    public function checkAnswer($answer)
    {
        $response = null;
        switch ($this->questionTemplate->type) {
            case strtolower(config('constants.MCQ_TYPE')):
                $response = $this->answer == $answer;
                break;
            case strtolower(config('constants.DIRECT_TYPE')):
                $correctAnswer = $this->answer;
                $upperBound = $this->upper_bound;
                $lowerBound = $this->lower_bound;
                // If correctAnswer doesn't have decimal values then append .00 at the end since the givenAnswer is in decimal format.
                if (!strpos($correctAnswer, '.')) {
                    $correctAnswer = AnswerFormatService::appendDecimal($correctAnswer);
                }
                if (!strpos($upperBound, '.')) {
                    $upperBound = AnswerFormatService::appendDecimal($upperBound);
                }
                if (!strpos($lowerBound, '.')) {
                    $lowerBound = AnswerFormatService::appendDecimal($lowerBound);
                }
                // If correctAnswer has the % sign then there are four formats of correct answer else compare the givenAnswer with correctAnswer.
                if (strpos($correctAnswer, '%')) {
                    $correctAnswers = AnswerFormatService::format($correctAnswer);
                    $upperBound = $this->upper_bound ? AnswerFormatService::format($upperBound) : [];
                    $lowerBound = $this->lower_bound ? AnswerFormatService::format($lowerBound) : [];
                    if (in_array($answer, $correctAnswers)) {
                        $response = true;
                    } elseif (sizeof($upperBound) && sizeof($lowerBound)) {
                        for ($i = 0; $i < sizeof($lowerBound); $i++) {
                            if (floatval($answer) >= floatval($lowerBound[$i]) && floatval($answer) <= floatval($upperBound[$i])) {
                                $response = true;
                            }
                        }
                    }
                } elseif (($correctAnswer == $answer)
                    || ($this->upper_bound && $this->lower_bound
                        && (floatval($answer) >= floatval($lowerBound)) && (floatval($answer) <= floatval($upperBound)))) {
                    $response = true;
                }
                break;

            default:
                # code...
                break;
        }
        return $response;
    }
}
