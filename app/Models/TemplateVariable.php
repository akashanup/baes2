<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplateVariable extends Model
{
    protected $fillable = [
        'values',
        'correct_answer',
        'question_template_id',
        'status',
        'upper_bound',
        'lower_bound',
    ];
    protected $table = 'template_variables';

    const PENDING_STATUS = 'pending';
    const FINISHED_STATUS = 'finished';

    /**
     * Get the question_template that owns the template_variable.
     */
    public function questionTemplate()
    {
        return $this->belongsTo(QuestionTemplate::class, 'question_template_id');
    }
    /**
     * check template variable exists
     * @param string $values, $correct_answer
     * @return app\Model\TemplateVariable
     */
    public function checkTemplateVariable($values, $correctAnswer)
    {
        return TemplateVariable::where('values', $values)
            ->where('correct_answer', $correctAnswer)
            ->where('status', static::FINISHED_STATUS)
            ->get();
    }

    /**
     * Scope to get all tempalte variable
     * @param app\Model\TemplateVariable $query
     * @return app\Model\TemplateVariable
     */
    public function scopegetAll($query)
    {
        return $query->addSelect(['template_variables.id', 'template_variables.values', 'template_variables.correct_answer', 'template_variables.status', 'template_variables.question_template_id', 'template_variables.status_edit',
        ]);
    }

    /**
     * Create Template variable
     * @param array $variableData
     * @return app\Model\TemplateVariable
     */
    public static function insertTemplateVariable($variableData)
    {
        return static::create($variableData);
    }

    /**
     * Get template variable set
     * @param int $id
     * @return app\Model\TemplateVariable
     */
    public static function templateVariableSet($id)
    {
        return static::getAll()
            ->where('template_variables.question_template_id', $id)
            ->where('template_variables.status_edit', config('constants.NO'))
            ->get();
    }

    /**
     * Get current template Variable
     * @param string $correctAnswer, $status
     * @param integer $questionTemplateId
     * @return app\Model\QuestionTemplate
     */
    public static function getCurrentTemplateVariable(
        $correctAnswer,
        $status,
        $questionTemplateId
    ) {
        return static::getAll()
            ->where('template_variables.correct_answer', $correctAnswer)
            ->where('template_variables.status', $status)
            ->where('template_variables.question_template_id', $questionTemplateId)
            ->first();
    }

    /**
     * Get current template Variable for all question templates
     * @param string $correctAnswer, $status
     * @param array $questionTemplateIds
     * @return app\Model\QuestionTemplate
     */
    public static function allQuestionTemplatesVariable(
        $correctAnswer,
        $status,
        $questionTemplateIds
    ) {
        return static::getAll()
            ->where('template_variables.correct_answer', $correctAnswer)
            ->where('template_variables.status', $status)
            ->whereIn('template_variables.question_template_id', $questionTemplateIds)
            ->get();
    }
}
