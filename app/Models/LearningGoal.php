<?php

namespace App\Models;

use App\Exceptions\NoQuestionFoundException;
use App\Models\QuestionAnswer;
use App\Repositories\UserRepository;
use App\Services\VideoEncodeService;
use Illuminate\Database\Eloquent\Model;
use Lang;

class LearningGoal extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'theory', 'example',
        'video', 'slug', 'theory_url'];

    const INTRODUCTION_QUESTION_COMPLEXITY = 0;

    /**
     * Many to many relationship between learning_goals and subjects.
     *
     * @return App\Model\Subject
     */
    public function subjects()
    {
        return $this->belongsToMany(
            Subject::class, 'subject_learning_goals', 'learning_goal_id',
            'subject_id')->select('subjects.id', 'subjects.name',
            'subjects.type')->withPivot('order')->withTimestamps();
    }

    /**
     * The Question templates that belong to the learning goal.
     */
    public function questionTemplates()
    {
        return $this->hasMany(QuestionTemplate::class, 'learningGoal_id');
    }

    /**
     * Scope to get learning goal's list by -  $id/$slug/$date
     *
     * @param pp\Model\Subject $query
     * @return pp\Model\Subject
     */
    public static function scopegetAll($query)
    {
        return $query->addSelect(['learning_goals.id', 'learning_goals.name',
            'learning_goals.slug', 'learning_goals.theory_url']);
    }

    /**
     * List of all learning goal's in relation table
     *
     * @param pp\Model\Subject
     * @return pp\Model\LearningGoal
     */
    public static function getAllLearningGoals($slug)
    {
        $queryBuilder = static::getAll()
            ->addSelect('learning_goals.theory', 'learning_goals.example');

        if ($slug) {
            $queryBuilder = $queryBuilder->join('subject_learning_goals',
                'subject_learning_goals.learning_goal_id', 'learning_goals.id')
                ->join('subjects', 'subjects.id', 'subject_learning_goals.subject_id')
                ->where('subjects.slug', $slug)
                ->groupBy('learning_goals.id');
        }
        return $queryBuilder;
    }

    /**
     * Save Learning goal.
     *
     * @param array $data Permission data
     * @return App\Models\Permission
     */
    public static function saveLearingGoal($data)
    {
        return static::create([
            'name' => $data['name'], 'theory' => $data['theory'],
            'example' => $data['example'],
            'video' => VideoEncodeService::videoEncode($data),
        ]);
    }

    /**
     * Update Learning goal by $slug.
     *
     * @param array $data Permission data
     * @return App\Models\Permission
     */
    public static function updateLearingGoal($data, $slug)
    {
        $learningGoal = static::where('slug', $slug)->first();
        $learningGoal->update([
            'name' => $data['name'], 'theory' => $data['theory'],
            'example' => $data['example'],
            'video' => VideoEncodeService::videoEncode($data),
        ]);
        return $learningGoal;
    }

    /**
     * Scope to get LearningGoal by slug
     *
     * @param App\Model\LearningGoal $query
     * @param string $slug
     * @return App\Model\LearningGoal
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('learning_goals.slug', $slug);
    }

    /**
     * Scope to get LearningGoal by slug
     *
     * @param App\Model\LearningGoal $query
     * @param string $slug
     * @return App\Model\LearningGoal
     */

    public static function allLearningGoalQuestions($slug, $type, $curriculumSlug)
    {
        $allQuestions = static::addSelect('learning_goals.name',
            'learning_goals.slug', 'question_templates.id',
            'question_templates.template', 'question_templates.language',
            'question_templates.complexity', 'question_templates.learningGoal_id',
            'question_templates.status', 'question_templates.order')
            ->join('question_templates', 'question_templates.learningGoal_id',
                'learning_goals.id');
        if ($curriculumSlug) {
            $allQuestions = $allQuestions->join('subject_learning_goals',
                'subject_learning_goals.learning_goal_id',
                'learning_goals.id')
                ->join('subjects', 'subjects.id', 'subject_learning_goals.subject_id')
                ->where('subjects.type', Subject::CURRICULUM)
                ->where('subjects.slug', $curriculumSlug);
        }
        if ($slug) {
            $allQuestions = $allQuestions->where
                ('learning_goals.slug', $slug);
        }
        $allQuestions = $allQuestions->where(
            'question_templates.type', strtolower($type))
            ->where('question_templates.status', config('constants.ACTIVE'));
        return $allQuestions;
    }

    /**
     * Get All question template of learning goal by curriculum
     *
     * @param string $curriculumSlug, $type
     * @return App\Model\LearningGoal
     */
    public static function allCurriculumLearningGoalQuestions(
        $curriculumSlug,
        $type
    ) {
        return static::select('learning_goals.name', 'question_templates.id',
            'question_templates.template', 'question_templates.language',
            'question_templates.complexity', 'question_templates.learningGoal_id',
            'question_templates.status')->join('question_templates',
            'question_templates.learningGoal_id', 'learning_goals.id')->join(
            'subject_learning_goals', 'subject_learning_goals.learning_goal_id',
            'learning_goals.id')->join('subjects', 'subjects.id',
            'subject_learning_goals.subject_id')
            ->where('question_templates.type', strtolower($type))
            ->where('question_templates.status', config('constants.ACTIVE'))
            ->where('subjects.type', Subject::CURRICULUM)
            ->where('subjects.slug', $curriculumSlug);
    }

    /**
     *  get All question answers by question Type : MCQ or DIRECT
     *
     * @param string $type
     * @return App\Model\LearningGoal
     */
    public function getAllQuestionAnswers($type)
    {
        $questionAnswers = LearningGoal::addSelect('question_answers.id',
            'question_answers.question', 'question_answers.answer',
            'question_templates.complexity', 'question_templates.type')
            ->join('question_templates', 'question_templates.learningGoal_id',
                'learning_goals.id')
            ->join('question_answers', 'question_answers.question_template_id',
                'question_templates.id')
            ->where('question_templates.type', $type)
            ->where('question_answers.status', config('constants.ACTIVE'));

        if ($type == config('constants.MCQ_TYPE')) {
            $questionAnswers->addSelect('template_variables.values')
                ->join('template_variables',
                    'template_variables.question_template_id',
                    'question_templates.id');
        }

        return $questionAnswers->get();
    }

    /**
     * Get a random introduction question of a learning goal.
     *
     * @param integer $previousQuestionAnswerId QuestionAnswerId of last attempted introduction question
     * @return array
     */
    public function getRandomIntroductionQuestion($previousQuestionAnswerId = null)
    {
        $questionAnswer = null;
        $questionTemplate = $this->questionTemplates()->where('complexity',
            static::INTRODUCTION_QUESTION_COMPLEXITY)->where('type',
            config('constants.MCQ_TYPE'))->where('status',
            config('constants.ACTIVE'));
        if ($previousQuestionAnswerId) {
            $questionTemplate = $questionTemplate->where('question_templates.id', '!=',
                QuestionAnswer::select('question_template_id')->findOrFail(
                    $previousQuestionAnswerId)->question_template_id);
        }
        $questionTemplate = $questionTemplate->inRandomOrder()->first();
        if ($questionTemplate) {
            $questionAnswer = $questionTemplate->questionAnswers()->where('status', config('constants.ACTIVE'))
                ->select('id', 'question', 'option', 'answer')
                ->inRandomOrder()->first();
        } else {
            throw new NoQuestionFoundException(Lang::get(
                'users.no_question_found'), 1);
        }
        return $questionAnswer;
    }

    /**
     * Get a random question of a learning goal.
     *
     * @param integer $learningGoalTestId LearningGoalTest id
     * @param integer $complexity question complexity
     * @return array
     */
    public function getRandomQuestion($learningGoalTestId, $complexity)
    {
        $questionAnswer = null;
        // accessed questions.
        $accessedQuestions = UserRepository::accessedQuestions($learningGoalTestId);
        // accessed question templates.
        $accessedQuestionTemplates = UserRepository::accessedQuestionTemplates($learningGoalTestId);
        // fetching new question templates by skipping accessed question templates.
        $questionTemplate = $this->questionTemplates()->where('complexity', $complexity)
            ->where('status', config('constants.ACTIVE'))->whereNotIn('question_templates.id', $accessedQuestionTemplates)
            ->inRandomOrder()->first();
        // if no question template is found then fetch any random question template.
        if (!$questionTemplate) {
            $questionTemplate = $this->questionTemplates()->where('complexity',
                $complexity)->where('status', config('constants.ACTIVE'))
                ->inRandomOrder()->first();
        }
        if ($questionTemplate) {
            // fetch any random question of this question template
            $questionAnswers = $questionTemplate->questionAnswers()->where('status', config('constants.ACTIVE'));
            // fetching new questions by skipping accessed questions.
            if ($accessedQuestions) {
                $questionAnswers->whereNotIn('question_answers.id', $accessedQuestions);
            }
            $questionAnswer = $questionAnswers->select('id', 'question', 'option', 'answer')->inRandomOrder()->first();
            // If all the questions are accessed then fetch any random question.
            if (!$questionAnswer) {
                $questionAnswer = $questionTemplate->questionAnswers()->select('id', 'question', 'option', 'answer')->where('status', config('constants.ACTIVE'))->inRandomOrder()->firstOrFail();
            }
            $questionAnswer->type = $questionTemplate->type;
        } else {
            throw new NoQuestionFoundException(Lang::get(
                'users.no_question_found'), 1);
        }
        return $questionAnswer;
    }
}
