<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'mollie_payment_unique_id',
        'subscription_id',
        'user_id',
        'status',
        'payment_sequence_type',
        'license_id',
    ];

    const PENDING = 'pending';
    const SUCCESS = 'success';
    const FAILED = 'failed';
    const OPEN = 'open';

    /**
     * Get the license for a payment.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function license()
    {
        return $this->belongsTo(License::class)
            ->select('licenses.id',
                'licenses.code', 'licenses.course_id', 'licenses.institution_id',
                'licenses.validity', 'licenses.price', 'licenses.status',
                'licenses.end_date');
    }

    /**
     * Save Payment.
     *
     * @param string $data License data
     * @return App\Models\Course
     */
    public static function savePayment($data)
    {
        return static::create($data);
    }

    /**
     * Update Payment.
     *
     * @param string $data License data
     */
    public function updatePayment($data)
    {
        static::update($data);
    }

    /**
     * Query to find available payments.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetPayments($query)
    {
        return $query->addSelect('payments.id', 'payments.mollie_payment_unique_id',
            'payments.subscription_id', 'payments.user_id', 'payments.status', 'payments.payment_sequence_type', 'payments.license_id');
    }

    /**
     * Query to get payment by mollie_payment_unique_id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $mollie_payment_unique_id
     * mollie_payment_unique_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCurrentPayment($mollie_payment_id)
    {
        return static::getPayments()
            ->addSelect('payments.created_at')
            ->where('payments.mollie_payment_unique_id',
                $mollie_payment_id)
            ->first();
    }
}
