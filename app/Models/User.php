<?php

namespace App\Models;

use App\Models\Course;
use App\Models\Institution;
use App\Models\License;
use App\Models\PracticeTestDetail;
use App\Models\Role;
use App\Models\StudentActivity;
use App\Models\StudentFeedback;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\UserActivationNotification;
use App\Services\ImageService;
use App\Services\PermissionService;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone', 'image',
        'slug', 'status', 'role_id', 'activation_token', 'previous_education',
        'dob', 'year',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the role that a user has.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function role()
    {
        return $this->belongsTo(Role::class)->select('roles.id', 'roles.name');
    }

    /**
     * Get the licenses that a user has.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function licenses()
    {
        return $this->belongsToMany(License::class, 'user_licenses', 'user_id',
            'license_id')->select('licenses.id', 'licenses.status', 'licenses.role',
            'licenses.validity', 'licenses.type', 'licenses.end_date')
            ->withPivot('grade_id', 'end_date', 'status', 'subscription_id')
            ->withTimestamps();
    }

    /**
     * Get the student practice tests.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function studentPracticeTestStatuses()
    {
        return $this->belongsToMany(PracticeTestDetail::class,
            'student_practice_test_statuses', 'user_id',
            'practice_test_detail_id')->select('practice_test_details.id',
            'practice_test_details.time', 'practice_test_details.feedback_type',
            'practice_test_details.name', 'practice_test_details.created_by',
            'practice_test_details.slug', 'practice_test_details.start_date',
            'practice_test_details.gmt')->withPivot('status', 'score')
            ->withTimestamps();
    }

    /**
     * Get the student practice tests.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function studentPracticeTestQuestions()
    {
        return $this->belongsToMany(PracticeTestDetail::class,
            'student_practice_test_questions', 'user_id',
            'practice_test_detail_id')->select('practice_test_details.id',
            'practice_test_details.name')->withPivot('id', 'question_answer_id',
            'answer', 'is_correct')->withTimestamps();
    }

    /**
     * Get the activities of a user.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function activities()
    {
        return $this->hasMany(StudentActivity::class)
            ->select('id', 'user_id', 'license_id', 'subject_id',
                'learning_goal_id', 'points');
    }

    /**
     * Get the feedbacks given by the user.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function feedbacks()
    {
        return $this->hasMany(StudentFeedback::class)
            ->select('id', 'status');
    }

    /**
     * Get practice question template created by user.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function practiceTestTemplates()
    {
        return $this->hasMany(PracticeTestTemplate::class)->select(
            'practice_test_templates.id', 'practice_test_templates.name',
            'practice_test_templates.number_of_question',
            'practice_test_templates.template');
    }

    /**
     * Check permission(s) based on user role(s).
     *
     * @param mixed $permissions permission name(s)
     * @return boolean
     */
    public function hasRolePermission($permissions)
    {
        return PermissionService::checkRolePermission(
            $this->role->id, $permissions
        );
    }

    /**
     * Send activation mail.
     *
     * @return void
     */
    public function sendActivationMail()
    {
        $this->notify(new UserActivationNotification(
            route('users.activate', $this->activation_token),
            $this->first_name)
        );
    }

    /**
     * Show full name.
     *
     * @return string
     */
    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Query to only include id and name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetAll($query)
    {
        return $query->addSelect(
            ['users.id', 'first_name', 'last_name', 'email', 'role_id']
        );
    }

    /**
     * Save user.
     *
     * @param array $data User data
     * @return void
     */
    public static function saveUser($data)
    {
        return static::create($data);
    }

    /**
     * Update user.
     *
     * @param array $data User data
     * @return void
     */
    public function updateUser($data)
    {
        if (array_key_exists('image', $data)) {
            $data['image'] = ImageService::save($data['image'], true, $this->slug);
        }
        $this->update($data);
    }

    /**
     * Send reset password link for user.
     *
     * @return boolean
     */
    public function resetPassword()
    {
        $token = str_random(20) . Carbon::now()->timestamp;
        DB::table('password_resets')->insert([
            'email' => $this->email,
            'token' => bcrypt($token),
            'created_at' => Carbon::now(),
        ]);
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Query to only find by slug.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $slug User slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Query to only find by token.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $token User token
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindByToken($query, $token)
    {
        return $query->where('activation_token', $token);
    }

    /**
     * Query to only find inactive status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeinactiveUser($query)
    {
        return $query->where('status', config('constants.INACTIVE'));
    }

    /**
     * Query to only find by email.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindByEmail($query, $email)
    {
        return $query->where('email', $email);
    }

    /**
     * Attach license to user
     *
     * @param integer $licenseId License id
     * @param array $data License data
     * @return void
     */
    public function attachLicense($licenseId, $data)
    {
        $this->licenses()->attach($licenseId, $data);
    }

    /**
     * Change license of a user.
     *
     * @param integer $licenseId License id
     * @return void
     */
    public function changeLicenseStatus($licenseId)
    {
        $userLicense = $this->licenses()->where('license_id', $licenseId)->first();
        if ($userLicense) {
            $status = $userLicense->pivot->status;
            $status = $status === config('constants.INACTIVE')
            ? config('constants.ACTIVE') : config('constants.INACTIVE');
            $userLicense->pivot->status = $status;
            $userLicense->pivot->save();
        }
    }

    /**
     * List of all users for datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @param integer $licenseId License id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getUsersForDatatable($institution, $course, $licenseId)
    {
        $query = static::leftJoin('user_licenses', 'user_licenses.user_id',
            'users.id')->leftJoin('licenses', 'licenses.id',
            'user_licenses.license_id')->leftJoin('courses',
            'licenses.course_id', 'courses.id')->leftJoin('institutions',
            'licenses.institution_id', 'institutions.id')->select('users.first_name',
            'users.last_name', 'users.email', 'users.status', 'users.id')
            ->where('users.role_id', Role::select('id')->where(
                'roles.name', config('constants.USER'))->first()->id)
            ->groupBy('users.id');

        if (Course::findBySlug($course)->count()) {
            $query->where('courses.slug', $course);
        }
        if (Institution::findBySlug($institution)->count()) {
            $query->where('institutions.slug', $institution);
        }
        if (License::find($licenseId)) {
            $query->where('user_licenses.license_id', $licenseId)
                ->where('user_licenses.is_associated', config('constants.NO'));
        }
        return $query;
    }

    /**
     * Add practice test for student.
     *
     * @param  App\Models $practiceTestDetail
     * @return App\Models
     */
    public function addPracticeTestStatuses($practiceTestDetail)
    {
        return $this->practiceTestStatuses()->attach(
            $practiceTestDetail->id);
    }

    /**
     * Add practice test questions for student.
     *
     * @param  App\Models $practiceTestDetail
     * @return void
     */
    public function addPracticeTestQuestions($practiceTestDetail)
    {
        $practiceTestTemplates = json_decode($practiceTestDetail->template, true);
        foreach ($practiceTestTemplates as $key => $practiceTestTemplate) {
            $question = $practiceTestDetail->questionAnswers()->wherePivot(
                'random_key', $key)->inRandomOrder()->first();
            if ($question) {
                $this->practiceTestQuestions()->attach($practiceTestDetail->id,
                    ['question_answer_id' => $question->id]);
            }
        }
    } //end addPracticeTestQuestions()

    /**
     * Get the practice_test_details associated with the student.
     *
     * @return Collection
     */
    public function practiceTestStatuses()
    {
        return $this->belongsToMany(PracticeTestDetail::class,
            'student_practice_test_statuses', 'user_id', 'practice_test_detail_id')
            ->withPivot('status', 'score')->withTimestamps();
    } //end practiceTestStatuses()

    /**
     * Get the practice_test_question associated with the student.
     *
     * @return Collection
     */
    public function practiceTestQuestions()
    {
        return $this->belongsToMany(PracticeTestQuestion::class,
            'student_practice_test_questions', 'user_id', 'practice_test_detail_id')
            ->withPivot('id', 'question_answer_id', 'answer')->withTimestamps();
    } //end practiceTestQuestions()

    public static function institutionUsers($courseId, $institutionId)
    {
        return static::addSelect('users.id',
            'licenses.institution_id', 'licenses.course_id')
            ->join('user_licenses', 'user_licenses.user_id', 'users.id')
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->where('licenses.course_id', $courseId)
            ->where('licenses.institution_id', $institutionId)
            ->where('licenses.role', 'student')
            ->get();
    }

    /**
     * Check for similar license with different role
     *
     * @param App\Models\License $license License model
     * @return boolean
     */
    public function checkForSimilarLicenseWithDifferentRole($license)
    {
        $response = null;
        $licenseWithDifferentRole = $this->licenses()->where('licenses.course_id', $license->course_id)
            ->where('licenses.institution_id', $license->institution_id)
            ->where('licenses.role', '!=', $license->role)->count();
        if ($licenseWithDifferentRole) {
            $response = true;
        } else {
            $response = false;
        }
        return $response;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
