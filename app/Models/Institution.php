<?php

namespace App\Models;

use App\Models\Course;
use App\Models\Grade;
use App\Models\License;
use App\Services\ImageService;
use DB;
use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'image', 'slug'];

    /**
     * Get the courses that belong to the institution.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'institution_courses',
            'institution_id', 'course_id')->select('courses.id',
            'courses.name', 'courses.slug')->withTimestamps();
    }

    /**
     * Get the licenses for the institution.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function licenses()
    {
        return $this->hasMany(License::class)->select('licenses.id',
            'licenses.status', 'licenses.price', 'licenses.validity',
            'licenses.code');
    }

    /**
     * Get the grades for the institution.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function grades()
    {
        return $this->hasMany(Grade::class)->select('grades.id',
            'grades.slug', 'grades.name', 'grades.parent_id', 'grades.year');
    }

    /**
     * Query to only find by slug.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindBySlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Create slug.
     *
     * @param string $name Institution name
     * @return string
     */
    public static function createSlug($name)
    {
        $slug = str_slug($name, '-');
        $rows = static::where('slug', 'like', $slug . '%')->get();
        if ($rows->count()) {
            $row = $rows->last();
            $slug .= '-' . $row->id;
        }
        return $slug;
    }

    /**
     * Scope to get institutions
     *
     * @param App\Model\Subject $query
     * @param string $type
     * @return App\Model\Subject
     */
    public function scopegetAll($query)
    {
        return $query->addSelect(['institutions.id', 'institutions.name',
            'institutions.slug']);
    }

    /**
     * Save institution.
     *
     * @param array $data Institution data
     * @return void
     */
    public static function saveInstitution($data)
    {
        $data['slug'] = static::createSlug($data['name']);
        if (array_key_exists('image', $data)) {
            $data['image'] = ImageService::save($data['image'], false, $data['slug']);
        }
        static::create($data);
    }

    /**
     * Update institution.
     *
     * @param array $data Institution data
     * @return void
     */
    public function updateInstitution($data)
    {
        if (array_key_exists('removeLogo', $data)) {
            $data['image'] = null;
        } elseif (array_key_exists('image', $data)) {
            $data['image'] = ImageService::save($data['image'], false, $this->slug);
        }
        $this->update($data);
    }

    /**
     * List of all institutions for datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getInstitutionForDatatable(
        $institution,
        $course
    ) {
        $query = static::leftJoin('institution_courses',
            'institution_courses.institution_id', 'institutions.id')->leftJoin(
            'courses', 'institution_courses.course_id', 'courses.id')->leftJoin(
            'licenses', function ($join) {
                $join->on('licenses.institution_id',
                    'institution_courses.institution_id')->on(
                    'licenses.course_id', 'institution_courses.course_id');
            })->leftJoin('user_licenses', function ($join) {
                $join->on('licenses.id', 'user_licenses.license_id')
                ->where('user_licenses.is_associated', config('constants.NO'));
            })->select('institutions.image', 'courses.name as course',
            'institutions.name as institution', 'institutions.type',
            'courses.slug as courseSlug', 'institutions.slug',
            'courses.id as courseId', 'institutions.id',
            DB::raw("COUNT(DISTINCT user_licenses.user_id) as users")
        )->groupBy('courses.id')->groupBy('institutions.id')
            ->orderBy('institutions.id')->orderBy('courses.id');

        if (Course::findBySlug($course)->count()) {
            $query->where('courses.slug', $course);
        }
        if (static::findBySlug($institution)->count()) {
            $query->where('institutions.slug', $institution);
        }
        return $query;
    }
}
