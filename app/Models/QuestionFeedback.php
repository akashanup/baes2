<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionFeedback extends Model
{
    protected $fillable = [
        'template',
        'step',
        'question_template_id',
    ];

    protected $table = 'question_feedbacks';

    /**
     * Get the question_template that owns the question_feedback.
     * @return app\Model\QuertionFeedback
     */
    public function question_template()
    {
        return $this->belongsTo(QuestionTemplate::class, 'question_template_id');
    }
    /**
     * Create question feedback
     * @param array $templateFeedbackData
     * @return app\Model\QuertionFeedback
     */

    public static function insertQuestionFeedback($templateFeedbackData)
    {
        return static::create($templateFeedbackData);
    }

    /**
     * Scope to get all question feedbacks
     * @param $query
     * @return App\Model\QuestionFeedback
     */

    public static function scopegetAll($query)
    {
        return $query->addSelect(['question_feedbacks.id', 'question_feedbacks.template', 'question_feedbacks.step', 'question_feedbacks.question_template_id']);
    }

    /**
     * Get current question feedback by
     * @param string $status, $question, $mcqComplexity
     * @param integer $learningGoalId
     * @return app\Model\QuestionTemplate
     */
    public static function getCurrentQuestionFeedback(
        $template,
        $step,
        $questionTemplateId
    ) {
        return static::getAll()
            ->where('question_feedbacks.template', $template)
            ->where('question_feedbacks.step', $step)
            ->where('question_feedbacks.question_template_id', $questionTemplateId)
            ->first();
    }
}
