<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLicense extends Model
{
    protected $table = 'user_licenses';

    protected $fillable = [
        'user_id',
        'license_id',
        'validity',
        'status',
        'subscription_id',
    ];

    /**
     * Save User License.
     *
     * @param string $data License data
     * @return App\Models\Subscription
     */
    public static function saveUserLicense($data)
    {
        return static::create($data);
    }

    /**
     * Update User License.
     *
     * @param string $data UserLicense data
     */
    public static function updateUserLicense($data)
    {
        static::update($data);
    }

    /**
     * Query to find all user license.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetUserLicenses($query)
    {
        return $query->addSelect('user_licenses.user_id',
            'user_licenses.license_id',
            'user_licenses.grade_id', 'user_licenses.validity',
            'user_licenses.status', 'user_licenses.subscription_id');
    }

    /**
     * Query to get User License by subscription_id and
     *    user_id.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $subscriptionId subscription_id
     * @param integer $userId user_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCurrentUserLicense($userId, $licenseId, $subscriptionId)
    {
        $query = static::getUserLicenses()
            ->where('user_licenses.user_id', $userId)
            ->where('user_licenses.license_id', $licenseId);
        if ($subscriptionId) {
            $query = $query->where('user_licenses.subscription_id', $subscriptionId);
        }
        return $query->first();
    }
}
