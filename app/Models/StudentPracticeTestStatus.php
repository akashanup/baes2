<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentPracticeTestStatus extends Model
{
    protected $fillable = ['user_id', 'practice_test_detail_id',
        'status', 'score'];
}
