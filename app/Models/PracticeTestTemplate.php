<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PracticeTestTemplate extends Model
{
    protected $fillable = [
        'name',
        'number_of_question',
        'template',
        'user_id',
        'folder_id',
        'course_id',
        'institution_id',
    ];
    /**
     * Save practice test template.
     *
     * @param string $data PracticeTestTemplate data
     * @return App\Models\PracticeTestTemplate
     */
    public static function saveTestTemplate($data)
    {
        return static::create($data);
    }

    /**
     * Get users from practice test template.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function users()
    {
        return $this->belongsTo(User::class)
            ->select('users.id', 'users.first_name',
                'users.last_name', 'users.email');
    }

    /**
     * scoope to get practice test template.
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopegetAll($query)
    {
        return $query->addSelect('practice_test_templates.id',
            'practice_test_templates.name', 'practice_test_templates.number_of_question',
            'practice_test_templates.template', 'practice_test_templates.user_id');
    }

    /**
     * Get all practice test template by folderId,
     * courseId and institutionId.
     * @param $folderId, $courseId, institutionId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function practiceTestTemplate(
        $courseId,
        $institutionId,
        $folderId
    ) {
        return static::getAll()
            ->where('folder_id', $folderId)
            ->where('course_id', $courseId)
            ->where('institution_id', $institutionId);
    }

    /**
     * Change practice test template folder
     * @param integer $folderId
     * @param array $templateIds
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function changeTestTemplateFolder($templateIds, $folderId)
    {
        return static::whereIn('practice_test_templates.id', $templateIds)
            ->update(['folder_id' => $folderId]);
    }

    /**
     * update test template data
     * @param integer $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function updateTestTemplate($data, $id)
    {
        return static::where('practice_test_templates.id', $id)
            ->update($data);
    }
}
