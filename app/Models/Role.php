<?php

namespace App\Models;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the permissions owned by the role.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function permissions()
    {
        return $this->belongsToMany(
            Permission::class, 'role_permissions', 'role_id', 'permission_id'
        )->select('permissions.id')->withTimestamps();
    }

    /**
     * Get the users for the role.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function users()
    {
        return $this->hasMany(User::class)->select('users.id', 'users.first_name',
            'users.last_name', 'users.email', 'users.image');
    }

    /**
     * Save role.
     *
     * @param array $data Role data
     * @return void
     */
    public static function saveRole($data)
    {
        static::create($data);
    }

    /**
     * Update role.
     *
     * @param array $data Role data
     * @return void
     */
    public function updateRole($data)
    {
        $this->update($data);
    }

    /**
     * Query to only include id and name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetAll($query)
    {
        return $query->select(['id', 'name']);
    }

    /**
     * Role Permissions
     *
     * @return json
     */
    public function rolePermissions()
    {
        $roleId = $this->id;
        return Permission::leftJoin('role_permissions',
            function ($join) use ($roleId) {
                $join->on('permissions.id', 'role_permissions.permission_id')
                    ->where('role_permissions.role_id', $roleId);
            }
        )->select('permissions.id as id', 'permissions.name as name',
            'role_permissions.role_id')->get();
    }

    /**
     * Save Role Permissions
     *
     * @param array $permissions permissions ids
     * @return json
     */
    public function savePermissions($permissions)
    {
        $this->permissions()->sync($permissions);
    }

    /**
     * Query to only include role id
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $roleId role id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetRoleName($query, $roleId)
    {
        return $query->select('name')->where('id', $roleId);
    }
}
