<?php

namespace App\Models;

use App\Events\DeactivateUserMollieSubsctriptionEvent;
use App\Models\Course;
use App\Models\Institution;
use App\Models\Payment;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    const USER_LICENSES = 'user_licenses';

    const TYPE_FIXED = 'fixed';

    const TYPE_MONTHLY = 'monthly';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['validity', 'price', 'status', 'code',
        'course_id', 'institution_id', 'type', 'role',
        'end_date', 'description'];

    /**
     * Get the course that a license has.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function course()
    {
        return $this->belongsTo(Course::class)
            ->select('courses.id', 'courses.name', 'courses.slug');
    }

    /**
     * Get the institution that a license has.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function institution()
    {
        return $this->belongsTo(Institution::class)
            ->select('institutions.id', 'institutions.name');
    }

    /**
     * Get the users that a license has.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_licenses',
            'license_id', 'user_id')->select('users.id', 'users.first_name',
            'users.last_name', 'users.email')->withPivot('grade_id', 'end_date',
            'status', 'subscription_id')->withTimestamps();
    }

    /**
     * Get practice test folder for licenses.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function practiceTestFolders()
    {
        return $this->hasMany(PracticeTestFolder::class)->select(
            'practice_test_folders.id', 'practice_test_folders.name',
            'practice_test_folders.slug',
            'practice_test_folders.description', 'practice_test_folders.user_id');
    }

    /**
     * Get the payments for the license.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function payments()
    {
        return $this->hasMany(Payment::class)->select('payments.id',
            'payments.mollie_payment_unique_id',
            'payments.subscription_id', 'payments.user_id',
            'payments.status', 'payments.license_id');
    }

    /**
     * Save License.
     *
     * @param string $data License data
     * @return App\Models\Course
     */
    public static function saveLicense($data)
    {
        return static::create($data);
    }

    /**
     * Update License.
     *
     * @param string $data License data
     */
    public function updateLicense($data)
    {
        static::update($data);
    }

    /**
     * List of all licenses for datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getLicensesForDatatable($course, $institution)
    {
        $query = static::join('courses', 'licenses.course_id',
            'courses.id')->join('institutions', 'licenses.institution_id',
            'institutions.id')->leftJoin('user_licenses', function ($join) {
                $join->on('licenses.id', 'user_licenses.license_id')
                ->where('user_licenses.is_associated', config('constants.NO'));
            })->leftJoin('course_subjects', 'courses.id', 'course_subjects.course_id')
            ->select('licenses.id', 'courses.name as course', 'courses.slug',
                'licenses.status', 'institutions.slug as institutionSlug',
                'licenses.code', 'licenses.role', 'institutions.name as institution',
                'licenses.validity', 'licenses.end_date', 'licenses.type',
                'licenses.price', 'courses.id as courseId',
                'institutions.id as institutionId',
                DB::raw("COUNT(DISTINCT course_subjects.subject_id) as subjectsCount"),
                DB::raw("COUNT(DISTINCT user_licenses.user_id) as usersCount"))
            ->groupBy('licenses.id')->orderBy('licenses.id');

        if (Course::findBySlug($course)->count()) {
            $query->where('courses.slug', $course);
        }
        if (Institution::findBySlug($institution)->count()) {
            $query->where('institutions.slug', $institution);
        }

        return $query;
    }

    /**
     * Deactivate expired Licenses.
     *
     * @return void
     */
    public static function deactivateExpiredLicenses()
    {
        static::whereNotNull('end_date')
            ->whereDate('end_date', '<=', Carbon::today()->toDateString())
            ->where('status', config('constants.ACTIVE'))
            ->update(['status' => config('constants.INACTIVE')]);
    }

    /**
     * Deactivate yearly expired  Licenses of users'.
     *
     * @return void
     */
    public static function deactivateExpiredYearlyUserLicenses()
    {
        return DB::table(static::USER_LICENSES)
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->whereNotNull('user_licenses.end_date')
            ->whereDate('user_licenses.end_date', '<=', Carbon::today()->toDateString())
            ->where('user_licenses.status', config('constants.ACTIVE'))
            ->where('licenses.type', config('constants.FIXED_PAYMENT'))
            ->update(['user_licenses.status' => config('constants.INACTIVE')]);
    }

    /**
     * Query to find available licenses for a user.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopegetLicenses($query)
    {
        return $query->addSelect('licenses.id', 'licenses.code', 'licenses.type',
            'licenses.validity', 'licenses.end_date', 'licenses.price');
    }

    /**
     * Query to find licenses' data for a user.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function coursesInstitutionsSubjects()
    {
        return static::join('courses', 'licenses.course_id', 'courses.id')
            ->join('institutions', 'licenses.institution_id', 'institutions.id')
            ->leftJoin('course_subjects', 'courses.id',
                'course_subjects.course_id')->leftJoin('subjects',
            'course_subjects.subject_id', 'subjects.id')->addSelect(
            'courses.name as courseName', 'courses.slug as courseSlug',
            'institutions.name as institutionName', 'licenses.description',
            'licenses.role as licenseRole',
            'institutions.slug as institutionSlug', 'institutions.image',
            DB::raw("GROUP_CONCAT(subjects.name SEPARATOR ' | ') as subjects"),
            DB::raw("GROUP_CONCAT(course_subjects.order SEPARATOR ' | ') as subjectOrders"))
            ->groupBy('licenses.id')->groupBy('courses.id')
            ->where('licenses.status', config('constants.ACTIVE'));
    }

    /**
     * Query to find licenses data for a user.
     * If code is found then show licnses for that code plus baes licenses
     * Else only baes licenses.
     *
     * @param string $code License code
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function licensesCoursesInstitutionsSubjects($code = null)
    {
        $query = static::coursesInstitutionsSubjects();
        if ($code) {
            $query->where('licenses.code', $code);
        } else {
            $query->whereNull('licenses.code');
            //->where('institutions.slug', config('constants.BAES_INSTITUTION_SLUG'));
        }
        return $query;
    }

    /**
     * Query to find all licenses for a user.
     *
     * @param string $code License code
     * @param integer $userId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function allLicenses($userId, $code = null)
    {
        return static::licensesCoursesInstitutionsSubjects($code)
            ->getLicenses()->get();
    }

    /**
     * Query to find existing licenses for a user.
     *
     * @param integer $userId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function userLicenses($userId)
    {
        return static::coursesInstitutionsSubjects()
            ->join('user_licenses', function ($join) use ($userId) {
                $join->on('licenses.id', 'user_licenses.license_id')
                    ->where('user_licenses.user_id', $userId)
                    ->where('user_licenses.status', config('constants.ACTIVE'));
            })->getLicenses();
    }

    /**
     * Query to get license by id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $id id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCurrentLicesnce($licenseId)
    {
        return static::getLicenses()
            ->addSelect('licenses.code', 'licenses.role')
            ->where('licenses.id', $licenseId)
            ->first();
    }

    /**
     * Query to get current active user license.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $id id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getCurrentUserLicense($licenseId, $userId)
    {
        return static::addSelect('user_licenses.user_id',
            'user_licenses.license_id', 'user_licenses.subscription_id', 'subscriptions.mollie_subscription_id',
            'subscriptions.mollie_customer_unique_id')
            ->join('user_licenses', 'user_licenses.license_id',
                'licenses.id')
            ->join('subscriptions', 'subscriptions.id',
                'user_licenses.subscription_id')
            ->where('user_licenses.license_id', $licenseId)
            ->where('user_licenses.user_id', $userId)
            ->where('user_licenses.STATUS', config('constants.ACTIVE'))
            ->first();
    }

    /**
     * Query to update user license status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $id id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function updateUserLicenseStatus(
        $licenseId,
        $userId,
        $subscriptionId,
        $date = null
    ) {
        $licenseUpdate = DB::table('user_licenses')
            ->where('user_licenses.license_id', $licenseId)
            ->where('user_licenses.user_id', $userId)
            ->where('user_licenses.subscription_id', $subscriptionId)
            ->where('user_licenses.status', config('constants.ACTIVE'));
        if ($date) {
            $licenseUpdate = $licenseUpdate->update
                (['user_licenses.end_date' => $date]);
        } else {
            $licenseUpdate = $licenseUpdate->update
                (['user_licenses.status' => config('constants.INACTIVE')]);
        }
        return $licenseUpdate;
    }

    /**
     * Query to find all licenses of an institution.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $institutionId Institution id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindByInstitution($query, $institutionId)
    {
        return $query->where('licenses.institution_id', $institutionId);
    }

    /**
     * Query to find all licenses of an course.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $courseId Course id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindByCourse($query, $courseId)
    {
        return $query->where('licenses.course_id', $courseId);
    }

    /**
     * Query to find all licenses of an role.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $role License role
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopefindByRole($query, $role)
    {
        return $query->where('licenses.role', $role);
    }

    /**
     * Deactivate monthly expired  Licenses of users'.
     *
     * @return void
     */
    public static function deactivateExpiredMonthlyUserLicenses()
    {
        $monthlyUserLicenses = DB::table(static::USER_LICENSES)
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->whereNotNull('user_licenses.end_date')
            ->whereDate('user_licenses.end_date', '<', Carbon::today()->toDateString())
            ->where('user_licenses.status', config('constants.ACTIVE'))
            ->where('licenses.type', config('constants.MONTHLY_PAYMENT'))
            ->get();
        foreach ($monthlyUserLicenses as $monthlyUserLicense) {
            $monthlyUserLicense = json_encode($monthlyUserLicense);
            $monthlyUserLicense = json_decode($monthlyUserLicense, true);
            License::updateUserLicenseStatus($monthlyUserLicense['license_id'],
                $monthlyUserLicense['user_id'],
                $monthlyUserLicense['subscription_id'], $date = null);
            if ($monthlyUserLicense['subscription_id']) {
                $subscription = Subscription::getSubscriptionByMollieId($monthlyUserLicense['subscription_id']);
                if ($subscription) {
                    event(new DeactivateUserMollieSubsctriptionEvent($subscription));
                }
            }
        }
    }
}
