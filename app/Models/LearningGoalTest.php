<?php

namespace App\Models;

use App\Exceptions\InvalidAttemptException;
use App\Models\QuestionAnswer;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Lang;

class LearningGoalTest extends Model
{
    const ZERO_POINT = 0;
    const POINTS = [10, 8, 5, 2];
    /**
     * Get the test that owns the question_answer.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function questionAnswers()
    {
        return $this->belongsToMany(QuestionAnswer::class,
            'learning_goal_test_question_answers', 'learning_goal_test_id',
            'question_answer_id')->withPivot('id', 'answer', 'points',
            'score_calculated')->select('question_answers.question',
            'question_answers.answer')->withTimestamps();
    }

    /**
     * Assign student learning goal
     *
     * @param object $studentLearningGoal
     * @return void
     */
    public function assignStudentLearningGoal($studentLearningGoal)
    {
        DB::Table('student_learning_goals')->where('user_id', auth()->user()->id)
            ->where('subject_id', $studentLearningGoal->subject_id)
            ->where('learning_goal_id', $studentLearningGoal->learning_goal_id)
            ->where('license_id', $studentLearningGoal->license_id)
            ->update(['learning_goal_test_id' => $this->id,
                'updated_at' => Carbon::now()]);
    }

    /**
     * Assign student learning goal
     *
     * @param integer $questionId QuestionAnswer id
     * @param string $answer answer
     * @param boolean $correctAnswer correctAnswer
     * @return integer
     */
    public function updatStudentLearningGoalTest(
        $questionId,
        $answer,
        $correctAnswer
    ) {
        $studentLearningGoalTest = $this->questionAnswers()
            ->wherePivot('question_answer_id', $questionId)->get()->last();
        $studentLearningGoalTestAnswer = $studentLearningGoalTest->pivot->answer;
        $studentLearningGoalTestAnswer = $studentLearningGoalTestAnswer
        ? json_decode($studentLearningGoalTestAnswer, true) : [];
        $attempts = sizeof($studentLearningGoalTestAnswer);
        if ($attempts < 4) {
            $attempts++;
            $studentLearningGoalTestAnswer[$attempts] = $answer;
            $studentLearningGoalTest->pivot->answer = json_encode(
                $studentLearningGoalTestAnswer);
            //Assign points
            if ($correctAnswer) {
                $studentLearningGoalTest->pivot->points =
                static::POINTS[$attempts - 1];
            } else {
                $studentLearningGoalTest->pivot->points = static::ZERO_POINT;
            }
            $studentLearningGoalTest->pivot->score_calculated =
                config('constants.NO');
            $studentLearningGoalTest->pivot->save();
        } else {
            throw new InvalidAttemptException(
                Lang::get('students.invalid_attempt'), 1);
        }
        return $attempts;
    }
}
