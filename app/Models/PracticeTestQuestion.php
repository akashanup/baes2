<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PracticeTestQuestion extends Model
{
    protected $fillable = [
        'question_answer_id',
        'practice_test_detail_id',
        'random_key',
        'created_at',
        'updated_at',
    ];

    /**
     * Get the practice test detail.
     *
     * @return App\Models
     */
    public function practiceTestDetail()
    {
        return $this->belongsTo(PracticeTestDetail::class);
    } //end practiceTestDetail()

    /**
     * Get the student associated with the practice_test_question.
     *
     * @return App\Models
     */
    public function students()
    {
        return $this->belongsToMany(Student::class, 'student_practice_test_questions', 'user_id', 'practice_test_question_id')->withPivot('id', 'answer');
    } //end students()
}
