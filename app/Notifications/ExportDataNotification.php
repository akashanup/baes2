<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class ExportDataNotification extends Notification
{
    use Queueable;

    /**
     * The file location
     *
     * @var string
     */
    public $url;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * Array
     *
     * @var array
     */
    public $tables;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url, $name, $tables)
    {
        $this->url = $url;
        $this->name = $name;
        $this->tables = $tables;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.exportData', ['tables' => $this->tables])
            ->subject(Lang::get('emails.welcome_to_baes'))
            ->greeting(Lang::get('emails.hello') . ' ' . $this->name . '!')
            ->line(Lang::get('admins.export_data_message'))
            ->level('success')
            ->action(Lang::get('admins.export_data_1'), $this->url)
            ->line(Lang::get('emails.sincerely'))
            ->salutation('BAES Education');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
