<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;
use Log;

class SuccessfulPaymentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The payment object.
     *
     * @var string
     */
    public $payment;

    /**
     * The license object.
     *
     * @var string
     */
    public $license;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * The course object.
     *
     * @var string
     */
    public $course;

    /**
     * The License overview link.
     *
     * @var string
     */
    public $licenseOverviewLink;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        $payment,
        $license,
        $name,
        $course,
        $licenseOverviewLink
    ) {
        $this->payment = $payment;
        $this->license = $license;
        $this->name = $name;
        $this->course = $course;
        $this->licenseOverviewLink = $licenseOverviewLink;
        Log::info("in constructor");
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Log::info("in two mail function");
        return (new MailMessage)
            ->markdown('emails.confirm_payment',
                [
                    'payment' => $this->payment,
                    'license' => $this->license,
                    'course' => $this->course,
                    'sincerely' => Lang::get('emails.sincerely'),
                ]
            )
            ->subject(Lang::get('emails.success_payment'))
            ->greeting(Lang::get('emails.hello') . ' ' . $this->name . '!')
            ->line(Lang::get('emails.thankyou_payment_message'))
            ->line(Lang::get('emails.payment_details') . ':')
            ->level('success')->action(Lang::get('emails.go_to_module'),
            $this->licenseOverviewLink)
            ->salutation('BAES Education');
    }
}
