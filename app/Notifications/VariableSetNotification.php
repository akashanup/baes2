<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class VariableSetNotification extends Notification
{
    use Queueable;

    /**
     * LearningGoal name
     *
     * @var string
     */
    public $learningGoalName;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * The message.
     *
     * @var string
     */
    public $message;

    /**
     * File.
     *
     * @var string
     */
    public $file;

    /**
     * Status.
     *
     * @var string
     */
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($learningGoal, $name, $message, $status, $file = null)
    {
        $this->name = $name;
        $this->learningGoalName = $learningGoal;
        $this->message = $message;
        $this->file = $file;
        $this->status = $status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->markdown('emails.exportVariableSet')
            ->subject($this->learningGoalName . Lang::get('admins.variable_set_upload') . $this->status)
            ->greeting(Lang::get('emails.hello') . ' ' . $this->name . '!')
            ->line(Lang::get('admins.variable_set_upload_message') . ' ' . $this->learningGoalName)
            ->line($this->message)
            ->line(Lang::get('emails.sincerely'))
            ->salutation('BAES Education');
        if ($this->file) {
            $mail->attach($this->file);
        }
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
