<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class PdfDownloadMail extends Notification
{
    use Queueable;

    /**
     * PDF file link.
     *
     * @return void
     */

    public $pdfLink;

    /**
     * Constructor to initialize values
     * @param string $fileUrl
     * @return void
     */

    public function __construct($fileUrl)
    {
        $this->pdfLink = $fileUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.email')
            ->subject(Lang::get('admins.welcome_to_baes'))
            ->line(Lang::get('admins.export_data_message'))
            ->action(Lang::get('admins.export_data'), url($this->pdfLink))
            ->line(Lang::get('admins.sincerely'))
            ->salutation('BAES Education');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
