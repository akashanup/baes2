<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class sendMakePaymentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Payment Link.
     *
     * @return void
     */

    public $paymentLink;

    /**
     * User first name.
     *
     * @return void
     */

    public $firstName;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($paymentLink, $firstName)
    {
        $this->paymentLink = $paymentLink;
        $this->firstName = $firstName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.payment')
            ->subject(Lang::get('emails.failed_payment'))
            ->greeting(Lang::get('emails.hello') . ' ' . $this->firstName . '!')
            ->line(Lang::get('emails.payment_message'))
            ->level('success')->action(Lang::get('emails.payment_button'), $this->paymentLink)
            ->line(Lang::get('emails.sincerely'))
            ->salutation('BAES Education');
    }
}
