<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class UserActivationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $name)
    {
        $this->token = $token;
        $this->name = $name;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    } //end via()

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.email')
            ->subject(Lang::get('emails.welcome_to_baes'))
            ->greeting(Lang::get('emails.hello') . ' ' . $this->name . '!')
            ->line(Lang::get('emails.activation_message'))
            ->level('success')->action(
            Lang::get('emails.activate_account_button'), $this->token)
            ->line(Lang::get('emails.one_time_link'))
            ->line(Lang::get('emails.sincerely'))
            ->salutation('BAES Education');
    } //end toMail()

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
