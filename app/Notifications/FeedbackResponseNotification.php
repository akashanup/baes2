<?php

namespace App\Notifications;

use App\Models\QuestionAnswer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class FeedbackResponseNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * StudentFeedback model
     *
     * @var App\Models\StudentFeedback
     */
    public $studentFeedback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($name, $studentFeedback)
    {
        $this->name = $name;
        $this->studentFeedback = $studentFeedback;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $learningGoalName = $this->studentFeedback->question_answer_id
        ? QuestionAnswer::findOrFail(
            $this->studentFeedback->question_answer_id)
            ->questionTemplate->learningGoal->name : null;

        $body = Lang::get('emails.feedback_mail_body');
        $subBody = null;
        switch ($this->studentFeedback->response_email_type) {
            case config('constants.PROCESSED'):
                $subBody = Lang::get('emails.feedback_mail_processed_body');
                break;
            case config('constants.ROUNDING'):
                $subBody = Lang::get('emails.feedback_mail_rounding_body');
                break;
            case config('constants.INCORRECT'):
                $subBody = Lang::get('emails.feedback_mail_incorrect_body');
                break;
            case config('constants.CUSTOM'):
                $subBody = $this->studentFeedback->note;
                break;
            default:
                # code...
                break;
        }
        return (new MailMessage)->markdown(
            'emails.feedback-response', [
                'feedback' => $this->studentFeedback->message,
                'your_feedback' =>
                Lang::get('emails.feedback_mail_your_feedback'),
                'body' => $body,
                'subBody' => $subBody,
                'sincerely' => Lang::get('emails.sincerely'),
            ])
            ->subject(Lang::get('emails.feedback_mail_subject'))
            ->greeting(Lang::get('emails.feedback_mail_dear')
                . ' ' . $this->name . ',')
            ->line($learningGoalName
                ? Lang::get('emails.feedback_mail_learning_goal')
                . ' ' . $learningGoalName
                : Lang::get('emails.feedback_mail_no_learning_goal')
            )
            ->salutation('BAES Education');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
