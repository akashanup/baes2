<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\LearningGoal;
use App\Observers\Observer;
use Illuminate\Validation\Rule;
use Lang;

class LearningGoalObserver extends Observer
{
    const LEARNING_GOAL = 'LearningGoal';
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = [
        'name' => 'required|string|max:100',
        'theory' => 'required|string',
        'example' => 'required|string',
        'video' => 'required|json',

    ];

    /**
     * Listen to the LearningGoal creating event.
     *
     * @param  \App\Models\LearningGoal  $learningGoal
     * @return void
     */
    public function creating(LearningGoal $learningGoal)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:120|unique:learning_goals']);
        $learningGoal->slug = $this->createSlug(static::LEARNING_GOAL,
            $learningGoal->name);
        $learningGoal = $this->validateLearningGoal($learningGoal);
    }

    /**
     * Listen to the LearningGoal updating event.
     *
     * @param  \App\Models\LearningGoal  $learningGoal
     * @return void
     */
    public function updating(LearningGoal $learningGoal)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:120']);
        // Check whether the slug is unique.
        if (LearningGoal::findBySlug($learningGoal->slug)
            ->where('id', '!=', $learningGoal->id)->count()) {
            throw new ModelValidationException(
                Lang::get('globals.duplicate_slug_found'), 1);
        }
        $learningGoal = $this->validateLearningGoal($learningGoal);
    }

    /**
     * Validates learningGoal.
     *
     * @param  \App\Models\LearningGoal  $learningGoal
     * @return \App\Models\LearningGoal  $learningGoal
     */
    private function validateLearningGoal($learningGoal)
    {
        $validationResponse = $this->validate($learningGoal->toArray(),
            $this->rules);
        if ($validationResponse) {
            $learningGoal->name = trim($learningGoal->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::LEARNING_GOAL), 1);
        }
        return $learningGoal;
    }
}
