<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Course;
use App\Observers\Observer;
use Lang;

class CourseObserver extends Observer
{
    const COURSE = 'Course';

    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = ['name' => 'required|string|max:50'];

    /**
     * Listen to the Course creating event.
     *
     * @param  \App\Models\Course  $course
     * @return void
     */
    public function creating(Course $course)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60|unique:courses']);

        $course->slug = $this->createSlug(static::COURSE, $course->name);
        $course = $this->validateCourse($course);
    }

    /**
     * Listen to the Course updating event.
     *
     * @param  \App\Models\Course  $course
     * @return void
     */
    public function updating(Course $course)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60']);
        // Check whether the slug is unique.
        if (Course::findBySlug($course->slug)->where('id', '!=', $course->id)
            ->count()) {
            throw new ModelValidationException(
                Lang::get('globals.duplicate_slug_found'), 1);
        }
        $course = $this->validateCourse($course);
    }

    /**
     * Validates course.
     *
     * @param  \App\Models\Course  $course
     * @return \App\Models\Course  $course
     */
    private function validateCourse($course)
    {
        if ($this->validate($course->toArray(), $this->rules)) {
            $course->name = trim($course->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::COURSE), 1);
        }
        return $course;
    }
}
