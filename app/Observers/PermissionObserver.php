<?php

namespace App\Observers;

use App\Events\AssignPermissionToSuperAdminEvent;
use App\Exceptions\ModelValidationException;
use App\Models\Permission;
use App\Observers\Observer;

class PermissionObserver extends Observer
{
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    private $rules = [
        'name' => 'required|string|max:100',
        'detail' => 'nullable|text',
    ];

    /**
     * Listen to the Permission saving event.
     *
     * @param  \App\Models\Permission  $permission
     * @return void
     */
    public function saving(Permission $permission)
    {
        $validationResponse = $this->validate(
            $permission->toArray(), $this->rules
        );
        if ($validationResponse) {
            $permission->name = trim($permission->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage('Permission'), 1
            );
        }
    }

    /**
     * Listen to the Permission created event.
     *
     * @param  \App\Models\Permission  $permission
     * @return void
     */
    public function created(Permission $permission)
    {
        // Assign the permission to super-admin
        event(new AssignPermissionToSuperAdminEvent($permission));
    }
}
