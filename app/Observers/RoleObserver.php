<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Role;
use App\Observers\Observer;

class RoleObserver extends Observer
{
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = ['name' => 'required|string|max:25'];

    /**
     * Listen to the Role saving event.
     *
     * @param  \App\Models\Role  $role
     * @return void
     */
    public function saving(Role $role)
    {
        $validationResponse = $this->validate(
            $role->toArray(), $this->rules
        );
        if ($validationResponse) {
            $role->name = trim($role->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage('Role'), 1
            );
        }
    }
}
