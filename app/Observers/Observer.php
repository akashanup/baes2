<?php

namespace App\Observers;

use Illuminate\Support\Facades\Validator;
use Lang;
use Log;

class Observer
{
    /**
     * Validation errors.
     *
     * @var array
     */
    protected $errors;

    /**
     * Validate a model.
     *
     * @param array $data Data to be validated
     * @param array $rules Rules to be followed
     * @param array $messages Error messages to be set if validation fails
     * @return void
     */
    protected function validate($data, $rules, $messages = [])
    {
        $validationResponse = true;
        // make a new validator object
        $validation = Validator::make($data, $rules, $messages);
        // check for failure
        if ($validation->fails()) {
            // set errors and return false
            $this->errors = $validation->errors();
            Log::error($this->errors);
            $validationResponse = false;
        }
        return $validationResponse;
    }

    /**
     * Validation failed message.
     *
     * @param string $model Model name
     * @return string
     */
    protected function validationFailedMessage($model)
    {
        return Lang::get('globals.validation_failed') . $model;
    }

    /**
     * Create slug.
     *
     * @param string $name Model name
     * @return string
     */
    protected function createSlug($model, $name)
    {
        $slug = str_slug($name, '-');
        $rows = ("\App\Models\\$model")::where('slug', 'like', $slug . '%')->get();
        if ($rows->count()) {
            $row = $rows->last();
            $slug .= '-' . $row->id;
        }
        return $slug;
    }
}
