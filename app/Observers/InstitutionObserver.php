<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Institution;
use App\Observers\Observer;
use Illuminate\Validation\Rule;
use Lang;

class InstitutionObserver extends Observer
{
    const INSTITUTION = 'Institution';

    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = [
        'name' => 'required|string|max:50',
        'image' => 'nullable|string|max:100',
    ];

    /**
     * Listen to the Institution creating event.
     *
     * @param  \App\Models\Institution  $institution
     * @return void
     */
    public function creating(Institution $institution)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60|unique:institutions']);
        $institution = $this->validateInstitution($institution);
    }

    /**
     * Listen to the Institution updating event.
     *
     * @param  \App\Models\Institution  $institution
     * @return void
     */
    public function updating(Institution $institution)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60']);
        // Check whether the slug is unique.
        if (Institution::findBySlug($institution->slug)
            ->where('id', '!=', $institution->id)->count()) {
            throw new ModelValidationException(
                Lang::get('globals.duplicate_slug_found'), 1);
        }
        $institution = $this->validateInstitution($institution);
    }

    /**
     * Listen to the Institution created event.
     * Create a default grade for the institution.
     *
     * @param  \App\Models\Institution  $institution
     * @return void
     */
    public function created(Institution $institution)
    {
        $institution->grades()->create([
            'name' => config('constants.DEFAULT_CLASS'),
        ]);
    }

    /**
     * Validates institution.
     *
     * @param  \App\Models\Institution  $institution
     * @return \App\Models\Institution  $institution
     */
    private function validateInstitution($institution)
    {
        $validationResponse = $this->validate(
            $institution->toArray(), array_merge($this->rules, ['type' =>
                ['required', Rule::in([config('constants.SCHOOL'),
                    config('constants.ORGANISATION')])]])
        );
        if ($validationResponse) {
            $institution->name = trim($institution->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::INSTITUTION), 1);
        }
        return $institution;
    }
}
