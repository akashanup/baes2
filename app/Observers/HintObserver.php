<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Hint;
use App\Observers\Observer;

class HintObserver extends Observer
{
    const HINT = 'Hint';

    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = ['hint' => 'nullable|string',
        'question_answer_id' => 'required|integer'];

    /**
     * Listen to the Hint saving event.
     *
     * @param  \App\Models\Hint  $hint
     * @return void
     */
    public function saving(Hint $hint)
    {
        if (!$this->validate($hint->toArray(), $this->rules)) {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::HINT), 1
            );
        }
    }
}
