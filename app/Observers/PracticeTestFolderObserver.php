<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\PracticeTestFolder;
use App\Observers\Observer;
use Illuminate\Validation\Rule;

class PracticeTestFolderObserver extends Observer
{
    const PRACTICE_TEST_FOLDER = 'PracticeTestFolder';
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = [
        'name' => 'required|string|max:60',
        'description' => 'required|string',
        'license_id' => 'required|integer',
        'user_id' => 'required|integer',
        'institution_id' => 'required|integer',
        'course_id' => 'required|integer',

    ];

    /**
     * Listen to the PracticeTestFolder creating event.
     *
     * @param  \App\Models\PracticeTestFolder  $practiceTestFolder
     * @return void
     */
    public function creating(PracticeTestFolder $practiceTestFolder)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60|unique:learning_goals']);
        $practiceTestFolder->slug = $this->createSlug(static::PRACTICE_TEST_FOLDER,
            $practiceTestFolder->name);
        $practiceTestFolder = $this->validatePracticeTestFolder($practiceTestFolder);
    }

    /**
     * Validates practiceTestFolder.
     *
     * @param  \App\Models\PracticeTestFolder  $practiceTestFolder
     * @return \App\Models\PracticeTestFolder  $practiceTestFolder
     */
    private function validatePracticeTestFolder($practiceTestFolder)
    {
        $validationResponse = $this->validate($practiceTestFolder->toArray(),
            $this->rules);
        if ($validationResponse) {
            $practiceTestFolder->name = trim($practiceTestFolder->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::PRACTICE_TEST_FOLDER), 1);
        }
        return $practiceTestFolder;
    }
}
