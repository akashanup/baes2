<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\License;
use App\Observers\Observer;
use Illuminate\Validation\Rule;

class LicenseObserver extends Observer
{
    const LICENSE = 'License';
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = ['code' => 'nullable|string|max:25',
        'course_id' => 'required|integer', 'institution_id' =>
        'required|integer', 'validity' => 'nullable|integer',
        'price' => 'required|integer', 'end_date' => 'nullable|date',
    ];

    /**
     * Listen to the Course saving event.
     *
     * @param  \App\Models\Course  $course
     * @return void
     */
    public function saving(License $license)
    {
        $validationResponse = $this->validate(
            $license->toArray(), array_merge($this->rules, [
                'type' => ['required',
                    Rule::in([config('constants.MONTHLY_PAYMENT'),
                        config('constants.FIXED_PAYMENT')]),
                ],
                'status' => ['required',
                    Rule::in([config('constants.ACTIVE'),
                        config('constants.INACTIVE')]),
                ],
                'role' => ['required',
                    Rule::in([config('constants.STUDENT'),
                        config('constants.TEACHER')]),
                ],
            ])
        );
        if (!$validationResponse) {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::LICENSE), 1
            );
        }
    }
}
