<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Grade;
use App\Observers\Observer;
use Lang;

class GradeObserver extends Observer
{
    const GRADE = 'Grade';

    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = ['name' => 'required|string|max:50',
        'year' => 'nullable|string|max:4',
        'institution_id' => 'required|integer',
        'parent_id' => 'nullable|integer',
    ];

    /**
     * Listen to the Grade creating event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function creating(Grade $grade)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60|unique:grades']);
        $grade->slug = $this->createSlug(static::GRADE, $grade->name);
        $grade = $this->validateGrade($grade);
    }

    /**
     * Listen to the Grade updating event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function updating(Grade $grade)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:60']);

        // Check whether the slug is unique.
        if (Grade::findBySlug($grade->slug)->where('id', '!=', $grade->id)
            ->count()) {
            throw new ModelValidationException(
                Lang::get('globals.duplicate_slug_found'), 1);
        }
        $grade = $this->validateGrade($grade);
    }

    /**
     * Validates grade.
     *
     * @param  \App\Models\Grade  $grade
     * @return \App\Models\Grade  $grade
     */
    private function validateGrade($grade)
    {
        if ($this->validate($grade->toArray(), $this->rules)) {
            $grade->name = trim($grade->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::GRADE), 1);
        }
        return $grade;
    }
}
