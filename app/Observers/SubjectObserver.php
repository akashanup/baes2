<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Subject;
use App\Observers\Observer;
use Illuminate\Validation\Rule;

class SubjectObserver extends Observer
{
    const SUBJECT = 'Subject';
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    public $rules = [
        'name' => 'required|string|max:100',
        'curriculum_id' => 'nullable|integer',
    ];

    /**
     * Listen to the Subject creating event.
     *
     * @param  \App\Models\Subject  $subject
     * @return void
     */
    public function creating(Subject $subject)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:120|unique:subjects']);
        $subject->slug = $this->createSlug(static::SUBJECT, $subject->name);
        $subject = $this->validateSubject($subject);
    }

    /**
     * Listen to the Subject updating event.
     *
     * @param  \App\Models\Subject  $subject
     * @return void
     */
    public function updating(Subject $subject)
    {
        $this->rules = array_merge($this->rules, [
            'slug' => 'required|string|max:120']);
        // Check whether the slug is unique.
        if (Subject::findBySlug($subject->slug)->where('id', '!=', $subject->id)
            ->count()) {
            throw new ModelValidationException(
                Lang::get('globals.duplicate_slug_found'), 1);
        }
        $subject = $this->validateSubject($subject);
    }

    /**
     * Validates subject.
     *
     * @param  \App\Models\Subject  $subject
     * @return \App\Models\Subject  $subject
     */
    private function validateSubject($subject)
    {
        $validationResponse = $this->validate($subject->toArray(),
            array_merge($this->rules, ['type' => ['required',
                Rule::in([Subject::SUBJECT, Subject::CURRICULUM])]])
        );
        if ($validationResponse) {
            $subject->name = trim($subject->name);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::SUBJECT), 1);
        }
        return $subject;
    }
}
