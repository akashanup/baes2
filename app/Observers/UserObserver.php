<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\Role;
use App\Models\User;
use App\Observers\Observer;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Lang;

class UserObserver extends Observer
{
    const USER = 'User';
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    private $rules = [
        'first_name' => 'required|string|max:50',
        'last_name' => 'required|string|max:50',
        'phone' => 'required|string|max:17',
        'password' => 'nullable|string|max:255',
        'image' => 'nullable|string|max:100',
        'role_id' => 'required|integer',
        'activation_token' => 'nullable|string|max:30',
        'previous_education' => 'nullable|string|max:50',
        'dob' => 'nullable|string|max:10',
        'year' => 'required|string|max:4',
    ];

    /**
     * Listen to the User creating event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function creating(User $user)
    {
        $this->rules = array_merge($this->rules, [
            'email' => 'required|email|max:50|unique:users',
            'slug' => 'required|string|max:100|unique:users']);

        $user->activation_token = str_random(15) . Carbon::now()->timestamp;
        $user->slug = $this->createSlug(static::USER,
            ($user->first_name . $user->last_name));
        $user->role_id = Role::where('name', config('constants.USER'))
            ->first()->id;
        $user->status = config('constants.INACTIVE');
        $user->year = Carbon::now()->year . '';
        $user = $this->validateUser($user);
    }

    /**
     * Listen to the User updating event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updating(User $user)
    {
        $this->rules = array_merge($this->rules, [
            'email' => 'required|email|max:50',
            'slug' => 'required|string|max:100']);

        // Check whether the new email and slug is unique.
        if (User::findByEmail($user->email)->where('id', '!=', $user->id)
            ->count()) {
            throw new ModelValidationException(
                Lang::get('users.duplicate_email_found'), 1);
        }
        if (User::findBySlug($user->slug)->where('id', '!=', $user->id)
            ->count()) {
            throw new ModelValidationException(
                Lang::get('globals.duplicate_slug_found'), 1);
        }
        $user = $this->validateUser($user);
    }

    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        // Send activation mails.
        $user->sendActivationMail();
    }

    /**
     * Validates user.
     *
     * @param  \App\Models\User  $user
     * @return \App\Models\User  $user
     */
    private function validateUser($user)
    {
        if ($this->validate(array_merge($user->toArray(),
            ['password' => $user->password]), array_merge($this->rules,
            ['status' => ['required', Rule::in([User::ACTIVE,
                User::INACTIVE])]]))) {
            $user->first_name = trim($user->first_name);
            $user->last_name = trim($user->last_name);
            $user->email = trim($user->email);
        } else {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::USER), 1);
        }
        return $user;
    }
}
