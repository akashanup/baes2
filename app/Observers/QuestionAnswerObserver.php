<?php

namespace App\Observers;

use App\Exceptions\ModelValidationException;
use App\Models\QuestionAnswer;
use App\Observers\Observer;
use Illuminate\Validation\Rule;

class QuestionAnswerObserver extends Observer
{
    const QUESTION_ANSWER = 'QuestionAnswer';
    /**
     * The rules that should be followed for creating and updating.
     *
     * @var array
     */
    private $rules = [
        'question' => 'required|string',
        'option' => 'nullable|string',
        'answer' => 'required|string',
        'question_template_id' => 'required|integer',
        'template_variable_id' => 'required|integer',
        'upper_bound' => 'nullable|string',
        'lower_bound' => 'nullable|string',
    ];

    /**
     * Listen to the QuestionAnswer saving event.
     *
     * @param  \App\Models\QuestionAnswer  $questionAnswer
     * @return void
     */
    public function saving(QuestionAnswer $questionAnswer)
    {
        $validationResponse = $this->validate(
            array_merge($questionAnswer->toArray(),
                ['answer' => $questionAnswer->answer,
                    'upper_bound' => $questionAnswer->upper_bound,
                    'lower_bound' => $questionAnswer->lower_bound]),
            array_merge($this->rules,
                ['status' => ['required', Rule::in([config('constants.ACTIVE'),
                    config('constants.INACTIVE')])]])
        );
        if (!$validationResponse) {
            throw new ModelValidationException(
                $this->validationFailedMessage(static::QUESTION_ANSWER), 1
            );
        }
    }
}
