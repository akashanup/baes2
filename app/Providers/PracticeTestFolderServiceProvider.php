<?php

namespace App\Providers;

use App\Models\PracticeTestFolder;
use App\Observers\PracticeTestFolderObserver;
use Illuminate\Support\ServiceProvider;

class PracticeTestFolderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        PracticeTestFolder::observe(PracticeTestFolderObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
