<?php

namespace App\Providers;

use App\Models\Hint;
use App\Observers\HintObserver;
use Illuminate\Support\ServiceProvider;

class HintServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Hint::observe(HintObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
