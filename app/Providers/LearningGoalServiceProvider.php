<?php

namespace App\Providers;

use App\Models\LearningGoal;
use App\Observers\LearningGoalObserver;
use Illuminate\Support\ServiceProvider;

class LearningGoalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        LearningGoal::observe(LearningGoalObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
