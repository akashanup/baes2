<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\AssignPermissionToSuperAdminEvent' => [
            'App\Listeners\AssignPermissionToSuperAdminEventListener',
        ],
        'App\Events\AssignLearningGoalToSubjectEvent' => [
            'App\Listeners\AssignLearningGoalToSubjectEventListener',
        ],
        'App\Events\AssignInstitutionToCourseEvent' => [
            'App\Listeners\AssignInstitutionToCourseEventListener',
        ],
        'App\Events\AssignSubjectToCourseEvent' => [
            'App\Listeners\AssignSubjectToCourseEventListener',
        ],
        'App\Events\AssignSubjectToLearningGoalEvent' => [
            'App\Listeners\AssignSubjectToLearningGoalEventListener',
        ],
        'App\Events\AssignLearningGoalToUserEvent' => [
            'App\Listeners\AssignLearningGoalToUserEventListener',
        ],
        'App\Events\CreateLearningGoalTestEvent' => [
            'App\Listeners\CreateLearningGoalTestEventListener',
        ],
        'App\Events\AssignQuestionAnswerToLearningGoalTestEvent' => [
            'App\Listeners\AssignQuestionAnswerToLearningGoalTestEventListener',
        ],
        'App\Events\CalculateLearningGoalScoreEvent' => [
            'App\Listeners\CalculateLearningGoalScoreEventListener',
        ],
        'App\Events\UpdateStudentLearningGoalEvent' => [
            'App\Listeners\UpdateStudentLearningGoalEventListener',
        ],
        'App\Events\CreateLicenseDefaultFolderEvent' => [
            'App\Listeners\CreateLicenseDefaultFolderEventListener',
        ],
        'App\Events\AddActivityEvent' => [
            'App\Listeners\AddActivityEventListener',
        ],
        'App\Events\AssignPracticeTestToStudentsEvent' => [
            'App\Listeners\AssignPracticeTestToStudentsEventListener',
        ],
        'App\Events\AssignActivePracticeTestsToUserEvent' => [
            'App\Listeners\AssignActivePracticeTestsToUserEventListener',
        ],
        'App\Events\AssociateStudentLicenseToTeacherLicenseEvent' => [
            'App\Listeners\AssociateStudentLicenseToTeacherLicenseEventListener',
        ],
        'App\Events\DeactivateUserMollieSubsctriptionEvent' => [
            'App\Listeners\DeactivateUserMollieSubsctriptionEventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
