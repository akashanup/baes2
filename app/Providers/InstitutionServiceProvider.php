<?php

namespace App\Providers;

use App\Models\Institution;
use App\Observers\InstitutionObserver;
use Illuminate\Support\ServiceProvider;

class InstitutionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Institution::observe(InstitutionObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
