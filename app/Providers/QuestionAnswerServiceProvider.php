<?php

namespace App\Providers;

use App\Models\QuestionAnswer;
use App\Observers\QuestionAnswerObserver;
use Illuminate\Support\ServiceProvider;

class QuestionAnswerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        QuestionAnswer::observe(QuestionAnswerObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
