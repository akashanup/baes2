<?php

namespace App\Providers;

use App\Models\Course;
use App\Models\Grade;
use App\Models\Institution;
use App\Models\License;
use App\Services\DashboardService;
use App\Services\LearningGoalRenderService;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use DB;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
	if(config('app.REDIRECT_HTTPS', 'http') == config('constants.HTTPS')){
	    $url->forceScheme('https');
	}

        view()->composer('*', function ($view) {
            $view->with(['version' => config('app.version', 1)]);
        });
        view()->composer('includes.logo', function ($view) {
            $view->with([
                'home' => DashboardService::dashboardRoute(),
            ]);
        });
        view()->composer(['includes.header', 'includes.student-header',
            'includes.teacher-header'], function ($view) {
                $user = auth()->user();
                $view->with([
                'dashboard' => DashboardService::dashboardRoute(),
                'userImage' => Storage::url($user->image),
                'userName' => $user->name(),
                'userRole' => $user->role->name,
                'userSlug' => $user->slug,
                ]);
            }
        );
        view()->composer('includes.student-header', function ($view) {
            $license = request()->route('license');
            $course = $license->course;
            $studentLicense = auth()->user()->licenses()->where('licenses.id', $license->id)
                ->select('licenses.institution_id', 'licenses.course_id', 'user_licenses.is_associated')
                ->firstOrFail();
            $data = [];
            if ($studentLicense->is_associated === config('constants.YES')) {
                $teacherLicense = License::findByInstitution($studentLicense->institution_id)
                    ->findByCourse($studentLicense->course_id)
                    ->findByRole(config('constants.TEACHER'))->select('id')
                    ->firstOrFail();
                $teacherDashboardRoute = route('users.courses',
                    [$teacherLicense->id, $course->slug]);
                $data = ['teacherDashboardRoute' => $teacherDashboardRoute];
            }
            $view->with(array_merge($data, [
                'school' => $license->institution->name,
                'grade' => Grade::select('name')->findOrFail(
                    auth()->user()->licenses()->where('licenses.id', $license->id)
                        ->first()->pivot->grade_id)->first()->name,
                'course' => $license->course->name,
                'licenseId' => $license->id,
            ]));
        });
        view()->composer('includes.teacher-header', function ($view) {
            $license = request()->route('license');
            $view->with(['currentLicenseId' => $license->id]);
        });
        view()->composer('includes.student-progress', function ($view) {
            $view->with([
                'subjectName' => request()->subjectName,
                'studentLevel' => request()->studentLevel,
                'progressBlock' => request()->progressBlock,
                'scoreBlock' => request()->scoreBlock,
                'lastFiveScores' => request()->lastFiveScores,
                'currentScore' => request()->currentScore,
            ]);
        });
        view()->composer('includes.breadcumb-header', function ($view) {
            if ($license = request()->route('license')) {
                $course = $license->course()->select('slug')->first();
                $route = route('users.courses', [$license->id, $course->slug]);
            } else {
                $route = DashboardService::dashboardRoute();
            }
            $view->with(['dashboard' => $route]);
        });
        view()->composer('includes.institution-course-dropdown', function ($view) {
            $view->with([
                'courses' => Course::getAll()->get()->toArray(),
                'institutions' => Institution::getAll()->get()->toArray(),
            ]);
        });
        view()->composer([
            'users.students.learning-goals.tests.introduction',
            'users.students.learning-goals.tests.show',
            'users.students.learning-goals.show'],
            function ($view) {
                $learningGoalSlug = request()->route('learningGoalSlug');
                $learningGoal = app(Pipeline::class)
                    ->send($learningGoalSlug)
                    ->through(LearningGoalRenderService::class)
                    ->then(function ($learningGoal) {
                        return $learningGoal;
                    });
                $studentLearningGoal = request()->studentLearningGoal;
                $view->with([
                    'subjectName' => request()->subjectName,
                    'licenseId' => request()->route('license')->id,
                    'subjectSlug' => request()->route('subjectSlug'),
                    'learningGoalSlug' => $learningGoalSlug,
                    'learningGoalOrder' => $studentLearningGoal->order,
                    'subjectId' => $studentLearningGoal->subject_id,
                    'learningGoalId' => $studentLearningGoal->learning_goal_id,
                    'learningGoalTestId' => $studentLearningGoal->learning_goal_test_id,
                    'learningGoalName' => request()->learningGoalName,
                    'learningGoal' => $learningGoal->toArray(),
                ]);
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
