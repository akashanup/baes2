<?php

namespace App\Providers;

use App\Models\Grade;
use App\Observers\GradeObserver;
use Illuminate\Support\ServiceProvider;

class GradeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Grade::observe(GradeObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
