<?php

namespace App\Services;

use App\Models\LearningGoalTest;
use Lang;

class StudentService
{
    const INTRODUCTION = [
        'complexity' => '0',
    ];
    const BAES_BEGINNER = [
        'start' => '0.0',
        'end' => '2.4',
        'level' => 'Beginner',
        'complexity' => '1',
    ];
    const BAES_JUNIOR = [
        'start' => '2.5',
        'end' => '4.9',
        'level' => 'Junior',
        'complexity' => '2',
    ];
    const BAES_BAES = [
        'start' => '5.0',
        'end' => '6.9',
        'level' => 'BAES',
        'complexity' => '3',
    ];
    const BAES_EXPERT = [
        'start' => '7.0',
        'end' => '8.4',
        'level' => 'Expert BAES',
        'complexity' => '4',
    ];
    const BAES_END = [
        'start' => '8.5',
        'end' => '10.0',
        'complexity' => '5',
    ];

    /**
     * Baes level by score
     *
     * @param float $data
     * @return string
     */
    public static function getBaesLevelByScore($data)
    {
        $level = null;
        if ($data <= static::BAES_BEGINNER['end']) {
            $level = static::BAES_BEGINNER;
        } elseif (($data >= static::BAES_JUNIOR['start'])
            && ($data <= static::BAES_JUNIOR['end'])) {
            $level = static::BAES_JUNIOR;
        } elseif (($data >= static::BAES_BAES['start'])
            && ($data <= static::BAES_BAES['end'])) {
            $level = static::BAES_BAES;
        } elseif (($data >= static::BAES_EXPERT['start'])
            && ($data <= static::BAES_EXPERT['end'])) {
            $level = static::BAES_EXPERT;
        } elseif ($data >= static::BAES_END['start']) {
            $level = ['complexity' => 5,
                'level' => Lang::get('globals.end_baes')];
        }
        return $level;
    }

    /**
     * Baes level by complexity
     *
     * @param integer $data
     * @return string
     */
    public static function getBaesLevelByComplexity($data)
    {
        $level = null;
        switch ($data) {
            case 0:
                $level = Lang::get('globals.introduction');
                break;
            case 1:
                $level = static::BAES_BEGINNER['level'];
                break;
            case 2:
                $level = static::BAES_JUNIOR['level'];
                break;
            case 3:
                $level = static::BAES_BAES['level'];
                break;
            case 4:
                $level = static::BAES_EXPERT['level'];
                break;
            case 5:
                $level = Lang::get('globals.end_baes');
                break;

            default:
                break;
        }
        return $level;
    }

    /**
     * Return score and progress block of the student for the subject.
     *
     * @param  collection $studentLearningGoals student learning goals
     * @return array
     */
    public static function getScoreAndProgressBlock($studentLearningGoals)
    {
        $finishedLearningGoal = $studentLearningGoals
            ->filter(function ($value) {
                return $value->status === config('constants.FINISHED');
            });

        $finishedLearningGoalScore = $finishedLearningGoal
            ->sum('finished_score');
        $finishedLearningGoalsCount = $finishedLearningGoal
            ->count();

        $scoreBlock = $finishedLearningGoalsCount ?
        ($finishedLearningGoalScore / $finishedLearningGoalsCount) : 0;
        $totalGoals = $studentLearningGoals->count();
        $progressBlock = $totalGoals ? (($finishedLearningGoalsCount / $totalGoals) * 100) : 0;
        return ['scoreBlock' => round($scoreBlock, 1),
            'progressBlock' => round($progressBlock, 1)];
    }

    /**
     * Return last five score of a subject
     *
     * @param  collection $studentLearningGoal student learning goal
     * @return array
     */
    public static function lastFiveScores($studentLearningGoal)
    {
        return LearningGoalTest::findOrFail(
            $studentLearningGoal->learning_goal_test_id)->questionAnswers()
            ->whereNotNull('learning_goal_test_question_answers.answer')
            ->orderBy('learning_goal_test_question_answers.id', 'desc')
            ->limit(config('constants.BAES_FACTOR'))
            ->select('learning_goal_test_question_answers.points')
            ->pluck('points')->toArray();
    }
}
