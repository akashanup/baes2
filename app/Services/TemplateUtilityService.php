<?php

namespace App\Services;

use Log;

class TemplateUtilityService
{

    const EURO_SIGN = '€ ';
    const TYPE_CURRENCY = 'Valuta';
    const TYPE_NUMBER = 'Getal';
    const TYPE_PERCENTAGE = 'Percentage';
    const ANSWER = 'ANTWOORD';
    const PERCENTAGE_SIGN = '%';
    const CURRENCY = ['$ ', '€ '];

    /**
     * Get currency symbol to be stored in DB
     *
     * @return const
     */

    public static function returnSymbol()
    {
        return self::EURO_SIGN;
    }

    /**
     * Funtion to generate template variable data to export.
     *
     * @param  Collection $templateVariables
     * @param  integer    $qt_id (optional)
     * @param  integer    $qv_id (optional)
     * @return array $data
     */
    public static function generateExportData($templateVariables, $qt_id = null, $qv_id = null)
    {
        $data = [];
        $qv_id = $qv_id ? $qv_id : 1;
        foreach ($templateVariables as $key => $templateVariable) {
            foreach (json_decode($templateVariable->values, true) as $key1 => $value) {
                $extractedData = TemplateUtilityService::extract($value);
                $data[$qv_id]['qv_id'] = $qv_id;
                if ($qt_id) {
                    $data[$qv_id]['qt_id'] = $qt_id;
                }
                $data[$qv_id]['qv_group'] = ($key + 1);
                $data[$qv_id]['qv_variable'] = $key1;
                $data[$qv_id]['qv_value'] = $extractedData['value'];
                $data[$qv_id]['value_type'] = $extractedData['type'];
                $data[$qv_id]['rounding_value'] = $extractedData['round'];
                $qv_id++;
            }
        }

        return $data;
    }

    /**
     * Returns the question level
     *
     * @param  string $value
     * @return array
     */
    public static function extract($value)
    {
        $value = str_replace(',', '.', str_replace('.', '', $value));
        Log::info($value);
        $data = [];
        if (in_array(substr($value, 0, 4), static::CURRENCY)) {
            $value = substr($value, 4);
            $data['value'] = $value;
            $data['type'] = static::TYPE_CURRENCY;
            $data['round'] = strlen(substr(strrchr($value, '.'), 1)) . '';
        } elseif (is_numeric($value)) {
            $data['value'] = $value;
            $data['type'] = static::TYPE_NUMBER;
            $data['round'] = strlen(substr(strrchr($value, '.'), 1)) . '';
        } elseif (substr($value, -1, 1) == static::PERCENTAGE_SIGN) {
            $value = substr($value, 0, (strlen($value) - 2));
            $data['value'] = ($value / 100);
            $data['type'] = static::TYPE_PERCENTAGE;
            $data['round'] = strlen(substr(strrchr($value, '.'), 1)) . '';
        } else {
            $data['value'] = $value;
            $data['type'] = '';
            $data['round'] = '';
        }

        return $data;
    } //end extract()

    /**
     * Funtion to show the answer in dutch format
     * @param string $answer
     * @return string $answers
     */
    public static function dutchFormat($answer)
    {
        return $answer ? strpos($answer, "%") ? $answer : number_format($answer, (strpos($answer, ".") ? (strlen($answer) - strpos($answer, ".") - 1) : 0), ',', '.') : '';
    } //end format()
}
