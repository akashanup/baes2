<?php

namespace App\Services;

use App\Models\Course;
use App\Models\Grade;
use App\Models\Institution;
use App\Models\License;
use App\Models\PracticeTestDetail;
use App\Models\PracticeTestFolder;
use App\Models\PracticeTestTemplate;
use App\Models\StudentFeedback;
use App\Models\Subject;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\StudentService;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Storage;
use Lang;

class YajraDataTableService
{
    /**
     * Curriculumns datatable
     *
     * @return json
     */
    public static function curriculums()
    {
        return Datatables::of(Subject::getCurriculumsForDatatable())
            ->editColumn('name', function ($curriculum) {
                return view('curriculums.action', [
                    'curriculumName' => $curriculum->name,
                    'curriculumRoute' => route(
                        'learning-goals.curriculum.index', $curriculum->slug
                    )])->render();
            })
            ->editColumn('action', function ($curriculum) {
                return view('curriculums.action', [
                    'curriculumEditRoute' =>
                    route('curriculums.edit', $curriculum->slug),
                    'curriculumDeleteRoute' =>
                    route('curriculums.destroy', $curriculum->slug),
                ])->render();
            })
            ->rawColumns(['name', 'action'])->make(true);
    }

    /**
     * Subjects datatable
     *
     * @return json
     */
    public static function subjects()
    {
        return Datatables::of(Subject::getSubjectsForDatatable())
            ->editColumn('action', function ($subject) {
                return view('subjects.action', [
                    'subject' => $subject->toArray(),
                ])->render();
            })->rawColumns(['action'])->make(true);
    }

    /**
     * Course datatable
     *
     * @return json
     */
    public static function courses()
    {
        return Datatables::of(Course::getCoursesForDatatable())
            ->editColumn('action', function ($course) {
                return view('courses.action',
                    ['course' => $course])->render();
            })->rawColumns(['action'])->make(true);
    }

    /**
     * License datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return json
     */
    public static function licenses($course, $institution)
    {
        return Datatables::of(License::getLicensesForDatatable($course,
            $institution))->editColumn('status', function ($license) {
                return view('licenses.action', ['status' => $license->status,
                'courseName' => $license->course])->render();
            })->editColumn('type', function ($license) {
                return view('licenses.action', ['type' => $license->type,
                'courseName' => $license->course])->render();
            })->editColumn('course', function ($license) {
                return view('licenses.action', ['courseSlug' => $license->slug,
                'courseName' => $license->course])->render();
            })->editColumn('subjectsCount', function ($license) {
                return view('licenses.action', ['courseSlug' => $license->slug,
                'subjectsCount' => $license->subjectsCount])->render();
            })->editColumn('usersCount', function ($license) {
                return $license->usersCount ? view('licenses.action', [
                'licenseId' => $license->id,
                'courseSlug' => $license->slug,
                'usersCount' => $license->usersCount,
                'institutionSlug' => $license->institutionSlug])->render()
                : config('constants.ZERO');
            })->editColumn('action', function ($license) {
                return view('licenses.action', ['licenseId' => $license->id,
                'courseId' => $license->courseId,
                'institutionId' => $license->institutionId])->render();
            })->editColumn('end_date', function ($license) {
                $endDate = $license->end_date;
                return $endDate ? view('licenses.action', [
                'end_date' => $endDate,
                'status' => Carbon::parse($endDate)->isPast(),
                ])->render() : $endDate;
            })->rawColumns(['course', 'usersCount', 'status', 'type',
            'subjectsCount', 'end_date', 'action'])
            ->make(true);
    }

    /**
     * Institution datatable
     *
     * @return json
     */
    public static function institutions($institution, $course)
    {
        return Datatables::of(Institution::getInstitutionForDatatable(
            $institution, $course))
            ->editColumn('institution', function ($institution) {
                return view('institutions.action', [
                    'institution' => $institution->institution,
                    'institutionId' => $institution->id,
                    'courseId' => $institution->courseId,
                ])->render();
            })->editColumn('image', function ($institution) {
                return $institution->image ? view('institutions.action', [
                'image' => Storage::url($institution->image),
                'institutionName' => $institution->institution,
                ])->render() : Lang::get('globals.image_not_found');
            })->editColumn('course', function ($institution) {
                return view('institutions.action', [
                'courseSlug' => $institution->courseSlug,
                'courseName' => $institution->course,
                ])->render();
            })->editColumn('users', function ($institution) {
                return $institution->users ? view('institutions.action', [
                'institutionSlug' => $institution->slug,
                'courseSlug' => $institution->courseSlug,
                'users' => $institution->users,
                ])->render() : config('constants.ZERO');
            })->editColumn('action', function ($institution) {
                return view('institutions.action', [
                'institutionSlug' => $institution->slug,
                'institutionType' => $institution->type,
                'institutionId' => $institution->id,
                'courseId' => $institution->courseId,
                'courseSlug' => $institution->courseSlug,
                ])->render();
            })
            ->rawColumns(['institution', 'image', 'users', 'course', 'action'])
            ->make(true);
    }

    /**
     * Practice test template datatable
     *
     * @return json
     */
    public static function practiceTestTemplates(
        $licenseId,
        $courseSlug,
        $courseId,
        $institutionId,
        $folderId
    ) {
        $folder = PracticeTestFolder::find($folderId);
        $image = "url('/images/achtergrond3_lightgray.png')";
        $license_id = 1;
        $practiceTestTemplates = PracticeTestTemplate::
            practiceTestTemplate($courseId, $institutionId, $folderId);
        return Datatables::of($practiceTestTemplates)
            ->editColumn('name', function ($practiceTestTemplates) use (
                $licenseId,
                $courseSlug,
                $folder
            ) {
                return view('practice-tests.action', [
                    'testTemplateName' => $practiceTestTemplates->name,
                    'testTemplateId' => $practiceTestTemplates->id,
                    'imageUrl' => "url('/images/achtergrond3_lightgray.png')",
                    'licenseId' => $licenseId,
                    'courseSlug' => $courseSlug,
                    'folder' => $folder,
                ])->render();
            })
            ->rawColumns(['name'])
            ->make(true);
    }

    /**
     * Active practice test datatable
     *
     * @return json
     */
    public static function activePracticeTest(
        $licenseId,
        $courseId,
        $institutionId,
        $folderId,
        $gradeId = null
    ) {

        $activePracticeTests = PracticeTestDetail::
            listPracticeTests($courseId, $institutionId,
            config('constants.STARTED'), $folderId, $gradeId);
        return Datatables::of($activePracticeTests)
            ->editColumn('name', function ($activePracticeTests) use ($licenseId) {
                $image = "url('/images/achtergrond3_lightgray.png')";
                $practiceTestDetail = PracticeTestDetail::findOrFail($activePracticeTests->id);
                $studentCount = UserRepository::studentsOfPracticeTest($practiceTestDetail)->get()->count();
                return view('practice-tests.active-practice-test', [
                    'testName' => $activePracticeTests->name,
                    'testId' => $activePracticeTests->id,
                    'testSlug' => $activePracticeTests->slug,
                    'testGrade' => $activePracticeTests->grade,
                    'testEndDate' => Carbon::parse($activePracticeTests->end_date)->format('d / m / Y : Hi'),
                    'testAvgScore' => $activePracticeTests->avg_score,
                    'studentsCount' => $studentCount,
                    'studentFinishedTestCount' => $activePracticeTests->student_attempted,
                    'imageUrl' => "url('/images/achtergrond3_lightgray.png')",
                    'practiceTestOverviewRoute' => route('teachers.practiceTests.overview', [$licenseId, $activePracticeTests->slug]),
                ])->render();
            })
            ->rawColumns(['name'])
            ->make(true);
    }

    /**
     * Finished practice test datatable
     *
     * @return json
     */
    public static function finishedPracticeTest(
        $licenseId,
        $courseId,
        $institutionId,
        $grade = null,
        $year = null
    ) {
        $finishedPracticeTests = PracticeTestDetail::
            listPracticeTests($courseId, $institutionId,
            config('constants.FINISHED'), $folderId = null,
            $grade = null, $year = null);
        return Datatables::of($finishedPracticeTests)
            ->editColumn('name', function ($finishedPracticeTests) use ($licenseId) {
                return view('practice-tests.active-practice-test', [
                    'finishedPracticeTestData' => true,
                    'testName' => $finishedPracticeTests->name,
                    'practiceTestOverviewRoute' => route('teachers.practiceTests.overview', [$licenseId, $finishedPracticeTests->slug]),
                ])->render();
            })
            ->editColumn('end_date', function ($finishedPracticeTests) {
                return Carbon::parse($finishedPracticeTests->end_date)
                    ->format('d-m-Y');
            })
            ->editColumn('student_attempted', function ($finishedPracticeTests) {
                $practiceTestDetail = PracticeTestDetail::findOrFail($finishedPracticeTests->id);
                $studentsCount = UserRepository::studentsOfPracticeTest($practiceTestDetail)->get()->count();
                return $finishedPracticeTests->student_attempted . '/'
                    . $studentsCount;
            })
            ->editColumn('avg_score', function ($finishedPracticeTests) {
                return $finishedPracticeTests->avg_score . ' %';
            })
            ->rawColumns(['name', 'end_date', 'student_attempted', 'avg_score'])
            ->make(true);
    }

    /**
     * Users datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @param integer $licenseId License id
     * @return json
     */
    public static function users($institution, $course, $licenseId)
    {
        return Datatables::of(User::getUsersForDatatable(
            $institution, $course, $licenseId))
            ->editColumn('name', function ($user) {
                return $user->first_name . ' ' . $user->last_name;
            })
            ->editColumn('email', function ($user) {
                return view('users.action', ['email' => $user->email])
                    ->render();
            })
            ->editColumn('action', function ($user) {
                return view('users.action', ['action' => true,
                    'userId' => $user->id, 'userStatus' => $user->status,
                ])->render();
            })
            ->rawColumns(['name', 'email', 'action'])->make(true);
    }

    /**
     * Student feedbacks datatable
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return json
     */
    public static function studentFeedbacks($institution, $course)
    {
        return Datatables::of(
            StudentFeedback::getStudentFeedbacksForDatatable(
                $institution, $course))
            ->editColumn('questionAnswerId', function ($feedback) {
                return view('student-feedbacks.action', [
                    'questionAnswerId' => $feedback->questionAnswerId,
                ])->render();
            })
            ->editColumn('level', function ($feedback) {
                return $feedback->complexity !== null ?
                StudentService::getBaesLevelByComplexity(
                    intval($feedback->complexity)) : '';
            })
            ->editColumn('users', function ($feedback) {
                return view('student-feedbacks.action', [
                    'fName' => $feedback->first_name,
                    'lName' => $feedback->last_name,
                    'email' => $feedback->email,
                ])->render();
            })
            ->editColumn('action', function ($feedback) {
                return view('student-feedbacks.action', [
                    'action' => true,
                    'feedbackId' => $feedback->id,
                    'feedbackStatus' => $feedback->status,
                    'feedbackNote' => $feedback->note,
                    'emailType' => $feedback->response_email_type
                    ? $feedback->response_email_type : null,
                ])->render();
            })
            ->rawColumns(['questionAnswerId', 'level', 'users', 'action'])
            ->make(true);
    }

    /**
     * Student list datatable for teachers
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return json
     */
    public static function studentsListTable(
        $institutionId,
        $courseId,
        $class,
        $year
    ) {
        $students = UserRepository::currentInstitutionCourseStudents
            ($institutionId, $courseId, $class, $year);
        return Datatables::of($students)
            ->editColumn('name', function ($students) {
                return "<span class = 'studentName'>" . $students->first_name . " " . $students->last_name . "</span>";
            })
            ->editColumn('year', function ($students) {
                return "<span class = 'studentYear'>" . $students->year . "</span>";
            })
            ->editColumn('class', function ($students) {
                return $students->grade;
            })
            ->editColumn('status', function ($students) {
                return $students->blocked_by_teacher == config('constants.NO')
                ? config('constants.ACTIVE') : config('constants.INACTIVE');
            })
            ->editColumn('selection', function ($students) {
                return view('teachers.student-table-action',
                    ['studentId' => $students->id])
                    ->render();
            })
            ->rawColumns(['name', 'year', 'class', 'status', 'selection'])
            ->make(true);
    }

    /**
     * class list datatable
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return json
     */
    public static function classListTable(
        $licenseId,
        $course,
        $institutionId,
        $courseId,
        $year
    ) {
        $license = License::find($licenseId);
        $grades = Grade::getGradesByLicenseInstitution($license, $year);
        return Datatables::of($grades)
            ->editColumn('year', function ($grades) {
                return view('teachers.class-management-action',
                    ['classyear' => $grades->year])
                    ->render();
            })
            ->editColumn('name', function ($grades) use ($licenseId, $course) {
                return view('teachers.class-management-action',
                    ['className' => $grades->name,
                        'classSlug' => $grades->slug,
                        'studentManagementRoute' => route('teachers.student-management',
                            [$licenseId, $course, $grades->slug, $grades->year]),
                    ])
                    ->render();
            })
            ->editColumn('students', function ($grades)
 use ($institutionId, $courseId) {
                    return UserRepository::gradeInstitutionStudents
                    ($institutionId, $courseId, $grades->id)->count();
            })
            ->editColumn('edit', function ($grades)
 use ($institutionId, $courseId) {
                    return view('teachers.class-management-action',
                        ['gradeName' => $grades->name,
                            'gradeId' => $grades->id,
                            'studentCount' => UserRepository::gradeInstitutionStudents
                            ($institutionId, $courseId, $grades->id)->count()])
                        ->render();
            })
            ->rawColumns(['year', 'name', 'students', 'edit'])
            ->make(true);
    }
}
