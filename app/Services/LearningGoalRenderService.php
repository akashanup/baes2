<?php

namespace App\Services;

use App\Models\LearningGoal;
use Closure;

class LearningGoalRenderService
{
    const SEARCH_WELL = ['[WELL]', '[/WELL]'];
    const REPLACE_WELL = ['<div class="well">', '</div>'];

    /**
     * Handle the data
     *
     * @param collection $data
     * @return string
     */
    public static function handle($learningGoalSlug, Closure $next)
    {
        $learningGoal = LearningGoal::findBySlug($learningGoalSlug)
            ->select('theory', 'example', 'video')->first();
        $learningGoal->video = json_decode($learningGoal->video, true);

        $learningGoal->theory = str_replace(static::SEARCH_WELL,
            static::REPLACE_WELL, $learningGoal->theory);
        $learningGoal->example = str_replace(static::SEARCH_WELL,
            static::REPLACE_WELL, $learningGoal->example);

        return $next($learningGoal);
    }
}
