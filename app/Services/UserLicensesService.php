<?php

namespace App\Services;

use Carbon\Carbon;
use Closure;
use DB;
use Lang;

class UserLicensesService
{
    /**
     * Handle the data
     *
     * @param collection $data
     * @return string
     */
    public static function handle($data, Closure $next)
    {
        $user = auth()->user();
        $data = $data->map(function ($item) use ($user) {
            $userLicense = DB::table('user_licenses')->select('user_licenses.end_date as licenseEndDate')
                ->where('user_id', $user->id)->where('license_id', $item->id)->first();
            $item->licenseDuration = Lang::get('users.lifetime');
            if ($userLicense) {
                $item->licenseDuration = Carbon::parse($userLicense->licenseEndDate)
                    ->format('d-m-Y');
            }
            return $item;
        });
        return $next($data);
    }
}
