<?php

namespace App\Services;

use App\Models\PracticeTestDetail;
use App\Models\QuestionAnswer;
use App\Services\AnswerFormatService;
use App\Services\HintService;
use DateTime;

class PracticeTestService
{
    /**
     * Get the practice tests for a subject.
     *
     * @param Collection $practiceTests PracticeTestDetails collection
     * @param App\Models\Subject $subject
     * @return boolean
     */
    public static function subjectPracticeTests($practiceTests, $subject)
    {
        return $practiceTests->filter(function ($practiceTest) use ($subject) {
            return in_array($subject->id, PracticeTestDetail::findOrFail(
                $practiceTest->practice_test_detail_id)->subjects());
        });
    }

    /**
     * Get the practice tests for a subject.
     *
     * @param Collection $practiceTests PracticeTestDetails collection
     * @param string $createdBy
     * @return boolean
     */
    public static function practiceTestsByRole($practiceTests, $createdBy)
    {
        return $practiceTests->filter(function ($practiceTest) use ($createdBy) {
            return $practiceTest->created_by === $createdBy;
        });
    }

    /**
     * Show hints for practice test questions.
     *
     * @param Collection $questionAnswers
     * @param string $feedbackType
     * @param string $createdBy
     * @return collection
     */
    public static function practiceTestQuestionsWithHints($questionAnswers, $feedbackType, $createdBy)
    {
        if ($feedbackType === config('constants.END_OF_TEST')) {
            $questionAnswers = $questionAnswers->map(function ($item) use ($createdBy) {
                $questionAnswer = QuestionAnswer::findOrFail($item->pivot->question_answer_id);
                $hints = $questionAnswer->hint->hint;
                if ($createdBy === config('constants.TEACHER')) {
                    $item->hint = HintService::getHint(
                        $hints, config('constants.FOURTH_ATTEMPT'));
                } else {
                    $item->hint = HintService::getHint(
                        $hints, config('constants.THIRD_ATTEMPT')) . HintService::getHint(
                        $hints, config('constants.FOURTH_ATTEMPT'));
                }
                $item->pivot->answer = AnswerFormatService::dutchFormat(
                    $item->pivot->answer);
                $item->answer = $questionAnswer->answer;
                return $item;
            });
        } elseif ($feedbackType === config('constants.PARTLY')) {
            $questionAnswers = $questionAnswers->map(function ($item) {
                $questionAnswer = QuestionAnswer::findOrFail($item->pivot->question_answer_id);
                $item->answer = $questionAnswer->answer;
                $item->pivot->answer = AnswerFormatService::dutchFormat(
                    $item->pivot->answer);
                return $item;
            });
        }
        return $questionAnswers;
    }

    /**
     * Set timer in cookie for student practice test
     *
     * @param Collection $studentPracticeTest
     * @return void
     */
    public static function studentPracticeTestCookie($studentPracticeTest)
    {
        // Time limit data set for all browser
        // Student test start time
        $startTimeSeconds = new DateTime($studentPracticeTest->pivot->updated_at);
        // Current time for the server
        $now = new DateTime();
        // Diff between current time and test start time
        $differenceInSeconds = ($now->format('U') - $startTimeSeconds->format('U'));
        // Calculating time left for test for the current user
        $timeLeft = (($studentPracticeTest->time * 60) - $differenceInSeconds);
        // Setting time to client browser
        setcookie(('time_left' . $studentPracticeTest->slug), $timeLeft);
    }
}
