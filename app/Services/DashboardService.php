<?php

namespace App\Services;

use App\Models\Grade;
use App\Repositories\UserRepository;

class DashboardService
{
    /**
     * Route of user's dashboard
     *
     * @return string
     */
    public static function dashboardRoute()
    {
        $route = null;
        if (auth()->check()) {
            $user = auth()->user();
            switch ($user->role->name) {
                case config('constants.SUPER_ADMIN'):
                    $route = route('roles.index');
                    break;
                case config('constants.ADMIN'):
                    $route = route('admins.dashboard');
                    break;
                case config('constants.SUB_ADMIN'):
                    $route = route('sub-admins.dashboard');
                    break;
                case config('constants.USER'):
                    $route = route('users.licenses');
                    break;
                default:
                    // Faulty user role
                    auth()->logout();
                    $route = route('login');
                    break;
            }
        } else {
            $route = route('login');
        }
        return $route;
    }

    /**
     * Get user's dashboard data
     *
     * @param App\Models\License $license
     * @return text
     */
    public static function dashboardData($license)
    {
        $response = null;
        switch ($license->role) {
            case config('constants.STUDENT'):
                $user = auth()->user();
                $course = $license->course()->select('id', 'name', 'slug')->first();
                $response = view('users.students.dashboard')->with([
                    'subjects' => UserRepository::getStudentSubjectsWithLastLearningGoal($user, $license->id, $course)->toArray(),
                    'school' => $license->institution->name,
                    'grade' => Grade::find($user->licenses()
                            ->where('licenses.id', $license->id)
                            ->first()->pivot->grade_id)->name,
                    'course' => $course->name,
                    'licenseId' => $license->id,
                    'courseSlug' => $course->slug,
                ])->render();
                break;
            case config('constants.TEACHER'):
                $course = $license->course;
                $grades = Grade::getInstitutionGrades($license->institution_id);
                $className = $grades->pluck('name')->toArray();
                $classSlug = $grades->pluck('slug')->toArray();
                $classes = array_combine($classSlug, $className);
                $subjects = $license->course->subjects;
                $response = view('teachers.dashboard')->with([
                    'licenseId' => $license->id,
                    'course' => request()->route('courseSlug'),
                    'courseId' => $course->id,
                    'classes' => $classes,
                    'defaultGrade' => $grades->first(),
                    'subjectSlug' => $subjects->first()->slug,
                ])->render();
                break;
            default:
                // Faulty license role
                auth()->logout();
                $route = route('login');
                break;
        }
        return $response;
    }
}
