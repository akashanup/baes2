<?php

namespace App\Services;

class VideoEncodeService
{
    /**
     * Create JSON for video in learnig goal
     *
     * @return json
     */
    public static function videoEncode($data)
    {
        $video = [
            [
                'name' => $data['video_text1'],
                'url' => $data['video1'],
            ],
            [
                'name' => $data['video_text2'],
                'url' => $data['video2'],
            ],
        ];
        return json_encode($video);
    }
}
