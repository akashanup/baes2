<?php

namespace App\Services;

use App\Models\Permission;

class PermissionService
{
    /**
     * Check permission(s) based on user role(s).
     *
     * @param integer $role user role
     * @param mixed $permissions permission name(s)
     * @return boolean
     */
    public static function checkRolePermission($role, $permissions)
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        $rolePermissions = Permission::join(
            'role_permissions', 'permissions.id',
            'role_permissions.permission_id'
        )->whereIn('permissions.name', $permissions)->where(
            'role_permissions.role_id', $role
        )->count();
        return $rolePermissions ? 1 : 0;
    }
}
