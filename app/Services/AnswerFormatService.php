<?php

namespace App\Services;

class AnswerFormatService
{
    /**
     * Funtion to append decimal properly
     * @param string $answer
     * @return string $decimalAnswer
     */
    public static function appendDecimal($answer)
    {
        $decimalAnswer = '';
        // If answer has % sign then append .00 before % sign
        if (strpos($answer, '%')) {
            $decimalAnswer = substr($answer, 0, (strlen($answer) - 1)) . '.00%';
        } else {
            $decimalAnswer = $answer . '.00';
        }

        return $decimalAnswer;
    }

    /**
     * Funtion to format the answer which has % sign
     * @param string $answer
     * @return string $answers
     */
    public static function format($answer)
    {
        $answerWithoutPercentage = substr($answer, 0, (strlen($answer) - 1));

        $answers = [];

        $answers[0] = $answer;
        $answers[1] = $answerWithoutPercentage;
        $answers[2] = ($answerWithoutPercentage / 100);
        $answers[3] = round($answers[2], 2);
        return $answers;
    }

    /**
     * Funtion to show the answer in dutch format
     * @param string $answer
     * @return string $answers
     */
    public static function dutchFormat($answer)
    {
        return $answer ? (strpos($answer, "%") ? $answer :
            (number_format($answer, (strpos($answer, ".") ? (strlen($answer) - strpos($answer, ".") - 1) : 0), ',', '.'))) : '';
    }
}
