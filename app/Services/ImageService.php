<?php

namespace App\Services;

use App\Exceptions\ImageUploadException;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Lang;

class ImageService
{
    const IMAGE_LOCATION = 'images/';

    /**
     * Save image
     *
     * @return string
     */
    public static function save($imageFile, $resize, $imageName = null)
    {
        $response = null;
        $imageName = $imageName ? $imageName : str_random(5) . time();
        $url = static::IMAGE_LOCATION . $imageName . '.' . $imageFile->getClientOriginalExtension();
        $image = Image::make($imageFile);
        $image = $resize ? $image->resize(61, 61) : $image;
        if ($image) {
            $image->stream();
            Storage::put($url, $image->__toString(),
                config('constants.S3_IMAGE_VISIBLE'));
            $response = $url;
        } else {
            throw new ImageUploadException(
                Lang::get('globals.image_upload_failed'), 1
            );
        }

        return $response;
    }
}
