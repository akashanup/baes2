<?php

namespace App\Services;

use App\Models\License;
use App\Models\Payment;
use App\Models\Subscription;
use App\Services\MolliePayment;
use Auth;
use Carbon\Carbon;
use Log;
use Mollie\Api\MollieApiClient;

class MolliePayment
{
    public $mollie;

    public function __construct()
    {
        $this->mollie = new MollieApiClient();
        $this->mollie->setApiKey(config('app.mollie_api_key'));
    }

    /**
     * Create mollie Customer.
     * @param array $selectedLicense
     * @return \Illuminate\Http\Response
     */

    public function createCustomer($selectedLicense, $gradeId)
    {
        $customer = $this->mollie->customers->create([
            "name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "email" => Auth::user()->email,
            "metadata" => [
                "licenseId" => $selectedLicense->id,
                "licenseCode" => $selectedLicense->code,
                "userId" => Auth::user()->id,
                "subscriptionAmount" => number_format($selectedLicense->price, 2),
                "grade_id" => $gradeId,
            ],
        ]);
        return $customer;
    }

    /**
     * Create mollie Payment.
     * @param array $customer
     * @param array $selectedLicense
     * @return \Illuminate\Http\Response
     */

    public function createPayment($customer, $selectedLicense, $gradeId, $method)
    {
        $paymentUrl = null;
        $paymentData = [
            'mollie_payment_unique_id' => null,
            'user_id' => Auth::user()->id,
            'license_id' => $selectedLicense->id,
        ];
        $savedPayment = Payment::savePayment($paymentData);
        $molliePayment = [
            "amount" => [
                "currency" => config('constants.DEFAULT_CURRENCY'),
                "value" => number_format($selectedLicense->price, 2),
                // You must send the correct number of decimals, thus we enforce the use of strings
            ],
            "method" => $method,
            "description" => "BAES Licentie " . $selectedLicense->code .
            " voor gebruiker " . Auth::user()->id . " - id: " . time(),
            "redirectUrl" => route('payments.index', $savedPayment->id),
            "webhookUrl" => route('payments.paymentWebHook'),
            "metadata" => [
                "licenseId" => $selectedLicense->id,
                "licenseCode" => $selectedLicense->code,
                "userId" => Auth::user()->id,
                "grade_id" => $gradeId,
            ],
        ];

        if ($customer) {
            $molliePayment["metadata"]["subscriptionAmount"] =
                number_format($selectedLicense->price, 2);
            $molliePayment["sequenceType"] = 'first';
            $payment = $customer->createPayment($molliePayment);
        } else {
            $molliePayment["metadata"]["validity"] =
            $selectedLicense->validity;
            $molliePayment["sequenceType"] = 'oneoff';
            $payment = $this->mollie->payments->create($molliePayment);
        }
        if ($payment->getCheckoutUrl()) {
            // redirect to mollie
            Log::info("Redirecting to mollie.......");
            $paymentUrl = $payment->getCheckoutUrl();
        } else {
            $paymentUrl = null;
        }
        // Udpate Payment table with mollie_payment_id
        $updateData = [
            'mollie_payment_unique_id' => $payment->id,
        ];
        $updatePayment = $savedPayment->updatePayment($updateData);
        return $paymentUrl;
    }

    /**
     * Create mollie Subscription.
     * @param array $customer
     * @param array $payment
     * @return \Illuminate\Http\Response
     */

    public static function createMollieSubscription($customer, $payment)
    {
        $subscription = $customer->createSubscription([
            "amount" => [
                "value" => number_format($payment->metadata->subscriptionAmount, 2),
                // You must send the correct number of decimals, thus we enforce the use of strings
                "currency" => config('constants.DEFAULT_CURRENCY'),
            ],
            "method" => config('constants.DEBIT_METHOD'),
            "interval" => "1 month", // every month
            "description" => "BAES Licentie " . $payment->metadata->licenseCode .
            " voor gebruiker " . $payment->metadata->userId . " - id: " . time() . "",
            "webhookUrl" => route('payments.paymentWebHook'),
            "startDate" => Carbon::now()->addMonth()->format('Y-m-d'),
        ]);
        return $subscription;
    }
}
