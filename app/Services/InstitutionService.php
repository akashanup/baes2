<?php

namespace App\Services;

use Closure;
use Illuminate\Support\Facades\Storage;

class InstitutionService
{
    /**
     * Handle the data
     *
     * @param collection $data
     * @return string
     */
    public static function handle($data, Closure $next)
    {
        $data = $data->map(function ($item) {
            $item->image = $item->image ? Storage::url($item->image)
            : $item->image;
            $subjects = explode(' | ', $item->subjects);
            $subjectOrders = explode(' | ', $item->subjectOrders);
            $subjectsCount = count($subjects);
            $subjectsByOrder = [];
            for ($i = 0; $i < $subjectsCount; $i++) {
                $subjectsByOrder[$subjectOrders[$i]] = $subjects[$i];
            }
            ksort($subjectsByOrder, 1);
            $item->subjects = $subjectsByOrder;
            if ($item->price) {
                $item->price = view('includes.item-price', [
                    'price' => number_format($item->price, 2, ',', '.'),
                    'type' => $item->type])->render();
            }
            return $item;
        });
        return $next($data);
    }
}
