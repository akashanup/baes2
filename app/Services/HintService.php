<?php

namespace App\Services;

use Lang;

class HintService
{
    /**
     * Returns the hint
     *
     * @param  array   $hints
     * @param  integer $attempt
     * @return string
     */
    public static function getHint($hints, $attempt)
    {
        $unavailableFeedback = Lang::get('users.no_feedback_available');
        $hints = json_decode($hints, true);
        $hint = $hints ? (array_key_exists($attempt, $hints)
            ? $hints[$attempt] : $unavailableFeedback) : $unavailableFeedback;
        return $hint;
    }
}
