<?php

namespace App\Services;

use App\Models\TemplateVariable;
use App\Services\TemplateUtilityService;
use Illuminate\Database\Eloquent\Collection;
use Log;
use Maatwebsite\Excel\Facades\Excel;

class ImportVariableSetService
{

    const CREATE_MODE = 'create';
    const EDIT_MODE = 'edit';
    const ODS = 'ods';
    const XLSX = 'xlsx';
    const NO = 'no';
    const YES = 'yes';
    const TYPE_DIRECT = 'direct';
    const TYPE_MCQ = 'mcq';
    const TYPE_CURRENCY = 'currency';
    const TYPE_NUMBER = 'Getal';
    const TYPE_PERCENTAGE = 'Percentage';
    const ANSWER = 'ANTWOORD';
    /**
     * Generate collection of variable set uploaded from file
     *
     * @param  integer $question_template_id
     * @param  file    $file
     * @param  array   $templateVariable
     * @param  string  $mode
     * @return mixed
     */
    public static function fileUpload(
        $question_template_id,
        $file,
        $template_variable,
        $mode
    ) {
        // Fetching the path of the file
        $file_path = $file->getRealPath();

        // Reading Excel file
        $variable_sets = Excel::load($file_path)->toArray();

        // check the file extension and parse it accordingly
        if ($file->getClientOriginalExtension() == static::ODS) {
            $variable_sets = $variable_sets[0];
        }
        // Total Variable present
        $variable_count = count($variable_sets);
        // Checking if question variable colummn name are as per documentation
        $header_question_variable = [
            'qv_id',
            'qt_id',
            'qv_group',
            'qv_variable',
            'qv_value',
            'value_type',
            'rounding_value',
        ];
        if (!empty(array_diff(array_keys($variable_sets[0]), $header_question_variable))) {
            return null;
        }

        // Counting total number of group present
        $total_group = 0;
        $count = 0;
        while ($count < $variable_count) {
            if ($variable_sets[$count]['qv_group'] != $total_group) {
                $total_group++;
            }

            $count++;
        }

        $invalid_group_array = array();

        if ($mode == static::EDIT_MODE) {
            $templateVariables = new Collection();
        }
        // Creating JSON and storing each variable set
        while ($total_group != 0) {
            $variable_array = array();
            $current_line = 0;
            while ($current_line < $variable_count) {
                Log::info($variable_sets[$current_line]['value_type']);
                if ($total_group == $variable_sets[$current_line]['qv_group']) {
                    // Adding value type symbol to values
                    if ($variable_sets[$current_line]['value_type'] == TemplateUtilityService::TYPE_CURRENCY) {
                        // Adding Euro sign before the value
                        $value = TemplateUtilityService::returnSymbol() . number_format($variable_sets[$current_line]['qv_value'], $variable_sets[$current_line]['rounding_value'], ',', '.');
                    } elseif ($variable_sets[$current_line]['value_type'] == TemplateUtilityService::TYPE_PERCENTAGE) {
                        $value = number_format(($variable_sets[$current_line]['qv_value'] * 100), $variable_sets[$current_line]['rounding_value'], ',', '.') . '\\%';
                    } elseif ($variable_sets[$current_line]['value_type'] == TemplateUtilityService::TYPE_NUMBER) {
                        if ($variable_sets[$current_line]['qv_variable'] == TemplateUtilityService::ANSWER) {
                            $value = round($variable_sets[$current_line]['qv_value'], $variable_sets[$current_line]['rounding_value']);
                        } else {
                            $value = number_format($variable_sets[$current_line]['qv_value'], $variable_sets[$current_line]['rounding_value'], ',', '.');
                        }
                    } else {
                        $value = $variable_sets[$current_line]['qv_value'];
                    }

                    // Storing answer in variables
                    if ($variable_sets[$current_line]['qv_variable'] == TemplateUtilityService::ANSWER) {
                        if ($variable_sets[$current_line]['value_type'] == TemplateUtilityService::TYPE_CURRENCY) {
                            $correct_answer = round($variable_sets[$current_line]['qv_value'], $variable_sets[$current_line]['rounding_value']);
                        } elseif ($variable_sets[$current_line]['value_type'] == TemplateUtilityService::TYPE_PERCENTAGE) {
                            $correct_answer = number_format(($variable_sets[$current_line]['qv_value'] * 100), $variable_sets[$current_line]['rounding_value'], '.', ',') . '%';
                        } else {
                            $correct_answer = $value;
                        }
                    }
                    // Storing variable into array in key value form : key is variable name in sheet and value is value given in sheet to that variable
                    $variable_array[trim($variable_sets[$current_line]['qv_variable'])] = $value;
                }
                $current_line++;
            }
            $variable_name = array_map('trim', array_keys($variable_array));
            if ((!empty(array_diff(array_unique($template_variable), array_unique($variable_name))))) {
                $invalid_group_array[$total_group] = $total_group;
            }
            // If current variable set is correct/contain all variables required insert into DB
            if (!in_array($total_group, $invalid_group_array)) {
                // this variable contain all the variable from excel sheet for the current group in json format
                $variable_set_json = json_encode($variable_array);
                if ($mode == static::CREATE_MODE) {
                    // Inserting All variable,values and answer into db
                    $insert_into_qvDB = new TemplateVariable();
                    $insert_into_qvDB->values = $variable_set_json;
                    $insert_into_qvDB->correct_answer = $correct_answer;
                    $insert_into_qvDB->question_template_id = $question_template_id;
                    $insert_into_qvDB->save();
                } else {
                    // Inserting All variable,values and answer into Collection
                    $templateVariable['values'] = $variable_set_json;
                    $templateVariable['correct_answer'] = $correct_answer;
                    $templateVariable['qt_id'] = $question_template_id;
                    $templateVariables->push($templateVariable);
                }
            } //end if
            // Changing group number
            $total_group--;
        }
        if ($mode == static::CREATE_MODE) {
            return true;
        } else {
            return $templateVariables;
        }
    }

    /**
     * Extract unique template variables from question template and feedback steps
     *
     * @param  string $questionTemplate
     * @param  string $feedbackStep2
     * @param  string $feedbackStep3
     * @param  string $feedbackStep4
     * @return array $templateVariables
     */
    public static function getTemplateVariables($questionTemplate, $feedbackStep2, $feedbackStep3, $feedbackStep4)
    {
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $questionTemplate, $template_variable1);
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $feedbackStep2, $template_variable2);
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $feedbackStep3, $template_variable3);
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $feedbackStep4, $template_variable4);

        // Creating a unique and single variable set array
        $template_variable = array_merge($template_variable1[1], $template_variable2[1], $template_variable3[1], $template_variable4[1]);
        $template_variable = array_unique($template_variable);
        return $template_variable;
    } //end getTemplateVariables()
}
