<?php

namespace App\Services;

class GenerateTextService
{
    /**
     * Funtion to generate text based on template and variables.
     * @param string $template
     * @param json $json
     * @return string $question
     */
    public static function text($template, $json)
    {
        $values = json_decode($json, true);
        $variable = [];
        foreach ($values as $key => $value) {
            $variable['[[' . $key . ']]'] = $value;
        }
        $question = strtr($template, $variable);
        return $question;
    }
}
