<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Carbon\Carbon;
use Closure;
use Lang;

class LicenseService
{
    /**
     * Handle the data
     *
     * @param collection $data
     * @return string
     */
    public static function handle($data, Closure $next)
    {
        /*
        1: monthly licence: expires after 1 month and will be automatically renewed each month.
        If end date is provided this will be the date on which registration for this licence is not possible anymore.
        2: monthly license: for registered users it would be inactive only if user deactivates/stops it manually.
        3: fixed term with end-date (for example 2018-12-31): it doesn't matter when a student registers for
        the licence. After 2018-12-31 it's not possible to register for this licence AND active user licences become invalid.
        4: fixed term without end-date (for example 365 days): there is no end-date in the licence itself.
        If student activates/pays for the licence we calculate the amount of days FROM PAYMENT to calculate end date.
        Student 1: registers with the licence at 4-7-2018 so will expire on 4-7-2019.
        Student 2: registers with the licence on 18-9-2018 so will expire on 18-9-2019.
         */
        $data = $data->map(function ($item) {
            $now = Carbon::now();
            $item->year = $now->year . '/' . $now->addYear()->year;
            $licenseDuration = null;
            $now = Carbon::now();
            if ($item->type === config('constants.MONTHLY_PAYMENT')) {
                $licenseDuration = $now->addMonth()->toDateString();
            } elseif ($item->type === config('constants.FIXED_PAYMENT')) {
                if ($item->end_date) {
                    $endDate = Carbon::parse($item->end_date);
                    if ($item->validity) {
                        $licenseDuration = $now->addDays($item->validity);
                        $licenseDuration = $licenseDuration->lt($endDate) ?
                        $licenseDuration : $endDate;
                    } else {
                        $licenseDuration = $endDate;
                    }
                    $licenseDuration = $licenseDuration->toDateString();
                } elseif ($item->validity) {
                    $licenseDuration = $now->addDays($item->validity)
                        ->format('d-m-Y');
                } else {
                    $licenseDuration = Lang::get('users.lifetime');
                }
            }
            $item->licenseDuration = $licenseDuration;
            return $item;
        });
        return $next($data);
    }

    /**
     * Render the data
     *
     * @param collection $data
     * @return string
     */
    public static function render($licenses, Closure $next)
    {
        foreach ($licenses as $license) {
            $license->activity = UserRepository::weeklyActivity(Carbon::now(),
                $license->user_id, $license->license_id, null);
        }
        return $next(view('users.licenses-table',
            ['licenses' => $licenses->toArray()])->render());
    }
}
