<?php

namespace App\Services;

use App\Jobs\ChangeQuestionAnswerStatus;
use App\Models\LearningGoal;
use App\Models\QuestionFeedback;
use App\Models\QuestionTemplate;
use App\Models\TemplateVariable;
use App\Services\TemplateUtilityService;
use Carbon\Carbon;
use Datatables;
use DB;
use Lang;
use Log;
use Maatwebsite\Excel\Facades\Excel;

class TemplateUploadService
{

    const TYPE_DIRECT = 'direct';
    const TYPE_MCQ = 'mcq';
    const TYPE_CURRENCY = 'Valuta';
    const TYPE_NUMBER = 'Getal';
    const TYPE_PERCENTAGE = 'Percentage';
    const ANSWER = 'ANTWOORD';
    const UPPERBOUND = 'UPPERBOUND';
    const LOWERBOUND = 'LOWERBOUND';
    const YES = 'Yes';
    const NO = 'No';
    const ACTIVE = 'active';
    const MAXIMUM_ROWS = 5000;
    const QUESTION_TEMPLATE = 'QuestionTemplate';
    const FEEDBACK_TEMPLATE = 'FeedbackTemplate';
    const QUESTION_VARIABLES = 'QuestionVariables';
    const MC_QUESTIONS = 'MCQuestions';
    const MC_ANSWERS = 'MCAnswers';

    /**
     * Export MCQ question template of a learningGoal
     *
     * @param  App\Model $questionTemplates
     * @return void
     */

    public static function exportMcqQuestionTemplates($questionTemplates)
    {
        $mcqQuestionData = TemplateUploadService::getMcqQuestions($questionTemplates);
        $mcqQuestionTemplates = $mcqQuestionData['mcqQuestionTemplates'];
        $mcqOptions = $mcqQuestionData['mcqOptions'];
        Excel::create(
            Lang::get(static::TYPE_MCQ . ' ' . $mcqQuestionData['learningGoalID'] . ' - ' . $mcqQuestionData['learningGoalName']),
            function ($excel) use ($mcqQuestionTemplates, $mcqOptions) {
                $excel->sheet(
                    static::MC_QUESTIONS,
                    function ($sheet) use ($mcqQuestionTemplates) {
                        $sheet->fromArray($mcqQuestionTemplates);
                    }
                );
                $excel->sheet(
                    static::MC_ANSWERS,
                    function ($sheet) use ($mcqOptions) {
                        $sheet->fromArray($mcqOptions);
                    }
                );
            }
        )->export('xlsx');
    }

    /**
     * Export Direct question template of a learningGoal
     *
     * @param  App\Model $questionTemplates
     * @return void
     */
    public static function exportDirectQuestionTemplates($questionTemplates)
    {
        $directQuestionData = TemplateUploadService::
            getDirectQuestions($questionTemplates);
        $directQuestionTemplates = $directQuestionData['directQuestionTemplates'];
        $feedbackTemplates = $directQuestionData['feedbackTemplates'];
        $templateVariables = $directQuestionData['templateVariables'];

        Excel::create(
            Lang::get(static::TYPE_DIRECT . ' ' . $directQuestionData['learningGoalID'] . ' - ' . $directQuestionData['learningGoalName']),
            function ($excel) use ($directQuestionTemplates, $feedbackTemplates, $templateVariables) {
                $excel->sheet(
                    static::QUESTION_TEMPLATE,
                    function ($sheet) use ($directQuestionTemplates) {
                        $sheet->fromArray($directQuestionTemplates);
                    }
                );
                $excel->sheet(
                    static::FEEDBACK_TEMPLATE,
                    function ($sheet) use ($feedbackTemplates) {
                        $sheet->fromArray($feedbackTemplates);
                    }
                );
                $excel->sheet(
                    static::QUESTION_VARIABLES,
                    function ($sheet) use ($templateVariables) {
                        $sheet->fromArray($templateVariables);
                    }
                );
            }
        )->export('xlsx');
    } //end exportDirectQuestionTemplates()

    /**
     * Export template variables set  of a learningGoal
     *
     * @param  App\Model $questionTemplates
     * @return void
     */
    public static function exportVariableSets($questionTemplates)
    {
        $templateVariables = [];
        foreach ($questionTemplates as $key => $questionTemplate) {
            $templateVariables = array_merge(
                $templateVariables,
                TemplateUtilityService::generateExportData(
                    $questionTemplate->templateVariables()->where('status_edit', static::NO)->get(),
                    $questionTemplate->id,
                    sizeof($templateVariables) + 1
                )
            );
        }

        if (sizeof($templateVariables)) {
            Excel::create(
                Carbon::now(),
                function ($excel) use ($templateVariables) {
                    $excel->sheet(
                        Lang::get('admins.variable_set'),
                        function ($sheet) use ($templateVariables) {
                            $sheet->fromArray($templateVariables);
                        }
                    );
                }
            )->export('xlsx');
        }
    } //end exportVariableSets()

    /**
     * Get mcq Template list by learing goal
     *
     * @param  string $slug, $type
     * @return void
     */

    public static function getMcqTemplateList($slug, $type)
    {
        $mcqTemplates = QuestionTemplate::getTemplateQuestion($slug, $type);

        return Datatables::of($mcqTemplates)
            ->editColumn(
                'action',
                function ($mcqTemplate) {
                    return '<a href="">' .
                    $mcqTemplate->id .
                        '</a>';
                }
            )
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Funtion to parse Direct question template sheet and insert into DB
     * @param $direct_template
     * @return string
     */
    public function direct($direct_template, $data)
    {
        try {
            DB::beginTransaction();
            $runJob = false;
            $allFeedbackArray = array();
            $AllTemplateVariable = array();
            $inactiveQuestionArray = array();
            $inActiveTemplateQuestion = array();
            $response = null;

            if ($data['status_change'] == self::YES) {
                QuestionTemplate::updatetemplateStatus(self::TYPE_DIRECT, $data);
                $runJob = true;
            }
            // Counting the number of Question Template present in the sheet
            $question_template_count = count($direct_template[0]);
            // Counting the number of Feedback Template present in the sheet
            $feedback_template_count = count($direct_template[1]);
            // Counting number of question variables in table
            $question_variable_count = count($direct_template[2]);
            // For counting how many template we have parsed
            $question_template_counter = 0;
            while ($question_template_counter < $question_template_count) {
                $qt_id = $direct_template[0][$question_template_counter]['qt_id'];
                $lg_id = $direct_template[0][$question_template_counter]['lg_id'];
                $qt_template = trim($direct_template[0][$question_template_counter]['qt_template']);
                $qt_complexity = $direct_template[0][$question_template_counter]['qt_complexity'];
                // Fetching all the variables from the template
                preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $qt_template, $template_variable);
                // Post increment of $question_template_counter
                $question_template_counter++;
                // Inserting question template into DB
                $insert_into_qtDB = new QuestionTemplate();
                // create QT array

                // For counting the number of feedback template we have parsed for each question template
                $feedback_template_counter = 0;
                $feedback_variable = array();
                $feedback_counter_check = 0;
                $templateFeedbackData = [];
                $AllTemplateVariable = [];
                $allFeedbackArray = [];
                while ($feedback_template_counter < $feedback_template_count) {
                    $current_feedback_template_variable = array();
                    // Checking feedback row parsed is for current inserted question template
                    if ($direct_template[1][$feedback_template_counter]['qt_id'] == $qt_id) {
                        $ft_id = $direct_template[1][$feedback_template_counter]['ft_id'];
                        $ft_template = trim($direct_template[1][$feedback_template_counter]['ft_template']);
                        $ft_step = $direct_template[1][$feedback_template_counter]['ft_step'];
                        // Fetching all the variables from current feedback templates
                        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $ft_template, $current_feedback_template_variable);
                        // create QF array
                        $templateFeedbackData = [
                            'template' => $ft_template,
                            'step' => $ft_step,
                        ];
                        array_push($allFeedbackArray, $templateFeedbackData);
                        $feedback_counter_check++;
                    }

                    if (!empty($current_feedback_template_variable[1])) {
                        //Adding variables of currect template to previous array.
                        $feedback_variable += $current_feedback_template_variable[1];
                    }

                    // Post increment of $feedback_template_counter to  move to next row
                    $feedback_template_counter++;
                }

                // Checking if 3 feedback for each Question template is present or not
                if ($feedback_counter_check != 3) {
                    DB::rollBack();
                    $responseData = [
                        'status' => 'failed',
                        'message' => Lang::get('admins.feedback_missing') . $qt_id,
                    ];
                    return $responseData;
                }

                //For counting the number of group present for current
                $question_variable_group_counter = 0;
                //group counter to compare the group number of the variable set
                $group_counter = 0;
                while ($question_variable_group_counter < $question_variable_count) {
                    // Counting the number of variable group set present in the question variable sheet for current template
                    if ($direct_template[2][$question_variable_group_counter]['qt_id'] == $qt_id && $direct_template[2][$question_variable_group_counter]['qv_group'] != $group_counter) {
                        $group_counter++;
                    }
                    $question_variable_group_counter++;
                }
                while ($group_counter != 0) {
                    // Counter to check the group number of the current row
                    $question_variable_counter = 0;

                    // Making variable_array empty for each group
                    $variable_array = array();
                    while ($question_variable_counter < $question_variable_count) {
                        // Checking group and qt_id for variables
                        if ($direct_template[2][$question_variable_counter]['qt_id'] == $qt_id && $direct_template[2][$question_variable_counter]['qv_group'] == $group_counter) {
                            //Adding value type symbol to values
                            if ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_CURRENCY) {
                                // Adding Euro sign before the value
                                $value = TemplateUtilityService::returnSymbol() . number_format($direct_template[2][$question_variable_counter]['qv_value'], $direct_template[2][$question_variable_counter]['rounding_value'], ',', '.');
                            } elseif ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_PERCENTAGE) {
                                $value = number_format(($direct_template[2][$question_variable_counter]['qv_value'] * 100), $direct_template[2][$question_variable_counter]['rounding_value'], ',', '.') . '\\%';
                            } elseif ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_NUMBER) {
                                if ($direct_template[2][$question_variable_counter]['qv_variable'] == self::ANSWER) {
                                    $value = round($direct_template[2][$question_variable_counter]['qv_value'], $direct_template[2][$question_variable_counter]['rounding_value']);
                                } else {
                                    $value = number_format($direct_template[2][$question_variable_counter]['qv_value'], $direct_template[2][$question_variable_counter]['rounding_value'], ',', '.');
                                }
                            } else {
                                $value = $direct_template[2][$question_variable_counter]['qv_value'];
                            }

                            // Storing answer in variables
                            if ($direct_template[2][$question_variable_counter]['qv_variable'] == self::ANSWER) {
                                if ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_CURRENCY) {
                                    $correct_answer = round($direct_template[2][$question_variable_counter]['qv_value'], $direct_template[2][$question_variable_counter]['rounding_value']);
                                } elseif ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_PERCENTAGE) {
                                    $correct_answer = number_format(($direct_template[2][$question_variable_counter]['qv_value'] * 100), $direct_template[2][$question_variable_counter]['rounding_value'], '.', ',') . "%";
                                } else {
                                    $correct_answer = $value;
                                }
                            }

                            // Storing UPPERBOND in variables
                            if ($direct_template[2][$question_variable_counter]['qv_variable'] == self::UPPERBOUND) {
                                if ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_CURRENCY) {
                                    $upper_bound = round($direct_template[2][$question_variable_counter]['qv_value'], $direct_template[2][$question_variable_counter]['rounding_value']);
                                } elseif ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_PERCENTAGE) {
                                    $upper_bound = number_format(($direct_template[2][$question_variable_counter]['qv_value'] * 100), $direct_template[2][$question_variable_counter]['rounding_value'], '.', ',') . "%";
                                } else {
                                    $upper_bound = $value;
                                }
                            } else {
                                $lower_bound = null;
                            }

                            // / Storing LOWERBOUND in variables
                            if ($direct_template[2][$question_variable_counter]['qv_variable'] == self::LOWERBOUND) {
                                if ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_CURRENCY) {
                                    $lower_bound = round($direct_template[2][$question_variable_counter]['qv_value'], $direct_template[2][$question_variable_counter]['rounding_value']);
                                } elseif ($direct_template[2][$question_variable_counter]['value_type'] == self::TYPE_PERCENTAGE) {
                                    $lower_bound = number_format(($direct_template[2][$question_variable_counter]['qv_value'] * 100), $direct_template[2][$question_variable_counter]['rounding_value'], '.', ',') . "%";
                                } else {
                                    $lower_bound = $value;
                                }
                            } else {
                                $lower_bound = null;
                            }
                            if ($direct_template[2][$question_variable_counter]['qv_variable'] == '') {
                                DB::rollBack();
                                $responseData = [
                                    'status' => 'failed',
                                    'message' => Lang::get('admins.empty_question_variable'),
                                ];
                                return $responseData;
                            }
                            Log::info($direct_template[2][$question_variable_counter]['qv_variable']);

                            // Storing variable into array in key value form : key is variable name in sheet and value is value given in sheet to that variable
                            $variable_array[trim($direct_template[2][$question_variable_counter]['qv_variable'])] = $value;
                        }

                        $question_variable_counter++;
                    }
                    $variable_name = array_map('trim', array_keys($variable_array));

                    if ((!empty(array_diff(array_unique($template_variable[1]), array_unique($variable_name)))) || (!empty(array_diff(array_unique($feedback_variable), array_unique($variable_name))))) {
                        DB::rollBack();

                        $error_details = [
                            'qt_id' => $qt_id,
                            'ft_id' => $ft_id,
                            'Error_variable_question_template' => array_diff(array_unique($template_variable[1]), array_unique($variable_name)),
                            'Error_variable_feedback_template' => array_diff(array_unique($feedback_variable), array_unique($variable_name)),
                            'qt_variables' => array_unique($template_variable[1]),
                            'ft_variables' => array_unique($feedback_variable),
                            'question_variables' => array_unique($variable_name),
                        ];

                        session()->put('error_details', $error_details);
                        $responseData = [
                            'status' => 'failed',
                            'message' => Lang::get('admins.qt_variable_mismatch'),
                        ];
                        return $responseData;
                    }

                    // this variable contain all the variable from excel sheet for the current group in json format
                    $all_variable = json_encode($variable_array);

                    // Changing the group number and re parsing in reverse order
                    $group_counter--;
                    //create QV array
                    $AllTemplateVariable[] = array(
                        'values' => $all_variable,
                        'correct_answer' => $correct_answer,
                    );
                }
                //check for all array exists
                $questionTemplateData = [
                    'template' => $qt_template,
                    'language' => 'nl',
                    'complexity' => $qt_complexity,
                    'type' => strtolower(config('constants.DIRECT_TYPE')),
                    'learningGoal_id' => $lg_id,
                    'order' => QuestionTemplate::getTemplateQuestion($lg_id, config('constants.ACTIVE'), strtolower(config('constants.DIRECT_TYPE')))->getAll()->count() + 1,
                ];
                $currentQuestionTemplate = QuestionTemplate::getCurrentTemplateQuestion($lg_id, config('constants.ACTIVE'), $qt_template, $qt_complexity)
                    ->first();

                if ($currentQuestionTemplate) {
                    //Question template exist
                    $Questionfeedback_1 = QuestionFeedback::getCurrentQuestionFeedback($allFeedbackArray[0]['template'], $allFeedbackArray[0]['step'], $currentQuestionTemplate->id);

                    $Questionfeedback_2 = QuestionFeedback::getCurrentQuestionFeedback($allFeedbackArray[1]['template'], $allFeedbackArray[1]['step'], $currentQuestionTemplate->id);

                    $Questionfeedback_3 = QuestionFeedback::getCurrentQuestionFeedback($allFeedbackArray[2]['template'], $allFeedbackArray[2]['step'], $currentQuestionTemplate->id);

                    if ($Questionfeedback_1 && $Questionfeedback_2 && $Questionfeedback_3) {
                        // All 3 exists
                        //check if template variable exists
                        foreach ($AllTemplateVariable as $allVariables) {
                            $currentTemplateVariable = TemplateVariable::getCurrentTemplateVariable($allVariables['correct_answer'], config('constants.FINISHED'), $currentQuestionTemplate->id);

                            if ($currentTemplateVariable) {
                                // correct answer exists
                                $optionChange = array_diff_assoc(json_decode(
                                    $currentTemplateVariable->values, true),
                                    json_decode($allVariables['values'], true));
                                if (!$optionChange) { //No change in MCQ options
                                    // pop from array
                                } else {
                                    // change in MCQ questions
                                    // insert question variable
                                    $allVariables['question_template_id']
                                    = $currentQuestionTemplate->id;
                                    try {
                                        $variableInserted = TemplateVariable::insertTemplateVariable(
                                            $allVariables);
                                    } catch (\Exception $e) {
                                        DB::rollBack();
                                        $responseData = [
                                            'status' => 'failed',
                                            'message' => Lang::get('admins.qt_variable_mismatch'),
                                        ];
                                        return $responseData;
                                    }
                                }
                            } else {
                                // Not exists
                                // insert question variable
                                $allVariables['question_template_id']
                                = $currentQuestionTemplate->id;
                                try {
                                    $variableInserted = TemplateVariable::insertTemplateVariable(
                                        $allVariables);
                                } catch (\Exception $e) {
                                    DB::rollBack();
                                    $responseData = [
                                        'status' => 'failed',
                                        'message' => Lang::get('admins.qt_variable_mismatch'),
                                    ];
                                    return $responseData;
                                }
                            }
                        }
                    } else {
                        // feedback not exists
                        // Inserted question variable and fedbacks
                        $runJob = true;
                        $questionTemplateIDArray['questionTemplate'] = $currentQuestionTemplate->id;
                        array_push($inActiveTemplateQuestion, $questionTemplateIDArray);
                        try {
                            foreach ($AllTemplateVariable as $insertRecordArray) {
                                $insertRecordArray['question_template_id']
                                = $currentQuestionTemplate->id;
                                $variableInserted = TemplateVariable::insertTemplateVariable(
                                    $insertRecordArray);
                            }
                        } catch (\Exception $e) {
                            DB::rollBack();
                            $responseData = [
                                'status' => 'failed',
                                'message' => Lang::get('admins.qt_variable_mismatch'),
                            ];
                            return $responseData;
                        }

                        try {
                            foreach ($allFeedbackArray as $insertFeedbackRecord) {
                                $insertFeedbackRecord['question_template_id']
                                = $currentQuestionTemplate->id;
                                $feedbackInserted = QuestionFeedback::insertQuestionFeedback(
                                    $insertFeedbackRecord);
                            }
                        } catch (\Exception $e) {
                            DB::rollBack();
                            $responseData = [
                                'status' => 'failed',
                                'message' => Lang::get('admins.feedback_db_error'),
                            ];
                            return $responseData;
                        }
                    }
                } else {
                    // Question template exist
                    // Insert All
                    try {
                        $insertedQuestionTemplate = QuestionTemplate::
                            insertQuestionTemplate($questionTemplateData);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $responseData = [
                            'status' => 'failed',
                            'message' => Lang::get('admins.question_db_error'),
                        ];
                        return $responseData;
                    }
                    try {
                        foreach ($AllTemplateVariable as $insertRecordArray) {
                            $insertRecordArray['question_template_id']
                            = $insertedQuestionTemplate->id;
                            $variableInserted = TemplateVariable::insertTemplateVariable(
                                $insertRecordArray);
                        }
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $responseData = [
                            'status' => 'failed',
                            'message' => Lang::get('admins.qt_variable_mismatch'),
                        ];
                        return $responseData;
                    }
                    try {
                        foreach ($allFeedbackArray as $insertFeedbackRecord) {
                            $insertFeedbackRecord['question_template_id']
                            = $insertedQuestionTemplate->id;
                            $feedbackInserted = QuestionFeedback::insertQuestionFeedback(
                                $insertFeedbackRecord);
                        }
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $responseData = [
                            'status' => 'failed',
                            'message' => Lang::get('admins.feedback_db_error'),
                        ];
                        return $responseData;
                    }
                }
            }

            DB::commit();
            if ($runJob === true) {
                dispatch(new ChangeQuestionAnswerStatus($data, $templateData = null, $inActiveTemplateQuestion));
            }
            $responseData = [
                'status' => 'success',
                'message' => Lang::get('admins.direct') . ' ' . Lang::get('admins.question_upload'),
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.sheet_error'),
            ];
        } finally {
            return $responseData;
        }
    }

    public function mcq($mcq_template, $data)
    {
        try {
            $response = null;
            $jobData = array();
            $inactiveQuestion = array();
            $runJob = false;
            $insertAllData = true;
            DB::beginTransaction();
            // If option for changing status is yes
            if ($data['status_change'] == self::YES) {
                QuestionTemplate::updatetemplateStatus(self::TYPE_MCQ, $data);
                $runJob = true;
            }
            // Counting the number of mcq template present
            $mcq_question_count = count($mcq_template[0]);

            // Counting the number of mcq option in the answer sheet
            $mcq_option_count = count($mcq_template[1]);
            $mcq_template_count = 0;

            while ($mcq_template_count < $mcq_question_count) {
                // Fetching data from the sheet and storing into variables
                $mcq_id = $mcq_template[0][$mcq_template_count]['mcq_id'];
                $lg_id = $mcq_template[0][$mcq_template_count]['lg_id'];
                $question = trim($mcq_template[0][$mcq_template_count]['question']);
                $correct_answer_index = $mcq_template[0][$mcq_template_count]['correctanswer'];
                $mcq_complexity = $mcq_template[0][$mcq_template_count]['complexity'];
                // Counter to count the number of rows parsed
                $mcq_template_count++;

                // Counter to count number of option parsed for current question
                $option_counter = 0;
                $option = array();

                while ($option_counter < $mcq_option_count) {
                    // Checking if question is same as current question id
                    if ($mcq_template[1][$option_counter]['mcq_id'] == $mcq_id) {
                        $option[$mcq_template[1][$option_counter]['mca_id']] = trim($mcq_template[1][$option_counter]['answer']);
                    }
                    $option_counter++;
                }

                // Getting the value of correct answer with index
                $correct_answer = $option[$correct_answer_index];
                // Converting option array into json
                $all_values = json_encode($option, true);

                $questionTemplateData = [
                    'template' => $question,
                    'language' => 'nl',
                    'complexity' => $mcq_complexity,
                    'type' => strtolower(config('constants.MCQ_TYPE')),
                    'learningGoal_id' => $lg_id,
                    'order' => QuestionTemplate::getTemplateQuestion($lg_id, config('constants.ACTIVE'), strtolower(config('constants.MCQ_TYPE')))->getAll()->count() + 1,
                ];
                $templateVariableData = [
                    'values' => $all_values,
                    'correct_answer' => $correct_answer,
                ];
                $currentQuestionTemplate = QuestionTemplate::getCurrentTemplateQuestion($lg_id, config('constants.ACTIVE'), $question, $mcq_complexity)
                    ->get();
                // Log::info($questionTemplateData);
                if ($currentQuestionTemplate->count() > 0) {
                    Log::info("incount " . $currentQuestionTemplate->count());
                    $questionTemplateIds = $currentQuestionTemplate->pluck('id');
                    //Question Template exists
                    $existingTemplateVariables = TemplateVariable::allQuestionTemplatesVariable($correct_answer, config('constants.FINISHED'),
                        $questionTemplateIds);
                    if ($existingTemplateVariables->count() > 0) {
                        $existingVariableValues = $existingTemplateVariables
                            ->pluck('values');
                        foreach ($existingVariableValues as $existingValue) {
                            //Template variable exists
                            $optionChange = array_diff_assoc(json_decode(
                                $existingValue, true), $option);
                            if (!$optionChange) {
                                //No change in MCQ options
                                $insertAllData = false;
                            } else {
                                // Insert All Data
                                $insertAllData = true;
                                break;
                            }
                        }
                    } else {
                        // Insert All Data
                        $insertAllData = true;
                    }
                } else {
                    $insertAllData = true;
                }
                if ($insertAllData === true) {
                    //Question Template not exists
                    // insert question template and template variable
                    try {
                        $insertedQuestionTemplate = QuestionTemplate::insertQuestionTemplate($questionTemplateData);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $responseData = [
                            'status' => 'failed',
                            'message' => Lang::get('admins.question_db_error'),
                        ];
                        return $responseData;
                    }
                    try {
                        $templateVariableData['question_template_id']
                        = $insertedQuestionTemplate->id;
                        TemplateVariable::insertTemplateVariable(
                            $templateVariableData);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $responseData = [
                            'status' => 'failed',
                            'message' => Lang::get('admins.variable_db_error'),
                        ];
                        return $responseData;
                    }
                }
            }
            DB::commit();
            if ($runJob === true) {
                dispatch(new ChangeQuestionAnswerStatus($data, $jobData, $inActiveTemplateQuestion = null));
            }
            $responseData = [
                'status' => 'success',
                'message' => Lang::get('admins.mcq') . ' ' . Lang::get('admins.question_upload'),
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.sheet_error'),
            ];
        } finally {
            return $responseData;
        }
    }

    /**
     * Function to extract all MCQ question per learning goal
     * @param array $questionTemplates
     * @return array $mcqQuestionTemplates
     */

    public static function getMcqQuestions($questionTemplates)
    {
        $mcqQuestionTemplates = [];
        $mcqOptions = [];
        $mcqOptionKey = 0;
        foreach ($questionTemplates as $key => $questionTemplate) {
            $questionAnswer = $questionTemplate->questionAnswers()->first();
            $options = json_decode($questionAnswer->option, true);
            $mcqQuestionTemplates[$key]['MCQ_id'] = $key + 1;
            $mcqQuestionTemplates[$key]['LG_id'] = $questionTemplate->learningGoal_id;
            $mcqQuestionTemplates[$key]['Question'] = $questionTemplate->template;
            $mcqQuestionTemplates[$key]['CorrectAnswer'] = array_search($questionAnswer->answer, $options);
            $mcqQuestionTemplates[$key]['Complexity'] = ($questionTemplate->complexity) . '';
            foreach ($options as $key1 => $option) {
                $mcqOptions[$mcqOptionKey]['MCA_id'] = $mcqOptionKey + 1;
                $mcqOptions[$mcqOptionKey]['MCQ_id'] = $mcqQuestionTemplates[$key]['MCQ_id'];
                $mcqOptions[$mcqOptionKey]['Answer'] = $option;
                $mcqOptionKey++;
            }
        }
        return $mcqQuestionData = [
            'mcqQuestionTemplates' => $mcqQuestionTemplates,
            'mcqOptions' => $mcqOptions,
            'learningGoalName' => $questionTemplate->name,
            'learningGoalID' => $questionTemplate->learningGoal_id,
        ];
    }

    /**
     * Function to extract all Direct question per learning goal
     * @param array $questionTemplates
     * @return array $directQuestionTemplates
     */

    public static function getDirectQuestions($questionTemplates)
    {
        $directQuestionTemplates = [];
        $feedbackTemplates = [];
        $templateVariables = [];
        $feedbackKey = 0;
        $templateVariableKey = 0;
        foreach ($questionTemplates as $key => $questionTemplate) {
            $directQuestionTemplates[$key]['QT_id'] = $key + 1;
            $directQuestionTemplates[$key]['LG_id'] = $questionTemplate->learningGoal_id;
            $directQuestionTemplates[$key]['QT_template'] = $questionTemplate->template;
            $directQuestionTemplates[$key]['QT_complexity'] = $questionTemplate->complexity;
            foreach ($questionTemplate->questionFeedbacks as $key1 => $questionFeedback) {
                $feedbackTemplates[$feedbackKey]['FT_id'] = $feedbackKey + 1;
                $feedbackTemplates[$feedbackKey]['QT_id'] = $directQuestionTemplates[$key]['QT_id'];
                $feedbackTemplates[$feedbackKey]['FT_template'] = $questionFeedback->template;
                $feedbackTemplates[$feedbackKey]['FT_step'] = $questionFeedback->step;
                $feedbackKey++;
            }
            $templateVariables = array_merge(
                $templateVariables,
                TemplateUtilityService::generateExportData(
                    $questionTemplate->templateVariables()->where('status_edit', static::NO)->get(),
                    $directQuestionTemplates[$key]['QT_id'],
                    sizeof($templateVariables) + 1
                )
            );
        }
        return $directQuestionData = [
            'directQuestionTemplates' => $directQuestionTemplates,
            'feedbackTemplates' => $feedbackTemplates,
            'templateVariables' => $templateVariables,
            'learningGoalName' => $questionTemplate->name,
            'learningGoalID' => $questionTemplate->learningGoal_id,

        ];
    }

    /**
     * Function to create datatable for all question template
     * @param string $slug, $type, $curriculumSlug
     * @return array $getTemplatesList
     */

    public static function getTemplatesList($slug, $type, $curriculumSlug = null)
    {
        if ($curriculumSlug) {
            $templateLists = LearningGoal::allCurriculumLearningGoalQuestions($curriculumSlug, $type);
        } else {
            $templateLists = LearningGoal::allLearningGoalQuestions
                ($slug, $type, $curriculumSlug);
        }
        return Datatables::of($templateLists)
            ->editColumn(
                'complexity',
                function ($templateList) {
                    return config('constants.COMPLEXITY')[$templateList->complexity];
                }
            )
            ->editColumn(
                'action',
                function ($templateList) use ($type, $slug) {
                    return view('templates.actions', [
                        'editTemplateRoute' => route('template.editQuestionTemplate',
                            [$slug, $type, $templateList->id]),
                        'editVariableSetRoute' => route('variable-set.showVariableSet',
                            [$slug, $templateList->id]),
                        'type' => $type,
                    ])->render();
                }
            )
            ->rawColumns(['com', 'action'])
            ->make(true);
    }

    /**
     * Export template variables
     *
     * @param  Collection $templateVariables
     * @return void
     */
    public static function exportVariableSet(
        $templateVariables,
        $question_template_id
    ) {
        $data = TemplateUtilityService::generateExportData(
            $templateVariables, $question_template_id);

        Excel::create(
            Carbon::now(),
            function ($excel) use ($data) {
                $excel->sheet(
                    Lang::get('admins.variable_set'),
                    function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                    }
                );
            }
        )->export('xlsx');
    } //end exportVariableSet()
}
