<?php

namespace App\Services;

use Closure;

class QuestionAnswerRenderService
{

    /**
     * Handle the data
     *
     * @param collection $data
     * @return string
     */
    public static function handle($questionAnswer, Closure $next)
    {
        $questionAnswer->question = str_replace('\%', '%', $questionAnswer->question);
        if ($questionAnswer->option) {
            //$questionAnswer->option = str_replace('\%', '%', $questionAnswer->option);
            $questionAnswer->option = $questionAnswer->option ?
            json_decode($questionAnswer->option, true) : $questionAnswer->option;
        }
        return $next($questionAnswer);
    }
}
