<?php

namespace App\Services;

use Config;
use Illuminate\Support\Facades\Artisan;

class EmailConfiguration
{
    /**
     * Change mailing configuration
     *
     * @return string
     */
    public static function change()
    {
        $config = array(
            'driver' => config('mail.driver'),
            'host' => config('mail.host'),
            'port' => config('mail.port'),
            'from' => array(
                'address' => config('constants.MAIL_FEEDBACK_FROM_ADDRESS'),
                'name' => config('constants.MAIL_FROM_NAME'),
            ),
            'encryption' => config('mail.encryption'),
            'username' => config('constants.MAIL_FEEDBACK_USERNAME'),
            'password' => config('constants.MAIL_FEEDBACK_PASSWORD'),
            'sendmail' => config('mail.sendmail'),
            'markdown' => config('mail.markdown'),
        );

        Config::set('mail', $config);

        Artisan::call('config:clear');
        Artisan::call('config:cache');
    }
}
