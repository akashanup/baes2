<?php

namespace App\Services;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Datatables;

class AclService
{
    /**
     * Permissions datatable
     *
     * @return json
     */
    public static function permissions()
    {
        $permissions = Permission::getAll();
        return Datatables::of($permissions)
            ->editColumn(
                'action',
                function ($permission) {
                    return view('acl.permissions.actions', [
                        'permissionEditRoute' =>
                        route('permissions.edit', $permission->id),
                        'permissionDeleteRoute' =>
                        route('permissions.destroy', $permission->id),
                    ])->render();
                }
            )
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Permissions datatable
     *
     * @return json
     */
    public static function roles()
    {
        $roles = Role::getAll();
        return Datatables::of($roles)
            ->editColumn(
                'action',
                function ($role) {
                    return view('acl.roles.actions', [
                        'rolePermissionsRoute' =>
                        route('roles.permissions.index', $role->id),
                        'roleEditRoute' =>
                        route('roles.edit', $role->id),
                        'roleDeleteRoute' =>
                        route('roles.destroy', $role->id),
                    ])->render();
                }
            )
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * User datatable
     *
     * @return json
     */
    public static function users()
    {
        $users = User::getAll();
        return Datatables::of($users)
            ->editColumn('name', function ($user) {
                return $user->first_name . ' ' . $user->last_name;
            })
            ->editColumn('email', function ($user) {
                return $user->email;
            })
            ->editColumn('role', function ($user) {
                $role = Role::getRoleName($user->role_id)->first();
                return $role->name;
            })
            ->editColumn(
                'action',
                function ($user) {
                    return view('acl.users.actions', [
                        'aclUserEditRoute' =>
                        route('acl.users.edit', $user->id),
                        'aclUserDeleteRoute' =>
                        route('acl.users.destroy', $user->id),
                    ])->render();
                }
            )
            ->rawColumns(['name', 'email', 'role', 'action'])
            ->make(true);
    }
}
