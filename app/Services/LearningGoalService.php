<?php

namespace App\Services;

use App\Models\LearningGoal;
use App\Models\QuestionAnswer;
use App\Services\StudentService;
use Datatables;

class LearningGoalService
{
    /**
     * Permissions datatable
     *
     * @return json
     */
    public static function learningGoals($slug)
    {
        $learningGoals = LearningGoal::getAllLearningGoals($slug);
        return Datatables::of($learningGoals)
            ->editColumn(
                'theory',
                function ($learningGoal) {
                    return view('learning-goals.action', [
                        'theoryRoute' => route('learning-goals.edit', $learningGoal->slug),
                        'deleteLearningGoal' => route('learning-goals.destroy',
                            $learningGoal->slug),
                        'uploadTheoryPdf' => route('learning-goals.uploadTheoryPdf',
                            $learningGoal->slug),
                        'printTheoryPdf' => route('learning-goals.theoryPreview',
                            $learningGoal->slug),

                    ])->render();
                }
            )
            ->editColumn(
                'mcq',
                function ($learningGoal) {
                    return view('learning-goals.action', [
                        'mcqRoute' => route('template.edit', [$learningGoal->slug, 'MCQ']),
                        'mcqDownloadRoute' => route('template.downloadQuestionTemplate', [$learningGoal->slug, 'MCQ']),
                        'uploadMcqTemplate' => route('template.upload', [$learningGoal->slug, config('constants.MCQ_TYPE')]),
                        'mcqQuestionPdf' => route('question-template.downloadPdf', [$learningGoal->slug, 'MCQ']),
                    ])->render();
                }
            )
            ->editColumn(
                'direct',
                function ($learningGoal) {
                    return view('learning-goals.action', [
                        'editDirect' => route('template.edit', [$learningGoal->slug, 'DIRECT']),
                        'exportData' => route('template.downloadQuestionTemplate', [$learningGoal->slug, 'DIRECT']),
                        'exportVariable' => route('template.downloadVariableSet', $learningGoal->slug),
                        'uploadVariable' => route('variable-set.upload', $learningGoal->slug),
                        'uploadDirectTemplate' => route('template.upload', [$learningGoal->slug, config('constants.DIRECT_TYPE')]),
                        'directQuestionPdf' => route('question-template.downloadPdf', [$learningGoal->slug, 'DIRECT']),
                    ])->render();
                }
            )
            ->rawColumns(['theory', 'mcq', 'direct'])
            ->make(true);
    }

    /**
     * LearningGoal questions datatable
     *
     * @return json
     */
    public static function learningGoalQuestions($unique, $learingGoalId)
    {
        $questionAnswers = QuestionAnswer::learningGoalQuestions
            ($unique, $learingGoalId);
        return Datatables::of($questionAnswers)
            ->editColumn(
                'complexity',
                function ($questionAnswer) {
                    return StudentService::getBaesLevelByComplexity($questionAnswer->complexity);
                }
            )
            ->editColumn(
                'action',
                function ($questionAnswer) {
                    return view('teachers.action', [
                        'questionAnswerId' => $questionAnswer->id,
                    ]);
                }
            )
            ->rawColumns(['complexity', 'action'])
            ->make(true);
    }
}
