<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Lang;

class VariableSetImportException extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * LearningGoal name
     *
     * @var string
     */
    protected $learningGoal;

    /**
     * File
     *
     * @var file
     */
    protected $file;

    /**
     * Error/Exception message
     *
     * @var string
     */
    protected $exceptionMessage;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($learningGoal, $exceptionMessage, $file = null)
    {
        $this->learningGoal = $learningGoal;
        $this->exceptionMessage = $exceptionMessage;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->markdown('emails.exportVariableSetDeveloper',
            [
                'learningGoal' => $this->learningGoal,
                'exceptionMessage' => $this->exceptionMessage,
            ]
        )
            ->subject($this->learningGoal . Lang::get('admins.variable_set_upload'));
        if ($this->file) {
            $mail->attach($this->file);
        }
        return $mail;
    }
}
