<?php

namespace App\Exceptions;

use App\Exceptions\NoQuestionFoundException;
use App\Mail\EmailException;
use App\Services\DashboardService;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Lang;
use Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = ['password', 'password_confirmation'];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception)) {
            if (config('app.env') == config('constants.PRODUCTION')) {
                $developers = json_decode(config('app.developers'), true);
                Mail::to(current($developers))->cc($developers)
                    ->send(new EmailException($exception));
            } else {
                Log::warning($exception);
            }
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $response = null;
        if (!($exception instanceof ValidationException)) {
            $message = Lang::get('globals.request_error_message');
            $statusCode = 500;
            if ($exception instanceof AuthenticationException) {
                $message = Lang::get('globals.unauthenticated_text');
                $statusCode = 401;
            }
            if ($request->expectsJson()) {
                if ($exception instanceof NoQuestionFoundException) {
                    $message = Lang::get('users.no_question_found');
                    $statusCode = 401;
                }
                $response = response()->json(['success' => false,
                    'errors' => $message], $statusCode);
            } else {
                if ($request->isMethod('GET')) {
                    $response = redirect(DashboardService::dashboardRoute())
                        ->withErrors($message);
                } else {
                    $response = redirect()->back()->withErrors($message);
                }
            }
        }
        //return parent::render($request, $exception);
        return $response ? $response : parent::render($request, $exception);
    }
}
