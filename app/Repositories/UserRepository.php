<?php

namespace App\Repositories;

use App\Events\AssignActivePracticeTestsToUserEvent;
use App\Events\AssignLearningGoalToUserEvent;
use App\Models\Course;
use App\Models\Grade;
use App\Models\LearningGoal;
use App\Models\LearningGoalTest;
use App\Models\License;
use App\Models\PracticeTestDetail;
use App\Models\PracticeTestQuestion;
use App\Models\QuestionAnswer;
use App\Models\StudentPracticeTestStatus;
use App\Models\Subject;
use App\Models\User;
use App\Services\PracticeTestService;
use App\Services\QuestionAnswerRenderService;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pipeline\Pipeline;
use Lang;

class UserRepository
{
    /**
     * Attach learning goal to student
     *
     * @param App\Models\User $user
     * @param App\Models\License $license
     * @return void
     */
    public static function attachLearningGoalToStudent(
        User $user,
        License $license
    ) {
        $learningGoals = Subject::join('course_subjects', 'subjects.id',
            'course_subjects.subject_id')->join('subject_learning_goals',
            'subjects.id', 'subject_learning_goals.subject_id')->join(
            'learning_goals', 'subject_learning_goals.learning_goal_id',
            'learning_goals.id')->select('learning_goals.id as learning_goal_id',
            'subject_learning_goals.order', 'subjects.id as subject_id')
            ->where('course_subjects.course_id', $license->course_id)
            ->orderBy('subject_learning_goals.order')->get()->toArray();

        $studentLearningGoals = [];
        $userLicenseDetail = ['license_id' => $license->id,
            'user_id' => $user->id];
        foreach ($learningGoals as $key => $learningGoal) {
            $studentLearningGoals[$key] = array_merge($learningGoal, $userLicenseDetail,
                ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        }

        DB::Table('student_learning_goals')->insert($studentLearningGoals);
    }

    /**
     * Get learningGoal data of a subject for a student
     *
     * @param ineger $userId User id
     * @param integer $licenseId License id
     * @param string $subjectSlug Subject slug
     * @return Collection
     */
    public static function getStudentSubjectLearningGoals(
        $userId,
        $licenseId,
        $subjectSlug
    ) {
        return Subject::join('student_learning_goals', 'subjects.id',
            'student_learning_goals.subject_id')->join('learning_goals',
            'student_learning_goals.learning_goal_id', 'learning_goals.id')
            ->select('subjects.name as subjectName', 'subjects.id as subjectId',
                'learning_goals.name', 'student_learning_goals.status',
                'student_learning_goals.finished_score',
                'student_learning_goals.current_score',
                'student_learning_goals.order',
                'student_learning_goals.user_id as userId',
                'student_learning_goals.subject_id as subjectId',
                'student_learning_goals.learning_goal_id as learningGoalId',
                'student_learning_goals.license_id as licenseId',
                'subjects.slug as subjectSlug',
                'learning_goals.slug as learningGoalSlug')
            ->where('student_learning_goals.user_id', $userId)
            ->where('student_learning_goals.license_id', $licenseId)
            ->where('subjects.slug', $subjectSlug)
            ->orderBy('student_learning_goals.order')->get();
    }

    /**
     * Update status of student learning goal to started
     *
     * @param ineger $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getStudentSubject(
        $userId,
        $licenseId,
        $subjectId
    ) {
        return DB::Table('student_learning_goals')->where('user_id', $userId)
            ->where('license_id', $licenseId)->where('subject_id', $subjectId);
    }

    /**
     * Update status of student learning goal to started
     *
     * @param ineger $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @param integer $learningGoalId LearningGoal id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getStudentLearningGoal(
        $userId,
        $licenseId,
        $subjectId,
        $learningGoalId
    ) {
        return static::getStudentSubject($userId, $licenseId, $subjectId)
            ->where('learning_goal_id', $learningGoalId);
    }

    /**
     * Update status of student learning goal to started
     *
     * @param ineger $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @param integer $learningGoalId LearningGoal id
     * @return void
     */
    public static function startStudentLearningGoal(
        $userId,
        $licenseId,
        $subjectId,
        $learningGoalId
    ) {
        $updateLearningGoal = $getLearningGoal = static::getStudentLearningGoal(
            $userId, $licenseId, $subjectId, $learningGoalId);
        if ($getLearningGoal->select('status')->first()->status
            === config('constants.AVAILABLE')) {
            $updateLearningGoal->update(['status' => config('constants.STARTED'),
                'updated_at' => Carbon::now()]);
        }
    }

    /**
     * Update current score of student learning goal
     *
     * @param ineger $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @param integer $learningGoalId LearningGoal id
     * @param double $currentScore LearningGoal currentScore
     * @param double $change LearningGoal change
     * @return void
     */
    public static function updateStudentLearningGoalCurrentScore(
        $userId,
        $licenseId,
        $subjectId,
        $learningGoalId,
        $currentScore,
        $change
    ) {
        static::getStudentLearningGoal($userId, $licenseId, $subjectId,
            $learningGoalId)->update(['current_score' => $currentScore,
            'change' => $change, 'updated_at' => Carbon::now()]);
    }

    /**
     * Get next learning goal for a student
     *
     * @param integer $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @param integer $learningGoalOrder LearningGoal order
     * @return string
     */
    public static function getNextLearningGoalSlug(
        $userId,
        $licenseId,
        $subjectId,
        $learningGoalOrder
    ) {
        $response = null;
        $nextLearningGoal = static::getStudentSubject($userId, $licenseId,
            $subjectId)->where('student_learning_goals.order', '>',
            $learningGoalOrder)->orderBy('student_learning_goals.order')
            ->select('student_learning_goals.learning_goal_id')->first();
        if ($nextLearningGoal) {
            $response = LearningGoal::select('slug')->findOrFail(
                $nextLearningGoal->learning_goal_id)->slug;
        }
        return $response;
    }

    /**
     * Get student learning goal details by
     * @param integer $institutionId
     * @param integer $courseId
     * @param integer $subjectId Subject id
     * @param integer $gradeId
     * @return string
     */
    public static function getStudentsForLicenseCourse(
        $institutionId,
        $courseId,
        $subjectId,
        $gradeId = null
    ) {

        $query = User::select('users.id', 'users.first_name', 'users.last_name',
            'licenses.institution_id', 'licenses.course_id', 'student_learning_goals.order', 'student_learning_goals.finished_score', 'student_learning_goals.current_score', 'student_learning_goals.change', 'student_learning_goals.learning_goal_test_id', 'student_learning_goals.status', 'user_licenses.grade_id', 'student_learning_goals.learning_goal_id', 'student_learning_goals.updated_at', 'grades.name as gradeName', 'grades.slug as gradeSlug')
            ->join('user_licenses', 'user_licenses.user_id', 'users.id')
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->join('student_learning_goals', 'student_learning_goals.user_id',
                'users.id')
            ->join('grades', 'grades.id', 'user_licenses.grade_id')
            ->where('licenses.institution_id', $institutionId)
            ->where('licenses.course_id', $courseId)
            ->where('user_licenses.is_associated', config('constants.NO'))
            ->where('student_learning_goals.subject_id', $subjectId)
            ->where('user_licenses.blocked_by_teacher', config('constants.NO'))
            ->where('licenses.role', config('constants.STUDENT'));
        if ($gradeId) {
            $query = $query->where('user_licenses.grade_id', $gradeId);
        }
        $query = $query->orderBy('student_learning_goals.order', 'asc')
            ->orderBy('student_learning_goals.user_id', 'asc')
            ->get();
        return $query;
    }

    /**
     * Practice tests for a student
     *
     * @param App\Models\User $user User
     * @param App\Models\License $license License model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function studentPracticeTests($user, $license)
    {
        return StudentPracticeTestStatus::join('practice_test_details',
            'student_practice_test_statuses.practice_test_detail_id',
            'practice_test_details.id')->where('practice_test_details.institution_id',
            $license->institution_id)->where('practice_test_details.course_id',
            $license->course_id)->where('student_practice_test_statuses.user_id',
            $user->id)->select('student_practice_test_statuses.status',
            'practice_test_details.name', 'practice_test_details.slug',
            'student_practice_test_statuses.score', 'practice_test_details.start_date',
            'practice_test_details.end_date', 'practice_test_details.status as testStatus',
            'practice_test_details.created_by',
            'student_practice_test_statuses.practice_test_detail_id');
    }

    /**
     * Add activity points to user
     *
     * @param App\Models\User $user User
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @return void
     */
    public static function addStudentActivity(
        $user,
        $licenseId,
        $subjectId,
        $learningGoalId = null
    ) {
        $currentDate = Carbon::today()->toDateString();
        $data = ['user_id' => $user->id, 'date' => $currentDate];
        $license = License::find($licenseId);
        if ($license) {
            if ($license->role == config('constants.STUDENT')
                && Subject::find($subjectId)) {
                $todayActivity = $user->activities()
                    ->where('date', $currentDate)
                    ->where('subject_id', $subjectId)
                    ->where('license_id', $licenseId);
                if (LearningGoal::find($learningGoalId)) {
                    $todayActivity = $todayActivity->where('learning_goal_id', $learningGoalId);
                }
                $todayActivity = $todayActivity->first();
                if ($todayActivity) {
                    $todayActivity->update(['points' => ($todayActivity->points + 1)]);
                    $data = null;
                } else {
                    $data = array_merge($data, ['license_id' => $licenseId,
                        'subject_id' => $subjectId, 'learning_goal_id' => $learningGoalId,
                        'points' => 1]);
                }
            } else {
                $data = null;
            }
        } else {
            $data = array_merge($data, ['points' => 1]);
        }
        if ($data) {
            $user->activities()->create($data);
        }
    }

    /**
     * Show user licenses
     *
     * @param App\Models\User $user User
     * @return Collection
     */
    public static function userLicenses($user)
    {
        return DB::Table('user_licenses')
            ->join('licenses', 'user_licenses.license_id', 'licenses.id')
            ->join('courses', 'licenses.course_id', 'courses.id')
            ->join('institutions', 'licenses.institution_id', 'institutions.id')
            ->select('institutions.name as institution', 'courses.name as course',
                'licenses.code', 'licenses.role', 'licenses.type', 'user_licenses.status',
                'user_licenses.end_date', 'user_licenses.license_id',
                'user_licenses.user_id')->where('user_licenses.user_id', $user->id)
            ->where('user_licenses.is_associated', config('constants.NO'))
            ->groupBy('user_licenses.license_id')
            ->groupBy('user_licenses.user_id');
    }

    /**
     * Weekly activity of the subject of the user.
     *
     * @param date $endDate
     * @param integer $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @return Collection
     */
    public static function weeklyActivity(
        $endDate,
        $userId,
        $licenseId,
        $subjectId,
        $learningGoalId = null,
        $activityOverview = null,
        $lastDate = null
    ) {
        $user = User::findOrFail($userId);
        $now = new Carbon($endDate);
        if ($lastDate) {
            $last = new Carbon($lastDate->subDays(1));
        } else {
            $last = new Carbon($endDate->subWeek());
        }

        $activities = $user->activities()->select(
            DB::raw('sum(points) as activity, date'))
            ->where('date', '>', $last->toDateString());

        if (License::find($licenseId)) {
            $activities = $activities->where('student_activities.user_id', $userId)
                ->where(function ($query) use ($licenseId, $activityOverview) {
                    $query->where('student_activities.license_id', $licenseId);
                    if ($activityOverview !== true) {
                        $query->orWhereNull('student_activities.license_id');
                    }
                });
            if (Subject::find($subjectId)) {
                $activities = $activities->where(function ($query) use ($subjectId) {
                    $query->where('student_activities.subject_id', $subjectId)
                        ->orWhereNull('student_activities.subject_id');
                });
                if (LearningGoal::find($learningGoalId)) {
                    $activities = $activities->where(function ($query) use ($learningGoalId) {
                        $query->where('student_activities.learning_goal_id', $learningGoalId)
                            ->orWhereNull('student_activities.learning_goal_id');
                    });
                }
            }
        }

        $activities = $activities->orderBy('date')->groupBy('date')->get();

        // Populating the activities of unaccessed days.
        $weeklyActivities = new Collection();
        while ($last < $now) {
            $last->addDay();
            $activity = $activities->where('date', $last->toDateString())->first();
            if ($activity) {
                $activity = ['points' => $activity->activity,
                    'date' => $activity->date, 'time' => Carbon::parse(
                        $activity->create_at)->toTimeString()];
            } else {
                $activity = ['points' => 0, 'date' => $last->toDateString(),
                    'time' => '-'];
            }
            $weeklyActivities->push($activity);
        }
        return $weeklyActivities->sortBy('date');
    }

    /**
     * Add activity points to user
     *
     * @param App\Models\User $user User
     * @param array $data Student Feedback data
     * @return void
     */
    public static function addStudentFeedback($user, $data)
    {
        $user->feedbacks()->create($data);
    }

    /**
     * Get students of a course and institution
     *
     * @param integer $institutionId Institution id
     * @param integer $courseId Course id
     * @param integer $gradeId Grade id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function institutionCourseGradeStudents(
        $institutionId,
        $courseId,
        $gradeId = null
    ) {
        $students = User::join('user_licenses', 'users.id', 'user_licenses.user_id')
            ->join('licenses', 'user_licenses.license_id', 'licenses.id')
            ->where('licenses.institution_id', $institutionId)
            ->where('licenses.course_id', $courseId)
            ->where('licenses.role', config('constants.STUDENT'))
            ->where('users.status', config('constants.ACTIVE'))
            ->where('user_licenses.status', config('constants.ACTIVE'));
        if ($gradeId) {
            $students->where('user_licenses.grade_id', $gradeId);
        }
        return $students->select('users.id');
    }

    /**
     * Get practice test questions of a student
     *
     * @param App\Models\User $user User
     * @param integer $practiceTestDetailId PracticeTestDetail id
     * @return collection
     */
    public static function studentPracticeTestQuestions(
        User $user,
        $practiceTestDetailId
    ) {
        // Logic to find the learning goal order
        $practiceTestDetail = PracticeTestDetail::select('institution_id', 'course_id',
            'template')->findorFail($practiceTestDetailId);
        $template = json_decode($practiceTestDetail->template, true);
        $currentLicenseId = $user->licenses()->where('licenses.course_id',
            $practiceTestDetail->course_id)->where('licenses.institution_id',
            $practiceTestDetail->institution_id)->select('licenses.id')
            ->first()->id;
        $questionAnswers = $user->studentPracticeTestQuestions()
            ->where('practice_test_details.id', $practiceTestDetailId)->get();
        // Generate data for practice test page
        foreach ($questionAnswers as $questionAnswer) {
            $randomKey = PracticeTestQuestion::select('random_key')
                ->where('question_answer_id', $questionAnswer->pivot->question_answer_id)
                ->where('practice_test_detail_id', $practiceTestDetailId)
                ->firstOrFail()->random_key;
            $currentTemplateDetail = $template[$randomKey];
            $question = app(Pipeline::class)
                ->send(QuestionAnswer::findOrFail($questionAnswer->pivot->question_answer_id))
                ->through(QuestionAnswerRenderService::class)
                ->then(function ($question) {
                    return $question;
                });
            $questionTemplate = $question->questionTemplate;
            $questionAnswer->learningGoalName = $questionTemplate->learningGoal->name;
            $questionAnswer->learningGoalOrder = static::getStudentLearningGoal($user->id,
                $currentLicenseId, $currentTemplateDetail['subject_id'],
                $currentTemplateDetail['goal_id'])->first()->order;
            $questionAnswer->learningGoalId = $currentTemplateDetail['goal_id'];
            $questionAnswer->subjectId = $currentTemplateDetail['subject_id'];
            $questionAnswer->type = $questionTemplate->type;
            $questionAnswer->level = config('constants.COMPLEXITY')[$questionTemplate->complexity];
            $questionAnswer->question = view($questionAnswer->type ===
                config('constants.MCQ_TYPE') ? 'includes.practice-mcq-div'
                : 'includes.practice-direct-div', ['question' => $question->toArray(),
                    'answer' => $question->answer,
                    'currentAnswer' => $questionAnswer->pivot->answer,
                ])->render();
        }
        return $questionAnswers;
    }

    /**
     * Get practice test questions of a student
     *
     * @param App\Models\User $user User
     * @param integer $practiceTestDetailId PracticeTestDetail id
     * @param integer $questionAnswerId QuestionAnswer id
     * @param string $answer StudentPracticeTest answer
     * @return void
     */
    public static function storeStudentPracticeTestAnswer(
        User $user,
        $practiceTestDetailId,
        $questionAnswerId,
        $answer
    ) {
        $studentPracticeTest = $user->studentPracticeTestQuestions()
            ->where('practice_test_details.id', $practiceTestDetailId)
            ->where('student_practice_test_questions.question_answer_id',
                $questionAnswerId)->firstOrFail();
        $studentPracticeTest->pivot->answer = $answer;
        $studentPracticeTest->pivot->save();
    }

    /**
     * Get practice test questions of a student
     *
     * @param App\Models\User $user User
     * @param integer $practiceTestDetailId PracticeTestDetail id
     * @return void
     */
    public static function finishStudentPracticeTest(
        User $user,
        $practiceTestDetailId
    ) {
        $studentPracticeTest = $user->studentPracticeTestStatuses()
            ->where('practice_test_details.id', $practiceTestDetailId)
            ->firstOrFail();
        $studentPracticeTest->pivot->status = config('constants.FINISHED');
        $studentPracticeTest->pivot->score = static::calculateStudentPracticeTestScore(
            $user, $practiceTestDetailId);
        $studentPracticeTest->pivot->save();
    }

    /**
     * Calculate student practice test score
     *
     * @param App\Models\User $user User
     * @param integer $practiceTestDetailId PracticeTestDetail id
     * @return string
     */
    public static function calculateStudentPracticeTestScore(
        User $user,
        $practiceTestDetailId
    ) {
        $studentPracticeTestQuestions = $user->studentPracticeTestQuestions()
            ->where('practice_test_details.id', $practiceTestDetailId)
            ->get();
        $correctAnswerCount = 0;
        foreach ($studentPracticeTestQuestions as $studentPracticeTestQuestion) {
            if (QuestionAnswer::findOrFail($studentPracticeTestQuestion->pivot->question_answer_id)
                ->checkAnswer($studentPracticeTestQuestion->pivot->answer)) {
                $correctAnswerCount++;
                $studentPracticeTestQuestion->pivot->is_correct = config('constants.YES');
                $studentPracticeTestQuestion->pivot->save();
            }
        }
        $count = $studentPracticeTestQuestions->count();
        return $count ? number_format((($correctAnswerCount / $count) * 100), 1) : 0.0;
    }

    /**
     * Update status od student practice test
     *
     * @param Collection $studentPracticeTest
     * @return Collection
     */
    public static function updateStudentPracticeTestStatusToStart(
        $studentPracticeTest
    ) {
        $studentPracticeTest->pivot->status = config('constants.STARTED');
        $studentPracticeTest->pivot->save();
        return $studentPracticeTest;
    }

    /**
     * Get student subjects with last LearningGoal
     *
     * @param App\Models\User $user User
     * @param integer $licenseId License id
     * @param App\Models\Course $course Course id
     * @return collection
     */
    public static function getStudentSubjectsWithLastLearningGoal(
        $user,
        $licenseId,
        $course
    ) {
        $subjects = $course->subjects()->orderBy('course_subjects.order')->get();
        foreach ($subjects as $subject) {
            $lastLearningGoal = $user->activities()->select('learning_goal_id')
                ->where('subject_id', $subject->id)->whereNotNull('learning_goal_id')
                ->where('license_id', $licenseId)->orderBy('student_activities.updated_at')
                ->get()->last();
            if ($lastLearningGoal) {
                $lastLearningGoalSlug = LearningGoal::select('slug')
                    ->find($lastLearningGoal->learning_goal_id)->slug;
            } else {
                $lastLearningGoalSlug = $subject->learningGoals()->select('slug')
                    ->orderBy('subject_learning_goals.order')->first()->slug;
            }
            $subject->lastLearningGoalSlug = $lastLearningGoalSlug;
        }
        return $subjects;
    }

    /**
     * Student login by a teacher
     *
     * @param App\Models\User $user User model
     * @param integer $licenseId License id
     * @return array
     */
    public static function studentLogin($user, $licenseId)
    {
        $response = null;
        $teacherLicense = $user->licenses()->where('licenses.id', $licenseId)
            ->select('licenses.id', 'licenses.institution_id', 'licenses.course_id')
            ->firstOrFail();
        $studentLicense = $user->licenses()
            ->where('licenses.institution_id', $teacherLicense->institution_id)
            ->where('licenses.course_id', $teacherLicense->course_id)
            ->where('licenses.role', config('constants.STUDENT'))->first();
        if ($studentLicense) {
            $response = ['response' => true, 'route' => route('users.courses',
                [$studentLicense->id, Course::select('slug')->findOrFail(
                    $teacherLicense->course_id)->slug])];
        } else {
            $studentLicense = License::findByInstitution($teacherLicense->institution_id)
                ->findByCourse($teacherLicense->course_id)
                ->findByRole(config('constants.STUDENT'))->first();
            if ($studentLicense) {
                $gradeId = Grade::where('institution_id',
                    $teacherLicense->institution_id)->select('id')->firstOrFail()->id;
                $user->attachLicense($studentLicense->id,
                    ['is_associated' => config('constants.YES'),
                        'grade_id' => $gradeId]);
                event(new AssignLearningGoalToUserEvent($user, $studentLicense));
                event(new AssignActivePracticeTestsToUserEvent($user, $studentLicense,
                    $gradeId));
                $response = ['response' => true, 'route' => route('users.courses',
                    [$studentLicense->id, Course::select('slug')->findOrFail(
                        $teacherLicense->course_id)->slug])];
            } else {
                $response = ['response' => false,
                    'message' => Lang::get('teachers.no_student_license_found')];
            }
        }
        return $response;
    }

    /**
     * Teacher login by a teacher
     *
     * @param App\Models\User $user User model
     * @param integer $institutionId Institution id
     * @param integer $courseId Course id
     * @return array
     */
    public static function teacherLogin($user, $institutionId, $courseId)
    {
        $response = null;
        $license = $user->licenses()
            ->where('licenses.institution_id', $institutionId)
            ->where('licenses.course_id', $courseId)
            ->where('licenses.role', config('constants.TEACHER'))->first();
        if ($license) {
            $response = ['response' => true, 'route' => route('users.courses',
                [$license->id, Course::select('slug')->findOrFail(
                    $courseId)->slug])];
        } else {
            $license = License::findByInstitution($institutionId)
                ->findByCourse($courseId)
                ->findByRole(config('constants.TEACHER'))->first();
            if ($license) {
                $user->attachLicense($license->id,
                    ['is_associated' => config('constants.YES'),
                        'grade_id' => Grade::where('institution_id',
                            $institutionId)->select('id')->firstOrFail()->id]);
                $response = ['response' => true, 'route' => route('users.courses',
                    [$license->id, Course::select('slug')->findOrFail(
                        $courseId)->slug])];
            } else {
                $response = ['response' => false,
                    'message' => Lang::get('admins.no_teacher_license_found')];
            }
        }
        return $response;
    }

    /**
     * Get user detials with type as student.
     * @param integer $userId
     * @param integer $licenseId
     * @param integer $institutionId
     * @param integer $grade
     * @param integer $subjectId
     * @return Collection
     */
    public static function getGradeUserDetails(
        $userId,
        $licenseId,
        $institutionId,
        $grade,
        $subjectId
    ) {
        $query = User::getAll()
            ->addSelect('licenses.id as licenseId', 'grades.id as gradeId',
                'grades.name as gradeName', 'users.image')
            ->join('user_licenses', 'user_licenses.user_id', 'users.id')
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->join('grades', 'grades.id', 'user_licenses.grade_id')
            ->join('course_subjects', 'course_subjects.course_id', 'licenses.course_id')
            ->where('users.id', $userId)
            ->where('licenses.institution_id', $institutionId);
        if ($grade !== config('constants.ALL')) {
            $query = $query->where('grades.slug', $grade);
        }
        return $query->where('licenses.role', config('constants.STUDENT'))
            ->where('course_subjects.subject_id', $subjectId)
            ->first();
    }

    /**
     * Get student current license
     *
     * @param ineger $courseId Course id
     * @param integer $userId User id
     * @param integer $institutionId Institution id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getStudentLicense(
        $courseId,
        $institutionId,
        $userId
    ) {
        return License::getLicenses()
            ->addSelect('user_licenses.created_at')
            ->join('user_licenses', 'user_licenses.license_id', 'licenses.id')
            ->where('licenses.course_id', $courseId)
            ->where('licenses.institution_id', $institutionId)
            ->where('user_licenses.user_id', $userId)
            ->where('licenses.role', config('constants.STUDENT'))
            ->first();
    }

    /**
     * Get all learning goals of a student
     *
     * @param ineger $userId User id
     * @param integer $licenseId License id
     * @param integer $subjectId Subject id
     * @param integer $learningGoalId Institution id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getStudentLearningGoals(
        $userId,
        $licenseId,
        $institutionId,
        $subjectId
    ) {
        return User::getAll()
            ->addSelect('licenses.institution_id', 'licenses.course_id', 'student_learning_goals.order', 'student_learning_goals.finished_score', 'student_learning_goals.current_score', 'student_learning_goals.change', 'student_learning_goals.learning_goal_test_id', 'student_learning_goals.status', 'student_learning_goals.learning_goal_id', 'student_learning_goals.subject_id', 'student_learning_goals.license_id')
            ->join('user_licenses', 'user_licenses.user_id', 'users.id')
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->join('student_learning_goals', 'student_learning_goals.user_id', 'users.id')
            ->where('student_learning_goals.license_id', $licenseId)
            ->where('users.id', $userId)
            ->where('licenses.institution_id', $institutionId)
            ->where('student_learning_goals.subject_id', $subjectId)
            ->where('licenses.role', config('constants.STUDENT'))
            ->orderBy('student_learning_goals.order')
            ->get();
    }

    /**
     * Get all students of institution and course
     * @param integer $InstitutionId Institution id
     * @param integer $CourseId Course id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function currentInstitutionCourseStudents(
        $institutionId,
        $courseId,
        $grade,
        $year
    ) {
        $query = User::addSelect('licenses.id', 'grades.name as grade',
            'grades.year as year', 'users.id', 'users.first_name',
            'users.last_name', 'user_licenses.blocked_by_teacher')
            ->join('user_licenses', 'user_licenses.user_id', 'users.id')
            ->join('licenses', 'licenses.id', 'user_licenses.license_id')
            ->join('grades', 'grades.id', 'user_licenses.grade_id')
            ->where('licenses.institution_id', $institutionId)
            ->where('licenses.course_id', $courseId)
            ->where('licenses.role', config('constants.STUDENT'))
            ->where('users.status', config('constants.ACTIVE'))
            ->where('user_licenses.is_associated', config('constants.NO'));
        if ($grade != config('constants.ALL')) {
            $query = $query->where('grades.slug', $grade);
        }
        if ($year != config('constants.ALL')) {
            $query = $query->where('grades.year', $year);
        }
        return $query;
    }

    /**
     * Get average BAES score per student
     * @param integer $UserId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function avgBaesScoreByStudent($userId)
    {
        return DB::Table('student_learning_goals')
            ->addSelect(DB::raw("AVG(student_learning_goals.current_score) as baes_score"))
            ->where('student_learning_goals.user_id', $userId)
            ->first();
    }

    /**
     * update student grades
     * @param integer $UserId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function updateStudentsGrade($studentIds, $gradeId)
    {
        return DB::Table('user_licenses')
            ->whereIn('user_licenses.user_id', $studentIds)
            ->update(['user_licenses.grade_id' => $gradeId]);
    }

    /**
     * update student status
     * @param array $studentIds User ids
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function updateStudentsStatus($studentIds, $status)
    {
        return DB::Table('user_licenses')
            ->whereIn('user_licenses.user_id', $studentIds)
            ->update(['user_licenses.blocked_by_teacher' => $status]);
    }

    /**
     * update student name
     * @param integer $UserId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function updateStudentsName(
        $studentIds,
        $firstName,
        $lastName
    ) {
        return User::whereIn('users.id', $studentIds)
            ->update(['first_name' => $firstName, 'last_name' => $lastName]);
    }

    /**
     * get students by Grade Id
     * @param integer $UserId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function gradeInstitutionStudents(
        $institutionId,
        $courseId,
        $gradeId = null
    ) {
        return static::institutionCourseGradeStudents(
            $institutionId, $courseId, $gradeId)
            ->where('user_licenses.is_associated', config('constants.NO'))
            ->get();
    }

    /**
     * get students finished test questions
     * @param integer $UserId User id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getStudentFinishedTestQuestions(
        $userId,
        $licenseId,
        $subjectId,
        $learningGoalId
    ) {
        return QuestionAnswer::addSelect('question_answers.question',
            'question_answers.option', 'question_answers.answer',
            'learning_goals.name', 'question_templates.complexity',
            'learning_goal_test_question_answers.answer as answer_given',
            'student_learning_goals.order', 'student_learning_goals.finished_score')
            ->join('learning_goal_test_question_answers',
                'learning_goal_test_question_answers.question_answer_id',
                'question_answers.id')
            ->join('student_learning_goals',
                'student_learning_goals.learning_goal_test_id',
                'learning_goal_test_question_answers.learning_goal_test_id')
            ->join('question_templates', 'question_templates.id',
                'question_answers.question_template_id')
            ->join('learning_goals', 'learning_goals.id',
                'question_templates.learningGoal_id')
            ->where('student_learning_goals.user_id', $userId)
            ->where('student_learning_goals.license_id', $licenseId)
            ->where('student_learning_goals.subject_id', $subjectId)
            ->where('student_learning_goals.learning_goal_id', $learningGoalId)
            ->get();
    }
    /**
     * Get students of a practice test
     *
     * @param App\Models\PracticeTestDetail $practiceTestDetail PracticeTestDetail model
     * @return Collection
     */
    public static function studentsOfPracticeTest(PracticeTestDetail $practiceTestDetail)
    {
        return User::join('student_practice_test_statuses', 'users.id',
            'student_practice_test_statuses.user_id')->join('practice_test_details',
            'student_practice_test_statuses.practice_test_detail_id', 'practice_test_details.id')
            ->join('user_licenses', function ($join) {
                $join->on('users.id', 'user_licenses.user_id')
                    ->where('user_licenses.is_associated', config('constants.NO'));
            })->join('licenses', function ($join) {
                $join->on('user_licenses.license_id', 'licenses.id')
                ->where('licenses.role', config('constants.STUDENT'));
            })->where('practice_test_details.id', $practiceTestDetail->id)->select('users.id',
            'users.first_name', 'users.last_name', 'student_practice_test_statuses.score',
            'student_practice_test_statuses.status', 'practice_test_details.id as practiceTestDetailId',
            'users.email')->groupBy('student_practice_test_statuses.id')->orderBy('users.id');
    }

    /**
     * Get students questions of a practice test
     *
     * @param App\Models\PracticeTestDetail $practiceTestDetail PracticeTestDetail model
     * @param collection $students
     * @return Collection
     */
    public static function getStudentQuestionsOfPracticeTest(PracticeTestDetail $practiceTestDetail, $students)
    {
        $data = new Collection();
        foreach ($students as $student) {
            $data->push(static::studentPracticeTestQuestions($student, $practiceTestDetail->id));
        }
        foreach ($data as $item) {
            $item = PracticeTestService::practiceTestQuestionsWithHints(
                $item, config('constants.END_OF_TEST'), $practiceTestDetail->created_by);
        }
        return $data;
    }

    /**
     * Get students average score of a question of a practice test
     *
     * @param collection $practiceTestQuestions
     * @param integer $studentsCount
     * @return array
     */
    public static function getAverageScoresOfPracticeTestQuestions(
        $studentsPracticeTestQuestions,
        $studentsCount
    ) {
        $data = [];
        foreach ($studentsPracticeTestQuestions as $studentPracticeTestQuestions) {
            foreach ($studentPracticeTestQuestions as $key => $studentPracticeTestQuestion) {
                if ($studentPracticeTestQuestion->pivot->is_correct === config('constants.YES')) {
                    $data[$key] = array_key_exists($key, $data) ? $data[$key] + 1 : 1;
                } else {
                    $data[$key] = array_key_exists($key, $data) ? $data[$key] : 0;
                }
            }
        }
        $questionsCount = sizeof($data);
        for ($i = 0; $i < $questionsCount; $i++) {
            $data[$i] = ($data[$i] * 100) / $studentsCount;
        }
        return $data;
    }

    /**
     * Get students accessed question templates of a learning goal test
     *
     * @param integer $learningGoalTestId LearningGoalTest id
     * @return array
     */
    public static function accessedQuestionTemplates($learningGoalTestId)
    {
        $accessedQuestionTemplates = [];
        foreach (static::accessedQuestions($learningGoalTestId) as $key => $accessedQuestion) {
            $accessedQuestionTemplates[$key] = QuestionAnswer::findOrFail($accessedQuestion)
                ->question_template_id;
        }

        return array_unique($accessedQuestionTemplates);
    }

    /**
     * Get students accessed questions of a learning goal test
     *
     * @param integer $learningGoalTestId LearningGoalTest id
     * @return array
     */
    public static function accessedQuestions($learningGoalTestId)
    {
        return LearningGoalTest::select('id')->findOrFail($learningGoalTestId)
            ->questionAnswers()->latest('learning_goal_test_question_answers.id')
            ->limit(config('constants.SKIP_QUESTION_FACTOR'))->select(
            'learning_goal_test_question_answers.question_answer_id')
            ->pluck('learning_goal_test_question_answers.question_answer_id')->toArray();
    }

    /**
     * get number of attempts per test Id
     * @param integer $learningGoalTestId LearningGoalTest id
     * @return array
     */
    public static function learningGoalTestAttempts(
        $learningGoalTestId,
        $currentDate = null
    ) {
        if ($currentDate) {
            $now = $currentDate;
            $last = Carbon::parse($currentDate->toDateString())->subWeek()->addDay();
        } else {
            $now = null;
            $last = null;
        }
        if ($learningGoalTestId) {
            $learningGoalTest = LearningGoalTest::findOrFail($learningGoalTestId);
            $attemptCount = $learningGoalTest->questionAnswers();
            if ($currentDate) {
                $attemptCount = $attemptCount->get();
                $attemptCountCollection = $attemptCount->filter(
                    function ($value) use ($last, $now) {
                        $questionAnsweredDate = $value->pivot->updated_at;
                        return ($questionAnsweredDate->gt(Carbon::parse($last)) && $questionAnsweredDate->lte(Carbon::parse($now)));
                    });
                $count = $attemptCountCollection->count();
            } else {
                $count = $attemptCount->count();
            }
        } else {
            $count = 0;
        }
        return $count;
    }

    /**
     * Get student learning goal ids
     *
     * @param integer $licenseId License id
     * @param integer $userId User id
     * @param integer $subjectId Subject id
     * @return array
     */
    public static function studentLearningGoalIds($licenseId, $userId, $subjectId)
    {
        return LearningGoal::select('id')->whereIn('id', DB::Table(
            'student_learning_goals')->where('license_id', $licenseId)
                ->where('user_id', $userId)->where('subject_id', $subjectId)
                ->pluck('learning_goal_id')->toArray())->get();
    }
}
