<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Mollie\Api\MollieApiClient;

class DeactivateUserMollieSubsctriptionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * mollie subscription
     *
     * @var App\Models\Subscription
     */
    public $mollieSubscription;

    /**
     * mollie payment
     *
     */
    public $mollie;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($mollieSubscription)
    {
        $this->mollieSubscription = $mollieSubscription;
        $this->mollie = new MollieApiClient();
        $this->mollie->setApiKey(config('app.mollie_api_key'));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
