<?php

namespace App\Events;

use App\Models\Subject;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignLearningGoalToSubjectEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Subject model
     *
     * @var App\Models\Subject
     */
    public $subject;

    /**
     * Learning goal ids
     *
     * @var array
     */
    public $learningGoalIds;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Subject $subject, $learningGoalIds)
    {
        $this->subject = $subject;
        $this->learningGoalIds = $learningGoalIds;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
