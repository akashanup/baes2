<?php

namespace App\Events;

use App\Models\License;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignActivePracticeTestsToUserEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User model
     *
     * @var App\Models\User
     */
    public $user;

    /**
     * License model
     *
     * @var App\Models\License
     */
    public $license;

    /**
     * Grade id
     *
     * @var integer
     */
    public $gradeId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, License $license, $gradeId)
    {
        $this->user = $user;
        $this->license = $license;
        $this->gradeId = $gradeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
