<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreateLicenseDefaultFolderEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User Id
     *
     */
    public $userId;

    /**
     * License Id
     *
     */
    public $licenseId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $licenseId)
    {
        $this->userId = $userId;
        $this->licenseId = $licenseId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
