<?php

namespace App\Events;

use App\Models\LearningGoal;
use App\Models\Subject;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignSubjectToLearningGoalEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * LearningGoal model
     *
     * @var App\Models\Subject
     */
    public $learningGoal;

    /**
     * Curriculum ID's
     *
     * @var array
     */
    public $curriculumIds;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(LearningGoal $learningGoal, $curriculumIds)
    {
        $this->learningGoal = $learningGoal;
        $this->curriculumIds = $curriculumIds;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
