<?php

namespace App\Events;

use App\Models\Course;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignInstitutionToCourseEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Course model
     *
     * @var App\Models\Course
     */
    public $course;

    /**
     * Institution ids
     *
     * @var array
     */
    public $institutionIds;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Course $course, $institutionIds)
    {
        $this->course = $course;
        $this->institutionIds = $institutionIds;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
