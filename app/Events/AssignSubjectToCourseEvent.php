<?php

namespace App\Events;

use App\Models\Course;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignSubjectToCourseEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Course model
     *
     * @var App\Models\Course
     */
    public $course;

    /**
     * Subject data
     *
     * @var array
     */
    public $subjects;

    /**
     * Type of request create/update
     *
     * @var string
     */
    public $type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Course $course, $subjects, $type)
    {
        $this->course = $course;
        $this->subjects = $subjects;
        $this->type = $type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
