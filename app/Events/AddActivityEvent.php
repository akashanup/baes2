<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AddActivityEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User
     *
     * @var App\Models\User
     */
    public $user;

    /**
     * License id
     *
     * @var array
     */
    public $licenseId;

    /**
     * Subject id
     *
     * @var array
     */
    public $subjectId;

    /**
     * LearningGoal id
     *
     * @var array
     */
    public $learningGoalId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $licenseId, $subjectId, $learningGoalId = null)
    {
        $this->user = $user;
        $this->licenseId = $licenseId;
        $this->subjectId = $subjectId;
        $this->learningGoalId = $learningGoalId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
