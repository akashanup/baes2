<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignQuestionAnswerToLearningGoalTestEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Learning goal test id
     *
     * @var integer
     */
    public $learningGoalTestId;

    /**
     * Question Answer id
     *
     * @var integer
     */
    public $questionAnswerId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($learningGoalTestId, $questionAnswerId)
    {
        $this->learningGoalTestId = $learningGoalTestId;
        $this->questionAnswerId = $questionAnswerId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
