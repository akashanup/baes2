<?php

namespace App\Events;

use App\Models\License;
use App\Models\PracticeTestDetail;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignPracticeTestToStudentsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * PracticeTestDetail model
     *
     * @var App\Models\PracticeTestDetail
     */
    public $practiceTestDetail;

    /**
     * License model
     *
     * @var App\Models\License
     */
    public $license;

    /**
     * Grade id
     *
     * @var integer
     */
    public $gradeId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PracticeTestDetail $practiceTestDetail, License $license, $gradeId)
    {
        $this->practiceTestDetail = $practiceTestDetail;
        $this->license = $license;
        $this->gradeId = $gradeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
