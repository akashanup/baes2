<?php

namespace App\Events;

use App\Models\LearningGoal;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreateLearningGoalTestEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Student LearningGoal
     *
     * @var studentLearningGoal
     */
    public $studentLearningGoal;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($studentLearningGoal)
    {
        $this->studentLearningGoal = $studentLearningGoal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
