<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssignLearningGoalToUserEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User model
     *
     * @var App\Models\User
     */
    public $user;

    /**
     * License model
     *
     * @var App\Models\License
     */
    public $license;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $license)
    {
        $this->user = $user;
        $this->license = $license;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
