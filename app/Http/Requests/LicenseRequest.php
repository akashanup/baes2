<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LicenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'nullable|string|max:25',
            'course_id' => 'required|integer',
            'institution_id' => 'required|integer',
            'validity' => 'nullable|integer',
            'price' => 'required|integer',
            'end_date' => 'nullable|date',
            'type' => ['required',
                Rule::in([config('constants.MONTHLY_PAYMENT'),
                    config('constants.FIXED_PAYMENT')]),
            ],
            'status' => ['required',
                Rule::in([config('constants.ACTIVE'),
                    config('constants.INACTIVE')]),
            ],
            'role' => ['required',
                Rule::in([config('constants.STUDENT'),
                    config('constants.TEACHER')]),
            ],
        ];
    }
}
