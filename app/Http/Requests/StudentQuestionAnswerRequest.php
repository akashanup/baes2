<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentQuestionAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'license' => 'required|integer',
            'subjectId' => 'required|integer',
            'subjectSlug' => 'required|string',
            'learningGoalId' => 'required|integer',
            'learningGoalTestId' => 'nullable|integer',
            'questionId' => 'required|integer',
            'answer' => 'required|string',
            'type' => ['required',
                Rule::in([config('constants.MCQ_TYPE'),
                    config('constants.DIRECT_TYPE')]),
            ],
            'attempt' => ['required', Rule::in([1, 2, 3, 4])],
            'introductionQuestion' => ['nullable', Rule::in(
                [config('constants.YES')])],
        ];
    }
}
