<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InstitutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'image' => 'nullable|mimes:jpg,jpeg,png,gif',
            'type' => ['required',
                Rule::in([config('constants.SCHOOL'), config('constants.ORGANISATION')]),
            ],
            'removeLogo' => 'nullable',
        ];
    }
}
