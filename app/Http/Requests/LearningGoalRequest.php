<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LearningGoalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|string|max:100',
            'theory' => 'required|string',
            'example' => 'required|string',
            'video1' => 'required|string|max:255',
            'video2' => 'required|string|max:255',
            'video_text1' => 'required|string|max:255',
            'video_text2' => 'required|string|max:255',
            'curriculum_id' => 'required|array',
        ];
    }
}
