<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentLearningGoalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'license' => 'required|integer',
            'subjectId' => 'required|integer',
            'learningGoalId' => 'required|integer',
            'status' => ['required',
                Rule::in([config('constants.UNAVAILABLE'), config('constants.AVAILABLE'),
                    config('constants.STARTED'), config('constants.FINISHED')]),
            ],
            'testId' => 'nullable|integer',
        ];
    }
}
