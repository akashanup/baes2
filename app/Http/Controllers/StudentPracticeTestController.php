<?php

namespace App\Http\Controllers;

use App\Events\AddActivityEvent;
use App\Http\Requests\PracticeTestAnswerRequest;
use App\Http\Requests\StudentPracticeTestDetailRequest;
use App\Models\License;
use App\Models\PracticeTestDetail;
use App\Models\Subject;
use App\Repositories\UserRepository;
use App\Services\PracticeTestService;
use DB;
use Lang;

class StudentPracticeTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param App\Models\License $license License model
     * @param string $subjectSlug Subject slug
     * @return \Illuminate\Http\Response
     */
    public function index(License $license, $subjectSlug)
    {
        $practiceTests = UserRepository::studentPracticeTests(
            auth()->user(), $license)->get();
        if ($subject = Subject::findBySlug($subjectSlug)->first()) {
            $practiceTests = PracticeTestService::subjectPracticeTests(
                $practiceTests, $subject);
        }
        return view('users.students.practice-tests.index')->with([
            'schoolPracticeTests' => PracticeTestService::practiceTestsByRole(
                $practiceTests, config('constants.TEACHER'))->toArray(),
            'studentPracticeTests' => PracticeTestService::practiceTestsByRole(
                $practiceTests, config('constants.STUDENT'))->toArray(),
            'licenseId' => $license->id,
        ]);
    }

    /**
     * Display a resource.
     *
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @param integer $questionAnswerId QuestionAnswer id
     * @return \Illuminate\Http\Response
     */
    public function test(
        License $license,
        $practiceTestSlug,
        $questionAnswerId = null
    ) {
        $studentPracticeTest = request()->studentPracticeTest;
        if ($studentPracticeTest->pivot->status === config('constants.AVAILABLE')) {
            DB::beginTransaction();
            $studentPracticeTest = UserRepository::updateStudentPracticeTestStatusToStart(
                $studentPracticeTest);
            // Update student_attempted
            $practiceTestDetail = PracticeTestDetail::findOrFail(
                $studentPracticeTest->pivot->practice_test_detail_id);
            $practiceTestDetail->update(['student_attempted' => (
                $practiceTestDetail->student_attempted + 1)]);
            DB::commit();
        }
        $questionAnswers = request()->questionAnswers;
        $questionAnswer = null;
        if ($questionAnswerId && $questionAnswers->contains(
            function ($questionAnswer) use ($questionAnswerId) {
                return $questionAnswer->pivot->question_answer_id == $questionAnswerId;
            })
        ) {
            $questionAnswer = $questionAnswers->filter(function ($questionAnswer) use ($questionAnswerId) {
                return $questionAnswer->pivot->question_answer_id == $questionAnswerId;
            })->first();
        } else {
            $questionAnswer = $questionAnswers->first();
        }
        event(new AddActivityEvent(auth()->user(), $license->id, $questionAnswer->subjectId,
            $questionAnswer->learningGoalId));
        PracticeTestService::studentPracticeTestCookie($studentPracticeTest);
        return view('users.students.practice-tests.test')->with([
            'licenseId' => $license->id,
            'practiceTestSlug' => $practiceTestSlug,
            'practiceTestQuestion' => $questionAnswer->toArray(),
            'answeredQuestions' => $questionAnswers->filter(function ($questionAnswer) {
                return $questionAnswer->pivot->answer;
            })->count(),
            'practiceTestQuestionsCount' => $questionAnswers->count(),
            'practiceTestQuestions' => $questionAnswers,
            'studentPracticeTestTime' => $studentPracticeTest->time,
        ]);
    }

    /**
     * Store answer of the student practice test.
     *
     * @param App\Http\Requests\PracticeTestAnswerRequest $request
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAnswer(
        PracticeTestAnswerRequest $request,
        License $license,
        $practiceTestSlug
    ) {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $studentPracticeTest = request()->studentPracticeTest;
        $questionAnswerId = $validatedData['question_answer_id'];
        UserRepository::storeStudentPracticeTestAnswer(auth()->user(),
            $studentPracticeTest->pivot->practice_test_detail_id,
            $questionAnswerId, $validatedData['answer']);
        DB::commit();
        $questionAnswers = request()->questionAnswers;
        $currentQuestion = $questionAnswers->filter(function ($questionAnswer) use ($questionAnswerId) {
            return $questionAnswer->pivot->question_answer_id == $questionAnswerId;
        })->first();
        $nextQuestion = $questionAnswers->filter(function ($questionAnswer) use ($currentQuestion) {
            return $questionAnswer->pivot->id > $currentQuestion->pivot->id;
        })->first();
        if (!$nextQuestion) {
            $nextQuestion = $questionAnswers->first();
        }
        return response()->json(['route' => route('students.practiceTests.test',
            [$license->id, $practiceTestSlug, $nextQuestion->pivot->question_answer_id])]);
    }

    /**
     * Submit student practice test.
     *
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(License $license, $practiceTestSlug)
    {
        DB::beginTransaction();
        $studentPracticeTest = request()->studentPracticeTest;
        UserRepository::finishStudentPracticeTest(auth()->user(),
            $studentPracticeTest->pivot->practice_test_detail_id);
        PracticeTestDetail::updatePracticeTestDetailScore(
            $studentPracticeTest->pivot->practice_test_detail_id);
        DB::commit();
        $response = null;
        if ($studentPracticeTest->feedback_type === config('constants.CLOSED')) {
            session()->flash('status', Lang::get('students.closed_feedback'));
            $response = ['route' => route('students.practiceTests.index',
                [$license->id, config('constants.ALL')])];
        } else {
            $response = ['route' => route('students.practiceTests.result',
                [$license->id, $practiceTestSlug])];
        }
        return response()->json($response);
    }

    /**
     * Show result of the student practice test.
     *
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @return \Illuminate\Http\Response
     */
    public function result(License $license, $practiceTestSlug)
    {
        $studentPracticeTest = request()->studentPracticeTest;
        if ($studentPracticeTest->pivot->status === config('constants.FINISHED')) {
            if ($studentPracticeTest->feedback_type === config('constants.CLOSED')) {
                session()->flash('status', Lang::get('students.closed_feedback'));
                $response = redirect()->route('students.practiceTests.index',
                    [$license->id, config('constants.ALL')]);
            } else {
                $questionAnswers = request()->questionAnswers;
                $questionAnswersCount = $questionAnswers->count();
                $questionAnswers = PracticeTestService::practiceTestQuestionsWithHints(
                    $questionAnswers, $studentPracticeTest->feedback_type,
                    $studentPracticeTest->created_by);
                $response = view('users.students.practice-tests.result')->with([
                    'licenseId' => $license->id,
                    'practiceTestSlug' => $practiceTestSlug,
                    'practiceTestQuestions' => $questionAnswers,
                    'studentPracticeTest' => $studentPracticeTest,
                    'practiceTestQuestionsCount' => $questionAnswers->count(),
                    'correctAnswersCount' => $questionAnswers->filter(function ($questionAnswer) {
                        return $questionAnswer->pivot->is_correct === config('constants.YES');
                    })->count(),
                ]);
            }
        } else {
            $response = redirect()->route('students.practiceTests.index',
                [$license->id, config('constants.ALL')])->withErrors(
                Lang::get('globals.unauthorized_text'));
        }
        return $response;
    }

    /**
     * Update the status of the student practice test.
     *
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(License $license, $practiceTestSlug)
    {
        DB::beginTransaction();
        $studentPracticeTest = request()->studentPracticeTest;
        UserRepository::updateStudentPracticeTestStatusToStart($studentPracticeTest);
        // Update student_attempted
        $practiceTestDetail = PracticeTestDetail::findOrFail(
            $studentPracticeTest->pivot->practice_test_detail_id);
        $practiceTestDetail->update(['student_attempted' => (
            $practiceTestDetail->student_attempted + 1)]);
        DB::commit();
        return response()->json([]);
    }

    /**
     * Create student practice test.
     *
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(License $license, $courseSlug)
    {
        return view('users.students.practice-tests.create')->with([
            'subjects' => $license->course->subjects,
            'licenseId' => $license->id,
            'courseSlug' => $courseSlug,
        ]);
    }

    /**
     * Store student practice test.
     *
     * @param App\Http\Requests\StudentPracticeTestDetailRequest $request
     * @param App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestSlug slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        StudentPracticeTestDetailRequest $request,
        License $license,
        $courseSlug
    ) {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $user = auth()->user();
        $userLicense = $user->licenses()->where('licenses.id', $license->id)
            ->addSelect('licenses.course_id', 'licenses.institution_id')->first();
        $practiceTestDetail = PracticeTestDetail::saveStudentTest(
            $validatedData, $userLicense->pivot->grade_id, $user->id,
            $userLicense->course_id, $userLicense->institution_id);
        // Add questions for practice test detail
        $addQuestionAnswers = $practiceTestDetail->addQuestionAnswers();
        if ($addQuestionAnswers['response']) {
            // Add practice test to the students of the same grade as of practice test.
            $user->addPracticeTestStatuses($practiceTestDetail);
            $user->addPracticeTestQuestions($practiceTestDetail);
            DB::commit();
            session()->flash('status', Lang::get('teachers.test_created'));
            $response = ['response' => true,
                'route' => route('students.practiceTests.index',
                    [$license->id, config('constants.ALL')])];
        } else {
            DB::rollback();
            $response = $addQuestionAnswers;
        };
        return response()->json($response);
    }
}
