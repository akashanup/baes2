<?php

namespace App\Http\Controllers;

use App\Events\AssignPracticeTestToStudentsEvent;
use App\Http\Requests\PracticeTestFolderRequest;
use App\Models\Course;
use App\Models\Grade;
use App\Models\License;
use App\Models\PracticeTestDetail;
use App\Models\PracticeTestFolder;
use App\Models\PracticeTestTemplate;
use App\Models\User;
use App\Services\DashboardService;
use App\Services\YajraDataTableService;
use Auth;
use DB;
use Illuminate\Http\Request;
use Lang;

class PracticeTestController extends Controller
{
    /**
     * Manage teacher's practice tests
     *
     * @return \Illuminate\Http\Response
     */

    public function managePracticeTest(License $license, $slug)
    {
        $grades = $license->institution->grades;
        $institution = $license->institution;
        $course = Course::findBySlug($slug)->first();
        $folders = PracticeTestFolder::
            getInstitutionCourseFolders($institution->id, $course->id);
        return view('practice-tests.practice-test-management')->with([
            'licenseId' => $license->id,
            'course' => $slug,
            'folders' => $folders,
            'grades' => $grades,
            'saveTestDetailsRoute' => route('teachers.saveTestDetails'),
        ]);
    }

    /**
     * List all practice test template
     *
     * @return \Illuminate\Http\Response
     */
    public function listPracticeTestTemplate(
        License $license,
        $courseSlug,
        $folderId
    ) {
        return YajraDataTableService::practiceTestTemplates
            ($license->id, $courseSlug, $license->course_id,
            $license->institution_id, $folderId);
    }

    /**
     * List all active practice test
     *
     * @return \Illuminate\Http\Response
     */
    public function listActivePracticeTest(
        License $license,
        $folderId,
        $gradeId = null
    ) {
        return YajraDataTableService::
            activePracticeTest($license->id, $license->course_id,
            $license->institution_id, $folderId, $gradeId);
    }
    /**
     * Create practicetest folder
     *
     * @return \Illuminate\Http\Response
     */
    public function createFolder(
        PracticeTestFolderRequest $request,
        License $license,
        $courseSlug
    ) {
        DB::beginTransaction();
        $institution = $license->institution;
        $course = Course::findBySlug($courseSlug)->first();
        $data = [
            'name' => $request->folder_name,
            'description' => $request->folder_description,
            'license_id' => $license->id,
            'user_id' => Auth::user()->id,
            'institution_id' => $institution->id,
            'course_id' => $course->id,
        ];
        PracticeTestFolder::saveFolder($data);
        session()->flash('status', Lang::get('teachers.folder_created'));
        DB::commit();
        return redirect()->back();
    }

    /**
     * Save test detials and questions
     * @return \Illuminate\Http\Response
     */
    public function saveTestDetails(Request $request)
    {
        DB::beginTransaction();
        $this->validate(request(), [
            'grade' => 'required|numeric',
            'start' => 'required',
            'end' => 'required',
            'time' => 'required|numeric',
            'feedback' => 'required',
            'gmt' => 'required',
            'availability' => 'required|string',
        ]);

        $practiceTestTemplate = PracticeTestTemplate::
            find($request->testTemplateId);
        $license = License::find($request->licenseId);
        $data = [
            'grade_id' => $request->grade,
            'start_date' => $request->start,
            'end_date' => $request->end,
            'time' => $request->time,
            'feedback_type' => $request->feedback,
            'gmt' => $request->gmt,
            'template' => $practiceTestTemplate->template,
            'practice_test_template_id' => $practiceTestTemplate->id,
            'name' => $practiceTestTemplate->name,
            'user_id' => Auth::user()->id,
            'status' => config('constants.STARTED'),
            'slug' => PracticeTestDetail::createSlug($practiceTestTemplate->name),
            'availability' => $request->availability,
            'folder_id' => $request->folderId,
            'course_id' => $license->course_id,
            'institution_id' => $license->institution_id,
        ];
        $practiceTestDetail = PracticeTestDetail::saveTestDetails($data);
        // Add questions for practice test detail
        $addQuestionAnswers = $practiceTestDetail->addQuestionAnswers();
        if ($addQuestionAnswers['response']) {
            // Add practice test to the students of the same grade as of practice test.
            event(new AssignPracticeTestToStudentsEvent($practiceTestDetail, $license,
                $request->grade));
            DB::commit();
            session()->flash('status', Lang::get('teachers.test_created'));
            $response = config('constants.SUCCESS');
        } else {
            DB::rollback();
            $response = $addQuestionAnswers;
        };
        return response()->json($response);
    }

    /**
     * List practic tests and tst templates by folder
     * @return \Illuminate\Http\Response
     */

    public function folderPracticeTests(
        License $license,
        $courseSlug,
        $folderSlug,
        $gradeSlug = null
    ) {
        $folder = PracticeTestFolder::findBySlug($folderSlug)->first();
        if ($gradeSlug) {
            $gradeId = Grade::findBySlug($gradeSlug)->first()->id;
            $activetestRoute = route('teachers.listActivePracticeTest',
                [$license->id, $folder->id, $gradeId]);
        } else {
            $activetestRoute = route('teachers.listActivePracticeTest',
                [$license->id, $folder->id]);
        }
        return view('practice-tests.folder-tests-list')->with([
            'listActiveTestRoute' => $activetestRoute,
            'folder' => $folder,
            'allFolders' => $license->practiceTestFolders,
            'licenseId' => $license->id,
            'course' => $courseSlug,
            'listpracticeTest' => route('teachers.listPracticeTestTemplate',
                [$license->id, $courseSlug, $folder->id]),
            'grades' => $license->institution->grades,
            'saveTestDetailsRoute' => route('teachers.saveTestDetails'),
            'changeTestStatusRoute' => route('teachers.changeStatus'),
            'updateTestTemplateRoute' => route('teachers.updateTestTemplateFolder'),
            'folderTestRoute' => route('users.folder-practice-test',
                [$license->id, $courseSlug, $folderSlug]),
            'gradeSlug' => $gradeSlug,
            'practiceTestRoute' => route('users.folder-practice-test',
                [$license->id, $courseSlug, $folderSlug]),
        ]);
    }

    public function changeTestStatus(Request $request)
    {
        $this->validate(
            request(),
            ['id' => 'required|numeric']
        );
        $practiceTestDetail = PracticeTestDetail::find($request->id);
        $changeStatus = $practiceTestDetail->finishPracticeTest();
        session()->flash('status', Lang::get('teachers.status_changed'));
        $response = config('constants.SUCCESS');
        DB::commit();
        return response()->json($response);
    }

    /**
     * List all the finished practice tests
     * @return \Illuminate\Http\Response
     */
    public function practiceTestFinished(License $license, $courseSlug)
    {
        $grades = Grade::getInstitutionGrades($license->institution_id);
        $className = $grades->pluck('name')->toArray();
        $classId = $grades->pluck('id')->toArray();
        $classes = array_combine($classId, $className);
        $years = $grades->pluck('year')->toArray();

        return view('practice-tests.practice-test-finished')->with([
            'classes' => $classes,
            'years' => $years,
            'licenseId' => $license->id,
            'course' => $courseSlug,
            'dashboard' => DashboardService::dashboardRoute(),
            'finishedTestTableRoute' => route(
                'teachers.finishedPracticeTestTable', [$license->id, $courseSlug]),
        ]);
    }

    /**
     * Data table for all finish practice tests
     * @param  $licenseId
     * @return \Illuminate\Http\Response
     */
    public function listFinishedTestTable(
        License $license,
        $year = null,
        $grade = null
    ) {
        return YajraDataTableService::
            finishedPracticeTest($license->id, $license->course_id,
            $license->institution_id, $grade, $year);
    }

    /**
     * Update practicetest folder
     * @param App\Models\License $license
     * @param string $course
     * @param integer $folderId
     * @return \Illuminate\Http\Response
     */
    public function updateFolder(
        PracticeTestFolderRequest $request,
        License $license,
        $course,
        $folderId
    ) {
        DB::beginTransaction();
        PracticeTestFolder::updateFolder(
            $request->folder_name, $request->folder_description,
            $folderId);
        session()->flash('status', Lang::get('teachers.folder_updated'));
        $response = config('constants.SUCCESS');
        DB::commit();
        return response()->json($response);
    }

    /**
     * delete practicetest folder
     * @param App\Models\License $license
     * @param string $course
     * @param integer $folderId
     * @return \Illuminate\Http\Response
     */
    public function deleteFolder(
        Request $request,
        License $license,
        $course,
        $folderId
    ) {
        DB::beginTransaction();
        $practiceTestFolder = practiceTestFolder::find($folderId);
        if ($practiceTestFolder) {
            $practiceTestFolder->delete();
            session()->flash('status', Lang::get('teachers.folder_deleted'));
            $response = ['status' => config('constants.SUCCESS')];
        } else {
            $response = ['status' => config('constants.FAILED'),
                'message' => Lang::get('teachers.unable_to_delete')];
        }
        DB::commit();
        return response()->json($response);
    }
}
