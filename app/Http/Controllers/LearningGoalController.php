<?php

namespace App\Http\Controllers;

use App\Events\AssignLearningGoalToSubjectEvent;
use App\Events\AssignSubjectToLearningGoalEvent;
use App\Events\UpdateStudentLearningGoalEvent;
use App\Http\Requests\LearningGoalRequest;
use App\Models\Course;
use App\Models\Grade;
use App\Models\LearningGoal;
use App\Models\License;
use App\Models\Subject;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\LearningGoalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Lang;
use PDF;

class LearningGoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {
        if (!$slug) {
            $learningGoalRoute = route('learning-goals.indexData');
        } else {
            $learningGoalRoute = route('learning-goals.indexData', $slug);
        }
        return view('learning-goals.index')->with([
            'learningGoalRoute' => $learningGoalRoute,
            'curriculumDataRoute' => route('learning-goals.curriculumData', 'learninggoal'),
            'learningCurriculumRoute' => route('learning-goals.curriculum.index'),
            'currentRoute' => route('learning-goals.index'),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('learning-goals.create')->with([
            'curriculumList' => Subject::getAll(config('constants.CURRICULUM'))
                ->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LearningGoalRequest $request)
    {
        $validatedData = $request->validated();
        DB::beginTransaction();

        event(new AssignSubjectToLearningGoalEvent(
            LearningGoal::saveLearingGoal($validatedData),
            $validatedData['curriculum_id'])
        );
        DB::commit();
        session()->flash('status', Lang::get('admins.goal_added'));
        return redirect()->route('learning-goals.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $learningGoal = LearningGoal::findBySlug($slug)->first();
        return view('learning-goals.edit')->with([
            'curriculumList' => Subject::getAll(config('constants.CURRICULUM'))
                ->get(),
            'LearningGoalDetails' => $learningGoal,
            'existingCurriculumIds' => $learningGoal->subjects->pluck('id')
                ->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LearningGoalRequest $request, $slug)
    {
        $validatedData = $request->validated();
        DB::beginTransaction();
        event(new AssignSubjectToLearningGoalEvent(
            LearningGoal::updateLearingGoal($validatedData, $slug),
            $validatedData['curriculum_id'])
        );
        DB::commit();
        session()->flash('status', Lang::get(
            'admins.learninggoal_updated'));
        return redirect()->route('learning-goals.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        DB::beginTransaction();
        $learningGoal = LearningGoal::findBySlug($slug)->first();
        $subjects = $learningGoal->subjects;
        $learningGoal->delete();
        foreach ($subjects as $subject) {
            event(new AssignLearningGoalToSubjectEvent(
                $subject, $subject->learningGoals()->select('id')->orderBy('subject_learning_goals.order')->pluck('id')));
            if ($subject->type === Subject::SUBJECT) {
                event(new UpdateStudentLearningGoalEvent($subject));
            }
        }
        DB::commit();
        session()->flash('status', Lang::get('admins.learninggoal_deleted'));
        return redirect()->route('learning-goals.index');
    }
    /**
     * Get the learning goals list for datatable
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function indexData($slug = null)
    {
        return LearningGoalService::learningGoals($slug);
    }

    /**
     * Get the learning goal's list for datatable
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function curriculumData($requestType, $type = null)
    {
        if ($requestType === 'learninggoal') {
            $curriculumView = view('learning-goals.curriculum-dropdown')->with([
                'DefaultRoute' => route('learning-goals.indexData'),
                'CurriculumData' => Subject::getAll(config('constants.CURRICULUM'))->get(),
            ]);
        } elseif ($requestType === 'template') {
            $curriculumView = view('templates.template-curriculum-filter')->with([
                'CurriculumData' => Subject::getAll(
                    config('constants.CURRICULUM'))->get(),
                'allLearningGoals' => LearningGoal::getAll()->get(),
                'type' => $type,
            ]);
        }
        return $curriculumView;
    }
    /**
     * export learning goal theory PDF
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function exportTheory(License $license, $couseSlug, $learningGoalSlug)
    {
        $theory_url = LearningGoal::findBySlug
        ($learningGoalSlug)->first()->theory_url;
        if (Storage::exists($theory_url)) {
            $response = response(Storage::get($theory_url), 200, [
                'Content-Type' => '.pdf',
                'Content-Length' => Storage::size($theory_url),
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename={$theory_url}",
                'Content-Transfer-Encoding' => 'binary',
            ]);
            return $response;
        }
        return redirect()->back()
            ->withErrors(Lang::get('admins.file_not_found'));
    }

    /**
     * export learning goal theory PDF
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function theoryPreview($slug)
    {
        $learningGoal = LearningGoal::findBySlug($slug)->first();
        return view('learning-goals.export-theory')->with([
            'learningGoal' => $learningGoal,
        ]);
    }

    /**
     * export learning goal theory PDF
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function uploadTheoryPdf(Request $request, $slug)
    {
        $this->validate($request, [
            'uploadTheoryPdfForm' => 'mimes:pdf',
        ]
        );
        DB::beginTransaction();
        $learningGoal = LearningGoal::
            findBySlug($slug)->first();

        $s3 = Storage::disk('s3');
        $theoryPdf = $request->file('uploadTheoryPdfForm');

        $imageFileName = 'images/' . $slug . '-' . time() . '.' . $theoryPdf->getClientOriginalExtension();

        $s3->put($imageFileName, file_get_contents($theoryPdf), 'public');
        $fileLink = Storage::url($imageFileName);
        $learningGoal->update(['theory_url' => $imageFileName]);
        DB::commit();
        session()->flash('status',
            Lang::get('admins.upload_theory_pdf_status'));
        return redirect()->route('learning-goals.index');
    }

    /**
     * export learning goal question Pdf
     * @param  App\Models\License  $license
     * @param  string  $course
     * @param  integer  $learningGoalId
     * @param  integer  $subjectId
     * @param  integer  $userId
     * @return \Illuminate\Http\Response
     */
    public function exportQuestionPdf(
        License $license,
        $course,
        $learningGoalId,
        $subjectId,
        $gradeSlug,
        $userId
    ) {
        $institution = $license->institution;
        $user = User::find($userId);
        $learningGoalName = '';
        $learningGoalOrder = '';
        $learningGoalScore = '';
        $grade = Grade::findBySlug($gradeSlug)->first();
        $courseId = Course::findBySlug($course)->first()->id;
        $studenLicense = UserRepository::getStudentLicense($courseId,
            $institution->id, $userId);
        $learningGoalQuestions = UserRepository::
            getStudentFinishedTestQuestions($userId, $studenLicense->id,
            $subjectId, $learningGoalId);
        foreach ($learningGoalQuestions as $learningQuestions) {
            $learningGoalName = $learningQuestions->name;
            $learningGoalOrder = $learningQuestions->order;
            $learningGoalScore = $learningQuestions->finished_score;
        }

        $pdf = PDF::loadView('templates.download-question-pdf',
            [
                'learningGoalQuestions' => $learningGoalQuestions,
                'user' => $user,
                'grade' => $grade,
                'learningGoalName' => $learningGoalName,
                'learningGoalOrder' => $learningGoalOrder,
                'learningGoalScore' => $learningGoalScore,
            ]
        );
        return $pdf->download($learningGoalName . '.pdf');
    }
}
