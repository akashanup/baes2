<?php

namespace App\Http\Controllers;

use App\Events\AssignLearningGoalToSubjectEvent;
use App\Http\Requests\CurriculumRequest;
use App\Models\LearningGoal;
use App\Models\Subject;
use App\Services\YajraDataTableService;
use DB;
use Illuminate\Http\Request;
use Lang;

class CurriculumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('curriculums.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('curriculums.create')->with([
            'allLearningGoals' =>
            LearningGoal::getAll()->get(),
            'existingLearningGoalIds' => '']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CurriculumRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CurriculumRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        event(new AssignLearningGoalToSubjectEvent(Subject::saveSubject(
            $validatedData, config('constants.CURRICULUM')),
            $validatedData['learningGoalIds'])
        );
        DB::commit();
        session()->flash('status', Lang::get('admins.curriculum_added'));
        return response()->json(['route' => route('curriculums.index')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug Curriculum slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $curriculum = Subject::findBySlug($slug)->first();
        $existingLearningGoals = $curriculum->learningGoals;
        $learningGoals = LearningGoal::all();
        return view('curriculums.edit')->with([
            'curriculum' => $curriculum,
            'allLearningGoals' => $learningGoals->toArray(),
            'existingLearningGoalIds' => $existingLearningGoals
                ->pluck('id')->toArray(),
            'existingLearningGoals' => $existingLearningGoals->pluck('id')->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Requests\CurriculumRequest  $request
     * @param  string  $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CurriculumRequest $request, $slug)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $curriculum = Subject::findBySlug($slug)->first();
        $curriculum->updateSubject($validatedData,
            config('constants.CURRICULUM'));
        event(new AssignLearningGoalToSubjectEvent($curriculum,
            $validatedData['learningGoalIds']));
        DB::commit();
        session()->flash('status', Lang::get('admins.curriculum_updated'));
        return response()->json(['route' => route('curriculums.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($slug)
    {
        DB::beginTransaction();
        Subject::findBySlug($slug)->first()->delete();
        DB::commit();
        session()->flash('status', Lang::get('admins.curriculum_deleted'));
        return response()->json(['route' => route('curriculums.index')]);
    }

    /**
     * Get the curriculum's list for datatable using services : CurriculumService.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function indexData()
    {
        return YajraDataTableService::curriculums();
    }

    /**
     * Get the learning goals of the curriculum
     *
     * @param  string  $curriculum curriculum slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function learningGoals($curriculum)
    {
        return view('includes.subjects-existings-learning-goal',
            ['allLearningGoals' => Subject::findBySlug($curriculum)
                    ->first()->learningGoals->toArray()]
        );
    }
}
