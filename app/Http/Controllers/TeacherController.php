<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentLoginRequest;
use App\Models\LearningGoal;
use App\Models\License;
use App\Models\PracticeTestDetail;
use App\Models\QuestionAnswer;
use App\Repositories\UserRepository;
use App\Services\HintService;
use App\Services\LearningGoalService;
use DB;
use Illuminate\Http\Request;
use Lang;

class TeacherController extends Controller
{

    /**
     * Show all the learning goal questions
     *
     * @return \Illuminate\Http\Response
     */

    public function learningGoalQuestions(License $license, $courseSlug)
    {
        $learingGoals = LearningGoal::getAll()->get();
        $learningGoalId = $learingGoals->pluck('id')->toArray();
        $learningGoalName = $learingGoals->pluck('name')->toArray();
        $learningGoalData = array_combine($learningGoalId, $learningGoalName);
        return view('teachers.all-questions')->with([
            'questionsRoute' => route('teachers.questionsData'),
            'learningGoalData' => $learningGoalData,
            'viewQuestionRoute' => route('teachers.viewQuestion'),
        ]);
    }

    /**
     * Fetch questions data by learnig goals.
     *
     * @param  integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function questionsData($unique, $id = null)
    {
        if ($id) {
            $learingGoalId = [$id];
        } else {
            $learingGoalId = LearningGoal::getAll()->get()->pluck('id')->toArray();
        }
        return LearningGoalService::learningGoalQuestions($unique, $learingGoalId);
    }

    /**
     * Get question answer details by Id
     * @param  integer $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function viewQuestion(Request $request)
    {
        $this->validate(request(), [
            'id' => 'required|integer',
        ]);
        $json = [];
        $question = QuestionAnswer::with('hint')->find(request('id'));
        $hint = $question->hint->hint;
        if ($question) {
            $json['question'] = $question->question;
            $json['options'] = $question->option;
            $json['answer'] = $question->answer;
            if ($hint) {
                $json['hints'] = [
                    HintService::getHint($hint, config('constants.SECOND_ATTEMPT')),
                    HintService::getHint($hint, config('constants.THIRD_ATTEMPT')),
                    HintService::getHint($hint, config('constants.FOURTH_ATTEMPT')),
                ];
            } else {
                $json['hints'] = '';
            }
        } else {
            $json['error'] = Lang::get('admins.question_not_found');
        }
        return response()->json($json);
    }

    /**
     * Goto teacher dashboard
     *
     * @param Illuminate\Http\StudentLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function studentLogin(StudentLoginRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $response = UserRepository::studentLogin(
            auth()->user(), $validatedData['license']);
        DB::commit();
        return response()->json($response);
    }

    /**
     * Goto teacher practice test overview screen
     *
     * @param \App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestDetail slug
     * @return \Illuminate\Http\Response
     */
    public function practiceTestOverview(License $license, $practiceTestSlug)
    {
        $practiceTestDetail = PracticeTestDetail::select('name', 'id', 'avg_score', 'status',
            'created_by', 'feedback_type', 'slug')->findBySlug($practiceTestSlug)->firstOrFail();
        $students = UserRepository::studentsOfPracticeTest($practiceTestDetail)->get();
        $studentsPracticeTestQuestions = UserRepository::getStudentQuestionsOfPracticeTest(
            $practiceTestDetail, $students);
        $averageScores = UserRepository::getAverageScoresOfPracticeTestQuestions(
            $studentsPracticeTestQuestions, $students->count());
        return view('teachers.practice-test-overview')->with([
            'practiceTestDetail' => $practiceTestDetail,
            'students' => $students,
            'studentsPracticeTestQuestions' => $studentsPracticeTestQuestions,
            'averageScores' => $averageScores,
            'questionsCount' => count($studentsPracticeTestQuestions->first()),
            'license' => $license,
        ]);
    }

    /**
     * Update practice test status feedback type
     *
     * @param \App\Models\License $license License model
     * @param string $practiceTestSlug PracticeTestDetail slug
     * @return \Illuminate\Http\Response
     */
    public function practiceTestFeedbackAvailable(License $license, $practiceTestSlug)
    {
        DB::beginTransaction();
        PracticeTestDetail::findBySlug($practiceTestSlug)->firstOrFail()
            ->update(['feedback_type' => config('constants.END_OF_TEST')]);
        DB::commit();
        session()->flash('status', Lang::get('teachers.feedback_available'));
        return redirect()->route('teachers.practiceTests.overview', [$license->id, $practiceTestSlug]);
    }
}
