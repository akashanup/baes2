<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\License;
use App\Models\PracticeTestFolder;
use App\Models\PracticeTestTemplate;
use App\Models\Subject;
use Auth;
use DB;
use Illuminate\Http\Request;
use Lang;

class PracticeTestTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function testTemplate(License $license, $slug, $folderSlug)
    {
        $subjects = $license->course->subjects;
        $institution = $license->institution;
        $course = Course::findBySlug($slug)->first();
        $folders = PracticeTestFolder::
            getInstitutionCourseFolders($institution->id, $course->id);
        return view('practice-tests.practice-test-template')->with([
            'subjects' => $subjects,
            'testManagementRoute' => route('teachers.manageTest',
                [$license->id, $slug]),
            'licenseId' => $license->id,
            'course' => $slug,
            'folders' => $folders,
            'createTestRoute' => route('teachers.savepracticeTestTemplate',
                [$license->id, $slug]),
            'folderSlug' => $folderSlug,
        ]);
    }

    /**
     * Get all learning goals by subject ID
     * @return \Illuminate\Http\Response
     */
    public function subjectlearningGoal(Request $request)
    {
        $this->validate(request(), [
            'subject_id' => 'required|numeric',
        ]);
        $subjects = Subject::find($request->subject_id);
        $learningGoals = $subjects->learningGoals->toArray();
        return response()->json($learningGoals);
    }

    /**
     * Save practice test template data
     * @return \Illuminate\Http\Response
     */
    public function saveTestTemplate(
        Request $request,
        License $license,
        $courseSlug
    ) {
        $request->template_json = json_encode($request->template);
        DB::beginTransaction();
        $this->validate(request(), [
            'test_name' => 'required',
            'number_of_template' => 'required|numeric',
            'template' => 'required',
            'update' => 'required|alpha',
            'template_json' => 'json',
            'folderId' => 'required|numeric',
        ]);
        $data = [
            'name' => $request->test_name,
            'number_of_question' => $request->number_of_template,
            'template' => $request->template_json,
            'user_id' => Auth::user()->id,
            'folder_id' => $request->folderId,
            'course_id' => $license->course_id,
            'institution_id' => $license->institution_id,
        ];
        $selectedFolder = PracticeTestFolder::
            find($request->folderId);
        if ($request->update === config('constants.NO')) {
            // insert record in practice test template
            $testTemplate = PracticeTestTemplate::saveTestTemplate($data);
            if ($testTemplate) {
                $response = ['status' => config('constants.SUCCESS'),
                    'route' => route('users.folder-practice-test',
                        [$license->id, $courseSlug, $selectedFolder->slug]),
                ];
                session()->flash('status',
                    Lang::get('teachers.test_created'));
            } else {
                $response = config('constants.FAILED');
            }
        } elseif ($request->update === config('constants.YES')) {
            $testTemplate = PracticeTestTemplate::find($request->templateId);
            $testTemplate->updateTestTemplate($data, $request->templateId);
            $response = ['status' => config('constants.SUCCESS'),
                'route' => route('users.folder-practice-test',
                    [$license->id, $courseSlug, $selectedFolder->slug]),
            ];
            session()->flash('status',
                Lang::get('teachers.test_updated'));
        }
        DB::commit();
        return response()->json($response);
    }

    /**
     * Delete practice test template by ID
     * @return \Illuminate\Http\Response
     */

    public function deleteTestTemplate(Request $request)
    {
        $this->validate(request(), [
            'testTemplateId' => 'required|numeric',
        ]);
        DB::beginTransaction();
        PracticeTestTemplate::where('id',
            $request->testTemplateId)->delete();
        session()->flash('status', Lang::get('teachers.test_template_deleted'));
        DB::commit();
        return redirect()->back();
    }

    /**
     * Copy the selected Test Template.
     *
     * @param  string  $id PracticeTestTemplate id
     * @return \Illuminate\Http\JsonResponse
     */
    public function copyTemplate(PracticeTestTemplate $practiceTestTemplate)
    {
        DB::beginTransaction();
        $data = [
            'name' => $practiceTestTemplate->name,
            'number_of_question' => $practiceTestTemplate->number_of_question,
            'template' => $practiceTestTemplate->template,
            'user_id' => Auth::user()->id,
            'folder_id' => $practiceTestTemplate->folder_id,
            'course_id' => $practiceTestTemplate->course_id,
            'institution_id' => $practiceTestTemplate->institution_id,
        ];
        $newPracticeTestTemplate = PracticeTestTemplate::
            saveTestTemplate($data);
        session()->flash('status', Lang::get('teachers.test_template_copied'));
        if ($newPracticeTestTemplate) {
            $response = config('constants.SUCCESS');
        } else {
            $response = config('constants.FAILED');
        }
        DB::commit();
        return response()->json($response);
    }

    /**
     * Edit Practice test template.
     * @param  string  $course
     * @param  integer  $licenseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function editTestTemplate(
        License $license,
        $course,
        $folderSlug,
        $templateId
    ) {
        $practiceTestTemplate = PracticeTestTemplate::find($templateId);
        $subjects = $license->course->subjects;
        $institution = $license->institution;
        $course = Course::findBySlug($course)->first();
        $folders = PracticeTestFolder::
            getInstitutionCourseFolders($institution->id, $course->id);
        $templates = array_map("static::mergeLearningGoals",
            json_decode($practiceTestTemplate->template, true));
        return view('practice-tests.edit-practice-test-template')->with([
            'subjects' => $subjects,
            'testManagementRoute' => route('teachers.manageTest',
                [$license->id, $course]),
            'licenseId' => $license->id,
            'folders' => $folders,
            'templates' => $templates,
            'name' => $practiceTestTemplate->name,
            'folderId' => $practiceTestTemplate->folder_id,
            'templateId' => $templateId,
            'createTestRoute' => route('teachers.savepracticeTestTemplate',
                [$license->id, $course]),
            'course' => $course,
            'folderSlug' => $folderSlug,
        ]);
    }

    /**
     * update Practice test template folder
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTestTemplateFolder(Request $request)
    {
        DB::beginTransaction();
        PracticeTestTemplate::changeTestTemplateFolder(
            $request->templateIdArray, $request->folderId);
        session()->flash('status', Lang::get('teachers.folder_changed'));
        $response = config('constants.SUCCESS');
        DB::commit();
        return response()->json($response);
    }

    /**
     * Merge learning goals in template array
     * @param  array  $data
     * @return $data
     */

    public function mergeLearningGoals($data)
    {
        $subjects = Subject::find($data['subject_id']);
        $learningGoals = $subjects->learningGoals->toArray();
        $data['learningGoals'] = $learningGoals;
        return $data;
    }
}
