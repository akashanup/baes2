<?php

namespace App\Http\Controllers;

use App\Events\AssignActivePracticeTestsToUserEvent;
use App\Events\AssignLearningGoalToUserEvent;
use App\Events\AssociateStudentLicenseToTeacherLicenseEvent;
use App\Models\License;
use App\Models\Payment;
use App\Models\Subscription;
use App\Models\User;
use App\Notifications\sendMakePaymentNotification;
use App\Notifications\SuccessfulPaymentNotification;
use App\Services\MolliePayment;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Lang;
use Log;
use Mollie\Api\MollieApiClient;

class PaymentController extends Controller
{

    public $mollie;

    public $paymentData;

    const ONEOFF = 'oneoff';
    const FIRST = 'first';
    const RECURRING = 'recurring';

    public function __construct()
    {
        $this->mollie = new MollieApiClient();
        $this->mollie->setApiKey(config('app.mollie_api_key'));
    }
    // test_DTG2PycWuJWjCk2z8VcxqqUKMKW8Dc
    // live_Nq9JPysJVH2UkG8tuNEFjGQzHJ8Wt5
    /**
     * Shows payment status to user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $payment = Payment::find($id);
        if ($payment) {
            $payment->updatedTime = $payment->updated_at->format('d-m-Y');
            $license = $payment->license;
            $course = $license->course;
            return view('payments.status')->with(
                [
                    'payment' => $payment,
                    'license' => $license,
                    'course' => $course,
                ]
            );
        } else {
            return redirect()->route('users.licenses')->withErrors(
                Lang::get('languages.final_catch_response'));
        }
    }

    /**
     * Initiates payment.
     *
     * @return \Illuminate\Http\Response
     */
    public function makePayment(Request $request)
    {
        $this->validate($request, [
            'licenseId' => 'required|integer',
            'gradeId' => 'integer',
        ]);
        DB::begintransaction();
        $user = Auth::user();
        $currentLicense = License::findOrFail($request->licenseId);
        $molliePayment = new MolliePayment();
        if ($user->role->name === config('constants.USER')) {
            if ($user->checkForSimilarLicenseWithDifferentRole($currentLicense)) {
                $response = redirect()->route('users.licenses')
                    ->withErrors(Lang::get('users.similar_license_found'));
            } else {
                if ($currentLicense->price !== 0) {
                    Log::info("Proceed for payment");
                    if ($currentLicense->type === License::TYPE_FIXED) {
                        // create One time payment
                        Log::info('Start onetime payment');
                        $redirectUrl = $molliePayment->createPayment(
                            $customer = null, $currentLicense,
                            $request->gradeId, config('constants.IDEAL_METHOD'));
                    } elseif ($currentLicense->type === License::TYPE_MONTHLY) {
                        // create recursive payment
                        Log::info('Start Recursive payment');
                        Log::info('Create Customer');
                        $customer = $molliePayment->createCustomer($currentLicense, $request->gradeId);
                        if ($customer) {
                            // Create Recursive payment
                            Log::info("Customer created......:  " . $customer->id);
                            $redirectUrl = $molliePayment->createPayment(
                                $customer, $currentLicense, $request->gradeId, config('constants.IDEAL_METHOD'));
                        } else {
                            // Error in recursive payment
                            Log::info("Error in creating customer");
                            return redirect()->route('users.availableLicenses')
                                ->withErrors(Lang::get('languages.payment_failed'));
                        }
                    }
                    if ($redirectUrl) {
                        $response = redirect($redirectUrl);
                    } else {
                        $response = redirect()->route('users.availableLicenses')
                            ->withErrors(Lang::get('languages.payment_failed2'));
                    }
                } else {
                    Log::info("Licence has no price. Insert User Block Record");
                    $licenseData = [
                        'end_date' => Carbon::now()->addDays(
                            $currentLicense->validity)->format('Y-m-d'),
                        'status' => config('constants.ACTIVE'),
                        'grade_id' => $request->gradeId,
                    ];
                    static::insertUserLicense(
                        $user->id, $currentLicense->id, $licenseData);
                    session()->flash('status', Lang::get('admins.licence_added'));
                    $response = redirect()->route('users.licenses');
                }
            }
        } else {
            $response = redirect()->back()->withErrors(Lang::get('admins.already_owns_license'));
        }
        DB::commit();
        return $response;
    }

    /**
     * Verifies payment and activates user.
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentWebHook()
    {
        DB::begintransaction();
        Log::info('=========================================================');
        $payment = $this->mollie->payments->get(request('id'));
        if ($payment->sequenceType === static::ONEOFF ||
            $payment->sequenceType === static::FIRST) {
            $paymentDetail = Payment::getCurrentPayment(request('id'));
            $user = User::find($paymentDetail->user_id);
            $sendRepaymentLink = true;
        } else {
            $customer = $this->mollie->customers->get($payment->customerId);
            $user = User::find($customer->metadata->userId);
            $sendRepaymentLink = false;
        }
        if ($payment) {
            Log::info("Create payment table data");
            if ($payment->isPaid() && !$payment->hasRefunds()
                && !$payment->hasChargebacks()) {
                Log::info("Payment Status: " . $payment->status);
                if ($payment->sequenceType === static::ONEOFF) {
                    Log::info('=============================================');
                    Log::info("Full oneoff payment");
                    $updateData = [
                        'status' => Payment::SUCCESS,
                    ];
                    $updatePayment = $paymentDetail->updatePayment($updateData);
                    Log::info("Payment Details updated");
                    // Insert User License
                    $licenseData = [
                        'end_date' => Carbon::now()->addDays
                        ($payment->metadata->validity)->format('Y-m-d'),
                        'status' => config('constants.ACTIVE'),
                        'grade_id' => $payment->metadata->grade_id,
                    ];
                    static::insertUserLicense(
                        $payment->metadata->userId,
                        $payment->metadata->licenseId, $licenseData);
                    Log::info("Send Successful payment Notification");
                    Log::info($paymentDetail->created_at . ' and ' . $paymentDetail->id);
                    static::paymentSuccessNotification($paymentDetail, $user);
                    Log::info("send Payment successfull mail");
                } elseif ($payment->sequenceType === static::FIRST) {
                    Log::info('==============================================');
                    Log::info("Subscription 1st payment");
                    $customer = $this->mollie->customers->get(
                        $payment->customerId);
                    $mandate = $customer->getMandate($payment->mandateId);
                    if ($mandate->isvalid() || $mandate->ispending()) {
                        $subscription = MolliePayment::createMollieSubscription(
                            $customer, $payment);
                        $savedSubscription = static::insertSubscriptionRecord(
                            $subscription, $payment->customerId,
                            $payment->mandateId, $payment->metadata->userId);
                        if ($savedSubscription) {
                            Log::info("update payment table with subscription Id");
                            $updateData = ['status' => Payment::SUCCESS,
                                'subscription_id' => $savedSubscription->id,
                                'payment_sequence_type' => $payment->sequenceType,
                            ];
                            $updatePayment = $paymentDetail->updatePayment(
                                $updateData);
                            Log::info("Payment table updated");
                            // Insert User License
                            $licenseEndDate = Carbon::parse($subscription->startDate)->addDays(5)->format('Y-m-d');
                            Log::info($licenseEndDate);
                            $licenseData = [
                                'end_date' => $licenseEndDate,
                                'status' => config('constants.ACTIVE'),
                                'subscription_id' => $savedSubscription->id,
                                'grade_id' => $payment->metadata->grade_id,
                            ];
                            static::insertUserLicense($payment->metadata->userId,
                                $payment->metadata->licenseId, $licenseData);
                            static::paymentSuccessNotification($paymentDetail, $user);
                            Log::info("send Payment successfull mail");
                        } else {
                            Log::info("Unable to insert subscription record");
                        }
                    } else {
                        Log::info("Mandata not found");
                        Log::info("Create 1st payment again");
                    }
                } elseif ($payment->sequenceType === static::RECURRING) {
                    Log::info('==============================================');
                    Log::info("Subscription recurring payment");
                    Log::info("Subscrption ID: " . $payment->subscriptionId);
                    Log::info("Customer ID: " . $payment->customerId);

                    $currentSubscription = Subscription::getCurrentSubscription(
                        $payment->subscriptionId, $payment->customerId);
                    if ($currentSubscription) {
                        Log::info("Current subscription Fetched: " .
                            $currentSubscription->id);
                        $customer = $this->mollie->customers->get(
                            $payment->customerId);
                        $paymentData = [
                            'mollie_payment_unique_id' => $payment->id,
                            'subscription_id' => $currentSubscription->id,
                            'user_id' => $customer->metadata->userId,
                            'status' => Payment::SUCCESS,
                            'payment_sequence_type' => $payment->sequenceType,
                            'license_id' => $customer->metadata->licenseId,
                        ];
                        $savedPayment = Payment::savePayment($paymentData);
                        Log::info("Payment Record Added");
                        Log::info("Update Subscription record");
                        Log::info("Subscription exists");
                        Log::info("Previous subsctiption date: " .
                            $currentSubscription->subscription_payment_date);
                        $subscriptionDate = Carbon::parse($currentSubscription->subscription_payment_date)->addMonth(); //addMonth
                        Log::info($subscriptionDate);
                        $updateSubscriptionData = [
                            'subscription_payment_date' => $subscriptionDate->format('Y-m-d'),
                        ];
                        $updateSubscription = $currentSubscription
                            ->updateSubscription($updateSubscriptionData);
                        Log::info("Subscription Updated");
                        Log::info("Update user License Date");
                        $currentUser = User::find($customer->metadata->userId);
                        $userLicense = $currentUser->licenses()
                            ->where('licenses.id', $customer->metadata->licenseId)
                            ->first();
                        if ($userLicense) {
                            Log::info("User License End Date: " .
                                $userLicense->pivot->end_date);
                            $licenseEndDate = $subscriptionDate->addDays(5)->format('Y-m-d'); //$subscriptionDate + 5 days
                            License::updateUserLicenseStatus(
                                $customer->metadata->licenseId,
                                $customer->metadata->userId,
                                $currentSubscription->id, $licenseEndDate);
                            Log::info("User Licence updated with ENDDATE: " . $licenseEndDate);

                            static::paymentSuccessNotification($savedPayment, $currentUser);
                            Log::info("send Payment successfull mail");

                            Log::info("User Licence updated with ENDDATE: " .
                                $licenseEndDate);
                        } else {
                            Log::info("License Not exist!");
                        }
                    } else {
                        Log::info("Subscription Not exist!");
                    }
                }
            } elseif ($payment->isPending()) {
                $updateData = [
                    'status' => Payment::PENDING,
                    'payment_sequence_type' => $payment->sequenceType,
                ];
                $updatePayment = $paymentDetail->updatePayment($updateData);
                Log::info('Payment status => ' . $payment->status);
            } elseif (!$payment->isOpen()) {
                $updateData = [
                    'status' => Payment::FAILED,
                    'payment_sequence_type' => $payment->sequenceType,
                ];
                $updatePayment = $paymentDetail->updatePayment($updateData);
                Log::info('Payment status => ' . $payment->status);
                // Sending repayment mail to user.
                if ($sendRepaymentLink) {
                    static::rePaymentLink($user, $paymentDetail->license_id);
                    Log::info('Sending repayment mail to => ');
                }
                Log::info('Repayment mail sent to.');
            }
        }
        DB::commit();
    }

    /**
     * Generate payment link for user for the block/package.
     *
     * @return string $url
     */
    public function rePaymentLink($user, $license_id)
    {
        $license = License::find($license_id);
        $rePaymentLink = route('licenses.show', [$license->id, $license->code]);
        $user->notify(new sendMakePaymentNotification
            ($rePaymentLink, $user->first_name));
        Log::info("Repayment link sent");
    }

    /**
     * send success payment email to user
     *
     * @return string $url
     */
    public function paymentSuccessNotification($payment, $user)
    {
        Log::info("In paymentSuccessNotification " . $payment->license_id);
        $license = License::find($payment->license_id);
        $course = $license->course;
        $licenseOverviewLink = route('users.licenses');
        $user->notify(new SuccessfulPaymentNotification($payment,
            $license, $user->first_name, $course, $licenseOverviewLink));
    }

    /**
     * Run event to insert license data.
     * @param integer $userId user_id
     * @param integer $licenseId id
     * @param array $licenseData
     * @return \Illuminate\Http\Response
     */
    private static function insertUserLicense($userId, $licenseId, $licenseData)
    {
        $user = User::find($userId);
        $license = License::find($licenseId);
        $user->attachLicense($license->id, $licenseData);
        if ($license->role === config('constants.STUDENT')) {
            event(new AssignLearningGoalToUserEvent($user, $license));
            event(new AssignActivePracticeTestsToUserEvent($user, $license,
                $licenseData['grade_id']));
        } elseif ($license->role === config('constants.TEACHER')) {
            //Associate corresponding student license
            event(new AssociateStudentLicenseToTeacherLicenseEvent($user, $license->id));
        }
    }

    /**
     * Insert record in payment table.
     * @param integer $userId user_id
     * @param integer $licenseId id
     * @param array $licenseData
     * @return \Illuminate\Http\Response
     */
    public static function insertPaymentRecord($userId, $status)
    {
        $paymentData['user_id'] = $userId;
        $paymentData['status'] = $status;
        return Payment::savePayment($paymentData);
    }

    /**
     * Insert record in subscription table.
     * @param array $subscription
     * @param string $customerId
     * @param string $mandateId
     * @return \Illuminate\Http\Response
     */
    public static function insertSubscriptionRecord(
        $subscription,
        $customerId,
        $mandateId,
        $userId
    ) {
        $subscriptionData = [
            'mollie_subscription_id' => $subscription->id,
            'mollie_customer_unique_id' => $customerId,
            'mandata_id' => $mandateId,
            'user_id' => $userId,
            'susbcription_period' => $subscription->interval,
            'subscription_start_date' => $subscription->startDate,
            'subscription_payment_date' => $subscription->startDate,
            'status' => $subscription->status,
        ];
        return Subscription::saveSubscription($subscriptionData);
    }

    /**
     * cancel licence and subscription for user by licence id.
     * @param App\Models\License $license
     * @param string $mandateId
     * @return \Illuminate\Http\Response
     */
    public function cancelUserLicense(License $license)
    {
        DB::beginTransaction();
        $user = Auth::user();
        $userLicense = $user->licenses()->where('licenses.id', $license->id)
            ->first();
        if ($userLicense->pivot->subscription_id) {
            $currentSubscription = Subscription::find(
                $userLicense->pivot->subscription_id);
            if ($currentSubscription->mollie_subscription_id &&
                $currentSubscription->mollie_customer_unique_id) {
                // cancel mollie subsctiption
                Log::info($currentSubscription->mollie_subscription_id
                    . ' and ' . $currentSubscription->mollie_customer_unique_id);
                $customer = $this->mollie->customers->get(
                    $currentSubscription->mollie_customer_unique_id);
                $subscription = $customer->getSubscription(
                    $currentSubscription->mollie_subscription_id);
                Log::info("Susbscription status " . $subscription->status);
                if ($subscription->status === config('constants.ACTIVE')) {
                    Log::info("in Susbscription status ");
                    $canceledSusbscription = $customer->cancelSubscription(
                        $currentSubscription->mollie_subscription_id);
                    Log::info("Canceled status: " .
                        $canceledSusbscription->status);
                    $currentSubscription->updateSubscription(
                        ['status' => config('constants.INACTIVE')]);
                    License::updateUserLicenseStatus($license->id,
                        $user->id, $currentSubscription->id,
                        $date = null);

                    session()->flash('status',
                        Lang::get('admins.cancel_license'));
                    $response = redirect()->route('users.licenses');
                } else {
                    $response = redirect()->route('users.licenses')
                        ->withErrors(Lang::get('admins.already_inactive'));
                }
            } else {
                $response = redirect()->route('users.licenses')
                    ->withErrors(Lang::get('admins.recurring_license'));
            }
        } else {
            $response = redirect()->route('users.licenses')
                ->withErrors(Lang::get('admins.recurring_license'));
        }
        DB::commit();
        return $response;
    }
}
