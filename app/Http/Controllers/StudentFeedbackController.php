<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentFeedbackNoteRequest;
use App\Http\Requests\StudentFeedbackRequest;
use App\Http\Requests\StudentFeedbackStatusRequest;
use App\Models\Institution;
use App\Models\StudentFeedback;
use App\Repositories\UserRepository;
use App\Services\YajraDataTableService;
use DB;
use Illuminate\Http\Request;
use Lang;

class StudentFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $institution Institution slug
     * @param string $course Course slug
     * @return \Illuminate\Http\Response
     */
    public function index($institution, $course)
    {
        return view('student-feedbacks.index')->with([
            'courseSlug' => $course,
            'institutionSlug' => $institution,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $institution Institution slug
     * @param string $course Course slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData($institution, $course)
    {
        return YajraDataTableService::studentFeedbacks(
            $institution, $course);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StudentFeedbackRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StudentFeedbackRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        UserRepository::addStudentFeedback(auth()->user(),
            $validatedData);
        DB::commit();
        return response()->json(['message' =>
            Lang::get('students.feedback_sent')]);
    }

    /**
     * Update a resource in storage.
     *
     * @param  App\Http\Requests\StudentFeedbackNoteRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeNote(
        StudentFeedbackNoteRequest $request,
        StudentFeedback $studentFeedback
    ) {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $studentFeedback->updateNote($validatedData);
        DB::commit();
        return response()->json(['message' =>
            Lang::get('admins.note_updated')]);
    }

    /**
     * Update a resource in storage.
     *
     * @param  App\Http\Requests\StudentFeedbackStatusRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(
        StudentFeedbackStatusRequest $request,
        StudentFeedback $studentFeedback
    ) {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $studentFeedback->updateStatus($validatedData);
        DB::commit();
        $studentFeedback->sendFeedbackResponse();
        return response()->json(['message' =>
            Lang::get('admins.feedback_updated')]);
    }
}
