<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Grade;
use App\Models\License;
use App\Models\Subject;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\StudentService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Lang;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGradeOverview(
        License $license,
        $course,
        $gradeSlug,
        $subject,
        $viewType,
        $sortOrder = null
    ) {
        $gradeId = null;
        $sortName = $sortOrder;
        $sortScore = $sortOrder;
        $grades = $license->institution->grades;
        $grade = Grade::findBySlug($gradeSlug)->first();
        if ($grade) {
            $gradeId = $grade->id;
        }
        $score = 0;
        $averageClassScore = 0;
        $studentScore = array();
        $lgClassProgess = array();
        $lgBaesScores = array();
        $studentLgBaesScore = array();
        $learningGoalAvgScore = [];
        $totalClassProgress = 0;

        $subjects = $license->course->subjects;
        if ($subject) {
            $activeSubject = Subject::findBySlug($subject)->first();
        } else {
            $activeSubject = $subjects->first();
        }
        $activeSubjectId = $activeSubject->id;
        $activeSubjectSlug = $activeSubject->slug;

        $learningGoals = $activeSubject->learningGoals()
            ->orderBy('subject_learning_goals.order')->get()->toArray();

        $studentLearningGoals = UserRepository::getStudentsForLicenseCourse(
            $license->institution_id, $license->course_id,
            $activeSubjectId, $gradeId);
        $studentsList = $studentLearningGoals->unique('id')->sort();
        foreach ($studentsList as $studentList) {
            $studentScore[$studentList->id] = 0;
            foreach ($learningGoals as $learningGoal) {
                $studentLgBaesScore[$studentList->id]
                [$learningGoal['id']]['score'] = 0;
                $studentLgBaesScore[$studentList->id]
                [$learningGoal['id']]['status'] = config('constants.AVAILABLE');
            }
        }
        $learningGoalArray = $studentLearningGoals->groupBy('learning_goal_id');
        if ($viewType === config('constants.WEEK_VIEW')) {
            $currentDate = Carbon::now();
            $now = Carbon::now();
            $last = Carbon::parse($currentDate->toDateString())->subWeek()->addDay();
            $newColelction = $studentLearningGoals->filter(
                function ($value) use ($last, $now) {
                    return Carbon::parse($value->updated_at)->gt($last) &&
                    Carbon::parse($value->updated_at)->lte($now);
                });
            $studentLearningGoals = $newColelction;
        }

        $students = $studentLearningGoals->groupBy('id')->toArray();
        foreach ($learningGoalArray as $key => $learning_goal_students) {
            $finishedGoalStudent = 0;
            $totalStudent = 0;
            $lgBaesScore = 0;
            foreach ($learning_goal_students as $goal_progress) {
                if ($goal_progress['status'] ===
                    config('constants.FINISHED')) {
                    $finishedGoalStudent++;
                    $lgBaesScore = $lgBaesScore + $goal_progress['current_score'];
                }
                $totalStudent++;
            }
            $lgClassProgess[$key] = ($finishedGoalStudent / $totalStudent) * 100;
            if (!count($lgClassProgess)) {
                $totalClassProgress = 0;
            } else {
                $totalClassProgress = array_sum($lgClassProgess) / count($lgClassProgess);
            }
        }
        foreach ($students as $key => $student) {
            $finishedLearningGoalCount = 0;
            $averageBaesScore = 0;
            foreach ($student as $finishedStudent) {
                if ($finishedStudent['status'] === config('constants.FINISHED')) {
                    $studentLgBaesScore[$key][$finishedStudent['learning_goal_id']]
                    ['score'] = $finishedStudent['finished_score'];
                } else {
                    $studentLgBaesScore[$key][$finishedStudent['learning_goal_id']]
                    ['score'] = $finishedStudent['current_score'];
                }
                $studentLgBaesScore[$key]
                [$finishedStudent['learning_goal_id']]['status'] = $finishedStudent['status'];
            }
        }
        foreach ($studentLgBaesScore as $key => $studentBaesScore) {
            $finishedScoreCount = 0;
            $scoreLgAverage = 0;
            foreach ($learningGoals as $learningGoal) {
                if ($studentBaesScore[$learningGoal['id']]['status'] === config('constants.FINISHED')) {
                    $finishedScoreCount++;
                    $scoreLgAverage = $scoreLgAverage + $studentBaesScore
                        [$learningGoal['id']]['score'];

                    if (isset($learningGoalAvgScore[$learningGoal['id']][config('constants.FINISHED')])) {
                        $learningGoalAvgScore[$learningGoal['id']][config('constants.FINISHED')]++;
                    } else {
                        $learningGoalAvgScore[$learningGoal['id']][config('constants.FINISHED')] = 1;
                    }
                    if (array_key_exists('score',
                        $learningGoalAvgScore[$learningGoal['id']])) {
                        $learningGoalAvgScore[$learningGoal['id']]['score'] = $learningGoalAvgScore[$learningGoal['id']]['score']
                             + $studentBaesScore[$learningGoal['id']]['score'];
                    } else {
                        $learningGoalAvgScore[$learningGoal['id']]['score'] =
                            $studentBaesScore[$learningGoal['id']]['score'];
                    }
                } elseif ($studentBaesScore[$learningGoal['id']]['status']
                    === config('constants.STARTED')) {
                    if (isset($learningGoalAvgScore[$learningGoal['id']][config('constants.STARTED')])) {
                        $learningGoalAvgScore[$learningGoal['id']][config('constants.STARTED')]++;
                    } else {
                        $learningGoalAvgScore[$learningGoal['id']][config('constants.STARTED')] = 1;
                    }
                } elseif ($studentBaesScore[$learningGoal['id']]['status']
                    === config('constants.AVAILABLE')) {
                    if (isset($learningGoalAvgScore[$learningGoal['id']][config('constants.AVAILABLE')])) {
                        $learningGoalAvgScore[$learningGoal['id']][config('constants.AVAILABLE')]++;
                    } else {
                        $learningGoalAvgScore[$learningGoal['id']][config('constants.AVAILABLE')] = 1;
                    }
                }
            }

            if ($finishedScoreCount > 0) {
                $studentScore[$key] = $scoreLgAverage / $finishedScoreCount;
            } else {
                $studentScore[$key] = 0;
            }
        }
        $avgScore = 0;
        $averageFinishCount = 0;
        foreach ($learningGoalAvgScore as $key => $scores) {
            if (array_key_exists(config('constants.FINISHED'), $scores)) {
                $averageFinishCount++;
                $avgScore = $scores['score'] / $scores[config('constants.FINISHED')];
                $averageClassScore = $averageClassScore + $avgScore;
                $scores['score'] = $avgScore;
            } else {
                $scores['score'] = 0;
            }
            $lgBaesScores[$key] = $scores;
        }
        if ($averageFinishCount) {
            $averageClassScore = $averageClassScore / $averageFinishCount;
        }
        foreach ($studentsList as $studentList) {
            $studentList->average_score = $studentScore[$studentList->id];
        }
        if ($sortOrder === config('constants.ASC_NAME')) {
            $studentsList = $studentsList->sortBy('first_name');
            $sortName = config('constants.DESC_NAME');
            $sortScore = config('constants.DESC_SCORE');
        } elseif ($sortOrder === config('constants.DESC_NAME')) {
            $studentsList = $studentsList->sortByDesc('first_name');
            $sortName = config('constants.ASC_NAME');
            $sortScore = config('constants.ASC_SCORE');
        } elseif ($sortOrder === config('constants.ASC_SCORE')) {
            $studentsList = $studentsList->sortBy('average_score');
            $sortName = config('constants.DESC_NAME');
            $sortScore = config('constants.DESC_SCORE');
        } elseif ($sortOrder === config('constants.DESC_SCORE')) {
            $studentsList = $studentsList->sortByDesc('average_score');
            $sortName = config('constants.ASC_NAME');
            $sortScore = config('constants.ASC_SCORE');
        }
        if (!$sortName) {
            $sortName = config('constants.ASC_NAME');
        }
        if (!$sortScore) {
            $sortScore = config('constants.ASC_SCORE');
        }

        return view('grades.overview')->with([
            'grades' => $grades,
            'gradeSlug' => $gradeSlug,
            'licenseId' => $license->id,
            'course' => $course,
            'subjects' => $subjects,
            'activeSubjectSlug' => $activeSubjectSlug,
            'learningGoals' => $learningGoals,
            'studentsList' => $studentsList,
            'studentScore' => $studentScore,
            'averageClassScore' => $averageClassScore,
            'lgClassProgess' => $lgClassProgess,
            'totalClassProgress' => $totalClassProgress,
            'lgBaesScores' => $lgBaesScores,
            'studentLgBaesScore' => $studentLgBaesScore,
            'currentYear' => Carbon::now()->year,
            'urlSubject' => $subject,
            'defaultRoute' => route('teachers.defaultRoute',
                [$license->id, $course]),
            'viewType' => $viewType,
            'sortName' => $sortName,
            'sortScore' => $sortScore,
        ]);
    }

    /**
     * Display student details.
     * @param App\Models\License $license
     * @param  string $course
     * @param  string $gradeSlug
     * @param  string $subjectSlug
     * @param  integer $userId
     * @return \Illuminate\Http\Response
     */
    public function getGradeStudent(
        License $license,
        $courseSlug,
        $gradeSlug,
        $subjectSlug,
        $userId
    ) {

        $course = Course::findBySlug($courseSlug)->first();
        $subject = Subject::findBySlug($subjectSlug)->first();
        $subjectId = $subject->id;
        $learningGoals = $subject->learningGoals()
            ->orderBy('order')->get();
        $learningGoalsName = $learningGoals->pluck('name', 'id')
            ->toArray();
        $institutionId = $license->institution->id;
        $studenLicense = UserRepository::getStudentLicense($course->id,
            $institutionId, $userId);
        $studentLearningGoals = UserRepository::getStudentSubjectLearningGoals(
            $userId, $studenLicense->id, $subjectSlug);
        $student = UserRepository::getGradeUserDetails(
            $userId, $license->id, $institutionId, $gradeSlug, $subjectId);
        $profileImageUrl = Storage::url($student->image);
        $weeklyReport = UserRepository::weeklyActivity(
            Carbon::now(), $student->id, $studenLicense->id, $subjectId);
        $maxActivityScore = $weeklyReport->max('points');
        $weeklyReport = array_map("static::GetProgressDay", $weeklyReport->toArray());
        $studentBlocks = StudentService::getScoreAndProgressBlock(
            $studentLearningGoals);
        return view('grades.student')->with([
            'gradeSlug' => $gradeSlug,
            'student' => $student,
            'course' => $courseSlug,
            'courseName' => $course->name,
            'subjectName' => $subject->name,
            'licenseId' => $license->id,
            'subject' => $subjectSlug,
            'profileImageUrl' => $profileImageUrl,
            'weeklyActivities' => $weeklyReport,
            'maxActivityScore' => $maxActivityScore,
            'studentBlocks' => $studentBlocks,
            'learningGoals' => $learningGoals->toArray(),
            'studentLearningGoals' => $studentLearningGoals,
            'learningGoalsName' => $learningGoalsName,
            'subjectId' => $subjectId,
            'userId' => $userId,
            'lastLearningGoalName' => $learningGoals->last()->name,
        ]);
    }

    /**
     * Get week day from weekly report
     * @param array $weeklyReport
     * @return array $weeklyReport
     */
    public static function GetProgressDay($weeklyReport)
    {
        $days = ['0' => Lang::get('teachers.sunday'), '1' => Lang::get('teachers.monday'),
            '2' => Lang::get('teachers.tuesday'), '3' => Lang::get('teachers.wednesday'),
            '4' => Lang::get('teachers.thursday'), '5' => Lang::get('teachers.friday'),
            '6' => Lang::get('teachers.saturday')];
        $weeklyReport['day'] = $days[Carbon::parse($weeklyReport['date'])->dayOfWeek];
        return $weeklyReport;
    }

    public function getActivityOverview(
        License $license,
        $course,
        $gradeSlug,
        $subject,
        $viewType,
        $sortOrder = null
    ) {

        $sortName = $sortOrder;
        $sortScore = $sortOrder;
        $learningGoalActivityAverage = array();
        $gradeId = null;
        $courseId = Course::findBySlug($course)->first()->id;
        $institutionId = $license->institution->id;
        $grades = $license->institution->grades;
        $subjects = $license->course->subjects;
        $studentWeeklyReport = [];
        $studentLearningGoalActivity = [];
        $avgClassCount = 0;
        $avgClassScore = 0;
        $averageClassActivityScore = 0;

        if ($subject) {
            $activeSubject = Subject::findBySlug($subject)->first();
        } else {
            $activeSubject = $subjects->first();
        }
        $activeSubjectId = $activeSubject->id;
        $activeSubjectSlug = $activeSubject->slug;
        $learningGoals = $activeSubject->learningGoals()
            ->orderBy('order')->get()->toArray();

        $grade = Grade::findBySlug($gradeSlug)->first();
        if ($grade) {
            $gradeId = $grade->id;
        }
        $studentLearningGoals = UserRepository::getStudentsForLicenseCourse(
            $license->institution_id,
            $license->course_id, $activeSubjectId, $gradeId);
        $studentsList = $studentLearningGoals->unique('id')->sort();
        foreach ($studentsList as $student) {
            $studenLicense = UserRepository::getStudentLicense($courseId,
                $institutionId, $student->id);
            if ($viewType === config('constants.WEEK_VIEW')) {
                $lastDate = null;
            } else {
                $lastDate = $studenLicense->created_at;
            }
            foreach ($learningGoals as $learningGoal) {
                $weeklyReport = UserRepository::weeklyActivity(
                    Carbon::now(), $student->id, $studenLicense->id,
                    $activeSubjectId, $learningGoal['id'],
                    $activityOverview = true, $lastDate);

                $studentWeeklyReport[$student->id][$learningGoal['id']] =
                $weeklyReport->sum('points');
                $learningGoalActivityAverage[$learningGoal['id']] = 0;
            }
        }
        $weeklyReportCollection = collect($studentWeeklyReport);
        $averageLearningGoalScore = [];
        foreach ($learningGoals as $learningGoal) {
            $activeStudentActivity = $weeklyReportCollection->filter(
                function ($item) use ($learningGoal) {
                    return $item[$learningGoal['id']] > 0;
                });
            $averageLearningGoalScore[$learningGoal['id']] =
            $activeStudentActivity->avg($learningGoal['id']);
        }
        foreach ($studentWeeklyReport as $key => $studentScore) {
            $studentActiveLgCount = 0;
            $activeLgScore = 0;
            $studentLearningGoalActivity[$key] = array_sum($studentScore);
        }
        $averageClassActivityScore = array_sum($averageLearningGoalScore);

        foreach ($studentsList as $studentList) {
            $studentList->average_activity =
                $studentLearningGoalActivity[$studentList->id];
        }

        if ($sortOrder === config('constants.ASC_NAME')) {
            $studentsList = $studentsList->sortBy('first_name');
            $sortName = config('constants.DESC_NAME');
            $sortScore = config('constants.DESC_SCORE');
        } elseif ($sortOrder === config('constants.DESC_NAME')) {
            $studentsList = $studentsList->sortByDesc('first_name');
            $sortName = config('constants.ASC_NAME');
            $sortScore = config('constants.ASC_SCORE');
        } elseif ($sortOrder === config('constants.ASC_SCORE')) {
            $studentsList = $studentsList->sortBy('average_activity');
            $sortName = config('constants.DESC_NAME');
            $sortScore = config('constants.DESC_SCORE');
        } elseif ($sortOrder === config('constants.DESC_SCORE')) {
            $studentsList = $studentsList->sortByDesc('average_activity');
            $sortName = config('constants.ASC_NAME');
            $sortScore = config('constants.ASC_SCORE');
        }
        if (!$sortName) {
            $sortName = config('constants.ASC_NAME');
        }
        if (!$sortScore) {
            $sortScore = config('constants.ASC_SCORE');
        }

        return view('grades.activity-overview')->with([
            'gradeSlug' => $gradeSlug,
            'licenseId' => $license->id,
            'course' => $course,
            'grades' => $grades,
            'subjects' => $subjects,
            'activeSubjectSlug' => $activeSubjectSlug,
            'urlSubject' => $subject,
            'studentsList' => $studentsList,
            'learningGoals' => $learningGoals,
            'studentWeeklyReport' => $studentWeeklyReport,
            'studentLearningGoalActivity' => $studentLearningGoalActivity,
            'averageLearningGoalScore' => $averageLearningGoalScore,
            'averageClassActivityScore' => $averageClassActivityScore,
            'currentYear' => Carbon::now()->year,
            'defaultRoute' => route('teachers.defaultRoute',
                [$license->id, $course]),
            'viewType' => $viewType,
            'sortName' => $sortName,
            'sortScore' => $sortScore,
        ]);
    }

    /**
     * Display student learning goal details with number of attempts
     * @param App\Models\License $license
     * @param  string $course
     * @param  string $gradeSlug
     * @param  string $subjectSlug
     * @return \Illuminate\Http\Response
     */
    public function getAttemptsOverview(
        License $license,
        $course,
        $gradeSlug,
        $subject,
        $viewType,
        $sortOrder = null
    ) {

        $sortName = $sortOrder;
        $sortScore = $sortOrder;
        if ($viewType === config('constants.WEEK_VIEW')) {
            $currentDate = Carbon::now();
        } else {
            $currentDate = null;
        }
        $studentLearningGoalAttempt = [];
        $lgClassProgess = [];
        $studentAverageGoalAttempt = [];
        $learningGoalAverageAttempt = [];
        $attemptCountArray = [];
        $totalClassProgress = 0;
        $gradeId = null;
        $courseId = Course::findBySlug($course)->first()->id;
        $institutionId = $license->institution->id;
        $grades = $license->institution->grades;
        $subjects = $license->course->subjects;
        if ($subject) {
            $activeSubject = Subject::findBySlug($subject)->first();
        } else {
            $activeSubject = $subjects->first();
        }
        $activeSubjectId = $activeSubject->id;
        $activeSubjectSlug = $activeSubject->slug;
        $learningGoals = $activeSubject->learningGoals()
            ->orderBy('order')->get()->toArray();
        $grade = Grade::findBySlug($gradeSlug)->first();
        if ($grade) {
            $gradeId = $grade->id;
        }
        $studentLearningGoals = UserRepository::getStudentsForLicenseCourse(
            $license->institution_id, $license->course_id,
            $activeSubjectId, $gradeId);
        $learningGoalArray = $studentLearningGoals->groupBy('learning_goal_id');
        foreach ($learningGoalArray as $key => $learning_goal_students) {
            $finishedGoalStudent = 0;
            $totalStudent = 0;
            foreach ($learning_goal_students as $goal_progress) {
                if ($goal_progress['status'] === config('constants.FINISHED')) {
                    $finishedGoalStudent++;
                }
                $totalStudent++;
            }
            $lgClassProgess[$key] = (($finishedGoalStudent / $totalStudent) * 100);
            if (!count($lgClassProgess)) {
                $totalClassProgress = 0;
            } else {
                $totalClassProgress = (array_sum($lgClassProgess) / count($lgClassProgess));
            }
        }

        $students = $studentLearningGoals->groupBy('id')->toArray();
        $studentsList = $studentLearningGoals->unique('id')->sort();
        foreach ($students as $key => $student) {
            foreach ($student as $finishedStudent) {
                $studentLearningGoalAttempt[$key]
                [$finishedStudent['learning_goal_id']]['attempts'] = UserRepository::learningGoalTestAttempts($finishedStudent['learning_goal_test_id'],
                    $currentDate);
                $studentLearningGoalAttempt[$key]
                [$finishedStudent['learning_goal_id']]['status'] = $finishedStudent['status'];
            }
        }
        foreach ($studentLearningGoalAttempt as $key => $studentGoalAttempt) {
            $finishedAttemptCount = 0;
            $attemptLgAverage = 0;
            foreach ($learningGoals as $learningGoal) {
                if ($studentGoalAttempt[$learningGoal['id']]['status'] === config('constants.FINISHED')) {
                    $finishedAttemptCount++;
                    $attemptLgAverage = $attemptLgAverage + $studentGoalAttempt
                        [$learningGoal['id']]['attempts'];

                    if (isset($learningGoalAverageAttempt[$learningGoal['id']][config('constants.FINISHED')])) {
                        $learningGoalAverageAttempt[$learningGoal['id']][config('constants.FINISHED')]++;
                    } else {
                        $learningGoalAverageAttempt[$learningGoal['id']][config('constants.FINISHED')] = 1;
                    }
                    if (array_key_exists('attempts',
                        $learningGoalAverageAttempt[$learningGoal['id']])) {
                        $learningGoalAverageAttempt[$learningGoal['id']]['attempts'] =
                            $learningGoalAverageAttempt[$learningGoal['id']]['attempts']
                             + $studentGoalAttempt[$learningGoal['id']]['attempts'];
                    } else {
                        $learningGoalAverageAttempt[$learningGoal['id']]['attempts'] =
                            $studentGoalAttempt[$learningGoal['id']]['attempts'];
                    }
                } elseif ($studentGoalAttempt[$learningGoal['id']]['status'] === config('constants.STARTED')) {
                    if (isset($learningGoalAverageAttempt[$learningGoal['id']][config('constants.STARTED')])) {
                        $learningGoalAverageAttempt[$learningGoal['id']][config('constants.STARTED')]++;
                    } else {
                        $learningGoalAverageAttempt[$learningGoal['id']][config('constants.STARTED')] = 1;
                    }
                } elseif ($studentGoalAttempt[$learningGoal['id']]['status'] === config('constants.AVAILABLE')) {
                    if (isset($learningGoalAverageAttempt[$learningGoal['id']][config('constants.AVAILABLE')])) {
                        $learningGoalAverageAttempt[$learningGoal['id']][config('constants.AVAILABLE')]++;
                    } else {
                        $learningGoalAverageAttempt[$learningGoal['id']][config('constants.AVAILABLE')] = 1;
                    }
                }
            }

            if ($finishedAttemptCount > 0) {
                $studentAverageGoalAttempt[$key] = $attemptLgAverage / $finishedAttemptCount;
            } else {
                $studentAverageGoalAttempt[$key] = 0;
            }
        }
        $learningGoalAttemptsArray = [];
        $averageAttempts = 0;
        $avgScore = 0;
        $averageFinishCount = 0;

        foreach ($learningGoalAverageAttempt as $key => $attempts) {
            if (array_key_exists(config('constants.FINISHED'), $attempts)) {
                $averageFinishCount++;
                $avgScore = ($attempts['attempts'] / $attempts[config('constants.FINISHED')]);
                $averageAttempts = $averageAttempts + number_format($avgScore);
                $attempts['attempts'] = number_format($avgScore);
            } else {
                $attempts['attempts'] = 0;
            }
            $learningGoalAttemptsArray[$key] = $attempts;
        }
        if ($averageFinishCount) {
            $averageAttempts = ($averageAttempts / $averageFinishCount);
        }
        foreach ($studentsList as $studentList) {
            $studentList->average_attempts =
                $studentAverageGoalAttempt[$studentList->id];
        }

        if ($sortOrder === config('constants.ASC_NAME')) {
            $studentsList = $studentsList->sortBy('first_name');
            $sortName = config('constants.DESC_NAME');
            $sortScore = config('constants.DESC_SCORE');
        } elseif ($sortOrder === config('constants.DESC_NAME')) {
            $studentsList = $studentsList->sortByDesc('first_name');
            $sortName = config('constants.ASC_NAME');
            $sortScore = config('constants.ASC_SCORE');
        } elseif ($sortOrder === config('constants.ASC_SCORE')) {
            $studentsList = $studentsList->sortBy('average_attempts');
            $sortName = config('constants.DESC_NAME');
            $sortScore = config('constants.DESC_SCORE');
        } elseif ($sortOrder === config('constants.DESC_SCORE')) {
            $studentsList = $studentsList->sortByDesc('average_attempts');
            $sortName = config('constants.ASC_NAME');
            $sortScore = config('constants.ASC_SCORE');
        }

        if (!$sortName) {
            $sortName = config('constants.ASC_NAME');
        }
        if (!$sortScore) {
            $sortScore = config('constants.ASC_SCORE');
        }
        return view('grades.attempts-overview')->with([
            'gradeSlug' => $gradeSlug,
            'licenseId' => $license->id,
            'course' => $course,
            'grades' => $grades,
            'subjects' => $subjects,
            'activeSubjectSlug' => $activeSubjectSlug,
            'urlSubject' => $subject,
            'learningGoals' => $learningGoals,
            'currentYear' => Carbon::now()->year,
            'studentsList' => $studentsList,
            'studentLearningGoalAttempt' => $studentLearningGoalAttempt,
            'lgClassProgess' => $lgClassProgess,
            'totalClassProgress' => $totalClassProgress,
            'studentAverageGoalAttempt' => $studentAverageGoalAttempt,
            'learningGoalAttemptsArray' => $learningGoalAttemptsArray,
            'averageAttempts' => $averageAttempts,
            'defaultRoute' => route('teachers.defaultRoute',
                [$license->id, $course]),
            'viewType' => $viewType,
            'sortName' => $sortName,
            'sortScore' => $sortScore,
        ]);
    }
}
