<?php

namespace App\Http\Controllers;

use App\Models\LearningGoal;
use App\Models\QuestionAnswer;
use App\Services\HintService;
use App\Services\QuestionAnswerRenderService;
use Illuminate\Pipeline\Pipeline;

class QuestionAnswerController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  App\Models\QuestionAnswer  $questionAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionAnswer $questionAnswer)
    {
        $question = app(Pipeline::class)
            ->send($questionAnswer)
            ->through(QuestionAnswerRenderService::class)
            ->then(function ($question) {
                return $question;
            });
        $questionTemplate = $questionAnswer->questionTemplate()
            ->select('id', 'type', 'learningGoal_id')->first();
        $learningGoal = LearningGoal::select('slug')->findOrFail(
            $questionTemplate->learningGoal_id);
        $questionType = strtoupper($questionTemplate->type);
        if ($questionType === config('constants.DIRECT_TYPE')) {
            $question->fullHint = HintService::getHint($question->hint->hint,
                config('constants.FOURTH_ATTEMPT'));
        }
        return view('student-feedbacks.question-answer', [
            'type' => $questionType,
            'questionTemplateId' => $questionTemplate->id,
            'learningGoalSlug' => $learningGoal->slug,
            'question' => $question->toArray()])->render();
    }
}
