<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExportDataRequest;
use App\Http\Requests\TeacherLoginRequest;
use App\Jobs\ExportTableJob;
use App\Models\Institution;
use App\Repositories\UserRepository;
use App\Services\ImageService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Lang;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    const IMAGE_UPLOAD = 'images/uploads/';

    /**
     * Show dashboard for the user
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('admins.dashboard');
    }

    /**
     * Show dashboard for the user
     *
     * @return \Illuminate\Http\Response
     */
    public function subAdminDashboard()
    {
        return view('sub-admins.dashboard');
    }

    /**
     * Upload image from ckeditor to AWS
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        if ($request->hasFile('upload')) {
            $url = ImageService::save($request->file('upload'), false);
            return view('includes.uploadImage', [
                'funcNum' => $request->CKEditorFuncNum,
                'url' => Storage::url($url), 'message' => 'Uploaded',
            ]);
        }
    }

    /**
     * Goto teacher dashboard
     *
     * @param Illuminate\Http\TeacherLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function teacherLogin(TeacherLoginRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $response = UserRepository::teacherLogin(auth()->user(),
            $validatedData['institutionId'], $validatedData['courseId']);
        DB::commit();
        return response()->json($response);
    }

    /**
     * Show table name for the admin to exort
     *
     * @return \Illuminate\Http\Response
     */
    public function exportTable()
    {
        return view('admins.export-tables')->with([
            'institutions' => Institution::select('id', 'name')->get(),
        ]);
    }

    /**
     * Export data in excel format
     *
     * @return \Illuminate\Http\Response
     */
    public function exportData(ExportDataRequest $request)
    {
        $validatedData = $request->validated();
        $institutionId = null;
        if (array_key_exists('institution_id', $validatedData)) {
            $institutionId = $validatedData['institution_id'];
        }
        $exportTables = $validatedData['tables'];
        dispatch(new ExportTableJob(auth()->user(), $exportTables, $institutionId));
        session()->flash('status', Lang::get('admins.data_exported_successfully')
            . implode(', ', $exportTables));
        return redirect()->route('admins.exportTable');
    }
}
