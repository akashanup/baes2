<?php

namespace App\Http\Controllers;

use App\Http\Requests\TemplateUploadRequest;
use App\Jobs\ChangeQuestionAnswerStatus;
use App\Jobs\GenerateLearningGoalQuestion;
use App\Jobs\GenerateQuestions;
use App\Models\LearningGoal;
use App\Models\QuestionAnswer;
use App\Models\QuestionFeedback;
use App\Models\QuestionTemplate;
use App\Models\TemplateVariable;
use App\Services\ImportVariableSetService;
use App\Services\TemplateUploadService;
use App\Services\TemplateUtilityService;
use Auth;
use DB;
use Illuminate\Http\Request;
use Lang;
use Log;
use Maatwebsite\Excel\Facades\Excel;
use Notifications;
use PDF;

class QuestionTemplateController extends Controller
{

    const MCQTYPE = 'MCQ';
    const DIRECTTYPE = 'DIRECT';

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $type, $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, $type)
    {
        if ($type == static::MCQTYPE) {
            return view('templates.edit-mcq-template')->with([
                'templateList' => route('template.getTemplateList', [$slug, $type]),
                'TemplateCurriculumData' => route('learning-goals.curriculumData',
                    ['template', $type]),
                'type' => $type,
            ]);
        } elseif ($type == static::DIRECTTYPE) {
            return view('templates.edit-direct-template')->with([
                'templateList' => route('template.getTemplateList', [$slug, $type]),
                'TemplateCurriculumData' => route('learning-goals.curriculumData',
                    ['template', $type]),
                'type' => $type,
            ]);
        }
    }

    /**
     * Upload ODS question template -MCQ or Direct.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadTemplate($slug, $type)
    {
        return view('templates.upload')->with([
            'learningGoals' => LearningGoal::getAll()->get(),
            'slug' => $slug,
            'type' => $type,
        ]);
    }

    /**
     * Upload ODS question template -MCQ.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */

    public function mcqTemplateList($slug)
    {
        return TemplateUploadService::getMcqTemplateList
            ($slug, config('constants.MCQ_TYPE'));
    }

    /**
     * Get Question template list .
     *
     * @param  string  $slug, $curriculum
     * @return \Illuminate\Http\Response
     */

    public function getTemplateList($slug, $type, $curriculumSlug = null)
    {
        if ($curriculumSlug === '0') {
            $curriculumSlug = null;
            $slug = null;
        }
        if ($curriculumSlug) {
            $slug = null;
        }
        if ($slug) {
            $curriculumSlug = null;
        }
        return TemplateUploadService::getTemplatesList(
            $slug, $type, $curriculumSlug);
    }

    /**
     * Download question template MCQ or DIRECT.
     * @param  string  $slug
     * @param  string  $type
     * @return \Illuminate\Http\Response
     */

    public function downloadQuestionTemplate($slug, $type)
    {
        $questionTemplateData = QuestionTemplate::
            getLearningTemplateQuestion($slug, $type)
            ->get();
        if ($questionTemplateData->count()) {
            if ($type === static::MCQTYPE) {
                TemplateUploadService::exportMcqQuestionTemplates($questionTemplateData);
            } elseif ($type === static::DIRECTTYPE) {
                TemplateUploadService::exportDirectQuestionTemplates($questionTemplateData);
            }
        } else {
            return redirect()->route('learning-goals.index')->withErrors(
                Lang::get('admins.error_download')
            );
        }

        session()->flash('status',
            Lang::get('admins.success_download')
        );
        return redirect()->route('learning-goals.index');
    }

    /**
     * Download direct variable set.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */

    public function downloadVariableSet($slug)
    {
        $questionTemplateData = QuestionTemplate::
            getLearningTemplateQuestion($slug, static::DIRECTTYPE)
            ->get();
        if ($questionTemplateData->count()) {
            TemplateUploadService::exportVariableSets($questionTemplateData);
        } else {
            return redirect()->route('learning-goals.index')->withErrors(
                Lang::get('admins.error_download')
            );
        }

        session()->flash('status',
            Lang::get('admins.success_download')
        );
        return redirect()->route('learning-goals.index');
    }

    /**
     * Upload ODS question template -MCQ or Direct.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */

    public function uploadData(TemplateUploadRequest $request)
    {
        $data['goal_id'] = $request->learningGoalID;
        $data['status_change'] = $request->status;

        // Fetching the name of the file
        $file_name = request()->file('file_name')->getClientOriginalName();
        // Fetching the path of the file
        $file_path = request()->file('file_name')->getRealPath();

        $response = null;
        $thisResponse = null;
        // Checking file name convention is followed
        if (strpos(strtolower($file_name), strtolower(config('constants.MCQ_TYPE'))) !== false || strpos(strtolower($file_name), strtolower(config('constants.DIRECT_TYPE'))) !== false) {
            // Checking Excel sheet Import is of which catergory - MCQ/DIRECT
            if (strpos(strtolower($file_name), strtolower(static::MCQTYPE))
                !== false) {
                $thisResponse = $this->importMCQFunction($file_path, $data);
            } elseif (strpos(strtolower($file_name), strtolower(static::DIRECTTYPE)) !== false) {
                $thisResponse = $this->importDirectFunction($file_path, $data);
            } else {
                return redirect()->back()->withErrors(Lang::get('admins.error_download'));
            } //end if
            Log::info("Run job in uploadData");
            if ($thisResponse['status'] === 'success') {
                // Queuing a job to generate questions
                Log::info("Run job");
                dispatch(new GenerateQuestions());
                session()->flash('status', $thisResponse['message']);
                return redirect()->route('learning-goals.index');
            } else {
                return redirect()->back()
                    ->withErrors($thisResponse['message']);
            }
        } else {
            // Redirecting with error to upload view
            return redirect()->back()
                ->withErrors(Lang::get('admins.file_convention'));
        } //end if
    } //end uploadData()

    /**
     * Import MCQ questions
     *
     * @param file_path
     * @return void
     */

    public function importMCQFunction($file_path, $data)
    {
        $responseData = array();
        // Reading the MCQ question sheet
        $mcq_template = Excel::load($file_path)->toArray();
        if (!empty(array_diff(array_keys($mcq_template[0][0]), ['mcq_id', 'lg_id', 'question', 'complexity', 'correctanswer']))) {
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.mcq_question_column_mismatch'),
            ];
        } elseif (!empty(array_diff(array_keys($mcq_template[1][0]), ['mca_id', 'mcq_id', 'answer']))) {
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.mcq_answer_column_mismatch'),
            ];
        } else {
            $data['type'] = strtolower(config('constants.MCQ_TYPE'));
            // Creating object for ImportData model
            $insert_mcq_templates = new TemplateUploadService();
            // Calling mcq function of the model to parse sheet and insert tables
            $responseData = $insert_mcq_templates->mcq($mcq_template, $data);
        }
        return $responseData;
    }

    /**
     * Import Direct questions
     *
     * @param file_path
     * @return void
     */

    public function importDirectFunction($file_path, $data)
    {
        $responseData = null;
        // Reading the Direct question sheet
        $direct_template = Excel::load($file_path)->toArray();

        // Checking if question template colummn name are as per documentation
        // Checking if feedback template colummn name are as per documentation
        // Checking if question variable colummn name are as per documentation
        if (!empty(array_diff(array_keys($direct_template[0][0]), ['qt_id', 'lg_id', 'qt_template', 'qt_complexity']))) {
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.question_template_column_mismatch'),
            ];
        } elseif (!empty(array_diff(array_keys($direct_template[1][0]), ['ft_id', 'qt_id', 'ft_template', 'ft_step']))) {
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.feedbcak_template_column_mismatch'),
            ];
        } elseif (!empty(array_diff(array_keys($direct_template[2][0]), ['qv_id', 'qt_id', 'qv_group', 'qv_variable', 'qv_value', 'value_type', 'rounding_value']))) {
            $responseData = [
                'status' => 'failed',
                'message' => Lang::get('admins.question_variable_column_mismatch'),
            ];
        } else {
            $data['type'] = strtolower(config('constants.DIRECT_TYPE'));
            // Creating object for ImportData model
            $insert_direct_templates = new TemplateUploadService();
            // Calling mcq function of the model to parse sheet and insert tables
            $responseData = $insert_direct_templates->direct($direct_template, $data);
        }
        return $responseData;
    }

    /**
     * Make the pdf from HTML view and send email using Jobs and Notifications
     *
     * @param  string  $slug, string $type
     * @return \Illuminate\Http\Response
     */

    public function downloadLearningGoalQuestion($slug, $type)
    {

        DB::beginTransaction();
        $learningGoal = LearningGoal::findBySlug($slug)->first();
        $questionTemplateData = $learningGoal->getAllQuestionAnswers($type)->toArray();
        DB::commit();

        if (count($questionTemplateData) === 0) {
            session()->flash('status',
                Lang::get('admins.no_question_found')
            );
            return redirect()->route('learning-goals.index');
        }

        $templateData = array_map("static::formatArray", $questionTemplateData);
        //    return view('templates.direct-question-pdf')->with([
        //     'questionTemplateData' => $templateData,
        //     'learningGoalName' => $learningGoal->name
        // ]);
        $user = Auth::user();
        dispatch(new GenerateLearningGoalQuestion($templateData, $learningGoal->name, $type, Auth::user()));
        session()->flash('status',
            Lang::get('admins.email_sent')
        );
        return redirect()->route('learning-goals.index');
    }

    /**
     * Modify Array values
     *
     * @param  array  $data
     * @return $data
     */

    public function formatArray($data)
    {
        if ($data['type'] == strtolower(static::DIRECTTYPE)) {
            $data['answer'] = TemplateUtilityService::dutchFormat($data['answer']);
        }
        $data['complexity'] = config('constants.COMPLEXITY')[$data['complexity']];
        return $data;
    }

    /**
     * Edit Learning goal question template
     * @param  string  $type
     * @param  integer  $type
     * @return \Illuminate\Http\Response
     */
    public function editQuestionTemplate($slug, $type, $id)
    {
        $question = QuestionTemplate::find($id);
        $learningGoalId = LearningGoal::findBySlug($slug)->first()->id;
        if ($type === config('constants.DIRECT_TYPE')) {
            $feedback = QuestionFeedback::where(
                'question_template_id', $question->id)->get();
            return view('templates.direct-question-template-edit')->with([
                'question' => $question,
                'feedbacks' => $feedback,
                'type' => $type,
                'questionTemplateId' => $id,
                'learningGoalId' => $learningGoalId,
            ]);
        }
        if ($type === config('constants.MCQ_TYPE')) {
            $options = '';
            $correctAnswer = '';
            $correctoption = '';
            $templateVariable = $question->templateVariables;
            $templateVariable = $templateVariable->toArray();
            foreach ($templateVariable as $variableData) {
                $options = $variableData['values'];
                $correctAnswer = $variableData['correct_answer'];
            }
            return view('templates.mcq-question-template-edit')->with([
                'question' => $question,
                'options' => json_decode($options, true),
                'correctAnswer' => $correctAnswer,
                'learningGoalId' => $learningGoalId,
                'questionTemplateId' => $id,
                'type' => $type,
            ]);
        }
    }

    public function updateQuestionTemplate(
        Request $request,
        $learningGoalId,
        $type,
        $id
    ) {
        if ($type === config('constants.MCQ_TYPE')) {
            $this->validate(request(), [
                'question' => 'required',
                'option1' => 'required',
                'option2' => 'required',
                'option3' => 'required',
                'correct_answer' => 'required|numeric',
            ]);

            return static::updateMcqQuestionTemplate
                ($request, $learningGoalId, $type, $id);
        } elseif ($type === config('constants.DIRECT_TYPE')) {
            $this->validate(request(), [
                'qt_id' => 'required|numeric',
                'question' => 'required',
                'feedback2' => 'required',
                'feedback3' => 'required',
                'feedback4' => 'required',
            ]);

            return static::updateDirectQuestionTemplate
                ($request, $learningGoalId, $type, $id);
        }
    }

    /**
     * update MCQ question template
     * @param  Request  $request
     * @param  integer  $learningGoalId
     * @param  string  $type
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMcqQuestionTemplate(
        $request,
        $learningGoalId,
        $type,
        $id
    ) {
        $runJob = 1;
        DB::begintransaction();
        $jobData = array();
        $data = [
            'goal_id' => $learningGoalId,
            'type' => $type,
        ];

        for ($option = 1; $option <= 4; $option++) {
            $option_name = "option" . $option;
            $option_array[$option] = $request->$option_name;
        }
        $option_json = json_encode($option_array);

        $currentQuestionTemplate = QuestionTemplate::find($id);
        $inactiveQuestion = [
            'questionTemplate' => $currentQuestionTemplate->id,
        ];
        array_push($jobData, $inactiveQuestion);

        $templateVariableData = [
            'values' => $option_json,
            'correct_answer' => $option_array[$request->correct_answer],
            'question_template_id' => $currentQuestionTemplate->id,
            'status' => config('constants.PENDING'),
        ];
        if ($currentQuestionTemplate->template === $request->question
            &&
            $currentQuestionTemplate->learningGoal_id === intval($learningGoalId)
            &&
            $currentQuestionTemplate->status === config('constants.ACTIVE')) {
            Log::info("Question temp exists");
            // check if Template variable exsists
            $currentTemplateVariable = TemplateVariable::
                getCurrentTemplateVariable(
                $option_array[$request->correct_answer],
                config('constants.FINISHED'),
                $currentQuestionTemplate->id);
            if ($currentTemplateVariable) {
                Log::info("Temp var exists");
                // check if options are same
                $optionChange = array_diff_assoc
                    (json_decode($currentTemplateVariable->values, true), $option_array);
                if ($optionChange) {
                    $runJob = 1;
                    Log::info("option chnage");
                    // insert template variable
                    TemplateVariable::insertTemplateVariable(
                        $templateVariableData);
                } else {
                    $runJob = 0;
                    Log::info("no change: " . $runJob);
                    //do nothing
                }
            } else {
                $runJob = 1;
                Log::info("Temp var not exists");
                // inactive all template variables and question answers
                // insert template variable
                TemplateVariable::insertTemplateVariable(
                    $templateVariableData);
            }
        } else {
            $runJob = 1;
            Log::info("Question temp not exists");
            // Not exsists
            // Inactive Question template
            // inactive template Variable
            // update question template
            $currentQuestionTemplate->updateQuestionTemplate
                (['template' => $request->question]);
            TemplateVariable::insertTemplateVariable(
                $templateVariableData);
        }
        Log::info($runJob);

        dispatch(new GenerateQuestions());
        if ($runJob) {
            Log::info("in run job");
            // $changeStatus = new ChangeQuestionAnswerStatus($data, $jobData, $inActiveTemplateQuestion = null);
            // $changeStatus->handle();
            dispatch(new ChangeQuestionAnswerStatus($data, $jobData, $inActiveTemplateQuestion = null));
        }
        DB::commit();
        session()->flash('status', Lang::get('admins.template_updated'));
        return redirect()->route('learning-goals.index');
    }

    /**
     * update DIRECT question template
     * @param  Request  $request
     * @param  integer  $learningGoalId
     * @param  string  $type
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDirectQuestionTemplate(
        $request,
        $learningGoalId,
        $type,
        $id
    ) {
        DB::begintransaction();
        // Getting all template variable
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $request->question, $template_variable1);
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $request->feedback2, $template_variable2);
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $request->feedback3, $template_variabl3);
        preg_match_all('/\[\[([A-Za-z0-9 \.]+)\]\]/i', $request->feedback4, $template_variabl4);

        // Creating a unique and single variable set array
        $template_variable = array_merge($template_variable1[1], $template_variable2[1], $template_variabl3[1], $template_variabl4[1]);
        $template_variable = array_unique($template_variable);

        // Finding template and adding clone of it with updated data
        $question_template = QuestionTemplate::find($request->qt_id);
        $question_copy = $question_template->replicate();
        $question_copy->template = $request->question;

        // Setting earlier template and question to inactive
        QuestionTemplate::where('id', $request->qt_id)
            ->update(['status' => config('constants.INACTIVE')]);
        QuestionAnswer::where('question_template_id', $request->qt_id)
            ->update(['status' => config('constants.INACTIVE')]);
        $question_copy->save();
        $question_template_id = $question_copy->id;

        // Fetching variable set and checking for variable match
        $template_sets = TemplateVariable::where('question_template_id',
            $request->qt_id)->get();
        foreach ($template_sets as $template_set) {
            $variable_name = array_map('trim', array_keys(json_decode($template_set->values, true)));
            $diff = array_diff($template_variable, array_unique($variable_name));
            $template_set_copy = $template_set->replicate();
            $template_set_copy->question_template_id = $question_template_id;
            $template_set_copy->status = config('constants.PENDING');
            $template_set_copy->save();
        }
        // Feedback adding
        for ($feedback = 2; $feedback < 5; $feedback++) {
            $feedback_insert = new QuestionFeedback();
            $feedback_name = "feedback" . $feedback;
            $feedback_insert->template = $request->$feedback_name;
            $feedback_insert->step = $feedback;
            $feedback_insert->question_template_id = $question_template_id;
            $feedback_insert->save();
        }
        DB::commit();
        dispatch(new GenerateQuestions());
        session()->flash('status', Lang::get('admins.template_updated'));
        return redirect()->route('learning-goals.index');
    }

    /**
     * Create learning goal question template
     * @param  string  $type
     * @return \Illuminate\Http\Response
     */
    public function createTemplate($type)
    {
        $learningGoals = LearningGoal::getAll()->get();
        if ($type === config('constants.MCQ_TYPE')) {
            return view('templates.create-mcq-template')->with([
                'learningGoals' => $learningGoals,
                'type' => $type,
            ]);
        } elseif ($type === config('constants.DIRECT_TYPE')) {
            return view('templates.create-direct-template')->with([
                'learningGoals' => $learningGoals,
                'type' => $type,
            ]);
        }
    }

    /**
     * save learning goal question template
     * @param  string  $type
     * @return \Illuminate\Http\Response
     */
    public function saveTemplate(Request $request, $type)
    {
        if ($type === config('constants.MCQ_TYPE')) {
            $this->validate($request, [
                'learning_goal_select' => 'required|numeric',
                'question' => 'required',
                'option1' => 'required',
                'option2' => 'required',
                'option3' => 'required',
                'option4' => 'required',
                'complexity' => 'required|numeric',
                'correct_answer' => 'required|numeric',
            ]
            );
            return static::createMcqQuestionTemplate($request, $type);
        } elseif ($type === config('constants.DIRECT_TYPE')) {
            $this->validate(
                $request,
                [
                    'learning_goal_select' => 'required|numeric',
                    'question' => 'required',
                    'feedback2' => 'required',
                    'feedback3' => 'required',
                    'feedback4' => 'required',
                    'set_number' => 'nullable|numeric',
                    'complexity' => 'required|numeric',
                    'fileUpload' => 'mimes:xlsx,ods',
                ],
                [
                    'question.required' => 'Question required',
                    'feedback2.required' => 'Feedback step 2 required',
                    'feedback3.required' => 'Feedback step 3 required',
                    'feedback4.required' => 'Feedback step 4 required',
                ]
            );

            return static::createDirectQuestionTemplate($request, $type);
        }
    }

    /**
     * Create MCQ question template
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createMcqQuestionTemplate($request, $type)
    {
        DB::begintransaction();
        $option = array(
            '1' => $request->option1,
            '2' => $request->option2,
            '3' => $request->option3,
            '4' => $request->option4,
        );
        $option_json = json_encode($option);

        $questionTemplateData = [
            'template' => $request->question,
            'language' => 'nl',
            'complexity' => $request->complexity,
            'type' => config('constants.MCQ_TYPE'),
            'learningGoal_id' => $request->learning_goal_select,
            'order' => QuestionTemplate::getTemplateQuestion(
                $request->learning_goal_select, config('constants.ACTIVE'),
                strtolower($type))
                ->getAll()->count() + 1,
        ];
        $templateVariableData = [
            'values' => $option_json,
            'correct_answer' => $option[$request->correct_answer],
        ];
        Log::info("insert quest temp");
        $insertedQuestionTemplate = QuestionTemplate::
            insertQuestionTemplate($questionTemplateData);
        if ($insertedQuestionTemplate) {
            Log::info("inserted... quest temp" . $insertedQuestionTemplate->id);
            $templateVariableData['question_template_id']
            = $insertedQuestionTemplate->id;
            Log::info("insert temp var");
            TemplateVariable::insertTemplateVariable
                ($templateVariableData);
            session()->flash('status', Lang::get('admins.template_added'));
            $response = redirect()->route('learning-goals.index');
        } else {
            Log::info("ques temp error");
            DB::rollback();
            $response = redirect()->route('learning-goals.index')
                ->withErrors(Lang::get('admins.unable_to_insert'));
        }
        dispatch(new GenerateQuestions());
        DB::commit();
        return $response;
    }

    /**
     * Create DIRECT question template
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createDirectQuestionTemplate($request, $type)
    {
        $questionTemplateData = [
            'template' => $request->question,
            'language' => 'nl',
            'complexity' => $request->complexity,
            'type' => strtolower(config('constants.DIRECT_TYPE')),
            'learningGoal_id' => $request->learning_goal_select,
            'order' => QuestionTemplate::getTemplateQuestion
            ($request->learning_goal_select, config('constants.ACTIVE'),
                strtolower(config('constants.DIRECT_TYPE')))
                ->getAll()->count() + 1,
        ];
        $insert_template = QuestionTemplate
            ::insertQuestionTemplate($questionTemplateData);
        // Inserting feedback template
        for ($step = 2; $step < 5; $step++) {
            $insert_feedback = new QuestionFeedback();
            $feedback = "feedback" . $step;
            $insert_feedback->template = $request->$feedback;
            $insert_feedback->step = $step;
            $insert_feedback->question_template_id = $insert_template->id;
            $insert_feedback->save();
        }
        $qt_id = $insert_template->id;
        session(['db_insert' => 0]);
        session(['qt_id' => $qt_id]);

        // Getting all template variable
        $template_variable = ImportVariableSetService::getTemplateVariables($request->question, $request->feedback2, $request->feedback3, $request->feedback4);
        session(['variable_array' => $template_variable]);

        $qt_id = $qt_id ? $qt_id : session('qt_id');
        $response = null;

        // If ods/xlsx file is present. Inserting variables with the file.
        if ($request->file_upload != null) {
            $newTemplateVariables = ImportVariableSetService::fileUpload($qt_id, $request->file_upload, $template_variable, ImportVariableSetService::CREATE_MODE);
            if (!$newTemplateVariables) {
                DB::rollback();
                return redirect()->route('learning-goals.index')
                    ->withErrors(Lang::get('admins.question_variable_column_mismatch'));
            }

            QuestionTemplate::where('id', $qt_id)
                ->update(['status' => config('constants.ACTIVE')]);
            DB::commit();
            dispatch(new GenerateQuestions());
            session()->flash('status', Lang::get('admins.template_added'));
            $response = redirect()->route('learning-goals.index');
        } elseif ($request->set_number) {
            DB::commit();
            $response = view('templates.add-direct-template-variable-set')
                ->with(['template_variables' => $template_variable,
                    'set_number' => $request->set_number,
                    'qt_id' => $qt_id]);
        } else {
            QuestionTemplate::where('id', $qt_id)
                ->update(['status' => config('constants.ACTIVE')]);
            DB::commit();
            session()->flash('status', Lang::get('admins.template_added'));
            $response = redirect()->route('learning-goals.index');
        }
        return $response;
    }
}
