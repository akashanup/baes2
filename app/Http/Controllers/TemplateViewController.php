<?php

namespace App\Http\Controllers;

use App\Models\TemplateVariable;
use App\Services\GenerateTextService;
use Illuminate\Http\Request;

class TemplateViewController extends Controller
{
    /**
     * get question template data to preview.
     * @return \Illuminate\Http\Response
     */

    public function previewDirectTemplate(Request $request)
    {
        $this->validate(request(), [
            'id' => 'required|numeric',
        ]);
        // Fetching first variable set for qt_id
        $variables = TemplateVariable::select('values')
            ->where('question_template_id', '=', $request->id)
            ->where('status_edit', config('constants.NO'))
            ->first();
        $data = array();
        // Generating text questions
        $data['question'] = GenerateTextService::text
            ($request->question, $variables->values);
        $data['feedback2'] = GenerateTextService::text
            ($request->feedback2, $variables->values);
        $data['feedback3'] = GenerateTextService::text
            ($request->feedback3, $variables->values);
        $data['feedback4'] = GenerateTextService::text
            ($request->feedback4, $variables->values);
        $data = json_encode($data);
        return $data;
    }
}
