<?php

namespace App\Http\Controllers;

use App\Http\Requests\LicenseRequest;
use App\Models\Course;
use App\Models\Institution;
use App\Models\License;
use App\Services\InstitutionService;
use App\Services\LicenseService;
use App\Services\YajraDataTableService;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Lang;
use Log;

class LicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return \Illuminate\Http\Response
     */
    public function index($course, $institution)
    {
        return view('licenses.index')->with([
            'courses' => Course::getAll()->get(),
            'courseSlug' => $course,
            'institutionSlug' => $institution,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $course Course slug
     * @param string $institution Institution slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData($course, $institution)
    {
        return YajraDataTableService::licenses($course, $institution);
    }

    /**
     * Display a resource.
     *
     * @param integer $license License id
     * @param string $code License code
     * @return \Illuminate\Http\Response
     */
    public function show(License $license, $code = null)
    {
        $licenseData = app(Pipeline::class)
            ->send(License::coursesInstitutionsSubjects()
                    ->getLicenses()->where('licenses.id', $license->id)
                    ->where('licenses.code', $code)->get())
            ->through([InstitutionService::class, LicenseService::class])
            ->then(function ($license) {
                return $license->first()->toArray();
            });
        return view('licenses.show')->with([
            'license' => $licenseData,
            'grades' => $license->institution->grades->toArray(),
            'code' => $code,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LicenseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LicenseRequest $request)
    {
        DB::beginTransaction();
        $response = null;
        $validatedData = $request->validated();
        //Check for similar license
        if (License::where($validatedData)->first()) {
            $response = ['response' => false,
                'message' => Lang::get('admins.license_already_exist')];
        } else {
            $license = License::saveLicense($validatedData);
            $userId = Auth::user()->id;
            Log::info("UserId: " . $userId);
            Log::info("LicenseId: " . $license->id);
            if ($license->role === config('constants.TEACHER')) {
                // event(new CreateLicenseDefaultFolderEvent(
                // $userId, $license->id));
            }
            $response = ['response' => true,
                'message' => Lang::get('admins.license_created')];
        }
        DB::commit();
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\LicenseRequest  $request
     * @param  \App\Models\License $license
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LicenseRequest $request, License $license)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        if (License::where($validatedData)->first()) {
            $response = ['response' => false,
                'message' => Lang::get('admins.license_already_exist')];
        } else {
            $license->updateLicense($validatedData);
            $response = ['response' => true,
                'message' => Lang::get('admins.license_updated')];
        }
        DB::commit();
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\License  $license
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(License $license)
    {
        $license->delete();
        return response()->json(['message' =>
            Lang::get('admins.license_deleted')]);
    }
}
