<?php

namespace App\Http\Controllers;

use App\Events\AssignInstitutionToCourseEvent;
use App\Events\AssignSubjectToCourseEvent;
use App\Http\Requests\CourseRequest;
use App\Models\Course;
use App\Models\Institution;
use App\Models\Subject;
use App\Services\YajraDataTableService;
use DB;
use Illuminate\Http\Request;
use Lang;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('courses.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData()
    {
        return YajraDataTableService::courses();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create')->with([
            'institutions' => Institution::getAll()->get(),
            'subjects' => Subject::getAll(config('constants.SUBJECT'))
                ->getSubjectWithLearningGoalCount()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CourseRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $course = Course::saveCourse($validatedData['name']);
        static::saveInstitutionAndSubject($course,
            $validatedData['institutionIds'], $validatedData['subjects'],
            config('constants.CREATE_REQUEST'));
        DB::commit();
        session()->flash('status', Lang::get('admins.course_created'));
        return response()->json(['route' => route('courses.index')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug Course slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $course = Course::findBySlug($slug)->first();
        return view('courses.edit')->with([
            'institutions' => Institution::getAll()->get(),
            'subjects' => Subject::getAll(config('constants.SUBJECT'))
                ->getSubjectWithLearningGoalCount()->get(),
            'courseSubjects' => Course::getSubjectsWithLearningGoalCount($slug)
                ->get()->toArray(),
            'course' => $course->toArray(),
            'courseInstitutions' => $course->institutions()->pluck('id')
                ->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\CourseRequest  $request
     * @param  string  $slug Course slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CourseRequest $request, $slug)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $course = Course::findBySlug($slug)->first();
        $course->updateCourse($validatedData['name']);
        static::saveInstitutionAndSubject($course,
            $validatedData['institutionIds'], $validatedData['subjects'],
            config('constants.UPDATE_REQUEST'));
        DB::commit();
        session()->flash('status', Lang::get('admins.course_updated'));
        return response()->json(['route' => route('courses.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug Course slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($slug)
    {
        DB::beginTransaction();
        Course::findBySlug($slug)->first()->delete();
        DB::commit();
        session()->flash('status', Lang::get('admins.course_deleted'));
        return response()->json(['route' => route('courses.index')]);
    }

    /**
     * Copy the selected course.
     *
     * @param  string  $slug Course slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function copy($slug)
    {
        DB::beginTransaction();
        $course = Course::findBySlug($slug)->first();
        $newCourse = Course::saveCourse($course->name);
        static::saveInstitutionAndSubject($newCourse,
            $course->institutions()->pluck('id'),
            $course->subjects()->select('subjects.id as subjectId',
                'course_subjects.order')->get()->toArray(),
            config('constants.CREATE_REQUEST'));
        DB::commit();
        session()->flash('status', Lang::get('admins.course_created'));
        return response()->json(['route' => route('courses.index')]);
    }

    /**
     * Copy the selected course.
     *
     * @param  App\Models\Course $course
     * @param  array $institutionIds Institution ids
     * @param  array $subjects Subject data
     * @return void
     */
    private static function saveInstitutionAndSubject(
        $course,
        $institutionIds,
        $subjects,
        $type
    ) {
        event(new AssignInstitutionToCourseEvent($course, $institutionIds));
        event(new AssignSubjectToCourseEvent($course, $subjects, $type));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string  $slug Course slug
     * @return \Illuminate\Http\Response
     */
    public function institutions($slug)
    {
        return view('licenses.institutions', [
            'institutions' => Course::findBySlug($slug)->first()->institutions
                ->toArray(),
        ]);
    }
}
