<?php

namespace App\Http\Controllers;

use App\Events\AddActivityEvent;
use App\Events\AssignQuestionAnswerToLearningGoalTestEvent;
use App\Events\CalculateLearningGoalScoreEvent;
use App\Events\CreateLearningGoalTestEvent;
use App\Exceptions\NoQuestionFoundException;
use App\Http\Requests\StudentLearningGoalRequest;
use App\Http\Requests\StudentQuestionAnswerRequest;
use App\Models\LearningGoal;
use App\Models\LearningGoalTest;
use App\Models\License;
use App\Models\QuestionAnswer;
use App\Models\Subject;
use App\Repositories\UserRepository;
use App\Services\HintService;
use App\Services\PracticeTestService;
use App\Services\QuestionAnswerRenderService;
use App\Services\StudentService;
use Carbon\Carbon;
use DB;
use Illuminate\Pipeline\Pipeline;
use Lang;

class StudentController extends Controller
{
    /**
     * Show learning goals of a subject for the user
     *
     * @param App\Models\license License license
     * @param string $subjectSlug Subject slug
     * @return \Illuminate\Http\Response
     */
    public function subjectLearningGoals(License $license, $subjectSlug)
    {
        return view('users.students.learning-goals.index')->with([
            'learningGoals' => request()->studentLearningGoals->toArray(),
            'subjectName' => request()->subjectName,
            'licenseId' => $license->id,
            'subjectSlug' => $subjectSlug,
            'practiceTestsCount' => PracticeTestService::subjectPracticeTests(
                UserRepository::studentPracticeTests(auth()->user(), $license)->get(),
                Subject::findOrFail(request()->subjectId))->count(),
        ]);
    }

    /**
     * Update status of student learning goal to started
     * @param  App\Http\Requests\StudentLearningGoalRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function startStudentLearningGoal(StudentLearningGoalRequest $request)
    {
        $validatedData = $request->validated();
        UserRepository::startStudentLearningGoal(auth()->user()->id,
            $validatedData['license'], $validatedData['subjectId'],
            $validatedData['learningGoalId']);
        return response()->json([]);
    }

    /**
     * Show learning goal's content
     *
     * @param App\Models\license License license
     * @param string $subjectSlug Subject slug
     * @param string $learningGoalSlug LearningGoal slug
     * @return \Illuminate\Http\Response
     */
    public function showLearningGoal(
        License $license,
        $subjectSlug,
        $learningGoalSlug
    ) {
        $theory_url = LearningGoal::
            findBySlug($learningGoalSlug)->first()->theory_url;
        $testRoute = null;
        $studentLearningGoal = request()->studentLearningGoal;
        if ($learningGoalTestId =
            $studentLearningGoal->learning_goal_test_id) {
            // LearningGoalTest Route
            $testRoute = route('students.learning-goals.test',
                [$license->id, $subjectSlug, $learningGoalSlug]);
        } else {
            // Introduction question route
            $testRoute = route('students.learning-goals.introductionTest',
                [$license->id, $subjectSlug, $learningGoalSlug]);
        }
        event(new AddActivityEvent(auth()->user(), $license->id,
            $studentLearningGoal->subject_id,
            $studentLearningGoal->learning_goal_id));
        return view('users.students.learning-goals.show')->with([
            'testRoute' => $testRoute,
            'learningGoalSlug' => $learningGoalSlug,
            'theory_url' => $theory_url,
        ]);
    }

    /**
     * Show introduction question
     *
     * @param App\Models\license License license
     * @param string $subjectSlug Subject slug
     * @param string $learningGoalSlug LearningGoal slug
     * @return \Illuminate\Http\Response
     */
    public function learningGoalTest(
        License $license,
        $subjectSlug,
        $learningGoalSlug
    ) {
        if (request()->studentLearningGoal->learning_goal_test_id) {
            $response = view('users.students.learning-goals.tests.show');
        } else {
            $response = view('users.students.learning-goals.tests.introduction');
        }
        return $response;
    }

    /**
     * Get learning goal test question
     *
     * @param App\Models\license License license
     * @param string $subjectSlug Subject slug
     * @param string $learningGoalSlug LearningGoal slug
     * @return \Illuminate\Http\Response
     */
    public function learningGoalTestQuestion(
        License $license,
        $subjectSlug,
        $learningGoalSlug,
        $questionAnswerId = null
    ) {
        $studentLearningGoal = request()->studentLearningGoal;
        $learningGoal = LearningGoal::findOrFail(
            request()->studentLearningGoal->learning_goal_id);
        if ($learningGoalTestId
            = $studentLearningGoal->learning_goal_test_id) {
            $currentScore = $studentLearningGoal->current_score;
            $questionLevel = StudentService::getBaesLevelByScore(
                $currentScore);
            $question = app(Pipeline::class)
                ->send($learningGoal->getRandomQuestion($learningGoalTestId,
                    $questionLevel['complexity']))
                ->through(QuestionAnswerRenderService::class)
                ->then(function ($question) {
                    return $question;
                });
            if (!$question) {
                throw new NoQuestionFoundException(Lang::get(
                    'users.no_question_found'), 1);
            }
            $response = [];
            // Finish Learning goal logic
            if ($currentScore >= config('constants.LEARNING_GOAL_FINISHING_SCORE')
                && ($currentScore > $studentLearningGoal->finished_score)) {
                if ($studentLearningGoal->status !== config('constants.FINISHED')) {
                    $response['eligibility'] = true;
                    $response['message'] = Lang::get('students.learning_goal_completed');
                } else {
                    // Improve Learning goal score
                    $response['improve'] = true;
                    $response['message'] = Lang::get('students.baes_score_improved');
                }
                $getNextLearningGoalSlug = UserRepository::getNextLearningGoalSlug(
                    auth()->user()->id, $license->id, $studentLearningGoal->subject_id,
                    $studentLearningGoal->order);
                if ($getNextLearningGoalSlug) {
                    $response['nextLearningGoalRoute'] =
                        route('students.learning-goals.show', [$license,
                        $subjectSlug, $getNextLearningGoalSlug]);
                }
                $response['learningGoalOverviewRoute'] = route('students.subjects.learning-goals',
                    [$license->id, $subjectSlug]);
            }
            // Assign Question to Learning goal test
            event(new AssignQuestionAnswerToLearningGoalTestEvent(
                $learningGoalTestId, $question->id));
            $questionView = view($question->type ===
                strtolower(config('constants.MCQ_TYPE')) ? 'includes.mcq-questions'
                : 'includes.direct-questions', array_merge($response,
                    ['question' => $question->toArray(),
                        'answer' => $question->answer,
                        'questionLevel' => $questionLevel['level'],
                        'learningGoalTestId' => $learningGoalTestId]
                ));
        } else {
            $question = app(Pipeline::class)
                ->send($learningGoal->getRandomIntroductionQuestion($questionAnswerId))
                ->through(QuestionAnswerRenderService::class)
                ->then(function ($question) {
                    return $question;
                });
            if ($question) {
                $questionView = view('includes.mcq-questions', [
                    'question' => $question->toArray(),
                    'answer' => $question->answer,
                ]);
            } else {
                throw new NoQuestionFoundException(Lang::get(
                    'users.no_question_found'), 1);
            }
        }
        event(new AddActivityEvent(auth()->user(), $license->id,
            $studentLearningGoal->subject_id,
            $studentLearningGoal->learning_goal_id));
        return $questionView;
    }

    /**
     * Check question answer of a student
     * @param  App\Http\Requests\StudentQuestionAnswerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkQuestionAnswer(StudentQuestionAnswerRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $question = QuestionAnswer::findOrFail($validatedData['questionId']);
        $correctAnswer = $question->checkAnswer($validatedData['answer']);
        $response = [];
        $studentLearningGoal = request()->studentLearningGoal;
        if ($learningGoalTestId
            = $studentLearningGoal->learning_goal_test_id) {
            $learningGoalTest = LearningGoalTest::findOrFail(
                $learningGoalTestId);
            //Update studentLearningGoalTest
            $attempt = $learningGoalTest->updatStudentLearningGoalTest(
                $validatedData['questionId'], $validatedData['answer'],
                $correctAnswer);
            // Get question hint
            $hints = $question->hint->hint;
            if ($correctAnswer ||
                $attempt == config('constants.FOURTH_ATTEMPT')) {
                // Calculate current score of studentLearningGoal
                event(new CalculateLearningGoalScoreEvent(
                    $studentLearningGoal));
                $response['hint'] = HintService::getHint(
                    $hints, config('constants.FOURTH_ATTEMPT'));
            } elseif ($attempt !== config('constants.FIRST_ATTEMPT')) {
                $response['hint'] = HintService::getHint($hints, $attempt);
            }
            // Get current score
            $currentScore = $currentLearningGoal =
            UserRepository::getStudentLearningGoal(
                auth()->user()->id, $studentLearningGoal->license_id,
                $studentLearningGoal->subject_id,
                $studentLearningGoal->learning_goal_id);
            $currentScore = $currentScore->first()->current_score;
            // Finish Learning goal logic
            if ($currentScore >= config('constants.LEARNING_GOAL_FINISHING_SCORE')
                && $studentLearningGoal->current_score >= config('constants.LEARNING_GOAL_FINISHING_SCORE')) {
                if ($correctAnswer && $attempt <= config('constants.SECOND_ATTEMPT')) {
                    if ($studentLearningGoal->status !== config('constants.FINISHED')
                        && $attempt <= config('constants.SECOND_ATTEMPT')) {
                        $response['eligibility'] = true;
                        // Finsh the learning goal
                        $currentLearningGoal->update([
                            'status' => config('constants.FINISHED'),
                            'finished_score' => $currentScore,
                            'updated_at' => Carbon::now()]);
                    } elseif ($currentScore > $studentLearningGoal->finished_score) {
                        // Improve Learning goal score
                        $currentLearningGoal->update(['finished_score' => $currentScore,
                            'updated_at' => Carbon::now()]);
                        $response['improve'] = true;
                        $getNextLearningGoalSlug = UserRepository::getNextLearningGoalSlug(
                            auth()->user()->id, $studentLearningGoal->license_id,
                            $studentLearningGoal->subject_id, $studentLearningGoal->order);
                        $nextLearningGoalRoute = null;
                        if ($getNextLearningGoalSlug) {
                            $nextLearningGoalRoute = route('students.learning-goals.show',
                                [$studentLearningGoal->license_id, $request->subjectSlug,
                                    $getNextLearningGoalSlug]);
                        }
                        $response['finishedLearningGoalModalData'] =
                        view('includes.learning-goal-finished-modal',
                            ['nextLearningGoalRoute' => $nextLearningGoalRoute,
                                'message' => Lang::get('students.baes_score_improved'),
                                'learningGoalOverviewRoute' => route('students.subjects.learning-goals',
                                    [$studentLearningGoal->license_id, $validatedData['subjectSlug']])])
                            ->render();
                    }
                } elseif (!$correctAnswer
                    && $attempt < config('constants.SECOND_ATTEMPT')) {
                    $response['eligibility'] = true;
                }
            }
            // Update scores in front end
            if ($correctAnswer ||
                $attempt == config('constants.FOURTH_ATTEMPT')) {
                $response['learningGoalTestScoreDiv'] = view(
                    'includes.learning-goal-test-score', [
                        'currentScore' => $currentScore,
                        'lastFiveScores' =>
                        StudentService::lastFiveScores($currentLearningGoal->first()),
                    ])->render();
            }
            $response['attempt'] = $attempt;
        } elseif ($validatedData['attempt'] <= config('constants.SECOND_ATTEMPT')) {
            $response['attempt'] = $validatedData['attempt'];
            if ($correctAnswer) {
                event(new CreateLearningGoalTestEvent(
                    request()->studentLearningGoal));
            }
        }
        DB::commit();
        return response()->json(array_merge($response,
            ['correctAnswer' => $correctAnswer]));
    }
}
