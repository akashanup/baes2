<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\License;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\DashboardService;
use App\Services\InstitutionService;
use App\Services\LicenseService;
use App\Services\UserLicensesService;
use App\Services\YajraDataTableService;
use DB;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Storage;
use Lang;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $institution Institution slug
     * @param string $course Course slug
     * @param integer $licenseId License id
     * @return \Illuminate\Http\Response
     */
    public function index($institution, $course, $licenseId)
    {
        return view('users.index')->with([
            'courseSlug' => $course,
            'institutionSlug' => $institution,
            'licenseId' => $licenseId,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $institution Institution slug
     * @param string $course Course slug
     * @param integer $licenseId License id
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData($institution, $course, $licenseId)
    {
        return YajraDataTableService::users($institution, $course, $licenseId);
    }

    /**
     * Change status of a user
     *
     * @param App\Models\User $user User
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(User $user)
    {
        $changedstatus = $user->status == config('constants.ACTIVE')
        ? config('constants.INACTIVE') : config('constants.ACTIVE');
        $user->updateUser(['status' => $changedstatus]);
        return response()->json(['message' =>
            Lang::get('acl.user_updated')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param App\Models\User $user User
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json(['message' =>
            Lang::get('acl.user_deleted')]);
    }

    /**
     * Edit the specified resource from storage.
     *
     * @param string $userSlug User slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($userSlug)
    {
        $user = auth()->user()->toArray();
        $user['image'] = Storage::url($user['image']);
        session(['previous_url' => request()->headers->get('referer')]);
        return view('users.edit')->with([
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @param string $userSlug User slug
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $userSlug)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        auth()->user()->updateUser($validatedData);
        DB::commit();
        session()->flash('status', Lang::get('acl.user_updated'));
        $route = null;
        if (session('previous_url')) {
            $route = session('previous_url');
            session()->forget('previous_url');
        } else {
            $route = DashboardService::dashboardRoute();
        }
        return redirect($route);
    }

    /**
     * Reset the password of user.
     *
     * @param App\Models\User $user User
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(User $user)
    {
        $user->resetPassword();
        return response()->json(['message' =>
            Lang::get('admins.reset_password_link_message')]);
    }

    /**
     * Show all licenses of a user at admins end.
     *
     * @param App\Models\User $user User
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllLicenses(User $user)
    {
        return response()->json(['licenses' =>
            app(Pipeline::class)
                ->send(UserRepository::userLicenses($user)->get())
                ->through(LicenseService::class)
                ->via('render')
                ->then(function ($licenses) {
                    return $licenses;
                }),
        ]);
    }

    /**
     * Change license of a user from admins end.
     *
     * @param App\Models\User $user User
     * @param App\Models\License $user License
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeLicense(User $user, License $license)
    {
        $user->changeLicenseStatus($license->id);
        return response()->json(['message' =>
            Lang::get('admins.license_status_updated')]);
    }

    /**
     * Show licenses for the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function licenses()
    {
        return view('users.licenses.index')->with(['licenses' =>
            app(Pipeline::class)
                ->send(License::userLicenses(auth()->user()->id)
                        ->where('user_licenses.is_associated', config('constants.NO'))
                        ->get())
                ->through(InstitutionService::class, UserLicensesService::class)
                ->then(function ($licenses) {
                    return $licenses->toArray();
                }),
        ]);
    }

    /**
     * Show available licenses for the user.
     *
     * @param string $code License code
     * @return \Illuminate\Http\Response
     */
    public function availableLicenses($code = null)
    {
        $userId = auth()->user()->id;
        return view('users.licenses.available')->with([
            'licenses' => app(Pipeline::class)
                ->send(License::allLicenses($userId, $code)
                        ->diff(License::userLicenses($userId)->get()))
                ->through(InstitutionService::class)
                ->then(function ($licenses) {
                    return $licenses->toArray();
                }),
            'code' => $code,
        ]);
    }

    /**
     * Show available subjects for the user of the course
     *
     * @param App\Models\license License license
     * @param string $courseSlug Course slug
     * @return \Illuminate\Http\Response
     */
    public function courses(License $license, $courseSlug)
    {
        return DashboardService::dashboardData($license);
    }

    /**
     * Show an available license details for the user
     *
     * @param App\Models\license License license
     * @return \Illuminate\Http\Response
     */
    public function showLicense(License $license)
    {
        return view('users.licenses.show')->with([
            'license' => app(Pipeline::class)
                ->send(License::coursesInstitutionsSubjects()
                        ->getLicenses()->where('licenses.id', $license->id)
                        ->get())
                ->through([InstitutionService::class, LicenseService::class])
                ->then(function ($license) {
                    return $license->first()->toArray();
                }),
        ]);
    }
}
