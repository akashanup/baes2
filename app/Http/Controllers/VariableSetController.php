<?php

namespace App\Http\Controllers;

use App\Jobs\GenerateQuestions;
use App\Jobs\ImportVariableSet;
use App\Models\QuestionAnswer;
use App\Models\QuestionFeedback;
use App\Models\QuestionTemplate;
use App\Models\TemplateVariable;
use App\Services\ImportVariableSetService;
use App\Services\TemplateUploadService;
use App\Services\TemplateUtilityService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Lang;
use Storage;

class VariableSetController extends Controller
{

    const TYPE_CURRENCY = 'currency';
    const TYPE_NUMBER = 'number';
    const TYPE_PERCENTAGE = 'percentage';

    /**
     * Display all variable set of question template.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function listVariableSet($learningGoalSlug, $id)
    {
        $templateVariables = TemplateVariable::where
        ('question_template_id', $id)
            ->where('status_edit', config('constants.NO'))
            ->get();
        if (!$templateVariables->count()) {
            return redirect()->back()->withErrors(Lang::get('admins.active_variable_set_not_found'));
        }
        $templateVariablesArray = array();
        foreach ($templateVariables as $set) {
            $templateVariablesArray[$set->id] = json_decode($set->values, true);
        }

        $variableKeys = json_decode($templateVariables[0]->values, true);
        $variableKeys = array_keys($variableKeys);
        return view('variable-sets.show-variable-set')->with(
            [
                'templateVariablesArray' => $templateVariablesArray,
                'variableKeys' => $variableKeys,
                'learningGoalSlug' => $learningGoalSlug,
                'question_template_id' => $id,
            ]
        );
    }

    /**
     * Upload all varaibel set for learning goal.
     * @param int $slug
     * @return \Illuminate\Http\Response
     */

    public function uploadVariableSet($slug)
    {
        $this->validate(
            request(),
            ['fileUpload' => 'required|mimes:xlsx,ods']
        );
        try {
            $file = request()->file('fileUpload');
            $fileName = str_slug(Lang::get('admins.export_data') . '_' . Carbon::now(), '-');
            $filePath = Storage::put('', $file, config('constants.S3_IMAGE_VISIBLE'));
            // $importSet = new ImportVariableSet($slug, $filePath,
            //     $file->getClientOriginalExtension(), auth()->user()->id);
            // $importSet->handle();
            dispatch(new ImportVariableSet($slug, $filePath,
                $file->getClientOriginalExtension(), auth()->user()->id));
            session()->flash('status', Lang::get('admins.variables_uploaded_status_mail'));
            return redirect()->route('learning-goals.index');
        } catch (Exception $e) {
            return redirect()->route('learning-goals.index')->withErrors(Lang::get('languages.final_catch_response'));
        } //end try
    }

    /**
     * Edit selected variable set
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function editSelectedVariableSet($id)
    {
        $variableSet = TemplateVariable::find($id);
        $variableKeys = json_decode($variableSet->values, true);
        return view('variable-sets.edit-selected-variable-set')->with([
            'variableKeys' => $variableKeys,
            'id' => $id,
        ]);
    }

    /**
     * Update selected variable set
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateVariableSet(Request $request, $id)
    {
        $this->validate(request(), [
            'id' => 'required|numeric',
            'ANTWOORD' => 'required',
        ]);
        DB::begintransaction();
        // Fetching all input from request to array
        $variables = $request->input();

        // saving id for later use
        $id = $variables['id'];

        // Saving correct answer and modifying it
        $correct_answer = $variables['ANTWOORD'];
        preg_match('/([0-9\.\, \%])+/', $correct_answer, $correct_answer);
        $correct_answer = trim(preg_replace('/([ ])/', '', $correct_answer[0]), ' ');
        $correct_answer = str_replace('.', '', $correct_answer);
        $correct_answer = str_replace(',', '.', $correct_answer);

        // Removing unwanted values from array
        unset($variables['_token']);
        unset($variables['id']);
        // Modifying array data
        foreach ($variables as $key => $value) {
            $variables[$key] = preg_replace('([\%])', '\\%', $value);
        }

        // Converting array to JSON
        $variables_json = json_encode($variables);
        // Fetching old variable set details
        $old_variable_set = TemplateVariable::find($id);

        // Getting question related to old variable set
        $old_questions = QuestionAnswer::where
        ('template_variable_id', $old_variable_set->id)
            ->where('question_template_id',
                $old_variable_set->question_template_id)
            ->where('status', config('constants.ACTIVE'))
            ->get();
        // If no variable set is found then de-activating all question with template id and creating again so that we could have variable_set_id present in question_answers table.
        if (count($old_questions) == 0) {
            // Setting all old question to inactive of that question template
            QuestionAnswer::where('question_template_id',
                $old_variable_set->question_template_id)
                ->where('status', config('constants.ACTIVE'))
                ->update(['status' => config('constants.INACTIVE')]);
            // Creating/copying new template
            $new_template = QuestionTemplate::where('id',
                $old_variable_set->question_template_id)
                ->get();
            $new_temp = $new_template[0]->replicate();
            $new_temp->save();

            // Deactivating old template
            QuestionTemplate::where('id',
                $old_variable_set->question_template_id)
                ->update(['status' => config('constants.INACTIVE')]);

            // Changing edit status for changed variable set
            TemplateVariable::where('id', '=', $id)
                ->update(['status_edit' => config('constants.YES')]);

            // Creating copy of all variable set which are unchanged for templates
            $template_variable_set = TemplateVariable::where('question_template_id',
                $old_variable_set->question_template_id)
                ->where('status_edit', config('constants.NO'))
                ->get();
            // Update the question_template_id
            foreach ($template_variable_set as $set) {
                $set_copy = $set->replicate();
                $set_copy->question_template_id = $new_temp->id;
                $set_copy->status = config('constants.PENDING');
                $set_copy->save();
                $set_copy->save();
            }
        } else {
            // Setting old question to inactive
            QuestionAnswer::where('id', $old_questions[0]->id)
                ->update(['status' => config('constants.INACTIVE')]);
            // Changing edit status for changed variable set
            TemplateVariable::where('id', $id)
                ->update(['status_edit' => config('constants.YES')]);
            // Inserting modified set
            $insert_new_set = new TemplateVariable();
            $insert_new_set->values = $variables_json;
            $insert_new_set->correct_answer = $correct_answer;
            $insert_new_set->question_template_id = $old_questions[0]->question_template_id;
            $insert_new_set->save();
        }
        dispatch(new GenerateQuestions());
        DB::commit();
        session()->flash('status', Lang::get('admins.variable_set_updated'));
        $response = ['status' => config('constants.SUCCESS'),
            'route' => route('learning-goals.index')];
        return response()->json($response);
    }

    /**
     * Function to store direct question template variable values and set template to active.
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function storeDirectVariableSet(Request $request)
    {
        DB::beginTransaction();
        for ($set_number = 1; $set_number <= $request->set_number; $set_number++) {
            $variable_set_data = array();
            foreach (session('variable_array') as $variable) {
                $type = 'type' . $variable . $set_number;
                $rounding = 'rounding' . $variable . $set_number;
                $name = $variable . $set_number;
                $value = self::getModifiedValue($request->$type,
                    $request->$rounding, $request->$name);
                $variable_set_data[$variable] = $value;
            }
            $type = 'typeanswer' . $set_number;
            $rounding = 'roundinganswer' . $set_number;
            $name = 'answer' . $set_number;
            $value = self::getModifiedValue($request->$type,
                $request->$rounding, $request->$name);

            $variable_set_data['ANTWOORD'] = $value;

            $variable_set_data_json = json_encode($variable_set_data);

            if ($request->$type == ImportVariableSetService::TYPE_CURRENCY || $request->$type == ImportVariableSetService::TYPE_NUMBER) {
                $correct_answer = round($request->$name, $request->$rounding);
            } elseif ($request->$type == ImportVariableSetService::TYPE_PERCENTAGE) {
                $correct_answer = number_format($request->$name, $request->$rounding, '.', ',') . "%";
            } else {
                $correct_answer = $request->$name;
            }

            $insert_variable = new TemplateVariable();
            $insert_variable->values = $variable_set_data_json;
            $insert_variable->correct_answer = $correct_answer;
            $insert_variable->question_template_id = $request->id;
            $insert_variable->save();
        }
        // Setting template to active
        QuestionTemplate::where('id', $request->id)
            ->update(['status' => config('constants.ACTIVE')]);
        DB::commit();

        dispatch(new GenerateQuestions());
        session()->flash('status', 'Direct Template Added');
        return redirect()->route('learning-goals.index');
    }

    /**
     * Function to modify value.
     * @param Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function getModifiedValue($type, $rounding, $name)
    {
        if ($type == static::TYPE_CURRENCY) {
            $value = TemplateUtilityService::returnSymbol()
            . number_format($name, $rounding, ',', '.');
        } elseif ($type == static::TYPE_PERCENTAGE) {
            $value = number_format($name, $rounding, ',', '.') . '\\%';
        } elseif ($type == static::TYPE_NUMBER) {
            $value = number_format($name, $rounding, ',', '.');
        } else {
            $value = $name;
        }
        return $value;
    }

    /**
     * Download variable set of a question template in ods format
     *
     * @param  integer $question_template_id
     * @return \Illuminate\Http\Response
     */
    public function downloadQuestionVariableSet($question_template_id)
    {
        DB::begintransaction();
        $templateVariables = TemplateVariable::
            where('question_template_id', $question_template_id)
            ->where('status_edit', config('constants.NO'))
            ->get();
        if ($templateVariables->count()) {
            TemplateUploadService::exportVariableSet($templateVariables, $question_template_id);
        }
        DB::commit();
    }

    /**
     * upload variable set of a question template in ods format
     *
     * @param  integer $question_template_id
     * @return \Illuminate\Http\Response
     */
    public function uploadQuestionVariableSet(
        Request $request,
        $question_template_id
    ) {
        $this->validate(
            request(),
            ['file_upload' => 'required|mimes:xlsx,ods']
        );
        DB::beginTransaction();
        $questionTemplate = QuestionTemplate::find($question_template_id);
        $questionFeedbacks = QuestionFeedback::
            where('question_template_id', $question_template_id)
            ->get();
        $template_variable = ImportVariableSetService::
            getTemplateVariables($questionTemplate->template,
            $questionFeedbacks[0], $questionFeedbacks[1],
            $questionFeedbacks[2]);

        $newTemplateVariables = ImportVariableSetService::fileUpload($question_template_id, request('file_upload'), $template_variable, ImportVariableSetService::EDIT_MODE);
        if (!$newTemplateVariables) {
            DB::rollback();
            return redirect()->route('learning-goals.index')
                ->withErrors(Lang::get('admins.question_variable_column_mismatch'));
        }
        // Creating/copying new template
        $newQuestionTemplate = $questionTemplate->replicate();
        $newQuestionTemplate->save();
        // Copying the feedback/hint and mapping with new id
        foreach ($questionFeedbacks as $questionFeedback) {
            $copy = $questionFeedback->replicate();
            $copy->question_template_id = $newQuestionTemplate->id;
            $copy->save();
        }
        // Deactivate the current question template
        $questionTemplate->update(['status' => config('constants.INACTIVE')]);
        // Deactivate all questions of the current question template
        $questionAnswers = $questionTemplate->questionAnswers()
            ->update(['status' => config('constants.INACTIVE')]);

        // Store new template variables
        foreach ($newTemplateVariables as $key => $newTemplateVariable) {
            // Inserting modified set
            $insert_new_set = new TemplateVariable();
            $insert_new_set->values = $newTemplateVariable['values'];
            $insert_new_set->correct_answer = $newTemplateVariable['correct_answer'];
            $insert_new_set->question_template_id = $newQuestionTemplate->id;
            $insert_new_set->save();
        }
        DB::commit();
        dispatch(new GenerateQuestions());
        session()->flash('status',
            Lang::get('admins.variables_uploaded_successfully'));
        return redirect()->route
            ('learning-goals.index');
    }
}
