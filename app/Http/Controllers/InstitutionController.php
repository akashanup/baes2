<?php

namespace App\Http\Controllers;

use App\Http\Requests\InstitutionRequest;
use App\Models\Institution;
use App\Services\YajraDataTableService;
use Illuminate\Http\Request;
use Lang;

class InstitutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $institution Institution slug
     * @param string $course Course slug
     * @return \Illuminate\Http\Response
     */
    public function index($institution, $course)
    {
        return view('institutions.index')->with([
            'courseSlug' => $course,
            'institutionSlug' => $institution,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $institution Institution slug
     * @param string $course Course slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData($institution, $course)
    {
        return YajraDataTableService::institutions($institution, $course);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('institutions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\InstitutionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstitutionRequest $request)
    {
        $validatedData = $request->validated();
        Institution::saveInstitution($validatedData);
        session()->flash('status', Lang::get('admins.institution_added'));
        return redirect()->route('institutions.index', [
            config('constants.ALL'), config('constants.ALL'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\InstitutionRequest  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(InstitutionRequest $request, $slug)
    {
        $validatedData = $request->validated();
        $institution = Institution::findBySlug($slug)->first();
        $institution->updateInstitution($validatedData);
        session()->flash('status', Lang::get('admins.institution_updated'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($slug)
    {
        $response = null;
        if ($slug === config('constants.BAES_INSTITUTION_SLUG')) {
            $response = ['response' => false, 'message' =>
                Lang::get('admins.baes_cannot_be_deleted')];
        } else {
            Institution::findBySlug($slug)->first()->delete();
            $response = ['response' => true, 'message' =>
                Lang::get('admins.institution_deleted')];
        }

        return response()->json($response);
    }
}
