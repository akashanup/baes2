<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Lang;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
     */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        $user = User::findByEmail($request->email)->first();
        $thisResponse = null;
        if ($user) {
            if ($user->status == User::ACTIVE) {
                // We will send the password reset link to this user. Once we have attempted
                // to send the link, we will examine the response then see the message we
                // need to show to the user. Finally, we'll send out a proper response.
                $response = $this->broker()->sendResetLink(
                    $request->only('email')
                );

                if ($response == Password::RESET_LINK_SENT) {
                    $thisResponse = $this->sendResetLinkResponse($response);
                } else {
                    $thisResponse = $this->sendResetLinkFailedResponse($request, $response);
                }
            } else {
                $thisResponse = redirect()->back()->withErrors(Lang::get('users.inactive_user'));
            }
        } else {
            $thisResponse = redirect()->back()->withErrors(Lang::get('users.user_not_found'));
        }
        return $thisResponse;
    }
}
