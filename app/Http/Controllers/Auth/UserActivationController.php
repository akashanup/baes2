<?php

namespace App\Http\Controllers\Auth;

use App\Events\AddActivityEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordRequest;
use App\Models\User;
use App\Services\DashboardService;
use Auth;
use Illuminate\Http\Request;
use Lang;

class UserActivationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function activate($token)
    {
        return view('auth.passwords.set')->with([
            'token' => $token,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\PasswordRequest $request
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function savePassword(PasswordRequest $request, $token)
    {
        $validatedData = $request->validated();
        $user = $request->user;
        if ($user->email === $validatedData['email']) {
            $user->updateUser([
                'password' => bcrypt($validatedData['password']),
                'status' => config('constants.ACTIVE'),
                'activation_token' => null,
            ]);
            Auth::login($user);
            event(new AddActivityEvent(auth()->user(), null, null));
            $route = redirect(DashboardService::dashboardRoute());
        } else {
            $route = redirect()->route('login')->withErrors(
                Lang::get('users.invalid_token'));
        }
        return $route;
    }
}
