<?php

namespace App\Http\Controllers;

use App\Events\AssignLearningGoalToSubjectEvent;
use App\Events\UpdateStudentLearningGoalEvent;
use App\Http\Requests\SubjectRequest;
use App\Models\Subject;
use App\Services\YajraDataTableService;
use DB;
use Illuminate\Http\Request;
use Lang;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subjects.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData()
    {
        return YajraDataTableService::subjects();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subjects.create')->with([
            'curriculums' => Subject::getAll(config('constants.CURRICULUM'))
                ->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\SubjectRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SubjectRequest $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        event(new AssignLearningGoalToSubjectEvent(
            Subject::saveSubject($validatedData, config('constants.SUBJECT')),
            $validatedData['learningGoalIds']));
        DB::commit();
        session()->flash('status', Lang::get('admins.subject_created'));
        return response()->json(['route' => route('subjects.index')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug Subject slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $subject = Subject::findBySlug($slug)->first();
        $curriculum = Subject::find($subject['curriculum_id']);
        $existingLearningGoals = $subject->learningGoals()
            ->orderBy('subject_learning_goals.order')->get();
        return view('subjects.edit')->with([
            'curriculums' => Subject::getAll(config('constants.CURRICULUM'))
                ->get(),
            'subject' => $subject,
            'allLearningGoals' => $curriculum ? $curriculum->learningGoals
                ->diff($existingLearningGoals)->toArray() : [],
            'existingLearningGoalIds' => $existingLearningGoals
                ->pluck('id')->toArray(),
            'existingLearningGoals' => $existingLearningGoals->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SubjectRequest  $request
     * @param  string  $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SubjectRequest $request, $slug)
    {
        DB::beginTransaction();
        $validatedData = $request->validated();
        $subject = Subject::findBySlug($slug)->first();
        $subject->updateSubject($validatedData, config('constants.SUBJECT'));
        event(new AssignLearningGoalToSubjectEvent($subject,
            $validatedData['learningGoalIds']));
        event(new UpdateStudentLearningGoalEvent($subject));
        DB::commit();
        session()->flash('status', Lang::get('admins.subject_updated'));
        return response()->json(['route' => route('subjects.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug Subject slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($slug)
    {
        DB::beginTransaction();
        Subject::findBySlug($slug)->first()->delete();
        DB::commit();
        session()->flash('status', Lang::get('admins.subject_deleted'));
        return response()->json(['route' => route('subjects.index')]);
    }
}
