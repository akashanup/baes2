<?php

namespace App\Http\Controllers;

use App\Http\Requests\GradeRequest;
use App\Models\Course;
use App\Models\Grade;
use App\Models\License;
use App\Repositories\UserRepository;
use App\Services\YajraDataTableService;
use DB;
use Illuminate\Http\Request;
use Lang;

class ClassController extends Controller
{
    /**
     * Display student for a group of class (student-management).
     *
     * @return \Illuminate\Http\Response
     */
    public function studentManagement(
        License $license,
        $course,
        $gradeSlug,
        $year
    ) {
        $studentDetails = array();
        $institution = $license->institution;
        $grades = Grade::getInstitutionGrades($institution->id);
        $gradeNames = $grades->pluck('name', 'slug')->toArray();
        $years = $grades->sortBy('year')->pluck('year')->unique()->toArray();
        $courseId = Course::findBySlug($course)->first()->id;

        $students = UserRepository::currentInstitutionCourseStudents
        ($institution->id, $courseId, $gradeSlug, $year)->get()->toArray();
        foreach ($students as $key => $student) {
            $students[$key]['baes_score'] = UserRepository::
                avgBaesScoreByStudent($student['id'])->baes_score;
            $students[$key]['sub_class'] = '';
        }
        return view('teachers.student-management')->with([
            'gradeNames' => $gradeNames,
            'years' => $years,
            'selectedGrade' => $gradeSlug,
            'selectedyear' => $year,
            'courseId' => $courseId,
            'gradesByYearRoute' => route('teachers.gradesByYear',
                [$license->id, $course]),
            'updateStudentsRoute' => route('teachers.updateStudents',
                [$license->id, $course]),
            'getstudentsListRoute' => route('teachers.studentsList',
                [$license->id, $course, $courseId, $year, $gradeSlug]),
            'defaultRoute' => route('teachers.defaultRoute',
                [$license->id, $course]),
        ]);
    }

    /**
     * List all grades.clases of institution.
     * @param App\Models\License $license
     * @param string $course
     * @return \Illuminate\Http\Response
     */
    public function classManagement(License $license, $course)
    {
        $institutionId = $license->institution->id;
        $courseId = $license->course->id;
        $allGrades = [];
        $grades = $license->institution->grades;
        $years = $grades->pluck('year')->unique();
        $grades = $grades->toArray();
        foreach ($grades as $grade) {
            $gradeInstitutionStudents = UserRepository::gradeInstitutionStudents
                ($institutionId, $courseId, $grade['id']);
            $grade['student_count'] = $gradeInstitutionStudents->count();
            array_push($allGrades, $grade);
        }
        return view('teachers.class-management')->with([
            'grades' => $allGrades,
            'years' => $years,
            'licenseId' => $license->id,
            'course' => $course,
            'institutionId' => $institutionId,
            'deleteClassRoute' => route('teachers.deleteClass',
                [$license->id, $course]),
            'updateClassRoute' => route('teachers.updateClass',
                [$license->id, $course]),
            'classListRoute' => route('teachers.classList',
                [$license->id, $course]),
        ]);
    }

    /**
     * Insert class data
     * @param App\Models\License $license
     * @param string $course
     * @param integer $institutionId
     * @return \Illuminate\Http\Response
     */
    public function saveClass(
        GradeRequest $request,
        License $license,
        $course
    ) {
        DB::beginTransaction();
        $institution = $license->institution;
        $data = [
            'name' => $request->className,
            'year' => $request->classYear,
        ];
        Grade::createGrade($institution, $data);
        DB::commit();
        session()->flash('status',
            Lang::get('teachers.class_created')
        );
        $response = ['response' => true];
        return response()->json($response);
    }

    /**
     * Insert class data
     * @param App\Models\License $license
     * @param string $course
     * @param integer $institutionId
     * @return \Illuminate\Http\Response
     */
    public function saveSubClass(
        Request $request,
        License $license,
        $course
    ) {
        $this->validate(request(), [
            'subClassName' => 'required|string|max:50',
            'parentClass' => 'required|integer',
        ]);
        $grade = Grade::find($request->parentClass);
        DB::beginTransaction();
        $data = [
            'name' => $request->subClassName,
            'year' => $grade->year,
            'parent_id' => $request->parentClass,
        ];
        $institution = $license->institution;
        Grade::createGrade($institution, $data);
        DB::commit();
        $response = ['response' => true, 'message' =>
            Lang::get('teachers.class_created')];
        return response()->json($response);
    }

    /**
     * Get all grades of License By Year.
     * @param App\Models\License $license
     * @param string $course
     * @param integer $year
     * @return \Illuminate\Http\Response
     */
    public function gradesByYear(License $license, $course, $year = null)
    {
        $institution = $license->institution;
        $grades = Grade::getGradesByYear($institution->id, $year)
            ->pluck('name', 'slug')->toArray();
        $response = ['response' => true, 'grades' =>
            $grades];
        return response()->json($response);
    }

    /**
     * Gupdate Student Records
     * @param App\Models\License $license
     * @param string $course
     * @param integer $year
     * @return \Illuminate\Http\Response
     */
    public function updateStudents(
        Request $request,
        License $license,
        $course
    ) {
        $institution = $license->institution;
        DB::beginTransaction();
        $studentIds = $request->keys;
        if (in_array('edit-year', $request->fields)) {
            $index = array_search('edit-year', $request->fields);
            $year = $request->values[$index];
            $gradeSlug = $request->values[count($request->values) - 1];
            $grade = Grade::getGradesBySlugYear($gradeSlug, $year);
            if ($grade->count()) {
                UserRepository::updateStudentsGrade($studentIds, $grade->id);
            }
        }

        // Updating Status
        if (in_array('edit-status', $request->fields)) {
            $index = array_search('edit-status', $request->fields);
            if ($request->values[$index] === config('constants.ACTIVE')) {
                $status = config('constants.NO');
            } else {
                $status = config('constants.YES');
            }
            UserRepository::updateStudentsStatus($studentIds, $status);
        }

        // Updating Name
        if (in_array('edit-name', $request->fields)) {
            $index = array_search('edit-name', $request->fields);
            $name = explode(" ", $request->values[$index]);
            $first_name = $name[0];
            $last_name = "";
            for ($join_name = 1; $join_name < count($name); $join_name++) {
                $last_name = $last_name . " " . $name[$join_name];
            }
            UserRepository::updateStudentsName($studentIds, $first_name, $last_name);
        }
        DB::commit();
        session()->flash('status', Lang::get('teachers.student_record_updated'));
        $response = ['response' => true];
        return response()->json($response);
    }

    /**
     * Delete class
     * @param App\Models\License $license
     * @param string $course
     * @param integer $year
     * @return \Illuminate\Http\Response
     */
    public function deleteClass(
        GradeRequest $request,
        License $license,
        $course
    ) {
        DB::beginTransaction();
        if ($request->className === config('constants.DEFAULT_CLASS')) {
            $response = ['response' => flase, 'message' =>
                Lang::get('teachers.cannot_delete')];
        } else {
            Grade::deleteGrade($request->className, $request->classYear);
            $response = ['response' => true, 'message' =>
                Lang::get('teachers.class_deleted')];
        }
        DB::commit();
        return response()->json($response);
    }

    /**
     * Get students list by class and year
     * @param App\Models\License $license
     * @param string $course
     * @param integer $year
     * @param string $class
     * @return \Illuminate\Http\Response
     */
    public function studentsList(
        License $license,
        $course,
        $courseId,
        $year,
        $class
    ) {
        $institution = $license->institution;
        return YajraDataTableService::studentsListTable($institution->id,
            $courseId, $class, $year);
    }

    /**
     * Update class
     * @param App\Models\License $license
     * @param string $course
     * @return \Illuminate\Http\Response
     */
    public function updateClass(
        GradeRequest $request,
        License $license,
        $course
    ) {
        DB::beginTransaction();
        $institutionId = $license->institution->id;
        if (!$request->id) {
            $response = ['response' => false, 'message' =>
                Lang::get('teachers.unable_to_update')];
        } else {
            Grade::updateClassDetails($institutionId, $request->id,
                $request->className, $request->classYear);
            session()->flash('status',
                Lang::get('teachers.class_updated'));
            $response = ['response' => true];
        }
        DB::commit();
        return response()->json($response);
    }

    /**
     * Get class list
     * @param App\Models\License $license
     * @param string $course
     * @param integer $year
     * @return \Illuminate\Http\Response
     */
    public function classList(
        License $license,
        $course,
        $year = null
    ) {
        $courseId = $license->course->id;
        $institutionId = $license->institution->id;
        return YajraDataTableService::classListTable
            ($license->id, $course, $institutionId, $courseId, $year);
    }
}
