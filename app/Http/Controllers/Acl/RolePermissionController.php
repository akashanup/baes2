<?php

namespace App\Http\Controllers\Acl;

use App\Http\Controllers\Controller;
use App\Http\Requests\RolePermissionRequest;
use App\Models\Permission;
use App\Models\Role;
use DB;
use Illuminate\Http\Request;
use Lang;

class RolePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Role $role)
    {
        return view('acl.roles.permissions')->with([
            'role' => $role,
            'rolePermissions' => $role->rolePermissions()->toArray(),
        ]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param  \Illuminate\Http\RolePermissionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(RolePermissionRequest $request, Role $role)
    {
        $validatedData = $request->validated();
        DB::beginTransaction();
        $role->savePermissions(
            array_key_exists('permission_ids', $validatedData) ?
            $validatedData['permission_ids'] : []
        );
        DB::commit();
        session()->flash('status', Lang::get('acl.role_permission_updated'));
        return redirect()->route('roles.permissions.index', $role->id);
    }
}
