<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use Closure;
use Lang;

class ActiveUser extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = null;
        $user = auth()->user();
        if ($user->status == config('constants.ACTIVE')) {
            $response = $next($request);
        } else {
            auth()->logout();
            $response = parent::sendErrorResponse($request,
                Lang::get('authentication.inactive_account'), 401,
                route('login'));
        }
        return $response;
    }
}
