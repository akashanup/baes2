<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Services\DashboardService;
use Closure;
use Lang;

class Permission extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = null;
        if (auth()->check()) {
            $user = auth()->user();
            $permission = $request->route()->getName();
            if ($user->role->name == config('constants.SUPER_ADMIN')
                || $user->hasRolePermission($permission)) {
                $response = $next($request);
            } else {
                $response = parent::sendErrorResponse($request,
                    Lang::get('globals.unauthorized_text'), 403,
                    DashboardService::dashboardRoute());
            }
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.request_error_message'), 500,
                route('login'));
        }
        return $response;
    }
}
