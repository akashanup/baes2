<?php

namespace App\Http\Middleware;

use App\Repositories\UserRepository;
use App\Services\StudentService;
use Closure;

class StudentProgress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $license = request()->route('license');
        $subjectSlug = request()->route('subjectSlug');
        $learningGoalSlug = request()->route('learningGoalSlug');
        $studentLearningGoals = UserRepository::getStudentSubjectLearningGoals(
            auth()->user()->id, $license->id, $subjectSlug);
        request()->request->add(
            ['studentLearningGoals' => $studentLearningGoals]);
        request()->request->add(
            ['subjectName' => $studentLearningGoals->first()->subjectName]);
        request()->request->add(
            ['subjectId' => $studentLearningGoals->first()->subjectId]);
        $scoreAndProgressBlock = StudentService::getScoreAndProgressBlock(
            $studentLearningGoals);
        request()->request->add(
            ['scoreBlock' => $scoreAndProgressBlock['scoreBlock']]);
        request()->request->add(
            ['progressBlock' => $scoreAndProgressBlock['progressBlock']]);
        $studentLevel = StudentService::getBaesLevelByScore(
            $studentLearningGoals->avg('finished_score'))['level'];
        if ($learningGoalSlug) {
            $studentLearningGoal = request()->studentLearningGoal;
            $studentLevel = StudentService::getBaesLevelByScore(
                $studentLearningGoal->current_score)['level'];
            request()->request->add(['learningGoalName' => $studentLearningGoals
                    ->filter(function ($value) use ($studentLearningGoal) {
                        return $value->learningGoalId ===
                        $studentLearningGoal->learning_goal_id;
                    })->first()->name]);
            if ($studentLearningGoal->learning_goal_test_id) {
                request()->request->add(
                    ['lastFiveScores' => StudentService::lastFiveScores(
                        $studentLearningGoal)]);
                request()->request->add(['currentScore' =>
                    $studentLearningGoal->current_score]);
            }
        }
        request()->request->add(['studentLevel' => $studentLevel]);
        return $next($request);
    }
}
