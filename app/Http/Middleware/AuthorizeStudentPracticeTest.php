<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Models\PracticeTestDetail;
use App\Repositories\UserRepository;
use Closure;
use Lang;

class AuthorizeStudentPracticeTest extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = null;
        $practiceTest = PracticeTestDetail::select('id')->findBySlug(
            $request->route('practiceTestSlug'))->firstOrFail();
        $studentPracticeTest = auth()->user()->studentPracticeTestStatuses()
            ->where('practice_test_details.id', $practiceTest->id)->first();
        if ($studentPracticeTest) {
            request()->request->add([
                'studentPracticeTest' => $studentPracticeTest]);
            request()->request->add([
                'questionAnswers' => UserRepository::studentPracticeTestQuestions(
                    auth()->user(), $studentPracticeTest->pivot->practice_test_detail_id)]);
            $response = $next($request);
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 403,
                route('students.practiceTests.index',
                    [request()->route('license')->id, config('constants.ALL')]));
        }
        return $response;
    }
}
