<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Services\DashboardService;
use Closure;
use Lang;

class StudentLicense extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (request()->licenseRole === config('constants.STUDENT')) {
            $response = $next($request);
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 403,
                DashboardService::dashboardRoute());
        }
        return $response;
    }
}
