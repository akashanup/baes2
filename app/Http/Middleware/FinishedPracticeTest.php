<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Models\PracticeTestDetail;
use App\Services\DashboardService;
use Closure;
use Lang;

class FinishedPracticeTest extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $practiceTestDetail = null;
        if ($request->route('practiceTestSlug')) {
            $practiceTestDetail = PracticeTestDetail::findBySlug(
                $request->route('practiceTestSlug'))->firstOrFail();
        } else {
            $practiceTestDetail = PracticeTestDetail::findBySlug($request->practiceTestSlug)
                ->firstOrFail();
        }
        $response = null;
        if ($practiceTestDetail->status === config('constants.FINISHED')) {
            $response = $next($request);
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 403,
                DashboardService::dashboardRoute());
        }

        return $response;
    }
}
