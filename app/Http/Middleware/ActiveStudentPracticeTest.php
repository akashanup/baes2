<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use Carbon\Carbon;
use Closure;
use Lang;

class ActiveStudentPracticeTest extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $studentPracticeTest = request()->studentPracticeTest;
        if ($studentPracticeTest->pivot->status !== config('constants.FINISHED')) {
            if (Carbon::parse($studentPracticeTest->start_date)->lte(
                Carbon::now('GMT')->addMinutes($studentPracticeTest->gmt))) {
                $response = $next($request);
            } else {
                $response = parent::sendErrorResponse($request,
                    (Lang::get('students.exercise_test_starts_at') .
                        $studentPracticeTest->start_date), 403,
                    route('students.practiceTests.index',
                        [request()->route('license')->id, config('constants.ALL')]));
            }
        } elseif ($studentPracticeTest->pivot->status === config('constants.FINISHED')) {
            $response = parent::sendErrorResponse($request,
                Lang::get('students.exercise_test_already_finished'), 403,
                route('students.practiceTests.result',
                    [request()->route('license')->id, $studentPracticeTest->slug]));
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 403,
                route('students.practiceTests.index',
                    [request()->route('license')->id, config('constants.ALL')]));
        }
        return $response;
    }
}
