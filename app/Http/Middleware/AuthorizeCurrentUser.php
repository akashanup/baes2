<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Models\User;
use App\Services\DashboardService;
use Closure;
use Lang;

class AuthorizeCurrentUser extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = null;
        $user = User::findBySlug($request->route('userSlug'))
            ->select('id')->firstOrFail();
        if ($user->id === auth()->user()->id) {
            $response = $next($request);
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 403,
                DashboardService::dashboardRoute());
        }
        return $response;
    }
}
