<?php

namespace App\Http\Middleware;

use Closure;

class CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Send error response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $errorMessage
     * @param  string  $redirectPath
     * @return mixed
     */
    public static function sendErrorResponse(
        $request,
        $errorMessage,
        $statusCode,
        $redirectPath
    ) {
        if ($request->expectsJson()) {
            $response = response()->json(['success' => false,
                'errors' => $errorMessage], $statusCode);
        } else {
            $response = redirect($redirectPath)->withErrors($errorMessage);
        }
        return $response;
    }
}
