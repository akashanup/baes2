<?php

namespace App\Http\Middleware;

use App\Events\CalculateLearningGoalScoreEvent;
use App\Http\Middleware\CustomMiddleware;
use App\Models\LearningGoal;
use App\Models\License;
use App\Models\Subject;
use App\Repositories\UserRepository;
use App\Services\DashboardService;
use Closure;
use Lang;

class AuthorizeStudentTask extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('license')) {
            $license = $request->route('license');
            $subject = Subject::findBySlug(
                $request->route('subjectSlug'))->select('id')->first();
            $learningGoal = LearningGoal::findBySlug(
                $request->route('learningGoalSlug'))->select('id')->first();
        } else {
            $license = License::select('id')->findOrFail($request->license);
            $subject = Subject::select('id')->findOrFail($request->subjectId);
            $learningGoal = LearningGoal::select('id')
                ->findOrFail($request->learningGoalId);
        }
        $authStatus = null;
        if ($subject) {
            if ($learningGoal) {
                $learningGoal = UserRepository::getStudentLearningGoal(
                    auth()->user()->id, $license->id,
                    $subject->id, $learningGoal->id);
                $authStatus = $learningGoal->count();
                $learningGoal = $learningGoal->first();
                if ($learningGoal->learning_goal_test_id && !$request->ajax()) {
                    // Calculate current score of studentLearningGoal
                    event(new CalculateLearningGoalScoreEvent(
                        $learningGoal));
                }
                request()->request->add(
                    ['studentLearningGoal' => UserRepository::getStudentLearningGoal(
                        auth()->user()->id, $learningGoal->license_id,
                        $learningGoal->subject_id,
                        $learningGoal->learning_goal_id)->first(),
                    ]);
                if (request()->studentLearningGoal
                    && request()->studentLearningGoal->status === config('constants.AVAILABLE')) {
                    UserRepository::startStudentLearningGoal(auth()->user()->id,
                        $learningGoal->license_id, $learningGoal->subject_id,
                        $learningGoal->learning_goal_id);
                }
            } else {
                $authStatus = UserRepository::getStudentSubject(auth()->user()->id, $license->id,
                    $subject->id)->count();
            }
            if ($authStatus) {
                $response = $next($request);
            } else {
                $response = parent::sendErrorResponse($request,
                    Lang::get('globals.unauthorized_text'), 403,
                    DashboardService::dashboardRoute());
            }
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 500,
                DashboardService::dashboardRoute());
        }
        return $response;
    }
}
