<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Models\License;
use App\Services\DashboardService;
use Closure;
use Lang;

class AuthorizeLicense extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('license')) {
            $license = $request->route('license');
        } else {
            $license = License::findOrFail($request->license);
        }
        if (auth()->user()->licenses()->where('licenses.id', $license->id)
            ->wherePivot('status', config('constants.ACTIVE'))->first()) {
            request()->request->add(['licenseRole' => $license->role]);
            $response = $next($request);
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('globals.unauthorized_text'), 403,
                DashboardService::dashboardRoute());
        }
        return $response;
    }
}
