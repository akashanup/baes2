<?php

namespace App\Http\Middleware;

use App\Http\Middleware\CustomMiddleware;
use App\Models\User;
use Closure;
use Lang;

class VerifyUserToken extends CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = User::inactiveUser()->findByToken(
            $request->route('token'))->first()) {
            $request->request->add(['user' => $user]);
            $response = $next($request);
        } else {
            $response = parent::sendErrorResponse($request,
                Lang::get('users.invalid_token'), 401,
                route('login'));
        }
        return $response;
    }
}
