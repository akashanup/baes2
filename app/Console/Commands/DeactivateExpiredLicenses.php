<?php

namespace App\Console\Commands;

use App\Models\License;
use Illuminate\Console\Command;

class DeactivateExpiredLicenses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'licenses:deactivate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deactivate expired licenses.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        License::deactivateExpiredLicenses();
        License::deactivateExpiredYearlyUserLicenses();
        License::deactivateExpiredMonthlyUserLicenses();
    }
}
