<?php

namespace App\Console\Commands;

use App\Models\PracticeTestDetail;
use Illuminate\Console\Command;

class FinishExpiredPracticeTests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finish:expired-practice-tests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finishes all expired practice tests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PracticeTestDetail::finishExpiredPracticeTestsDetails();
    }
}
