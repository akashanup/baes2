@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('roles.permissions.update', $role->id)}}" method="post">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="role_id">Role:</label>
            <strong>{{ $role->name }}</strong>
        </div>
        <div class="form-group">
            <label for="permission_id">Permissions:</label>
            <br>
            <div class="row">
                @foreach($rolePermissions as $permission)
                    <div class="col-md-4">
                        @if($permission['role_id'])
                            <input type="checkbox" name="permission_ids[]" checked value="{{ $permission['id'] }}">
                            {{ $permission['name'] }}
                        @else
                            <input type="checkbox" name="permission_ids[]" value="{{ $permission['id'] }}">
                            {{ $permission['name'] }}
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <button type="submit" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection
