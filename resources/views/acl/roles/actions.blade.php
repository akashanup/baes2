<div class="row">
    <div class="col-md-4">
        <a href="{{ $rolePermissionsRoute }}" class="btn btn-sm btn-secondary btn-block">Permissions</a>
    </div>
    <div class="col-md-4">
        <a href="{{ $roleEditRoute }}" class="btn  btn-sm btn-info btn-block">Edit</a>
    </div>
    <div class="col-md-4">
        <form action="{{$roleDeleteRoute}}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button type="submit" id="submitBtn" class="btn btn-sm btn-danger btn-block submitBtn">Delete</button>
        </form>
    </div>
</div>
