@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('roles.store')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Role name:</label>
            <input type="text" class="form-control" name="name" maxlength="25" required>
        </div>
        <button type="submit" id="submitBtn" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection
