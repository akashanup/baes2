@extends('acl.layouts')

@section('css')
    @include('includes.datatable-css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1>Roles</h1>
        </div>
        <div class="col-md-6">
            <a href="{{route('roles.create')}}" class="pull-right btn btn-primary btn-lg">Create Role</a>
        </div>
    </div>
    <table id="roles-table" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    <input type="hidden" id="rolesRoute" value="{{route('roles.indexData')}}">
</div>
@endsection
@section('script')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/acl/role.js').'?v='.$version}}"></script>
@endsection
