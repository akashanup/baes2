@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('roles.update', $role->id)}}" method="post">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Role name:</label>
            <input type="text" class="form-control" name="name" value="{{ $role->name }}"  maxlength="25" required>
        </div>
        <button type="submit" id="submitBtn" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection
