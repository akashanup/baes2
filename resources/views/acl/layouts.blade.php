<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
        @yield('css')
    </head>
    <body>
        @include('includes.error-modal')
        @include('includes.status-modal')
        <div id="app" class="container">
            <div class="row">
                <ul class="list-inline">
                    @guest
                        <li class="list-inline-item"><a href="{{ route('login') }}">Login</a></li>
                        <li class="list-inline-item"><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="list-inline-item">{{ Auth::user()->first_name }}</li>
                        <li class="list-inline-item"><a href="{{ route('roles.index') }}">Role</a></li>
                        <li class="list-inline-item"><a href="{{ route('permissions.index') }}">Permission</a></li>
                        <li class="list-inline-item"><a href="{{ route('acl.users.index') }}">Users</a></li>
                        <li class="list-inline-item">
                            @include('includes.logout')
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
        @yield('content')

        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{asset('js/app.js').'?v='.config('app.version', 1)}}"></script>
        @yield('script')
    </body>
</html>
