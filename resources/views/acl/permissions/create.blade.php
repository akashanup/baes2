@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('permissions.store')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Permission name:</label>
            <input type="text" class="form-control" name="name" maxlength="100" required>
        </div>
        <button type="submit" id="submitBtn" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection
