<div class="row">
    <div class="col-md-6">
        <a href="{{ $permissionEditRoute }}" class="btn  btn-sm btn-info btn-block">Edit</a>
    </div>
    <div class="col-md-6">
        <form action="{{$permissionDeleteRoute}}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button type="submit"  id="submitBtn"class="btn btn-sm btn-danger btn-block submitBtn">Delete</button>
        </form>
    </div>
</div>
