@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('permissions.update', $permission->id)}}" method="post">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Permission name:</label>
            <input type="text" class="form-control" name="name" value="{{ $permission->name }}"  maxlength="100" required>
        </div>
        <button type="submit" id="submitBtn" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection
