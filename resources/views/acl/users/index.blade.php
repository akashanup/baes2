@extends('acl.layouts')

@section('css')
    @include('includes.datatable-css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1>Users</h1>
        </div>
        <div class="col-md-6">
            <a href="{{route('acl.users.create')}}" class="pull-right btn btn-primary btn-lg">Create User</a>
        </div>
    </div>
    <table id="users-table" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    <input type="hidden" id="usersRoute" value="{{route('acl.users.indexData')}}">
</div>
@endsection
@section('script')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/acl/user.js').'?v='.$version}}"></script>
@endsection
