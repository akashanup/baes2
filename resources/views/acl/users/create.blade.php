@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('acl.users.store')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">First name:</label>
            <input type="text" class="form-control" name="first_name"  maxlength="50" required>
        </div>
        <div class="form-group">
            <label for="name">Last name:</label>
            <input type="text" class="form-control" name="last_name"  maxlength="50" required>
        </div>
        <div class="form-group">
            <label for="name">Email:</label>
            <input type="email" class="form-control" name="email" maxlength="50" required>
        </div>
        <div class="form-group">
            <label for="name">Password:</label>
            <input type="password" class="form-control" name="password" required>
        </div>
        <div class="form-group">
            <label for="name">Role name:</label>
            <select name="role_id" required>
                @foreach($roles as $role)
                    <option value="{{$role['id']}}">{{$role['name']}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="name">Status:</label>
            <select name="status" required>
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
            </select>
        </div>
        <input type="hidden" class="form-control" name="phone" id="phone" value="+00 0 00 00 00 00" required>
        <button type="submit" id="submitBtn" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection

@section('script')
    @include('includes.auth-scripts')
@endsection
