@extends('acl.layouts')

@section('content')
<div class="container">
    <form action="{{route('acl.users.update', $user['id'])}}" method="post">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">First name:</label>
            <input type="text" class="form-control" name="first_name"  maxlength="50" value="{{$user['first_name']}}" required>
        </div>
        <div class="form-group">
            <label for="name">Last name:</label>
            <input type="text" class="form-control" name="last_name"  maxlength="50" value="{{$user['last_name']}}" required>
        </div>
        <div class="form-group">
            <label for="name">Email:</label>
            <input type="email" class="form-control" name="email" maxlength="50" value="{{$user['email']}}" required>
        </div>
        <div class="form-group">
            <label for="name">Role name:</label>
            <select name="role_id" required>
                @foreach($roles as $role)
                    @if($role['id'] == $user['role_id'])
                        <option value="{{$role['id']}}" selected>{{$role['name']}}</option>
                    @else
                        <option value="{{$role['id']}}">{{$role['name']}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="name">Status:</label>
            <select name="status" required>
                @if($user['status'] === 'active')
                    <option value="active" selected>Active</option>
                    <option value="inactive">Inactive</option>
                @else
                    <option value="active">Active</option>
                    <option value="inactive" selected>Inactive</option>
                @endif
            </select>
        </div>
        <input type="hidden" class="form-control" name="phone" id="phone" value="{{$user['phone']}}" required>
        <button type="submit" id="submitBtn" class="btn btn-default submitBtn">Submit</button>
    </form>
</div>
@endsection

@section('script')
    @include('includes.auth-scripts')
@endsection
