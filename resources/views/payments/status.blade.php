@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('header')
    <header class="header">
        <div class="container">

            <div class="row">
                <div class="col-xs-6">
                    @include('includes.logo')
                </div>
            </div>  
        </div>
    </header>

    <div class="pattern">
    </div>
@endsection

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="title title--page title--underline--dark">
                  <div class="row">
                    <div class="col-md-4 mt-10">
                      {{__('admins.orders')}}
                    </div>
                  </div>
                </div>
            </div>
        </div>

    <div class="row container-min-height-500">
    @if($payment->status === config('constants.SUCCESS'))
      <div class="alert-sucess alert-primary text-center mb-30">
         {{__('admins.success_status')}}
      </div>
    @elseif($payment->status === config('constants.FAILED'))
      <div class="alert-failure alert-primary text-center mb-30">
         {{__('admins.failed_status')}}
      </div>
    @elseif($payment->status === config('constants.PENDING') || $payment->status === config('constants.OPEN'))
      <div class="alert-pending alert-primary text-center mb-30">
         {{__('admins.pending_status')}}
      </div>
    @endif
  <div class="col-md-5">
    <div class="product-detail">
      <div class="title-block">
          {{__('admins.details_billings')}}
      </div>
      <div class="row specifications lh-20">
        <div class="col-md-8">
          <p>
            <strong>{{__('admins.payment_id')}}</strong>
            <br>
            {{$payment->mollie_payment_unique_id}}
          </p>
          <p>
            <strong>{{__('admins.course')}}</strong>
            <br>
            {{$course->name}}
          </p>
        </div>
        <div class="col-md-4">
          <p>
            <strong>{{__('admins.cost')}}</strong>
            <br>
            &euro; {{number_format($license->price, 2, ',', '.')}}
          </p>
          <p>
            <strong>{{__('admins.date')}}</strong>
            <br>
            {{$payment->updatedTime}}
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="product-detail">
      <!-- item detail -->
      <div class="row specifications pb-20">
        <div class="col-md-12 mb-66 text-center">
            @if($payment->status === config('constants.SUCCESS'))
                <strong>{{__('admins.success_message')}}</strong>
            @elseif($payment->status === config('constants.FAILED'))
            <strong>{{__('admins.failed_message')}}</strong>
            @elseif($payment->status === config('constants.PENDING') || $payment->status === config('constants.OPEN'))
            <strong>{{__('admins.pending_message')}}</strong>
            @endif
        </div>
        <div class="col-md-12 text-center mb-10">
            @if($payment->status === config('constants.SUCCESS'))
               <a href="{{route('users.courses', [$license->id, $course->slug])}}" class="button btn-min-width-200 mr-20">{{__('admins.start_course')}}</a>
          <a href="{{route('users.licenses')}}" class="button button--quaternary btn-min-width-200">{{__('admins.my_license')}}</a>
            @elseif($payment->status === config('constants.FAILED'))
            <a href="{{route('licenses.show', [$license->id, $license->code ? $license->code : ''])}}" class="button btn-min-width-200 mr-20">{{__('admins.order_again')}}</a>
            @elseif($payment->status === config('constants.PENDING') || $payment->status === config('constants.OPEN'))
            <a href="{{route('users.licenses')}}" class="button button--quaternary btn-min-width-200">{{__('admins.my_license')}}</a>
            @endif
          
        </div>
      </div>
    </div>
  </div>
</div>

    </div>
@endsection