<!doctype html>
<html lang="{{config('app.locale', 'en')}}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>@yield('title') {{ config('app.name', 'Laravel') }}</title>
        <meta name="description" content="">
        <meta name="robots" content="NOODP, NOYDIR">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="author" content="PROJECT">
        <meta name="Copyright" content="Copyright 4net interactive {{date('Y')}}. All Rights Reserved.">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}" />
        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datetimepicker.min.css').'?v='.$version}}">
        <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/style.css').'?v='.$version}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/profile.css').'?v='.$version}}">

        @yield('css')

        @if( config('app.env', '') == config('constants.PRODUCTION') )
            <!-- Hotjar Tracking Code for http://www.baes.academy -->
            <script>
                (function(h,o,t,j,a,r){
                    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                    h._hjSettings={hjid:694304,hjsv:6};
                    a=o.getElementsByTagName('head')[0];
                    r=o.createElement('script');r.async=1;
                    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                    a.appendChild(r);
                })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
        @endif
        <!--[if lte IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>
    <body class="" itemscope itemtype="http://schema.org/WebPage">
        <!--Header starts-->
            @yield('header')
        <!--Header ends-->
            <div class="container">
                <!--Breadcumb starts-->
                    @yield('breadcumb')
                <!--Breadcumb ends-->

                <!--Content starts-->
                    @yield('content')
                <!--Content ends-->
            </div>
        <!--Modal starts-->
            @include('includes.error-modal')
            @include('includes.status-modal')
            @include('includes.ie-modal')
            @include('includes.privacy-overlay-new')
            @include('includes.disclaimer-overlay')
            @include('includes.general-feedback-overlay')
            <input type="hidden" id="feedbackRoute" value="{{route('studentFeedbacks.store')}}">
            @yield('modals')
        <!--Modal ends-->

        <!--Footer starts-->
            <div class="footer footer--low">
                <div class="container">
                    @include('includes.logo')
                    <a href="javascript:void(0)" class="footer__link js__overlay-show" data-overlay="privacy">{{__('master.privacy')}}</a>
                    <a href="javascript:void(0)" class="footer__link js__overlay-show" data-overlay="disclaimer">{{__('master.disclaimer')}}</a>
                    <a href="http://baes.education/contact.html" target="_blank" class="footer__link">{{__('master.contact')}}</a>
                </div>
            </div>
        <!--Footer ends-->

        <!-- Scripts starts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $.fn.isIE = function() {
                return ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true ));
            }

            /* Create an alert to show if the browser is IE or not */
            if ($.fn.isIE()){
                $('#ieModal').modal('show');
            }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
        <script type="text/javascript"src="{{asset('js/bootstrap-datetimepicker.min.js').'?v='.$version}}"></script>
        <script type="text/javascript" src="{{asset('js/app.js').'?v='.$version}}"></script>
        <script src="{{asset('js/cookie-helper.js')}}"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
        @yield('scripts')
        <!-- Scripts ends -->
    </body>
</html>
