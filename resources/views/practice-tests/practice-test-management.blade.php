@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools-multiple hideDivider">
        <div class="col-xs-6">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.manage_exercise_test')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('content')
<div class="row tools-multiple">
    <div class="col-xs-3 title">
        {{__('teachers.practice_folders')}}
    </div>
</div>
<hr class="section-divider"/>
<div class="row card-group">
    <div class="card col-md-3" id="createFolderButton">
        <div class="card-inner text-center">
            <i class="fa fa-plus"></i> {{__('teachers.add_folder')}}
        </div>
    </div>
    @foreach($folders as $folder)
        <div class="card col-md-3 folder-card custom-tooltip">
            <div class="folder-action">
                <span class="edit-action" folder-id = "{{$folder->id}}" update-route="{{route('teachers.updateFolder', [$licenseId, $course, $folder->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                @if($folder->count === 0)
                <span class="delete-action" folder-id = "{{$folder->id}}" delete-route="{{route('teachers.deleteFolder', [$licenseId, $course, $folder->id])}}"><i class="fa fa-close" aria-hidden="true"></i></span>
                @endif
            </div>
            <a href="{{route('users.folder-practice-test',
             [$licenseId, $course, $folder->slug])}}">
            <div class="card-inner">
                <h3 class="folder-name">{{$folder->name}}</h3>
                <span class="custom-tooltiptext folder-description">{{$folder->description}}</span>
            </div>
        </a>
        </div>
    @endforeach
</div>
<hr class="section-divider"/>
</br>

@endsection

@section('modals')
<div id="createFolderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-8">
                        <h1>{{__('teachers.create')}} {{__('teachers.folder')}}</h1>
                        <hr/>
                        <form method="post" action="{{route('teachers.createFolder', [$licenseId, $course])}}" role="form">
                            {{ csrf_field() }}
                             <div class="form-group">
                                <label for="">{{__('teachers.name')}}</label>
                                <input type="text " class="form-control required" name="folder_name" id="folderName" required />
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.description')}}</label>
                                <div class="input-group">
                                   <textarea name="folder_description" class="folder-description form-control" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button float-right submitBtn">{{__('teachers.create')}}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-2">
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>

<div id="editFolderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-8">
                        <h1>{{__('admins.update')}} {{__('teachers.folder')}}</h1>
                        <hr/>
                        <form method="post" action="" role="form">
                            {{ csrf_field() }}
                             <div class="form-group">
                                <label for="">{{__('teachers.name')}}</label>
                                <input type="text " class="form-control required" name="folder_name" id="editFolderName" required />
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.description')}}</label>
                                <div class="input-group">
                                   <textarea name="folder_description" id="editFolderDescription"class="folder-description form-control required" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="button" id="updateFolderButton" class="button float-right submitBtn">{{__('admins.update')}}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-2">
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteFolder" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h2>{{__('teachers.confirm_folder_delete')}}</h2>
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmFolderDelete" class="btn btn-default" >Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/teachers.js').'?v='.$version}}"></script>
@endsection
