<!-- Overlay code -->
<input type="hidden" id="saveTestDetailsRoute" value="{{$saveTestDetailsRoute}}">
<div class="overlay" data-overlay="template_toewijzen">
        <div class="overlay__bg"></div>
        <div class="container">
            <div class="overlay__inner">
                <div class="overlay__scroll">
                    <div class="overlay__title">
                        {{__('teachers.activate_key')}}
                    </div>
                    <form class="col-xs-5" id="saveTestDetailsForm" action="{{route('teachers.saveTestDetails')}}" role="form" method="post">
                        <div class="row">
                            <div class="form-group">
                                <label for="">{{__('teachers.class')}}</label>
                                <select id="grade" class="form-control">
                                    @foreach($grades as $grade)
                                        <option value="{{$grade->id}}">
                                            {{$grade->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.start')}}</label>
                                <div class="input-group">
                                    <input type="text" class="form-control form_datetime" id="start_date" placeholder="25-07-2017 15:27" required>
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a id="current_date" class="button button--primary">{{__('teachers.start_now')}}</a>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.end')}}</label>
                                <div class="input-group">
                                    <input type="text" class="form-control form_datetime" id="end_date" placeholder="25-07-2017 15:27" required>
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.time')}}</label>
                                <select id="time" class="form-control">
                                    <option value="30">30 min</option>
                                    <option value="45">45 min</option>
                                    <option value="60">60 min</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.feedback')}}</label>
                                <select id="feedback_type" class="form-control">
                                    <option value="end_of_test">{{__('teachers.end_of_test')}}</option>
                                    <option value="partly">{{__('teachers.good_or_bad')}}</option>
                                    <option value="closed">{{__('teachers.feedback_by_teacher')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('teachers.availability')}}</label>
                                <select id="availability" class="form-control">
                                    <option value="existing_users">{{__('teachers.existing_users')}}</option>
                                    <option value="all_users">{{__('teachers.all_users')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="licenseId" value="{{$licenseId}}" id="licenseId"/>
                                <input type="hidden" name="folderId" value="{{$folder->id}}" id="folderId"/>
                                <button id="save_test" class="button button--primary js__overlay-close submitBtn" data-overlay="template_toewijzen">{{__('teachers.save_and_active')}}</button>
                            </div>
                        </div>
                    </form>
                </div><!-- end overlay-scroll-->
                <div class="overlay__close js__overlay-close" data-overlay="template_toewijzen">
                    <i class="icon-close"></i>
                </div>
            </div>
        </div>
    </div>
