@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools-multiple hideDivider">
        <div class="col-xs-6">
                    @include('includes.breadcumb-header')
                        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a itemprop="itemListElement" href="{{route('teachers.manageTest', [$licenseId, $course])}}"
                             title="" class="breadcrumbs__link">
                                <span itemprop="name">
                                    {{__('teachers.manage_exercise_test')}}
                                </span>
                            </a>
                            <meta itemprop="position" content="2" />
                        </li>
                        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link"><span itemprop="name">{{__('teachers.rounded_practice_keys')}}</span></a>
                            <meta itemprop="position" content="3" />
                        </li>
            @include('includes.breadcumb-footer')
        </div>
        <div class="col-xs-6 text-right">
                <div class="dropdown">
                    <ul class="dropdown__list js__dropdown" id="edit-filter-year">
                        <i class="icon-arrow"></i>
                        <li class="active"><button value="0" class="button button--quaternary button--100">{{__('teachers.year')}}: {{__('teachers.no')}}</button></li>
                        @foreach($years as $year)
                            <li><button class="button button--quaternary button-25p" value="{{ $year }}">{{__('teachers.year')}}: {{ $year }}</button></li>
                        @endforeach
                    </ul>
                </div>
                <div class="dropdown">
                    <ul class="dropdown__list js__dropdown" id="edit-filter-class">
                        <i class="icon-arrow"></i>
                        <li class="active"><button value="0" class="button button--quaternary button--100">{{__('teachers.class')}}: {{__('teachers.no')}}</button></li>
                        @foreach($classes as $key => $class)
                            <li><button class="button button--quaternary button--100" value="{{ $key }}">{{__('teachers.class')}}: {{ $class }}</button></li>
                        @endforeach
                    </ul>
                </div>
                <div class="dropdown ">
                    <div class="dropdown__list">
                        <button class="button button--quaternary button--100" id="apply-button"><span id="apply">{{__('teachers.apply')}}</span></button>
                    </div>
                </div>
            </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-baes table-hover" id="finishedTestTable">
                    <thead>
                        <tr>

                            <th class='text-center'>{{__('teachers.test_name')}}</th>
                            <th class='text-center'>{{__('teachers.date')}}</th>
                            <th class='text-center'>{{__('teachers.year')}}</th>
                            <th class='text-center'>{{__('teachers.class')}}</th>
                            <th class='text-center'>{{__('teachers.students')}}</th>
                            <th class='text-center'>{{__('teachers.score')}}</th>
                        </tr>
                    </thead>
                </table>
                <input type="hidden" name="" value="{{$finishedTestTableRoute}}" id="finishedTestTableRoute">
            </div><!--col-->
        </div><!--row-->
        <hr class="section-divider"/>
    </div>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
