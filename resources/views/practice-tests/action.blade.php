<div class="row">
    <div class="content__block content__block--inactive list-blocked" style="background-image: {{$imageUrl}}; background-repeat:repeat-y;">
        <input type="hidden" value="{{$testTemplateId}}" id="testTemplateId">
        <div class="block__inner block__inner--small">
            <div class="row">
                <div class="col-xs-4">
                    <input type="checkbox" class="testTemplateCheck" name="testTemplateCheck" value="{{$testTemplateId}}"/>
                    <text class="testTitle list-blocked__title" style="color: #000000;">{{$testTemplateName}}</text>
                </div>
                <div class="col-xs-2">
                    <a href="#"  data-id="{{$testTemplateId}}" class="activateTest button button--secondary pull-right js__overlay-show" data-overlay="template_toewijzen">
                        {{__('teachers.activate')}}
                    </a>
                </div>
                <div class="col-xs-3 pull-right">
                    <a href="{{route('teachers.editPracticeTestTemplate', [$licenseId, $courseSlug, $folder->slug, $testTemplateId])}}" name="edit" class="pull-right button btn-toolbar button--quaternary button-width-auto mt-0" data-id="{{$testTemplateId}}" title="bewerken">
                        <i class="icon-edit text-bold" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" class="deleteTestTemplate pull-right button btn-toolbar button--quaternary button-width-auto mt-0" data-id="{{$testTemplateId}}" title="Delete">
                        <i class="glyphicon glyphicon-trash " aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" class="copyTestTemplate pull-right button btn-toolbar button--quaternary button-width-auto mt-0" data-route="{{route('teachers.copyTestTemplate', $testTemplateId)}}" data-id="{{$testTemplateId}}" title="kopieer">
                        <i class="glyphicon glyphicon-copy" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>