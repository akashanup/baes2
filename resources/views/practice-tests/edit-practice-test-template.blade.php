@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools-multiple">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="{{route('users.folder-practice-test', [$licenseId, $course, $folderSlug])}}" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.excercise_key')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.student_management')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="3" />
                </li>
            @include('includes.breadcumb-footer')
    </div>
@endsection

@section('content')
<div class="hidden alert alert-danger" id="some_error">
    {{__('admins.final_catch_response')}}</div>
<div class="row">
    <div class="col-xs-8">
        <table id="practice-test-table" class="table table-baes table-hover">
                    <thead>
                    <tr>
                        <th class='text-center'>{{__('teachers.question')}}</th>
                        <th class='text-center'>{{__('teachers.block')}}</th>
                        <th class='text-center'>{{__('admins.learning_goal')}}</th>
                        <th class='text-center'>{{__('admins.complexity')}}</th>
                        <th class='text-center'>{{__('teachers.random')}}</th>
                        <th class='text-center'></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hidden" id="hidden_row">
                        <td class='text-center'>0</td>
                        <td class='subjectField text-center'>
                            <select name="subject_selected"  class="test-template-subject form-control" required="required">
                                <option value=""></option>
                                @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class='learningGoalField text-center'>
                            <select name="goal_selected"  class="subjectLearningGoal form-control" required="required"></select>
                        </td>
                        <td class='complexityField text-center'>
                            <select name="level_selected"  class="complexityLevel form-control" required="required">
                                @foreach(config('constants.COMPLEXITY') as
                                    $key => $complexity)
                                    @if(intval($key) !== 0)
                                        <option value={{$key}}>{{$complexity}}</option>
                                    @endif
                                    @endforeach
                            </select>
                        </td>
                        <td class='randomField text-center'>
                            <input name="random_selected" type="checkbox" value="">
                        </td>
                        <td class='text-center'>
                            <button class="button button--quaternary" id="deletePracticeBlock">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @foreach($templates as $key => $template)
                        <tr>
                            <td class='text-center'>{{$key}}</td>
                            <td class='subjectField text-center'>
                                <select name="subject_selected"  class="test-template-subject form-control" required="required">
                                    <option value=""></option>
                                    @foreach($subjects as $subject)
                                        @if(intval($template['subject_id']) === intval($subject->id))
                                            <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                                        @else
                                            <option value="{{$subject->id}}">{{$subject->name}}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </td>
                            <td class='learningGoalField text-center'>
                                <select name="goal_selected"  class="subjectLearningGoal form-control" required="required">
                                    @foreach($template['learningGoals'] as $learningGoal)
                                        @if(intval($template['goal_id']) === intval($learningGoal['id']))
                                            <option value={{$learningGoal['id']}} selected>{{$learningGoal['name']}}</option>
                                        @else
                                            <option value={{$learningGoal['id']}}>{{$learningGoal['name']}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </td>
                            <td class='complexityField text-center'>
                                <select name="level_selected"  class="complexityLevel form-control" required="required">
                                    @foreach(config('constants.COMPLEXITY') as
                                    $key => $complexity)
                                    @if(intval($template['level']) === intval($key))
                                        <option value={{$key}} selected>{{$complexity}}</option>
                                    @else
                                        <option value={{$key}}>{{$complexity}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </td>
                            <td class='randomField text-center'>
                                @if($template['random'] === 'no')
                                    <input name="random_selected" type="checkbox"
                                     value="">
                                @else
                                    <input name="random_selected" type="checkbox"
                                    value="" checked>
                                @endif
                            </td>
                            <td class='text-center'>
                                <button class="button button--quaternary" id="deletePracticeBlock">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-lg-offset-5 col-lg-6"><button class="button button--quaternary" id="addPracticeBlock"> {{__('teachers.add_a_row')}}</button></div>
                <input type="hidden" id="subjectLearningGoal"
                value="{{route('teachers.subjectlearningGoal')}}">
                <input type="hidden" id="saveTestTemplate"
                value="{{$createTestRoute}}">
                <input type="hidden" name="testManagementRoute" id="testManagementRoute"
                value="{{$testManagementRoute}}">
    </div>
    <div class="col-xs-4">
                <div class="progress mt-60 mb-60">
                    <div class="progress__inner">
                        <div class="progress__title title js__progress-title">
                            {{__('teachers.practice_key_making')}}
                        </div>
                        <div class="form-baes">
                            <div class="form-group">
                                <label for="">{{__('teachers.name')}} {{__('teachers.excercise_key')}}</label>
                                <input type="text" class="form-control" id="testTemplateName" value="{{$name}}" name="name_selected" placeholder="{{__('teachers.name')}} {{__('teachers.excercise_key')}}" required>
                                <input type="hidden" id="testTemplateId" name="testTemplateId"
                                value="{{$templateId}}">
                            </div>
                             <div class="form-group">
                                <label for="">{{__('teachers.folder')}}</label>
                                <select name="testTemplateFolder" id="testTemplateFolder" class="complexityLevel form-control" required="required">
                                @foreach($folders as $folder)
                                    @if($folderId === $folder->id)
                                        <option value="{{$folder->id}}" selected="">{{$folder->name}}</option>
                                    @else
                                         <option value="{{$folder->id}}">{{$folder->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-20 text-center">
                                  <button id="insert_practice_template" class="button">{{__('teachers.make_available')}}</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
 <div class="row">
            <div class="col-xs-10 text-right mt-50"></div>
</div>
<hr class="section-divider"/>
@endsection
@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
