@if(!empty($finishedPracticeTestData))
    <a href="{{$practiceTestOverviewRoute}}" class="list-blocked__title">{{ $testName }}</a>
@else
    <div class="row">
        <div class="content__block list-blocked" style="background-image: {{$imageUrl}}; background-repeat:repeat-y;">
            <div class="block__inner block__inner--small">
                <div class="row">
                    <div class="col-xs-4">
                        <a href="{{$practiceTestOverviewRoute}}" class="list-blocked__title">{{ $testName }}</a>
                    </div>
                    <div class="col-xs-2">
                        <a href="javascript:void(0)" data-id="{{ $testId }}" class="test_finished button button--secondary pull-right">{{__('globals.close')}}</a>
                    </div>
                    <div class="col-xs-1 text-right">
                        {{ $testGrade }}
                    </div>
                    <div class="col-xs-3 text-right">
                        {{$testEndDate}}
                    </div>
                    <div class="col-xs-1 text-right">
                        {{ $testAvgScore }} %
                    </div>
                    <div class="col-xs-1 text-right">
                        {{ $studentFinishedTestCount }} / {{ $studentsCount }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
