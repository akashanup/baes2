@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools-multiple hideDivider">
        <div class="col-xs-6">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="{{route('teachers.manageTest', [$licenseId, $course])}}" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.manage_exercise_test')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
                 <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{$folder->name}}
                        </span>
                    </a>
                    <meta itemprop="position" content="3" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
        <div class="col-xs-offset-3 col-xs-3 text-right">
            <a href="{{route('teachers.finishedPracticeTest',
             [$licenseId, $course])}}" class="button button--quaternary">{{__('teachers.rounded_practice_keys')}}</a>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('content')
<div class="row tools-multiple">
    <div class="col-xs-3 title">
        {{__('teachers.active_test')}}
    </div>
    <div class="col-xs-9">
        <div class="row">
            <div class="col-xs-10">
                <div class="dropdown pull-right" id="folderDropdownGrade">
                    <ul class="dropdown__list js__dropdown" id="grade-dropdown">
                        <i class="icon-arrow"></i>
                        @foreach($grades as $grade)
                        <li class="{{$gradeSlug === $grade->slug ? 'active' : ''}}">
                             <button class="button button--quaternary button--100" data-slug="{{$grade->slug}}">
                                        {{__('teachers.class')}}: {{$grade->name}}
                                    </button>
                        </li>
                        @endforeach
                        <li class="{{!$gradeSlug ? 'active' : ''}}">
                            <button class="button button--quaternary button--100" data-slug="">
                                {{__('teachers.class')}}: {{__('teachers.all')}}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-2">
                <button class="button" id="folderGradeBtn" data-route="{{$practiceTestRoute}}">
                    {{__('teachers.apply')}}
                </button>
            </div>
        </div>
    </div>

        <div class="col-xs-12">
            <table id="active-practice-test-table" cellpadding="0" cellspacing="0">
                <thead class="hidden">
                    <tr>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
        <input type="hidden" id="listActiveTestRoute" name="listActiveTestRoute" value="{{$listActiveTestRoute}}">
        <input type="hidden" id="changeTestStatusRoute" name="changeTestStatusRoute" value="{{$changeTestStatusRoute}}">
</div>
<hr class="section-divider"/>
</br>

<div class="row tools-multiple">
    <div class="col-xs-3 title">
        {{__('teachers.available_templates')}}
    </div>
    <div class="col-xs-6"  style="text-align: right;">
        <div id="changeFolderSection">
            <select name="testTemplateFolder" id="testTemplateFolder" class="folderListDrop form-control" required="required">
            @foreach($allFolders as $allFolder)
                <option value="{{$allFolder->id}}">{{$allFolder->name}}</option>
                @endforeach
            </select>
            <button name="changeFolderButton" id="changeFolderButton" class="button">Move Templates</button>
            <input type="hidden" value="{{$updateTestTemplateRoute}}" id="updateTestTemplateRoute"/>
        </div>
    </div>
    <div class="col-xs-3">
        <a href="{{route('teachers.manage-practice-test-template', [$licenseId, $course, $folder->slug])}}" id="new_template" class="button button--quaternary">{{__('teachers.create_new_key')}}</a>
    </div>
    <div class="col-xs-12">
        <table id="practice-test-tempate-table" cellpadding="0" cellspacing="0">
            <thead class="hidden">
                <tr>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
    <input type="hidden" id="listpracticeTestRoute" name="listpracticeTestRoute" value="{{$listpracticeTest}}">
</div>

<div id="deletePracticeTestTemplate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="text-align: center;">
                <div class="modal-body">
                    <h2>{{__('teachers.delete_template_message')}}</h2>
                </div>
                <div class="modal-footer">
                    <form method="post" action="{{route('teachers.deleteTestTemplate')}}" id="deleteTemplateForm">
                        <input type="hidden" name="testTemplateId" id="testTemplateId"/>
                        {{ csrf_field() }}
                        <button type="submit" id="confirmTemplateDelete" class="btn btn-default submitBtn">Yes</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folderTestRoute" id="folderTestRoute" value="{{$folderTestRoute}}">
@include('practice-tests.create-test-overlay')
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
