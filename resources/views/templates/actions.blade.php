
@if($type === config('constants.DIRECT_TYPE'))
	@if(!empty($editTemplateRoute) && !empty($editTemplateRoute))
		<div class="col-xs-12">
			<a class="edit center-block btn btn-sm btn-primary margin-bottom" href="{{$editTemplateRoute}}">{{__('admins.edit')}} {{__('admins.template')}}</a>
		</div>
	@endif

	@if(!empty($editVariableSetRoute))
	<div class="col-xs-12">
		<a class=" edit center-block btn btn-sm btn-primary" href='{{$editVariableSetRoute}}'>{{__('admins.edit')}} {{__('admins.variable')}} {{__('admins.set')}}</a>
	</div>	
	@endif
@endif

@if($type === config('constants.MCQ_TYPE'))
	<a class="edit center-block btn btn-sm btn-primary" href="{{$editTemplateRoute}}">{{__('admins.edit')}}</a>
@endif
