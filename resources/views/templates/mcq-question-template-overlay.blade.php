<div class="overlay" data-overlay="template_view">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('admins.template_view')}}
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="mcqDiv">
                            <div class="title" id="mcqquestion"></div>
                            <div class="content__block content__block--border" id="optionDiv">
                                <div class="form form--answers">
                                    <div class="form__item form__item--hover">
                                        <div class="form__radio">
                                            <input type="radio"class="form__radio-input">
                                            <label class="form__radio-label"><i class="icon-a form__icon"></i><div id="option1"></div></label>
                                        </div>
                                        <div class="form__radio">
                                            <input type="radio" class="form__radio-input">
                                            <label class="form__radio-label"><i class="icon-b form__icon"></i><div id="option2"></div></label>
                                        </div>
                                        <div class="form__radio">
                                            <input type="radio" class="form__radio-input">
                                            <label class="form__radio-label"><i class="icon-c form__icon"></i><div id="option3"></div></label>
                                        </div>
                                        <div class="form__radio">
                                            <input type="radio" class="form__radio-input">
                                            <label class="form__radio-label"><i class="icon-d form__icon"></i><div id="option4"></div></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="give-feedback js__overlay-show" data-overlay="feedback">
                                    <i class="icon-speak"></i>{{__('admins.give_feedback')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end overlay-scroll-->
            <div class="overlay__close js__overlay-close template_view_closed" data-overlay="template_view">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>