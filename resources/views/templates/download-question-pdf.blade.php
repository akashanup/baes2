<!doctype html>
<html lang="{{config('app.locale', 'en')}}" dir="ltr">
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style type="text/css">
            .hint > ol > li {
                margin: 1em;
            }
            .page-break {
                page-break-after: always;
            }
        </style>

        <!--[if lte IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->



    </head>
    <body>
        <div class="container">
            <div class="row" style="text-align: center; margin-top: 25%">
                <div class="row">
                    <p style="font-size: xx-large">
                        <b>{{__('teachers.students')}}: </b> {{$user->first_name}} {{$user->last_name}}, {{$grade->name}}, {{$grade->year}}
                    </p>
                </div>
                <div class="row">
                    <p style="font-size: xx-large">
                        <b>{{__('admins.learning_goal')}} {{$learningGoalOrder}}: </b> {{$learningGoalName}}
                    </p>
                </div>
                <div class="row">
                    <p style="font-size: larger">
                        {{__('admins.learning_goal')}} {{__('teachers.finished_with')}} {{__('teachers.baes_score')}}: <b>{{$learningGoalScore}}</b>
                    </p>
                </div>
            </div>
            <div class="row">
                @foreach($learningGoalQuestions as $key => $learningGoalQuestion)
                    <div class="page-break">
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                <h3>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <b>{{__('admins.questions')}}: {{$key + 1}}</b>
                                        </div>
                                        <div class="col-xs-6 col-xs-offset-1">
                                            <span class="pull-right">
                                                {{__('admins.question_level')}}: 
                                                {{config('constants.COMPLEXITY')[$learningGoalQuestion->complexity]}}
                                            </span>
                                        </div>
                                    </div>
                                </h3>    
                                <div class="directQuestionDiv">{!! $learningGoalQuestion->question !!}</div>
                        </div>
                        <div class="panel-body">
                            <h4><b>{{__('admins.correct_answer')}}:</b></h4>
                            <ul>
                                <li> 
                                    {!! $learningGoalQuestion->answer !!}
                                </li>
                            </ul>
                            <h4><b>{{__('teachers.answer_given_by_student')}}:</b></h4>
                            <ul>
                                @if($learningGoalQuestion->answer_given)
                                    @foreach(json_decode($learningGoalQuestion->answer_given, true) as $answers)
                                    <li>{!! $answers !!}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
        <script type="text/javascript">
        $(function () {
            $.fn.replacePercentageFn = function () {
                $.each($('.directQuestionDiv'), function (i, directQuestionDiv) {
                    var directQues = $(directQuestionDiv).html();
                    directQues     = directQues.replace(/\\%/g, '%');
                    $(directQuestionDiv).html('');
                    $(directQuestionDiv).html(directQues);
                });
            };

            MathJax.Hub.Queue(function () {
                $.fn.replacePercentageFn();
            });
        });
        </script>
		
    </body>
</html>