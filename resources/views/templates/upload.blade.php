@extends('layouts.master')

@section('page-title')
    BAES
@endsection

@section('css')
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumbs breadcrumbs--big" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="itemListElement" href="{{ route('admins.dashboard') }}" title="" class="breadcrumbs__link"><span itemprop="name">Dashboard</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="itemListElement" href="#" title="" class="breadcrumbs__link">
                            <span itemprop="name">{{__('admins.add_temp_sheet')}}</span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ol>
            </div>
        </div>
        <hr class="section-divider"/>
        <div class="row">
            @if (session()->has('error_details'))
                <div class="alert alert-danger">
                    @php $error_details = session()->get('error_details'); @endphp
                    <ul>
                        <li><b>Question Template id :</b> @php echo $error_details['qt_id']; @endphp</li>

                        @if (count($error_details['Error_variable_question_template']) > 0)
                            <li><b>Error variable in question template:</b></li>
                            <ol>
                                @foreach($error_details['Error_variable_question_template'] as $variables)
                                        <li>{{ $variables }}</li>
                                @endforeach
                            </ol>
                            <li><b>Question template variables present</b></li>
                            <ol>
                                @foreach($error_details['qt_variables'] as $variables)
                                        <li> {{ $variables }} </li>
                                @endforeach
                            </ol>
                        @endif

                        @if (count($error_details['Error_variable_feedback_template']) > 0)
                            <li><b>Feedback template id : </b>@php echo $error_details['ft_id']; @endphp </li>
                            <li><b>Error variable in feedback template:</b></li>
                            <ol>
                                @foreach($error_details['Error_variable_feedback_template'] as $variables)
                                    <li>{{ $variables }}</li>
                                @endforeach
                            </ol>
                            <li><b>Feedback template variables present</b></li>
                            <ol>
                                @foreach($error_details['ft_variables'] as $variables)
                                    <li> {{ $variables }} </li>
                                @endforeach
                            </ol>
                        @endif
                        <li><b>Variables in variable set</b></li>
                        <ol>
                        @foreach($error_details['question_variables'] as $variables)
                            <li> {{ $variables }} </li>
                        @endforeach
                        </ol>
                    </ul>

                    {{ session()->forget('error_details') }}
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8">
                <form action="{{route('template.uploadContent')}}" method="POST" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">{{__('admins.uploadods')}}</label>
                            <input name="file_name" type="file" required="">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.template_inactive')}}</label></br>
                            <input id="status_yes" class="templateStatus" name="status" type="radio" value="Yes" 
                            {{$type === config('constants.MCQ_TYPE') ? 'checked' : ''}}> {{__('admins.yes')}}
                            <input id="status_no" class="templateStatus" name="status" type="radio" value="No"> {{__('admins.no')}}
                        </div>
                        <div class="form-group" id="learningGoalList">
                            <label for="">
                            {{__('admins.enter')}} {{__('admins.learning_goal')}} id :</label>
                            <select id="lg_id" name="learningGoalID" class="js-example-basic-single select2 form-control">
                            @foreach($learningGoals as $learning_goal)
                              <option value="{{ $learning_goal->id }}" 
                                {{$learning_goal->slug === $slug ? 'selected' : ''}}>{{ $learning_goal->id }} - {{ $learning_goal->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group mt-40 text-right">
                            <button type="submit" class="button submitBtn">{{__('admins.upload')}}</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
