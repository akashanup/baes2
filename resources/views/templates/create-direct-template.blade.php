@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.add')}} {{__('admins.direct')}} {{__('admins.template')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row mt-30">
        <div class="col-xs-8 col-xs-offset-2">
            <form action="{{route('admins.save-template', $type)}}"
            method="POST" role="form" enctype="multipart/form-data" class="createQuestionTemplateForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <h2 for="">{{__('admins.learning_goal')}}</h2>
                        <select class="js-example-basic-single select2 form-control" name="learning_goal_select" required>
                            @foreach($learningGoals as $learning_goal)
                                <option value='{{ $learning_goal->id }}'>{{ $learning_goal->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <h2 for="">{{__('admins.question')}}</h2>
                        <textarea name="question" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <h2 for="">{{__('admins.feedback')}} step 2</h2>
                        <textarea name="feedback2" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <h2 for="">{{__('admins.feedback')}} step 3</h2>
                        <textarea name="feedback3" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <h2 for="">{{__('admins.feedback')}} step 4</h2>
                        <textarea name="feedback4" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <h2 for="">{{__('admins.complexity')}}</h2>
                                <input class="form-control" placeholder="complexity"  type="number" min="1" max="6" name="complexity" required>
                            </div>
                            <div class="col-lg-6">
                                <h2 for="">{{__('admins.number_of_variable_set')}}</h2>
                                <input class="form-control" placeholder="number of set" type="number" min="1" name="set_number">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2 for="">{{__('admins.upload_variable_set')}}</h2>
                                <input class="form-control" type="file" name="file_upload">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-40 text-right">
                        <button type="submit" class="button submitBtn">{{__('admins.save')}}</button>
                    </div>
            </form>
        </div>
    </div>
     <input type="hidden" name="requiredFieldValidation"
    value="{{__('admins.requiredField')}}"id="requiredFieldValidation" />
     <input type="hidden" name="valueRangeValidation"
    value="{{__('admins.value_range')}}"id="valueRangeValidation" />
@endsection

@section('scripts')
    @include('includes.datatable-js')
    @include('learning-goals.ckeditor-script')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
