<label class="curriculumLabel"> Learning Goals: 
	<select class="singleSelect select2 form-control" name="templateLearningDropDown" id="templateLearningDropDown">
		<option value="0">All</option>
		@foreach($allLearningGoals as $learningGoal)
			<option value="{{route('template.getTemplateList', 
			[$learningGoal->slug, $type])}}">{{$learningGoal->name}}</option>
		@endforeach
	</select>
</label>

<label class="curriculumLabel"> Curriculum: 
	<select class="singleSelect select2 form-control" name="templateCurriculumDropDown" id="templateCurriculumDropDown">
		<option value="0">All</option>
		@foreach($CurriculumData as $curriculumList)
			<option value="{{$curriculumList->slug}}">{{$curriculumList->name}}</option>
		@endforeach
	</select>
</label>