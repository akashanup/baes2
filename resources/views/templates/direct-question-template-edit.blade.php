@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
 <div class="row tools-multiple hideDivider">
        <div class="col-xs-6">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('admins.edit')}} {{__('admins.template')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
        <div class="col-xs-offset-3 col-xs-3 text-right">
            <a href="{{route('admins.create-template', $type)}}" class="button button--quaternary">
            {{__('admins.add')}} {{__('admins.direct')}} {{__('admins.template')}}</a>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection


@section('content')
    <div class="row mt-30">
        <div class="col-xs-8 col-xs-offset-2">
            <form action="{{route('question-template.update', [$learningGoalId, $type, $questionTemplateId])}}" id="editTemplateFrm" method="POST" role="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type='hidden' name="qt_id" class="form-control" value="{{ $question->id }}"  required>
                </div>
                <div class="form-group">
                    <h2 for="">{{__('admins.question')}}</h2>
                    <textarea name="question" id="old_question" class="form-control" required>{{ $question->template }}</textarea>
                </div>
                @foreach($feedbacks as $feedback)
                    <div class="form-group">
                        <h2 for="">{{__('admins.feedback')}} step {{ $feedback->step }}</h2>
                        <textarea name="feedback{{ $feedback->step }}" id="old_feedback{{ $feedback->step }}" class="form-control" required>{{ $feedback->template }}</textarea>
                    </div>
                @endforeach
                <div class="form-group mt-40 text-right">
                    <button id="view_direct"  class="button js__overlay-show" data-overlay="template_view">{{__('admins.view')}}</button>
                    <button type="submi" id="submitEditFrmBtn" class="button submitBtn">{{__('admins.save')}}</button>
                </div>
            </form>
        </div>
    </div>
    <input type="hidden" name="questionTemplateId" id ="questionTemplateId"
    value="{{$questionTemplateId}}"/>
    <input type="hidden" name="previewDirectTemplate" id ="previewDirectTemplate"
    value="{{route('admins.previewDirectTemplate')}}"/>
    <hr class="section-divider"/>
    @include('templates.direct-template-view-overlay')
@endsection

@section('scripts')
    @include('includes.datatable-js')
    @include('learning-goals.ckeditor-script')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
