<div class="overlay directTemplatePreview" data-overlay="template_view">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    Template View
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="directDiv">
                            <div class="content__block content__block--border">
                                <div id="question"></div>
                                <div class="form form--answers">
                                    <div class="table__key"><strong>{{__('admins.answer')}}:</strong></div>
                                    <div class="form__item">
                                        <div class="form__radio">
                                            <input type="text"  class="form__input" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="give-feedback">
                                    <i class="icon-speak"></i>{{__('admins.give_feedback')}}
                                </div>
                            </div>
                        </div>
                        <div class="content__block content__block--border js__block-feedback">
                            <div class="question__feedback" data-message="wrong" style="display: block;">
                                <p id="wrongAnswer">
                                    <strong>{{__('admins.error')}}</strong>. {{__('admins.try_again')}}
                                </p>
                                <hr>
                                <div id="feedback2"></div>
                                <hr>
                                <div id="feedback3"></div>
                                <hr>
                                <div id="feedback4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end overlay-scroll-->
            <div class="overlay__close js__overlay-close template_view_closed" data-overlay="template_view">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>