@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.add')}} {{__('admins.direct')}} {{__('admins.template')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
 <div class="row">
    <div class="col-lg-12">
        <h3> Important</h3>
        <ul>
            <li>Don't add any sign in input fields.</li>
        </ul>
    </div>
    <hr class="section-divider"/>
    <div class="col-xs-8 col-xs-offset-2">
                <form action="{{route('template.direct-store')}}" method="POST" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="number" name="id" class="form-control" value="{{ $qt_id }}" hidden>
                        <input type="number" name="set_number" class="form-control" value="{{ $set_number }}" hidden>
                    </div>
                    @for($number = 1; $number <= $set_number; $number++)
                        <div class="row">
                            <div class="col-lg-12">
                                <br>
                                <h1>Variable set {{ $number }}</h1>
                            </div>
                            @foreach($template_variables as $template_variable)
                                <div class="col-lg-12 form-group">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label for="">{{$template_variable}}</label>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" placeholder="Enter the value" name="{{ $template_variable }}{{ $number }}" class="form-control" required>
                                        </div>
                                        <div class="col-lg-3">
                                            <select class="js-example-basic-single form-control" name="type{{ $template_variable }}{{ $number }}" required>
                                                <option value="no-type">No type</option>
                                                <option value="currency">Valuta/Currency</option>
                                                <option value="number">Getal/Number</option>
                                                <option value="percentage">Percentage/Percentage</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="number" value="0" placeholder="Rounding value" name="rounding{{ $template_variable }}{{ $number }}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label for="">Antwoord/Answer</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text"  placeholder="Enter the answer" name="answer{{ $number }}" class="form-control" required>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="js-example-basic-single form-control" name="typeanswer{{ $number }}" required>
                                            <option value="no-type">No type</option>
                                            <option value="currency">Valuta/Currency</option>
                                            <option value="number">Getal/Number</option>
                                            <option value="percentage">Percentage/Percentage</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="number" value="0" placeholder="Rounding value" name="roundinganswer{{ $number }}" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                    <div class="form-group mt-40 text-right">
                        <button type="submit" class="button submitBtn">{{__('admins.save')}}</button>
                    </div>
                </form>
            </div>
</div>
@endsection
