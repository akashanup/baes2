@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
 <div class="row tools-multiple hideDivider">
        <div class="col-xs-6">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('admins.edit')}} {{__('admins.template')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
        <div class="col-xs-offset-3 col-xs-3 text-right">
            <a href="{{route('admins.create-template', $type)}}" class="button button--quaternary">{{__('admins.add')}} {{__('admins.direct')}} {{__('admins.template')}}</a>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection


@section('content')
    <div class="row mt-30">
        <table id="template-table" class="display table table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th style="width: 250px;">{{__('admins.learning_goal')}}</th>
                    <th>{{__('admins.template')}}</th>
                    <th style="width: 100px;">{{__('admins.complexity')}}</th>
                    <th class="text-center">{{__('admins.order')}}</th>
                    <th>{{__('admins.action')}}</th>
                </tr>
            </thead>
        </table>
    </div>
    <input type="hidden" id="templateTableRoute" value="{{$templateList}}">
    <input type="hidden" id="TemplateCurriculumData" value="{{$TemplateCurriculumData}}">
    <hr class="section-divider"/>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
