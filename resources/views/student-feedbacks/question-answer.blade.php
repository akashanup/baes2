<div class="col-xs-12">
    @if($type === config('constants.MCQ_TYPE'))
    <div class="content__block content__block--border">
        <div class="title">
            {!! $question['question'] !!}
        </div>
        @php $adder = 97; $counter = 1; @endphp
        @foreach($question['option'] as $option)
            <div class="form form--answers">
                <div class="form__item form__item--hover">
                    <div class="form__radio">
                        <label for="answer{{$counter++}}">
                            <i class="icon-{{chr($adder++)}} form__icon"></i>
                            {!! $option !!}
                        </label>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@elseif($type === config('constants.DIRECT_TYPE'))
    <div class="content__block content__block--border">
        <div class="row">
            {!! $question['question'] !!}
        </div>
        <div class="row">
            {!! $question['fullHint'] !!}
        </div>
    </div>
@endif
</div>
<div class="col-xs-12">
    <br>
    <a type="button" class="center-block btn btn-primary col-xs-4 col-xs-offset-2" href="{{route('template.editQuestionTemplate', [$learningGoalSlug, $type, $questionTemplateId])}}">
        {{__('admins.edit_question_template')}}
    </a>
    <a type="button" class="center-block btn btn-primary col-xs-4 col-xs-offset-2 pull-right" href="{{route('variable-set.showVariableSet', [$learningGoalSlug, $questionTemplateId])}}">
        {{__('admins.edit_question_variable')}}
    </a>
    <br>
</div>
