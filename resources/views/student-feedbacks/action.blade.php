@if(!empty($action))
	@if($feedbackStatus === config('constants.PROCESSED'))
	    <a href="javascript:void(0)" data-route="{{route('studentFeedbacks.updateStatus', $feedbackId)}}" data-toggle="tooltip" title="{{__('admins.click_to_make_it_unprocessed')}}" class="col-xs-6 js__overlay-show feedbackStatus" data-type="{{$emailType}}">
	        <i class="fa fa-check-square-o"></i>
	    </a>
	@else
	    <a href="javascript:void(0)" data-route="{{route('studentFeedbacks.updateStatus', $feedbackId)}}" data-toggle="tooltip" title="{{__('admins.click_to_make_it_processed')}}" class="col-xs-6 js__overlay-show feedbackStatus" data-type="{{$emailType}}">
	        <i class="fa fa-square-o"></i>
	    </a>
	@endif
	<a href="javascript:void(0)" data-route="{{route('studentFeedbacks.storeNote', $feedbackId)}}" data-toggle="tooltip" title="{{__('admins.add_a_note')}}" class="col-xs-6 js__overlay-show feedbackNote" data-note="{{$feedbackNote}}">
	    <i class="fa fa-sticky-note" aria-hidden="true"></i>
	</a>
@endif
@if(!empty($email))
	<a href="mailto:{{$email}}">{{$fName . ' ' . $lName}}</a>
@endif
@if(!empty($questionAnswerId))
	<a href="javascript:void(null)" data-route="{{route('questionAnswers.show', $questionAnswerId)}}" class="questionAnswerBtn">{{$questionAnswerId}}</a>
@endif
