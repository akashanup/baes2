@extends('layouts.master')

@section('title')
    {{__('admins.manage_feedback')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
    <style type="text/css">
        .greenBtn {
            background-color: green;
        }
    </style>
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.feedbacks')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row mt-30">
        <div class="col-xs-12">
            <table class="table table-bordered table-hover" id="feedback-table">
                <thead>
                    <tr>
                        <th width="5%">{{__('admins.question_id')}}</th>
                        <th width="10%">{{__('admins.learning_goal')}}</th>
                        <th width="5%">{{__('globals.level')}}</th>
                        <th width="5%">{{__('globals.since')}}</th>
                        <th width="15%">{{__('admins.feedback')}}</th>
                        <th width="15%">{{__('users.student')}}</th>
                        <th width="15%">{{__('admins.institution')}}</th>
                        <th width="15%">{{__('admins.course')}}</th>
                        <th width="15">{{__('globals.action')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <input type="hidden" id="homeRoute" value="{{route('home')}}">
    <input type="hidden" id="courseSlug" value="{{$courseSlug}}">
    <input type="hidden" id="institutionSlug" value="{{$institutionSlug}}">
    <hr class="section-divider"/>
    @include('includes.institution-course-dropdown')
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/javascript" src="{{asset('js/student-feedbacks.js').'?v='.$version}}"></script>
@endsection

@section('modals')
    <div id="feedbackQuestionModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div id="feedbackNoteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <h2>{{__('admins.note')}}</h2>
                        <textarea id="feedbackNoteTxt" cols="50" rows="5"></textarea>
                        <button id="feedbackSaveNoteBtn" class="button pull-right js__overlay-close">{{__('globals.save')}}</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div id="feedbackStatusModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <h3>{{__('admins.send_feedback_response')}}</h3>
                    </div>
                    <hr>
                    <input type="hidden" id="feedbackStatusRoute">
                    <div class="col-xs-12">
                        <div class="col-xs-4 col-xs-offset-1">
                            <button class="button feedbackMailBtn" data-type="processed">
                                {{__('admins.send_processed_mail')}}
                            </button>
                        </div>
                        <div class="col-xs-4 col-xs-offset-2">
                            <button class="button feedbackMailBtn" data-type="rounding">
                                {{__('admins.send_rounding_mail')}}
                            </button>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-xs-12">
                        <div class="col-xs-4 col-xs-offset-1">
                            <button class="button feedbackMailBtn" data-type="incorrect">
                                {{__('admins.send_incorrect_mail')}}
                            </button>
                        </div>
                        <div class="col-xs-4 col-xs-offset-2">
                            <button class="button feedbackMailBtn" data-type="custom">
                                {{__('admins.send_custom_mail')}}
                            </button>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-xs-12">
                        <div class="col-xs-4 col-xs-offset-1">
                            <button class="button feedbackMailBtn" data-type="none">
                                {{__('admins.no_mail')}}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
