<a href="{{route('courses.edit', $course['slug'])}}" class="col-xs-4" data-toggle="tooltip" title="{{__('globals.edit')}}">
    <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
</a>
<a href="javascript:void(0)" data-route="{{route('courses.copy', $course['slug'])}}" class="col-xs-4 copyCourse" data-toggle="tooltip" title="{{__('globals.copy')}}">
    <i class="fa fa-clone fa-2x" aria-hidden="true"></i>
</a>
<a href="javascript:void(0)" class="col-xs-4 deleteCourse" data-route="{{route('courses.destroy', $course['slug'])}}" data-toggle="tooltip" title="{{__('admins.delete')}}">
    <i class="fa fa-close fa-2x" aria-hidden="true"></i>
</a>
