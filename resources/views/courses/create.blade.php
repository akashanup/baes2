@extends('layouts.master')

@section('title')
    {{__('admins.create_course')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('courses.index')}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage_course')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.create_course')}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <form action="{{route('courses.store')}}" role="form">
                <div class="form-group">
                    <label for="name">{{__('users.name')}}</label>
                    <input type="text" class="form-control required" id="name" maxlength="50" placeholder="{{__('admins.course')}}">
                </div>
                <div class="form-group">
                    <label for="schools">{{__('admins.institutions')}}</label>
                    <select class="js-example-basic-multiple select2 form-control" id="institutions" multiple="multiple">
                        @foreach($institutions as $institution)
                            <option value="{{$institution['id']}}">{{$institution['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <table class="table table-responsive table-bordered" id="subjectTable">
                        <thead>
                            <tr>
                                <th width="50%">{{__('admins.subject')}}</th>
                                <th width="20%">{{__('admins.learning_goal_count')}}</th>
                                <th width="20%">{{__('globals.order')}}</th>
                                <th width="10%">{{__('globals.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <button type="button" class="button button--quaternary pull-left" id="addSubject">
                    {{ __('admins.add_subject') }}</button>
                </div>
                <div class="form-group">
                    <button type="button" id="saveCourseBtn" class="button pull-right submitBtn" data-request-type="post" data-institutionsChecked="1" data-subjectsChecked="1">
                    {{__('globals.save')}}</button>
                </div>
            </form>
        </div>
    </div>
    @include('courses.subject-hidden-table')
    <hr class="section-divider"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/course.js').'?v='.$version}}"></script>
@endsection
