@extends('layouts.master')

@section('title')
    {{$course['name']}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('courses.index')}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage_course')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$course['name']}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <form action="{{route('courses.update', $course['slug'])}}" role="form">
                <div class="form-group">
                    <label for="name">{{__('users.name')}}</label>
                    <input type="text" class="form-control required" value="{{$course['name']}}" id="name" maxlength="50">
                </div>
                <div class="form-group">
                    <label for="schools">{{__('admins.institutions')}}</label>
                    <select class="js-example-basic-multiple select2 form-control" id="institutions" multiple="multiple">
                        @foreach($institutions as $institution)
                            @if(in_array($institution['id'], $courseInstitutions))
                            <option value="{{$institution['id']}}" selected>{{$institution['name']}}</option>
                            @else
                              <option value="{{$institution['id']}}">{{$institution['name']}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <table class="table table-responsive table-bordered" id="subjectTable">
                        <thead>
                            <tr>
                                <th width="50%">{{__('admins.subject')}}</th>
                                <th width="20%">{{__('admins.learning_goals')}} {{__('admins.count')}}</th>
                                <th width="20%">{{__('globals.order')}}</th>
                                <th width="10%">{{__('globals.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($courseSubjects as $subject)
                                <tr class="subject">
                                    <td><input type="text" class="form-control subjectId" data-id="{{$subject['id']}}" value="{{$subject['name']}}" disabled></td>
                                    <td><input type="text" class="form-control" value="{{$subject['count']}}" disabled></td>
                                    <td><input type="number" min="0" class="form-control subjectOrder" value="{{$subject['order']}}"></td>
                                    <td style="text-align: center;">
                                        <button type="button" class="button button--quaternary deleteSubject">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <button type="button" class="button button--quaternary pull-left" id="addSubject">
                    {{ __('admins.add_subject') }}</button>
                </div>
                <div class="form-group">
                    <button type="button" id="saveCourseBtn" class="button pull-right submitBtn" data-request-type="put" data-institutionsChecked="0" data-subjectsChecked="0">
                    {{__('globals.save')}}</button>
                </div>
            </form>
        </div>
    </div>
    @include('courses.subject-hidden-table')
    <input type="hidden" value="{{json_encode($courseInstitutions)}}" id="existingInstitutions">
    <input type="hidden" value="{{json_encode($courseSubjects)}}" id="existingSubjects">
    <hr class="section-divider"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/course.js').'?v='.$version}}"></script>
@endsection



@section('modals')
<div id="deleteInstitutionModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h5>{{__('admins.remove_institution_course')}}</h5>
                <label>{{__('admins.remove_institution_course_effect')}}</label>
                <br>
                <span style="color: red">Type REMOVE</span>
                <input type="text" id="confirmDeleteInstitutionInpt" placeholder="REMOVE">
                <button type="button" id="confirmDeleteInstitutionBtn" class="button">{{__('admins.update')}}</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteSubjectModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h5>{{__('admins.remove_course_subject')}}</h5>
                <label>{{__('admins.remove_course_subject_effect')}}</label>
                <br>
                <span style="color: red">Type REMOVE</span>
                <input type="text" id="confirmDeleteSubjectInpt" placeholder="REMOVE">
                <button type="button" id="confirmDeleteSubjectBtn" class="button">{{__('admins.update')}}</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection
