<table id="subjectHiddenTable" style="display: none">
    <tbody>
        <tr class="subject">
            <td>
                <select class="form-control subjectId">
                    <option value="" disabled selected></option>
                    @foreach($subjects as $subject)
                        <option value="{{$subject['id']}}" data-learning-goal-count="{{$subject['count']}}">
                            {{$subject['name']}}
                        </option>
                    @endforeach
                </select>
            <td><input type="text" class="form-control learningGoalsCount" disabled=""></td>
            <td><input type="number" class="form-control subjectOrder" min="0" value="0"></td>
            <td style="text-align: center;">
                <button type="button" class="button button--quaternary deleteSubject">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
    </tbody>
</table>
<input type="hidden" id="duplicatedSubjectWarning" value="{{__('admins.duplicatedSubjectWarning')}}">
