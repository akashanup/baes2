@extends('layouts.master')

@section('title')
    {{__('admins.manage_course')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage_course')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a class="button button--quaternary mb-20" href="{{route('courses.create')}}">
                {{__('admins.create_course')}}
            </a>
        </div>
    </div>

    <div class="row mt-30">
        <div class="col-xs-12">
            <table id="course-table" class="display table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">ID</th>
                        <th width="30%">{{__('admins.course')}}</th>
                        <th width="30%">{{__('admins.institutions')}}</th>
                        <th width="10%">{{__('admins.subject_count')}}</th>
                        <th width="20%">{{__('admins.action')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <input type="hidden" id="coursesRoute" value="{{route('courses.indexData')}}">
    <hr class="section-divider"/>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/course.js').'?v='.$version}}"></script>
@endsection

@section('modals')
<div id="deleteCourseModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h5>{{__('admins.delete_course_warning')}}</h5>
                <label>{{__('admins.delete_course_effect')}}</label>
                <form>
                    <span style="color: red" class="center-block">Type DELETE</span>
                    <br>
                    <input type="text" id="confirmDeleteCourseInpt" placeholder="DELETE">
                    <br>
                    <br>
                    <button type="button" id="confirmDeleteCourseBtn" class="button center-block submitBtn">{{__('admins.delete')}}</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection
