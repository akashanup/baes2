@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
        @include('includes.breadcumb-header')
            <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.edit')}} {{__('admins.template')}} {{__('admins.variable')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        @include('includes.breadcumb-footer')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-8 col-xs-offset-2">
        <form id="variableSetForm" action="{{route('admins.updateVariableSet', $id)}}" method="POST" role="form">
            {{ csrf_field() }}
            <div class="form-group">
                <input type='hidden' name="id" class="form-control" value="{{ $id }}"  required>
            </div>
            @foreach($variableKeys as $key => $value)
                <div class="col-lg-12 form-group">
                    <div class="col-lg-4">
                        <label for="">{{ $key }}</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" type="text" name="{{ $key }}" value="{{ $value }}" data-name="{{ $key }}" required/>
                        <div class="error_div"></div>
                    </div>
                </div>
            @endforeach
            <div class=" form-group text-right">
                <button type="submit" id="updateVariableSet" class="button submitBtn">{{__('admins.save')}}</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
