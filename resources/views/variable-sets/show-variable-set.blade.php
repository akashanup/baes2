@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
 <div class="row tools-multiple hideDivider">
    <div class="col-xs-4">
        @include('includes.breadcumb-header')
            <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.edit')}} {{__('admins.template')}} {{__('admins.variable')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        @include('includes.breadcumb-footer')
    </div>
    <div class="col-xs-4 text-right">
        <a class="button button--quaternary mb-20" href="javascript:void(0)" id="uploadFileBtn">
            {{__('admins.upload')}} {{__('admins.variable')}} {{__('admins.set')}}
        </a>
        <div class="hiddenDiv pull-right" id="uploadFileForm">
                        <form role="form" action="{{route('admins.uploadQuestionVariableSet', $question_template_id)}}" method="post" enctype="multipart/form-data" onsubmit="submitBtn.disabled = true; return true;">
                            {{ csrf_field() }}
                            <div class="row pull-right">
                                <div class="col-xs-8">
                                    <input class="form-control" type="file" name="file_upload" required>
                                </div>
                                <div class="col-xs-4">
                                    <button type="submit" name="submitBtn" class="button submitBtn">{{__('admins.upload')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
    </div>
    <div class="col-xs-4 text-right">
        <a class="button button--quaternary mb-20" href="{{route('admins.downloadQuestionVariableSet', $question_template_id)}}">
            {{__('admins.download')}} {{__('admins.variable')}} {{__('admins.set')}}
        </a>
    </div>
</div>
<hr class="section-divider"/>
@endsection

@section('content')
<div class="row mt-30">
       <div class="col-lg-12 scroll-horizontal" id="scroll-horizontal">
            <table id="variableSetTable" class="table table-bordered header-fixed  table-hover table-responsive">
                <thead>
                    <tr>
                        <th>#</th>
                        @foreach( $variableKeys as $key )
                            <th class="text-center">{{ $key }}</th>
                        @endforeach
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                        @php $counter = 1 @endphp
                            @foreach($templateVariablesArray as $data_key => $data)
                                <tr>
                                    <td>{{ $counter++ }}</td>
                                    @foreach($variableKeys as $key)
                                        <td>
                                            <a href="{{route('admins.editSelectedVariableSet', $data_key)}}">{{ $data[$key] }} </a>
                                        </td>
                                    @endforeach
                                    <td><a href="{{route('admins.editSelectedVariableSet', $data_key)}}" class="btn btn-sm btn-primary" >Edit</a></td>
                                </tr>
                            @endforeach
                        </tbody>
            </table>
        </div>
@endsection


@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
