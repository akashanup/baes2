@extends('layouts.master')

@section('title')
    {{__('admins.export_data_1')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(null)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.export_data_2')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  {{__('admins.export_data_1')}}
                </div>
                <div class="panel-body">
                    <form action="{{route('admins.exportData')}}" method="GET" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">{{__('admins.export_data_2')}}</label><br>
                            <div class="row">
                                <label class="col-xs-3">
                                    <input type="checkbox" value="institutions" name="tables[]">
                                    Institutions
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="courses" name="tables[]">
                                    Courses
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="institution_courses" name="tables[]">
                                    Institution Courses
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="subjects" name="tables[]">
                                    Subjects
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="course_subjects" name="tables[]">
                                    Course Subjects
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="learning_goals" name="tables[]">
                                    Learning Goals
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="subject_learning_goals" name="tables[]">
                                    Subject Learning Goals
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="licenses" name="tables[]">
                                    Licenses
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="users" name="tables[]">
                                    Users
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="user_licenses" name="tables[]">
                                    User Licenses
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="question_templates" name="tables[]">
                                    Question Templates
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="question_answers" name="tables[]">
                                    Question Answers
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="hints" name="tables[]">
                                    Hints
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="student_learning_goals" name="tables[]">
                                    Student Learning Goals
                                </label>
                                <label class="col-xs-6">
                                    <input type="checkbox" value="learning_goal_test_question_answers" name="tables[]">
                                    Learning Goal Test Question Answers
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="practice_test_details" name="tables[]">
                                    Practice Test Details
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="practice_test_questions" name="tables[]">
                                    Practice Test Questions
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="student_practice_test_questions" name="tables[]">
                                    Student Practice Test Questions
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="student_practice_test_statuses" name="tables[]">
                                    Student Practice Test Statuses
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="student_feedbacks" name="tables[]">
                                    Student Feedbacks
                                </label>
                                <label class="col-xs-3">
                                    <input type="checkbox" value="student_activities" name="tables[]">
                                    Student Activities
                                </label>
                            </div>
                        </div>
                        <div class="form-group mt-40 text-right">
                            <button type="submit" class="button submitBtn">{{__('admins.export_data')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  {{__('admins.export_special_data')}}
                </div>
                <div class="panel-body">
                    <form action="{{route('admins.exportData')}}" method="GET" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="">{{__('admins.institution')}}</label><br>
                                    <select class="js-example-basic-single select2 form-control" name="institution_id"  required>
                                        @foreach($institutions as $institution)
                                            <option value="{{$institution->id}}">{{$institution->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-6">
                                    <label for="">{{__('admins.export_special_data')}}</label><br>
                                    <select class="js-example-basic-single select2 form-control" name="tables[]" required>
                                        <option value=" learning_goal_test_question_answers">{{__('admins.student_performance')}}</option>
                                        <option value="student_activities">{{__('admins.student_activity')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-40 text-right">
                            <button type="submit" class="button">{{__('admins.export_data')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection
