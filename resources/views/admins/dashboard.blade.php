@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('title')
    {{__('admins.admin')}} {{__('users.dashboard')}}
@endsection

@section('content')
    <div class="title title--page">
        {{__('admins.admin')}} {{__('users.dashboard')}}
    </div>
    <div class="row">
        <div class="col-xs-6 pb50" style="padding-left: 0">
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond5.png');">
                <div class="block__inner block__inner--small block__inner--alt">
                    <div class="row">
                        <div class="col-xs-5">
                            <h3 class="block__title">{{__('admins.curriculum_management')}}</h3>
                        </div>

                        <div class="col-xs-7">
                            <a href="{{route('curriculums.create')}}" class="button">
                                {{__('admins.create_curriculum')}}
                            </a>
                            <a href="{{route('curriculums.index')}}" class="button">
                                {{__('admins.edit_curriculum')}}
                            </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond5.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('admins.product_management')}}</h3>
                            </div>

                            <div class="col-xs-7">
                                <a href="{{route('subjects.index')}}" class="button">
                                    {{__('admins.manage_subject')}}
                                </a>
                                <a href="{{route('courses.index')}}" class="button">
                                    {{__('admins.manage_course')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6"  style="padding-right: 0">
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond4.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('admins.user_management')}}</h3>
                            </div>
                            <div class="col-xs-7">
                                <a href="{{route('licenses.index', [config('constants.ALL'), config('constants.ALL')])}}" class="button">
                                    {{__('admins.manage_licenses')}}
                                </a>
                                <a href="{{route('institutions.index',[config('constants.ALL'), config('constants.ALL')])}}" class="button">
                                    {{__('admins.manage_institution')}}
                                </a>
                                <a href="{{route('users.index',[config('constants.ALL'), config('constants.ALL'), config('constants.ALL')])}}" class="button">
                                    {{__('admins.manage_user')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond4.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('admins.feedback_management')}}</h3>
                            </div>
                            <div class="col-xs-7">
                                <a href="{{route('studentFeedbacks.index',[config('constants.ALL'), config('constants.ALL')])}}" class="button">
                                    {{__('admins.manage_feedback')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond4.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('admins.export_data_1')}}</h3>
                            </div>
                            <div class="col-xs-7">
                                <a href="{{route('admins.exportTable')}}" class="button">
                                    {{__('admins.export_data_2')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection
