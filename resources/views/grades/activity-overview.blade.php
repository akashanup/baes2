@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools hideDivider">
        <div class="col-xs-7">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.class')}} {{__('teachers.overview')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
                 <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.class')}} {{$gradeSlug}}
                        </span>
                    </a>
                    <meta itemprop="position" content="3" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('content')

    <div class="row tools">
        <div class="col-xs-3">
        </div>
        <div class="col-xs-10 text-right pull-right">
            <div class="dropdown custom-dropdown">
                <ul class="dropdown__list js__dropdown" id="teachersViewSwitch">
                    <i class="icon-arrow"></i>
                    <li class="{{$viewType === config('constants.FULL_TIME') ? 'active' : ''}}">
                        <button class="button button--quaternary button--100" 
                        data-slug="{{config('constants.FULL_TIME')}}">
                                    All time view
                        </button>
                    </li>

                    <li class="{{$viewType === config('constants.WEEK_VIEW') ? 'active' : ''}}">
                        <button class="button button--quaternary button--100" 
                        data-slug="{{config('constants.WEEK_VIEW')}}">
                            Week View
                        </button>
                    </li>
                </ul>
            </div>

            <div class="dropdown custom-dropdown">
                <ul class="dropdown__list js__dropdown" id="teachersViewDropdown">
                    <i class="icon-arrow"></i>
                    <li>
                        <button class="button button--quaternary button--100" data-slug="scoreView">
                                    BAES {{__('teachers.score')}}
                        </button>
                    </li>

                    <li class="active">
                        <button class="button button--quaternary button--100" 
                        data-slug="activityView">
                            {{__('teachers.activity')}}
                        </button>
                    </li>

                    <li class="">
                        <button class="button button--quaternary button--100" 
                        data-slug="attemptsView">
                            {{__('teachers.attempts')}}
                        </button>
                    </li>
                </ul>
            </div>
            <div class="dropdown custom-dropdown">
                <ul class="dropdown__list js__dropdown" id="gradeDropdown">
                    <i class="icon-arrow"></i>
                    @foreach($grades as $grade)
                    <li class="{{$gradeSlug === $grade->slug ? 'active' : ''}}">
                        <button class="button button--quaternary button--100" data-slug="{{$grade->slug}}">
                                    {{$grade->name}}
                        </button>
                    </li>
                    @endforeach

                    <li class="{{$gradeSlug == 'all' ? 'active' : ''}}">
                        <button class="button button--quaternary button--100" data-slug="all">
                            {{__('teachers.all')}}
                        </button>
                    </li>
                </ul>
            </div>
            <div class="dropdown custom-dropdown">
                <ul class="dropdown__list js__dropdown" id="subjectDropdown">
                    <i class="icon-arrow"></i>
                   @foreach($subjects as $subject)
                        <li class="{{$activeSubjectSlug === $subject->slug ? 'active' : ''}}">
                            <button class="button button--quaternary button--100" style="font-size: smaller;" data-slug="{{$subject->slug}}">
                                {{$subject->name}}
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
                <button class="button button button--submit float-right" id="teachersOverviewButton">{{__('teachers.apply')}}</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-9 pl-0">
            <div class="table-baes-tooltip-leerdoel">
                <div id="hovertext" class="leerdoel-title text-center">
                    {{__('teachers.learning_goal')}} 1
                </div>
            </div>
            <div id="x-scroll-horizontal">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 pr-0">
            <table class="table table-baes table-hover">
                <thead>
                    <tr>
                        <th>
                            <a href="{{route('grades.activityOverview', [$licenseId, $course, $gradeSlug, $urlSubject, $viewType, $sortName ? $sortName : ''])}}">
                                {{__('teachers.learner')}}
                            </a>
                        </th>
                        <th class="bg-lightgreen text-bold text-center">
                            <a href="{{route('grades.activityOverview', [$licenseId, $course, $gradeSlug, $urlSubject, $viewType, $sortScore ? $sortScore : ''])}}">
                                <span style="font-size:12px;">{{__('teachers.activity')}}</span>
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <span class="text-bold">{{__('teachers.average_class')}}</span>
                        </td>
                        <td class="bg-lightgreen text-center">
                            <span class="text-bold">{{number_format($averageClassActivityScore, 1)}}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="text-bold">{{__('teachers.class_progress')}}</span>
                        </td>
                        <td class="bg-lightgreen text-center">
                            <span class="text-bold" id="average_class_progress">-</span>
                        </td>
                    </tr>
                    @foreach($studentsList as $student)
                        <tr>
                            <td class="break_name">
                                <div class="name-break">
                                    <a href="{{route('grades.students', [$licenseId, $course, $gradeSlug === config('constants.ALL') ? $student->gradeSlug : $gradeSlug, $activeSubjectSlug, $student['id']])}}">
                                        {{$student['first_name']}} {{$student['last_name']}}
                                    </a>
                                </div>
                            </td>
                            <td class="bg-lightgreen text-center">
                                @if(count($studentLearningGoalActivity))
                                {{number_format($student['average_activity'],1)}}
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        <div class="col-md-8 pl-0 ">
            <div id="scroll-horizontal" class="scroll-horizontal">
                 <table class="table table-baes table-baes--scroll table-hover mb-0">
                    <thead>
                        <tr>
                            @foreach($learningGoals as $learningGoal)
                                <th style="text-align: center">
                                    <strong class="showOnHover" data-targetid="hovertext" data-title="{{$learningGoal['name']}}">
                                        L{{$learningGoal['pivot']['order']}}
                                    </strong>
                                </th>
                            @endforeach
                        </tr>

                    </thead>
                    <tbody>
                        <tr>
                            
                           @foreach($learningGoals as $learningGoal)
                            <td class="text-center">
                                @if(count($averageLearningGoalScore))
                                    @if($averageLearningGoalScore[$learningGoal['id']])
                                        @if($averageLearningGoalScore[$learningGoal['id']] >= 8)
                                            <div class="result result--round result--green">
                                        @else
                                            <div class="result result--round result--gray">
                                        @endif
                                            {{number_format($averageLearningGoalScore[$learningGoal['id']], 1)}}
                                    @else
                                        <div class="result result--pending">
                                    @endif
                                @else
                                    <div class="result result--pending">
                                @endif
                                    </div>
                                    
                            </td>
                            @endforeach
                        </tr>
                        <tr>
                           @foreach($learningGoals as $learningGoal)
                            <td class="text-center">
                                -
                            </td>
                            @endforeach
                        </tr>

                        @foreach($studentsList as $student)
                            <tr>
                                @foreach($learningGoals as $learningGoal)
                                <td class="text-center">
                                    @if(count($studentWeeklyReport))
                                    @if($studentWeeklyReport[$student['id']][$learningGoal['id']])
                                    @if($studentWeeklyReport[$student['id']][$learningGoal['id']] >= 8)
                                        <div class="result result--round result--green">
                                    @else
                                        <div class="result result--round result--gray">
                                    @endif
                                            {{number_format($studentWeeklyReport[$student['id']][$learningGoal['id']], 1)}}
                                    @else
                                        <div class="result result--pending">
                                    @endif
                                        </div>
                                    @endif
                                </td>

                            @endforeach
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="text-right mt-20">
        <a href="{{route('teachers.student-management', 
        [$licenseId, $course, $gradeSlug, $currentYear])}}" 
        class="button button--quaternary">{{__('teachers.student_management')}}</a>
    </div>
    <input type="hidden" name="defaultRoute" id="defaultRoute" 
    value="{{$defaultRoute}}"/>
        
@endsection
@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/teachers.js').'?v='.$version}}"></script>
@endsection
