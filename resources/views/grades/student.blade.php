@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('css')
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('grades.overview', [$licenseId, $course, $gradeSlug, $subject, config('constants.FULL_TIME')])}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('teachers.class')}} {{__('teachers.overview')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('grades.overview', [$licenseId, $course, $gradeSlug, $subject, config('constants.FULL_TIME')])}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('teachers.class')}} {{$gradeSlug}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$student->first_name}} {{$student->last_name}}
                </span>
            </a>
            <meta itemprop="position" content="4" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-4">
        <div class="profile profile--student">
            <div class="profile__inner">
                <div class="profile__head">
                    <div class="profile__img">
                        <img src="{{$profileImageUrl}}" alt="" 
                        style="max-width: 75px; width: 100%; max-height: 60px;"
                         class="img-circle">
                    </div>
                    <div class="profile__name">
                        {{$student->first_name}} {{$student->last_name}}
                    </div>
                </div>
                <div class="profile__slide">
                    <div class="profile__info">
                        <div class="profile__key">{{__('teachers.class')}}:</div>
                        <div class="profile__value">{{$student->gradeName}}</div>
                        <div class="profile__key">{{__('admins.course')}}:</div>
                        <div class="profile__value">{{$courseName}}</div>
                        <div class="profile__key">{{__('admins.subject')}}</div>
                        <div class="profile__value" style="font-size: small;">{{$subjectName}}</div>
                    </div>
                    <div class="progress">
                        <div class="progress__inner">
                            <div class="progress__item">
                                <div class="progress__subtitle">
                                    <small>
                                        {{__('teachers.progress')}} {{__('teachers.t_block')}}:
                                    </small>
                                    <span class="pull-right text-uppercase" style="font-size: small;">
                                        {{$subject}}
                                    </span>
                                </div>
                                <div class="progress__bar">
                                    <span class="progress__value js__progress-bar" style="width: {{number_format($studentBlocks['progressBlock'])}}%;">
                                    </span>
                                    <i>{{number_format($studentBlocks['progressBlock'])}}%</i>
                                </div>
                            </div>
                            <div class="progress__item">
                                <div class="progress__subtitle">
                                    <small>{{__('teachers.score')}} {{__('teachers.t_block')}}:</small>
                                    <span class="pull-right text-uppercase" style="font-size: small;">
                                        {{$subject}}
                                    </span>
                                </div>
                                <div class="progress__bar">
                                    <span class="progress__value js__progress-bar" style="width: {{($studentBlocks['scoreBlock'])*10}}%;"></span><i>{{$studentBlocks['scoreBlock']}}</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-8">
        <div class="activity-overview mt-30">
            <div class="title">Week {{__('teachers.activity')}}</div>
            <div class="chart">
                @foreach($weeklyActivities as $activity)
                    <div class="chart__item">
                        <div class="chart__label">{{$activity['day']}}</div>
                        <div class="chart__bar js__chart-bar">
                            <span style="height: {{$maxActivityScore === 0 ? 0 : $activity['points']/$maxActivityScore * 100}}%;"></span>
                        </div>
                        <div style="text-align: center;"><span>{{$activity['points']}}</span></div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="progress-overview">
            <div class="row">
                <div class="col-xs-4">
                    <div class="title">{{__('teachers.progress')}}</div>
                </div>
                <div class="col-xs-8">
                    <div class="table-baes-tooltip-leerdoel">
                        <div id="hovertext" class="leerdoel-title text-center">
                            {{__('teachers.learning_goal')}} 1
                        </div>
                    </div>
                    <div id="x-scroll-horizontal">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="scroll-horizontal">
                        <table class="table table-baes mb-0table-baes--scroll table-hover">
                            <thead>
                                <tr>
                                @foreach($learningGoals as $learningGoal)
                                    <th style="text-align: center">
                                        <strong class="showOnHover" data-targetid="hovertext" data-title="{{$learningGoal['name']}}">
                                            L{{$learningGoal['pivot']['order']}}
                                        </strong>
                                    </th>
                                @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach($studentLearningGoals as $studentLearningGoal)
                                        <td class="text-center">
                                    @if($studentLearningGoal->status === config('constants.AVAILABLE'))
                                    <div class="result result--pending"></div>
                                    @else
                                        @if($studentLearningGoal->current_score >= 0 &&
                                        $studentLearningGoal->current_score <= 4.9)
                                        <div class="result result--round
                                                    result--gray">
                                        @elseif($studentLearningGoal->current_score  >= 5.0 &&
                                        $studentLearningGoal->current_score  <= 6.5)
                                        <div class="result result--square result--red">
                                        @elseif($studentLearningGoal->current_score  >= 6.6 &&
                                        $studentLearningGoal->current_score  <= 7.9)
                                        <div class="result result--round result--orange">
                                        @elseif($studentLearningGoal->current_score  >= 8.0 &&
                                        $studentLearningGoal->current_score <= 10.0)
                                        <div class="result result--round result--green">
                                        @endif
                                        {{$studentLearningGoal->current_score}}
                                        </div>
                                    @endif
                                </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($studentLearningGoals as $studentLearningGoal)
                @if($studentLearningGoal->status === config('constants.FINISHED'))
                    <div class="col-xs-6">
                        <div class="progress progress--overview">
                            <div class="progress__item">
                                <div class="progress__subtitle">
                                    <i>L{{$studentLearningGoal->order}}: {{$studentLearningGoal->name}}</i>

                                </div>
                                <div class="progress__bar" style="height: 20px; color: white">
                                    <span class="progress__value js__progress-bar js__progress-bar-final baesScoreBar" style="width: {{($studentLearningGoal->finished_score) * 10}}%;"></span>
                                    <i class="js__baes-score baesScoreITag">
                                    {{($studentLearningGoal->finished_score)}}
                                </div>
                                <br>
                                <a target="_blank" class="button button--ghost button--small pull-right pdf_export" href="{{route('teachers.exportQuestionPdf', [$licenseId, $course, $studentLearningGoal->learningGoalId, $subjectId, $gradeSlug, $userId])}}">
                                    {{__('teachers.export_questions')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection