@if(!empty($email))
	<a href="mailto:{{$email}}">{{$email}}</a>
@endif
@if(!empty($action))
	@if($userStatus === config('constants.ACTIVE'))
		<a href="javascript:void(null)" class="col-xs-3 userActions" data-toggle="tooltip" title="{{__('admins.block')}}" data-route="{{route('users.changeStatus', $userId)}}" data-function="status" data-request="put">
			<i class="fa fa-ban" aria-hidden="true"></i>
		</a>
	@else
		<a href="javascript:void(null)" class="col-xs-3 userActions" data-toggle="tooltip" title="{{__('admins.activate')}}" data-route="{{route('users.changeStatus', $userId)}}" data-function="status" data-request="put">
			<i class="fa fa-check" aria-hidden="true"></i>
		</a>
	@endif
	<a href="javascript:void(null)" class="col-xs-3 userActions" data-toggle="tooltip" title="{{__('globals.delete')}}" data-route="{{route('users.destroy', $userId)}}" data-function="delete" data-request="delete">
		<i class="fa fa-close" aria-hidden="true"></i>
	</a>
	<a href="javascript:void(null)" class="col-xs-3 userActions" data-toggle="tooltip" title="{{__('users.reset_password')}}" data-route="{{route('users.resetPassword', $userId)}}" data-function="password" data-request="get">
		<i class="fa fa-key" aria-hidden="true"></i>
	</a>
	<a href="javascript:void(null)" class="col-xs-3 userActions" data-toggle="tooltip" title="{{__('globals.licenses')}}" data-route="{{route('users.show.licenses', $userId)}}" data-function="licenses">
		<i class="fa fa-id-card" aria-hidden="true"></i>
	</a>
@endif
