@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('content')
    @include('includes.code-panel')

    <div class="row mt-30 licenses-grid">
        @foreach($licenses as $license)
            <div class="col-xs-3 licence-item">
                <div class="content__block content__block--border product product--text-center">
                    @include('includes.product-details')
                    <a href="{{route('licenses.show',[$license['id'], $license['code']])}}" class="button">{{__('users.purchase')}}</a>
                </div>
            </div>
        @endforeach
    </div>
    <hr class="section-divider"/>
@endsection
