@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('content')
    <div class="row">
        <h2>{{__('admins.manage_licenses')}}</h2>
        <hr class="section-divider"/>
        <div class="col-md-3 product-detail-detail-block">
            <div class="content__block product product--text-center detailed__product">
                <div class="product__content">
                    @include('includes.product-details')
                </div>
            </div>
        </div>
        <div class="col-md-6 product-detail-detail-block">
            <div class="product-detail">
                <div class="title-block">
                    {{__('users.specifications')}}
                </div>
                <div class="row specifications">
                    <div class="col-md-6">
                        <p>
                            <strong>{{__('admins.course')}}</strong>
                            <br>
                            {{$license['courseName']}}
                        </p>
                        <p>
                            <strong>{{__('globals.year')}}</strong>
                            <br>
                            {{$license['year']}}
                        </p>
                        <p>
                            <strong>{{__('users.license_duration')}}</strong>
                            <br>
                            @if($license['type'] === config('constants.MONTHLY_PAYMENT'))
                                {{__('users.continuous')}}
                            @else
                                {{$license['licenseDuration']}}
                            @endif
                        </p>
                    </div>
                    <div class="col-md-6">
                        <strong>{{__('users.description')}}</strong>
                        <br>
                        <p class="specifications-description">
                            {{$license['description']}}
                        </p>
                    </div>
                    @if($license['type'] === config('constants.MONTHLY_PAYMENT'))
                        <div class="col-md-12">
                            <div class="product__price">
                                <button class="button js__overlay-show pull-right" data-overlay="cancelUserLicense">{{__('admins.stop_subscription')}}</button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
    @if($license['type'] === config('constants.MONTHLY_PAYMENT'))
        <div class="overlay" data-overlay="cancelUserLicense">
            <div class="overlay__bg"></div>
            <div class="container">
                <div class="overlay__inner" style="text-align: center;">
                    <div class="overlay__title">
                        <div class="row">
                            {{__('admins.confirm_cancel')}}
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-xs-offset-4">
                                <a class="button pull-right js__overlay-close" data-overlay="cancelUserLicense">{{__('admins.close_screen')}}</a>
                            </div>
                            <div class="col-xs-2 col-xs-offset-1">
                                <a href="{{route('license.cancelUserLicense', $license['id'])}}" class="button">{{__('admins.cancel')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="overlay__close js__overlay-close" data-overlay="cancelUserLicense">
                        <i class="icon-close"></i>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
