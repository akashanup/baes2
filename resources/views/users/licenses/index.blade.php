@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('content')
    <h2>{{__('users.my_courses')}}</h2>
    <hr class="section-divider"/>
    <div class="row">
        <div class="grid-container-4">
            @foreach($licenses as $license)
                    <div class="grid-item content__block product product--text-center">
                        <div class="product__content">
                            <img src="{{$license['image']}}" alt="{{$license['courseName']}}" class="product__logo--school">
                            <h2 class="product__title">{{$license['courseName']}}</h2>
                            <div class="product__description product-detail">
                                <p>
                                    @foreach($license['subjects'] as $subject)
                                        {{$subject}}</br>
                                    @endforeach
                                </p>
                            </div>
                            <div class="product__price course-pricing product-detail">
                                @if($license['licenseRole'] === config('constants.TEACHER'))
                                    <div class="product__price__badge">
                                      {{__('users.teacher')}}
                                    </div>
                                @endif
                                @if($license['price'])
                                    <p>{!!$license['price']!!}</p>
                                @else
                                    <p>{{__('users.free')}}</p>
                                @endif
                                <p class="course-date">
                                <strong>{{__('globals.end_date')}}</strong>:
                                @if($license['type'] === config('constants.MONTHLY_PAYMENT'))
                                    {{__('users.continuous')}}
                                @else
                                    {{$license['licenseDuration']}}
                                @endif
                            </p>
                            <a href="{{route('users.courses', [$license['id'], $license['courseSlug']])}}" class="button">{{__('users.open_course')}}</a>
                            <a href="{{route('users.licenses.show', $license['id'])}}" class="button button--quaternary">{{__('globals.manage')}}</a>
                        </div>
                        </div>
                    </div>
            @endforeach
            <div class="grid-item content__block content__block--border product product--text-center empty-block">
                <div class="course-flex-container">
                    <div class="course-flex-container-content">
                        <p>
                            <strong>{{__('users.add_course')}}</strong>
                        </p>
                        <a href="{{route('users.availableLicenses')}}" class="button">{{__('users.add')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection
        