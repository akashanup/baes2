@extends('layouts.master')

@section('title')
    {{__('admins.manage_user')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage_user')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row mt-30">
        <table id="user-table" class="display table table-hover table-bordered">
            <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="30%">{{__('admins.name')}}</th>
                    <th width="30%">{{__('authentication.email')}}</th>
                    <th width="10%">{{__('globals.status')}}</th>
                    <th width="25%">{{__('admins.action')}}</th>
                </tr>
            </thead>
        </table>
    </div>
    <input type="hidden" id="homeRoute" value="{{route('home')}}">
    <input type="hidden" id="courseSlug" value="{{$courseSlug}}">
    <input type="hidden" id="licenseId" value="{{$licenseId}}">
    <input type="hidden" id="institutionSlug" value="{{$institutionSlug}}">
    <hr class="section-divider"/>
    @include('includes.institution-course-dropdown')
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/users.js').'?v='.$version}}"></script>
@endsection

@section('modals')
    <div id="userLicenseModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <h1>{{__('globals.licenses')}}</h1>
                    </div>
                    <div id="userLicensesDiv" class="col-xs-12">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
