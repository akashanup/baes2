@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8">
            <div class="title title--page">
                {{__('users.dashboard')}}
            </div>
            <div class="intro">
                <div class="row">
                    <div class="col-xs-6">
                        <h1 class="subtitle">{{__('students.dashboard_intro_welcome')}}</h1>
                        <p>{{__('students.dashboard_intro_message1')}}</p>
                        <p>{{__('students.dashboard_intro_message2')}}</p>
                    </div>
                    <div class="col-xs-6">
                        <div class="video js__overlay-show" data-overlay="fullscreen-video3">
                            <div class="video__placeholder">
                                <img class="video__image" src="/images/placeholder3.png" alt="">
                                <div class="video__play"><i class="icon-play2"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="section-divider"/>
            <h2 class="title">{{__('students.dashboard_subject_heading')}}</h2>
            @php $flag = 0 @endphp
            @foreach($subjects as $subject)
                @if($flag % 2 == 0)
                    <div class="row content-blocks">
                @endif
                <div class="col-xs-6">
                    <div class="content__block content__block--green">
                        <div class="block__inner">
                            <h3 class="block__title">{{$subject['name']}}</h3>
                            <a href="{{ route('students.subjects.learning-goals', [$licenseId, $subject['slug']]) }}" class="button button--left button--icon button--quaternary">
                                <i class="icon-arrow"></i>{{__('students.button_overview_learning_objective')}}
                            </a>
                            <a href="{{ route('students.learning-goals.show', [$licenseId, $subject['slug'], $subject['lastLearningGoalSlug']]) }}" class="button button--left button--icon">
                                <i class="icon-arrow"></i>{{__('students.button_skip')}}
                            </a>
                        </div>
                    </div>
                </div>
                @if($flag % 2 == 1 || $flag == count($subjects) - 1)
                    </div>
                @endif
                @php $flag+=1 @endphp
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="col-xs-6">
                <div class="content__block content__block--green" style="margin-left: -1em">
                    <div class="block__inner">
                        <h3 class="block__title">{{__('students.exercise_test')}}</h3>
                        <a href="{{route('students.practiceTests.index', [$licenseId, config('constants.ALL')])}}" class="button button--left button--icon button--quaternary">
                            <i class="icon-arrow"></i>{{__('students.exercise_test_key')}}
                        </a>
                        <a href="{{route('students.practiceTests.create', [$licenseId, $courseSlug])}}" class="button button--left button--icon button--quaternary">
                            <i class="icon-arrow"></i>{{__('users.create_a_test')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('modals')
    <div class="overlay js__overlay-video" data-overlay="fullscreen-video3">
        <div class="overlay__bg"></div>
        <div class="container">
            <div class="overlay__inner">
                <div class="overlay__video">
                    <div class="video__placeholder">
                        <iframe width="100%" height="550" id="iFrame1" src="https://www.youtube.com/embed/tj3JjUz8cp8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="overlay__close js__overlay-close" data-overlay="fullscreen-video3">
                    <i class="icon-close"></i>
                </div>
            </div>
        </div>
    </div>
@endsection
