@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection


@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('students.subjects.learning-goals', [$licenseId, $subjectSlug])}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$subjectName}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="#" title="" class="breadcrumbs__link"><span itemprop="name">{{ $learningGoalName }}</span></a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8">
            <div class="title title--page">
                <span>
                    {{__('admins.learning_goal')}} {{ $learningGoalOrder }} : {{ $learningGoalName }}  <a title="{{__('users.export_theory')}}" href="{{route('students.exportTheory', [$licenseId, $subjectSlug, $learningGoalSlug])}}"><i class="fa fa-download  small-icon" aria-hidden="true"></i></a>
                </span>
                <a href="{{$testRoute}}" class="button pull-right small-button">
                    {{__('users.start_test')}}
                </a>
            </div>
            <div class="intro intro--theory">
                <div class="title">{{__('admins.theory')}}</div>
                <div class="intro__overflow js__intro-overflow">
                     {!! $learningGoal['theory'] !!}
                </div>
                <div class="toggle-intro js__toggle-intro pull-right">{{__('users.view_more')}}</div>
            </div>
            <div class="examples">
                <div class="examples__overflow js__examples-overflow">
                    {!! $learningGoal['example'] !!}
                </div>
                <div class="toggle-examples js__toggle-examples pull-right">{{__('users.view_more')}}</div>
            </div>

            <div class="row">
                @foreach($learningGoal['video'] as $key => $video)
                    <div class="col-xs-6">
                        <div class="video js__overlay-show" data-overlay="fullscreen-video{{($key+1)}}">
                            <div class="video__placeholder">
                                <img class="video__image" src="{{asset('images/placeholder'.($key+1).'.png')}}" alt="">
                                <div class="video__play"><i class="icon-play2"></i></div>
                                <div class="video__title video__title--inset">{{ $video['name'] }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="overlay js__overlay-video" data-overlay="fullscreen-video{{($key+1)}}">
                        <div class="overlay__bg"></div>
                        <div class="container">
                            <div class="overlay__inner">
                                <div class="overlay__video">
                                    <div class="video__placeholder">
                                        <div class="video__play video__play--primary js__video-play"></div>
                                        <iframe width="100%" height="550" id="iFrame{{($key+2)}}" src="{{ $video['url'] }}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <div class="overlay__close js__overlay-close " data-overlay="fullscreen-video{{($key+1)}}">
                                        <i class="icon-close"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @include('includes.student-progress')
    </div>
    <a id="back-to-top" href="#" class="btn btn-lg back-to-top" role="button" title="{{__('users.go_up')}}"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <br><br>
@endsection

@include('includes.learning-goal-test-scripts')
