@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$subjectName}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8">
            <div class="title title--page">{{__('admins.learning_goals')}}</div>
            @if($practiceTestsCount)
                <div class="content__block" style="background-image: url({{asset('images/achtergrond3.png')}})">
                    <div class="block__inner block__inner--small">
                        <div class="row">
                            <div class="col-xs-6">
                                <h3 class="block__title">{{__('students.exercise_test')}}</h3>
                            </div>
                            <div class="col-xs-6">
                                <a href="{{route('students.practiceTests.index', [$licenseId, $subjectSlug])}}" class="button button--secondary pull-right">
                                    {{__('students.exercise_test_key')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="content__block" style="background-image: url({{asset('images/achtergrond3_gray.png')}}); background-repeat:repeat-y;">
                    <div class="block__inner block__inner--small">
                        <div class="row">
                            <div class="col-xs-6">
                                <h3 class="block__title">{{__('students.practice_test')}}</h3>
                            </div>
                            <div class="col-xs-6">

                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="tiles">
                @foreach($learningGoals as $learningGoal)
                    @if($learningGoal['status'] === config('constants.AVAILABLE'))
                        <div class="tile__item">
                            <div class="tile__inner">
                                <div class="tile__subtitle">
                                    {{__('admins.learning_goal')}} {{ $learningGoal['order'] }}
                                </div>
                                <div class="tile__title">
                                    {{$learningGoal['name']}}
                                </div>
                                <div class="tile__hover">
                                    <a href="javascript:void(null)" class="button beginLearningGoalBtn" data-redirectRoute="{{route('students.learning-goals.show', [$learningGoal['licenseId'], $learningGoal['subjectSlug'], $learningGoal['learningGoalSlug']])}}" data-updateRoute="{{ route('students.learning-goals.start') }}" data-subjectId="{{$learningGoal['subjectId']}}" data-licenseID="{{$learningGoal['licenseId']}}" data-learningGoalId="{{$learningGoal['learningGoalId']}}" data-updateStatus="{{config('constants.STARTED')}}">
                                        {{__('students.begin_button')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @elseif($learningGoal['status'] === config('constants.UNAVAILABLE'))
                        <div class="tile__item tile__item--inactive" style="pointer-events: none;">
                            <div class="tile__inner">
                                <div class="tile__subtitle">
                                    {{ $subjectName }} {{ $learningGoal['order'] }}
                                </div>
                                <div class="tile__title">
                                    {{ $learningGoal['name'] }}
                                </div>
                            </div>
                        </div>
                    @elseif($learningGoal['status'] === config('constants.STARTED'))
                        <div class="tile__item tile__item--active">
                            <div class="tile__inner">
                                <div class="tile__subtitle">
                                    {{__('admins.learning_goal')}} {{ $learningGoal['order'] }}
                                </div>
                                <div class="tile__title">
                                    {{ $learningGoal['name'] }}
                                </div>
                                <div class="tile__hover">
                                    <a href="{{route('students.learning-goals.show', [$learningGoal['licenseId'], $learningGoal['subjectSlug'], $learningGoal['learningGoalSlug']])}}" class="button">
                                        {{__('students.continue_button')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @elseif($learningGoal['status'] === config('constants.FINISHED'))
                        <div class="tile__item tile__item--done">
                            <div class="tile__inner">
                                <div class="tile__subtitle">
                                    {{__('admins.learning_goal')}} {{ $learningGoal['order'] }}
                                </div>
                                <div class="tile__title">
                                    {{ $learningGoal['name'] }}
                                </div>
                                <div class="tile__hover">
                                    <a href="{{route('students.learning-goals.show', [$learningGoal['licenseId'], $learningGoal['subjectSlug'], $learningGoal['learningGoalSlug']])}}" class="button">
                                        {{__('students.improve_button')}}
                                    </a>
                                </div>
                                <div class="tile__result">
                                    {{$learningGoal['finished_score']}}
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        @include('includes.student-progress')
    </div>
    <br><br>
@endsection

@section('scripts')
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/javascript" src="{{asset('js/students.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/number-formatting.js').'?v='.$version}}"></script>
@endsection
