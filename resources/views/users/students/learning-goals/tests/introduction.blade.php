@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
    @include('includes.learning-goal-test-breadcumbs')
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8">
            <div class="title title--page">
                <span>
                    {{__('admins.learning_goals')}} {{ $learningGoalOrder }} : {{ $learningGoalName }}
                </span>
            </div>
            <div id="questionDiv" data-introductionQuestion="true">
            </div>
            <div class="content__block content__block--border">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="score">
                            <strong>{{__('students.attempts')}}:</strong>
                            <div class="score__item">&nbsp;</div>
                            <div class="score__item">&nbsp;</div>
                        </div>
                    </div>
                    @include('includes.learning-goal-test-buttons')
                </div>
            </div>
            @include('includes.student-help')
        </div>
        @include('includes.student-progress')
        <input type="hidden" id="subjectSlug" value="{{$subjectSlug}}">
    </div>
    <br><br>
@endsection

@include('includes.learning-goal-test-scripts')
