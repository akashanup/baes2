@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
    @include('includes.learning-goal-test-breadcumbs')
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-8">
            <div class="title title--page">
                <span>
                    {{__('admins.learning_goals')}} {{ $learningGoalOrder }} : {{ $learningGoalName }}
                </span>
                <small class="pull-right">{{__('admins.question_level')}}:
                    <strong class="baesLevel" id="questionLevelText"></strong>
                </small>
            </div>
            <div id="questionDiv">
            </div>
            <div class="content__block content__block--border">
                <div class="row">
                    <div class="col-xs-5">
                        <div class="score">
                            <strong>{{__('students.attempts')}}:</strong>
                            <div class="score__item">10</div>
                            <div class="score__item">8</div>
                            <div class="score__item">5</div>
                            <div class="score__item">2</div>
                        </div>
                    </div>
                    <div id="mortarboardDiv" class="col-xs-2 hidden">
                        <img src="{{asset('/images/mortarboard.png')}}" class="mortarboard" alt="" width="75px">
                    </div>
                    @include('includes.learning-goal-test-buttons')
                </div>
            </div>
            @include('includes.student-help')
        </div>
        @include('includes.student-progress')
        <input type="hidden" id="subjectSlug" value="{{$subjectSlug}}">
        <div id="learningGoalFinishedModalData"></div>
    </div>
    <br><br>
@endsection

@include('includes.learning-goal-test-scripts')
