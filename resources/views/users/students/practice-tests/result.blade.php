@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('students.practiceTests.index', [$licenseId, config('constants.ALL')])}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('students.exercise_test')}} {{__('students.overview')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascrip:void(null)" title="" class="breadcrumbs__link">
                <span itemprop="name">{{ $studentPracticeTest->name }}</span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="title title--page">
                {{__('students.exercise_test')}}
            </div>
            <div class="row content-blocks mt-40">
                <div class="col-xs-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="75%">
                                    <span class="big">{{__('admins.questions')}}</span>
                                </th>
                                <th class="text-center">{{__('students.level')}}</th>
                                <th class="text-center">
                                    {{__('students.good')}} / {{__('students.error')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{__('students.result')}}</td>
                                <td></td>
                                <td class="text-center">{{$correctAnswersCount}} / {{$practiceTestQuestionsCount}}</td>
                            </tr>
                            @foreach($practiceTestQuestions as $key => $practiceTestQuestion)
                                @if($practiceTestQuestion->pivot->is_correct === config('constants.YES'))
                                    <tr>
                                        <td>
                                            <a href="javascript:void(null)" class="js__overlay-show overlayLink overlayBtn" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                                                {{$key+1}}. {{$practiceTestQuestion->learningGoalName}}
                                            </a>

                                        </td>
                                        <td class="text-center">
                                            {{$practiceTestQuestion->level}}
                                        </td>
                                        <td class="text-center">
                                            <span>
                                                <div class="result result--round result--green"></div>
                                            </span>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>
                                            <a href="javascript:void(null)" class="js__overlay-show overlayLink overlayBtn" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                                                {{$key+1}}. {{$practiceTestQuestion->learningGoalName}}
                                            </a>

                                        </td>
                                        <td class="text-center">
                                            {{$practiceTestQuestion->level}}
                                        </td>
                                        <td class="text-center">
                                            <span>
                                                <div class="result result-- result--red"></div>
                                            </span>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!--Questions overlay-->
            @if($studentPracticeTest->feedback_type === config('constants.END_OF_TEST'))
                @foreach($practiceTestQuestions as $key => $practiceTestQuestion)
                    <div class="overlay" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                        <div class="overlay__bg"></div>
                        <div class="container">
                            <div class="overlay__inner">
                                <div class="overlay__scroll">
                                    <div class="overlay__title">
                                        {{__('admins.question')}}
                                        <span class="pull-right text-small">{{__('admins.question_level')}}: {{$practiceTestQuestion->level}}</span>
                                    </div>
                                    <div class="block__table">
                                        {!! $practiceTestQuestion->question !!}
                                    </div>
                                    <div class="block__table">
                                        {!! $practiceTestQuestion->hint !!}
                                    </div>
                                    <div class="block__table">
                                        <h4>{{__('students.correct_answer')}}</h4>
                                        {!! $practiceTestQuestion->answer !!}
                                    </div>
                                    <div class="block__table">
                                        <h4>{{__('students.student_answer')}}</h4>
                                        {!! $practiceTestQuestion->pivot->answer !!}
                                    </div>
                                    <button class="button pull-right js__overlay-close" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                                        {{__('globals.close')}}
                                    </button>
                                </div>
                                <div class="overlay__close js__overlay-close" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                                    <i class="icon-close"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @elseif($studentPracticeTest->feedback_type === config('constants.PARTLY'))
                @foreach($practiceTestQuestions as $key => $practiceTestQuestion)
                    <div class="overlay" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                        <div class="overlay__bg"></div>
                        <div class="container">
                            <div class="overlay__inner">
                                <div class="overlay__scroll">
                                    <div class="overlay__title">
                                        {{__('admins.question')}}
                                        <span class="pull-right text-small">{{__('admins.question_level')}}: {{$practiceTestQuestion->level}}</span>
                                    </div>
                                    <div class="block__table">
                                        {!! $practiceTestQuestion->question !!}
                                    </div>
                                    <div class="block__table">
                                        <h4>{{__('students.correct_answer')}}</h4>
                                        {!! $practiceTestQuestion->answer !!}
                                    </div>
                                    <div class="block__table">
                                        <h4>{{__('students.student_answer')}}</h4>
                                        {!! $practiceTestQuestion->pivot->answer !!}
                                    </div>
                                    <button class="button pull-right js__overlay-close" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                                        {{__('globals.close')}}
                                    </button>
                                </div>
                                <div class="overlay__close js__overlay-close" data-overlay="question{{$practiceTestQuestion->pivot->question_answer_id}}">
                                    <i class="icon-close"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    <br><br>
@endsection

@include('includes.learning-goal-test-scripts')
