@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('students.practiceTests.index', [$licenseId, config('constants.ALL')])}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('students.exercise_test')}} {{__('students.overview')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascrip:void(null)" title="" class="breadcrumbs__link">
                <span itemprop="name">{{ $practiceTestQuestion['name'] }}</span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-8">
                <div class="title title--page">
                    <span>
                        {{__('admins.learning_goals')}} {{ $practiceTestQuestion['learningGoalOrder'] }} : {{ $practiceTestQuestion['learningGoalName'] }}
                    </span>
                    <small class="pull-right">
                        {{__('admins.question_level')}}: <strong>{{$practiceTestQuestion['level']}}</strong>
                    </small>
                </div>
                <div id="questionDiv">
                    {!! $practiceTestQuestion['question'] !!}
                    <div class="content__block content__block--border">
                        <div class="row">
                            <div class="col-xs-4 pull-right" id="answerBtnDiv">
                                <button id="practiceTestAnswerBtn" class="button pull-right js__save-answer" data-savePracticeAnswerRoute="{{route('students.practiceTests.storeAnswer', [$licenseId, $practiceTestSlug])}}" data-questionId="{{$practiceTestQuestion['pivot']['question_answer_id']}}">{{__('students.answer_button')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="progress">
                    <div class="progress__inner">
                        <div class="progress__title title js__progress-title">
                            {{$practiceTestQuestion['name']}}
                        </div>
                        @if($studentPracticeTestTime)
                            <div class="progress__item">
                                <div class="progress__subtitle">
                                    <small>{{__('students.time_left')}}:</small>
                                    <span id="timer" class="pull-right"></span>
                                    <span class="pull-right hidden"></span>
                                </div>
                            </div>
                        @endif
                        <div class="progress__item">
                            <div class="progress__subtitle">
                                <small>{{__('students.ansering_question')}}:</small>
                                <span class="pull-right">{{$answeredQuestions}} / {{$practiceTestQuestionsCount}}</span>
                            </div>
                            <div class="progress__bar">
                                <span class="progress__value js__progress-bar" style="width: {{($answeredQuestions/$practiceTestQuestionsCount)*100}}%;"></span>
                                <i>{{$answeredQuestions}}</i>
                            </div>
                        </div>
                        <div class="progress__item">
                            <div class="progress__subtitle">
                                <small>{{__('students.ansering_question')}}</small>
                            </div>
                            <div class="progress__result js__progress-result text-left">
                                @foreach($practiceTestQuestions as $key => $practiceTestQuestion)
                                    @if($practiceTestQuestion->pivot->answer)
                                        <a href="{{route('students.practiceTests.test', [$licenseId, $practiceTestSlug, $practiceTestQuestion->pivot->question_answer_id])}}" data-status="done" class="questionLink progress__score done">{{$key + 1}}</a>
                                    @else
                                        <a href="{{route('students.practiceTests.test', [$licenseId, $practiceTestSlug, $practiceTestQuestion->pivot->question_answer_id])}}" data-status="undone" class="questionLink progress__score">{{$key + 1}}</a>
                                    @endif

                                @endforeach
                            </div>
                        </div>
                        <div class="row text-center mt-30">
                            <button type="button" class="button" id="submitPracticeTestBtn" data-route="{{route('students.practiceTests.submit', [$licenseId, $practiceTestSlug])}}">
                                {{__('students.submit_test')}}
                            </button>
                            <a href="{{route('students.practiceTests.index', [$licenseId, config('constants.ALL')])}}"
                            type="button" class="button">
                                {{__('globals.save')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="practiceTestSlug" value="{{$practiceTestSlug}}">
        <input type="hidden" id="minutesTxt" value="{{__('students.minutes')}}">
        <input type="hidden" id="timeLeft" value="{{$studentPracticeTestTime}}">
    </div>
    <br><br>
@endsection

@section('scripts')
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/javascript" src="{{asset('js/students.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/number-formatting.js').'?v='.$version}}"></script>
    @if($studentPracticeTestTime)
        <script type="text/javascript" src="{{asset('js/timer.js').'?v='.$version}}"></script>
    @endif
@endsection


@section('modals')
    <div id="unansweredQuestionModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12" style="text-align: center;">
                        <h3 style="margin: 20px;">{{__('students.unansweredQuestionMessage')}}</h3>
                        <div class="col-xs-8 col-xs-offset-2">
                            <button type="button" class="button" id="confirmSubmitPracticeTestBtn" data-route="{{route('students.practiceTests.submit', [$licenseId, $practiceTestSlug])}}">
                                {{__('students.submit_test')}}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
