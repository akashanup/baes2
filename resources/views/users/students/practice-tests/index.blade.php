@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('students.exercise_test')}} {{__('students.overview')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="title title--page">{{__('students.school_exercise_test')}}</div>
            @if(count($schoolPracticeTests))
                @foreach($schoolPracticeTests as $practiceTest)
                    @if($practiceTest['status'] === config('constants.FINISHED'))
                        <div class="col-xs-12">
                            <div class="content__block content__block--green list-blocked" style="background-image: url({{asset('images/achtergrond3.png')}}); background-repeat:repeat-y;">
                                <div class="block__inner block__inner--small"  style="color: black;">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['name']}}
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="{{route('students.practiceTests.result', [$licenseId, $practiceTest['slug']])}}" class="button button--secondary pull-right">
                                                {{__('students.view_result')}}
                                            </a>
                                        </div>
                                        <div class="col-xs-2 text-right">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['score']}}%
                                            </span>
                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <div class="list-blocked__timeframe">
                                                <p class=""><strong>{{__('globals.start')}}:</strong> {{$practiceTest['start_date']}}</p>
                                                <p class=""><strong>{{__('globals.end')}}:</strong> {{$practiceTest['end_date']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    @elseif($practiceTest['status'] === config('constants.STARTED'))
                        <div class="col-xs-12">
                            <div class="content__block list-blocked" style="background-image: url({{asset('images/achtergrond3_gray.png')}}); background-repeat:repeat-y;">
                                <div class="block__inner block__inner--small"  style="color: black;">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['name']}}
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="{{route('students.practiceTests.test', [$licenseId, $practiceTest['slug']])}}" class="button button--secondary pull-right">
                                                {{__('students.continue_button')}}
                                            </a>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <div class="list-blocked__timeframe">
                                                <p class=""><strong>{{__('globals.start')}}:</strong> {{$practiceTest['start_date']}}</p>
                                                <p class=""><strong>{{__('globals.end')}}:</strong> {{$practiceTest['end_date']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    @elseif($practiceTest['status'] === config('constants.AVAILABLE'))
                        <div class="col-xs-12">
                            @if($practiceTest['testStatus'] === config('constants.FINISHED'))
                                <div class="content__block content__block--inactive list-blocked" style="background-color:grey; background-repeat:repeat-y;">
                            @else
                                <div class="content__block content__block--inactive list-blocked" style="background-image: url({{asset('images/achtergrond3_lightgray.png')}}); background-repeat:repeat-y;">
                            @endif
                                <div class="block__inner block__inner--small"  style="color: black;">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['name']}}
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="javascript:void(null)" data-redirectRoute="{{route('students.practiceTests.test', [$licenseId, $practiceTest['slug']])}}" class="button button--secondary pull-right updateStudentPracticeTestStatusBtn" data-updateRoute="{{route('students.practiceTests.updateStatus', [$licenseId, $practiceTest['slug']])}}">
                                                {{__('students.make_test')}}
                                            </a>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <div class="list-blocked__timeframe">
                                                <p class=""><strong>{{__('globals.start')}}:</strong> {{$practiceTest['start_date']}}</p>
                                                <p class=""><strong>{{__('globals.end')}}:</strong> {{$practiceTest['end_date']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    @endif
                @endforeach
            @else
                <h1 style="text-align: center;">{{__('students.no_active_test_found')}}</h1>
            @endif
        </div>
        <div class="col-xs-12">
            <div class="title title--page">{{__('students.my_exercise_test')}}</div>
            @if(count($studentPracticeTests))
                @foreach($studentPracticeTests as $practiceTest)
                    @if($practiceTest['status'] === config('constants.FINISHED'))
                        <div class="col-xs-12">
                            <div class="content__block content__block--green list-blocked" style="background-image: url({{asset('images/achtergrond3.png')}}); background-repeat:repeat-y;">
                                <div class="block__inner block__inner--small"  style="color: black;">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['name']}}
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="{{route('students.practiceTests.result', [$licenseId, $practiceTest['slug']])}}" class="button button--secondary pull-right">
                                                {{__('students.view_result')}}
                                            </a>
                                        </div>
                                        <div class="col-xs-2 text-right">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['score']}}%
                                            </span>
                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <div class="list-blocked__timeframe">
                                                <p class=""><strong>{{__('globals.start')}}:</strong> {{$practiceTest['start_date']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    @elseif($practiceTest['status'] === config('constants.STARTED'))
                        <div class="col-xs-12">
                            <div class="content__block list-blocked" style="background-image: url({{asset('images/achtergrond3_gray.png')}}); background-repeat:repeat-y;">
                                <div class="block__inner block__inner--small"  style="color: black;">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['name']}}
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="{{route('students.practiceTests.test', [$licenseId, $practiceTest['slug']])}}" class="button button--secondary pull-right">
                                                {{__('students.continue_button')}}
                                            </a>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <div class="list-blocked__timeframe">
                                                <p class=""><strong>{{__('globals.start')}}:</strong> {{$practiceTest['start_date']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    @elseif($practiceTest['status'] === config('constants.AVAILABLE'))
                        <div class="col-xs-12">
                            @if($practiceTest['status'] === config('constants.FINISHED'))
                            <div class="content__block content__block--inactive list-blocked" style="background-color:grey; background-repeat:repeat-y;">
                            @else
                            <div class="content__block content__block--inactive list-blocked" style="background-image: url({{asset('images/achtergrond3_lightgray.png')}}); background-repeat:repeat-y;">
                            @endif
                                <div class="block__inner block__inner--small"  style="color: black;">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <span class="list-blocked__title">
                                                {{$practiceTest['name']}}
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="javascript:void(null)" data-redirectRoute="{{route('students.practiceTests.test', [$licenseId, $practiceTest['slug']])}}" class="button button--secondary pull-right updateStudentPracticeTestStatusBtn" data-updateRoute="{{route('students.practiceTests.updateStatus', [$licenseId, $practiceTest['slug']])}}">
                                                {{__('students.make_test')}}
                                            </a>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <div class="list-blocked__timeframe">
                                                <p class=""><strong>{{__('globals.start')}}:</strong> {{$practiceTest['start_date']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    @endif
                @endforeach
            @else
                <h1 style="text-align: center;">{{__('students.no_active_test_found')}}</h1>
            @endif
        </div>
    </div>
    <br><br>
@endsection
@include('includes.learning-goal-test-scripts')
