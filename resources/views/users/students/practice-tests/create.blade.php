@extends('layouts.master')

@section('header')
    @include('includes.student-header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('users.create_a_test')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
		<div class="col-xs-12">
			<div class="col-xs-8">
		        <table id="practiceTestDetailTable" class="table table-baes table-hover">
		                    <thead>
			                    <tr>
			                        <th class='text-center'>{{__('teachers.question')}}</th>
			                        <th class='text-center'>{{__('teachers.block')}}</th>
			                        <th class='text-center'>{{__('admins.learning_goal')}}</th>
			                        <th class='text-center'>{{__('admins.complexity')}}</th>
			                        <th class='text-center'>{{__('teachers.random')}}</th>
			                        <th class='text-center'></th>
			                    </tr>
		                    </thead>
		                    <tbody>
			                    <tr class="hidden" id="hidden_row">
			                        <td class='text-center'>0</td>
			                        <td class='subjectField text-center'>
			                            <select name="subject_selected"  class="practiceTestDetailSubject form-control" required="required">
			                                <option value=""></option>
			                                @foreach($subjects as $subject)
			                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
			                                @endforeach
			                            </select>
			                        </td>
			                        <td class='learningGoalField text-center'>
			                            <select name="goal_selected"  class="subjectLearningGoal form-control" required="required"></select>
			                        </td>
			                        <td class='complexityField text-center'>
			                            <select name="level_selected" class="complexityLevel form-control" required="required">
			                                @foreach(config('constants.COMPLEXITY') as
			                                    $key => $complexity)
			                                    @if(intval($key) !== 0)
			                                        <option value={{$key}}>{{$complexity}}</option>
			                                    @endif
			                                    @endforeach
			                            </select>
			                        </td>
			                        <td class='randomField text-center'>
			                            <input name="random_selected" type="checkbox" value="">
			                        </td>
			                        <td class='text-center'>
			                            <button class="button button--quaternary deletePracticeTestQuestionBtn">
			                                <i class="fa fa-trash"></i>
			                            </button>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td class='text-center'>1</td>
			                        <td class='subjectField text-center'>
			                            <select name="subject_selected"  class="practiceTestDetailSubject form-control" required="required">
			                                <option value=""></option>
			                                @foreach($subjects as $subject)
			                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
			                                @endforeach
			                            </select>

			                        </td>

			                        <td class='learningGoalField text-center'>
			                            <select name="goal_selected"  class="subjectLearningGoal form-control" required="required"></select>
			                        </td>
			                        <td class='complexityField text-center'>
			                            <select name="level_selected"  class="complexityLevel form-control" required="required">
			                                 @foreach(config('constants.COMPLEXITY') as
			                                    $key => $complexity)
			                                    @if(intval($key) !== 0)
			                                        <option value={{$key}}>{{$complexity}}</option>
			                                    @endif
			                                @endforeach
			                            </select>
			                        </td>
			                        <td class='randomField text-center'>
			                            <input name="random_selected" type="checkbox" value="">
			                        </td>
			                        <td class='text-center'>
			                            <button class="button button--quaternary deletePracticeTestQuestionBtn">
			                                <i class="fa fa-trash"></i>
			                            </button>
			                        </td>
			                    </tr>
		                    </tbody>
		                </table>
		                <div class="col-lg-offset-5 col-lg-6">
		                	<button class="button button--quaternary" id="addPracticeTestQuestionBtn"> {{__('teachers.add_a_row')}}
		                	</button>
		                </div>
		    </div>
		    <div class="col-xs-4">
		        <div class="progress mt-60 mb-60">
		            <div class="progress__inner">
		                <div class="progress__title title js__progress-title">
		                    {{__('teachers.practice_key_making')}}
		                </div>
		                <div class="form-baes">
		                    <div class="form-group">
		                        <label for="">{{__('teachers.excercise_key')}}</label>
		                        <input type="text" class="form-control" id="testTemplateName" name="name_selected" placeholder="{{__('teachers.name')}} {{__('teachers.excercise_key')}}" required>
		                    </div>
		                    <div class="mt-20 text-center">
		                          <button id="savePracticeTestBtn" data-route="{{route('students.practiceTests.store', [$licenseId, $courseSlug])}}" class="button">{{__('teachers.make_available')}}
		                          </button>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		    <input type="hidden" id="subjectLearningGoal" value="{{route('teachers.subjectlearningGoal')}}">
        </div>
    </div>
    <br><br>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('js/student-practice-tests.js').'?v='.$version}}"></script>
@endsection
