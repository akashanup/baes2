@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
    	<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('globals.edit_profile')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8">
            <div class="title title--page">
                {{__('users.profile')}}
            </div>
            <div class="intro">
            	<form action="{{route('users.update', $user['slug'])}}" method="POST" role="form" enctype="multipart/form-data" id="edit_profile_form">
					<input type="hidden" name="_method" value="PUT">
					{{ csrf_field() }}
					<div class="form-group">
						<div class="row">
							<div class="col-xs-4">
								<img id="profile_picture" src="{{$user['image']}}" alt="{{$user['first_name']}}" class="img-circle edit_profile_picture" title="{{__('users.change_photo')}}">
							</div>
							<div class="col-xs-4 mt-20" style="display: none">
								<label for="file_upload">{{__('users.profile_photo')}}</label>
								<input id="file_upload" type="file" class="form-control" name="image">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="">{{__('users.first_name')}}</label>
						<input type="text" class="form-control" value="{{$user['first_name']}}" name="first_name" required maxlength="50">
					</div>
					<div class="form-group">
						<label for="">{{__('users.last_name')}}</label>
						<input type="text" class="form-control" value="{{$user['last_name']}}" name="last_name" required maxlength="50">
					</div>
					<div class="form-group">
						<label for="">{{__('users.phone_number')}}</label>
						<input type="text" class="form__input form__input--profile js__login-name-control" name="phone" id="phone" placeholder="+00 0 00 00 00 00" required value="{{$user['phone']}}"  maxlength="17">
					</div>
					<div class="form-group mt-40 text-right">
						<button type="submit" class="button submitBtn">{{__('globals.save')}}</button>
					</div>
				</form>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection
@section('scripts')
    @include('includes.auth-scripts')
@endsection
