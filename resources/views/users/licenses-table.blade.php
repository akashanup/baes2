<table class="table table-responsive table-bordered table-hover">
	<thead>
		<tr>
            <th width="25%">{{__('globals.institution')}}</th>
            <th width="25%">{{__('globals.course')}}</th>
            <th width="10%">{{__('users.code')}}</th>
            <th width="20%">{{__('globals.end_date')}}</th>
            <th width="10%">{{__('globals.type')}}</th>
            <th width="10%">{{__('admins.action')}}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($licenses as $license)
			<tr>
	            <td>{{$license->institution}}</td>
	            <td>{{$license->course}}</td>
	            <td>{{$license->code}}</td>
	            <td>{{$license->end_date}}</td>
	            <td>
	            	@if($license->type === config('constants.MONTHLY_PAYMENT'))
						<span>{{__('globals.monthly')}}</span>
					@else
						<span>{{__('globals.fixed')}}</span>
					@endif
				</td>
	            <td>
	            	@if($license->status === config('constants.ACTIVE'))
		            	<a href="javascript:void(null)" class="col-xs-6 changeUserLicenseBtn" data-toggle="tooltip" title="{{__('admins.deactivate')}}" data-route="{{route('users.changeLicense', [$license->user_id, $license->license_id])}}">
						    <i class="fa fa-ban" aria-hidden="true"></i>
						</a>
					@else
						<a href="javascript:void(null)" class="col-xs-6 changeUserLicenseBtn" data-toggle="tooltip" title="{{__('admins.activate')}}"  data-route="{{route('users.changeLicense', [$license->user_id, $license->license_id])}}">
						    <i class="fa fa-check" aria-hidden="true"></i>
						</a>
					@endif
	            	@if($license->role === config('constants.STUDENT'))
		            	<a href="javascript:void(null)" class="col-xs-6 activityBtn" data-toggle="tooltip" title="{{__('admins.activity')}}" data-id="{{$license->user_id.'-'.$license->license_id}}">
						    <i class="fa fa-bar-chart" aria-hidden="true"></i>
						</a>
					@endif
	            </td>
			</tr>
			@if($license->role === config('constants.STUDENT'))
				<tr class="hidden" id="activity{{$license->user_id.'-'.$license->license_id}}" data-status="hidden">
					<td colspan="6">
						<table class="table table-responsive table-bordered table-hover">
							<thead>
								<tr>
									<thead>
										<td>{{__('users.login_date')}}</td>
										<td>{{__('users.login_time')}}</td>
										<td>{{__('users.activity')}}</td>
									</thead>
								</tr>
							</thead>
							<tbody>
								@foreach($license->activity->toArray() as $activity)
									<tr>
										<td>{{$activity['date']}}</td>
										<td>{{$activity['time']}}</td>
										<td>{{$activity['points']}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>
			@endif
		@endforeach
	</tbody>
</table>
