<a href="{{route('subjects.edit', $subject['slug'])}}" class="col-xs-6" data-toggle="tooltip" title="{{__('globals.edit')}}">
    <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
</a>
<a href="javascript:void(0)" class="col-xs-6 deleteSubjects" data-route="{{route('subjects.destroy', $subject['slug'])}}" data-toggle="tooltip" title="{{__('admins.delete')}}">
    <i class="fa fa-close fa-2x" aria-hidden="true"></i>
</a>
