@extends('layouts.master')

@section('title')
    {{__('admins.create_subject')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('subjects.index')}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage_subject')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.create_subject')}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <form action="{{route('subjects.store')}}" role="form">
                <div class="form-group">
                    <label for="name">{{__('users.name')}}</label>
                    <input type="text" class="form-control required" id="name" maxlength="100" placeholder="{{__('admins.subject')}}">
                </div>
                <div class="form-group">
                    <label for="curriculumId">{{__('admins.curriculum')}}</label>
                    <select id="curriculumId" class="js-example-basic-single select2 form-control" data-type="create">
                        @foreach($curriculums as $curriculum)
                            <option value="{{$curriculum['id']}}" data-route="{{route('curriculums.learning-goals', $curriculum['slug'])}}">{{$curriculum['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="learningGoals">
                </div>
                <div class="form-group mt-40 text-right">
                    <button type="button" id="saveSubjectBtn" data-request-type="post" data-learningGoalChecked="1" class="button submitBtn">
                    {{__('globals.save')}}</button>
                </div>
            </form>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.sortable.min.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/subject.js').'?v='.$version}}"></script>
@endsection
