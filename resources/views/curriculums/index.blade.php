@extends('layouts.master')

@section('title')
    {{__('admins.manage')}} {{__('admins.curriculum')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection


@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage')}} {{__('admins.curriculum')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a class="button button--quaternary mb-20" href="{{route('curriculums.create')}}">
                {{__('admins.create_curriculum')}}
            </a>
        </div>
    </div>

    <div class="row mt-30">
        <div class="col-xs-12">
            <table id="curriculum-table" class="display table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">ID</th>
                        <th width="50%">{{__('admins.curriculum')}}</th>
                        <th width="20%">{{__('admins.learning_goal_count')}}</th>
                        <th width="20%">{{__('admins.action')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <hr class="section-divider"/>
    <input type="hidden" id="curriculumsRoute" value="{{route('curriculums.indexData')}}">
@endsection


@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/curriculum.js').'?v='.$version}}"></script>
@endsection

@section('modals')
<div id="deleteCurriculumModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h5>{{__('admins.delete_curriculum_warning')}}</h5>
                <label>{{__('admins.delete_curriculum_effect')}}</label>
                <br>
                <span style="color: red" class="center-block">{{__('admins.delete_curriculum_instruction')}}</span>
                <br>
                <input type="text" id="confirmDeleteCurriculumInpt" placeholder="DELETE">
                <br>
                <br>
                <button type="button" id="confirmDeleteCurriculumBtn" class="button center-block">{{__('admins.delete')}}</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection
