@if(!empty($curriculumName) && !empty($curriculumRoute))
	<a href="{{ $curriculumRoute }}">{{$curriculumName}}</a>
@endif

@if(!empty($curriculumEditRoute))
	<a href="{{ $curriculumEditRoute }}" class="col-xs-6" data-toggle="tooltip" title="{{__('globals.edit')}}">
	    <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
	</a>
	<a href="javascript:void(0)" class="col-xs-6 deleteCurriculum" data-route="{{$curriculumDeleteRoute}}" data-toggle="tooltip" title="{{__('admins.delete')}}">
	    <i class="fa fa-close fa-2x" aria-hidden="true"></i>
	</a>
@endif
