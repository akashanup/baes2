@extends('layouts.master')

@section('title')
    {{__('admins.edit_curriculum')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('curriculums.index')}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.edit_curriculum')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$curriculum['name']}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <form action="{{route('curriculums.update', $curriculum['slug'])}}" role="form">
                <input type="hidden" name="_method" value="put">
                <div class="form-group">
                    <label for="name">{{__('users.name')}}</label>
                    <input type="text" class="form-control required" id="name" maxlength="100" value="{{$curriculum['name']}}">
                </div>
                <div class="form-group" id="learningGoals">
                    <label for="name">{{__('admins.learning_goals')}}</label>
                    @include('includes.available-existing-learning-goals')
                </div>
                <div class="form-group mt-40 text-right">
                    <button type="button" id="saveCurriculumBtn" data-request-type="put" data-learningGoalChecked="0" class="button submitBtn">
                    {{__('globals.save')}}</button>
                </div>
            </form>
        </div>
    </div>
    <input type="hidden" id="existingLearningGoalIds" value="{{json_encode($existingLearningGoalIds)}}">
    <hr class="section-divider"/>
@endsection

@section('modals')
    <div id="removeLearningGoalModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="text-align: center;">
                <div class="modal-body">
                    <h5>{{__('admins.remove_curriculum_learning_goal')}}</h5>
                    <br>
                    <span style="color: red">Type REMOVE</span>
                    <input type="text" id="confirmRemoveLearningGoalInpt" placeholder="REMOVE">
                    <button type="button" id="confirmRemoveLearningGoalBtn" class="button">{{__('admins.update')}}</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.sortable.min.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/curriculum.js').'?v='.$version}}"></script>
@endsection
