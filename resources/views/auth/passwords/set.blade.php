@extends('layouts.master')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{asset('css/profile.css').'?v='.$version}}">
@endsection

@section('header')
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    @include('includes.logo')
                </div>
                <div class="col-xs-2">
                </div>
                <div class="col-xs-4">
                    <div class="profile profile--login">
                        <div class="profile__inner">
                            <div class="profile__head">
                            </div>
                            <div class="profile__slide">
                                <div class="title">
                                    {{__('authentication.save_password')}}
                                </div>
                                <form class="form form--login" method="post" action="{{ route('users.savePassword', $token)}}">
                                    {{ csrf_field() }}
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('authentication.email')}}</label>
                                        <input type="email" class="form__input form__input--profile js__login-name" name="email" required  maxlength="50">
                                    </div>
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('authentication.password')}}</label>
                                        <input type="password" class="form__input form__input--profile js__login-name" name="password" required>
                                    </div>
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('authentication.cnf_password')}}</label>
                                        <input type="password" class="form__input form__input--profile js__login-name" name="password_confirmation" required>
                                    </div>
                                    <div class="profile__buttons form__item">
                                        <br>
                                        <button type="submit" class="button submitBtn" id="submitBtn">
                                            {{__('authentication.save_password')}}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="pattern">
    </div>
@endsection
@section('content')
    <hr class="section-divider section-divider--login"/>
    <div class="row">
        @include('includes.slogan')
        <div class="col-xs-5">
            @include('includes.baes-informations')
        </div>
    </div>
    <hr class="section-divider"/>
    <br>
@endsection
@section('scripts')
    @include('includes.auth-scripts')
@endsection
