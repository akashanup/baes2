@extends('layouts.master')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{asset('css/profile.css').'?v='.$version}}">
@endsection

@section('header')
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    @include('includes.logo')
                </div>
                <div class="col-xs-2">
                </div>
                <div class="col-xs-4">
                    <div class="profile profile--login">
                        <div class="profile__inner" style="height: 825px !important;">
                            <div class="profile__head">
                            </div>
                            <div class="profile__slide">
                                <div class="title">
                                    {{__('authentication.register')}}
                                </div>
                                <form class="form form--login" method="post" action="{{route('register')}}">
                                    {{ csrf_field() }}
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('users.first_name')}}</label>
                                        <input type="text" class="form__input form__input--profile js__login-name" value="{{old('first_name')}}" name="first_name" required maxlength="50">
                                    </div>
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('users.last_name')}}</label>
                                        <input type="text" class="form__input form__input--profile js__login-name" value="{{old('last_name')}}" name="last_name" required  maxlength="50">
                                    </div>
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('authentication.email')}}</label>
                                        <input type="email" class="form__input form__input--profile js__login-name" value="{{old('email')}}" name="email" required  maxlength="50">
                                    </div>
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('users.confirm_email')}}</label>
                                        <input type="email" class="form__input form__input--profile js__login-name" name="email_confirmation" required  maxlength="50">
                                    </div>
                                    <div class="form__item">
                                        <label for="" class="form__label">{{__('users.phone_number')}}</label>
                                        <input type="text" class="form__input form__input--profile js__login-name-control" name="phone" id="phone" placeholder="+00 0 00 00 00 00" required  maxlength="17">
                                    </div>
                                    <div class="form__item" style="padding: 8px; margin: 20px 0 0;">
                                        <input type="checkbox" name="privacy" id="privacy" required> {{__('globals.privacy1')}}
                                        <a href="javascript:void(0)" class="js__overlay-show link-design" data-overlay="privacy"><strong>{{__('globals.privacy2')}}</strong></a>{{__('globals.privacy3')}}
                                    </div>
                                    <div class="profile__buttons form__item">
                                        <button type="submit" class="button submitBtn" id="submitBtn">
                                            </i>{{__('authentication.register')}}
                                        </button>
                                        <a href="{{route('login')}}" class="link-design col-xs-offset-3">
                                            {{__('globals.already_have_account')}}
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="pattern">
    </div>
@endsection
@section('content')
    <hr class="section-divider section-divider--login"/>
    <div class="row" style="margin-bottom: 215px;">
        @include('includes.slogan')
        <div class="col-xs-5">
            @include('includes.baes-informations')
        </div>
    </div>
    <hr class="section-divider"/>
@endsection
@section('scripts')
    @include('includes.auth-scripts')
@endsection
