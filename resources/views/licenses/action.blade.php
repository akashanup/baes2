@if(!empty($courseName) && !empty($courseSlug))
	<a href="{{route('courses.edit', $courseSlug)}}">{{$courseName}}</a>
@endif

@if(!empty($subjectsCount) && !empty($courseSlug))
	<a href="{{route('courses.edit', $courseSlug)}}">{{$subjectsCount}}</a>
@endif

@if(!empty($end_date))
	@if($status)
		<span style="color: red">{{$end_date}}</span>
	@else
		<span style="color: green">{{$end_date}}</span>
	@endif
@endif

@if(!empty($licenseId) && !empty($courseId) && !empty($institutionId))
	<a href="javascript:void(0)" class="col-xs-6 editLicense" data-courseId="{{$courseId}}" data-institutionId="{{$institutionId}}" data-id="{{$licenseId}}" data-toggle="tooltip" title="{{__('globals.edit')}}">
	    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	</a>
	<a href="javascript:void(0)" class="col-xs-6 deleteLicense" data-route="{{route('licenses.destroy', $licenseId)}}" data-toggle="tooltip" title="{{__('admins.delete')}}">
	    <i class="fa fa-close" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($status))
	@if($status === config('constants.INACTIVE'))
		<span data-value="{{config('constants.INACTIVE')}}">{{__('globals.inactive')}}</span>
	@else
		<span data-value="{{config('constants.ACTIVE')}}">{{__('globals.active')}}</span>
	@endif
@endif

@if(!empty($type))
	@if($type === config('constants.MONTHLY_PAYMENT'))
		<span data-value="{{config('constants.MONTHLY_PAYMENT')}}">{{__('globals.monthly')}}</span>
	@else
		<span data-value="{{config('constants.FIXED_PAYMENT')}}">{{__('globals.fixed')}}</span>
	@endif
@endif

@if(!empty($courseSlug) && !empty($usersCount) && !empty($institutionSlug))
	<a href="{{route('users.index', [$institutionSlug, $courseSlug, $licenseId])}}">
        {{$usersCount}}
    </a>
@endif
