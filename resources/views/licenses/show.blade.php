@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('content')
    <div class="row">
    <div class="col-xs-12">
        <div class="title title--page title--underline-dark">
            <div class="row">
                <div class="col-md-4 mt-10">
                    {{__('globals.licenses')}}
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-2">
            <a href="{{route('users.availableLicenses', $license['code'] ? $license['code'] : '')}}">&lt; Terug naar overzicht</a>
        </div>
        <div class="col-md-3 product-detail-detail-block">
            <div class="content__block product product--text-center detailed__product">
                <div class="product__content">
                    <img src="{{$license['image']}}" alt="{{$license['courseName']}}" class="product__logo--school">
                    <h2 class="product__title">{{$license['courseName']}}</h2>
                    <div class="product__description product-detail">
                        @foreach($license['subjects'] as $subject)
                            <p>{{$subject}}</p>
                        @endforeach
                    </div>
                    <div class="product__price product-detail">
                        @if($license['licenseRole'] === config('constants.TEACHER'))
                            <div class="product__price__badge">
                              {{__('users.teacher')}}
                            </div>
                        @endif
                        @if($license['price'])
                            <p>{!!$license['price']!!}</p>
                        @else
                             <p>{{__('users.free')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 product-detail-detail-block">
            <div class="product-detail">
                <div class="title-block">
                    {{__('users.specifications')}}
                </div>
                <div class="row specifications">
                    <div class="col-md-6">
                        <p>
                            <strong>{{__('admins.course')}}</strong>
                            <br>
                            {{$license['courseName']}}
                        </p>
                        <p>
                            <strong>{{__('globals.year')}}</strong>
                            <br>
                            {{$license['year']}}
                        </p>
                        <p>
                            <strong>{{__('users.license_duration')}}</strong>
                            <br>
                            @if($license['type'] === config('constants.MONTHLY_PAYMENT'))
                                {{__('users.continuous')}}
                            @else
                                {{$license['licenseDuration']}}
                            @endif
                        </p>
                    </div>
                    <div class="col-md-6">
                        <strong>{{__('users.description')}}</strong>
                        <br>
                        <p class="specifications-description">
                            {{$license['description']}}
                        </p>
                    </div>
                    <div class="col-md-12">
                        <hr class="hr-dark">
                        <form id="purchaseLicenseForm" action="{{route('payments.makePayment')}}" method="POST" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" value ="{{$license['id']}}" name="licenseId" required />
                            <div class="col-md-6 no-gutter-left">
                                @if($license['institutionSlug'] === config('constants.BAES_INSTITUTION_SLUG')
                                    || $license['licenseRole'] === config('constants.TEACHER'))
                                    <input type="hidden" name="gradeId" value="{{$grades[0]['id']}}">
                                @else
                                    <div class="col-md-2 no-gutter-left">
                                        <div style="margin-top:5px;">
                                            <strong>{{__('users.grade')}}:</strong>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="">
                                            <select class="form-control select-klas" id="gradeId" name="gradeId" required>
                                                @foreach($grades as $grade)
                                                    <option value="{{$grade['id']}}">{{$grade['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6 no-gutter-right">
                                <button type="submit" id="purchaseLicenseButton" class="button btn-block submitBtn">{{__('users.purchase')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
