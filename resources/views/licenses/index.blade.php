@extends('layouts.master')

@section('title')
    {{__('admins.edit_licenses')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.edit_licenses')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="javascript:void(0)" id="createLicenseModalBtn" class="button button--quaternary mb-20">
                {{__('admins.create_license')}}
            </a>
        </div>
    </div>

    <div class="row  mt-30">
        <div class="col-xs-12">
            <table class="table table-bordered table-hover" id="license-table">
                <thead>
                    <tr>
                        <th width="30%">{{__('admins.course')}}</th>
                        <th width="10%">{{__('admins.institution')}}</th>
                        <th width="5%">{{__('users.code')}}</th>
                        <th width="5%">{{__('globals.price')}}</th>
                        <th width="5%">{{__('admins.registration_status')}}</th>
                        <th width="5%">{{__('admins.user_count')}}</th>
                        <th width="5%">{{__('admins.subject_count')}}</th>
                        <th width="5%">{{__('globals.validity')}}</th>
                        <th width="10%">{{__('globals.end_date')}}</th>
                        <th width="5%">{{__('globals.role')}}</th>
                        <th width="5%">{{__('globals.type')}}</th>
                        <th width="10%">{{__('admins.action')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <input type="hidden" id="homeRoute" value="{{route('home')}}">
    <input type="hidden" id="courseSlug" value="{{$courseSlug}}">
    <input type="hidden" id="institutionSlug" value="{{$institutionSlug}}">
    <hr class="section-divider"/>
    @include('includes.institution-course-dropdown')
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/licenses.js').'?v='.$version}}"></script>
@endsection

@section('modals')
    <div id="createLicenseModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <h1>{{__('admins.create_license')}}</h1>
                        <form role="form" action="{{ route('licenses.store') }}">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="course_id">{{__('admins.course')}}</label>
                                    <select class="form-control required courseName" id="courseSlct">
                                        @foreach($courses as $course)
                                            <option value="{{$course['id']}}" data-institutionsRoute="{{route('courses.institutions', $course['slug'])}}">{{$course['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="institution_id">{{__('admins.institution')}}</label><br>
                                    <select class="form-control required institutionName" id="institutionSlct">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">{{__('users.code')}}</label>
                                    <input type="text" class="form-control code" maxlength="25">
                                </div>
                                <div class="form-group">
                                    <label for="role">{{__('users.code')}} {{__('globals.for')}}</label>
                                    <select class="form-control required role">
                                        <option value="{{config('constants.STUDENT')}}">{{__('globals.for')}} {{__('users.student')}}</option>
                                        <option value="{{config('constants.TEACHER')}}">{{__('globals.for')}} {{__('users.teacher')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="price">{{__('globals.price')}}</label>
                                    <input type="number" class="form-control required price" min="0" value="0" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="status">{{__('admins.active_for_registration')}}</label>
                                    <select class="form-control required status">
                                        <option value="{{config('constants.ACTIVE')}}">{{__('globals.active')}}</option>
                                        <option value="{{config('constants.INACTIVE')}}">{{__('globals.inactive')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="type">{{__('admins.license_type')}}</label>
                                    <select class="form-control typeSlct required type">
                                        <option value="{{config('constants.FIXED_PAYMENT')}}">{{__('globals.fixed')}}</option>
                                        <option value="{{config('constants.MONTHLY_PAYMENT')}}">{{__('globals.monthly')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="validityInpt">{{__('globals.license_validity')}} ({{__('globals.days')}})</label>
                                    <input type="number" class="form-control validityInpt" min="0">
                                </div>
                                <div class="form-group">
                                    <label for="end_date">{{__('globals.end_date')}}</label>
                                    <input type="date" class="form-control endDate">
                                </div>
                                <div class="form-group">
                                    <br>
                                    <button type="button" id="storeLicenseBtn" class="button pull-right submitBtn">{{__('admins.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div id="editLicenseModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <h1>{{__('admins.edit_license')}}</h1>
                        <form role="form">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="courseName">{{__('admins.course')}}</label>
                                    <input type="text" class="form-control courseName required" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="institutionName">{{__('admins.institution')}}</label><br>
                                    <input type="text" class="form-control institutionName required" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="code">{{__('users.code')}}</label>
                                    <input type="text" class="form-control code" maxlength="25">
                                </div>
                                <div class="form-group">
                                    <label for="role">{{__('users.code')}} {{__('globals.for')}}</label>
                                    <select class="form-control role required">
                                        <option value="{{config('constants.STUDENT')}}">{{__('globals.for')}} {{__('users.student')}}</option>
                                        <option value="{{config('constants.TEACHER')}}">{{__('globals.for')}} {{__('users.teacher')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="price">{{__('globals.price')}}</label>
                                    <input type="number" class="form-control price required" min="0" value="0" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="status">{{__('admins.active_for_registration')}}</label>
                                    <select class="form-control status required">
                                        <option value="{{config('constants.ACTIVE')}}">{{__('globals.active')}}</option>
                                        <option value="{{config('constants.INACTIVE')}}">{{__('globals.inactive')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="type">{{__('admins.license_type')}}</label>
                                    <select class="form-control typeSlct required">
                                        <option value="{{config('constants.FIXED_PAYMENT')}}">{{__('globals.fixed')}}</option>
                                        <option value="{{config('constants.MONTHLY_PAYMENT')}}">{{__('globals.monthly')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="validityInpt">{{__('globals.license_validity')}} ({{__('globals.days')}})</label>
                                    <input type="number" class="form-control validityInpt" min="0">
                                </div>
                                <div class="form-group">
                                    <label for="end_date">{{__('globals.end_date')}}</label>
                                    <input type="date" class="form-control endDate">
                                </div>
                                <div class="form-group">
                                    <br>
                                    <button type="button" id="updateLicenseBtn" class="button pull-right submitBtn">{{__('admins.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteLicenseModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="text-align: center;">
                <div class="modal-body">
                    <h5>{{__('admins.delete_license_warning')}}</h5>
                    <label>{{__('admins.delete_license_effect')}}</label>
                    <br>
                    <span style="color: red" class="center-block">Type DELETE</span>
                    <form>
                        <input type="text" id="deleteLicenseModalInpt" placeholder="DELETE">
                        <br><br>
                        <button type="button" id="deleteLicenseModalBtn" class="button center-block submitBtn">{{__('admins.delete')}}</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
