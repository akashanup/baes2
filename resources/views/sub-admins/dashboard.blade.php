@extends('layouts.master')

@section('header')
    @include('includes.header')
@endsection

@section('content')
    <div class="title title--page">
        {{__('admins.admin')}} {{__('users.dashboard')}}
    </div>
    <div class="row">
        <div class="col-xs-6 pb50">
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond5.png');">
                <div class="block__inner block__inner--small block__inner--alt">
                    <div class="row">
                        <div class="col-xs-5">
                            <h3 class="block__title">{{__('admins.curriculum_management')}}</h3>
                        </div>

                        <div class="col-xs-7">
                            <a href="{{route('curriculums.create')}}" class="button">
                                {{__('admins.create_curriculum')}}
                            </a>
                            <a href="{{route('curriculums.index')}}" class="button">
                                {{__('admins.edit_curriculum')}}
                            </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="content__block" style="background-image: url('/images/achtergrond4.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('admins.feedback_management')}}</h3>
                            </div>
                            <div class="col-xs-7">
                                <a href="{{route('studentFeedbacks.index',[config('constants.ALL'), config('constants.ALL')])}}" class="button">
                                    {{__('admins.manage_feedback')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection
