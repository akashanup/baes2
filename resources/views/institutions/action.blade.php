@if(!empty($image) && !empty($institutionName))
<img src="{{$image}}" alt="{{$institutionName}}" class="img rowImage center-block" style="max-width: 50px; width: 100%; max-height: 40px;">
@endif
@if(!empty($institutionSlug) && !empty($institutionType))
<div class="row">
    <a href="javascript:void(0)" data-updateRoute="{{route('institutions.update', $institutionSlug)}}" data-type="{{$institutionType}}" class="col-xs-4 center-block editInstitution" data-toggle="tooltip" title="{{__('admins.edit')}}">
        <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
    </a>
    <a href="javascript:void(0)" data-deleteRoute="{{route('institutions.destroy', $institutionSlug)}}" class="col-xs-4 center-block deleteInstitution" data-toggle="tooltip" title="{{__('admins.delete')}}">
        <i class="fa fa-close fa-2x" aria-hidden="true"></i>
    </a>
    @if(!empty($courseId) && !empty($courseSlug))
        <a href="javascript:void(0)" class="col-xs-4 center-block createLicense" data-toggle="tooltip" title="{{__('admins.create_license')}}" data-courseId="{{$courseId}}" data-institutionId="{{$institutionId}}" data-courseSlug="{{$courseSlug}}" data-institutionSlug="{{$institutionSlug}}">
            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
        </a>
    @endif
</div>
@endif
@if(!empty($courseSlug) && !empty($courseName))
    {{$courseName}}
@endif
@if(!empty($users) && !empty($institutionSlug) && !empty($courseSlug))
    <a href="{{route('users.index', [$institutionSlug, $courseSlug, config('constants.ALL')])}}">
        {{$users}}
    </a>
@endif
@if(!empty($institution) && !empty($institutionId))
    <a href="javascript:void(null)" class="teacherLoginBtn" data-route="{{route('admins.teacherLogin')}}" data-institutionId="{{$institutionId}}" data-courseId="{{$courseId}}">
        {{$institution}}
    </a>
@endif
