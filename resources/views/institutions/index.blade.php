@extends('layouts.master')

@section('title')
    {{__('admins.edit_institution')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.edit_institution')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('institutions.create')}}" class="button button--quaternary mb-20">
                {{__('admins.add_institution')}}
            </a>
        </div>
    </div>
    <div class="row mt-30">
        <div class="col-xs-12">
            <table class="table table-bordered table-hover" id="institution-table">
                <thead>
                    <tr>
                        <th width="35%">{{__('admins.institution')}}</th>
                        <th width="15%">{{__('globals.image')}}</th>
                        <th width="25%">{{__('admins.course')}}</th>
                        <th width="10%">{{__('admins.view_users')}}</th>
                        <th width="15">{{__('globals.action')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <input type="hidden" id="homeRoute" value="{{route('home')}}">
    <input type="hidden" id="courseSlug" value="{{$courseSlug}}">
    <input type="hidden" id="institutionSlug" value="{{$institutionSlug}}">
    <hr class="section-divider"/>
    @include('includes.institution-course-dropdown')
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/institution.js').'?v='.$version}}"></script>
@endsection

@section('modals')
    <div id="editInstitutionModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <h1>{{__('admins.edit_institution')}}</h1>
                        <form method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group">
                                <label for="name">{{__('users.name')}}</label>
                                <input type="text" class="form-control" name="name" maxlength="50" required>
                            </div>
                            <div class="form-group">
                                <label for="type">{{__('globals.type')}}</label>
                                <select name="type" class="form-control" required>
                                    <option value="{{config('constants.SCHOOL')}}">{{__('globals.school')}}</option>
                                    <option value="{{config('constants.ORGANISATION')}}">{{__('globals.organisation')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">{{__('globals.logo')}}</label>
                                <input type="file" class="form-control" name="image">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="removeLogo" value="1"> {{__('admins.remove_logo')}}
                            </div>
                            <div class="form-group mt-40 text-right">
                                <button type="submit" id="submitBtn" class="button submitBtn">{{__('globals.save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteInstitutionModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="text-align: center;">
                <div class="modal-body">
                    <h5>{{__('admins.delete_institution_warning')}}</h5>
                    <label>{{__('admins.delete_institution_effect')}}</label>
                    <br>
                    <span style="color: red" class="center-block">Type DELETE</span>
                    <form>
                        <input type="text" id="deleteInstitutionModalInpt" placeholder="DELETE">
                        <br><br>
                        <button type="button" id="deleteInstitutionModalBtn" class="button center-block submitBtn">{{__('admins.delete')}}</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div id="createLicenseModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <h1>{{__('admins.create_license')}}</h1>
                        <form role="form" action="{{ route('licenses.store') }}">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="course_id">{{__('admins.course')}}</label>
                                    <input type="text" class="form-control" id="course" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="institution_id">{{__('admins.institution')}}</label><br>
                                    <input type="text" class="form-control" id="institution" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="code">{{__('users.code')}}</label>
                                    <input type="text" class="form-control" id="code" maxlength="25">
                                </div>
                                <div class="form-group">
                                    <label for="role">{{__('users.code')}} {{__('globals.for')}}</label>
                                    <select class="form-control required" id="role">
                                        <option value="{{config('constants.STUDENT')}}">{{__('globals.for')}} {{__('users.student')}}</option>
                                        <option value="{{config('constants.TEACHER')}}">{{__('globals.for')}} {{__('users.teacher')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="price">{{__('globals.price')}}</label>
                                    <input type="number" class="form-control required" id="price" min="0" value="0" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="status">{{__('admins.active_for_registration')}}</label>
                                    <select class="form-control required" id="status">
                                        <option value="{{config('constants.ACTIVE')}}">{{__('globals.active')}}</option>
                                        <option value="{{config('constants.INACTIVE')}}">{{__('globals.inactive')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="type">{{__('admins.license_type')}}</label>
                                    <select class="form-control typeSlct required" id="type">
                                        <option value="{{config('constants.FIXED_PAYMENT')}}">{{__('admins.fixed_term')}}</option>
                                        <option value="{{config('constants.MONTHLY_PAYMENT')}}">{{__('admins.per_month')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="validityInpt">{{__('globals.license_validity')}} ({{__('globals.days')}})</label>
                                    <input type="number" class="form-control validityInpt" id="validity" min="0">
                                </div>
                                <div class="form-group">
                                    <label for="end_date">{{__('globals.end_date')}}</label>
                                    <input type="date" class="form-control" id="endDate">
                                </div>
                                <div class="form-group">
                                    <br>
                                    <button type="button" id="saveLicenseBtn" class="button pull-right submitBtn">{{__('admins.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
