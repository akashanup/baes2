@extends('layouts.master')

@section('title')
    {{__('admins.add_institution')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('institutions.index',[config('constants.ALL'), config('constants.ALL')])}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.manage_institution')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.add_institution')}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
                <form action="{{route('institutions.store')}}" method="POST" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">{{__('users.name')}}</label>
                        <input type="text" class="form-control" name="name" maxlength="50" required>
                    </div>
                    <div class="form-group">
                        <label for="type">{{__('globals.type')}}</label>
                        <select name="type" class="form-control" required>
                            <option value="{{config('constants.ORGANISATION')}}">{{__('globals.school')}}</option>
                            <option value="{{config('constants.SCHOOL')}}">{{__('globals.organisation')}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="image">{{__('globals.logo')}}</label>
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="form-group mt-40 text-right">
                        <button type="submit" id="submitBtn" class="button submitBtn">{{__('globals.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/institution.js').'?v='.$version}}"></script>
@endsection
