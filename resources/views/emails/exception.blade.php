@component('mail::message')
{{-- Greeting --}}
# Hello!


{{-- Action Panel --}}

@component('mail::panel', ['color' => '#f2dede'])
{{ $exception }}
@endcomponent


{{-- Salutation --}}
{{ config('app.name') }}

@endcomponent
