@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

@component('mail::table')
| {{__('emails.payment_id')}}  | {{__('emails.course')}} | {{__('emails.amount')}} | {{__('emails.date')}}                |
| ------------------------------  |:--------------------------:|:--------------------------:| ---------------------------------------:|
| {{$payment->mollie_payment_unique_id}} | {{$course->name}}       | {{$license->price}}          | {{$payment->created_at->toDateString()}}|
@endcomponent

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
switch ($level) {
    case 'success':
        $color = 'green';
        break;
    case 'error':
        $color = 'red';
        break;
    default:
        $color = 'blue';
}
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset


{{$sincerely}}

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
{{ config('app.name') }}
@endif

@endcomponent
