@component('mail::message')
{{-- Greeting --}}

# Hello!

{{__('admins.variable_set_upload_message')}} {{$learningGoal}}
{{$exceptionMessage}}


{{ config('app.name') }}

@endcomponent
