<div class="content__block content__block--border">
    <div class="title" id="learningGoalQuestionDiv" data-questionId="{{$question['id']}}" data-learningGoalTestId="{{!empty($learningGoalTestId) ? $learningGoalTestId :''}}">
        {!! $question['question'] !!}
    </div>
    @php $adder = 97; $counter = 1; @endphp
    @foreach($question['option'] as $option)
        <div class="form form--answers">
            <div class="form__item form__item--hover">
                <div class="form__radio">
                	@if($currentAnswer == $option)
	                    <input type="radio" id="answer{{$counter}}" name="mcqAnswers" value="{{ $option }}" class="form__radio-input mcqAnswers" checked>
	                    <label for="answer{{$counter++}}" class="form__radio-label form__radio-label"><i class="icon-{{chr($adder++)}} form__icon"></i>
	                        {!! $option !!}
	                    </label>
	                @else
	                	<input type="radio" id="answer{{$counter}}" name="mcqAnswers" value="{{ $option }}" class="form__radio-input mcqAnswers">
	                    <label for="answer{{$counter++}}" class="form__radio-label form__radio-label"><i class="icon-{{chr($adder++)}} form__icon"></i>
	                        {!! $option !!}
	                    </label>
	                @endif
                </div>
            </div>
        </div>
    @endforeach
	@if( config('app.env') != config('constants.PRODUCTION')
		&& config('app.env') != config('constants.DEMO'))
	    <br>
	    <br>
	    <span id="correctAnswerSpan" class="pull-left">{!! $answer !!}</span>
	@endif
</div>
