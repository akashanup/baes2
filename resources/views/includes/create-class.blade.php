<div class="overlay" data-overlay="make-class">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('teachers.create')}} {{__('teachers.class')}}
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <form action="" method="POST" role="form">
                                <div class="form-group">
                                    <label for="">{{__('teachers.year')}}</label>
                                    <select name="new-selection-year" id="yearList" 
                                    class="form-control">
                                        <option value="">{{__('teachers.select')}} {{__('teachers.year')}}</option>
                                        @foreach($years as $year)
                                            <option value= {{ $year }}>{{ $year }}</option>
                                        @endforeach
                                            <option id="other_option" value="others">{{__('teachers.other')}}</option>
                                    </select>
                                </div>
                                <div class="form-group hidden" id="customYearField">
                                    <input class="form-control" id="customYear" placeholder="Enter the new year" name="customYear" type="text" value=""/>
                                </div>
                                <div class="form-group" >
                                    <label for="">{{__('teachers.class')}}</label>
                                    <input class="required form-control" id="clasName" placeholder="Enter the new class name" name="className" type="text" value="">
                                </div>
                                <div class="form">
                                    <button type="button" class="button button--primary" id="add-class">{{__('admins.save')}}</button>
                                </div>
                        </form>
                        <input type="hidden" name="" id="insertClassData" value="{{route('teachers.saveClass', [$licenseId, $course, $institutionId])}}">
                    </div>
                </div>
            </div>
            <div class="overlay__close js__overlay-close" data-overlay="make-class">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>