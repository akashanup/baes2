<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-success" id="successModalStatus">
                        @if($flash = session('status'))
                            <ul style="list-style: none;" id="successStatus">
                                <li>{{$flash}}</li>
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
