<div class="content__block content__block--border">
    <div id="learningGoalQuestionDiv" data-questionId="{{$question['id']}}" data-learningGoalTestId="{{!empty($learningGoalTestId) ? $learningGoalTestId :''}}">
        {!! $question['question'] !!}
    </div>
    <br>
    <div class="form form--answers">
        <div class="table__key"><strong>{{__('admins.answer')}}:</strong></div>
        <div class="form__item">
            <div class="form__radio">
                <input type="text" id="inptAnswer" value="{{$currentAnswer}}" class="form__input numeric">
            </div>
        </div>
    </div>
    @if( config('app.env') != config('constants.PRODUCTION')
        && config('app.env') != config('constants.DEMO'))
        <br>
        <br>
        <span id="correctAnswerSpan" class="pull-left">{!! $answer !!}</span>
    @endif
</div>
