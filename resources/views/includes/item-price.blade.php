@if($type === config('constants.MONTHLY_PAYMENT'))
	&euro; {{$price}} <span class="product__price__period">{{__('globals.per_month')}}</span>
@else
	&euro; {{$price}}
@endif
