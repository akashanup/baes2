<div class="row">
    <div class="col-xs-6" style="max-height: 400px; overflow-y: scroll;">
        <ul class="list-group list-group-sortable-connected">
            <li class="list-group-item disabled">
               {{__('admins.available_learning_goals')}}
            </li>
            @foreach($allLearningGoals as $leaningGoal)
                <li class="list-group-item list-group-item-info" data-id="{{$leaningGoal['id']}}">
                    {{$leaningGoal['name']}}
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-xs-6" style="max-height: 400px; overflow-y: scroll;">
        <ul class="list-group list-group-sortable-connected" id="selectedLearningGoals">
            <li class="list-group-item disabled">
               {{__('admins.selected_learning_goals')}}
            </li>
            @if(!empty($existingLearningGoals))
                @foreach($existingLearningGoals as $leaningGoal)
                    <li class="list-group-item list-group-item-success" data-id="{{$leaningGoal['id']}}">
                        {{$leaningGoal['name']}}
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
