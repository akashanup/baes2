<div class="intro intro--login">
    <div class="title title--marked">{{ __('authentication.develop_yourself') }}:</div>
    <ul class="usps">
        <li>{{ __('authentication.login_page_statement_1') }}</li>
        <li>{{__('authentication.login_page_statement_2')}}</li>
        <li>{{__('authentication.login_page_statement_3')}}</li>
    </ul>
    <div class="title title--marked">{{__('authentication.improve_yourself')}}:</div>
    <ul class="usps">
        <li>{{__('authentication.login_page_statement_4')}}</li>
        <li>{{__('authentication.login_page_statement_5')}}</li>
        <li>{{__('authentication.login_page_statement_6')}}</li>
    </ul>
</div>
