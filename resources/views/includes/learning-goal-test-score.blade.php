@if($currentScore)
    <div class="progress__subtitle">
        <small>{{__('students.last_5_question_score')}}:</small>
    </div>
    <div class="progress__result js__progress-result last_5_scores">
        @for($i = (5 - sizeof($lastFiveScores)); $i > 0; $i--)
            <span class="progress__score">-</span>
        @endfor
        @for($i = (sizeof($lastFiveScores) -1); $i >= 0; $i--)
            <span class="progress__score">{{$lastFiveScores[$i]}}</span>
        @endfor
        <span class="progress__score hidden"></span>
    </div>
	<div class="progress__item">
	    <div class="progress__subtitle">
	        <small>{{__('students.baes_score')}} <i class="icon-info js__overlay-show" data-overlay="score"></i>:</small>
	    </div>
	    <div class="progress__bar">
	        <span class="progress__value js__progress-bar js__progress-bar-final baesScoreBar" style="width: {{($currentScore) * 10}}%;"></span>
	        <i class="js__baes-score baesScoreITag">{{$currentScore}}</i>
	    </div>
	</div>
    <div class="overlay" data-overlay="score">
        <div class="overlay__bg"></div>
        <div class="container">
            <div class="overlay__inner">
                <div class="overlay__scroll">
                    <div class="overlay__title">{{__('students.baes_score_heading')}}</div>
                    <p>
                        {{__('students.baes_score_intro')}}
                    </p>
                    <table class="table--overlay" style="width:400px">
                        <colgroup>
                            <col style="width:50%" />
                            <col style="width:50%" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th>{{__('students.level')}}:</th>
                            <th>BAES score:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Beginner</td>
                            <td>0,0 – 2,4</td>
                        </tr>
                        <tr>
                            <td>Junior</td>
                            <td>2,5 – 4,9</td>
                        </tr>
                        <tr>
                            <td>BAES</td>
                            <td>5,0 – 6,9</td>
                        </tr>
                        <tr>
                            <td>Expert BAES</td>
                            <td>7,0 – 8,4</td>
                        </tr>
                        <tr>
                            <td>{{__('globals.end_baes')}}</td>
                            <td>8,5 – 10</td>
                        </tr>
                        </tbody>
                    </table>
                    <p>
                        {{__('students.baes_description_1')}}
                    </p>

                    <table class="table--overlay" style="width:400px">
                        <colgroup>
                            <col style="width:50%" />
                            <col style="width:50%" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th>{{__('students.number_of_attempts')}}</th>
                            <th>{{__('students.points')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{__('students.attempt')}} 1</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td>{{__('students.attempt')}} 2</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td>{{__('students.attempt')}} 3</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>{{__('students.attempt')}} 4</td>
                            <td>{{__('students.good')}} = 2 | {{__('students.answer_error')}} = 0</td>
                        </tr>
                        </tbody>
                    </table>
                    <p>
                        {{__('students.baes_description_2')}}
                    </p>
                    <p>
                        {{__('students.baes_description_3')}} <br />
                        {{__('students.baes_description_4')}}  <br />
                    </p>
                    <div class="overlay__title">{{__('students.baes_description_5')}}</div>
                    <p>
                        {{__('students.baes_description_6')}}:
                    </p>
                    <ol>
                        <li>{{__('students.baes_description_7')}}; </li>
                        <li>{{__('students.baes_description_8')}}</li>
                    </ol>
                    <p>
                        {{__('students.baes_description_9')}}
                    </p>

                    <button class="button pull-right js__overlay-close" data-overlay="score">{{__('globals.close')}}</button>
                </div>
                <div class="overlay__close js__overlay-close" data-overlay="score">
                    <i class="icon-close"></i>
                </div>
            </div>
        </div>
    </div>
@endif
