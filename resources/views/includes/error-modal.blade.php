<div id="errorModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-danger" style="text-align: justify;" id="errorModalErrors">
                        @if(count($errors))
                            <ul style="list-style: none;" id="serverErrors">
                                @foreach($errors->all() as $error)
                                <li>
                                    {{$error}}
                                </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
