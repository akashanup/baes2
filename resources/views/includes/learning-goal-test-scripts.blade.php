@section('scripts')
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/javascript" src="{{asset('js/students.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/number-formatting.js').'?v='.$version}}"></script>
@endsection
