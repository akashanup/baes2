<div id="ieModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align: justify;">
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning">
                        {{__('globals.ie_warning1')}}<br>
                        {{__('globals.ie_warning2')}}<br>
                        {{__('globals.ie_warning3')}}<br>
                        {{__('globals.ie_warning4')}}<br>
                        {{__('globals.ie_warning5')}}<br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
