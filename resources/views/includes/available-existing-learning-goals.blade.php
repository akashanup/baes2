<div class="row">
    @foreach($allLearningGoals as $leaningGoal)
            <div class="col-xs-4 custom-row">
            <input type="checkbox" class="learningGoalId" 
            name="learningGoalId" value="{{$leaningGoal['id']}}"
             {{$existingLearningGoalIds ? in_array($leaningGoal['id'], $existingLearningGoalIds) ? 'checked' : '' : ''}} > {{$leaningGoal['name']}}
        </div>
    @endforeach
</div>

