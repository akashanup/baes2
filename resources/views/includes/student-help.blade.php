<div class="content__block content__block--border js__block-feedback hidden">
    <div class="question__feedback" data-message="wrong" style="display: block;">
        <p id="wrongAnswer">
            <strong>{{__('students.error')}}</strong>. {{__('students.try_again')}}
        </p>
        <p class="hint">
        </p>
    </div>
    <div class="question__feedback" data-message="correct" style="display: none;">
        <p>
            <strong>{{__('students.good')}}</strong>
        </p>
        <p class="hint">

        </p>
    </div>
</div>
<div class="toolbar clearfix">
    <div class="need-help pull-left">
        {{__('students.help')}}
        <span class="js__overlay-show" data-overlay="theorie">
            <i class="icon-book"></i>
        </span>
        <span class="js__overlay-show" data-overlay="video">
            <i class="icon-play"></i>
        </span>
        <span class="js__overlay-show" data-overlay="voorbeeld1">
            <i class="icon-question"></i>
        </span>
    </div>
    <a href="{{route('students.subjects.learning-goals', [$licenseId, $subjectSlug])}}" class="button button--quaternary pull-right">{{__('students.shut_down')}}</a>
</div>
<div class="overlay" data-overlay="theorie">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">{{__('admins.theory')}}</div>
                {!! $learningGoal['theory'] !!}
                <button class="button pull-right js__overlay-close" data-overlay="theorie">{{__('globals.close')}}</button>
            </div>
            <div class="overlay__close js__overlay-close" data-overlay="theorie">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
<div class="overlay js__overlay-video" data-overlay="video">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__video">
                <div class="overlay__scroll">
                    <div class="overlay__title">
                        {{__('globals.videos')}}
                    </div>
		            <div class="row">
		                @foreach($learningGoal['video'] as $key => $video)
		                    <div class="col-xs-6">
		                        <div class="video js__overlay-show youtubeVideoOverlayDiv" data-overlay="fullscreen-video{{($key+1)}}">
		                            <div class="video__placeholder">
		                                <img class="video__image" src="{{asset('images/placeholder'.($key+1).'.png')}}" alt="">
		                                <div class="video__play"><i class="icon-play2"></i></div>
		                                <div class="video__title video__title--inset">{{ $video['name'] }}</div>
		                            </div>
		                        </div>
		                    </div>
		                @endforeach
		            </div>
                </div>
            </div>
            <div class="overlay__close js__overlay-close" id="youtubeVideoOverlayCloseDiv" data-overlay="video">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
<div class="overlay" data-overlay="voorbeeld1">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                {!! $learningGoal['example'] !!}
                <button class="button pull-right js__overlay-close" data-overlay="voorbeeld1">{{__('globals.close')}}</button>
            </div>
            <div class="overlay__close js__overlay-close" data-overlay="voorbeeld1">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
@foreach($learningGoal['video'] as $key => $video)
    <div class="overlay js__overlay-video" data-overlay="fullscreen-video{{($key+1)}}">
        <div class="overlay__bg"></div>
        <div class="container">
            <div class="overlay__inner">
                <div class="overlay__video">
                    <div class="video__placeholder">
                        <div class="video__play video__play--primary js__video-play"></div>
                        <iframe width="100%" height="550" id="iFrame{{($key+2)}}" src="{{ $video['url'] }}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="overlay__close js__overlay-close " data-overlay="fullscreen-video{{($key+1)}}">
                        <i class="icon-close"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
<input type="hidden" id="checkQuestionAnswerRoute" value="{{route('students.checkQuestionAnswer')}}">
<input type="hidden" id="license" value="{{$licenseId}}">
<input type="hidden" id="subjectId" value="{{$subjectId}}">
<input type="hidden" id="learningGoalId" value="{{$learningGoalId}}">
<input type="hidden" id="learningGoalTestQuestionRoute" value="{{route('students.learning-goals.learningGoalTestQuestion', [$licenseId, $subjectSlug, $learningGoalSlug])}}">
