<div class="give-feedback js__overlay-show" data-overlay="generalFeedback">
    <i class="icon-speak"></i>{{__('users.give_feedback')}}
</div>
@if( config('app.env') != config('constants.PRODUCTION')
	&& config('app.env') != config('constants.DEMO'))
    <br>
    <br>
    <span id="correctAnswerSpan" class="pull-left">{!! $answer !!}</span>
@endif
