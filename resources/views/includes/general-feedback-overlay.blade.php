<div class="overlay" data-overlay="generalFeedback">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">{{__('users.give_feedback')}}</div>
                <div class="form__item">
                    <textarea id="generalFeedbackText" class="form__text" cols="30" rows="5"></textarea>
                </div>

                <button id="generalFeedbackSend" class="button pull-right js__overlay-close" data-overlay="generalFeedback">{{__('globals.send')}}</button>
            </div>
            <div class="overlay__close js__overlay-close" data-overlay="generalFeedback">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
