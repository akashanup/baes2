<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a itemprop="itemListElement" href="{{route('students.subjects.learning-goals', [$licenseId, $subjectSlug])}}" title="" class="breadcrumbs__link">
        <span itemprop="name">
            {{$subjectName}}
        </span>
    </a>
    <meta itemprop="position" content="2" />
</li>
<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a itemprop="itemListElement" href="{{route('students.learning-goals.show', [$licenseId, $subjectSlug, $learningGoalSlug])}}" title="" class="breadcrumbs__link"><span itemprop="name">{{ $learningGoalName }}</span></a>
    <meta itemprop="position" content="3" />
</li>
<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a itemprop="itemListElement" href="javascript:void(null)" title="" class="breadcrumbs__link"><span itemprop="name">{{__('users.question')}}</span></a>
    <meta itemprop="position" content="4" />
</li>
