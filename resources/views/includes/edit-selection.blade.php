<div class="overlay" data-overlay="bewerk_leerling">
                <div class="overlay__bg"></div>
                <div class="container">
                    <div class="overlay__inner">
                        <div class="overlay__scroll">
                            <div class="overlay__title">
                                {{__('admins.edit')}} {{__('teachers.selection')}}
                            </div>
                            <form class="col-xs-12" role="form">
                                <div class="row">
                                    <div class="col-xs-12 form-group" id="edit-name-div">
                                        <div class="col-xs-6">
                                            <label for="">{{__('teachers.name')}}</label>
                                            <input type="text" class="form-control" id="edit-name" value="">
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="checkbox" id="update-check" value="edit-name" checked> <label>{{__('teachers.do_not_update')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-6">
                                            <label for="">{{__('teachers.year')}}</label>
                                            <select class="form-control" id="edit-year">
                                                @foreach($years as $year)
                                                    <option value="{{ $year }}">{{ $year }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="checkbox" id="update-check" value="edit-year" checked> <label>{{__('teachers.do_not_update')}}</label>
                                        </div>
                                        </div>
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-6">
                                            <label for="">{{__('teachers.class')}}</label>
                                            <select class="form-control" id="edit-class"></select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-6">
                                            <label for="">{{__('teachers.status')}}</label>
                                            <select class="form-control" id="edit-status">
                                                <option value = "{{config('constants.ACTIVE')}}">{{config('constants.ACTIVE')}}</option>
                                                <option value = "{{config('constants.INACTIVE')}}" >{{config('constants.INACTIVE')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="checkbox" id="update-check" value="edit-status" checked> <label>{{__('teachers.do_not_update')}}</label>
                                        </div>
                                    </div>
                                </div>

                            </form></div>

                        <div class="overlay__footer">
                            <div class="col-xs-5">
                                <input type="hidden" name="gradesByYearRoute" id="gradesByYearRoute" value="{{$gradesByYearRoute}}"/>

                                <input type="hidden" id="updateStudentsRoute" name="updateStudentsRoute" value="{{$updateStudentsRoute}}"/>
                                
                                <button type="submit" class="js__overlay-close button button--primary" id="edit-student-button" data-overlay="bewerk_leerling">Opslaan</button>
                            </div>
                        </div><!-- end overlay-footer-->
                        <div class="overlay__close js__overlay-close" data-overlay="bewerk_leerling">
                            <i class="icon-close"></i>
                        </div>
                    </div>
                </div>
            </div>