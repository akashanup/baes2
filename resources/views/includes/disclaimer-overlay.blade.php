<div class="overlay" data-overlay="disclaimer">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('master.disclaimer')}}
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        {{__('disclaimer.line_1')}}<br><br>

                        <b>{{__('disclaimer.point_1')}}</b><br>
                        {{__('disclaimer.point_1_1_c')}}<br><br>
                        {{__('disclaimer.point_1_2_c')}}<br><br>
                        {{__('disclaimer.point_1_3_c')}}<br><br>
                        <b>{{__('disclaimer.point_2')}}</b><br>
                        {{__('disclaimer.point_2_c')}}<br><br>
                        <b>{{__('disclaimer.point_3')}}</b><br>
                        {{__('disclaimer.point_3_c')}}<br><br>
                        <b>{{__('disclaimer.point_4')}}</b><br>
                        {{__('disclaimer.point_4_c')}}<br><br>
                    </div>
                </div>
            </div><!-- end overlay-scroll-->
            <div class="overlay__close js__overlay-close" data-overlay="disclaimer">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
