<div class="row">
    <div class="col-xs-12">
        <div class="title title--page title--underline-dark">
            <div class="row">
                <div class="col-md-4 mt-10">
                    {{__('globals.licenses')}}
                </div>
                <div class="col-md-8">
                    <div class="licence-search">
                        <form>
                            <div class="licence-search-info">
                                {{__('users.code_text_1')}}
                                <br>
                                {{__('users.code_text_2')}}
                            </div>
                            <div class="licence-search-input">
                                <input type="text" id="codeInpt" class="form_input" placeholder="Bijv. licentiecode" value="{{!empty($code) ? $code : ''}}" >
                          </div>
                          <button class="button button--quaternary licence-search-button submitBtn" type="button"  id="codeInptBtn" data-route="{{route('users.availableLicenses')}}">{{__('users.code_text_3')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
