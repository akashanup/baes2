<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<a href="{{ route('logout') }}" class="button button--quaternary"
   onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
    <i class="icon-logout"></i>{{__('users.logout')}}
</a>
