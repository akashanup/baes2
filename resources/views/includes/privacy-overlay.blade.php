<div class="overlay" data-overlay="privacy">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('master.privacy')}}
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        {{__('privacy.line_1')}}<br><br>

                        1) {{__('privacy.point_1')}}<br>
                          {{__('privacy.point_1_c')}}<br><br>
                        2) {{__('privacy.point_2')}}<br>
                          {{__('privacy.point_2_c')}}<br><br>
                        3) {{__('privacy.point_3')}}<br>
                          {{__('privacy.point_3_c')}}<br><br>
                        4) {{__('privacy.point_4')}}<br>
                          {{__('privacy.point_4_c')}}<br><br>
                        5) {{__('privacy.point_5')}}<br>
                          {{__('privacy.point_5_c')}}<br><br>
                        6) {{__('privacy.point_6')}}<br>
                         {{__('privacy.point_6_c')}}<br><br>
                    </div>
                </div>
            </div><!-- end overlay-scroll-->
            <div class="overlay__close js__overlay-close" data-overlay="privacy">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
