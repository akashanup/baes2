<div class="col-xs-4">
    <div class="progress">
        <div class="progress__inner">
            <div class="progress__title title js__progress-title baesLevel">
				{{$studentLevel ? $studentLevel : ''}}
            </div>
            <div class="progress__item">
			    <div class="progress__subtitle">
			        <small>{{__('students.progress_block')}}:</small>
			        <span class="pull-right" style="font-size: small;">{{$subjectName}}</span>
			    </div>
			    <div class="progress__bar">
			        <span class="progress__value js__progress-bar" style="width: {{$progressBlock}}%;"></span><i>{{$progressBlock}}%</i>
			    </div>
			</div>

			<div class="progress__item">
			    <div class="progress__subtitle">
			        <small>{{__('students.score_block')}}:</small>
			        <span class="pull-right" style="font-size: small;">{{$subjectName}}</span>
			    </div>
			    <div class="progress__bar">
			        <span class="progress__value js__progress-bar" style="width: {{($scoreBlock)*10}}%;"></span><i>{{$scoreBlock}}</i>
			    </div>
			</div>

			<div id="learningGoalTestScoreDiv">
				@include('includes.learning-goal-test-score')
			</div>
        </div>
    </div>
</div>
