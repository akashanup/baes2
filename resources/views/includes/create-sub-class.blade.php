<div class="overlay" data-overlay="make-subclass">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('teachers.create')}} {{__('teachers.subclass')}} 
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <form action="{{route('teachers.saveClass', [$licenseId, $course, $institutionId])}}" method="POST" role="form">
                                <div class="form-group">
                                    <label for="">{{__('teachers.class')}}</label>
                                    <select name="new-selection-year" id="classList" 
                                    class="form-control">
                                        @foreach($grades as $grade)
                                            <option value= {{ $grade->id }}>{{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="">{{__('teachers.subclass')}}</label>
                                    <input class="required form-control" id="subClassName" placeholder="Enter the new sub class name" name="subClassName" type="text" value="">
                                </div>
                                <div class="form">
                                    <button type="button" class="button button--primary" id="addSubClass">{{__('admins.save')}}</button>
                                </div>
                        </form>
                        <input type="hidden" name="" id="insertSubClassData" value="{{route('teachers.saveSubClass', [$licenseId, $course, $institutionId])}}">
                    </div>
                </div>
            </div>
            <div class="overlay__close js__overlay-close" data-overlay="make-subclass">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>