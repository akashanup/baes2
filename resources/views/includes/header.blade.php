<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                @include('includes.logo')
            </div>
            <div class="col-xs-2">
                <div class="dashboard-button">
                    <a href="{{$dashboard}}" class="dashboard-button__link">
                        <i class="icon-home"></i>{{$userRole === config('constants.USER') ? __('globals.licenses') : __('users.dashboard')}}
                    </a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="profile js__profile">
                    <div class="profile__inner">
                        <div class="profile__head">
                            <div class="profile__img">
                                <img src="{{$userImage}}" alt="{{$userName}}" class="img-circle" style="max-width: 75px; width: 100%; max-height: 60px;">
                            </div>
                            <div class="profile__name profile_slider_name">
                                {{$userName}}
                            </div>
                            <div class="profile__toggle js__profile-toggle">
                                <i class="icon-plus"></i>
                                <i class="icon-min hidden"></i>
                            </div>
                        </div>
                        <div class="profile__slide">
                            <div class="title">
                                {{__('users.profile')}}
                            </div>
                            <a href="{{route('users.edit', $userSlug)}}" class="profile__edit">
                                <i class="icon-edit"></i>{{__('globals.edit_profile')}}
                            </a>
                            <div class="profile__info">
                                <div class="profile__key">
                                    {{__('users.name')}}:
                                </div>
                                <div class="profile__value">
                                    {{$userName}}
                                </div>
                            </div>
                            <div class="profile__buttons">
                                @if($userRole === config('constants.USER'))
                                    <div class="button button--feedback js__overlay-show" data-overlay="generalFeedback"><i class="icon-speak2"></i>{{__('users.give_feedback')}}</div>
                                @endif
                                @include('includes.logout')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
