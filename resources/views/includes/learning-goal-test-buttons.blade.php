<div class="col-xs-4 pull-right" id="answerBtnDiv">
    <button id="answerBtn" class="button pull-right js__save-answer answerBtn">{{__('students.answer_button')}}</button>
    <button id="retryBtn" class="button pull-right js__save-answer hidden answerBtn">{{__('students.retry_button')}}</button>
    <button id="nextQuestionBtn" class="button pull-right js__save-answer hidden">{{__('students.next_question')}}</button>
    <a id="learningGoalTestBtn" href="{{route('students.learning-goals.test', [$licenseId, $subjectSlug, $learningGoalSlug])}}" class="button pull-right js__save-answer hidden">{{__('students.next_question')}}</a>
</div>
