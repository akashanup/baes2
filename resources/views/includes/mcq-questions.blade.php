@include('includes.mcq-div')
<input type="hidden" id="questionLevel" value="{{!empty($questionLevel)? $questionLevel : ''}}">
<input type="hidden" id="eligibility" value="{{!empty($eligibility)? $eligibility : ''}}">
@if((!empty($eligibility) && $eligibility) || (!empty($improve) && $improve))
    @include('includes.learning-goal-finished-modal')
@endif
