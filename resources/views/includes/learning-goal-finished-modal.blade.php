<div id="learningGoalFinishedModal" class="modal fade" role="dialog">
    @if(!empty($nextLearningGoalRoute) && $nextLearningGoalRoute)
    <div class="modal-dialog modal-lg">
    @else
    <div class="modal-dialog">
    @endif
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h5>{{$message}}</h5>
                <div class="row">
                    @if(!empty($nextLearningGoalRoute) && $nextLearningGoalRoute)
                        <div class="col-xs-4">
                            <a type="button" class="button" href="{{$learningGoalOverviewRoute}}" style="width: 100%">
                                {{__('students.subjects_overview')}}
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <button type="button" class="button" id="improveScoreBtn" data-dismiss="modal" style="width: 100%">
                                {{__('students.improve_score')}}
                            </button>
                        </div>
                        <div class="col-xs-4">
                            <a type="button" class="button" href="{{$nextLearningGoalRoute}}" style="width: 100%">
                                {{__('students.next_goal')}}
                            </a>
                        </div>
                    @else
                        <div class="col-xs-4 col-xs-offset-1">
                            <a type="button" class="button" href="{{$learningGoalOverviewRoute}}" style="width: 100%">
                                {{__('students.subjects_overview')}}
                            </a>
                        </div>
                        <div class="col-xs-4 col-xs-offset-2">
                            <button type="button" class="button" id="improveScoreBtn" data-dismiss="modal" style="width: 100%">
                                {{__('students.improve_score')}}
                            </button>
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('globals.close')}}</button>
            </div>
        </div>
    </div>
</div>
