<div class="overlay" data-overlay="privacy">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    Privacy Statement BAES Education
                </div>
                <div class="row">
                    <div class="col-lg-12">
                    <p>BAES Education (hierna genoemd BAES) verwerkt persoonsgegevens. Dit privacy statement informeert je over de doeleinden waarvoor persoonsgegevens verwerkt worden en hoe je jouw privacy rechten uitoefent. In de cookieverklaring lees je van welke cookies gebruik wordt gemaakt door BAES. We gebruiken cookies alleen voor functionele doeleinden, niet voor reclame doeleinden.</p>

                    <p>Dit privacy statement is van toepassing op alle activiteiten (inclusief de activiteiten via de website) van BAES. Dit privacy statement geeft per onderwerp de meest relevante informatie. BAES gaat op zorgvuldige wijze met persoonsgegevens om en handelt binnen de grenzen van de Algemene Verordening Gegevensbescherming (AVG).</p>

                    <p><strong>Verantwoordelijke en verantwoordelijkheid</strong></p>

                    <p>BAES is verantwoordelijke in de zin van de AVG. BAES vindt het van essentieel belang dat de persoonlijke gegevens van al haar klanten, dus de studenten, docenten en haar eigen medewerkers met de grootst mogelijke zorgvuldigheid worden behandeld. Daarom lichten wij dit hieronder toe.</p>

                    <p><strong>Welke persoonsgegevens verzamelt BAES?</strong></p>

                    <p>De persoonlijke gegevens die van jou worden verzameld, worden door BAES gebruikt voor de ordelijke werking van haar leermodules en voor het naar behoren uitvoeren van de wettelijke taken en plichten. De belangrijkste processen waarvoor BAES persoonsgegevens verwerkt zijn:</p>

                    <ul>
                        <li><strong>Aanmelden van studenten in de leermodules</strong>: De gegevens van de studenten die BAES verzamelt omvatten de voornaam, de achternaam, het email-adres en het telefoonnummer. Indien je een lesproduct afneemt wordt je gekoppeld aan een leermodule van BAES. In sommige gevallen zijn de lesmodules ontwikkeld voor een specifieke onderwijsinstelling, vak en klas. In deze gevallen worden deze gegevens gevraagd bij de aanmelding en opgeslagen door BAES. Naast deze gegevens die bij de aanmelding worden verzameld, worden contactgegevens, die je via het contactformulier of via de feedback knop in de leermodules naar ons stuurt, bewaard.</li>
                        <li><strong>Aanmelden van docenten in de leermodules: </strong>De gegevens van de docent die BAES verzamelt omvatten de voornaam, de achternaam, het email-adres en het telefoonnummer. Tevens wordt de docent gekoppeld aan een leermodule van BAES die is gemaakt voor een specifieke onderwijsinstelling en wordt specifiek gemaakt welk vakken en welke studenten de docent volgt.</li>
                        <li><strong>Personeelszaken: </strong>De gegeven van het personeel omvatten de NAW-gegevens, bankrekeningnummer (IBAN), het telefoonnummer, de geboortedatum, het geslachten het e-mailadres.</li>
                    </ul>

                    <p>BAES verzamelt (persoons)gegevens direct bij de gebruiker van de BAES lesmodules. Er worden geen persoonsgegevens ontvangen van en/of ingekocht bij derden.</p>

                    <p><strong>Wat doet BAES met deze verzamelde gegevens?</strong></p>

                    <p>De persoonsgegevens die we van je verzamelen worden gebruikt voor diverse doeleinden. Iedere onderneming heeft een wettelijke grondslag en een doel nodig om de verwerking van je persoonsgegevens te rechtvaardigen. In de Algemene Verordening Gegevensbescherming (AVG) wordt een aantal grondslagen genoemd waarvan er drie voor BAES van belang zijn :</p>

                    <ul>
                        <li>Een gerechtvaardigd belang;</li>
                        <li>Een wettelijke verplichting;</li>
                        <li>Toestemming van jou.</li>
                    </ul>

                    <p>Een gerechtvaardigd belang zien wij in de volgende punten:</p>

                    <ul>
                        <li>Voor onze leermodules hebben we diverse gegevens nodig om je accounts in onze module aan te maken en om de afgenomen dienstverlening te kunnen factureren;</li>
                        <li>Daarnaast hebben we je gegevens ook nodig om onze dienstverlening inclusief site en eventuele mobiele applicaties te verbeteren. De verbetering zijn uitsluitend operationeel en op het gebied van veiligheid;</li>
                        <li>We hebben je gegevens nodig om contact met je op te kunnen nemen inzake onze dienstverlening of inzake het debiteuren beheer en om eventuele problemen met je account op te lossen;</li>
                        <li>Op het moment dat jij contact opneemt hebben we je gegevens nodig om te kunnen verifi&euml;ren wie we aan de telefoon hebben, zodat we zorgvuldig en veilig met je opleidingsgegevens om kunnen gaan;</li>
                        <li>Om onze algemene voorwaarden, deze privacyverklaring met je te delen en aanpassingen hierin te kunnen melden.</li>
                    </ul>

                    <p>Voor BAES geldt natuurlijk ook de wettelijke verplichting inzake de administratieplicht waardoor BAES facturatiegegevens minimaal zeven jaar moeten bewaren.</p>

                    <p><strong>Geven en intrekken van toestemming</strong></p>

                    <p>BAES biedt activiteiten die alleen uitgevoerd kan worden door gebruik te maken van uw persoonlijke gegevens, namelijk de leermodule. Wanneer je BAES toestemming geeft voor het gebruik van persoonlijke gegevens, dan kunt je deze toestemming op een later moment altijd intrekken door een verwijderingsverzoek te richten aan BAES.</p>

                    <p><strong>Hoe zorgt BAES voor vertrouwelijke omgang met persoonsgegevens?</strong></p>

                    <p>BAES gaat vertrouwelijk om met persoonsgegevens. BAES neemt passende technische en organisatorische maatregelen, zodat persoonsgegevens worden beschermd. BAES deelt geen persoonsgegevens met derden.</p>

                    <p><strong>Doorgifte van uw gegevens buiten de EU.</strong></p>

                    <p>BAES verstrekt in geen gevallen persoonsgegevens aan landen buiten de EU.</p>

                    <p><strong>Hoe lang worden gegevens bewaard?</strong></p>

                    <p>BAES bewaart uw persoonsgegevens in overeenstemming met de AVG. De gegevens worden niet langer bewaard dan strikt noodzakelijk is om de doelen te bereiken waarvoor de gegevens verzameld zijn. Voor enkele gegevens geldt de administratieplicht.</p>

                    <p><strong>Hoe kunt je jouw gegevens inzien, corrigeren of verwijderen?</strong></p>

                    <p>U kunt een inzage- of correctieverzoek richten aan BAES. Geef daarbij duidelijk aan dat het gaat om een inzage- of correctieverzoek op grond van de AVG. Je kunt ook verzoeken uw gegevens te laten verwijderen, echter dit is slechts mogelijk voor zover BAES nog aan zijn wettelijke verplichtingen kan voldoen, zoals de wettelijke bewaartermijnen. Je bent tevens gerechtigd een klacht in te dienen over het gebruik van uw gegevens bij de Autoriteit persoonsgegevens.</p>

                    <p><strong>Technische beveiliging</strong></p>

                    <p>Om jouw persoonlijke gegevens optimaal te beschermen tegen onbevoegde toegang of onbevoegd gebruik, past BAES beveiligingstechnologie&euml;n toe. Van (pogingen tot) misbruik doen wij aangifte. BAES neemt daarnaast organisatorische maatregelen om persoonsgegevens te beveiligen tegen toegang door onbevoegden.</p>

                    <p><strong>Datalekken</strong></p>

                    <p>Van datalekken maken wij melding bij de Autoriteit persoonsgegevens. BAES neemt daarnaast organisatorische maatregelen om persoonsgegevens te beveiligen tegen toegang door onbevoegden.</p>

                    <p><strong>Cookies en clickgedrag</strong></p>

                    <p>Op onze website en in onze module worden algemene bezoekgegevens bijgehouden, zoals bijvoorbeeld de meest opgevraagde pagina&#39;s. Het doel van het verzamelen van deze algemene bezoekgegevens is om de inrichting van de website en de leermodule voor jou te optimaliseren. BAES gebruikt enkele tools waaronder bijvoorbeeld Hotjar om de website en leermodule optimaal te laten functioneren en het gebruiksgemak te verbeteren. Wij halen tevens actief feedback op bij gebruikers via de feedback knoppen.</p>

                    <p>De website en de leermodule van BAES maken gebruik van cookies. Een cookie is een hoeveelheid data die een server naar de browser stuurt met de bedoeling dat deze informatie opgeslagen en/of uitgevoerd wordt. Bij een volgend bezoek aan onze website worden de opgeslagen gegevens weer teruggestuurd naar de server. Op deze manier herkent de server de browser en kunnen bij een volgend bezoek bepaalde gegevens worden opgehaald. Wanneer je onze leermodules bezoekt worden er altijd cookies geplaatst voor functionele doeleinden. Dit zijn cookies die noodzakelijk zijn voor het gebruik van de website en cookies waarmee wij informatie kunnen verkrijgen over de kwaliteit en/of effectiviteit van de website. Op basis van deze informatie kunnen we bepaalde verbeteringen doorvoeren en kan de website in de toekomst worden aangepast aan de behoefte van de bezoeker. Deze cookies, die op uw computer worden opgeslagen en waarmee je bij een volgend bezoek aan onze website kunt worden herkend, bevatten geen persoonsgegevens. Ze bevatten enkel een (uniek) nummer. Ze maken daarom geen inbreuk op uw privacy.</p>

                    <p>Indien je geen functionele en analytische cookies wil ontvangen dient je deze te blokkeren in de browser.</p>

                    <p>BAES gebruikt geen tracking cookies.</p>

                    <p><strong>Privacybeleid van derden</strong></p>

                    <p>Op de website van BAES zijn links opgenomen naar andere websites die niet tot BAES behoren, zoals Youtube en Mollie Payments. Wij kunnen geen verantwoordelijkheid dragen met betrekking tot de wijze waarop deze partijen omgaan met persoonsgegevens. Wij adviseren daarom, je altijd op de hoogte te stellen van het privacy beleid van deze partijen.</p>

                    <p><strong>Vragen</strong></p>

                    <p>Hebt je naar aanleiding van deze informatie nog specifieke vragen of opmerkingen over onze privacy statement? Neem dan contact met ons op. Je kan hiervoor het contactformulier op de website gebruiken of een email sturen naar&nbsp;<a href="mailto:privacy@baes.education">privacy@baes.education</a>.&nbsp;De functionaris gegevensbescherming van BAES is bereikbaar via dit emailadres. &nbsp;</p>
                    </div>
                </div>
            </div><!-- end overlay-scroll-->

            <div class="overlay__close js__overlay-close" data-overlay="privacy">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>
