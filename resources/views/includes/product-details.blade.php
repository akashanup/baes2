<img src="{{$license['image']}}" alt="{{$license['courseName']}}" class="product__logo--school">
<h2 class="product__title">{{$license['courseName']}}</h2>
<div class="product__description product-detail">
    <p>
        @foreach($license['subjects'] as $subject)
            {{$subject}}</br>
        @endforeach
    </p>
</div>
<div class="product__price course-pricing product-detail">
    @if($license['licenseRole'] === config('constants.TEACHER'))
        <div class="product__price__badge">
          {{__('users.teacher')}}
        </div>
    @endif
    @if($license['price'])
        <p>{!!$license['price']!!}</p>
    @else
        <p>{{__('users.free')}}</p>
    @endif
</div>
