<div class="overlay view__learningGoal" data-overlay="learning_goal_view">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('admins.template_view')}}
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="directDiv">
                            <div class="content__block content__block--border">
                                <div class="title title--page">
                                    <span>
                                        Learning Objective <span id="learningGoalId"></span> : <span id="lg_name"></span>
                                    </span>
                                </div>

                                <div class="intro intro--theory">
                                    <div class="title">{{__('admins.theory')}}</div>
                                    <div class="intro__overflow toggle-content" id="lg_theory">
                                    </div>
                                    <div class="toggle-intro js__toggle-content pull-right" attr-more="{{__('admins.view_more')}}" attr-less="{{__('admins.view_less')}}">{{__('admins.view_more')}}</div>
                                </div>

                                <div class="examples">
                                    <div class="examples__overflow toggle-content" id="lg_example">
                                    </div>
                                    <div class="toggle-examples js__toggle-content pull-right" attr-more="{{__('admins.view_more')}}" attr-less="{{__('admins.view_less')}}">{{__('admins.view_more')}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6" id="video1-box">
                                        <div class="video js__overlay-show" data-overlay="fullscreen-video1">
                                            <div class="video__placeholder">
                                                <img class="video__image" src="{{asset('images/placeholder1.png')}}" alt="">
                                                <div class="video__play"><i class="icon-play2"></i></div>
                                                <div class="video__title video__title--inset" id="lg_videotext1"></div>
                                            </div>
                                        </div>
                                        <div class="invalid-video">{{__('admins.invalid_video')}}</div>
                                    </div>
                                    <div class="col-xs-6" id="video2-box">
                                        <div class="video js__overlay-show" data-overlay="fullscreen-video2">
                                            <div class="video__placeholder">
                                                <img class="video__image" src="{{asset('images/placeholder2.png')}}" alt="">
                                                <div class="video__play"><i class="icon-play2"></i></div>
                                                <div class="video__title video__title--inset" id="lg_videotext2"></div>
                                            </div>
                                        </div>
                                        <div class="invalid-video">{{__('admins.invalid_video')}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end overlay-scroll-->

            <div class="overlay js__overlay-video" data-overlay="fullscreen-video1">
            <div class="overlay__bg"></div>
            <div class="container">
                <div class="overlay__inner">
                    <div class="overlay__video">
                        <div class="video__placeholder">
                            <div class="video__play video__play--primary js__video-play"></div>
                            <iframe width="100%" height="550" id="lg_video1" src="" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="overlay__close js__overlay-close "  data-overlay="fullscreen-video1">
                            <i class="icon-close"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="overlay js__overlay-video" data-overlay="fullscreen-video2">
            <div class="overlay__bg"></div>
            <div class="container">
                <div class="overlay__inner">
                    <div class="overlay__video">
                        <div class="video__placeholder">
                            <div class="video__play video__play--primary js__video-play"></div>
                            <iframe width="100%" height="550" id="lg_video2" src="" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="overlay__close js__overlay-close" data-overlay="fullscreen-video2">
                        <i class="icon-close"></i>
                    </div>
                </div>
            </div>
        </div>

            <div class="overlay__close js__overlay-close template_view_closed" data-overlay="learning_goal_view">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>