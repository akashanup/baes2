<tr>
    @foreach($learningGoals as $learningGoal)
        <th>
            <strong class="showOnHover" data-targetid="hovertext" data-title="{{$learningGoal->name}}">
                L{{$learningGoal->id}}
            </strong>
        </th>
    @endforeach
</tr>