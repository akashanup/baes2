<div class="overlay" data-overlay="edit-selection">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('admins.edit')}} {{__('teachers.selection')}}
                </div>
                <form class="col-xs-5" action="" method="POST" role="form">
                    <div class="row">
                        <div class="form-group">
                            <label for="">{{__('teachers.year')}}</label>
                            <select name="edit-selection-year" id="editClassYear" class="form-control" required="required">
                                @foreach($years as $year)
                                <option value= {{ $year }}>{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('teachers.class')}}</label>
                            <input class="required form-control" id="editClassName" placeholder="Enter the new class name" name="editClassName" type="text"
                            />
                        </div>
                        <div class="form-group">
                            <button type="button" class="button button--primary" id="editClassButton" class-id = "">{{__('admins.save')}}</button>
                        </div>
                    </div>
                </form>
                <input type="hidden" name="updateClassRoute" id="updateClassRoute"
                value="{{$updateClassRoute}}"/>
            </div>
            </form>
            <div class="overlay__close js__overlay-close" data-overlay="edit-selection">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>