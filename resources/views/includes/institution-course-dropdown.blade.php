<div id="filterLblDiv" style="display: none;">
    <label class="filterLabel">
        {{__('admins.institution')}}:
        <select class="singleSelect form-control" id="institutionDropDown">
            <option value="{{config('constants.ALL')}}">{{__('admins.all')}}</option>
            @foreach($institutions as $institution)
                <option value="{{$institution['slug']}}">{{$institution['name']}}</option>
            @endforeach
        </select>
    </label>
    <label class="filterLabel">
        {{__('admins.course')}}:
        <select class="singleSelect form-control" id="courseDropDown">
            <option value="{{config('constants.ALL')}}">{{__('admins.all')}}</option>
            @foreach($courses as $course)
                <option value="{{$course['slug']}}">{{$course['name']}}</option>
            @endforeach
        </select>
    </label>
</div>
