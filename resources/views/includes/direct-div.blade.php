<div class="content__block content__block--border">
    <div id="learningGoalQuestionDiv" data-questionId="{{$question['id']}}" data-learningGoalTestId="{{!empty($learningGoalTestId) ? $learningGoalTestId :''}}" class="direct_question">
        {!! $question['question'] !!}
    </div>
    <br>
    <div class="form form--answers">
        <div class="table__key"><strong>{{__('admins.answer')}}:</strong></div>
        <div class="form__item">
            <div class="form__radio">
                <input type="text" id="inptAnswer" class="form__input numeric">
            </div>
        </div>
    </div>
    @include('includes.learning-goal-test-feedback-and-answer')
</div>
