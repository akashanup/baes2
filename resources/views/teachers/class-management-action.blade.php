@if(!empty($classyear))
<span class="classYear">{{$classyear}}</span>
@endif

@if(!empty($className))
<span class="classGrade" slug="{{$classSlug}}">
	<a href="{{$studentManagementRoute}}">{{$className}}</a>
</span>
@endif

@if(!empty($gradeName))
	@if($gradeName !== config('constants.DEFAULT_CLASS'))
		@if($studentCount === 0)
			    <button type="button" id="deleteClass" class="btn btn-default btn-sm" >
			        <span class="glyphicon glyphicon-trash"></span>{{__('admins.delete')}}
			    </button>
		@else
		    <button type="button" id="editClass" class="btn btn-default btn-sm js__overlay-show" data-overlay="edit-selection" data-id = "{{$gradeId}}">
		        <span class="glyphicon glyphicon-pencil"></span>{{__('admins.edit')}}
		    </button>
		@endif
	@endif
@endif  