@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('breadcumb')
	@include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(null)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$practiceTestDetail['name']}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-3 pr-0">
            <table class="table table-baes table-hover">
                <thead>
                    <tr>
                        <th>
                            <a href="#">
                                {{__('users.teacher')}}
                            </a>
                        </th>
                        <th class="bg-lightgreen text-bold">
                            <a href="#">
                                {{__('users.score')}}
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <span class="text-bold">{{__('teachers.average_class')}}</span>
                        </td>
                        <td class="bg-lightgreen text-center">
                            <span class="text-bold">{{$practiceTestDetail['avg_score']}}%</span>
                        </td>
                    </tr>
                    @foreach($students as $student)
                        <tr>
                            <td class="break_name">
                                <div class="name-break">
                                    {{$student['first_name'] . ' ' . $student['last_name']}}
                                </div>
                            </td>
                            <td class="bg-lightgreen text-center">
                                {{$student['score']}}%
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-9 pl-0 ">
            <div id="scroll-horizontal" class="scroll-horizontal">
                <table class="table table-baes table-baes--scroll table-hover mb-0">
                    <thead>
                        <tr>
                            @for($i = 0; $i < $questionsCount; $i++)
                            	<th style="text-align: center;">
                                    <strong>
                                        {{__('teachers.q')}} {{($i+1)}}
                                    </strong>
                                </th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody style="overflow-y: scroll; text-align: center;">
                        <tr>
                            @for($i = 0; $i < sizeof($averageScores); $i++)
                                <td class="text-center">
                                    {{$averageScores[$i]}}%
                                </td>
                            @endfor
                        </tr>
                        @foreach($studentsPracticeTestQuestions as $key => $studentPracticeTestQuestions)
	                        <tr>
	                        	@foreach($studentPracticeTestQuestions as $studentPracticeTestQuestion)
                                    @if($students[$key]->status === config('constants.FINISHED'))
                                        @if($studentPracticeTestQuestion->pivot->is_correct === config('constants.YES'))
                                            <td>
                                                <a href="javascript:void(null)" class="result result--green result--round js__overlay-show overlayLink overlayBtn" data-overlay="{{$studentPracticeTestQuestion->pivot->id}}"
                                                    &nbsp;
                                                </a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="javascript:void(null)" class="result result--red result--round js__overlay-show overlayLink overlayBtn" data-overlay="{{$studentPracticeTestQuestion->pivot->id}}"
                                                    &nbsp;
                                                </a>
                                            </td>
                                        @endif
                                    @else
                                        <td>
                                            <span class="result result--round result--gray">
                                                &nbsp;
                                            </span>
                                        </td>
                                    @endif
		                        @endforeach
							</tr>
	                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if($practiceTestDetail['feedback_type'] != config('constants.END_OF_TEST'))
	    <div class="row">
		    <div class="col-xs-12">
		        <div class="text-right mt-20">
		            <form action="{{route('teachers.practiceTests.feedbackAvailable', [$license->id, $practiceTestDetail['slug']])}}" method="post">
		            	{{ csrf_field() }}
		            	<button type="submit" class="button button--quaternary">
			                {{__('teachers.avail_feedback')}}
			            </button>
		            </form>
		        </div>
		    </div>
	    </div>
    @endif
    <div class="row">
    	@foreach($studentsPracticeTestQuestions as $key => $studentPracticeTestQuestions)
    		@foreach($studentPracticeTestQuestions as $studentPracticeTestQuestion)
    			<div class="overlay" data-overlay="{{$studentPracticeTestQuestion->pivot->id}}">
		            <div class="overlay__bg"></div>
		            <div class="container">
		                <div class="overlay__inner">
		                    <div class="overlay__scroll">
		                        <div class="overlay__title">
                                    {{__('teachers.example_question')}}
                                    <span class="pull-right text-small">{{__('admins.question_level')}}: {{$studentPracticeTestQuestion->level}}</span>
                                </div>
                                <div class="block__table">
                                    {!! $studentPracticeTestQuestion->question !!}
                                </div>
                                <div class="block__table">
                                    {!! $studentPracticeTestQuestion->hint !!}
                                </div>
                                <div class="block__table">
                                    <h4>{{__('students.correct_answer')}}</h4>
                                    {!! $studentPracticeTestQuestion->answer !!}
                                </div>
                                <div class="block__table">
                                    <h4>{{__('teachers.student_answer')}}</h4>
                                    {!! $studentPracticeTestQuestion->pivot->answer !!}
                                </div>
                                <button class="button pull-right js__overlay-close" data-overlay="{{$studentPracticeTestQuestion->pivot->id}}">
                                    {{__('globals.close')}}
                                </button>
		                    </div>
		                    <div class="overlay__close js__overlay-close" data-overlay="{{$studentPracticeTestQuestion->pivot->id}}">
		                        <i class="icon-close"></i>
		                    </div>
		                </div>
		            </div>
		        </div>
    		@endforeach
    	@endforeach
    </div>
@endsection
@include('includes.learning-goal-test-scripts')
