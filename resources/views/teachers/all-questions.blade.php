@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('teachers.view')}} {{__('teachers.questions')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection


@section('content')

<div class="row">
    <div class="col-xs-4 form-group" style="margin-top: 3%">
        <label>{{__('admins.learning_goal')}}</label>
        <select class="js-example-basic-single select2 form-control" id="questionLearningGoal">
            <option value="0" selected>{{__('teachers.all')}}</option>
            @foreach($learningGoalData as $key => $learningGoal)
                        <option value="{{$key}}">{{$learningGoal}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-xs-4 col-xs-offset-4 form-group" style="margin-top: 3%">
       <a href="javascript:void(0)" class="button pull-right" id="uniqueQuestions"
       data-val="{{config('constants.YES')}}" style="margin-top: 6%">
            {{__('teachers.filter_unique_questions')}}
        </a>
        <a href="javascript:void(0)" class="button pull-right" id="allQuestions"
        data-val="{{config('constants.NO')}}" style="margin-top: 6%; display: none;">
            {{__('teachers.show_all_questions')}}
        </a>
    </div>
</div>
<hr class="section-divider"/>
<div class="row mt-30">
    <div class="col-xs-12">
        <table id="question-table" class="display table table-hover table-bordered">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">{{__('admins.learning_goal')}}</th>
                    <th class="text-center">{{__('teachers.difficulty')}}</th>
                    <th class="text-center">{{__('admins.action')}}</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<input type="hidden" id="questionsRoute" value="{{$questionsRoute}}">
<input type="hidden" id="viewQuestionRoute" value="{{$viewQuestionRoute}}">
<input type="hidden" name="correctAnswer" id="correctAnswer"
value="{{__('admins.correct_answer')}}">

<div class="overlay" data-overlay="question_view">
    <div class="overlay__bg"></div>
    <div class="container">
        <div class="overlay__inner">
            <div class="overlay__scroll">
                <div class="overlay__title">
                    {{__('teachers.question')}}
                </div>
                <div class="row block__table directQuestionDiv" id="questionDiv">

                </div>
                <div class="row block__table directQuestionDiv" id="optionDiv">

                </div>
            </div><!-- end overlay-scroll-->
            <div class="overlay__close js__overlay-close" data-overlay="question_view">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
</div>

<hr class="section-divider"/>
@endsection
@section('scripts')
    @include('learning-goals.ckeditor-script')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
