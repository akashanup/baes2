@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools hideDivider">
        <div class="col-xs-6">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.classes_management')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
        <div class="col-xs-6">
            <a href="" style="width: auto;" class="float-right js__overlay-show button button--quaternary" data-overlay="make-class">{{__('teachers.create_class')}}</a>

            <div class="dropdown">
                <ul class="dropdown__list js__dropdown" id="classYearDropdown">
                        <i class="icon-arrow"></i>
                    @foreach($years as $year)
                        <li>
                            <button class="button button--quaternary button-25p" value="{{ $year }}">{{__('teachers.year')}}: {{ $year }}</button>
                        </li>
                    @endforeach
                    <li class="active">
                        <button value="{{config('constants.ALL')}}" class="button button--quaternary button--100">{{__('teachers.year')}}: {{__('teachers.no')}}</button>
                    </li>
                </ul>
            </div>
            <button class="button button button--submit" id="getYearClasses">{{__('teachers.apply')}}</button>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <table class="table table-baes table-hover" id="class-details-table">
            <thead>
            <tr>
                <th class='text-center'>{{__('teachers.year')}}</th>
                <th class='text-center'>{{__('teachers.class')}}</th>
                <th class='text-center'>{{__('teachers.students')}}</th>
                <th class='text-center'>{{__('admins.edit')}}</th>
            </tr>
            </thead>
        </table>
</div>
<input type="hidden" name="classListRoute" id="classListRoute"
 value="{{$classListRoute}}"/>
@include('includes.create-class')
@include('includes.edit-class')
<div id="delete_model" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="text-align: center;">
            <div class="modal-body">
                <h2>{{__('teachers.delete_confirm_message')}}</h2>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete_confirm" class="btn btn-default" >Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="deleteClassRoute" id="deleteClassRoute"
 value="{{$deleteClassRoute}}">
<!--Success modal ends-->
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/teachers.js').'?v='.$version}}"></script>
@endsection
