@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('css')
    @include('includes.datatable-css')
@endsection

@section('breadcumb')
    <div class="row tools-multiple hideDivider">
        <div class="col-xs-5">
            @include('includes.breadcumb-header')
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="itemListElement" href="" title="" class="breadcrumbs__link">
                        <span itemprop="name">
                            {{__('teachers.student_management')}}
                        </span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
            @include('includes.breadcumb-footer')
        </div>
        <div class="col-xs-7 text-right">
            <div class="dropdown">
                <ul class="dropdown__list js__dropdown" id="studentYearFilter">
                        <i class="icon-arrow"></i>
                    @foreach($years as $year)
                        <li class="{{$year === $selectedyear ? 'active' : ''}}">
                            <button class="button button--quaternary button-25p" value="{{ $year }}">{{__('teachers.year')}}: {{ $year }}</button>
                        </li>
                    @endforeach
                    <li class="{{$selectedyear === config('constants.ALL') ? 'active' : ''}}">
                        <button value="all" class="button button--quaternary button--100">{{__('teachers.year')}}: {{__('teachers.no')}}</button>
                    </li>
                </ul>
            </div>
            <div class="dropdown">
                <ul class="dropdown__list js__dropdown" id="studentClassFilter">
                    <i class="icon-arrow"></i>
                    <li class="{{$selectedGrade === config('constants.ALL') ? 'active' : ''}}">
                        <button value="all" class="button button--quaternary button--100">{{__('teachers.class')}}: {{__('teachers.no')}}</button>
                    </li>
                    @foreach($gradeNames as $key => $gradeName)
                        <li class="{{$selectedGrade === $key ? 'active' : ''}}">
                            <button class="button button--quaternary button--100" value="{{ $key }}">{{__('teachers.class')}}: {{ $gradeName }}</button>
                        </li>
                    @endforeach
                </ul>
            </div>
                <button class="button button button--submit float-right" id="studentListFilter" data-route="">{{__('teachers.apply')}}</button>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <table class="table table-baes table-hover" id="students-details-table">
            <thead>
                <tr>
                    <th class='text-center'>{{__('teachers.learner')}}</th>
                    <th class='text-center'>{{__('teachers.year')}}</th>
                    <th class='text-center'>{{__('teachers.class')}}</th>
                    <!-- <th class='text-center'>{{__('teachers.subclass')}}</th> -->
                    <!-- <th class='text-center'>{{__('teachers.baes')}}<br>{{__('teachers.score')}}</th> -->
                    <th class='text-center'>{{__('teachers.status')}}</th>
                    <th class='text-center'>{{__('teachers.selection')}}</th>
                </tr>
            </thead>
            
        </table>
    </div>
</div>
<div class="text-right mt-20">
    <button class="button" id = "editStudents">{{__('admins.edit')}} {{__('teachers.selection')}}</button>
</div>
@include('includes.edit-selection')
<input type="hidden" name="getstudentsListRoute" 
id="getstudentsListRoute" value="{{$getstudentsListRoute}}">
<input type="hidden" name="defaultRoute" id="defaultRoute" 
    value="{{$defaultRoute}}"/>
    <input type="hidden" name="courseId" id="courseId" value="{{$courseId}}">
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/teachers.js').'?v='.$version}}"></script>
@endsection