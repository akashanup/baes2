@extends('layouts.master')

@section('header')
    @include('includes.teacher-header')
@endsection

@section('content')
    <div class="title title--page">
        {{__('teachers.teachers')}} {{__('users.dashboard')}}
    </div>
    <div class="row">
        <div class="col-xs-6 pb50">
            <div class="col-xs-12">
                <div class="content__block dashboard_block" style="background-image: url('/images/achtergrond5.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('teachers.classes_overview')}}</h3>
                            </div>

                            <div class="col-xs-7">
                                <div class="dropdown">
                                    <div class="dropdown__label">{{__('teachers.select_class')}}:</div>
                                    <ul class="dropdown__list js__dropdown" id="gradeDropdown">
                                        <i class="icon-arrow"></i>
                                        <li><button value="no-class" class="button button--quaternary button--100" data-route="{{route('grades.overview', [$licenseId, $course, 'all', $subjectSlug, config('constants.FULL_TIME')])}}">{{__('teachers.all')}}</button></li>
                                        @foreach($classes as $key => $class)
                                            <li class="{{$defaultGrade->slug === $key ? 'active' : ''}}"><button class="button button--quaternary button--100" value="{{ $key }}" data-route="{{route('grades.overview', [$licenseId, $course, $key, $subjectSlug, config('constants.FULL_TIME')])}}">{{ $class}}</button></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <a href="javascript:void(0)" class="button" id="class_overview_btn" data-route="{{route('home')}}">{{__('teachers.apply')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="content__block dashboard_block" style="background-image: url('/images/achtergrond5.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('teachers.managing_practice_test')}}</h3>
                            </div>

                            <div class="col-xs-7">
                               <a href="{{route('teachers.manageTest', [$licenseId, $course])}}" class="button button--icon button--quaternary button--100 pull-right">
                                    <i class="icon-arrow"></i>{{__('teachers.Management_practice_test')}}
                                </a>
                                <a href="{{route('teachers.finishedPracticeTest', [$licenseId, $course])}}" class="button button--icon button--quaternary button--100 pull-right">
                                    <i class="icon-arrow"></i>{{__('teachers.rounded_practice_keys')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="col-xs-12">
                <div class="content__block dashboard_block" style="background-image: url('/images/achtergrond4.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('teachers.administrative_management')}}</h3>
                            </div>
                            <div class="col-xs-7">
                                <a href="{{route('teachers.classManagement', [$licenseId, $course])}}" class="button button--icon button--quaternary button--100 pull-right">
                                    <i class="icon-arrow"></i>{{__('teachers.classes_management')}}
                                </a>
                                <a href="{{route('teachers.student-management', [$licenseId, $course, 'all', 'all'])}}" class="button button--icon button--quaternary button--100 pull-right">
                                    <i class="icon-arrow"></i>{{__('teachers.students_management')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="content__block dashboard_block" style="background-image: url('/images/achtergrond4.png');">
                    <div class="block__inner block__inner--small block__inner--alt">
                        <div class="row">
                            <div class="col-xs-5">
                                <h3 class="block__title">{{__('teachers.other_control')}}</h3>
                            </div>
                            <div class="col-xs-7">
                                <a href="{{route('teachers.learningGoalQuestions',
                                [$licenseId, $course])}}" class="button button--icon button--quaternary button--100 pull-right">
                                    <i class="icon-arrow"></i>{{__('teachers.view_questions')}}
                                </a>
                                <a href="javascript:void(0)" id="switchStudentEnvironment" class="button button--icon button--quaternary button--100 pull-right">
                                    <i class="icon-arrow"></i>{{__('teachers.student_environment')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="section-divider"/>
@endsection

@section('scripts')
    @include('includes.datatable-js')
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
    <script type="text/javascript" src="{{asset('js/teachers.js').'?v='.$version}}"></script>
@endsection
