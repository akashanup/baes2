<!doctype html>
<html lang="{{config('app.locale', 'en')}}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style type="text/css">
            .hint > ol > li {
                margin: 1em;
            }
            .page-break {
                page-break-after: always;
            }
            .directQuestionDiv img{
                width: 80% !important;
                height: auto !important;
                text-align: center;
                display: block;
                margin: 0 auto;
            }
            .example-section h2 {
                page-break-before: always;
            }
            .page-break{
                page-break-after: always;
            }
            .no-page-break{
                page-break-before: always;
            }
           @page {
                margin: 2cm;
            }

        </style>

        <!--[if lte IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>
    <body>
        <div class="container" id="content">
            <div class="row">
                <div style="font-size: xx-large; margin-top: 20px;">{{$learningGoal->name}}</div>
                <hr class="section-divider"/>
                <div class="directQuestionDiv">{!! $learningGoal->theory !!}</div>
                <div class="directQuestionDiv example-section">
                    {!! $learningGoal->example !!}</div>
                <!-- <div class="page-break"></div> -->
                <div style="text-align: center; margin-top: 40%;" class="no-page-break">
                <img style="width: 75%;" src="http://newdev.timplicity.com/images/logo.gif"/>
                <br/>
                <div style="text-align:center;">Meer weten?</div>
                <a href="https://www.baes.education" style="text-align:center;">www.baes.education</a>
                <div style="text-align:center; margin-top:20px;">Copyright by BAES Education</div>
                </div>
            </div>

        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                MathJax.Hub.Queue(function(){
                    $(document).find('table').addClass('table');
                    window.print();
                });
          });

          
          // window.print();
        </script>
		
    </body>
</html>