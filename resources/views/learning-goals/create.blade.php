@extends('layouts.master')

@section('title')
    {{__('admins.add_learning_goal')}}
@endsection

@section('css')
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('learning-goals.index')}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.learning_goals')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.add_learning_goal')}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                    <form action="{{route('learning-goals.store')}}" method="POST" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">{{__('admins.curriculum')}}</label>
                            <select class="js-example-basic-multiple select2 form-control" name="curriculum_id[]" multiple="multiple" required>
                                @foreach($curriculumList as $curriculum)
                                    <option value="{{$curriculum->id}}">{{$curriculum->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.name')}}</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.theory')}}</label>
                            <textarea name="theory" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.example')}}</label>
                            <textarea name="example" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Video1</label>
                            <input type="text" class="form-control" name="video1" required>
                        </div>
                        <div class="form-group">
                            <label for="">Video2</label>
                            <input type="text" class="form-control" name="video2" required>
                        </div>
                        <div class="form-group">
                            <label for="">Video {{__('admins.text')}} 1</label>
                            <input type="text" class="form-control" name="video_text1" required>
                        </div>
                        <div class="form-group">
                            <label for="">Video {{__('admins.text')}} 2</label>
                            <input type="text" class="form-control" name="video_text2" required>
                        </div>
                        <div class="form-group mt-40 text-right">
                            <button type="submit" class="button submitBtn">{{__('admins.save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <hr class="section-divider"/>
        <input type="hidden" id="awsImageUpload" value="{{route('admins.uploadImage')}}">
    </div>
@endsection

@section('scripts')
    @include('learning-goals.ckeditor-script')
@endsection
