@extends('layouts.master')

@section('title')
    {{__('admins.learning_goals')}}
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.learning_goals')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <a class="button button--quaternary mb-20" href="{{route('learning-goals.create')}}">
                {{__('admins.add_learning_goal')}}
            </a>
        </div>
        <div class="row mt-30">
            <table class="table table-baes table-hover" id="lg_management_table">
                    <thead>
                    <tr>
                        <th class='text-center'>Id</th>
                        <th class='text-center' width="35%">{{__('admins.learning_goal')}}</th>
                        <th class='text-center custom-width'>{{__('admins.theory')}}</th>
                        <th class='text-center'>MCQ {{__('admins.questions')}}</th>
                        <th class='text-center'>Direct {{__('admins.questions')}}</th>
                    </tr>
                    </thead>
                </table>
        </div>
        <hr class="section-divider"/>

        <input type="hidden" id="currentRoute" value="{{$currentRoute}}">
        <input type="hidden" id="curriculumData" value="{{$curriculumDataRoute}}">
        <input type="hidden" id="learningGoalRoute" value="{{$learningGoalRoute}}">
        <input type="hidden" id="learningCurriculumRoute" value="{{$learningCurriculumRoute}}">
        <form role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="file" name="fileUpload" class="hidden" id="fileUpload" required>
        </form>
        <form role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="file" name="uploadTheoryPdfForm" class="hidden" id="uploadTheoryPdfForm" required>
        </form>
    </div>

    <!-- Delete learning goal overlay -->
        <div class="overlay" data-overlay="deleteLearningGoal">
            <div class="overlay__bg"></div>
            <div class="container">
                <div class="overlay__inner" style="text-align: center;">
                    <div class="overlay__title">
                        <div class="row">
                            {{__('admins.confirm_delete')}}
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-xs-offset-4">
                                <a class="button pull-right js__overlay-close" data-overlay="deleteLearningGoal">{{__('admins.close_screen')}}</a>
                            </div>
                            <div class="col-xs-2 col-xs-offset-1">
                                <form action="" method="POST" role="form" id="deleteLearningGoalForm">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" class="button submitBtn">{{__('admins.delete')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="overlay__close js__overlay-close" data-overlay="deleteLearningGoal">
                        <i class="icon-close"></i>
                    </div>
                </div>
            </div>
        </div>
@endsection


@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{asset('js/main.js').'?v='.$version}}"></script>
@endsection
