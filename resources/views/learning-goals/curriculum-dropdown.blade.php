<label class="curriculumLabel"> Curriculum: 
	<select class="singleSelect select2 form-control" name="curriculumDropDown" id="curriculumDropDown">
		<option value='' data-route="{{route('learning-goals.indexData')}}">All</option>
		@foreach($CurriculumData as $curriculumList)
			<option value="{{$curriculumList->slug}}" data-route="{{route('learning-goals.indexData', $curriculumList->slug)}}">{{$curriculumList->name}}</option>
		@endforeach
	</select>
</label>