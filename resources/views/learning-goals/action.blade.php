@if(!empty($theoryRoute))
	<a class="center-block col-xs-3" href="{{$theoryRoute}}" title="{{__('admins.edit')}}">
		<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($deleteLearningGoal))
	<a class="center-block col-xs-3 js__overlay-show deleteLearningGoal" title="{{__('admins.delete')}}" href="javascript:void(0)" 
	data-route = "{{$deleteLearningGoal}}" data-overlay="deleteLearningGoal">
		<i class="fa fa-close" aria-hidden="true"></i>
	</a>
@endif
@if(!empty($printTheoryPdf))
	<a class="center-block col-xs-3" href="{{$printTheoryPdf}}"	
		title="{{__('admins.print_theory_pdf')}}">
		<i class="fa fa-print" aria-hidden="true"></i>
	</a>
@endif
@if(!empty($uploadTheoryPdf))
	<a class="center-block col-xs-3" href="javascript:void(0)"  id="uploadTheoryPdf" data-route = "{{$uploadTheoryPdf}}"
		title="{{__('admins.upload_theory_pdf')}}">
		<i class="fa fa-upload" aria-hidden="true"></i>
	</a>
@endif


@if(!empty($mcqRoute))
	<a class="col-xs-3 center-block" href="{{$mcqRoute}}" title="{{__('admins.edit')}}">
		<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($mcqDownloadRoute))
	<a class="col-xs-3 center-block" href="{{$mcqDownloadRoute}}" title="{{__('admins.export_data')}}">
		<i class="fa fa-download" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($mcqQuestionPdf))
	<a class="col-xs-3 center-block" href="{{$mcqQuestionPdf}}" title="{{__('admins.mcq_question_pdf')}}">
		<i class="fa fa-download" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($uploadMcqTemplate))
	<a class="col-xs-3 center-block" href="{{$uploadMcqTemplate}}" title="{{__('admins.upload_mcq')}}">
		<i class="fa fa-upload" aria-hidden="true"></i>
	</a>
@endif




@if(!empty($editDirect))
	<a class="col-xs-2" href="{{$editDirect}}" title="{{__('admins.edit')}}">
		<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($exportData))
	<a class="col-xs-2 center-block" href="{{$exportData}}" title="{{__('admins.export_data')}}">
		<i class="fa fa-download" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($directQuestionPdf))
	<a class="col-xs-2 center-block" href="{{$directQuestionPdf}}" title="{{__('admins.direct_question_pdf')}}">
		<i class="fa fa-download" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($exportVariable))
	<a class="col-xs-2 center-block" href="{{$exportVariable}}" title="{{__('admins.export_variable_set')}}">
		<i class="fa fa-download" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($uploadVariable))
	<a class="uploadVariableSet col-xs-2 center-block" data-route="{{$uploadVariable}}" href="javascript:void(0)" title="{{__('admins.import_variable_set')}}">
		<i class="fa fa-upload" aria-hidden="true"></i>
	</a>
@endif

@if(!empty($uploadDirectTemplate))
	<a class="col-xs-2 center-block" href="{{$uploadDirectTemplate}}" title="{{__('admins.upload_direct')}}">
		<i class="fa fa-upload" aria-hidden="true"></i>
	</a>
@endif

