@extends('layouts.master')

@section('title')
    {{__('admins.edit_learning_goal')}}
@endsection

@section('css')
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/76b0a162-767c-4376-9d5d-de188d65b4b2.css"/>
@endsection

@section('header')
    @include('includes.header')
@endsection

@section('breadcumb')
    @include('includes.breadcumb-header')
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="{{route('learning-goals.index')}}" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{__('admins.learning_goals')}}
                </span>
            </a>
            <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="itemListElement" href="javascript:void(0)" title="" class="breadcrumbs__link">
                <span itemprop="name">
                    {{$LearningGoalDetails->name}}
                </span>
            </a>
            <meta itemprop="position" content="3" />
        </li>
    @include('includes.breadcumb-footer')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                    <form action="{{route('learning-goals.update', $LearningGoalDetails->slug)}}" method="POST" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="learningGoalId" class="form-control" value="{{$LearningGoalDetails->id}}">
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="">{{__('admins.curriculum')}}</label>
                            <select class="js-example-basic-multiple select2 form-control" name="curriculum_id[]" multiple="multiple" required>
                                @foreach($curriculumList as $curriculum)
                                    <option value="{{$curriculum->id}}"
                                        {{ in_array($curriculum->id, $existingCurriculumIds) ? 'selected' : '' }}>{{$curriculum->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.name')}}</label>
                            <input type="text" class="form-control" name="name" value="{{$LearningGoalDetails->name}}"required>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.theory')}}</label>
                            <textarea name="theory" class="form-control" required>
                                {{$LearningGoalDetails->theory}}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admins.example')}}</label>
                            <textarea name="example" class="form-control" required>
                                {{$LearningGoalDetails->example}}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Video1</label>
                            <input type="text" class="form-control" value="{{json_decode($LearningGoalDetails->video, true)[0]['url']}}" name="video1" required>
                        </div>
                        <div class="form-group">
                            <label for="">Video2</label>
                            <input type="text" class="form-control" value="{{json_decode($LearningGoalDetails->video, true)[1]['url']}}" name="video2" required>
                        </div>
                        <div class="form-group">
                            <label for="">Video {{__('admins.text')}} 1</label>
                            <input type="text" class="form-control" value="{{json_decode($LearningGoalDetails->video, true)[0]['name']}}" name="video_text1" required>
                        </div>
                        <div class="form-group">
                            <label for="">Video {{__('admins.text')}} 2</label>
                            <input type="text" class="form-control" value="{{json_decode($LearningGoalDetails->video, true)[1]['name']}}" name="video_text2" required>
                        </div>
                        <div class="form-group mt-40 text-right">
                            <button id="view_learning" class="button js__overlay-show" data-overlay="learning_goal_view">View</button>
                            <button type="submit" class="button">{{__('admins.update')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <hr class="section-divider"/>
        <input type="hidden" id="awsImageUpload" value="{{route('admins.uploadImage')}}">
    <!-- learning goal preview  -->
    @include('includes.learning-goal-preview')
    </div>
@endsection

@section('scripts')
    @include('learning-goals.ckeditor-script')
@endsection
