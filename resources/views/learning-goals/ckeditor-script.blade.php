<script src="https://cdn.ckeditor.com/4.7.2/full-all/ckeditor.js"></script>
<script type="text/javascript" src="{{asset('js/ckeditor-learning-goal.js').'?v='.$version}}"></script>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
<script type="text/javascript">
	var mathjax ='//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML';
</script>
<script type="text/javascript">
    $(function () {
        $.fn.replacePercentageFn = function () {
            $.each(
                $('.directQuestionDiv'),
                function (i, directQuestionDiv) {
                    var directQues = $(directQuestionDiv).html();
                    directQues     = directQues.replace(/\\%/g, '%');
                    $(directQuestionDiv).html('');
                    $(directQuestionDiv).html(directQues);
                }
            );
        };

        MathJax.Hub.Queue(
            function () {
                $.fn.replacePercentageFn();
            }
        );
    }
);
</script>

