<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
     */

    'accepted' => 'Het :attribute moet worden geaccepteerd.',
    'active_url' => 'Het :attribute is geen geldige URL.',
    'after' => 'Het :attribute moet een datum zijn :date.',
    'after_or_equal' => 'Het :attribute moet een datum na of gelijk aan :date.',
    'alpha' => 'Het :attribute mag alleen letters bevatten.',
    'alpha_dash' => 'Het :attribute mag bevat alleen letters, cijfers en streepjes.',
    'alpha_num' => 'Het :attribute mag bevat alleen letters en cijfers.',
    'array' => 'Het :attribute moet een array zijn.',
    'before' => 'Het :attribute moet wees een datum voor :date.',
    'before_or_equal' => 'Het :attribute moet wees een datum voor of gelijk aan :date.',
    'between' => [
        'numeric' => 'Het :attribute moet worden tussen :min en :max.',
        'file' => 'Het :attribute moet worden tussen :min en :max kilobytes.',
        'string' => 'Het :attribute moet worden tussen :min en :max tekens.',
        'array' => 'Het :attribute moet hebben tussen :min en :max items.',
    ],
    'boolean' => 'Het :attribute veld moet waar of onwaar zijn',
    'confirmed' => 'Het :attribute de bevestiging komt niet overeen.',
    'date' => 'Het :attribute is geen geldige datum.',
    'date_format' => 'Het :attribute oomt niet overeen met het formaat :format.',
    'different' => 'Het :attribute en :other moet anders zijn.',
    'digits' => 'Het :attribute moet worden :digits cijfers.',
    'digits_between' => 'Het :attribute moet worden tussen :min en :max cijfers.',
    'dimensions' => 'Het :attribute heeft ongeldige afbeeldingsafmetingen.',
    'distinct' => 'Het :attribute veld heeft een dubbele waarde.',
    'email' => 'Het :attribute moet worden een geldig e-mailadres',
    'exists' => 'Het gekozen :attribute is ongeldig.',
    'file' => 'Het :attribute moet een bestand zijn.',
    'filled' => 'Het :attribute veld moet een waarde hebben.',
    'image' => 'Het :attribute moet een afbeelding zijn.',
    'in' => 'Het gekozen :attribute is ongeldig.',
    'in_array' => 'Het :attribute veld bestaat niet in :other.',
    'integer' => 'Het :attribute moet worden een geheel getal.',
    'ip' => 'Het :attribute moet worden een geldig IP-adres.',
    'ipv4' => 'Het :attribute moet worden een geldig IPv4 adres.',
    'ipv6' => 'Het :attribute moet worden een geldig IPv6 adres.',
    'json' => 'Het :attribute moet worden een geldig JSON string.',
    'max' => [
        'numeric' => 'Het :attribute moet niet worden groter dan :max.',
        'file' => 'Het :attribute moet niet worden groter dan :max kilobytes.',
        'string' => 'Het :attribute moet niet worden groter dan :max tekens.',
        'array' => 'Het :attribute moet niet hebben meer dan :max items.',
    ],
    'mimes' => 'Het :attribute moet worden een bestand van file: :values.',
    'mimetypes' => 'Het :attribute moet worden een bestand van file: :values.',
    'min' => [
        'numeric' => 'Het :attribute moet worden minstens :min.',
        'file' => 'Het :attribute moet worden minstens :min kilobytes.',
        'string' => 'Het :attribute moet worden minstens :min tekens.',
        'array' => 'Het :attribute moet hebben minstens :min items.',
    ],
    'not_in' => 'Het gekozen :attribute is ongeldig.',
    'numeric' => 'Het :attribute moet een nummer zijn.',
    'present' => 'Het :attribute veld moet aanwezig zijn.',
    'regex' => 'Het :attribute formaat is ongeldig.',
    'required' => 'Het :attribute veld is benodigd.',
    'required_if' => 'Het :attribute veld is benodigd wanneer :other is :value.',
    'required_unless' => 'Het :attribute veld is benodigd tenzij :other is in :values.',
    'required_with' => 'Het :attribute veld is benodigd wanneer :values is aanwezig.',
    'required_with_all' => 'Het :attribute veld is benodigd wanneer :values is aanwezig.',
    'required_without' => 'Het :attribute veld is benodigd wanneer :values is niet aanwezig.',
    'required_without_all' => 'Het :attribute veld is benodigd wanneer geen van :values zijn aanwezig.',
    'same' => 'Het :attribute en :other moet overeenkomen.',
    'size' => [
        'numeric' => 'Het :attribute moet be :size.',
        'file' => 'Het :attribute moet be :size kilobytes.',
        'string' => 'Het :attribute moet be :size tekens.',
        'array' => 'Het :attribute moet bevatten :size items.',
    ],
    'string' => 'Het :attribute moet een tekenreeks zijn.',
    'timezone' => 'Het :attribute moet een geldige zone zijn.',
    'unique' => 'Het :attribute is al bezet.',
    'uploaded' => 'Het :attribute mislukt uploaden',
    'url' => 'Het :attribute formaat is ongeldig.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
     */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
     */

    'attributes' => [],

];
