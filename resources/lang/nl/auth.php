<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed' => 'Het e-mailadres of wachtwoord is onjuist.',
    'throttle' => 'Er zijn te veel aanmeldpogingen gedaan. Probeer het opnieuw over :seconds seconden.',

];
