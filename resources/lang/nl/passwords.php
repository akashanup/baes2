<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
     */

    'password' => 'Wachtwoorden moeten minstens zes karakters bevatten en overeenkomen met de bevestiging.',
    'reset' => 'Je wachtwoord is gereset.',
    'sent' => 'Er is een e-mail verstuurd met instructies om jouw wachtwoord te herstellen.',
    'token' => 'De wachtwoord reset token is ongeldig',
    'user' => "We kunnen geen gebruiker vinden met dat e-mailadres.",

];
