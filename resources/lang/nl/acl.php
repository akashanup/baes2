<?php

return [
"role_created" => "Nieuwe rol aangemaakt.",
"permission_created" => "Nieuwe toestemming opgeslagen.",
"user_created" => "Nieuwe gebruiker aangemaakt.",
"role_updated" => "Rol bijgewerkt.",
"permission_updated" => "Toestemming bijgewerkt.",
"user_updated" => "Gebruiker bijgewerkt.",
"role_deleted" => "Rol verwijderd.",
"permission_deleted" => "Toestemming verwijderd.",
"user_deleted" => "Gebruiker verwijderd.",
"role_permission_updated" => "Machtigingen bijgewerkt.",
"user_permission_updated" => "Machtigingen bijgewerkt voor de gebruiker.",
];