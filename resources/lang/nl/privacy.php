<?php

return [
"line_1" => "Privacy policy voor Baes Education, eigenaar van www.baes.education en www.baes.academy.",
"point_1" => "Waarborgen Privacy",
"point_1_c" => "Het waarborgen van de privacy van bezoekers van Baes.education en Baes.academy is
                                         een belangrijke taak voor ons. Daarom beschrijven we in onze privacy policy welke
                                         informatie we verzamelen en hoe we deze informatie gebruiken. We delen jouw
                                         studiegegevens - op individueel niveau - niet met iemand anders dan jouw docent(en) in
                                         het kader van het optimaliseren van je slagingskansen en het verbeteren van de
                                         begeleiding van de docent aan de student. BAES Education zal analyses kunnen
                                         maken op basis van geanonimiseerde data.",
"point_2" => "Toestemming",
"point_2_c" => "Door de informatie en de diensten op Baes.education en Baes.academy te gebruiken,
                                         gaat u akkoord met onze privacy policy en de voorwaarden die wij hierin hebben
                                         opgenomen.",
"point_3" => "Vragen",
"point_3_c" => "Als u meer informatie wilt ontvangen, of vragen hebt over de privacy policy van Baes
                                         Education en specifiek www.baes.education en www.baes.academy, kun u ons
                                         benaderen via e-mail. Ons e-mailadres is info@baes.education.",
"point_4" => "Monitoren gedrag bezoeker",
"point_4_c" => "www.baes.education en www.baes.academy maakt gebruik van verschillende
                                         technieken om bij te houden wie de website bezoekt, hoe deze bezoeker zich op de
                                         website gedraagt en welke pagina’s worden bezocht. Dat is een gebruikelijke manier
                                         van werken voor websites omdat het informatie oplevert op die bijdraagt aan de
                                         kwaliteit van de gebruikerservaring. De informatie die we, via cookies, registreren,
                                         bestaat uit onder meer IP-adressen, het type browser en de bezochte pagina’s.
                                         Tevens monitoren we waar bezoekers de website voor het eerst bezoeken en vanaf
                                         welke pagina ze vertrekken. Deze informatie houden we anoniem bij en is niet",
"point_5" => "Gebruik van cookies",
"point_5_c" => "Baes.education en Baes.academy plaatst cookies bij bezoekers. Dat doen we om
                                         informatie te verzamelen over de pagina’s die gebruikers op onze website bezoeken,
                                         om bij te houden hoe vaak bezoekers terug komen en om te zien welke pagina’s het
                                         goed doen op de website. Ook houden we bij welke informatie de browser deelt. Wij
                                         gebruiken cookies dus alleen voor onszelf, om de experience van de BAES producten
                                         te verbeteren, niet om te delen met andere partijen om hier gewin uit te halen.",
"point_6" => "Cookies uitschakelen",
"point_6_c" => "U kunt er voor kiezen om cookies uit te schakelen. Dat doet u door gebruik te maken de
                                         mogelijkheden van uw browser. U vindt meer informatie over deze mogelijkheden op de
                                         website van de aanbieder van uw browser.",
];