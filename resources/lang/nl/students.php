<?php

return [
"dashboard_intro_welcome" => "Welkom!",
"dashboard_intro_message1" => "Met BAES kun je leren op jouw niveau en eigen temp. Jij bepaalt wanneer en waar je studeert.",
"dashboard_intro_message2" => "Bekijk dit filmpje om te kijken hoe BAES bedrijfseconomie werkt.",
"dashboard_subject_heading" => "Ga direct verder met je laatste blok of toets",
"button_overview_learning_objective" => "Overzicht leerdoelen",
"button_practice_test" => "Overzicht leerdoelen",
"button_skip" => "Ga verder naar laatste leerdoel",
"exercise_test_key" => "Overzicht oefentoetsen",
"exercise_test" => "Oefentoetsen",
"begin_button" => "Beginnen",
"continue_button" => "Ga verder",
"improve_button" => "Verbeteren",
"practice_test" => "Oefentoets beschikbaar!",
"take_test" => "Maak toets",
"progress_block" => "Voortgang blok",
"score_block" => "Score blok",
"last_5_question_score" => "Score laatste 5 vragen",
"baes_score" => "BAES Score",
"next_question" => "Volgende vraag",
"error" => "Fout",
"good" => "Goed",
"try_again" => "Probeer het opnieuw",
"learning_goal_completed" => "Het leerdoel is voltooid",
"baes_score_improved" => "BAES score is verbeterd",
"attempts" => "Pogingen",
"answer_button" => "Antwoord geven",
"help" => "Hulp nodig?",
"shut_down" => "Afsluiten",
"retry_button" => "Opnieuw proberen",
"omit_period_text" => "Het is niet mogelijk een ' . ' in te voeren. Gebruik een ' , ' als decimaal teken. Duizendtallen worden automatisch verwerkt.",
"invalid_attempt" => "Maximale poging bereikt",
"improve_score" => "Verbeter score",
"next_goal" => "Volgende leerdoel",
"feedback_sent" => "Feedback is verzonden naar de beheerder.",
"baes_score_heading" => "De BAES score",
"baes_score_intro" => "In dit programma krijg je punten voor het juist beantwoorden van vragen. Deze punten worden vertaald in een BAES score. In onderstaande tabel zie je de BAES scores gekoppeld aan vijf verschillende niveaus die jij kunt behalen.",
"level" => "Niveau",
"baes_description_1" => "Hoe hoger je BAES score, hoe hoger jouw niveau. Bijvoorbeeld als je een BAES score hebt behaald van 3,2 dan is jouw niveau Junior. Je kunt jouw niveau verhogen door vragen in zo min mogelijk pogingen goed te beantwoorden. Hoe minder pogingen je nodig hebt, hoe hoger het aantal behaalde punten voor de betreffende vraag.",
"baes_description_2" => "Wanneer het aantal behaalde punten voor een vraag hoger is dan jouw huidige BAES score, dan stijgt jouw BAES score. Wanneer je in de bovenstaande situatie startend met BAES score 3,2 de volgende vraag in één keer goed beantwoordt, krijg je voor deze vraag 10 punten. Dit is hoger dan jouw BAES score van 3,2. Hierdoor zal jouw BAES score stijgen. Bij een score van 3,2 zal jouw BAES score alleen dalen wanneer je voor een volgende vraag meer dan 3 pogingen nodig hebt.",
"baes_description_3" => "Je BAES score wordt berekend op basis van het behaalde aantal punten op de laatste 5 vragen.",
"baes_description_4" => "Je wordt EindBAES door zoveel mogelijk vragen achter elkaar in 1 poging goed te beantwoorden! Je behaald daarmee een BAES score van 10. Dit is echter niet zo makkelijk als het lijkt; naarmate jouw niveau stijgt, worden de vragen steeds moeilijker.",
"baes_description_5" => "Leerdoel behalen",
"baes_description_6" => "Om een leerdoel te behalen moet je aan 2 voorwaarden voldoen",
"baes_description_7" => "Je moet minimaal het niveau ‘BAES’ hebben behaald als je aan de vraag begint",
"baes_description_8" => "én de vraag van het betreffende leerdoel moet in maximaal 2 pogingen goed beantwoord worden.",
"baes_description_9" => "Je weet wanneer een leerdoel afgesloten kan worden, doordat het afstudeerhoedje in beeld komt onder de vraag. Wanneer je het leerdoel hebt behaald dan begin je het volgende leerdoel (na het beantwoorden van een verplichte theorievraag) op het niveau waarmee je het vorige leerdoel hebt afgesloten. Dit kan dus betekenen dat je met het goed beantwoorden van 2 vragen een nieuw leerdoel behaald.",
"number_of_attempts" => "Aantal pogingen",
"points" => "Punten",
"overview" => "overzicht",
"school_exercise_test" => "School oefentoetsen",
"no_active_test_found" => "Geen actieve toets gevonden!",
"my_exercise_test" => "Eigen oefentoetsen",
"view_result" => "Bekijk resultaat",
"make_test" => "Maak de toets",
"time_left" => "Resterende tijd",
"result" => "Resultaat",
"ansering_question" => "Beantwoorde vragen",
"submit_test" => "Toets inleveren",
"closed_feedback" => "Je hebt deze toets reeds ingeleverd, de resultaten zijn pas te zien nadat de docent deze publiceert.",
"unansweredQuestionMessage" => "Je hebt nog niet alle vragen beantwoord, weet je zeker dat je de toets wilt inleveren?",
"exercise_test_starts_at" => "De oefentoets is beschikbaar vanaf:",
"student_answer" => "Antwoord van de student:",
"minutes" => "minuten",
"attempt" => "Poging",
"answer_error" => "Fout",
"correct_answer" => "Het juiste antwoord is:",
"exercise_test_already_finished" => "Oefentest is al voltooid.",
"subjects_overview" => "Blok overzicht",
];