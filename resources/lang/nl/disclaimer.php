<?php

return [
"line_1" => "Op het gebruik van deze website (www.baes.education en www.baes.academy) zijn
                                         onderstaande gebruiksvoorwaarden van toepassing. Door gebruik te maken van deze
                                         websites, wordt u geacht kennis te hebben genomen van de gebruiksvoorwaarden en
                                         deze te hebben aanvaard.",
"point_1" => "Gebruik van informatie",
"point_1_1_c" => "BAES Education Vof streeft ernaar op deze website altijd juiste en actuele informatie en kennis aan te bieden. Hoewel deze informatie met de grootst mogelijke zorgvuldigheid is samengesteld, staat BAES Education Vof niet in voor de volledigheid, juistheid of actualiteit van de informatie. Op basis van het verwereken van feedback zullen wij trachten om de informatie en kennis op zo’n hoge mogelijke kwaliteit aan te kunnen bieden.",
"point_1_2_c" => "Aan de verstrekte informatie en kennis kunnen geen rechten worden ontleend. BAES Education Vof aanvaardt geen aansprakelijkheid voor schade die voortvloeit uit het gebruik van de informatie of de website en evenmin voor het niet goed functioneren van de website.",
"point_1_3_c" => "Op basis van het verzenden en ontvangen van informatie via de website of via e-mail kan niet zonder meer een relatie tussen BAES Education Vof en de gebruiker van de website ontstaan.",
"point_2" => "E-mail en communicatie",
"point_2_c" => "BAES Education Vof garandeert niet dat aan haar gezonden e-mails (tijdig) worden ontvangen of verwerkt, omdat tijdige ontvangst van e-mails niet altijd kan worden gegarandeerd. We doen ons best om zo snel mogelijk te reageren. Ook de veiligheid van het e-mailverkeer kan niet volledig worden gegarandeerd door de hieraan verbonden veiligheidsrisico’s. Door zonder encryptie of wachtwoordbeveiliging per e-mail met BAES Education Vof te corresponderen, accepteert u dit risico.",
"point_3" => "Hyperlinks",
"point_3_c" => "Deze website kan hyperlinks bevatten naar websites van derden. BAES Education Vof heeft geen invloed op websites van derden en is niet verantwoordelijk voor de beschikbaarheid of inhoud daarvan. BAES Education Vof aanvaardt dan ook geen aansprakelijkheid voor schade die voortvloeit uit het gebruik van websites van derden.",
"point_4" => "Intellectuele eigendomsrechten",
"point_4_c" => "De vragen en de theorie van BAES Education Vof zijn beschermd door auteursrecht en andere intellectuele eigendomsrechten. Behalve voor persoonlijk en niet commercieel gebruik in het kader van jouw studie, mag niets uit deze publicaties en uitingen op welke manier dan ook verveelvoudigd, gekopieerd of op een andere manier openbaar worden gemaakt, zonder dat BAES Education Vof daar vooraf schriftelijke toestemming voor heeft gegeven. Wij stellen onze informatie zo veel mogelijk beschikbaar via derden, bijvoorbeeld via Youtube. Hiervan mag openlijk gebruik gemaakt worden en mag naar worden verwezen. ",
];