<?php

include_once 'generate-file.php';

$dictionary = [
    'admin' => [
        'en' => 'Admin',
        'nl' => 'Beheerder',
    ],
    'curriculum_management' => [
        'en' => 'Curriculum management',
        'nl' => 'Curriculum beheer',
    ],
    'create_curriculum' => [
        'en' => 'Add curriculum',
        'nl' => 'Maak curriculum aan',
    ],
    'edit_curriculum' => [
        'en' => 'Edit curriculums',
        'nl' => 'Beheer curriculum',
    ],
    'user_management' => [
        'en' => 'User Management',
        'nl' => 'Gebruikers beheer',
    ],
    'edit_licenses' => [
        'en' => 'Edit Licenses',
        'nl' => 'Beheer licenties',
    ],
    'edit_institution' => [
        'en' => 'Edit institution',
        'nl' => 'Beheer instellingen',
    ],
    'edit_user' => [
        'en' => 'Edit users',
        'nl' => 'Beheer gebruikers',
    ],
    'product_management' => [
        'en' => 'Product management',
        'nl' => 'Product beheer',
    ],
    'edit_course' => [
        'en' => 'Edit courses',
        'nl' => 'Beheer blokken',
    ],
    'edit_subject' => [
        'en' => 'Edit subjects',
        'nl' => 'Beheer vakken',
    ],
    'feedback_management' => [
        'en' => 'Feedback management',
        'nl' => 'Feedback beheer',
    ],
    'edit_feedback' => [
        'en' => 'Edit feedback',
        'nl' => 'Beheer feedback',
    ],
    'learning_goals' => [
        'en' => 'Learning goals',
        'nl' => 'Leerdoelen',
    ],
    'save' => [
        'en' => 'Save',
        'nl' => 'Opslaan',
    ],
    'dashboard' => [
        'en' => 'dashboard',
        'nl' => 'dashboard',
    ],
    'add_learning_goal' => [
        'en' => 'Add learning goal',
        'nl' => 'Voeg een leerdoel toe',
    ],
    'theory' => [
        'en' => 'THEORY',
        'nl' => 'THEORIE',
    ],

    'example' => [
        'en' => 'EXAMPLE QUESTIONS',
        'nl' => 'VOORBEELDVRAGEN',
    ],
    'text' => [
        'en' => 'text',
        'nl' => 'tekst',
    ],
    'goal_added' => [
        'en' => 'Learning goal added.',
        'nl' => 'Leerdoel succesvol toegevoegd.',
    ],
    'curriculum' => [
        'en' => 'Curriculum',
        'nl' => 'Curriculum',
    ],
    'course' => [
        'en' => 'Course',
        'nl' => 'Vak',
    ],
    'action' => [
        'en' => 'Action',
        'nl' => 'Actie',
    ],
    'subject' => [
        'en' => 'Subject',
        'nl' => 'Onderwerpen',
    ],
    'count' => [
        'en' => 'Count',
        'nl' => 'Aantal',
    ],
    'heading_lg' => [
        'en' => 'Learning goals',
        'nl' => 'Leerdoelen',
    ],
    'available' => [
        'en' => 'Available',
        'nl' => 'Beschikbare',
    ],
    'existing' => [
        'en' => 'Existing',
        'nl' => 'Bestaande',
    ],
    'update' => [
        'en' => 'Update',
        'nl' => 'Bijwerken',
    ],
    'delete' => [
        'en' => 'Delete',
        'nl' => 'Verwijder',
    ],
    'institution_added' => [
        'en' => 'Institution added',
        'nl' => 'Instelling toegevoegd',
    ],
    'view_users' => [
        'en' => 'View users',
        'nl' => '# Gebruikers',
    ],
    'curriculum_deleted' => [
        'en' => 'Curriculum deleted.',
        'nl' => 'Curriculum succesvol verwijderd.',
    ],
    'confirm_delete' => [
        'en' => 'Are you sure you want to delete this?',
        'nl' => 'Weet u zeker dat u dit wilt verwijderen?',
    ],
    'close_screen' => [
        'en' => 'Close screen',
        'nl' => 'Scherm sluiten',
    ],
    'confirm_lg_delete' => [
        'en' => 'Are you sure, you want to delete the learning goals.',
        'nl' => 'Weet u zeker dat u de leerdoelen wilt verwijderen',
    ],
    'name_curriculum' => [
        'en' => 'Curriculum Name',
        'nl' => 'Naam Curriculum',
    ],
    'edit' => [
        'en' => 'Edit',
        'nl' => 'Bewerk',
    ],
    'export_data' => [
        'en' => 'Export data',
        'nl' => 'Exportgegevens',
    ],
    'export_variable_set' => [
        'en' => 'Export variable set',
        'nl' => 'Variabele verzameling exporteren',
    ],
    'import_variable_set' => [
        'en' => 'Import variable set',
        'nl' => 'Variabele verzameling importeren',
    ],
    'upload_direct' => [
        'en' => 'Import direct question template',
        'nl' => 'Directe vraagsjabloon importeren',
    ],
    'upload_mcq' => [
        'en' => 'Import MCQ question template',
        'nl' => 'MCQ-vragen sjabloon importeren',
    ],

    'manage_licenses' => [
        'en' => 'Manage licenses',
        'nl' => 'Beheer licenties',
    ],
    'manage_institution' => [
        'en' => 'Manage institutions',
        'nl' => 'Beheer instellingen',
    ],
    'manage_user' => [
        'en' => 'Manage users',
        'nl' => 'Beheer gebruikers',
    ],
    'product_management' => [
        'en' => 'Product management',
        'nl' => 'Product beheer',
    ],
    'manage_course' => [
        'en' => 'Manage courses',
        'nl' => 'Beheer vakken',
    ],
    'manage_subject' => [
        'en' => 'Manage subjects',
        'nl' => 'Beheer blokken',
    ],
    'manage_feedback' => [
        'en' => 'Manage feedback',
        'nl' => 'Beheer feedback',
    ],
    'export_data_1' => [
        'en' => 'Export data',
        'nl' => 'Exportgegevens',
    ],
    'export_data_2' => [
        'en' => 'Export data',
        'nl' => 'Exporteer gegevens',
    ],
    'add_institution' => [
        'en' => 'Add Institution',
        'nl' => 'Voeg een instelling toe',
    ],
    'institution_added' => [
        'en' => 'Institution added',
        'nl' => 'Instelling toegevoegd',
    ],
    'create_license' => [
        'en' => 'Add License',
        'nl' => 'Voeg licentie toe',
    ],
    'license_created' => [
        'en' => 'License added.',
        'nl' => 'Licentie toegevoegd.',
    ],
    'course' => [
        'en' => 'Course',
        'nl' => 'Vak',
    ],
    'name' => [
        'en' => 'Name',
        'nl' => 'Naam',
    ],

    'goal_added' => [
        'en' => 'Learning goal added.',
        'nl' => 'Leerdoel toegevoegd.',
    ],
    'curriculum_added' => [
        'en' => 'Curriculum added.',
        'nl' => 'Curriculum toegevoegd.',
    ],
    'subject' => [
        'en' => 'Subject',
        'nl' => 'Blok',
    ],
    'curriculum_updated' => [
        'en' => 'Curriculum updated.',
        'nl' => 'Curriculum is bijgewerkt.',
    ],
    'create_course' => [
        'en' => 'Create Course',
        'nl' => 'Maak vak aan',
    ],
    'create_subject' => [
        'en' => 'Create subject',
        'nl' => 'Maak blok aan',
    ],
    'subject_name' => [
        'en' => 'Subject name',
        'nl' => 'Naam blok',
    ],
    'selected' => [
        'en' => 'Selected',
        'nl' => 'Gekozen',
    ],
    'subject_created' => [
        'en' => 'Subject created',
        'nl' => 'Blok aangemaakt',
    ],
    'institutions' => [
        'en' => 'Institutions',
        'nl' => 'Instellingen',
    ],
    'add_subject' => [
        'en' => 'Add subject',
        'nl' => 'Voeg een blok toe',
    ],
    'edit_subject' => [
        'en' => 'Edit subject',
        'nl' => 'Bewerk blok',
    ],
    'course_created' => [
        'en' => 'Course created',
        'nl' => 'Vak aangemaakt',
    ],
    'course_updated' => [
        'en' => 'Course updated',
        'nl' => 'Vak is bijgewerkt',
    ],
    'remove_institution_course' => [
        'en' => 'Are you sure that you want to remove institution(s) from the course?',
        'nl' => 'Weet je zeker dat je instelling(en) uit dit vak wilt verwijderen?',
    ],
    'remove_institution_course_effect' => [
        'en' => 'All the data for this course would be lost for the removed institution(s)',
        'nl' => 'Alle gegevens van verwijderde instelling(en) voor dit vak zullen verloren gaan',
    ],
    'remove_course_subject' => [
        'en' => 'Are you sure that you want to remove subject(s) from the course?',
        'nl' => 'Weet je zeker dat je blok(ken) uit dit vak wilt verwijderen?',
    ],
    'remove_course_subject_effect' => [
        'en' => 'All the data related to the removed subject(s) will be lost from this course.',
        'nl' => 'Alle gegevens met betrekking tot de verwijderde blok(ken) zullen uit dit vak verloren gaan.',
    ],
    'duplicatedSubjectWarning' => [
        'en' => 'Course has duplicated subjects.',
        'nl' => 'Vak heeft gedupliceerde onderwerpen.',
    ],
    'delete_course_warning' => [
        'en' => 'Are you sure that you want to delete the course?',
        'nl' => 'Weet je zeker dat je dit vak wilt verwijderen?',
    ],
    'delete_course_effect' => [
        'en' => 'All the data related to the course will also be deleted.',
        'nl' => 'Alle gegevens met betrekking tot dit vak worden ook verwijderd.',
    ],
    'course_deleted' => [
        'en' => 'Course deleted.',
        'nl' => 'Vak succesvol verwijderd.',
    ],
    'remove_subject_learning_goal' => [
        'en' => 'Are you sure that you want to remove the learning goal(s) from the subject?',
        'nl' => 'Weet je zeker dat je de leerdoellen uit het blok wilt verwijderen?',
    ],
    'remove_subject_learning_goal_effect' => [
        'en' => 'All the data related to the learning goal(s) will also be removed.',
        'nl' => 'Alle gegevens met betrekking tot deze leerdoellen worden ook verwijderd.',
    ],
    'subject_updated' => [
        'en' => 'Subject updated',
        'nl' => 'Blok is bijgewerkt',
    ],
    'subject_deleted' => [
        'en' => 'Subject deleted.',
        'nl' => 'Blok succesvol verwijderd.',
    ],
    'delete_subject_warning' => [
        'en' => 'Are you sure that you want to delete the subject?',
        'nl' => 'Weet je zeker dat je dit blok wilt verwijderen?',
    ],
    'delete_subject_effect' => [
        'en' => 'All the data related to the subject will also be deleted.',
        'nl' => 'Alle gegevens met betrekking tot dit blok worden ook verwijderd.',
    ],
    'edit_institution' => [
        'en' => 'Edit instiution',
        'nl' => 'Bewerk instelling',
    ],
    'learninggoal_updated' => [
        'en' => 'Learning goals updated.',
        'nl' => 'Leerdoel is succesvol geupdate.',
    ],
    'learninggoal_deleted' => [
        'en' => 'Learning goals deleted.',
        'nl' => 'Leerdoelen succesvol verwijderd.',
    ],
    'template_view' => [
        'en' => 'Template View',
        'nl' => 'sjabloonweergave',
    ],
    'view_more' => [
        'en' => 'View more',
        'nl' => 'Lees meer',
    ],
    'view_less' => [
        'en' => 'View less',
        'nl' => 'Lees minder',
    ],
    'invalid_video' => [
        'en' => 'Invalid video source',
        'nl' => 'Ongeldige videobron',
    ],
    'uploadods' => [
        'en' => 'Upload ODS file',
        'nl' => 'ODS-bestand uploaden',
    ],
    'upload' => [
        'en' => 'Upload',
        'nl' => 'Uploaden',
    ],
    'add_temp_sheet' => [
        'en' => 'Add Template Sheet',
        'nl' => 'Sjabloonblad toevoegen',
    ],
    'questions' => [
        'en' => 'Questions',
        'nl' => 'vragen',
    ],
    'template_inactive' => [
        'en' => 'Do you want to make all earlier templates and question for selected sheet to be inactive?',
        'nl' => 'Wilt u alle eerdere sjablonen en vragen voor het geselecteerde leerdoel inactief maken?',
    ],
    'yes' => [
        'en' => 'Yes',
        'nl' => 'Ja',
    ],
    'no' => [
        'en' => 'No',
        'nl' => 'Nee',
    ],
    'enter' => [
        'en' => 'Enter',
        'nl' => 'invoeren',
    ],
    'learning_goal' => [
        'en' => 'Learning goal',
        'nl' => 'Leerdoel',
    ],
    'manage' => [
        'en' => 'Manage',
        'nl' => 'Beheren',
    ],
    'mcq_question_pdf' => [
        'en' => 'Download MCQ questions pdf',
        'nl' => 'Download MCQ vragen pdf',
    ],
    'direct_question_pdf' => [
        'en' => 'Download Direct questions pdf',
        'nl' => 'Download Direct vragen pdf',
    ],
    'question' => [
        'en' => 'Question',
        'nl' => 'vragen',
    ],
    'question_level' => [
        'en' => 'Question level',
        'nl' => 'Niveau vraag',
    ],
    'answer' => [
        'en' => 'Answer',
        'nl' => 'Antwoord',
    ],
    'email_sent' => [
        'en' => 'You will receive a download link on your registered Email ID',
        'nl' => 'U ontvangt een downloadlink op uw geregistreerde e-mailadres',
    ],
    'download' => [
        'en' => 'Download',
        'nl' => 'Download',
    ],
    'pdf' => [
        'en' => 'PDF',
        'nl' => 'PDF',
    ],
    'no_question_found' => [
        'en' => 'Questions not exist',
        'nl' => 'Er zijn geen vragen gevonden',
    ],
    'add_direct' => [
        'en' => 'Add Direct',
        'nl' => 'Voeg direct toe',
    ],
    'template' => [
        'en' => 'Template',
        'nl' => 'Sjabloon',
    ],
    'complexity' => [
        'en' => 'Complexity',
        'nl' => 'ingewikkeldheid',
    ],
    'variable' => [
        'en' => 'Variable',
        'nl' => 'Variabelen',
    ],
    'set' => [
        'en' => 'Set',
        'nl' => 'Reeks',
    ],
    'welcome_to_baes' => [
        'en' => 'Welcome to BAES!',
        'nl' => 'Welkom bij BAES!',
    ],
    'export_data_message' => [
        'en' => 'Click on the button to download the Question PDF',
        'nl' => 'Klik op de knop om de Vraag-PDF te downloaden',
    ],
    'export_data' => [
        'en' => 'Export data',
        'nl' => 'Exportgegevens',
    ],
    'sincerely' => [
        'en' => 'Sincerely,',
        'nl' => 'Met vriendelijke groet,',
    ],
    'delete_curriculum_warning' => [
        'en' => 'Are you sure that you want to delete the curriculum?',
        'nl' => 'Weet je zeker dat je dit curriculum wilt verwijderen?',
    ],
    'delete_curriculum_effect' => [
        'en' => 'All the data related to the curriculum will also be deleted.',
        'nl' => 'Deze actie kan niet ongedaan worden gemaakt, alle gegevens met betrekking tot dit curriculum worden verwijderd.',
    ],
    'remove_curriculum_learning_goal' => [
        'en' => 'Are you sure that you want to remove the learning goal(s) from the curriculum?',
        'nl' => 'Weet je zeker dat je de leerdoellen uit het curriculum wilt verwijderen?',
    ],
    'active_for_registration' => [
        'en' => 'Active for registration',
        'nl' => 'Actief voor registratie',
    ],
    'license_type' => [
        'en' => 'License type',
        'nl' => 'Type licentie',
    ],
    'per_month' => [
        'en' => 'Per month',
        'nl' => 'Maandelijks',
    ],
    'fixed_term' => [
        'en' => 'Fixed term',
        'nl' => 'Vaste duur',
    ],
    'all' => [
        'en' => 'All',
        'nl' => 'Alle',
    ],
    'license_updated' => [
        'en' => 'License updated.',
        'nl' => 'Licentie is bijgewerkt.',
    ],
    'license_deleted' => [
        'en' => 'License deleted.',
        'nl' => 'Licentie succesvol verwijderd.',
    ],
    'delete_license_warning' => [
        'en' => 'Are you sure that you want to delete the license?',
        'nl' => 'Weet je zeker dat je de licentie wilt verwijderen?',
    ],
    'delete_license_effect' => [
        'en' => 'All the data related to the license will also be deleted.',
        'nl' => 'Alle gegevens met betrekking tot de licentie worden ook verwijderd.',
    ],
    'edit_license' => [
        'en' => 'Edit license',
        'nl' => 'Bewerk licentie',
    ],
    'institution' => [
        'en' => 'Institution',
        'nl' => 'Instellingen',
    ],
    'registration_status' => [
        'en' => 'Reg.',
        'nl' => 'Reg.',
    ],
    'user_count' => [
        'en' => 'Users',
        'nl' => 'Users',
    ],
    'subject_count' => [
        'en' => 'Subj.',
        'nl' => 'Subj.',
    ],
    'file_convention' => [
        'en' => 'File name convention not followed',
        'nl' => 'Bestandsnaam conventie niet gevolgd',
    ],
    'error_download' => [
        'en' => 'Something went wrong! Please try again.',
        'nl' => 'Er is iets mis gegaan, probeer het opnieuw.',
    ],
    'institution_updated' => [
        'en' => 'Institution updated.',
        'nl' => 'Instelling is bijgewerkt.',
    ],
    'delete_institution_warning' => [
        'en' => 'Are you sure that you want to delete the institution?',
        'nl' => 'Weet je zeker dat je de instelling wilt verwijderen?',
    ],
    'delete_institution_effect' => [
        'en' => 'All the data related to the institution will also be deleted.',
        'nl' => 'Alle gegevens met betrekking tot de instelling worden ook verwijderd.',
    ],
    'success_download' => [
        'en' => 'File download successfully',
        'nl' => 'Bestandsdownload succesvol',
    ],
    'insert_data_error' => [
        'en' => 'Unable to insert data. Error in the sheet.',
        'nl' => 'Gegevens kunnen niet worden ingevoegd. Fout in het blad.',
    ],
    'export_data' => [
        'en' => 'Export data',
        'nl' => 'Exportgegevens',
    ],
    'variable_db_error' => [
        'en' => 'Question Variable Insertion Error',
        'nl' => 'Vraag variabele invoegen Error',
    ],
    'question_db_error' => [
        'en' => 'Question Template Insertion Error',
        'nl' => 'Vraag sjabloon invoegen Error',
    ],
    'sheet_error' => [
        'en' => 'Insertion fail: Sheet Error/Variable field not present',
        'nl' => 'Invoeging fail: blad fout/variabele veld niet aanwezig',
    ],
    'question_upload' => [
        'en' => 'questions inserted successfully',
        'nl' => 'vragen succesvol toegevoegd',
    ],
    'mcq' => [
        'en' => 'MCQ',
        'nl' => 'MCQ',
    ],
    'direct' => [
        'en' => 'DIRECT',
        'nl' => 'DIRECT',
    ],
    'mcq_question_column_mismatch' => [
        'en' => 'MCQ question column name mismatch',
        'nl' => 'MCQ vraag kolomnaam mismatch',
    ],
    'mcq_answer_column_mismatch' => [
        'en' => 'MCQ answer column name mismatch',
        'nl' => 'MCQ antwoord kolom naam mismatch',
    ],
    'feedback_missing' => [
        'en' => 'Feedback is missing for question id: ',
        'nl' => 'Er is geen feedback voor vraag id: ',
    ],
    'qt_variable_mismatch' => [
        'en' => 'Insertion fail: template variable mismatch',
        'nl' => 'Invoeging fail: template variabele mismatch',
    ],
    'feedback_db_error' => [
        'en' => 'Feedback Insertion Error',
        'nl' => 'Feedback invoeging fout',
    ],
    'options' => [
        'en' => 'Option',
        'nl' => 'Option',
    ],
    'give_feedback' => [
        'en' => 'Give feedback',
        'nl' => 'Geef feedback',
    ],
    'loading' => [
        'en' => 'Loading',
        'nl' => 'Bezig met laden',
    ],
    'payment_received' => [
        'en' => 'Payment received',
        'nl' => 'Betaling ontvangen',
    ],
    'payment_details' => [
        'en' => 'Payment details',
        'nl' => 'Betalings details',
    ],
    'payment_id' => [
        'en' => 'Payment ID',
        'nl' => 'Betalings ID',
    ],
    'amount' => [
        'en' => 'Amount',
        'nl' => 'Bedrag',
    ],
    'date' => [
        'en' => 'Date',
        'nl' => 'Datum',
    ],
    'payment_pending' => [
        'en' => 'Payment pending',
        'nl' => 'Betaling in afwachting',
    ],
    'payment_failed' => [
        'en' => 'Payment failed! A link has been sent to your e-mail address to try again.',
        'nl' => 'Betaling mislukt! Er is per e-mail een link verstuurd om het opnieuw te proberen.',
    ],
    'licence_added' => [
        'en' => 'Your license has been added.',
        'nl' => 'Je licentie is toegevoegd.',
    ],
    'already_inactive' => [
        'en' => 'Your license is already inactive.',
        'nl' => 'Je licentie is al inactief.',
    ],
    'cancel_license' => [
        'en' => 'Your license has been cancelled.',
        'nl' => 'Je licentie is geannuleerd.',
    ],
    'recurring_license' => [
        'en' => 'Only subscribed licenses can be canelled',
        'nl' => 'Alleen geabonneerde licenties kunnen worden stopgezet',
    ],
    'error_license' => [
        'en' => 'Unable to cancel your license',
        'nl' => 'Kan de licentie niet annuleren',
    ],
    'confirm_cancel' => [
        'en' => 'Are you sure you want to cancel your license ?',
        'nl' => 'Weet je zeker dat je je licentie wilt annuleren?',
    ],
    'cancel' => [
        'en' => 'Cancel',
        'nl' => 'Annuleer',
    ],
    'stop_subscription' => [
        'en' => 'Stop Subscription',
        'nl' => 'Stop abonnement',
    ],
    'institution_deleted' => [
        'en' => 'Institution deleted.',
        'nl' => 'Instelling succesvol verwijderd.',
    ],
    'available_learning_goals' => [
        'en' => 'Available learning goals',
        'nl' => 'Beschikbare leerdoelen',
    ],
    'selected_learning_goals' => [
        'en' => 'Selected learning goals',
        'nl' => 'Gekozen leerdoelen',
    ],
    'learning_goal_count' => [
        'en' => 'Learning goal count',
        'nl' => 'Aantal leerdoelen',
    ],
    'delete_curriculum_instruction' => [
        'en' => 'Type DELETE to delete the curriculum.',
        'nl' => 'Type DELETE om dit curriculum te verwijderen',
    ],
    'edit_learning_goal' => [
        'en' => 'Edit learning goal',
        'nl' => 'Beheer leerdoel',
    ],
    'institution' => [
        'en' => 'Institution',
        'nl' => 'Instelling',
    ],
    'correct_answer' => [
        'en' => 'Correct answer',
        'nl' => 'Goed antwoord',
    ],
    'remove_logo' => [
        'en' => 'Remove logo',
        'nl' => 'Verwijder logo',
    ],
    'block' => [
        'en' => 'Block',
        'nl' => 'Blok',
    ],
    'activate' => [
        'en' => 'Activate',
        'nl' => 'Activeren',
    ],
    'deactivate' => [
        'en' => 'Deactivate',
        'nl' => 'Deactiveren',
    ],
    'reset_password_link_message' => [
        'en' => "Reset password link has been sent to user's email id.",
        'nl' => 'Reset wachtwoord link is verzonden naar de gebruikers e-mail id.',
    ],
    'license_status_updated' => [
        'en' => 'License status updated!',
        'nl' => 'Licentiestatus geüpdatet!',
    ],
    'feedbacks' => [
        'en' => 'Feedbacks',
        'nl' => 'Feedbacks',
    ],
    'feedback' => [
        'en' => 'Feedback',
        'nl' => 'Feedback',
    ],
    'question_id' => [
        'en' => 'Question ID',
        'nl' => 'Vraag ID',
    ],
    'click_to_make_it_processed' => [
        'en' => 'Click to make it processed',
        'nl' => 'Klik om het te verwerken',
    ],
    'click_to_make_it_unprocessed' => [
        'en' => 'Click to make it unprocessed',
        'nl' => 'Klik om het onbehandeld te maken',
    ],
    'add_a_note' => [
        'en' => 'Add a note',
        'nl' => 'Voeg een notitie toe',
    ],
    'edit_question_template' => [
        'en' => 'Edit question template',
        'nl' => 'Vraagsjabloon bewerken',
    ],
    'edit_question_variable' => [
        'en' => 'Edit question variable',
        'nl' => 'Bewerk vraagvariabele',
    ],
    'note_updated' => [
        'en' => 'Note updated',
        'nl' => 'Opmerking bijgewerkt',
    ],
    'note' => [
        'en' => 'Note',
        'nl' => 'Notitie',
    ],
    'feedback_updated' => [
        'en' => 'Feedback updated',
        'nl' => 'Feedback bijgewerkt',
    ],
    'send_processed_mail' => [
        'en' => 'Send processed email',
        'nl' => 'Stuur feedback verwerkt e-mail',
    ],
    'send_rounding_mail' => [
        'en' => 'Send rounding email',
        'nl' => 'Stuur afrondingsfout e-mail',
    ],
    'send_incorrect_mail' => [
        'en' => 'Send incorrect email',
        'nl' => 'Stuur feedback was foutief e-mail',
    ],
    'send_custom_mail' => [
        'en' => 'Send custom email',
        'nl' => 'Stuur e-mail met notitie',
    ],
    'no_mail' => [
        'en' => 'Do not send an email',
        'nl' => 'Stuur geen e-mail',
    ],
    'send_feedback_response' => [
        'en' => 'Send feedback response',
        'nl' => 'Stuur feedback reactie',
    ],
    'add' => [
        'en' => 'Add',
        'nl' => 'Toevoegen',
    ],
    'order' => [
        'en' => 'Order',
        'nl' => 'Bestellen',
    ],
    'requiredField' => [
        'en' => 'Please fill out this field!',
        'nl' => 'Dit veld is verplicht.',
    ],
    'feedback' => [
        'en' => 'Feedback',
        'nl' => 'terugkoppeling',
    ],
    'template_updated' => [
        'en' => 'Question templat updated successfully!',
        'nl' => 'Question templated succesvol bijgewerkt!',
    ],

    'variable' => [
        'en' => 'Variable',
        'nl' => 'Variabelen',
    ],
    'set' => [
        'en' => 'Set',
        'nl' => 'Reeks',
    ],
    'try_again' => [
        'en' => 'Try again',
        'nl' => 'Probeer het nog een keer',
    ],
    'view' => [
        'en' => 'View',
        'nl' => 'View',
    ],
    'number_of_variable_set' => [
        'en' => 'Number of variable set',
        'nl' => 'Aantal variabele set',
    ],
    'upload_variable_set' => [
        'en' => 'Upload variable set',
        'nl' => 'Uploaden variabele set',
    ],
    'template_added' => [
        'en' => 'Question templated added successfully!',
        'nl' => 'Question templated is succesvol toegevoegd!',
    ],
    'failed_variable_set_upload' => [
        'en' => 'Variable set upload failed. Please contact to developers.',
        'nl' => 'Variabele set uploaden mislukt. Neem contact op met ontwikkelaars.',
    ],
    'variable_mismatch' => [
        'en' => 'Variables mismatch for templateId-',
        'nl' => 'Variabelen komen niet overeen voor templateId-',
    ],
    'variable_sequence_mismatch' => [
        'en' => 'Variables sequence mismatch for templateId-',
        'nl' => 'Variabelensequentie komt niet overeen voor templateId-',
    ],
    'variables_uploaded_successfully' => [
        'en' => 'Variable set uploaded successfully!',
        'nl' => 'Variabele set is succesvol geüpload!',
    ],
    'variable_count_mismatch' => [
        'en' => 'The number of variables uploaded does not match with the existing number of variables.',
        'nl' => 'Het aantal geüploade variabelen komt niet overeen met het bestaande aantal variabelen.',
    ],
    'invalid_columns' => [
        'en' => 'Columns present in the file is not valid.',
        'nl' => 'Kolommen die in het bestand aanwezig zijn, zijn niet geldig.',
    ],
    'file_not_found' => [
        'en' => 'Sorry, but the requested file not found.',
        'nl' => 'Sorry, maar het aangevraagde bestand niet gevonden.',
    ],
    'variable_set_upload' => [
        'en' => '|Variable set upload|',
        'nl' => '|Variabele setupload|',
    ],
    'variable_set_upload_message' => [
        'en' => 'Below is the status of variable set upload for the learning goal- ',
        'nl' => 'Hieronder staat de status van variabele set upload voor het leerdoel-',
    ],
    'variable_set_updated' => [
        'en' => 'Variable set updated',
        'nl' => 'Variabele set bijgewerkt',
    ],
    'question_variable_column_mismatch' => [
        'en' => 'Question variable column name mismatch',
        'nl' => 'Vraag variabele kolom naam mismatch',
    ],
    'unable_to_insert' => [
        'en' => 'Error in creating question template.',
        'nl' => 'Fout bij het maken van een vraagsjabloon.',
    ],
    'variables_uploaded_status_mail' => [
        'en' => 'Status of the file upload will be shared though e-mail',
        'nl' => 'De status van het uploaden van het bestand zal via e-mail worden gedeeld',
    ],
    'upload_theory_pdf_status' => [
        'en' => 'PDF uploaded successfully.',
        'nl' => 'PDF geüpload met succes.',
    ],
    'upload_theory_pdf' => [
        'en' => 'Upload theory PDF',
        'nl' => 'Upload theorie PDF',
    ],
    'print_theory_pdf' => [
        'en' => 'Print theory PDF',
        'nl' => 'Afdruk theorie PDF',
    ],
    'empty_question_variable' => [
        'en' => 'Empty Value in Question Variables',
        'nl' => 'Lege waarde in vraagvariabelen',
    ],
    'no_teacher_license_found' => [
        'en' => 'No teacher license found for this institution and course.',
        'nl' => 'Geen leraar licentie gevonden voor deze instelling en dit vak.',
    ],
    'value_range' => [
        'en' => 'Value must be Between : ',
        'nl' => 'De waarde moet tussen zijn : ',
    ],
    'active_variable_set_not_found' => [
        'en' => 'No active variabe set found.',
        'nl' => 'Geen actieve variabe set gevonden.',
    ],
    'data_exported_successfully' => [
        'en' => 'Table(s) exported successfully. You will recieve an email to download the following table(s)- ',
        'nl' => 'Tabellen zijn succesvol geëxporteerd. U ontvangt een email om de volgende tabellen te downloaden- ',
    ],
    'export_data_message' => [
        'en' => 'Click on the button to download the following table(s)-',
        'nl' => 'Klik op de knop om de volgende tabellen te downloaden -',
    ],
    'error' => [
        'en' => 'Wrong',
        'nl' => 'Fout',
    ],
    'baes_cannot_be_deleted' => [
        'en' => 'Baes can\'t be deleted.',
        'nl' => 'Baes kan niet worden verwijderd',
    ],
    'license_already_exist' => [
        'en' => 'License already exists',
        'nl' => 'Licentie bestaat al',
    ],
    'already_owns_license' => [
        'en' => 'You are admin. You already own this license.',
        'nl' => 'Jij bent admin. Je bent al eigenaar van deze licentie.',
    ],
    'final_catch_response' => [
        'en' => 'Something went wrong! Please try again.',
        'nl' => 'Er is iets mis gegaan, probeer het opnieuw.',
    ],
    'export_special_data' => [
        'en' => 'Export special data',
        'nl' => 'Speciale gegevens exporteren',
    ],
    'student_activity' => [
        'en' => 'Student activity',
        'nl' => 'Student activiteit',
    ],
    'student_performance' => [
        'en' => 'Student performance',
        'nl' => 'Studentenprestaties',
    ],
    'empty_table' => [
        'en' => 'This table is empty',
        'nl' => 'Deze tabel is leeg',
    ],
    'success_status' => [
        'en' => 'Thanks! We have received the payment!',
        'nl' => 'Bedankt! We hebben de betaling ontvangen!',
    ],
    'orders' => [
        'en' => 'Orders',
        'nl' => 'Bestellingen',
    ],
    'details_billings' => [
        'en' => 'Details Payment',
        'nl' => 'Details Betaling',
    ],
    'payment_id' => [
        'en' => 'Payment ID',
        'nl' => 'Betalings ID',
    ],
    'cost' => [
        'en' => 'Cost',
        'nl' => 'Kosten',
    ],
    'success_message' => [
        'en' => 'You can now immediately start following your new course.',
        'nl' => 'Je kunt nu direct beginnen met het volgen van jouw nieuwe opleiding.',
    ],
    'my_license' => [
        'en' => 'My licenses',
        'nl' => 'Mijn licenties',
    ],
    'failed_status' => [
        'en' => 'Unfortunately, the payment has failed.',
        'nl' => ' Helaas, de betaling is mislukt.',
    ],
    'failed_message' => [
        'en' => 'Something went wrong during payment. We have sent you an e-mail about this. Try again with the button below.',
        'nl' => 'Er is iets mis gegaan tijdens te betaling. Wij hebben je hierover een e-mail verstuurd. Probeer het met onderstaande knop nogmaals.',
    ],
    'order_again' => [
        'en' => 'Order again',
        'nl' => 'Bestel opnieuw',
    ],
    'pending_status' => [
        'en' => 'Payment in anticipation',
        'nl' => 'Betaling in afwachting',
    ],
    'pending_message' => [
        'en' => 'We have not yet received a payment confirmation from Mollie. As soon as we receive it, we will send an e-mail to confirm the payment.',
        'nl' => 'Wij hebben van Mollie nog geen betaalbevestiging ontvangen. Zodra wij deze ontvangen sturen we een e-mail om de betaling te bevestigen.',
    ],
    'start_course' => [
        'en' => 'Start course',
        'nl' => 'Start vak',
    ],
];

generateFile($dictionary, basename(__FILE__));
