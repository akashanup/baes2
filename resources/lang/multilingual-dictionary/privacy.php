<?php

$languages = [


    'line_1'            => [
                                'en' => 'Privacy Policy for Baes Education, owner of www.baes.education and www.baes.academy.',
                                'nl' => 'Privacy policy voor Baes Education, eigenaar van www.baes.education en www.baes.academy.',
                           ],

    'point_1'           => [
                                'en' => 'Guarantee Privacy',
                                'nl' => 'Waarborgen Privacy',
                           ],

    'point_1_c'         => [
                                'en' => 'Ensuring the privacy of visitors to the Baes Education and the Baes Academy is
                                         An important task for us. Therefore we describe in our privacy policy which
                                         Information we collect and how we use this information. We Share your
                                         Study data-on an individual level-not with anyone other than your teacher (s) in
                                         The framework of optimizing your chances of impact and improving the
                                         Guidance from the teacher to the student. Baes \'s Education will be able to analyse
                                         Based on anonymous data.',
                                'nl' => 'Het waarborgen van de privacy van bezoekers van Baes.education en Baes.academy is
                                         een belangrijke taak voor ons. Daarom beschrijven we in onze privacy policy welke
                                         informatie we verzamelen en hoe we deze informatie gebruiken. We delen jouw
                                         studiegegevens - op individueel niveau - niet met iemand anders dan jouw docent(en) in
                                         het kader van het optimaliseren van je slagingskansen en het verbeteren van de
                                         begeleiding van de docent aan de student. BAES Education zal analyses kunnen
                                         maken op basis van geanonimiseerde data.',
                           ],

    'point_2'            => [
                                'en' => 'Permission',
                                'nl' => 'Toestemming',
                            ],

    'point_2_c'          => [
                                'en' => 'By using the information and services on the Baes Education and the Baes Academy,
                                         Do you agree with our Privacy policy and the terms and conditions that we have
                                         Included.',
                                'nl' => 'Door de informatie en de diensten op Baes.education en Baes.academy te gebruiken,
                                         gaat u akkoord met onze privacy policy en de voorwaarden die wij hierin hebben
                                         opgenomen.',
                           ],

    'point_3'            => [
                                'en' => 'Questions',
                                'nl' => 'Vragen',
                           ],

    'point_3_c'         => [
                                'en' => 'If you would like to receive more information, or have questions about the privacy policy of B
                                         Education and specifically www. Baes Education and www. Baes Academy, you can contact us
                                         Access via email. Our email address is info @ Baes. Education.',
                                'nl' => 'Als u meer informatie wilt ontvangen, of vragen hebt over de privacy policy van Baes
                                         Education en specifiek www.baes.education en www.baes.academy, kun u ons
                                         benaderen via e-mail. Ons e-mailadres is info@baes.education.',
                           ],

    'point_4'            => [
                                'en' => 'Monitor Behavior Visitor',
                                'nl' => 'Monitoren gedrag bezoeker',
                           ],
    'point_4_c'           => [
                                'en' => 'www.baesEducation and www.BaesAcademy uses different
                                         Techniques to keep track of who visits the website, how this visitor
                                         Website and which pages are visited. That\'s a common way
                                         of working for websites because it provides information that contributes to the
                                         Quality of the user experience. The information we register, via cookies,
                                         Includes IP addresses, browser type, and visited pages.
                                         We also monitor where visitors visit the website for the first time and from
                                         Which page they are leaving. We keep this information anonymous and is not
                                         Associated with other personal information.',
                                'nl' => 'www.baes.education en www.baes.academy maakt gebruik van verschillende
                                         technieken om bij te houden wie de website bezoekt, hoe deze bezoeker zich op de
                                         website gedraagt en welke pagina’s worden bezocht. Dat is een gebruikelijke manier
                                         van werken voor websites omdat het informatie oplevert op die bijdraagt aan de
                                         kwaliteit van de gebruikerservaring. De informatie die we, via cookies, registreren,
                                         bestaat uit onder meer IP-adressen, het type browser en de bezochte pagina’s.
                                         Tevens monitoren we waar bezoekers de website voor het eerst bezoeken en vanaf
                                         welke pagina ze vertrekken. Deze informatie houden we anoniem bij en is niet',
                           ],

    'point_5'            => [
                                'en' => 'Use of cookies',
                                'nl' => 'Gebruik van cookies',
                            ],

    'point_5_c'          => [
                                'en' => 'Baes Education and Baes Academy place cookies with visitors. We do this to
                                         To collect information about the pages users visit on our website,
                                         To keep track of how often visitors come back and to see which pages it
                                         Do well on the website. We also keep track of what information the browser shares. We
                                         Use cookies so only for ourselves, to the experience of the Baes \'s products
                                         To improve, not to share with other parties to take advantage of this.',
                                'nl' => 'Baes.education en Baes.academy plaatst cookies bij bezoekers. Dat doen we om
                                         informatie te verzamelen over de pagina’s die gebruikers op onze website bezoeken,
                                         om bij te houden hoe vaak bezoekers terug komen en om te zien welke pagina’s het
                                         goed doen op de website. Ook houden we bij welke informatie de browser deelt. Wij
                                         gebruiken cookies dus alleen voor onszelf, om de experience van de BAES producten
                                         te verbeteren, niet om te delen met andere partijen om hier gewin uit te halen.',
                           ],
    'point_6'            => [
                                'en' => 'Disable Cookies',
                                'nl' => 'Cookies uitschakelen',
                           ],

    'point_6_c'          => [
                                'en' => 'You can choose to disable cookies. You do this by using the
                                         capabilities of your browser. You can find more information about these possibilities on the
                                         Website of your browser\'s provider.',
                                'nl' => 'U kunt er voor kiezen om cookies uit te schakelen. Dat doet u door gebruik te maken de
                                         mogelijkheden van uw browser. U vindt meer informatie over deze mogelijkheden op de
                                         website van de aanbieder van uw browser.',
                           ],

];

$en = '';
$nl = '';
foreach ($languages as $key => $a) {
    $en .= '"'.$key.'" => "'.$a["en"].'",'."\n";
    $nl .= '"'.$key.'" => "'.$a["nl"].'",'."\n";
}

$myfile = fopen('resources/lang/en/privacy.php', 'w+') or die('Unable to open file!');
$txt    = "<?php\n\n";
fwrite($myfile, $txt);
$txt = "return [\n";
fwrite($myfile, $txt);
$txt = $en;
fwrite($myfile, $txt);
$txt = '];';
fwrite($myfile, $txt);
fclose($myfile);

$myfile = fopen('resources/lang/nl/privacy.php', 'w+') or die('Unable to open file!');
$txt    = "<?php\n\n";
fwrite($myfile, $txt);
$txt = "return [\n";
fwrite($myfile, $txt);
$txt = $nl;
fwrite($myfile, $txt);
$txt = '];';
fwrite($myfile, $txt);
fclose($myfile);
