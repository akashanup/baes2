<?php

include_once 'generate-file.php';

$dictionary = [
    'role_created' => [
        'en' => 'A new role is created.',
        'nl' => 'Nieuwe rol aangemaakt.',
    ],
    'permission_created' => [
        'en' => 'A new permission is created.',
        'nl' => 'Nieuwe toestemming opgeslagen.',
    ],
    'user_created' => [
        'en' => 'A new user is created.',
        'nl' => 'Nieuwe gebruiker aangemaakt.',
    ],
    'role_updated' => [
        'en' => 'Role updated.',
        'nl' => 'Rol bijgewerkt.',
    ],
    'permission_updated' => [
        'en' => 'Permission updated.',
        'nl' => 'Toestemming bijgewerkt.',
    ],
    'user_updated' => [
        'en' => 'User updated.',
        'nl' => 'Gebruiker bijgewerkt.',
    ],
    'role_deleted' => [
        'en' => 'Role deleted.',
        'nl' => 'Rol verwijderd.',
    ],
    'permission_deleted' => [
        'en' => 'Permission deleted.',
        'nl' => 'Toestemming verwijderd.',
    ],
    'user_deleted' => [
        'en' => 'User deleted.',
        'nl' => 'Gebruiker verwijderd.',
    ],
    'role_permission_updated' => [
        'en' => 'Permissions updated for the role.',
        'nl' => 'Machtigingen bijgewerkt.',
    ],
    'user_permission_updated' => [
        'en' => 'Permissions updated for the user.',
        'nl' => 'Machtigingen bijgewerkt voor de gebruiker.',
    ],
];

generateFile($dictionary, basename(__FILE__));
