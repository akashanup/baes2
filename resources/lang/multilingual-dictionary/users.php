<?php

include_once 'generate-file.php';

$dictionary = [
    'duplicate_email_found' => [
        'en' => 'Duplicate email found.',
        'nl' => 'Dubbele e-mail gevonden.',
    ],
    'dashboard' => [
        'en' => 'Dashboard',
        'nl' => 'Dashboard',
    ],
    'profile' => [
        'en' => 'Profile',
        'nl' => 'Profiel',
    ],
    'name' => [
        'en' => 'Name',
        'nl' => 'Naam',
    ],
    'logout' => [
        'en' => 'Logout',
        'nl' => 'Uitloggen',
    ],
    'code' => [
        'en' => 'Code',
        'nl' => 'Code',
    ],
    'student' => [
        'en' => 'Student',
        'nl' => 'Student',
    ],
    'teacher' => [
        'en' => 'Teacher',
        'nl' => 'Docenten',
    ],
    'first_name' => [
        'en' => 'First name',
        'nl' => 'Naam',
    ],
    'last_name' => [
        'en' => 'Last name',
        'nl' => 'Achternaam',
    ],
    'confirm_email' => [
        'en' => 'Confirm e-mail',
        'nl' => 'Bevestig e-mail',
    ],
    'phone_number' => [
        'en' => 'Phone number',
        'nl' => 'Telefoonnummer',
    ],
    'successful_registration_response' => [
        'en' => 'Registration successful! An activation link has been sent to your e-mail address.',
        'nl' => 'De registratie is gelukt! Er is per e-mail een activeringslink verzonden.',
    ],
    'user_not_found' => [
        'en' => 'User not found',
        'nl' => 'Gebruiker niet gevonden',
    ],
    'buy_license' => [
        'en' => 'Buy license',
        'nl' => 'Koop licentie',
    ],
    'my_courses' => [
        'en' => 'My courses',
        'nl' => 'Mijn vakken',
    ],
    'expired_token' => [
        'en' => 'The link is expired',
        'nl' => 'De link is verlopen',
    ],
    'invalid_token' => [
        'en' => 'The link is invalid',
        'nl' => 'De link is ongeldig',
    ],
    'specifications' => [
        'en' => 'Specifications',
        'nl' => 'Specificaties',
    ],
    'license_duration' => [
        'en' => 'License duration',
        'nl' => 'Licentieduur',
    ],
    'purchase' => [
        'en' => 'Purchase',
        'nl' => 'Bestellen',
    ],
    'lifetime' => [
        'en' => 'Lifetime',
        'nl' => 'Levenslang',
    ],
    'open_course' => [
        'en' => 'Open Course',
        'nl' => 'Open Vak',
    ],
    'close_license' => [
        'en' => 'Close',
        'nl' => 'Beëindigen',
    ],
    'continuous' => [
        'en' => 'Continuous',
        'nl' => 'Doorlopend',
    ],
    'create_a_test' => [
        'en' => 'Create a test',
        'nl' => 'Maak een toets aan',
    ],
    'grade' => [
        'en' => 'Class',
        'nl' => 'Klas',
    ],
    'school' => [
        'en' => 'School',
        'nl' => 'School',
    ],
    'start_test' => [
        'en' => 'Start learning goal',
        'nl' => 'Start leerdoel',
    ],
    'example' => [
        'en' => 'EXAMPLE QUESTIONS',
        'nl' => 'VOORBEELDVRAGEN',
    ],
    'view_more' => [
        'en' => 'View more',
        'nl' => 'Lees meer',
    ],
    'view_less' => [
        'en' => 'View less',
        'nl' => 'Lees minder',
    ],
    'go_up' => [
        'en' => 'Go up',
        'nl' => 'Omhoog gaan',
    ],
    'test' => [
        'en' => 'Test',
        'nl' => 'Vragen',
    ],
    'question' => [
        'en' => 'Question',
        'nl' => 'Vragen',
    ],
    'give_feedback' => [
        'en' => 'Feedback',
        'nl' => 'Geef feedback',
    ],
    'no_feedback_available' => [
        'en' => 'Sorry, no feedback available for this question.',
        'nl' => 'Sorry, geen feedback beschikbaar voor deze vraag.',
    ],
    'extra_hint_text' => [
        'en' => '<p style=font-size:small><i>Please be aware:<br>The numbers in the intermediate steps have been rounded because of display reasons. However, the system is always calculating with the unrounded numbers unless otherwise indicated in the question.Example: 1,000 x € 1.10 = € 1,105.<br>This result, € 1,105 seems wrong! After all, you would expect € 1,100. However, in this example the system is calculating with € 1.105 instead of € 1.10. Therefore the result is correct, 1,000 x € 1.105 = € 1,105.<br>Do not round your intermediate calculations unless indicated in the question (!)</i></p>',
        'nl' => '<p style=font-size:small><i>Let op:<br>De getallen in de tussenstappen zijn afgerond om ze goed weer te kunnen geven. Op de achtergrond wordt echter altijd gerekend met onafgeronde getallen, tenzij dit in de opgave anders is aangegeven.Voorbeeld: 1.000 x €1,10 = €1.105.<br>Dit resultaat, €1.105 lijkt fout! Je zou immers €1.100 verwachten. Er wordt echter in dit voorbeeld op de achtergrond gerekend met €1,105 ipv €1,10. Het resultaat is dus juist, 1.000 x €1,105 = €1.105.<br>Rond dus niet af bij tussentijdse berekeningen tenzij aangegeven in de opgave (!)</i></p>',
    ],
    'no_question_found' => [
        'en' => 'Sorry, no question found.',
        'nl' => 'Sorry, geen vraag gevonden.',
    ],
    'reset_password' => [
        'en' => 'Reset Password',
        'nl' => 'Wachtwoord opnieuw instellen',
    ],
    'activity' => [
        'en' => 'Activity',
        'nl' => 'Activiteit',
    ],
    'login_date' => [
        'en' => 'Login date',
        'nl' => 'Aanmeldingsdatum',
    ],
    'login_time' => [
        'en' => 'Login time',
        'nl' => 'Aanmeldtijd',
    ],
    'export_theory' => [
        'en' => 'Export theory',
        'nl' => 'Export theorie',
    ],
    'profile_photo' => [
        'en' => 'Profile picture',
        'nl' => 'Profiel foto',
    ],
    'change_photo' => [
        'en' => 'Click to change photo',
        'nl' => 'Klik om de foto te veranderen',
    ],
    'similar_license_found' => [
        'en' => 'You cannot be the teacher and student of same course and same institution.',
        'nl' => 'Je kunt niet de leraar en student zijn van hetzelfde vak en dezelfde instelling.',
    ],
    'score' => [
        'en' => 'Score',
        'nl' => 'Score',
    ],
    'add_course' => [
        'en' => 'Add course',
        'nl' => 'Vak toevoegen',
    ],
    'add' => [
        'en' => 'Add',
        'nl' => 'Toevoegen',
    ],
    'code_text_1' => [
        'en' => 'Have you received a license code',
        'nl' => 'Heb je een licentiecode gekregen',
    ],
    'code_text_2' => [
        'en' => 'from your school? Then enter it here.',
        'nl' => 'van je school? Vul deze dan hier in.',
    ],
    'code_text_3' => [
        'en' => 'License Search',
        'nl' => 'Licentie Zoeken',
    ],
    'description' => [
        'en' => 'Description',
        'nl' => 'Omschrijving',
    ],
    'free' => [
        'en' => 'Free',
        'nl' => 'Gratis',
    ],
];

generateFile($dictionary, basename(__FILE__));
