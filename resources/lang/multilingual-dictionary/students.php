<?php

include_once 'generate-file.php';

$dictionary = [
    'dashboard_intro_welcome' => [
        'en' => 'Welcome!',
        'nl' => 'Welkom!',
    ],
    'dashboard_intro_message1' => [
        'en' => 'With BAES you can learn on your own level and pace.You decide when and where you study.',
        'nl' => 'Met BAES kun je leren op jouw niveau en eigen temp. Jij bepaalt wanneer en waar je studeert.',
    ],
    'dashboard_intro_message2' => [
        'en' => 'View this video to see how B Business Economics works.',
        'nl' => 'Bekijk dit filmpje om te kijken hoe BAES bedrijfseconomie werkt.',
    ],
    'dashboard_subject_heading' => [
        'en' => 'Go straight on with your last block or key',
        'nl' => 'Ga direct verder met je laatste blok of toets',
    ],
    'button_overview_learning_objective' => [
        'en' => 'Overview learning objectives',
        'nl' => 'Overzicht leerdoelen',
    ],
    'button_practice_test' => [
        'en' => 'Practice key available',
        'nl' => 'Overzicht leerdoelen',
    ],
    'button_skip' => [
        'en' => 'Skip to last learning objective',
        'nl' => 'Ga verder naar laatste leerdoel',
    ],
    'exercise_test_key' => [
        'en' => 'Overview of Practice Keys',
        'nl' => 'Overzicht oefentoetsen',
    ],
    'exercise_test' => [
        'en' => 'Exercise tests',
        'nl' => 'Oefentoetsen',
    ],
    'begin_button' => [
        'en' => 'Begin',
        'nl' => 'Beginnen',
    ],
    'continue_button' => [
        'en' => 'Continue',
        'nl' => 'Ga verder',
    ],
    'improve_button' => [
        'en' => 'Improve',
        'nl' => 'Verbeteren',
    ],
    'practice_test' => [
        'en' => 'Practice Test available!',
        'nl' => 'Oefentoets beschikbaar!',
    ],
    'take_test' => [
        'en' => 'Take this test',
        'nl' => 'Maak deze toets',
    ],
    'practice_test' => [
        'en' => 'Practice Test available!',
        'nl' => 'Oefentoets beschikbaar!',
    ],
    'take_test' => [
        'en' => 'Take this test',
        'nl' => 'Maak toets',
    ],
    'progress_block' => [
        'en' => 'Progress block',
        'nl' => 'Voortgang blok',
    ],
    'score_block' => [
        'en' => 'Score block',
        'nl' => 'Score blok',
    ],
    'last_5_question_score' => [
        'en' => 'Score the last 5 questions',
        'nl' => 'Score laatste 5 vragen',
    ],
    'baes_score' => [
        'en' => 'BAES Score',
        'nl' => 'BAES Score',
    ],
    'next_question' => [
        'en' => 'Next Question',
        'nl' => 'Volgende vraag',
    ],
    'error' => [
        'en' => 'Wrong',
        'nl' => 'Fout',
    ],
    'good' => [
        'en' => 'Good',
        'nl' => 'Goed',
    ],
    'try_again' => [
        'en' => 'Try again',
        'nl' => 'Probeer het nog een keer',
    ],
    'learning_goal_completed' => [
        'en' => 'Learning Goal is completed',
        'nl' => 'Het leerdoel is voltooid',
    ],
    'baes_score_improved' => [
        'en' => 'BAES score is improved',
        'nl' => 'BAES score is verbeterd',
    ],
    'attempts' => [
        'en' => 'Attempts',
        'nl' => 'Pogingen',
    ],
    'answer_button' => [
        'en' => 'Answer',
        'nl' => 'Antwoord geven',
    ],
    'try_again' => [
        'en' => 'Try it one more time',
        'nl' => 'Probeer het opnieuw',
    ],
    'help' => [
        'en' => 'Need help?',
        'nl' => 'Hulp nodig?',
    ],
    'shut_down' => [
        'en' => 'Shut down',
        'nl' => 'Afsluiten',
    ],
    'retry_button' => [
        'en' => 'Retry',
        'nl' => 'Opnieuw proberen',
    ],
    'omit_period_text' => [
        'en' => "You can't input ' . ' in the answer field. Please use a ' , ' as decimal separator.",
        'nl' => "Het is niet mogelijk een ' . ' in te voeren. Gebruik een ' , ' als decimaal teken. Duizendtallen worden automatisch verwerkt.",
    ],
    'invalid_attempt' => [
        'en' => 'Maximum attempt reached',
        'nl' => 'Maximale poging bereikt',
    ],
    'improve_score' => [
        'en' => 'Improve score',
        'nl' => 'Verbeter score',
    ],
    'learning_goal_completed' => [
        'en' => 'Learning Goal is completed',
        'nl' => 'Het leerdoel is voltooid',
    ],
    'baes_score_improved' => [
        'en' => 'BAES score is improved',
        'nl' => 'BAES score is verbeterd',
    ],
    'next_goal' => [
        'en' => 'Go to the next learning goal',
        'nl' => 'Volgende leerdoel',
    ],
    'feedback_sent' => [
        'en' => 'Feedback has been sent to the admin.',
        'nl' => 'Feedback is verzonden naar de beheerder.',
    ], 'baes_score_heading' => [
        'en' => 'The BAES score',
        'nl' => 'De BAES score',
    ],
    'baes_score_intro' => [
        'en' => 'In this program you get points for answering questions correctly. These points are translated into a B score. In the table below you can see the B scores linked to five different levels that you can achieve.',
        'nl' => 'In dit programma krijg je punten voor het juist beantwoorden van vragen. Deze punten worden vertaald in een BAES score. In onderstaande tabel zie je de BAES scores gekoppeld aan vijf verschillende niveaus die jij kunt behalen.',
    ],
    'level' => [
        'en' => 'Level',
        'nl' => 'Niveau',
    ],
    'baes_description_1' => [
        'en' => 'The higher your B score, the higher your level. For example if you have earned a B score of 3.2 then your level Junior. You can raise your level by questions in as less as possible attempts to answer well. The fewer attempts you need, the higher the number of points earned for the question.',
        'nl' => 'Hoe hoger je BAES score, hoe hoger jouw niveau. Bijvoorbeeld als je een BAES score hebt behaald van 3,2 dan is jouw niveau Junior. Je kunt jouw niveau verhogen door vragen in zo min mogelijk pogingen goed te beantwoorden. Hoe minder pogingen je nodig hebt, hoe hoger het aantal behaalde punten voor de betreffende vraag.',
    ],
    'baes_description_2' => [
        'en' => 'When the number of points scored for a question is higher than your current B score, then increases your B score. When you\'re in the above situation starting with B 3.2 score the next question right first time answered, you get 10 points for this question. This is higher than your B score from 3.2. This will allow your B score rise. On scores of 3.2 will write a B score only drop when you stay in one following question more than 3 attempts need.',
        'nl' => 'Wanneer het aantal behaalde punten voor een vraag hoger is dan jouw huidige BAES score, dan stijgt jouw BAES score. Wanneer je in de bovenstaande situatie startend met BAES score 3,2 de volgende vraag in één keer goed beantwoordt, krijg je voor deze vraag 10 punten. Dit is hoger dan jouw BAES score van 3,2. Hierdoor zal jouw BAES score stijgen. Bij een score van 3,2 zal jouw BAES score alleen dalen wanneer je voor een volgende vraag meer dan 3 pogingen nodig hebt.',
    ],
    'baes_description_3' => [
        'en' => 'You B score is calculated on the basis of the number of points on the last 5 questions.',
        'nl' => 'Je BAES score wordt berekend op basis van het behaalde aantal punten op de laatste 5 vragen.',
    ],

    'baes_description_4' => [
        'en' => 'You will be EindBAES by as many questions behind each other in 1 attempt to answer well! You achieved a B score of 10. However, this is not as easy as it looks; as your level increases, the questions are getting harder and harder.',
        'nl' => 'Je wordt EindBAES door zoveel mogelijk vragen achter elkaar in 1 poging goed te beantwoorden! Je behaald daarmee een BAES score van 10. Dit is echter niet zo makkelijk als het lijkt; naarmate jouw niveau stijgt, worden de vragen steeds moeilijker.',
    ],
    'baes_description_5' => [
        'en' => 'Achieve learning goal',
        'nl' => 'Leerdoel behalen',
    ],
    'baes_description_6' => [
        'en' => 'Learning objective to achieve you need to behalenOm a learning objective 2 conditions',
        'nl' => 'Om een leerdoel te behalen moet je aan 2 voorwaarden voldoen',
    ],
    'baes_description_7' => [
        'en' => 'You must be at least the level \' B \' have been earned if you to the question begins',
        'nl' => 'Je moet minimaal het niveau ‘BAES’ hebben behaald als je aan de vraag begint',
    ],
    'baes_description_8' => [
        'en' => 'and the question of the particular learning objective should be answered well in up to 2 attempts.',
        'nl' => 'én de vraag van het betreffende leerdoel moet in maximaal 2 pogingen goed beantwoord worden.',
    ],
    'baes_description_9' => [
        'en' => 'You know when a learning objective can be closed, because the graduation Hat comes into the picture under the question. When you have achieved the learning objective then you start the next learning objective (after answering a mandatory theory question) on the level with which you have closed the previous learning objective. This can mean that you with the good answering 2 questions, a new objective achieved.',
        'nl' => 'Je weet wanneer een leerdoel afgesloten kan worden, doordat het afstudeerhoedje in beeld komt onder de vraag. Wanneer je het leerdoel hebt behaald dan begin je het volgende leerdoel (na het beantwoorden van een verplichte theorievraag) op het niveau waarmee je het vorige leerdoel hebt afgesloten. Dit kan dus betekenen dat je met het goed beantwoorden van 2 vragen een nieuw leerdoel behaald.',
    ],
    'number_of_attempts' => [
        'en' => 'Number of retries',
        'nl' => 'Aantal pogingen',
    ],
    'points' => [
        'en' => 'Points',
        'nl' => 'Punten',
    ],
    'overview' => [
        'en' => 'overview',
        'nl' => 'overzicht',
    ],
    'school_exercise_test' => [
        'en' => 'School exercise test',
        'nl' => 'School oefentoetsen',
    ],
    'no_active_test_found' => [
        'en' => 'No active test found!',
        'nl' => 'Geen actieve toets gevonden!',
    ],
    'my_exercise_test' => [
        'en' => 'My exercise test',
        'nl' => 'Eigen oefentoetsen',
    ],
    'view_result' => [
        'en' => 'View result',
        'nl' => 'Bekijk resultaat',
    ],
    'make_test' => [
        'en' => 'Make the test',
        'nl' => 'Maak de toets',
    ],
    'begin_button' => [
        'en' => 'Begin',
        'nl' => 'Beginnen',
    ],
    'continue_button' => [
        'en' => 'Continue',
        'nl' => 'Ga verder',
    ],
    'improve_button' => [
        'en' => 'Improve',
        'nl' => 'Verbeteren',
    ],
    'time_left' => [
        'en' => 'Time left',
        'nl' => 'Resterende tijd',
    ],
    'result' => [
        'en' => 'Result',
        'nl' => 'Resultaat',
    ],
    'ansering_question' => [
        'en' => 'Answering questions',
        'nl' => 'Beantwoorde vragen',
    ],
    'submit_test' => [
        'en' => 'Submit test',
        'nl' => 'Toets inleveren',
    ],
    'closed_feedback' => [
        'en' => 'You have already completed this test, the results will be shown once the teacher shares the results.',
        'nl' => 'Je hebt deze toets reeds ingeleverd, de resultaten zijn pas te zien nadat de docent deze publiceert.',
    ],
    'unansweredQuestionMessage' => [
        'en' => 'You have not completed all the questions, are you sure you want to submit the test.',
        'nl' => 'Je hebt nog niet alle vragen beantwoord, weet je zeker dat je de toets wilt inleveren?',
    ],
    'exercise_test_starts_at' => [
        'en' => 'Exercise test starts at:',
        'nl' => 'De oefentoets is beschikbaar vanaf:',
    ],
    'student_answer' => [
        'en' => 'Your answer:',
        'nl' => 'Antwoord van de student:',
    ],
    'minutes' => [
        'en' => 'Minutes',
        'nl' => 'minuten',
    ],
    'attempt' => [
        'en' => 'Attempt',
        'nl' => 'Poging',
    ],
    'answer_error' => [
        'en' => 'Error',
        'nl' => 'Fout',
    ],
    'correct_answer' => [
        'en' => 'The correct answer is:',
        'nl' => 'Het juiste antwoord is:',
    ],
    'exercise_test_already_finished' => [
        'en' => 'Exercise test is already finished.',
        'nl' => 'Oefentest is al voltooid.',
    ],
    'subjects_overview' => [
        'en' => 'Subject overview',
        'nl' => 'Blok overzicht',
    ],
];

generateFile($dictionary, basename(__FILE__));
