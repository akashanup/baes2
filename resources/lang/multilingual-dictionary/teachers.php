<?php

include_once 'generate-file.php';

$dictionary = [
    'classes_overview' => [
        'en' => 'Classes overview',
        'nl' => 'Klassenoverzicht',
    ],
    'select_class' => [
        'en' => 'Select a class',
        'nl' => 'Selecteer een klas',
    ],
    'apply' => [
        'en' => 'Apply',
        'nl' => 'Pas toe',
    ],
    'managing_practice_test' => [
        'en' => 'Practice test management',
        'nl' => 'Oefentoets beheren',
    ],
    'Management_practice_test' => [
        'en' => 'Practice test',
        'nl' => 'Beheer oefentoetsen',
    ],
    'rounded_practice_keys' => [
        'en' => 'Finished practice keys',
        'nl' => 'Afgeronde oefentoetsen',
    ],
    'administrative_management' => [
        'en' => 'Administrative Management',
        'nl' => 'Administratief Beheer',
    ],
    'classes_management' => [
        'en' => 'Class management',
        'nl' => 'Klassenbeheer',
    ],
    'students_management' => [
        'en' => 'Student management',
        'nl' => 'Leerlingbeheer',
    ],
    'questions_management' => [
        'en' => 'Questions management',
        'nl' => 'Vragen Beheer',
    ],
    'view_questions' => [
        'en' => 'View questions',
        'nl' => 'Bekijk vragen',
    ],
    'teachers' => [
        'en' => 'Teachers',
        'nl' => 'Docenten',
    ],
    'view' => [
        'en' => 'View',
        'nl' => 'Bekijk',
    ],
    'questions' => [
        'en' => 'Questions',
        'nl' => 'vragen',
    ],
    'excercise_key' => [
        'en' => 'Practice test',
        'nl' => 'Oefentoets',
    ],
    'active_practice_keys' => [
        'en' => 'Active practice tests',
        'nl' => 'Actieve oefentoetsen',
    ],
    'class' => [
        'en' => 'Class',
        'nl' => 'Klas',
    ],
    'no_practice_test_available' => [
        'en' => 'No practice test available!',
        'nl' => 'Geen oefentoets beschikbaar!',
    ],
    'available_templates' => [
        'en' => 'Available Templates',
        'nl' => 'Beschikbare Templates',
    ],
    'create_new_key' => [
        'en' => 'Create new key template',
        'nl' => 'Maak nieuwe toets template',
    ],
    'student_management' => [
        'en' => 'Student Management',
        'nl' => 'Leerling Beheer',
    ],
    'question' => [
        'en' => 'Question',
        'nl' => 'Vraag',
    ],
    'block' => [
        'en' => 'Subject',
        'nl' => 'Blok',
    ],
    'random' => [
        'en' => 'Random',
        'nl' => 'Willekeurig',
    ],
    'practice_key_making' => [
        'en' => 'Make practice test',
        'nl' => 'Oefentoets maken',
    ],
    'name' => [
        'en' => 'Name',
        'nl' => 'Naam',
    ],
    'make_available' => [
        'en' => 'Make available',
        'nl' => 'Maak beschikbaar',
    ],
    'add_a_row' => [
        'en' => 'Add question',
        'nl' => 'Voeg vraag toe',
    ],
    'activate' => [
        'en' => 'Activate',
        'nl' => 'Activeren',
    ],
    'start' => [
        'en' => 'Start',
        'nl' => 'Start',
    ],
    'start_now' => [
        'en' => 'Start now',
        'nl' => 'Start nu',
    ],
    'end' => [
        'en' => 'End',
        'nl' => 'Einde',
    ],
    'time' => [
        'en' => 'Time',
        'nl' => 'Tijd',
    ],
    'feedback' => [
        'en' => 'Feedback',
        'nl' => 'Feedback',
    ],
    'end_of_test' => [
        'en' => 'End of test',
        'nl' => 'Einde van de toets',
    ],
    'good_or_bad' => [
        'en' => 'Only right or wrong',
        'nl' => 'Alleen goed of fout',
    ],
    'feedback_by_teacher' => [
        'en' => 'Feedback by teacher',
        'nl' => 'Feedback door docent',
    ],
    'existing_users' => [
        'en' => 'Existing Users',
        'nl' => 'Bestaande gebruikers',
    ],
    'all_users' => [
        'en' => 'All users',
        'nl' => 'Alle gebruikers',
    ],
    'save_and_active' => [
        'en' => 'Save and activate',
        'nl' => 'Opslaan en Activeren',
    ],
    'activate_key' => [
        'en' => 'Activate test',
        'nl' => 'Toets activeren',
    ],
    'description' => [
        'en' => 'Description',
        'nl' => 'Beschrijving',
    ],
    'create' => [
        'en' => 'Create',
        'nl' => 'creëren',
    ],
    'folder' => [
        'en' => 'Folder',
        'nl' => 'Map',
    ],
    'practice_folders' => [
        'en' => 'Practice test folders',
        'nl' => 'Oefentoets mappen',
    ],
    'test_name' => [
        'en' => 'Test name',
        'nl' => 'Toets naam',
    ],
    'date' => [
        'en' => 'Date',
        'nl' => 'Datum',
    ],
    'year' => [
        'en' => 'Year',
        'nl' => 'Jaar',
    ],
    'class' => [
        'en' => 'Class',
        'nl' => 'Klas',
    ],
    'students' => [
        'en' => 'Students',
        'nl' => 'Studenten',
    ],
    'score' => [
        'en' => 'Score',
        'nl' => 'Score',
    ],
    'year' => [
        'en' => 'Year',
        'nl' => 'Jaar',
    ],
    'no' => [
        'en' => 'No',
        'nl' => 'Geen',
    ],
    'rounded_practice_keys' => [
        'en' => 'Finished practice keys',
        'nl' => 'Afgeronde oefentoetsen',
    ],
    'all' => [
        'en' => 'All',
        'nl' => 'Alle',
    ],
    'student_environment' => [
        'en' => 'Student environment',
        'nl' => 'Studentomgeving',
    ],
    'filter_unique_questions' => [
        'en' => 'Filter unique questions',
        'nl' => 'Filter unieke vragen',
    ],
    'show_all_questions' => [
        'en' => 'Show all questions',
        'nl' => 'Laat alle vragen zien',
    ],
    'folder_created' => [
        'en' => 'New folder created.',
        'nl' => 'Nieuwe map gemaakt',
    ],
    'test_created' => [
        'en' => 'Practice test created.',
        'nl' => 'Oefentest gemaakt',
    ],
    'status_changed' => [
        'en' => 'Practice test status changed.',
        'nl' => 'Oefentoets status gewijzigd',
    ],
    'folder_changed' => [
        'en' => 'Practice test moved to different folder.',
        'nl' => 'Oefentoets verplaatst naar een andere map.',
    ],
    'delete_template_message' => [
        'en' => 'Are you sure you want to delete the template?',
        'nl' => 'Weet je zeker dat je de toets template wilt verwijderen?',
    ],
    'test_template_copied' => [
        'en' => 'Practice test template copied successfully.',
        'nl' => 'Oefentoets template gekopieerd.',
    ],
    'average_class' => [
        'en' => 'Average class',
        'nl' => 'Klasgemiddelde',
    ],
    'class_progress' => [
        'en' => 'Class Progress',
        'nl' => 'Klas voortgang',
    ],
    'overview' => [
        'en' => 'Overview',
        'nl' => 'Overzicht',
    ],
    'learner' => [
        'en' => 'Learner',
        'nl' => 'Leerling',
    ],
    'subclass' => [
        'en' => 'Subclass',
        'nl' => 'Subklas',
    ],
    'baes' => [
        'en' => 'BAES',
        'nl' => 'BAES',
    ],
    'score' => [
        'en' => 'Score',
        'nl' => 'Score',
    ],
    'status' => [
        'en' => 'Status',
        'nl' => 'Status',
    ],
    'selection' => [
        'en' => 'Selection',
        'nl' => 'Selectie',
    ],
    'manage_exercise_test' => [
        'en' => 'Manage practice tests',
        'nl' => 'Beheer oefentoetsen',
    ],
    'add_folder' => [
        'en' => 'Add folder',
        'nl' => 'Map toevoegen',
    ],
    'difficulty' => [
        'en' => 'Difficulty',
        'nl' => 'Moeilijkheid',
    ],
    'active_test' => [
        'en' => 'Active exercise test',
        'nl' => 'Actieve oefentoetsen',
    ],
    'availability' => [
        'en' => 'Availability',
        'nl' => 'Beschikbaarheid',
    ],
    'no_student_license_found' => [
        'en' => 'Sorry, at this time we couldn\'t find any student licence. Please contact BAES to fix this issue.',
        'nl' => 'Er is op dit moment helaas geen studentenlicentie gevonden. Neem a.u.b. contact op met BAES om dit probleem te verhelpen',
    ],
    'select' => [
        'en' => 'Select',
        'nl' => 'Kiezen',
    ],
    'year' => [
        'en' => 'Year',
        'nl' => 'Jaar',
    ],
    'other' => [
        'en' => 'Other',
        'nl' => 'anders',
    ],
    'sunday' => [
        'en' => 'Sunday',
        'nl' => 'Zondag',
    ],
    'monday' => [
        'en' => 'Monday',
        'nl' => 'Maandag',
    ],
    'tuesday' => [
        'en' => 'Tuesday',
        'nl' => 'Dinsdag',
    ],
    'thursday' => [
        'en' => 'Thursday',
        'nl' => 'Donderdag',
    ],
    'friday' => [
        'en' => 'Friday',
        'nl' => 'Vrijdag',
    ],
    'saturday' => [
        'en' => 'Saturday',
        'nl' => 'Zaterdag',
    ],
    'wednesday' => [
        'en' => 'Wednesday',
        'nl' => 'Woensdag',
    ],
    'activity' => [
        'en' => 'Activity',
        'nl' => 'Activiteit',
    ],
    't_block' => [
        'en' => 'Block',
        'nl' => 'blok',
    ],
    'progress' => [
        'en' => 'Progress',
        'nl' => 'Voortgang',
    ],
    'last_5_question_score' => [
        'en' => 'Last 5 questions score',
        'nl' => 'Score laatste 5 vragen',
    ],
    'export_questions' => [
        'en' => 'Export Questions',
        'nl' => 'Exporteer vragen',
    ],
    'do_not_update' => [
        'en' => 'Do not update',
        'nl' => 'Niet bijwerken',
    ],
    'switch_activity' => [
        'en' => 'Activity',
        'nl' => 'Activiteit',
    ],
    'switch_learningGoal' => [
        'en' => 'Learning goal',
        'nl' => 'Leerdoel',
    ],
    'students' => [
        'en' => 'Students',
        'nl' => 'Studenten',
    ],
    'delete_confirm_message' => [
        'en' => 'Are you sure you want to delete the class?',
        'nl' => 'Klas verwijderen?',
    ],
    'finished_with' => [
        'en' => 'finished with',
        'nl' => 'afgerond met een',
    ],
    'baes_score' => [
        'en' => 'BAES Score',
        'nl' => 'BAES Score',
    ],
    'answer_given_by_student' => [
        'en' => 'Answer given by student',
        'nl' => 'Antwoord gegeven door student',
    ],
    'class_deleted' => [
        'en' => 'Class deleted successfully.',
        'nl' => 'Klas is succesvol verwijderd.',
    ],
    'class_created' => [
        'en' => 'Class created successfully.',
        'nl' => 'Klas is succesvol gemaakt.',
    ],
    'q' => [
        'en' => 'Q',
        'nl' => 'V',
    ],
    'example_question' => [
        'en' => 'EXAMPLE QUESTIONS',
        'nl' => 'VOORBEELDVRAGEN',
    ],
    'student_answer' => [
        'en' => 'Student\'s answer',
        'nl' => 'Antwoord student',
    ],
    'avail_feedback' => [
        'en' => 'Make feedback available',
        'nl' => 'Feedback beschikbaar maken',
    ],
    'feedback_available' => [
        'en' => 'Feedback is available for this test now.',
        'nl' => 'Feedback is nu beschikbaar voor deze toets.',
    ],
    'create_class' => [
        'en' => 'Create class',
        'nl' => 'Maak klas aan',
    ],
    'student_record_updated' => [
        'en' => 'Student record updated.',
        'nl' => 'Student bijgewerkt',
    ],
    'other_control' => [
        'en' => 'Other management',
        'nl' => 'Overig beheer',
    ],
    'test_updated' => [
        'en' => 'Practice test updated.',
        'nl' => 'Oefentoets bijgewerkt',
    ],
    'class_updated' => [
        'en' => 'Class details updated.',
        'nl' => 'Klasgegevens bijgewerkt.',
    ],
    'unable_to_update' => [
        'en' => 'Unable to update class details.',
        'nl' => 'Klas details niet bijgewerkt',
    ],
    'attempts' => [
        'en' => 'Attempts',
        'nl' => 'Pogingen',
    ],
    'folder_updated' => [
        'en' => 'Folder details has been updated.',
        'nl' => 'Mapgegevens zijn bijgewerkt',
    ],
    'folder_deleted' => [
        'en' => 'Folder deleted successfully.',
        'nl' => 'Map is verwijderd',
    ],
    'unable_to_delete' => [
        'en' => 'Unable to delete folder',
        'nl' => 'Kan map niet verwijderen',
    ],
    'confirm_folder_delete' => [
        'en' => 'Are you sure you want to delete the folder?',
        'nl' => 'Weet je zeker dat je de map wilt verwijderen?',
    ],
];

generateFile($dictionary, basename(__FILE__));
