<?php

include_once 'generate-file.php';

$dictionary = [
    'privacy' => [
        'en' => 'Privacy',
        'nl' => 'Privacy',
    ],
    'disclaimer' => [
        'en' => 'Disclaimer',
        'nl' => 'Disclaimer',
    ],
    'contact' => [
        'en' => 'Contact',
        'nl' => 'Contact',
    ],
];

generateFile($dictionary, basename(__FILE__));
