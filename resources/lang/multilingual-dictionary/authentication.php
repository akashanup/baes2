<?php

include_once 'generate-file.php';

$dictionary = [
    'login' => [
        'en' => 'Login',
        'nl' => 'Inloggen',
    ],
    'register' => [
        'en' => 'Register',
        'nl' => 'Registreren',
    ],
    'email' => [
        'en' => 'E-mail',
        'nl' => 'E-mail',
    ],
    'password' => [
        'en' => 'Password.',
        'nl' => 'Wachtwoord',
    ],
    'cnf_password' => [
        'en' => 'Confirm Password.',
        'nl' => 'Bevestig wachtwoord',
    ],
    'forget_password' => [
        'en' => 'Forget your password',
        'nl' => 'Wachtwoord vergeten',
    ],
    'function' => [
        'en' => 'Function',
        'nl' => 'Functie',
    ],
    'pre_school' => [
        'en' => 'Pre-school',
        'nl' => 'Vooropleiding',
    ],
    'vwo' => [
        'en' => 'Vwo',
        'nl' => 'vwo',
    ],
    'havo' => [
        'en' => 'Havo',
        'nl' => 'havo',
    ],
    'mbo' => [
        'en' => 'Mbo',
        'nl' => 'mbo',
    ],
    'others' => [
        'en' => 'Other',
        'nl' => 'anders',
    ],
    'please_specify' => [
        'en' => 'Please specify',
        'nl' => 'Specificeren',
    ],
    'requiredField' => [
        'en' => 'Please fill out this field!',
        'nl' => 'Dit veld is verplicht.',
    ],
    'slogan' => [
        'en' => 'Everyone becomes business economics',
        'nl' => 'Word alle onderwerpen',
    ],
    'the_baes' => [
        'en' => 'the BAES',
        'nl' => 'de BAES',
    ],
    'reset_password' => [
        'en' => 'Reset Password',
        'nl' => 'Wachtwoord opnieuw instellen',
    ],
    'send_password_reset_link' => [
        'en' => 'Reset Password',
        'nl' => 'Wachtwoord herstellen',
    ],
    'set_password' => [
        'en' => 'Set a password',
        'nl' => 'Stel een wachtwoord in',
    ],
    'confirm_password' => [
        'en' => 'Confirm Password',
        'nl' => 'Wachtwoord bevestigen',
    ],
    'save_password' => [
        'en' => 'Save Password',
        'nl' => 'Wachtwoord opslaan',
    ],
    'password_reset_instructions' => [
        'en' => 'Enter your email address and you will receive a link within a few minutes to reset your password.',
        'nl' => 'Vul je e-mailadres in en je ontvangt binnen enkele minuten een link waarmee je jouw wachtwoord opnieuw kunt instellen.',
    ],
    'develop_yourself' => [
        'en' => 'Develop yourself',
        'nl' => 'Ontwikkel jezelf',
    ],
    'improve_yourself' => [
        'en' => 'Keep improving yourself',
        'nl' => 'Blijf jezelf verbeteren',
    ],
    'login_page_statement_1' => [
        'en' => 'Register your profile',
        'nl' => 'Registreer jouw profiel',
    ],
    'login_page_statement_2' => [
        'en' => 'Get access to unique learning',
        'nl' => 'Krijg toegang tot uniek leren',
    ],
    'login_page_statement_3' => [
        'en' => 'Choose your curriculum and start',
        'nl' => 'Kies jouw lesprogramma en start',
    ],
    'login_page_statement_4' => [
        'en' => 'Choose new courses',
        'nl' => 'Kies nieuwe vakken',
    ],
    'login_page_statement_5' => [
        'en' => 'Improve your level',
        'nl' => 'Verbeter jouw niveau',
    ],
    'login_page_statement_6' => [
        'en' => 'Increase your value',
        'nl' => 'Verhoog jouw waarde',
    ],
    'inactive_account' => [
        'en' => 'Your account is inactive!',
        'nl' => 'Uw account is niet actief!',
    ],
    'signup' => [
        'en' => 'Sign up',
        'nl' => 'Aanmelden',
    ],
];

generateFile($dictionary, basename(__FILE__));
