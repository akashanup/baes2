<?php

function generateFile($dictionary, $name)
{
    $file = ['en' => '', 'nl' => ''];
    foreach ($dictionary as $key => $a) {
        $file['en'] .= '"' . $key . '" => "' . $a['en'] . '",' . "\n";
        $file['nl'] .= '"' . $key . '" => "' . $a['nl'] . '",' . "\n";
    }

    foreach ($file as $key => $locale) {
        $myfile = fopen("resources/lang/$key/$name", 'w+') or die('Unable to open file!');
        $txt = "<?php\n\n";
        fwrite($myfile, $txt);
        $txt = "return [\n";
        fwrite($myfile, $txt);
        $txt = $locale;
        fwrite($myfile, $txt);
        $txt = '];';
        fwrite($myfile, $txt);
        fclose($myfile);
    }
}
