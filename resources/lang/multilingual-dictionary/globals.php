<?php

include_once 'generate-file.php';

$dictionary = [
    'request_error_message' => [
        'en' => 'Something went wrong. Please try again.',
        'nl' => 'Er is iets fout gegaan. Probeer het opnieuw.',
    ],
    'validation_error' => [
        'en' => 'The given data was invalid.',
        'nl' => 'De ingevulde gegevens zijn ongeldig.',
    ],
    'unauthorized_text' => [
        'en' => 'You are not authorized to perform this task.',
        'nl' => 'U bent niet gemachtigd om deze taak uit te voeren.',
    ],
    'email_has_been_sent' => [
        'en' => 'An email has been sent.',
        'nl' => 'Er is een e-mail verzonden.',
    ],
    'unauthenticated_text' => [
        'en' => 'Unauthenticated. Please login to continue.',
        'nl' => 'U bent niet ingelogd. Meld u aan om verder te gaan.',
    ],
    'validation_failed' => [
        'en' => 'Validation failed for saving ',
        'nl' => 'Validatie mislukt voor opslaan ',
    ],
    'logo' => [
        'en' => 'Logo',
        'nl' => 'Logo',
    ],
    'action' => [
        'en' => 'Action',
        'nl' => 'Actie',
    ],
    'type' => [
        'en' => 'Type',
        'nl' => 'Type',
    ],
    'save' => [
        'en' => 'Save',
        'nl' => 'Opslaan',
    ],
    'image_upload_failed' => [
        'en' => 'Image failed to upload',
        'nl' => 'Afbeelding kan niet worden geüpload',
    ],
    'duplicate_slug_found' => [
        'en' => 'Duplicate slug found.',
        'nl' => 'Dubbele slug gevonden.',
    ],
    'delete' => [
        'en' => 'Delete',
        'nl' => 'Verwijder',
    ],
    'order' => [
        'en' => 'Order',
        'nl' => 'Volgorde',
    ],
    'edit' => [
        'en' => 'Edit',
        'nl' => 'Bewerk',
    ],
    'copy' => [
        'en' => 'Copy',
        'nl' => 'Kopiëren',
    ],
    'close' => [
        'en' => 'Close',
        'nl' => 'Sluiten',
    ],
    'active' => [
        'en' => 'Active',
        'nl' => 'Actief',
    ],
    'inactive' => [
        'en' => 'Inactive',
        'nl' => 'Inactief',
    ],
    'for' => [
        'en' => 'For',
        'nl' => 'Voor',
    ],
    'license_validity' => [
        'en' => 'License duration',
        'nl' => 'Licentieduur',
    ],
    'days' => [
        'en' => 'days',
        'nl' => 'dagen',
    ],
    'price' => [
        'en' => 'Price',
        'nl' => 'Prijs',
    ],
    'end_date' => [
        'en' => 'End date',
        'nl' => 'Einddatum',
    ],
    'validity' => [
        'en' => 'Validity',
        'nl' => 'Geldigheid',
    ],
    'role' => [
        'en' => 'Role',
        'nl' => 'Rol',
    ],
    'institution' => [
        'en' => 'Institution',
        'nl' => 'Instellingen',
    ],
    'course' => [
        'en' => 'Course',
        'nl' => 'Vak',
    ],
    'image_not_found' => [
        'en' => 'Image not found',
        'nl' => 'Afbeelding niet gevonden',
    ],
    'image' => [
        'en' => 'Image',
        'nl' => 'Afbeelding',
    ],
    'school' => [
        'en' => 'School',
        'nl' => 'School',
    ],
    'organisation' => [
        'en' => 'Organisation',
        'nl' => 'Organisatie',
    ],
    'search' => [
        'en' => 'Search',
        'nl' => 'Zoek',
    ],
    'licenses' => [
        'en' => 'Licenses',
        'nl' => 'Licentie',
    ],
    'view' => [
        'en' => 'View',
        'nl' => 'Bekijk',
    ],
    'year' => [
        'en' => 'Year',
        'nl' => 'Jaar',
    ],
    'per_month' => [
        'en' => 'Per month',
        'nl' => 'Per maand',
    ],
    'manage' => [
        'en' => 'Manage',
        'nl' => 'Beheer',
    ],
    'fixed' => [
        'en' => 'Fixed',
        'nl' => 'Eenmalig',
    ],
    'monthly' => [
        'en' => 'Monthly',
        'nl' => 'Maandelijks',
    ],
    'end_baes' => [
        'en' => 'End BAES',
        'nl' => 'Eind BAES',
    ],
    'videos' => [
        'en' => 'Videos',
        'nl' => 'Videos',
    ],
    'status' => [
        'en' => 'Status',
        'nl' => 'Status',
    ],
    'send' => [
        'en' => 'Send',
        'nl' => 'Verzenden',
    ],
    'level' => [
        'en' => 'Level',
        'nl' => 'Niveau',
    ],
    'since' => [
        'en' => 'Date',
        'nl' => 'Sinds',
    ],
    'edit_profile' => [
        'en' => 'Edit Profile',
        'nl' => 'Bewerk profiel',
    ],
    'start' => [
        'en' => 'Start',
        'nl' => 'Begin',
    ],
    'end' => [
        'en' => 'End',
        'nl' => 'Einde',
    ],
    'introduction' => [
        'en' => 'Introduction',
        'nl' => 'Introductie',
    ],
    'privacy1' => [
        'en' => 'I agree with ',
        'nl' => 'Ik ga akkoord met het ',
    ],
    'privacy2' => [
        'en' => 'Privacy Statement ',
        'nl' => 'Privacy Statement ',
    ],
    'privacy3' => [
        'en' => 'of BAES Education',
        'nl' => 'van BAES Education',
    ],
    'ie_warning1' => [
        'en' => 'Dear user,',
        'nl' => 'Beste gebruiker,',
    ],
    'ie_warning2' => [
        'en' => 'At the moment you use Internet Explorer or Edge to visit our website. The BAES application is optimized for, among other things, Google Chrome.',
        'nl' => 'Op dit moment gebruik jij Internet Explorer of Edge om onze website te bezoeken. De BAES applicatie is geoptimaliseerd voor onder andere Google Chrome.',
    ],
    'ie_warning3' => [
        'en' => 'We are working on changes to also support Internet Explorer and Edge 100% but for correct use the use of Google Chrome is currently required.',
        'nl' => 'Wij werken aan wijzigingen om ook Internet Explorer en Edge 100% te kunnen ondersteunen maar voor correct gebruik is op dit moment het gebruik van Google Chrome noodzakelijk.',
    ],
    'ie_warning4' => [
        'en' => 'Apologies for the discomfort.',
        'nl' => 'Excuses voor het ongemak.',
    ],
    'ie_warning5' => [
        'en' => 'BAES',
        'nl' => 'BAES',
    ],
    'already_have_account' => [
        'en' => 'I already have an account',
        'nl' => 'Ik heb al een account',
    ],
];

generateFile($dictionary, basename(__FILE__));
