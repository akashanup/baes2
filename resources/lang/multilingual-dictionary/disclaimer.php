<?php

$languages = [


    'line_1'            => [
                                'en' => 'On the use of this website (www.BAES Education and www.BAES Academy) are
                                         Conditions of use below. By using this
                                         Web sites, you are deemed to have taken note of the terms of Use and
                                         To have accepted them.',
                                'nl' => 'Op het gebruik van deze website (www.baes.education en www.baes.academy) zijn
                                         onderstaande gebruiksvoorwaarden van toepassing. Door gebruik te maken van deze
                                         websites, wordt u geacht kennis te hebben genomen van de gebruiksvoorwaarden en
                                         deze te hebben aanvaard.',
                           ],

    'point_1'           => [
                                'en' => 'Use of information',
                                'nl' => 'Gebruik van informatie',
                            ],

    'point_1_1_c'         => [
                                'en' => 'BAES Education Vof strives to offer this website always accurate and up-to-date information and knowledge. Although this information has been compiled with the utmost care, BAES Education Vof does not allow for the completeness, accuracy or topicality of the information. On the basis of the defence of feedback, we will endeavour to provide the information and knowledge to the highest possible quality.',
                                'nl' => 'BAES Education Vof streeft ernaar op deze website altijd juiste en actuele informatie en kennis aan te bieden. Hoewel deze informatie met de grootst mogelijke zorgvuldigheid is samengesteld, staat BAES Education Vof niet in voor de volledigheid, juistheid of actualiteit van de informatie. Op basis van het verwereken van feedback zullen wij trachten om de informatie en kennis op zo’n hoge mogelijke kwaliteit aan te kunnen bieden.',
                             ],

    'point_1_2_c'         => [
                                'en' => 'No rights can be derived from the information and knowledge provided. BAES Education will accepts no liability for damages arising from the use of the information or the website nor for the malfunction of the website.',
                                'nl' => 'Aan de verstrekte informatie en kennis kunnen geen rechten worden ontleend. BAES Education Vof aanvaardt geen aansprakelijkheid voor schade die voortvloeit uit het gebruik van de informatie of de website en evenmin voor het niet goed functioneren van de website.',
                             ],

    'point_1_3_c'         => [
                                'en' => 'Based on the sending and receiving of information via the website or via e-mail, a relationship between the BAES Education Vof and the user of the website can not be automatically created.',
                                'nl' => 'Op basis van het verzenden en ontvangen van informatie via de website of via e-mail kan niet zonder meer een relatie tussen BAES Education Vof en de gebruiker van de website ontstaan.',
                             ],

    'point_2'            => [
                                'en' => 'E-mail and communication',
                                'nl' => 'E-mail en communicatie',
                            ],

    'point_2_c'          => [
                                'en' => 'BAES Education Vof does not warrant that e-mails sent to her (timely) are received or processed, because timely receipt of e-mails cannot always be guaranteed. We do our best to respond as quickly as possible. The security of the e-mail traffic cannot be fully guaranteed by the associated safety risks. By emailing with BAES Education Vof without encryption or password protection, you accept this risk.',
                                'nl' => 'BAES Education Vof garandeert niet dat aan haar gezonden e-mails (tijdig) worden ontvangen of verwerkt, omdat tijdige ontvangst van e-mails niet altijd kan worden gegarandeerd. We doen ons best om zo snel mogelijk te reageren. Ook de veiligheid van het e-mailverkeer kan niet volledig worden gegarandeerd door de hieraan verbonden veiligheidsrisico’s. Door zonder encryptie of wachtwoordbeveiliging per e-mail met BAES Education Vof te corresponderen, accepteert u dit risico.',
                            ],

    'point_3'            => [
                                'en' => 'Hyperlinks',
                                'nl' => 'Hyperlinks',
                            ],

    'point_3_c'         => [
                                'en' => 'This website may contain hyperlinks to third-party websites.BAES Education Vof does not affect third party websites and is not responsible for the availability or content thereof. BAES Education Vof therefore accepts no liability for damages resulting from the use of third party websites.',
                                'nl' => 'Deze website kan hyperlinks bevatten naar websites van derden. BAES Education Vof heeft geen invloed op websites van derden en is niet verantwoordelijk voor de beschikbaarheid of inhoud daarvan. BAES Education Vof aanvaardt dan ook geen aansprakelijkheid voor schade die voortvloeit uit het gebruik van websites van derden.',
                            ],

    'point_4'            => [
                                'en' => 'Intellectual property rights',
                                'nl' => 'Intellectuele eigendomsrechten',
                            ],
    'point_4_c'           => [
                                'en' => 'The questions and the theory of the BAES Education Vof are protected by copyright and other intellectual property rights. Except for personal and non-commercial use in the course of your study, nothing from these publications and expressions may be reproduced, copied or otherwise disclosed in any way whatsoever, without the prior written consent of the BAES Education Vof. We make our information available via third parties as much as possible, for example via Youtube. This may be used openly and may be referred to.',
                                'nl' => 'De vragen en de theorie van BAES Education Vof zijn beschermd door auteursrecht en andere intellectuele eigendomsrechten. Behalve voor persoonlijk en niet commercieel gebruik in het kader van jouw studie, mag niets uit deze publicaties en uitingen op welke manier dan ook verveelvoudigd, gekopieerd of op een andere manier openbaar worden gemaakt, zonder dat BAES Education Vof daar vooraf schriftelijke toestemming voor heeft gegeven. Wij stellen onze informatie zo veel mogelijk beschikbaar via derden, bijvoorbeeld via Youtube. Hiervan mag openlijk gebruik gemaakt worden en mag naar worden verwezen. ',
                            ],



];

$en = '';
$nl = '';
foreach ($languages as $key => $a) {
    $en .= '"'.$key.'" => "'.$a["en"].'",'."\n";
    $nl .= '"'.$key.'" => "'.$a["nl"].'",'."\n";
}

$myfile = fopen('resources/lang/en/disclaimer.php', 'w+') or die('Unable to open file!');
$txt    = "<?php\n\n";
fwrite($myfile, $txt);
$txt = "return [\n";
fwrite($myfile, $txt);
$txt = $en;
fwrite($myfile, $txt);
$txt = '];';
fwrite($myfile, $txt);
fclose($myfile);

$myfile = fopen('resources/lang/nl/disclaimer.php', 'w+') or die('Unable to open file!');
$txt    = "<?php\n\n";
fwrite($myfile, $txt);
$txt = "return [\n";
fwrite($myfile, $txt);
$txt = $nl;
fwrite($myfile, $txt);
$txt = '];';
fwrite($myfile, $txt);
fclose($myfile);
