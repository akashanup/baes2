<?php

include_once 'generate-file.php';

$dictionary = [
    'welcome_to_baes' => [
        'en' => 'Welcome to BAES!',
        'nl' => 'Welkom bij BAES!',
    ],
    'hello' => [
        'en' => 'Hello',
        'nl' => 'Hallo',
    ],
    'activation_message' => [
        'en' => 'The registration at BAES has been successful, please click on the link below to activate your account.',
        'nl' => 'De registratie bij BAES is gelukt, klik op de onderstaande link om jouw account te activeren.',
    ],
    'activate_account_button' => [
        'en' => 'Activate account',
        'nl' => 'Account activeren',
    ],
    'one_time_link' => [
        'en' => 'Note: The above button can only be used once.',
        'nl' => 'Let op: de bovenstaande knop kan maar één keer worden gebruikt.',
    ],
    'sincerely' => [
        'en' => 'Sincerely',
        'nl' => 'Met vriendelijke groet,',
    ],
    'payment_message' => [
        'en' => 'Please click on the button below to make payment.',
        'nl' => 'Klik op de onderstaande knop om een betaling te doen.',
    ],
    'payment_button' => [
        'en' => 'Make payment',
        'nl' => 'Betalen',
    ],
    'payment_received' => [
        'en' => 'Payment received.',
        'nl' => 'Betaling ontvangen.',
    ],
    'payment_details' => [
        'en' => 'Payment details',
        'nl' => 'Betalings details',
    ],
    'payment_id' => [
        'en' => 'Payment ID',
        'nl' => 'Betalings ID',
    ],
    'course' => [
        'en' => 'Course',
        'nl' => 'Vak',
    ],
    'amount' => [
        'en' => 'Amount',
        'nl' => 'Bedrag',
    ],
    'date' => [
        'en' => 'Date',
        'nl' => 'Datum',
    ],
    'change_password_request' => [
        'en' => 'You have requested a change of your password. You can reset your password using the link below.',
        'nl' => 'Je hebt een wijziging van jouw wachtwoord aangevraagd. Je kunt je wachtwoord opnieuw instellen met de onderstaande link.',
    ],
    'reset_password_button' => [
        'en' => 'Reset Password',
        'nl' => 'Wachtwoord herstellen',
    ],
    'password_unaffected' => [
        'en' => 'If you have not reset your password, please do not use the link.Your current password will then remain in effect.',
        'nl' => 'Heb je jouw wachtwoord niet opnieuw aangevraagd, gebruik de link dan niet. Je huidige wachtwoord blijft dan gewoon van kracht.',
    ],
    'send_password_reset_link' => [
        'en' => 'Send Password Reset Link',
        'nl' => 'Stuur wachtwoord reset Link',
    ],
    'success_payment' => [
        'en' => 'Thank you for your payment to BAES!',
        'nl' => 'Hartelijk dank voor je betaling aan BAES!',
    ],
    'failed_payment' => [
        'en' => 'Your payment at BAES has failed!',
        'nl' => 'Je betaling bij BAES is mislukt!',
    ],

    'subcopy_text_1' => [
        'en' => 'If you’re having trouble clicking the',
        'nl' => 'Als het niet lukt om de',
    ],
    'subcopy_text_2' => [
        'en' => 'button, copy and paste the URL below into your web browser :',
        'nl' => 'knop te gebruiken, kopieer en plak dan de volgende URL naar je web browser :',
    ],
    'feedback_mail_subject' => [
        'en' => 'BAES - Thank you for your feedback!',
        'nl' => 'BAES - Bedankt voor jouw feedback!',
    ],
    'feedback_mail_dear' => [
        'en' => 'Dear',
        'nl' => 'Beste',
    ],
    'feedback_mail_learning_goal' => [
        'en' => 'Thank you for your feedback on the learning goal:',
        'nl' => 'Bedankt voor jouw feedback op het leerdoel:',
    ],
    'feedback_mail_no_learning_goal' => [
        'en' => 'Thank you for your feedback.',
        'nl' => 'Bedankt voor jouw feedback.',
    ],
    'feedback_mail_body' => [
        'en' => 'We are very happy with this, and continue to give feedback. Together we can create an even better learning experience!',
        'nl' => 'Hier zijn wij ontzettend blij mee, blijf vooral doorgaan met het geven van feedback. Samen maken we van BAES een nog betere onderwijsmodule!',
    ],
    'feedback_mail_processed_body' => [
        'en' => 'Your feedback was indeed correct. We therefore processed your feedback directly.',
        'nl' => 'Jouw feedback was inderdaad juist. Wij hebben jouw feedback daarom direct verwerkt.',
    ],
    'feedback_mail_incorrect_body' => [
        'en' => 'We looked your feedback however the BAES module turns out to be correct. That is why your feedback has not been processed. We will clarify the question to prevent confusion in the future.',
        'nl' => 'Wij hebben ernaar gekeken maar de gegevens in de BAES module blijken toch juist te zijn. Daarom is jouw feedback niet verwerkt. We zullen de vraag verduidelijken.',
    ],
    'feedback_mail_rounding_body' => [
        'en' => 'Your feedback relates to the rounding of variables in the question. Note: we never round numbers on interim calculations and outcomes. Because we do not want to have very long numbers in the module with many digits behind the comma, we often only show a maximum of 2 figures behind the comma. In the background, however, we calculate with unrounded numbers. We understand that this is sometimes confusing, but now you can easily take this into account when doing calculations.',
        'nl' => 'Jouw feedback heeft betrekking op afronding van de variabelen in de vraag. Let op: de BAES module rond tussentijdse berekeningen nooit af. Omdat wij niet willen dat er hele lange getallen in de module staan met veel cijfers achter de comma, tonen wij vaak maar maximaal 2 cijfers achter de comma. Op de achtergrond wordt er echter dan wel met niet afgeronde getallen gerekend. Wij begrijpen dat dit soms verwarrend is, maar als je het weet kun je er gemakkelijk rekening mee houden.',
    ],
    'feedback_mail_your_feedback' => [
        'en' => 'Your feedback:',
        'nl' => 'Jouw feedback:',
    ],
    'thankyou_payment_message' => [
        'en' => 'Thank you for your payment to BAES! You can now use the BAES module',
        'nl' => 'Hartelijk dank voor je betaling aan BAES! Je kunt vanaf nu gebruik maken van de BAES-module',
    ],
    'go_to_module' => [
        'en' => 'Go directly to the module',
        'nl' => 'Ga direct naar de module',
    ],
];

generateFile($dictionary, basename(__FILE__));
