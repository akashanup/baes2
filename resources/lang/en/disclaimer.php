<?php

return [
"line_1" => "On the use of this website (www.BAES Education and www.BAES Academy) are
                                         Conditions of use below. By using this
                                         Web sites, you are deemed to have taken note of the terms of Use and
                                         To have accepted them.",
"point_1" => "Use of information",
"point_1_1_c" => "BAES Education Vof strives to offer this website always accurate and up-to-date information and knowledge. Although this information has been compiled with the utmost care, BAES Education Vof does not allow for the completeness, accuracy or topicality of the information. On the basis of the defence of feedback, we will endeavour to provide the information and knowledge to the highest possible quality.",
"point_1_2_c" => "No rights can be derived from the information and knowledge provided. BAES Education will accepts no liability for damages arising from the use of the information or the website nor for the malfunction of the website.",
"point_1_3_c" => "Based on the sending and receiving of information via the website or via e-mail, a relationship between the BAES Education Vof and the user of the website can not be automatically created.",
"point_2" => "E-mail and communication",
"point_2_c" => "BAES Education Vof does not warrant that e-mails sent to her (timely) are received or processed, because timely receipt of e-mails cannot always be guaranteed. We do our best to respond as quickly as possible. The security of the e-mail traffic cannot be fully guaranteed by the associated safety risks. By emailing with BAES Education Vof without encryption or password protection, you accept this risk.",
"point_3" => "Hyperlinks",
"point_3_c" => "This website may contain hyperlinks to third-party websites.BAES Education Vof does not affect third party websites and is not responsible for the availability or content thereof. BAES Education Vof therefore accepts no liability for damages resulting from the use of third party websites.",
"point_4" => "Intellectual property rights",
"point_4_c" => "The questions and the theory of the BAES Education Vof are protected by copyright and other intellectual property rights. Except for personal and non-commercial use in the course of your study, nothing from these publications and expressions may be reproduced, copied or otherwise disclosed in any way whatsoever, without the prior written consent of the BAES Education Vof. We make our information available via third parties as much as possible, for example via Youtube. This may be used openly and may be referred to.",
];