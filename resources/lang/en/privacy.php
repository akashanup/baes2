<?php

return [
"line_1" => "Privacy Policy for Baes Education, owner of www.baes.education and www.baes.academy.",
"point_1" => "Guarantee Privacy",
"point_1_c" => "Ensuring the privacy of visitors to the Baes Education and the Baes Academy is
                                         An important task for us. Therefore we describe in our privacy policy which
                                         Information we collect and how we use this information. We Share your
                                         Study data-on an individual level-not with anyone other than your teacher (s) in
                                         The framework of optimizing your chances of impact and improving the
                                         Guidance from the teacher to the student. Baes 's Education will be able to analyse
                                         Based on anonymous data.",
"point_2" => "Permission",
"point_2_c" => "By using the information and services on the Baes Education and the Baes Academy,
                                         Do you agree with our Privacy policy and the terms and conditions that we have
                                         Included.",
"point_3" => "Questions",
"point_3_c" => "If you would like to receive more information, or have questions about the privacy policy of B
                                         Education and specifically www. Baes Education and www. Baes Academy, you can contact us
                                         Access via email. Our email address is info @ Baes. Education.",
"point_4" => "Monitor Behavior Visitor",
"point_4_c" => "www.baesEducation and www.BaesAcademy uses different
                                         Techniques to keep track of who visits the website, how this visitor
                                         Website and which pages are visited. That's a common way
                                         of working for websites because it provides information that contributes to the
                                         Quality of the user experience. The information we register, via cookies,
                                         Includes IP addresses, browser type, and visited pages.
                                         We also monitor where visitors visit the website for the first time and from
                                         Which page they are leaving. We keep this information anonymous and is not
                                         Associated with other personal information.",
"point_5" => "Use of cookies",
"point_5_c" => "Baes Education and Baes Academy place cookies with visitors. We do this to
                                         To collect information about the pages users visit on our website,
                                         To keep track of how often visitors come back and to see which pages it
                                         Do well on the website. We also keep track of what information the browser shares. We
                                         Use cookies so only for ourselves, to the experience of the Baes 's products
                                         To improve, not to share with other parties to take advantage of this.",
"point_6" => "Disable Cookies",
"point_6_c" => "You can choose to disable cookies. You do this by using the
                                         capabilities of your browser. You can find more information about these possibilities on the
                                         Website of your browser's provider.",
];