<?php

return [
"welcome_to_baes" => "Welcome to BAES!",
"hello" => "Hello",
"activation_message" => "The registration at BAES has been successful, please click on the link below to activate your account.",
"activate_account_button" => "Activate account",
"one_time_link" => "Note: The above button can only be used once.",
"sincerely" => "Sincerely",
"payment_message" => "Please click on the button below to make payment.",
"payment_button" => "Make payment",
"payment_received" => "Payment received.",
"payment_details" => "Payment details",
"payment_id" => "Payment ID",
"course" => "Course",
"amount" => "Amount",
"date" => "Date",
"change_password_request" => "You have requested a change of your password. You can reset your password using the link below.",
"reset_password_button" => "Reset Password",
"password_unaffected" => "If you have not reset your password, please do not use the link.Your current password will then remain in effect.",
"send_password_reset_link" => "Send Password Reset Link",
"success_payment" => "Thank you for your payment to BAES!",
"failed_payment" => "Your payment at BAES has failed!",
"subcopy_text_1" => "If you’re having trouble clicking the",
"subcopy_text_2" => "button, copy and paste the URL below into your web browser :",
"feedback_mail_subject" => "BAES - Thank you for your feedback!",
"feedback_mail_dear" => "Dear",
"feedback_mail_learning_goal" => "Thank you for your feedback on the learning goal:",
"feedback_mail_no_learning_goal" => "Thank you for your feedback.",
"feedback_mail_body" => "We are very happy with this, and continue to give feedback. Together we can create an even better learning experience!",
"feedback_mail_processed_body" => "Your feedback was indeed correct. We therefore processed your feedback directly.",
"feedback_mail_incorrect_body" => "We looked your feedback however the BAES module turns out to be correct. That is why your feedback has not been processed. We will clarify the question to prevent confusion in the future.",
"feedback_mail_rounding_body" => "Your feedback relates to the rounding of variables in the question. Note: we never round numbers on interim calculations and outcomes. Because we do not want to have very long numbers in the module with many digits behind the comma, we often only show a maximum of 2 figures behind the comma. In the background, however, we calculate with unrounded numbers. We understand that this is sometimes confusing, but now you can easily take this into account when doing calculations.",
"feedback_mail_your_feedback" => "Your feedback:",
"thankyou_payment_message" => "Thank you for your payment to BAES! You can now use the BAES module",
"go_to_module" => "Go directly to the module",
];