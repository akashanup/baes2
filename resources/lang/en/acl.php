<?php

return [
"role_created" => "A new role is created.",
"permission_created" => "A new permission is created.",
"user_created" => "A new user is created.",
"role_updated" => "Role updated.",
"permission_updated" => "Permission updated.",
"user_updated" => "User updated.",
"role_deleted" => "Role deleted.",
"permission_deleted" => "Permission deleted.",
"user_deleted" => "User deleted.",
"role_permission_updated" => "Permissions updated for the role.",
"user_permission_updated" => "Permissions updated for the user.",
];