<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_licenses', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('license_id')->unsigned();
            //grade_id #1 is when grade is unknown
            $table->integer('grade_id')->unsigned()->nullable();
            $table->integer('sub_grade_id')->unsigned()->nullable();
            // If validity is null then it is always available.
            $table->date('end_date')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('blocked_by_teacher', ['yes', 'no'])->default('no');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('license_id')->references('id')->on('licenses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_grade_id')->references('id')->on('grades')
                ->onDelete('set null')->onUpdate('cascade');
            $table->primary(['user_id', 'license_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_licenses', function (Blueprint $table) {
            $table->dropForeign('user_licenses_user_id_foreign');
            $table->dropForeign('user_licenses_license_id_foreign');
            $table->dropForeign('user_licenses_grade_id_foreign');
            $table->dropForeign('user_licenses_sub_grade_id_foreign');
            $table->dropPrimary('PRIMARY');
            $table->dropColumn('user_id');
            $table->dropColumn('license_id');
            $table->dropColumn('grade_id');
            $table->dropColumn('sub_grade_id');
        });

        Schema::dropIfExists('user_licenses');
    }
}
