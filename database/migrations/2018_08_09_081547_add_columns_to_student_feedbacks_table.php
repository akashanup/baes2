<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStudentFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_feedbacks', function (Blueprint $table) {
            $table->dropForeign('student_feedbacks_learning_goal_test_id_foreign');
            $table->dropColumn('learning_goal_test_id');
        });
        Schema::table('student_feedbacks', function (Blueprint $table) {
            $table->integer('license_id')->unsigned()->nullable();
            $table->foreign('license_id')->references('id')
                ->on('licenses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_feedbacks', function (Blueprint $table) {
            $table->integer('learning_goal_test_id')->unsigned()->nullable();
            $table->foreign('learning_goal_test_id')->references('id')
                ->on('learning_goal_tests')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('student_feedbacks', function (Blueprint $table) {
            $table->dropForeign('student_feedbacks_license_id_foreign');
            $table->dropColumn('license_id');
        });
    }
}
