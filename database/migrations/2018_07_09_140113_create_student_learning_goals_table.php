<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentLearningGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_learning_goals', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('license_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('learning_goal_id')->unsigned();
            $table->mediumInteger('order')->default(0);
            $table->decimal('finished_score', 3, 1)->default(0.0);
            $table->decimal('current_score', 3, 1)->default(0.0);
            $table->decimal('change', 2, 1)->default(0.0);
            $table->integer('learning_goal_test_id')->unsigned()->nullable();
            $table->enum('status', ['unavailable', 'available', 'started', 'finished'])
                ->default('available');
            $table->timestamps();
            $table->foreign('learning_goal_id')->references('id')->on('learning_goals')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('license_id')->references('id')->on('licenses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('learning_goal_test_id')->references('id')->on('learning_goal_tests')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['user_id', 'license_id', 'subject_id', 'learning_goal_id'],
                'student_learning_goals_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_learning_goals', function (Blueprint $table) {
            $table->dropForeign('student_learning_goals_subject_id_foreign');
            $table->dropForeign('student_learning_goals_learning_goal_id_foreign');
            $table->dropForeign('student_learning_goals_license_id_foreign');
            $table->dropForeign('student_learning_goals_user_id_foreign');
            $table->dropForeign('student_learning_goals_learning_goal_test_id_foreign');
            $table->dropPrimary('PRIMARY');
            $table->dropColumn('subject_id');
            $table->dropColumn('license_id');
            $table->dropColumn('user_id');
            $table->dropColumn('learning_goal_id');
        });
        Schema::dropIfExists('student_learning_goals');
    }
}
