<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mollie_subscription_id', 100)->unique();
            $table->string('mollie_customer_unique_id', 100)->unique();
            $table->string('mandata_id', 100)->nullable();
            $table->unsignedInteger('user_id');
            $table->string('susbcription_period');
            $table->date('subscription_start_date');
            $table->date('subscription_payment_date');
            $table->enum('status', ['active', 'inactive', 'pending'])->default('inactive');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('subscriptions_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('subscriptions');
    }
}
