<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_subjects', function (Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->mediumInteger('order')->default(0);
            $table->timestamps();
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['course_id', 'subject_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_subjects', function (Blueprint $table) {
            $table->dropForeign('course_subjects_course_id_foreign');
            $table->dropForeign('course_subjects_subject_id_foreign');
            $table->dropPrimary('PRIMARY');
            $table->dropColumn('course_id');
            $table->dropColumn('subject_id');
        });
        Schema::dropIfExists('course_subjects');
    }
}
