<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyColumnsInStudentPracticeTestQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_practice_test_questions', function (Blueprint $table) {
            $table->dropForeign('stu_prac_test_ques_prac_test_ques_id_foreign');
            $table->dropColumn('practice_test_question_id');
            $table->integer('practice_test_detail_id')->unsigned();
            $table->integer('question_answer_id')->unsigned();
            $table->foreign('practice_test_detail_id', 'stu_prac_test_que_prac_test_detail_id_foreign')
                ->references('id')->on('practice_test_details')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('question_answer_id', 'stu_prac_test_que_que_ans_id_foreign')
                ->references('id')->on('question_answers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_practice_test_questions', function (Blueprint $table) {
            $table->dropForeign('stu_prac_test_que_prac_test_detail_id_foreign');
            $table->dropColumn('practice_test_detail_id');
            $table->dropForeign('stu_prac_test_que_que_ans_id_foreign');
            $table->dropColumn('question_answer_id');
            $table->integer('practice_test_question_id')->unsigned()->nullable();
            $table->foreign('practice_test_question_id', 'stu_prac_test_ques_prac_test_ques_id_foreign')
                ->references('id')->on('practice_test_questions')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
