<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 25)->nullable()->index();
            $table->integer('course_id')->unsigned();
            $table->integer('institution_id')->unsigned();
            // If validity is null then it is always available.
            $table->integer('validity')->nullable();
            $table->integer('price')->default(0);
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('type', ['monthly', 'fixed'])->default('fixed');
            $table->timestamps();
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('institution_id')->references('id')->on('institutions')
                ->onDelete('cascade')->onUpdate('cascade');
            /*
            CourseId and InstituteId won't be unique as we can have more then one
            license with same course_id and institution_id but different code or price or validity.
             */
            //$table->unique(['course_id', 'institution_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('licenses', function (Blueprint $table) {
            $table->dropForeign('licenses_course_id_foreign');
            $table->dropColumn('course_id');
            $table->dropForeign('licenses_institution_id_foreign');
            $table->dropColumn('institution_id');
        });
        Schema::dropIfExists('licenses');
    }
}
