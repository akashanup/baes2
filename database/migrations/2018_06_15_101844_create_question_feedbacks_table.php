<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->text('template');
            $table->unsignedTinyInteger('step');
            $table->unsignedInteger('question_template_id');
            $table->foreign('question_template_id')->references('id')->on('question_templates')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_feedbacks');
    }
}
