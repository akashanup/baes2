<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('template');
            $table->string('language',2);
            $table->unsignedTinyInteger('complexity');
            $table->enum('type',['mcq','direct']);
            $table->unsignedInteger('learningGoal_id');
            $table->enum('status',['active','inactive'])->deafult('active');
            $table->unsignedInteger('order')->deafult(0);
            $table->foreign('learningGoal_id')->references('id')->on('learning_goals')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_templates');
    }
}
