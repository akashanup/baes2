<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeInTemplateVariables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_variables', function (Blueprint $table) {
            DB::statement('ALTER TABLE template_variables CHANGE correct_answer 
                correct_answer text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE template_variables CHANGE correct_answer 
                correct_answer varchar(255)');
    }
}
