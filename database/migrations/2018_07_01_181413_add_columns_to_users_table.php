<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('previous_education', 50)->nullable();
            $table->string('dob', 10)->nullable();
            $table->string('activation_token', 30)->nullable();
            $table->char('year', 4)->default(Carbon::now()->year);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('previous_education');
            $table->dropColumn('dob');
            $table->dropColumn('year');
            $table->dropColumn('activation_token');
        });
    }
}
