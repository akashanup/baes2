<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLearningGoalTestQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learning_goal_test_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('learning_goal_test_id')->unsigned();
            $table->integer('question_answer_id')->unsigned();
            $table->json('answer')->nullable();
            $table->smallInteger('points')->default(0);
            $table->enum('score_calculated', ['yes', 'no'])->default('yes');
            $table->timestamps();
            $table->foreign('learning_goal_test_id', 'lgtqa_lgti_foreign')->references('id')
                ->on('learning_goal_tests')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('question_answer_id', 'lgtqa_qai_foreign')->references('id')
                ->on('question_answers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('learning_goal_test_question_answers', function (Blueprint $table) {
            $table->dropForeign('lgtqa_lgti_foreign');
            $table->dropForeign('lgtqa_qai_foreign');
            $table->dropColumn('learning_goal_test_id');
            $table->dropColumn('question_answer_id');
        });
        Schema::dropIfExists('learning_goal_test_question_answers');
    }
}
