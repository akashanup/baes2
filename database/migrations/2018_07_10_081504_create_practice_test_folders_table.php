<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticeTestFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_test_folders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('slug', 60)->unique();
            $table->text('description');
            $table->integer('user_id')->unsigned();
            $table->integer('license_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('license_id')->references('id')->on('licenses')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practice_test_folders', function (Blueprint $table) {
            $table->dropForeign('practice_test_folders_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('practice_test_folders_license_id_foreign');
            $table->dropColumn('license_id');
        }); 
        Schema::dropIfExists('practice_test_folders');
    }
}
