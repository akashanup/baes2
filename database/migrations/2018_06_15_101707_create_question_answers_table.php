<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question');                
            $table->text('option')->nullable();
            $table->string('answer');
            $table->enum('status',['active','inactive'])->default('active');
            $table->integer('question_template_id')->unsigned();
            $table->integer('template_variable_id')->unsigned();

            $table->foreign('question_template_id')->references('id')->on('question_templates')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('template_variable_id')->references('id')->on('template_variables')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_answers');
    }
}
