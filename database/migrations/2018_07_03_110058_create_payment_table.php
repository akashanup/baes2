<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mollie_payment_unique_id', 100)->nullable()->unique();
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->enum('status', ['open', 'pending', 'success', 'failed'])
                ->default('open');
            $table->enum('payment_sequence_type', ['oneoff', 'first', 'recurring'])
                ->default('oneoff');
            $table->timestamps();
            $table->foreign('subscription_id')->references('id')->on('subscriptions')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payments_subscription_id_foreign');
            $table->dropForeign('payments_user_id_foreign');
            $table->dropColumn('subscription_id');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('payments');
    }
}
