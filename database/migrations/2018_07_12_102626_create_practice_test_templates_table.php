<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePracticeTestTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_test_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('number_of_question');
            $table->json('template');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->integer('folder_id')->unsigned();
            $table->foreign('folder_id')->references('id')->on('practice_test_folders')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->integer('course_id')->unsigned();
            $table->integer('institution_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('institution_id')->references('id')->on('institutions')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practice_test_templates', function (Blueprint $table) {
            $table->dropForeign('practice_test_templates_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('practice_test_templates_folder_id_foreign');
            $table->dropColumn('folder_id');
            $table->dropForeign('practice_test_templates_course_id_foreign');
            $table->dropColumn('course_id');
            $table->dropForeign('practice_test_templates_institution_id_foreign');
            $table->dropColumn('institution_id');
        });
        Schema::dropIfExists('practice_test_templates');
    }
}
