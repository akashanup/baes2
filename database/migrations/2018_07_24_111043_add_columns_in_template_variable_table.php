<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInTemplateVariableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_variables', function (Blueprint $table) {
            $table->string('upper_bound')->nullable();
            $table->string('lower_bound')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_variables', function (Blueprint $table) {
            $table->dropColumn('upper_bound');
            $table->dropColumn('lower_bound');
        });
    }
}
