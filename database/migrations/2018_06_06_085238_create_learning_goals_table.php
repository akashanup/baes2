<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLearningGoalsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('learning_goals', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->binary('theory');
			$table->binary('example');
			$table->json('video');
			$table->string('slug', 120)->unique();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('learning_goals');
	}
}
