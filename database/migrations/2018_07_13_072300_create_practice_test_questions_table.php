<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticeTestQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_test_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_answer_id')->unsigned();
            $table->integer('practice_test_detail_id')->unsigned();
            $table->tinyInteger('random_key')->nullable();
            $table->timestamps();
            $table->foreign('question_answer_id')->references('id')->on('question_answers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('practice_test_detail_id')->references('id')->on('practice_test_details')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practice_test_questions', function (Blueprint $table) {
            $table->dropForeign('practice_test_questions_question_answer_id_foreign');
            $table->dropColumn('question_answer_id');
            $table->dropForeign('practice_test_questions_practice_test_detail_id_foreign');
            $table->dropColumn('practice_test_detail_id');
        });
        Schema::dropIfExists('practice_test_questions');
    }
}
