<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyColumnsOfPracticeTestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('practice_test_details', function (Blueprint $table) {
            $table->dropForeign('practice_test_details_folder_id_foreign');
            $table->dropColumn('folder_id');
            $table->dropColumn('availability');
            $table->dropColumn('status');
            $table->dropColumn('feedback_type');
            $table->dropColumn('end_Date');
            $table->dropColumn('time');
        });
        Schema::table('practice_test_details', function (Blueprint $table) {
            $table->dateTime('end_date')->nullable();
            $table->TinyInteger('time')->unsigned()->nullable();
            $table->enum('availability', ['all_users', 'existing_users'])->default('existing_users');
            $table->enum('feedback_type', ['end_of_test', 'partly', 'closed'])->default('end_of_test');
            $table->enum('status', ['started', 'finished'])->default('started');
            $table->integer('folder_id')->unsigned()->nullable();
            $table->foreign('folder_id')->references('id')->on('practice_test_folders')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practice_test_details', function (Blueprint $table) {
            $table->dropForeign('practice_test_details_folder_id_foreign');
            $table->dropColumn('folder_id');
            $table->dropColumn('availability');
            $table->dropColumn('status');
            $table->dropColumn('feedback_type');
            $table->dropColumn('end_Date');
            $table->dropColumn('time');
        });
        Schema::table('practice_test_details', function (Blueprint $table) {
            $table->dateTime('end_date')->nullable();
            $table->TinyInteger('time')->unsigned()->nullable();
            $table->enum('availability', ['all_users', 'existing_users']);
            $table->enum('feedback_type', ['end_of_test', 'partly', 'closed']);
            $table->enum('status', ['started', 'finished']);
            $table->integer('folder_id')->unsigned()->nullable();
            $table->foreign('folder_id')->references('id')->on('practice_test_folders')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
