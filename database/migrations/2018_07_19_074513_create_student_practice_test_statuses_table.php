<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentPracticeTestStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_practice_test_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('practice_test_detail_id')->unsigned();
            $table->enum('status', ['available', 'started', 'finished'])
                ->default('available');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('practice_test_detail_id', 'stu_prac_test_stat_prac_test_detail_id_foreign')
                ->references('id')->on('practice_test_details')->onDelete('cascade')->onUpdate('cascade');
            $table->decimal('score', 4, 1)->default(0.0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('student_practice_test_statuses', function (Blueprint $table) {
            $table->dropForeign('student_practice_test_statuses_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('stu_prac_test_stat_prac_test_detail_id_foreign');
            $table->dropColumn('practice_test_detail_id');
            $table->dropColumn('score');
        });
        Schema::dropIfExists('student_practice_test_statuses');
    }
}
