<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentPracticeTestQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_practice_test_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('practice_test_question_id')->unsigned();
            $table->string('answer')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('practice_test_question_id', 'stu_prac_test_ques_prac_test_ques_id_foreign')
                ->references('id')->on('practice_test_questions')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('student_practice_test_questions', function (Blueprint $table) {
            $table->dropForeign('student_practice_test_questions_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('stu_prac_test_ques_prac_test_ques_id_foreign');
            $table->dropColumn('practice_test_question_id');
        });
        Schema::dropIfExists('student_practice_test_questions');
    }
}
