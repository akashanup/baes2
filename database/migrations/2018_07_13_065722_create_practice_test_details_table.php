<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticeTestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_test_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('grade_id')->unsigned();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->TinyInteger('time')->unsigned();
            $table->enum('feedback_type',['end_of_test','partly','closed']);
            $table->enum('status',['started','finished']);
            $table->integer('avg_score')->unsigned()->default(0);
            $table->TinyInteger('student_attempted')->unsigned()->default(0);
            $table->string('name');
            $table->string('slug')->unique();
            $table->json('template');
            $table->integer('user_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->integer('institution_id')->unsigned();
            $table->integer('folder_id')->unsigned();
            $table->integer('gmt')->nullable(); 
            $table->enum('availability',['all_users','existing_users']);
            $table->foreign('grade_id')->references('id')
            ->on('grades')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('institution_id')->references('id')->on('institutions')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('folder_id')->references('id')->on('practice_test_folders')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')->onUpdate('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practice_test_details', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('template');
            $table->dropForeign('practice_test_details_grade_id_foreign');
            $table->dropForeign('practice_test_details_user_id_foreign');
            $table->dropColumn('grade_id');
            $table->dropColumn('user_id');
            $table->dropColumn('slug');
            $table->dropForeign('practice_test_details_course_id_foreign');
            $table->dropColumn('course_id');
            $table->dropForeign('practice_test_details_institution_id_foreign');
            $table->dropColumn('institution_id');
            $table->dropForeign('practice_test_details_folder_id_foreign');
            $table->dropColumn('folder_id');
        });
        Schema::dropIfExists('practice_test_details');
    }
}
