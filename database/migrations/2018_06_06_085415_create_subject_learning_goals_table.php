<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectLearningGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_learning_goals', function (Blueprint $table) {
            $table->integer('subject_id')->unsigned();
            $table->integer('learning_goal_id')->unsigned();
            $table->mediumInteger('order')->default(0);
            $table->foreign('subject_id')->references('id')->on('subjects')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('learning_goal_id')->references('id')
                ->on('learning_goals')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['subject_id', 'learning_goal_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subject_learning_goals', function (Blueprint $table) {
            $table->dropForeign('subject_learning_goals_subject_id_foreign');
            $table->dropForeign('subject_learning_goals_learning_goal_id_foreign');
            $table->dropPrimary('PRIMARY');
            $table->dropColumn('subject_id');
            $table->dropColumn('learning_goal_id');
        });

        Schema::dropIfExists('subject_learning_goals');
    }
}
