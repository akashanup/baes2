<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_variables', function (Blueprint $table) {
            $table->increments('id');
            $table->json('values');
            $table->string('correct_answer');
            $table->enum('status',['finished','pending'])->default('pending');
            $table->unsignedInteger('question_template_id');
            $table->enum('status_edit', ['yes', 'no'])->default('no');
            $table->foreign('question_template_id')->references('id')->on('question_templates')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_variables');
    }
}
