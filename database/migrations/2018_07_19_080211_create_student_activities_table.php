<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('license_id')->unsigned()->nullable();
            $table->integer('subject_id')->unsigned()->nullable();
            $table->integer('learning_goal_id')->unsigned()->nullable();
            $table->integer('points')->default(0);
            $table->string('date')->default(Carbon::now()->toDateString());
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('license_id')->references('id')->on('licenses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('learning_goal_id')->references('id')
                ->on('learning_goals')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_activities', function (Blueprint $table) {
            $table->dropForeign('student_activities_user_id_foreign');
            $table->dropForeign('student_activities_license_id_foreign');
            $table->dropForeign('student_activities_subject_id_foreign');
            $table->dropForeign('student_activities_learning_goal_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('license_id');
            $table->dropColumn('subject_id');
            $table->dropColumn('learning_goal_id');
        });
        Schema::dropIfExists('student_activities');
    }
}
