<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('slug', 120)->unique();
            $table->enum('type', ['subject', 'curriculum'])->default('subject');
            $table->integer('curriculum_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('curriculum_id')->references('id')
                ->on('subjects')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->dropForeign('subjects_curriculum_id_foreign');
            $table->dropColumn('curriculum_id');
        });
        Schema::dropIfExists('subjects');
    }
}
