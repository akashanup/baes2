<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->enum('status', ['processed', 'unprocessed'])
                ->default('unprocessed');
            $table->text('note')->nullable();
            $table->enum('response_email_type', ['processed', 'rounding',
                'incorrect', 'custom', 'none'])->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('question_answer_id')->unsigned()->nullable();
            $table->integer('learning_goal_test_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('question_answer_id')->references('id')
                ->on('question_answers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('learning_goal_test_id')->references('id')
                ->on('learning_goal_tests')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_feedbacks', function (Blueprint $table) {
            $table->dropForeign('student_feedbacks_user_id_foreign');
            $table->dropForeign('student_feedbacks_question_answer_id_foreign');
            $table->dropForeign('student_feedbacks_learning_goal_test_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('question_answer_id');
            $table->dropColumn('learning_goal_test_id');
        });
        Schema::dropIfExists('student_feedbacks');
    }
}
