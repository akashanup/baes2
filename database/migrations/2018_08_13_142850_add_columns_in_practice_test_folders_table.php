<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInPracticeTestFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('practice_test_folders', function (Blueprint $table) {
            $table->integer('institution_id')->unsigned()->nullable();
            $table->integer('course_id')->unsigned()->nullable();
            $table->foreign('institution_id')->references('id')->on('institutions')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('course_id')->references('id')->on('courses')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practice_test_folders', function (Blueprint $table) {
            $table->dropForeign('practice_test_folders_institution_id_foreign');
            $table->dropColumn('institution_id');
            $table->dropForeign('practice_test_folders_course_id_foreign');
            $table->dropColumn('course_id');
        });
    }
}
