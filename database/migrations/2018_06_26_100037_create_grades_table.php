<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('slug', 60)->unique();
            $table->char('year', 4)->default(Carbon::now()->year);
            // There can be sub grades also.
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('institution_id')->unsigned();
            $table->foreign('parent_id')->references('id')->on('grades')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('institution_id')->references('id')
                ->on('institutions')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->dropForeign('grades_institution_id_foreign');
            $table->dropForeign('grades_parent_id_foreign');
            $table->dropColumn('institution_id');
            $table->dropColumn('parent_id');
        });
        Schema::dropIfExists('grades');
    }
}
