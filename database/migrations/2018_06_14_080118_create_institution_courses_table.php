<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_courses', function (Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('institution_id')->unsigned();
            $table->timestamps();
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('institution_id')->references('id')->on('institutions')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['course_id', 'institution_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('institution_courses', function (Blueprint $table) {
            $table->dropForeign('institution_courses_course_id_foreign');
            $table->dropForeign('institution_courses_institution_id_foreign');
            $table->dropPrimary('PRIMARY');
            $table->dropColumn('course_id');
            $table->dropColumn('institution_id');
        });
        Schema::dropIfExists('institution_courses');
    }
}
