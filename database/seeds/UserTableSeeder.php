<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt('akashanup');
        User::insert([
            [
                'first_name' => 'SuperAdmin', 'last_name' => 'Account',
                'phone' => '+00 00 00 00 00 0', 'password' => $password,
                'email' => 'akasha@mindfiresolutions.com', 'slug' => 'super-admin',
                'role_id' => Role::where('name', config('constants.SUPER_ADMIN'))->first()->id,
                'status' => config('constants.ACTIVE'),
            ],
            [
                'first_name' => 'Admin', 'last_name' => 'Account',
                'phone' => '+00 00 00 00 00 0', 'password' => $password,
                'email' => 'akashanup.1993@hotmail.com', 'slug' => 'admin',
                'role_id' => Role::where('name', config('constants.ADMIN'))->first()->id,
                'status' => config('constants.ACTIVE'),
            ],
            [
                'first_name' => 'SubAdmin', 'last_name' => 'Account',
                'phone' => '+00 00 00 00 00 0', 'password' => $password,
                'email' => 'akashanup.1993@gmail.com', 'slug' => 'sub-admin',
                'role_id' => Role::where('name', config('constants.SUB_ADMIN'))->first()->id,
                'status' => config('constants.ACTIVE'),
            ],
        ]);
    }
}
