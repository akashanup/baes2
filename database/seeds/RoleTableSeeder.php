<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            ['name' => config('constants.SUPER_ADMIN')],
            ['name' => config('constants.ADMIN')],
            ['name' => config('constants.SUB_ADMIN')],
            ['name' => config('constants.USER')],
        ]);
    }
}
