<?php

use App\Models\LearningGoal;
use Faker\Factory;
use Illuminate\Database\Seeder;

class LearningGoalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            LearningGoal::create(['name' => $faker->word,
                'theory' => $faker->paragraph, 'example' => $faker->paragraph,
                'video' => json_encode([['name' => $faker->sentence,
                    'url' => $faker->url], ['name' => $faker->sentence,
                    'url' => $faker->url]]
                ),
            ]);
        }
    }
}
