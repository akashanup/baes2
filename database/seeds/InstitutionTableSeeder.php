<?php

use App\Models\Institution;
use Faker\Factory;
use Illuminate\Database\Seeder;

class InstitutionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Institution::create([
            'name' => config('constants.BAES_INSTITUTION'),
            'type' => 'school',
            'slug' => config('constants.BAES_INSTITUTION_SLUG')]);
    }
}
