<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdminPermissions = [
            ['name' => 'roles.index'], ['name' => 'roles.indexData'],
            ['name' => 'roles.create'], ['name' => 'roles.store'],
            ['name' => 'roles.edit'], ['name' => 'roles.update'],
            ['name' => 'roles.destroy'],

            ['name' => 'permissions.index'],
            ['name' => 'permissions.indexData'],
            ['name' => 'permissions.create'],
            ['name' => 'permissions.store'], ['name' => 'permissions.edit'],
            ['name' => 'permissions.update'], ['name' => 'permissions.destroy'],

            ['name' => 'roles.permissions.index'],
            ['name' => 'roles.permissions.update'],

            ['name' => 'acl.users.index'],
            ['name' => 'acl.users.indexData'], ['name' => 'acl.users.create'],
            ['name' => 'acl.users.store'], ['name' => 'acl.users.edit'],
            ['name' => 'acl.users.update'], ['name' => 'acl.users.destroy'],
        ];

        $adminPermissions = [
            ['name' => 'admins.dashboard'],
            ['name' => 'admins.uploadImage'], ['name' => 'admins.teacherLogin'],
            ['name' => 'admins.downloadQuestionVariableSet'],
            ['name' => 'admins.uploadQuestionVariableSet'],
            ['name' => 'admins.previewDirectTemplate'],
            ['name' => 'admins.updateVariableSet'],
            ['name' => 'admins.editSelectedVariableSet'],
            ['name' => 'admins.save-template'],
            ['name' => 'admins.create-template'],
            ['name' => 'admins.exportTable'],
            ['name' => 'admins.exportData'],

            ['name' => 'institutions.index'],
            ['name' => 'institutions.indexData'],
            ['name' => 'institutions.create'], ['name' => 'institutions.store'],
            ['name' => 'institutions.update'], ['name' => 'institutions.destroy'],

            ['name' => 'courses.index'], ['name' => 'courses.copy'],
            ['name' => 'courses.indexData'], ['name' => 'courses.create'],
            ['name' => 'courses.store'], ['name' => 'courses.edit'],
            ['name' => 'courses.update'], ['name' => 'courses.destroy'],
            ['name' => 'courses.institutions'],

            ['name' => 'licenses.index'], ['name' => 'licenses.indexData'],
            ['name' => 'licenses.store'], ['name' => 'licenses.destroy'],
            ['name' => 'licenses.edit'], ['name' => 'licenses.update'],

            ['name' => 'learning-goals.index'],
            ['name' => 'learning-goals.indexData'],
            ['name' => 'learning-goals.create'],
            ['name' => 'learning-goals.store'],
            ['name' => 'learning-goals.edit'],
            ['name' => 'learning-goals.update'],
            ['name' => 'learning-goals.destroy'],
            ['name' => 'learning-goals.curriculum.index'],
            ['name' => 'learning-goals.curriculumData'],
            ['name' => 'learning-goals.theoryPreview'],
            ['name' => 'learning-goals.uploadTheoryPdf'],

            ['name' => 'subjects.index'], ['name' => 'subjects.indexData'],
            ['name' => 'subjects.create'], ['name' => 'subjects.store'],
            ['name' => 'subjects.edit'], ['name' => 'subjects.update'],
            ['name' => 'subjects.destroy'],

            ['name' => 'template.upload'], ['name' => 'template.edit'],
            ['name' => 'template.uploadContent'],
            ['name' => 'template.mcqTemplateList'],
            ['name' => 'template.downloadQuestionTemplate'],
            ['name' => 'template.downloadVariableSet'],
            ['name' => 'template.getTemplateList'],
            ['name' => 'template.editVariableSet'],
            ['name' => 'template.direct-store'],

            ['name' => 'question-template.downloadPdf'],
            ['name' => 'question-template.update'],

            ['name' => 'template.curriculumTemplate'],
            ['name' => 'template.editQuestionTemplate'],

            ['name' => 'variable-set.showVariableSet'],
            ['name' => 'variable-set.upload'],

            ['name' => 'users.index'], ['name' => 'users.indexData'],
            ['name' => 'users.changeStatus'], ['name' => 'users.destroy'],
            ['name' => 'users.resetPassword'], ['name' => 'users.show.licenses'],
            ['name' => 'users.changeLicense'],

            ['name' => 'questionAnswers.show'],
        ];

        $subAdminPermissions = [
            ['name' => 'sub-admins.dashboard'],

            ['name' => 'curriculums.index'], ['name' => 'curriculums.indexData'],
            ['name' => 'curriculums.create'], ['name' => 'curriculums.store'],
            ['name' => 'curriculums.edit'], ['name' => 'curriculums.update'],
            ['name' => 'curriculums.destroy'],
            ['name' => 'curriculums.learning-goals'],

            ['name' => 'studentFeedbacks.index'],
            ['name' => 'studentFeedbacks.indexData'],
            ['name' => 'studentFeedbacks.storeNote'],
            ['name' => 'studentFeedbacks.updateStatus'],
        ];

        $userPermission = [
            ['name' => 'users.licenses'], ['name' => 'users.availableLicenses'],
            ['name' => 'users.courses'], ['name' => 'users.licenses.show'],
            ['name' => 'users.edit'], ['name' => 'users.update'],

            ['name' => 'licenses.show'],
            ['name' => 'license.cancelUserLicense'],

            ['name' => 'payments.makePayment'],
            ['name' => 'payments.webhook'],
            ['name' => 'payments.index'],
            ['name' => 'payments.paymentHistory'],

            ['name' => 'teachers.manageTest'],
            ['name' => 'teachers.manage-practice-test-template'],
            ['name' => 'teachers.subjectlearningGoal'],
            ['name' => 'teachers.savepracticeTestTemplate'],
            ['name' => 'teachers.listPracticeTestTemplate'],
            ['name' => 'teachers.createFolder'],
            ['name' => 'teachers.saveTestDetails'],
            ['name' => 'teachers.listActivePracticeTest'],
            ['name' => 'teachers.changeStatus'],
            ['name' => 'teachers.deleteTestTemplate'],
            ['name' => 'teachers.copyTestTemplate'],
            ['name' => 'teachers.finishedPracticeTest'],
            ['name' => 'teachers.finishedPracticeTestTable'],
            ['name' => 'teachers.editPracticeTestTemplate'],
            ['name' => 'teachers.updateTestTemplateFolder'],
            ['name' => 'teachers.learningGoalQuestions'],
            ['name' => 'teachers.questionsData'],
            ['name' => 'teachers.viewQuestion'],
            ['name' => 'teachers.student-management'],
            ['name' => 'teachers.studentLogin'],
            ['name' => 'teachers.classManagement'],
            ['name' => 'teachers.saveClass'],
            ['name' => 'teachers.saveSubClass'],
            ['name' => 'teachers.gradesByYear'],
            ['name' => 'teachers.updateStudents'],
            ['name' => 'teachers.deleteClass'],
            ['name' => 'teachers.defaultRoute'],
            ['name' => 'teachers.studentsList'],
            ['name' => 'teachers.exportQuestionPdf'],
            ['name' => 'teachers.updateClass'],
            ['name' => 'teachers.classList'],
            ['name' => 'teachers.updateFolder'],
            ['name' => 'teachers.deleteFolder'],

            ['name' => 'grades.students'],
            ['name' => 'grades.overview'],
            ['name' => 'grades.activityOverview'],
            ['name' => 'grades.attemptsOverview'],

            ['name' => 'users.folder-practice-test'],

            ['name' => 'students.subjects.learning-goals'],

            ['name' => 'students.learning-goals.start'],
            ['name' => 'students.learning-goals.show'],
            ['name' => 'students.learning-goals.introductionTest'],
            ['name' => 'students.learning-goals.test'],
            ['name' => 'students.learning-goals.learningGoalTestQuestion'],
            ['name' => 'students.checkQuestionAnswer'],
            ['name' => 'students.exportTheory'],

            ['name' => 'studentFeedbacks.store'],

            ['name' => 'students.practiceTests.index'],
            ['name' => 'students.practiceTests.test'],
            ['name' => 'students.practiceTests.result'],
            ['name' => 'students.practiceTests.storeAnswer'],
            ['name' => 'students.practiceTests.submit'],
            ['name' => 'students.practiceTests.updateStatus'],
            ['name' => 'students.practiceTests.create'],
            ['name' => 'students.practiceTests.store'],

            ['name' => 'teachers.practiceTests.overview'],
            ['name' => 'teachers.practiceTests.feedbackAvailable'],
        ];

        //Drop all permissions
        DB::statement("SET foreign_key_checks=0");
        DB::table('role_permissions')->truncate();
        Permission::truncate();
        DB::statement("SET foreign_key_checks=1");

        foreach ($superAdminPermissions as $superAdminPermission) {
            Permission::create($superAdminPermission);
        }

        $adminRole = Role::where('name', config('constants.ADMIN'))->first();
        foreach ($adminPermissions as $permission) {
            $newPermission = Permission::create($permission);
            $adminRole->permissions()->attach($newPermission->id);
        }

        $subAdminRole = Role::where('name', config('constants.SUB_ADMIN'))->first();
        foreach ($subAdminPermissions as $permission) {
            $newPermission = Permission::create($permission);
            $adminRole->permissions()->attach($newPermission->id);
            $subAdminRole->permissions()->attach($newPermission->id);
        }

        $userRole = Role::where('name', config('constants.USER'))->first();
        foreach ($userPermission as $permission) {
            $newPermission = Permission::create($permission);
            $adminRole->permissions()->attach($newPermission->id);
            $userRole->permissions()->attach($newPermission->id);
        }
    }
}
