<?php

use App\Models\Subject;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CurriculumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            Subject::create(['name' => $faker->word, 'type' => 'curriculum']);
        }
    }
}
